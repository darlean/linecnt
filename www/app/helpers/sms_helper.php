<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function sms_sender($in_msg, $in_rphone, $in_sphone, $in_subject = '', $in_smsType = '', $nointeractive = true)
{
	/******************** 인증정보 ********************/        
    $sms['user_id'] = base64_encode("hbnetwork4"); //SMS 아이디.
    $sms['secure'] = base64_encode("d68ba7569098021262774a5f27a38b85");//인증키
    $sms['msg'] = base64_encode(stripslashes($in_msg));

    if($in_smsType == 'L') 
    {
        $sms['subject'] = base64_encode($in_subject); //제목        
        $sms['smsType'] = base64_encode($in_smsType); // LMS일경우 L
    }    
    
    $sms['rphone'] = base64_encode($in_rphone);      
    $sms['mode'] = base64_encode("1"); // base64 사용시 반드시 모드값을 1로 주셔야 합니다.    

    //$sms['testflag'] = base64_encode('Y');                            

    if ( strlen($in_sphone) == 8 )
    {
    	$sms['sphone1'] = base64_encode(substr($in_sphone, 0, 4)) ;
    	$sms['sphone2'] = base64_encode(substr($in_sphone, -4)) ;	        
    }
    else
    {
    	$sms['sphone1'] = base64_encode(substr($in_sphone, 0, 3)) ;
    	$sms['sphone2'] = base64_encode(substr($in_sphone, 3, -4)) ;
    	$sms['sphone3'] = base64_encode(substr($in_sphone, -4)) ;	        
    }

    $sms_url = "http://sslsms.cafe24.com/sms_sender.php"; // 전송요청 URL
    // $sms_url = "https://sslsms.cafe24.com/sms_sender.php"; // HTTPS 전송요청 URL

    $host_info = explode("/", $sms_url);
    $host = $host_info[2];
    $path = $host_info[3];

    srand((double)microtime()*1000000);
    $boundary = "---------------------".substr(md5(rand(0,32000)),0,10);
    //print_r($sms);

    // 헤더 생성
    $header = "POST /".$path ." HTTP/1.0\r\n";
    $header .= "Host: ".$host."\r\n";
    $header .= "Content-type: multipart/form-data, boundary=".$boundary."\r\n";

    $data = "" ;
    // 본문 생성
    foreach($sms AS $index => $value)
    {
        $data .="--$boundary\r\n";
        $data .= "Content-Disposition: form-data; name=\"".$index."\"\r\n";
        $data .= "\r\n".$value."\r\n";
        $data .="--$boundary\r\n";
    }
    $header .= "Content-length: " . strlen($data) . "\r\n\r\n";

    $fp = fsockopen($host, 80);

    if ($fp) 
    {
        fputs($fp, $header.$data);
        $rsp = '';
        while(!feof($fp)) {
            $rsp .= fgets($fp,8192);
        }
        fclose($fp);
        $msg = explode("\r\n\r\n",trim($rsp));
        $rMsg = explode(",", $msg[1]);
        $Result= $rMsg[0]; //발송결과
        $Count= $rMsg[1]; //잔여건수

        //발송결과 알림
        if($Result=="success") 
        {
            $alert = "성공적으로 발송(예약)되었습니다.";
            $alert .= " 잔여건수는 ".$Count."건 입니다.";
        }
        else if($Result=="reserved") {
            $alert = "성공적으로 발송(예약)되었습니다.";
            $alert .= " 잔여건수는 ".$Count."건 입니다.";
        }
        else if($Result=="3205") {
            $alert = "잘못된 번호형식입니다.";
        }

        else if($Result=="0044") {
            $alert = "스팸문자는발송되지 않습니다.";
        }

        else {
            $alert = "[Error]".$Result;
        }
    }
    else {
        $alert = "Connection Failed";
    }

    if($nointeractive=="1" && ($Result!="success" && $Result!="Test Success!" && $Result!="reserved") ) 
    {
        echo "<script>alert('".$alert ."')</script>";
    }
    else if($nointeractive!="1") 
    {
        echo "<script>alert('".$alert ."')</script>";
    }    
}
?>