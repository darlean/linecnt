<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// 회원권한을 SELECT 형식으로 얻음
function get_weekly_day_index($day)
{
	$convert = array(0, 6, 5, 4, 3, 2, 1);
	if( $day > count( $convert ) )
	{
		return 0;
	}
	return $convert[$day-1];
}

function get_day_string($date)
{
    $yo = array("일요일","월요일","화요일","수요일","목요일","금요일","토요일");
    return $yo[date("w",strtotime($date))];   
}

function get_week()
{
	$result = array();	
	$curr_date = TIME_YMD;

	$day 					= get_weekly_day_index(date("N", strtotime($curr_date)));
    $result[ "end_date" ]	= date("Y-m-d", strtotime($curr_date." + ".$day." day"));    
    $result[ "start_date" ]	= date("Y-m-d", strtotime($result[ "end_date" ]." - 6 day"));    

    return $result;
}

function get_last_week($curr_date=FALSE)
{
	$result = array();

	if( $curr_date == FALSE )
		$curr_date = TIME_YMD;

	$day 					= get_weekly_day_index(date("N", strtotime($curr_date)));
    $end_date 				= date("Y-m-d", strtotime($curr_date." + ".$day." day"));
    $result[ "end_date" ] 	= date("Y-m-d", strtotime($end_date." - 1 week"));
    $result[ "start_date" ] 	= date("Y-m-d", strtotime($result[ "end_date" ]." - 6 day"));    

    return $result;
}

function get_next_week($curr_date=FALSE)
{
	$result = array();

	if( $curr_date == FALSE )
		$curr_date = TIME_YMD;

	$day 					= get_weekly_day_index(date("N", strtotime($curr_date)));
    $end_date 				= date("Y-m-d", strtotime($curr_date." + ".$day." day"));
    $result[ "end_date" ] 	= date("Y-m-d", strtotime($end_date." + 1 week"));
    $result[ "start_date" ] 	= date("Y-m-d", strtotime($result[ "end_date" ]." - 6 day"));    

    return $result;
}