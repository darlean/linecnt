<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function mail_sender($in_to, $in_subject, $in_message)
{
    $subject = "=?UTF-8?B?".base64_encode($in_subject)."?=" ;
    
    $message = wordwrap($in_message, 70);

    $headers = 'From: webmaster@hbn.co.kr' . "\r\n" .
        'Reply-To: webmaster@hbn.co.kr' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    mail($in_to, $subject, $message, $headers) ;
}
?>