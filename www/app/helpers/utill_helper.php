<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function typedTel($in_tel)
{
    $tmp_tel = $in_tel ;
    $out_tel = "" ;

    $out_tel = substr($tmp_tel, 0, 3)."-" ;             

    $str = substr($tmp_tel, 3, -4) ;
    if ( strlen($str) > 4 )
    {                
        $out_tel .= substr($tmp_tel, 4, -5) ;
    }
    else
    {
        $out_tel .= $str ;     
    }
    
    $out_tel .= "-".substr($tmp_tel, -4) ;

    return $out_tel ;
}

?>