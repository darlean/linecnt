<?php
class D_Rank_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function get_rank_count($in_year, $in_month, $in_rank, $in_show)
	{
		$start = $in_year.$in_month.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_month + 1, 1, $in_year)) ;
		
		$this->db->from("D_RANK A") ;
		$this->db->where("REG_DATE BETWEEN {$start} AND {$finish}") ;
		$this->db->where('RANK_CD', $in_rank) ;

		if ( $in_show != 'all')
		{
			$this->db->where('RANK_KIND', $in_show) ;	
		}

		if( !IS_MANAGER && !IS_DIRECT_BRANCH && !IS_CENTER && !IS_AGENCY && !IS_CARD_AGENCY && !IS_BRANCH )
		{
			$user_id = $this->session->userdata('ss_mb_id') ;
			$subquery = "SELECT USERID FROM D_MEMBER START WITH USERID = '".$user_id."' CONNECT BY P_ID = PRIOR USERID" ;
        	$this->db->where_in('A.USERID', $subquery, FALSE) ; 
		}

		return $this->db->count_all_results() ;
	}

	function get_rank_list($in_year, $in_month, $in_rank, $in_show, $limit, $offset)
	{
		$this->db->limit($limit, $offset) ;

		$start = $in_year.$in_month.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_month + 1, 1, $in_year)) ;
		
		$select = "A.USERID, PKG_CRYPTO_FIELD.get('USERNAME', B.USERNAME, '9', '".G_CRYPTO."') AS USERNAME, 
					TO_CHAR(TO_DATE(A.REG_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') as EXE_DATE,
					TO_CHAR(TO_DATE(B.REG_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') as REG_DATE,
					(SELECT RANK_NAME FROM S_RANK WHERE RANK_CD = A.OLD_RANK_CD) as OLD_RANK_CD,
					(SELECT RANK_NAME FROM S_RANK WHERE RANK_CD = A.RANK_CD) as RANK_CD,
					DECODE(A.RANK_KIND, '0', '', '1', '승급', '2', '강등') AS RANK_KIND" ;

		$this->db->select($select, false) ;
		$this->db->from("D_RANK A") ;
		$this->db->join("D_MEMBER B", "A.USERID = B.USERID", "LEFT") ;

		$this->db->where("A.REG_DATE BETWEEN {$start} AND {$finish}") ;        
		$this->db->where('A.RANK_CD', $in_rank) ;

		if ( $in_show != 'all')
		{
			$this->db->where('A.RANK_KIND', $in_show) ;	
		}

		if( !IS_MANAGER && !IS_DIRECT_BRANCH && !IS_CENTER && !IS_AGENCY && !IS_CARD_AGENCY && !IS_BRANCH )
		{
			$user_id = $this->session->userdata('ss_mb_id') ;
			$subquery = "SELECT USERID FROM D_MEMBER START WITH USERID = '".$user_id."' CONNECT BY P_ID = PRIOR USERID" ;
        	$this->db->where_in('A.USERID', $subquery, FALSE) ; 
		}		

		$this->db->order_by('A.REG_DATE', "desc") ;

		return $this->db->get()->result_array() ;
	}
}
?>