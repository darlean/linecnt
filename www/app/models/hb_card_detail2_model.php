<?php
class HB_card_detail2_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function insert() 
	{	
		$pdt_cd = $this->input->post('PDT_CD') ;
		$pdt_name = $this->input->post('PDT_NAME') ;

		$arr_cdp_id = $this->input->post('CDP_ID') ;
		$arr_cdp_name = $this->input->post('CDP_NAME') ;

		$arr_save_rate = $this->input->post('SAVE_RATE') ;
		$arr_save_limit = $this->input->post('SAVE_LIMIT') ;
		$arr_last_month_result = $this->input->post('LAST_MONTH_RESULT') ;
		$arr_etc = $this->input->post('ETC') ;
        $arr_card_place_result = $this->input->post('CARD_PLACE_RESULT') ;

		$arr_cdd_id = $this->input->post('CDD_ID') ;

		foreach ($arr_cdp_id as $key => $value) 
		{
			$SAVE_RATE    = (isset($arr_save_rate[$key]) && ($arr_save_rate[$key] != '')) ? $arr_save_rate[$key] : 0 ;
			$SAVE_LIMIT    = (isset($arr_save_limit[$key]) && ($arr_save_limit[$key] != '')) ? $arr_save_limit[$key] : 0 ;
			$LAST_MONTH_RESULT    = (isset($arr_last_month_result[$key]) && ($arr_last_month_result[$key] != '')) ? $arr_last_month_result[$key] : 0 ;
            $CARD_PLACE_RESULT    = (isset($arr_card_place_result[$key]) && ($arr_card_place_result[$key] != '')) ? $arr_card_place_result[$key] : 0 ;

           $IS_ACCEPT_RESULT    = ($this->input->post("IS_ACCEPT_RESULT_".$value) == 'on') ? 'O' : 'X' ;

			if ( $arr_cdd_id[$key] == '' )
			{
				$sql = "INSERT INTO HB_CARD_DETAIL2 (CDD_ID, PDT_CD, PDT_NAME, CDP_ID, CDP_NAME, SAVE_RATE, SAVE_LIMIT, IS_ACCEPT_RESULT, ETC, LAST_MONTH_RESULT, CARD_PLACE_RESULT) 
						VALUES (SEQ_HB_CARD_DETAIL.NEXTVAL, '".$pdt_cd."', '".$pdt_name."', '".$arr_cdp_id[$key]."', '".$arr_cdp_name[$key]."'
						, '".$SAVE_RATE."', '".$SAVE_LIMIT."', '".$IS_ACCEPT_RESULT."', '".$arr_etc[$key]."', '".$LAST_MONTH_RESULT."', '".$CARD_PLACE_RESULT."')" ;
			}
			else
			{
				$sql = "UPDATE HB_CARD_DETAIL2 SET SAVE_RATE = '".$SAVE_RATE."', SAVE_LIMIT = '".$SAVE_LIMIT."', 
						IS_ACCEPT_RESULT = '".$IS_ACCEPT_RESULT."', ETC = '".$arr_etc[$key]."', LAST_MONTH_RESULT = '".$LAST_MONTH_RESULT."', CARD_PLACE_RESULT = '".$CARD_PLACE_RESULT."' WHERE CDD_ID = '".$arr_cdd_id[$key]."'" ;
			}				

			$result = $this->db->query($sql) ;
		}		

		return $result ;
	}

	function get_card_detail_info($in_pdt_cd)
	{
		$this->db->where('PDT_CD', $in_pdt_cd) ;

		$result = $this->db->get('HB_CARD_DETAIL2')->result_array() ;

		$list = array() ;
		foreach ($result as $key => $value) 
		{
			$list[$value['CDP_ID']] = $value ;
		}

		return $list ;
	}
}
?>