<?php
class Pay_model extends CI_Model 
{
    function __construct() {
        parent::__construct();
    }

    function get_pay_list_count($in_type, $in_user_id, $in_pay_date = '')
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        $where1 = array(
              'A.PAY_DATE'    => 'B.PAY_DATE',              
              'A.OLD_RANK_CD' => 'C.RANK_CD',
              'A.RANK_CD'     => 'D.RANK_CD',               
          ) ;

        $where2 = array(              
              'B.PAY_KIND'    => '1',
              'B.SHOW_YN'     => 'Y',                      
          ) ;

        if( !IS_MANAGER )
        {
            $where2['A.USERID'] = $in_user_id ;          
        }

        $from = '' ;

        if ( $in_type == 'day' )
        {
            $from = 'PAY_DAY A' ;
        }
        else if ( $in_type == 'week' )
        {
            $from = 'PAY_WEEK2 A' ;
            $where2['B.PAY_KIND'] = '2' ;
        }
        else if ( $in_type == 'month' )
        {
            $from = 'PAY_MONTH2 A' ;
            $where2['B.PAY_KIND'] = '4' ;
        }

        $this->db->from($from.', PAY_CALC_INFO B, S_RANK C, S_RANK D') ;
        $this->db->where($where1, NULL, false) ;

        if ( !IS_MANAGER )
        {
            $this->db->where($where2) ;        
        }        

        if ( $in_pay_date != '' )
        {
            $this->db->where('A.PAY_DATE', $in_pay_date) ;
        }                

        return $this->db->count_all_results() ;
    }

    function get_pay_list($in_type, $in_user_id, $sst, $sod, $limit, $offset, $in_pay_date = '')
    {
        if ($sst && $sod)
        {
            $this->db->order_by($sst, $sod);
        }

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        $sql = "A.*, C.FULL_NAME AS OLD_FULL_NAME, D.FULL_NAME,
               PKG_CRYPTO_FIELD.get('USERNAME', A.USERNAME,'9','".G_CRYPTO."')||'('||A.USERID||')' AS USERTEXT,
               TO_CHAR(TO_DATE(A.PAY_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') AS PAY_DATES,
               TO_CHAR(TO_DATE(B.GIVE_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') AS GIVE_DATES,
               (A.TAX1+A.TAX2) AS TOTAL_TAX" ;

        $this->db->select($sql, false) ;    

        $where1 = array(
              'A.PAY_DATE'    => 'B.PAY_DATE',              
              'A.OLD_RANK_CD' => 'C.RANK_CD',
              'A.RANK_CD'     => 'D.RANK_CD',                           
          ) ;

        $where2 = array(              
              'B.PAY_KIND'    => '1',
              'B.SHOW_YN'     => 'Y',              
          ) ;

        if( !IS_MANAGER )
        {
            $where2['A.USERID'] = $in_user_id ;          
        }

        $from = '' ;

        if ( $in_type == 'day' )
        {
            $from = 'PAY_DAY A' ;
        }
        else if ( $in_type == 'week' )
        {
            $from = 'PAY_WEEK2 A' ;
            $where2['B.PAY_KIND'] = 2 ;
        }
        else if ( $in_type == 'month' )
        {
            $from = 'PAY_MONTH2 A' ;
            $where2['B.PAY_KIND'] = 4 ;
        }

        $this->db->from($from.', PAY_CALC_INFO B, S_RANK C, S_RANK D') ;
        $this->db->where($where1, NULL, false) ;

        if ( !IS_MANAGER )
        {
            $this->db->where($where2) ;        
        }

        if ( $in_pay_date != '' )
        {
            $this->db->where('A.PAY_DATE', $in_pay_date) ;
        }        

        return $this->db->get()->result_array() ;
    }

    // 기간 검색
    function _get_search_date_range()
    {
        $sdt = 'A.PAY_DATE' ;
        $start_date = $this->input->get('start');   // 시작날짜
        $finish_date = $this->input->get('finish'); // 종료날짜

        if ( $start_date != '' )
        {
            $this->db->where($sdt.' >= ', "'".$start_date."'", false) ;
        }

        if ( $finish_date != '' )
        {
            $this->db->where($sdt.' <= ', "'".$finish_date."'", false) ;
        }
    }

    function get_user_grp_info($in_userid, $in_pay_date)
    {
        $this->db->select('CALC_GRP_CV, OLD_RANK_CD') ;
        $this->db->where(array('ISCLASS' => '1', 'PAY_DATE' => $in_pay_date, 'USERID' => $in_userid)) ;

        return $this->db->get('PAY_WEEK')->row_array() ;        
    }

    function get_pay_calc_info($in_pay_date)
    {        
        $this->db->where('PAY_DATE', $in_pay_date) ; 

        return $this->db->get('PAY_CALC_INFO')->row_array() ;        
    }

    function get_pay_date_list()
    {
        $this->db->select('PAY_DATE') ;                
        $this->db->order_by('PAY_DATE', 'DESC') ;

        if ( !IS_MANAGER )
        {         
            $this->db->where('SHOW_YN', 'Y') ;
        }

        return $this->db->get('PAY_CALC_INFO')->result_array() ;    
    }

    function get_rank_count($in_rank, $in_pay_date)
    {
        $this->db->from("PAY_WEEK") ;        
        $this->db->where('ISCLASS', '1') ;
        $this->db->where('PAY_DATE', $in_pay_date) ;              

        if ( $in_rank > 0 )      
        {
            $this->db->where('OLD_RANK_CD', $in_rank) ;    
        }
        else
        {
            $this->db->where('OLD_RANK_CD >= 31') ;       
        }                     

        return $this->db->count_all_results() ;
    }

    function get_rank_list($in_rank, $limit, $offset, $in_pay_date)
    {
        $this->db->limit($limit, $offset) ;
        
        $select = "A.USERID, A.CALC_GRP_CV,
                    PKG_CRYPTO_FIELD.get('USERNAME', B.USERNAME, '9', '".G_CRYPTO."') AS USERNAME,                     
                    TO_CHAR(TO_DATE(B.REG_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') as REG_DATE,
                    (SELECT RANK_NAME FROM S_RANK WHERE RANK_CD = A.OLD_RANK_CD) as OLD_RANK_CD,
                    (SELECT RANK_NAME FROM S_RANK WHERE RANK_CD = A.RANK_CD) as RANK_CD" ;

        $this->db->select($select, false) ;
        $this->db->from("PAY_WEEK A") ;
        $this->db->join("D_MEMBER B", "A.USERID = B.USERID", "LEFT") ;        
        $this->db->where('A.ISCLASS', '1') ;
        $this->db->where('A.PAY_DATE', $in_pay_date) ;        

        if ( $in_rank > 0 )      
        {
            $this->db->where('A.OLD_RANK_CD', $in_rank) ;    
        }
        else
        {
            $this->db->where('A.OLD_RANK_CD >= 31') ;       
        }

        $this->db->order_by('A.CALC_GRP_CV', "desc") ;
        $this->db->order_by('A.USERID', "asc") ;

        return $this->db->get()->result_array() ;                      
    }

    function get_total_grp($in_rank, $in_pay_date)
    {        
        $select = "SUM(CALC_GRP_CV) AS GRP" ;

        $this->db->select($select, false) ;
        $this->db->from("PAY_WEEK") ;        
        $this->db->where('ISCLASS', '1') ;
        $this->db->where('PAY_DATE', $in_pay_date) ;        

        if ( $in_rank > 0 )      
        {
            $this->db->where('OLD_RANK_CD', $in_rank) ;    
        }
        else
        {
            $this->db->where('OLD_RANK_CD >= 31') ;       
        }

        $result = $this->db->get()->row_array() ;

        return isset($result['GRP']) ? $result['GRP'] : 0 ;
    }

    function get_member_count_subtree_classified_by_rank( $in_pay_date, $in_user_id )
    {
        $sql = "SELECT count(*) AS CNT, A.OLD_RANK_CD 
                FROM PAY_WEEK A
                JOIN (SELECT USERID FROM D_MEMBER WHERE USERID <> '$in_user_id' 
                START WITH USERID = '$in_user_id'
                CONNECT BY PRIOR USERID = P_ID) B ON A.USERID = B.USERID 
                WHERE A.ISCLASS =  '1'
                AND A.PAY_DATE =  '$in_pay_date'
                AND A.OLD_RANK_CD >= 31 
                GROUP BY A.OLD_RANK_CD" ;  

        $result = $this->db->query($sql)->result_array() ;

        $list = array() ;
        foreach ($result as $key => $row) 
        {
            $list[$row['OLD_RANK_CD']] = isset($row['CNT']) ? $row['CNT'] : 0 ;
        }

        return $list ;
    }

    function get_member_subtree_total_pv( $in_pay_date, $in_user_id )
    {
        $sql = "SELECT SUM(PV) AS PV 
                FROM PAY_WEEK A
                JOIN (SELECT USERID FROM D_MEMBER WHERE USERID <> '$in_user_id' 
                START WITH USERID = '$in_user_id'
                CONNECT BY PRIOR USERID = P_ID) B ON A.USERID = B.USERID 
                WHERE A.ISCLASS =  '1'
                AND A.PAY_DATE =  '$in_pay_date'" ;  

        $result = $this->db->query($sql)->row_array() ;

        return isset($result['PV']) ? number_format($result['PV']) : 0 ;
    }

    function get_honor_list($in_pay_date)
    {
        $this->db->select("A.USERID, PKG_CRYPTO_FIELD.get('USERNAME', B.USERNAME, '9', '".G_CRYPTO."') AS USERNAME, A.RANK_CD, A.OLD_RANK_CD, A.AMT_WCY", false) ;
        $this->db->where('A.PAY_DATE', $in_pay_date) ;
        $this->db->where('A.IS_FULL', 'O') ;
        $this->db->join("D_MEMBER B", "A.USERID = B.USERID", "LEFT") ;
        $this->db->order_by('A.USERID', "asc") ;

        $result = $this->db->get('PAY_WEEK2 A')->result_array() ;

        $list = array() ;
        $i = 0 ;

        foreach ($result as $item)
        {
            if ( !isset($list[$item['RANK_CD']]) )
            {
                $i = 0 ;
            }

            $list[$item['RANK_CD']][$i] = $item ;

            $i++ ;
        }

        return $list ;
    }
}
?>