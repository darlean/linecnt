<?php
class HB_Use_ucube_center_model extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

	function list_count()
	{
        $this->db->where('ISUSE', 'O');

        $this->db->where('CENTER_CD < ', '1000') ;
        $this->db->where('CENTER_CD > ', '0000') ;

		return $this->db->count_all_results('S_CENTER') ;
	}

	function list_result($limit, $offset)
	{
		$this->db->limit($limit, $offset) ;

		$this->db->select("B.CENTER_CD, B.CENTER_NAME, A.USE_UCUBE, A.HELP_CENTER_CDS", false) ;

        $this->db->from('HB_USE_UCUBE_CENTER A') ;
        $this->db->join('S_CENTER B', 'A.CENTER_CD = B.CENTER_CD', 'right') ;

        $this->db->where('B.ISUSE', 'O') ;
        $this->db->where('B.CENTER_CD < ', '1000') ;
        $this->db->where('B.CENTER_CD > ', '0000') ;
        
        $this->db->order_by('B.CENTER_NAME', 'asc') ;

		return $this->db->get()->result_array() ;
	}

	function add_help_center($in_select_center_cd, $in_center_cd)
    {
        if ( $in_select_center_cd == $in_center_cd )
        {
            return false ;
        }

        if ( $this->exist_help_center($in_select_center_cd, $in_center_cd) )
        {
            return false ;
        }

        $count = $this->get_count($in_select_center_cd) ;

        if ( $count > 0 )
        {
            $sql = "UPDATE HB_USE_UCUBE_CENTER SET HELP_CENTER_CDS = HELP_CENTER_CDS||',".$in_center_cd."' WHERE CENTER_CD = '".$in_select_center_cd."'" ;
        }
        else
        {
            $sql = "INSERT INTO HB_USE_UCUBE_CENTER VALUES ('".$in_select_center_cd."', 'X', '".$in_center_cd."')" ;
        }

        return $this->db->query($sql) ;
    }

    function get_count($in_center_cd)
    {
        $this->db->where('CENTER_CD', $in_center_cd) ;

        return $this->db->count_all_results('HB_USE_UCUBE_CENTER') ;
    }

    function exist_help_center($in_select_center_cd, $in_center_cd)
    {
        $this->db->select('HELP_CENTER_CDS') ;
        $this->db->where('CENTER_CD', $in_select_center_cd) ;

        $result = $this->db->get('HB_USE_UCUBE_CENTER')->row_array() ;

        if ( isset($result['HELP_CENTER_CDS']) && $result['HELP_CENTER_CDS'] != '' )
        {
            $pos = strpos($result['HELP_CENTER_CDS'], $in_center_cd);

            if ($pos === false) {
                return false;
            }
            else {
                return true;
            }
        }

        return false ;
    }

    function del_help_center($in_select_center_cd, $in_center_cd)
    {
        $this->db->select('HELP_CENTER_CDS') ;
        $this->db->where('CENTER_CD', $in_select_center_cd) ;

        $result = $this->db->get('HB_USE_UCUBE_CENTER')->row_array() ;

        if ( isset($result['HELP_CENTER_CDS']) && $result['HELP_CENTER_CDS'] != '' ) {

            $arr = explode(',', $result['HELP_CENTER_CDS'] ) ;

            $help_center_cds = "" ;
            foreach ($arr as $value) {
                if ($value != "" && $value != $in_center_cd) {
                    $help_center_cds .= "," . $value;
                }
            }

            $sql = "UPDATE HB_USE_UCUBE_CENTER SET HELP_CENTER_CDS = '".$help_center_cds."' WHERE CENTER_CD = '".$in_select_center_cd."'" ;
            return $this->db->query($sql) ;
        }

        return false ;
    }

    function use_ucube($in_center_cd, $in_use_ucube)
    {
        $count = $this->get_count($in_center_cd) ;

        if ( $count > 0 )
        {
            $sql = "UPDATE HB_USE_UCUBE_CENTER SET USE_UCUBE = '".$in_use_ucube."' WHERE CENTER_CD = '".$in_center_cd."'" ;
        }
        else
        {
            $sql = "INSERT INTO HB_USE_UCUBE_CENTER VALUES ('".$in_center_cd."', '".$in_use_ucube."', '')" ;
        }

        return $this->db->query($sql) ;
    }
}
?>