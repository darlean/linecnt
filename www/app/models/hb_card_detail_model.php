<?php
class HB_card_detail_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function insert() 
	{	
		$cd_id = $this->input->post('CD_ID') ;
		$cd_name = $this->input->post('CD_NAME') ;

		$arr_cdp_id = $this->input->post('CDP_ID') ;
		$arr_cdp_name = $this->input->post('CDP_NAME') ;

		$arr_save_rate = $this->input->post('SAVE_RATE') ;
		$arr_save_limit = $this->input->post('SAVE_LIMIT') ;
		$arr_etc = $this->input->post('ETC') ;
		$arr_is_accept_result = $this->input->post('IS_ACCEPT_RESULT') ;

		$arr_cdd_id = $this->input->post('CDD_ID') ;

		foreach ($arr_cdp_id as $key => $value) 
		{
			$SAVE_RATE    = (isset($arr_save_rate[$key]) && ($arr_save_rate[$key] != '')) ? $arr_save_rate[$key] : 0 ;
			$SAVE_LIMIT    = (isset($arr_save_limit[$key]) && ($arr_save_limit[$key] != '')) ? $arr_save_limit[$key] : 0 ;
			$IS_ACCEPT_RESULT    = (isset($arr_is_accept_result[$key]) && ($arr_is_accept_result[$key] == 'on')) ? 'O' : 'X' ;					

			if ( $arr_cdd_id[$key] == '' )
			{
				$sql = "INSERT INTO HB_CARD_DETAIL (CDD_ID, CD_ID, CD_NAME, CDP_ID, CDP_NAME, SAVE_RATE, SAVE_LIMIT, IS_ACCEPT_RESULT, ETC) 
						VALUES (SEQ_HB_CARD_DETAIL.NEXTVAL, '".$cd_id."', '".$cd_name."', '".$arr_cdp_id[$key]."', '".$arr_cdp_name[$key]."'
						, '".$SAVE_RATE."', '".$SAVE_LIMIT."', '".$IS_ACCEPT_RESULT."', '".$arr_etc[$key]."')" ;
			}
			else
			{
				$sql = "UPDATE HB_CARD_DETAIL SET SAVE_RATE = '".$SAVE_RATE."', SAVE_LIMIT = '".$SAVE_LIMIT."', 
						IS_ACCEPT_RESULT = '".$IS_ACCEPT_RESULT."', ETC = '".$arr_etc[$key]."' WHERE CDD_ID = '".$arr_cdd_id[$key]."'" ;
			}				

			$result = $this->db->query($sql) ;
		}		

		return $result ;
	}

	function get_card_detail_info($in_cd_id)
	{
		$this->db->where('CD_ID', $in_cd_id) ;

		$result = $this->db->get('HB_CARD_DETAIL')->result_array() ;

		$list = array() ;
		foreach ($result as $key => $value) 
		{
			$list[$value['CDP_ID']] = $value ;
		}

		return $list ;
	}
}
?>