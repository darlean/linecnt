<?php
class S_Center_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function get_center_list($bAll = false)
	{
		$this->db->select('CENTER_CD, CENTER_NAME') ;

        if ( !$bAll )
        {
            $this->db->where('ISUSE', 'O');
        }

		$result = $this->db->get('S_CENTER')->result_array() ;

		$list = array() ;
		foreach ($result as $key => $row) 
		{
			if ( $bAll || (int)$row['CENTER_CD'] < 9990 )
			{
				$list[$row['CENTER_CD']] = $row['CENTER_NAME'] ;
			}
		}

		return $list ;
	}

	function get_center_name($in_center_cd)
	{
		$this->db->select('CENTER_NAME') ;
		$this->db->where('CENTER_CD', $in_center_cd) ;

		$result = $this->db->get('S_CENTER')->row_array() ;

		return  ( $result && isset($result['CENTER_NAME']) ) ? $result['CENTER_NAME'] : '' ;
	}

	function list_count($sido = '', $bCenter = '')
	{
        $this->db->where('ISUSE', 'O') ;

        if ( $sido != '' )
        {
            $this->db->where('ADDR1 LIKE', '%'.$sido.'%') ;            
        }

        if ( $bCenter )
        {
            $this->db->where('CENTER_CD < ', '1000') ;                
            $this->db->where('CENTER_CD > ', '0000') ;                
        }
        else
        {
            $this->db->where('CENTER_CD < ', '9990') ;
            $this->db->where('CENTER_CD >= ', '1000') ;                
            
            if ( $sido == '' || $sido == '서울특별시' )
            {
                $this->db->or_where('CENTER_CD', '0000') ;                
            }                          
        }

		return $this->db->count_all_results('S_CENTER') ;
	}

	function list_result($limit, $offset, $sido = '', $bCenter = '')
	{
		$this->db->limit($limit, $offset) ;

		$this->db->select("CENTER_CD, CENTER_NAME, TEL, FAX, ZIPCODE, ADDR1, ADDR2, TO_CHAR(TO_DATE(OPEN_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') AS OPEN_DATE", false) ;

        $this->db->where('ISUSE', 'O') ;

        if ( $sido != '' )
        {
            $this->db->where('ADDR1 LIKE', '%'.$sido.'%') ;            
        }

        if ( $bCenter )
        {
            $this->db->where('CENTER_CD < ', '1000') ;                
            $this->db->where('CENTER_CD > ', '0000') ;                
        }
        else
        {
            $this->db->where('CENTER_CD < ', '9990') ;
            $this->db->where('CENTER_CD >= ', '1000') ;                
            
            if ( $sido == '' || $sido == '서울특별시' )
            {
                $this->db->or_where('CENTER_CD', '0000') ;                
            }                          
        }
        
        $this->db->order_by('CENTER_CD', 'asc') ;

		return $this->db->get('S_CENTER')->result_array() ;
	}

	function get_order_open_count_by_center($in_center_cd, $in_Y, $in_M, $in_ord_type = 0) 
    {   
        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;    

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;           

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;          
        $this->db->where('A.ORD_DATE >=', $start) ; 
        $this->db->where('A.ORD_DATE <', $finish) ;   
        $this->db->where('A.CENTER_CD', $in_center_cd) ;     

        if ( $in_ord_type == 0 ) // 개통완료
        {
            $this->db->where('A.ORD_TYPE', 1) ;            
            $this->db->where('A.ORD_STATE >', 10) ; 
            $this->db->where('A.ORD_STATE <', 20) ; 
        }
        else
        {
            $this->db->where('A.ORD_TYPE', $in_ord_type) ;

            if ( $in_ord_type == 1 )
            {
                $this->db->or_where('(A.ORD_STATE <= 10 AND A.ORD_STATE >= 20)', NULL, false) ;
            }
        }

        return $this->db->count_all_results() ;
    }

    function get_order_open_list_by_center($in_center_cd, $limit, $offset, $in_Y, $in_M, $in_ord_type) 
    {   
        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;          

        $this->db->limit($limit, $offset) ;

        $sql = "A.*, PKG_CRYPTO_FIELD.get('USERNAME', Z.USERNAME,'9','".G_CRYPTO."')||'('||A.USERID||')' AS USERTEXT,
               TO_CHAR(TO_DATE(A.ORD_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') AS ORD_DATES, 
               DECODE(A.ORD_KIND, '1', 'BV', '2', 'SV', '3', '재구매') AS ORD_KINDS,
               DECODE(A.ORD_TYPE, '1', '주문', '2', '반품', '3', '교환', '4', '취소') AS ORD_TYPES, C.TYPE_CD" ;

        $this->db->select($sql, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;        
        $this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID') ;        

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;    
        $this->db->where('A.ORD_DATE >=', $start) ; 
        $this->db->where('A.ORD_DATE <', $finish) ;   
        $this->db->where('A.CENTER_CD', $in_center_cd) ;     

        if ( $in_ord_type == 0 ) // 개통완료
        {
            $this->db->where('A.ORD_TYPE', 1) ;            
            $this->db->where('A.ORD_STATE >', 10) ; 
            $this->db->where('A.ORD_STATE <', 20) ; 
        }
        else
        {
            $this->db->where('A.ORD_TYPE', $in_ord_type) ;

            if ( $in_ord_type == 1 )
            {
                $this->db->or_where('(A.ORD_STATE <= 10 AND A.ORD_STATE >= 20)', NULL, false) ;            
            }
        }

        return $this->db->get()->result_array() ;
    }

    function get_cv_pv_by_center($in_center_cd, $in_Y, $in_M)
    {
        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;    

        $sql = "SUM(A.PV1) AS CV, SUM(A.PV2) AS PV, SUM(A.PRICE) AS PRICE, SUM(A.VAT) AS VAT, SUM(A.AMT) AS AMT, SUM(A.PV4) AS PV4" ;

        $this->db->select($sql, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;        

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        $this->db->where('A.ORD_TYPE', '1') ; 
        $this->db->where('A.ORD_STATE >', 10) ; 
        $this->db->where('A.ORD_STATE <', 20) ;     
        $this->db->where('A.ORD_DATE >=', $start) ; 
        $this->db->where('A.ORD_DATE <', $finish) ;           
        $this->db->where('A.CENTER_CD', $in_center_cd) ;         

        return $this->db->get()->row_array() ;
    }

    function get_cv_pv_by_center2($in_center_cd, $in_Y, $in_M)
    {
        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;    

        $sql = "SUM(A.PV1) AS CV, SUM(A.PV2) AS PV, SUM(A.PRICE) AS PRICE, SUM(A.VAT) AS VAT, SUM(A.AMT) AS AMT, SUM(A.PV4) AS PV4, C.TYPE_CD" ;

        $this->db->select($sql, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;        

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        $this->db->where('A.ORD_TYPE', '1') ; 
        $this->db->where('A.ORD_STATE >', 10) ; 
        $this->db->where('A.ORD_STATE <', 20) ;     
        $this->db->where('A.ORD_DATE >=', $start) ; 
        $this->db->where('A.ORD_DATE <', $finish) ;           
        $this->db->where('A.CENTER_CD', $in_center_cd) ;         
        $this->db->group_by('C.TYPE_CD') ;

        return $this->db->get()->result_array() ;
    }    

    function get_order_center_manager_id ( $in_ord_no )
    {
        $this->db->select('A.CENTER_CD') ;

        $this->db->join('O_ORDERMASTER B', 'A.CENTER_CD = B.CENTER_CD') ;
        
        $this->db->where('A.ISUSE', "O") ;
        $this->db->where("B.ORDNO", $in_ord_no) ;        

        $result = $this->db->get("S_CENTER A")->row_array() ;

        if ( $result )
        {
            return "HBN".$result['CENTER_CD'] ;
        }

        return "" ;
    }

    function get_order_center_email ( $in_ord_no )
    {
        $center_id = $this->get_order_center_manager_id($in_ord_no) ;

        if ( $center_id == "" )
            return ;

        $this->db->select("E_MAIL") ;                
        $this->db->where("USERID", $center_id) ;
        $this->db->where('ISUSE', "O") ;

        $result = $this->db->get("SM_USER")->row_array() ;       

        if ( $result )
        {
            return $result['E_MAIL'] ;
        }

        return "" ; 
    }

    function get_mobile_order_open_count($in_Y, $in_M)
    {
        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;

        $this->db->select("A.CENTER_CD, B.REG_KIND, B.TEL_COMPANY, COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERTEL B', 'B.ORDNO = A.ORDNO') ;
        $this->db->join('O_ORDERSTATE C', 'C.ORDNO = A.ORDNO') ;

        $this->db->where('A.IS_TEL_PDT', 'O') ;
        $this->db->where('A.ORD_TYPE', '1') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('C.STATE_CD !=', '7', false) ;
        $this->db->where('B.REAL_OPEN_DT4 >=', $start) ;
        $this->db->where('B.REAL_OPEN_DT4 <', $finish) ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD'));
        }

        $this->db->group_by( array ("A.CENTER_CD", "B.REG_KIND", "B.TEL_COMPANY") ) ;

        $result = $this->db->get()->result_array() ;

        $list = array() ;
        foreach ($result as $key => $value)
        {
            if ( isset($list[$value['CENTER_CD']][$value['TEL_COMPANY']][$value['REG_KIND']] ) && $list[$value['CENTER_CD']][$value['TEL_COMPANY']][$value['REG_KIND']]  >= 0 )
            {
                $list[$value['CENTER_CD']][$value['TEL_COMPANY']][$value['REG_KIND']]  += $value['CNT'] ;
            }
            else
            {
                $list[$value['CENTER_CD']][$value['TEL_COMPANY']][$value['REG_KIND']] = $value['CNT'] ;
            }

            if ( isset($list[$value['CENTER_CD']][$value['TEL_COMPANY']]['TOTAL'] ) && $list[$value['CENTER_CD']][$value['TEL_COMPANY']]['TOTAL']  >= 0 )
            {
                $list[$value['CENTER_CD']][$value['TEL_COMPANY']]['TOTAL'] += $value['CNT'] ;
            }
            else
            {
                $list[$value['CENTER_CD']][$value['TEL_COMPANY']]['TOTAL'] = $value['CNT'] ;
            }

            if ( isset($list[$value['CENTER_CD']]['TOTAL'] ) && $list[$value['CENTER_CD']]['TOTAL']  >= 0 )
            {
                $list[$value['CENTER_CD']]['TOTAL'] += $value['CNT'] ;
            }
            else
            {
                $list[$value['CENTER_CD']]['TOTAL'] = $value['CNT'] ;
            }

            if ( isset($list['TOTAL'] ) && $list['TOTAL']  >= 0 )
            {
                $list['TOTAL'] += $value['CNT'] ;
            }
            else
            {
                $list['TOTAL'] = $value['CNT'] ;
            }
        }

        return $list ;
    }

    function get_wired_order_open_count($in_Y, $in_M)
    {
        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;

        $this->db->select("A.CENTER_CD, B.TEL_COMPANY, COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDER_CTEL B', 'B.ORDNO = A.ORDNO') ;
        $this->db->join('O_ORDERSTATE C', 'C.ORDNO = A.ORDNO') ;

        $this->db->where('A.IS_TEL_PDT', 'O') ;
        $this->db->where('A.ORD_TYPE', '1') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('C.STATE_CD !=', '7', false) ;
        $this->db->where('B.REAL_OPEN_DT4 >=', $start) ;
        $this->db->where('B.REAL_OPEN_DT4 <', $finish) ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD'));
        }

        $this->db->group_by( array ("A.CENTER_CD", "B.TEL_COMPANY") ) ;

        $result = $this->db->get()->result_array() ;

        $list = array() ;
        foreach ($result as $key => $value)
        {
            if ( isset($list[$value['CENTER_CD']][$value['TEL_COMPANY']] ) && $list[$value['CENTER_CD']][$value['TEL_COMPANY']]  >= 0 )
            {
                $list[$value['CENTER_CD']][$value['TEL_COMPANY']] += $value['CNT'] ;
            }
            else
            {
                $list[$value['CENTER_CD']][$value['TEL_COMPANY']] = $value['CNT'] ;
            }

            if ( isset($list[$value['CENTER_CD']]['TOTAL'] ) && $list[$value['CENTER_CD']]['TOTAL']  >= 0 )
            {
                $list[$value['CENTER_CD']]['TOTAL'] += $value['CNT'] ;
            }
            else
            {
                $list[$value['CENTER_CD']]['TOTAL'] = $value['CNT'] ;
            }

            if ( isset($list['TOTAL'] ) && $list['TOTAL']  >= 0 )
            {
                $list['TOTAL'] += $value['CNT'] ;
            }
            else
            {
                $list['TOTAL'] = $value['CNT'] ;
            }
        }

        return $list ;
    }

    // 카운팅만
    function get_mobile_order_open_count2($in_Y, $in_M, $in_center = '')
    {
        if ( $in_M < 10 )
        {
            $in_M = '0'.$in_M ;
        }

        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;

        $this->db->select("COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERTEL B', 'B.ORDNO = A.ORDNO') ;
        $this->db->join('O_ORDERSTATE C', 'C.ORDNO = A.ORDNO') ;

        $this->db->where('A.IS_TEL_PDT', 'O') ;
        $this->db->where('A.ORD_TYPE', '1') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('C.STATE_CD !=', '7', false) ;
        $this->db->where('B.REAL_OPEN_DT4 >=', $start) ;
        $this->db->where('B.REAL_OPEN_DT4 <', $finish) ;

        if ( $in_center != '' )
        {
            $this->db->where('A.CENTER_CD', $in_center);
        }

        $result = $this->db->get()->row_array() ;

        return ( isset($result) && isset($result['CNT']) ) ? $result['CNT'] : 0 ;
    }

    function get_wired_order_open_count2($in_Y, $in_M, $in_center = '')
    {
        if ( $in_M < 10 )
        {
            $in_M = '0'.$in_M ;
        }

        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;

        $this->db->select("COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDER_CTEL B', 'B.ORDNO = A.ORDNO') ;
        $this->db->join('O_ORDERSTATE C', 'C.ORDNO = A.ORDNO') ;

        $this->db->where('A.IS_TEL_PDT', 'O') ;
        $this->db->where('A.ORD_TYPE', '1') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('C.STATE_CD !=', '7', false) ;
        $this->db->where('B.REAL_OPEN_DT4 >=', $start) ;
        $this->db->where('B.REAL_OPEN_DT4 <', $finish) ;

        if ( $in_center != '' )
        {
            $this->db->where('A.CENTER_CD', $in_center);
        }

        $result = $this->db->get()->row_array() ;

        return ( isset($result) && isset($result['CNT']) ) ? $result['CNT'] : 0 ;
    }
}
?>