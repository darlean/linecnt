<?php
class P_Calling_Plan_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

    function get_calling_plan_list ( $in_cp_comm )
    {
        $this->db->select("CP_CD, CP_NAME") ;
        $this->db->where("CP_COMM", $in_cp_comm) ;
        $this->db->where("CP_USE", "O") ;
        //$this->db->orderby("CP_CD", "DESC") ;
        $this->db->order_by("CP_NAME", "ASC") ;

        $result = $this->db->get("P_CALLING_PLAN")->result_array() ;

        $list = array() ;
        foreach ($result as $key => $value) 
        {
            $list[$value['CP_CD']] = $value['CP_NAME'] ;
        }

        return $list ;
    }
}
?>