<?php
class HB_card_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function insert($in_img_path) 
	{	
		$data = array(			
			'CD_NAME' 					=> $this->input->post('CD_NAME'),
			'CD_COMPANY' 				=> $this->input->post('CD_COMPANY'),
			'TEL_COMPANY' 				=> $this->input->post('TEL_COMPANY'),			
			'SAVE_LIMIT' 				=> $this->input->post('SAVE_LIMIT'),			
			'SAVE_COMMISSION' 			=> $this->input->post('SAVE_COMMISSION'),
			'IS_SAVE_CARD' 				=> $this->input->post('IS_SAVE_CARD'),			
			'SAVE_POINT_TERMS1_OVER' 	=> $this->input->post('SAVE_POINT_TERMS1_OVER'),
			'SAVE_POINT_TERMS1_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS1_UNDER'),
			'SAVE_POINT_TERMS1_POINT' 	=> $this->input->post('SAVE_POINT_TERMS1_POINT'),
			'SAVE_POINT_TERMS2_OVER' 	=> $this->input->post('SAVE_POINT_TERMS2_OVER'),
			'SAVE_POINT_TERMS2_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS2_UNDER'),
			'SAVE_POINT_TERMS2_POINT' 	=> $this->input->post('SAVE_POINT_TERMS2_POINT'),
			'SAVE_POINT_TERMS3_OVER' 	=> $this->input->post('SAVE_POINT_TERMS3_OVER'),
			'SAVE_POINT_TERMS3_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS3_UNDER'),
			'SAVE_POINT_TERMS3_POINT' 	=> $this->input->post('SAVE_POINT_TERMS3_POINT'),
			'ANNUAL_FEE' 				=> $this->input->post('ANNUAL_FEE'),
			'JOIN_POINT' 				=> $this->input->post('JOIN_POINT'),
			'ADD_BENEFIT' 				=> $this->input->post('ADD_BENEFIT'),
			'ETC_MSG' 					=> $this->input->post('ETC_MSG'),
			'IMG_PATH'					=> $in_img_path
		);

		$sql = "INSERT INTO HB_CARD (CD_ID, " ;

		$i = 0 ;
		foreach ($data as $key => $value) 
		{
			if ( $value != '' )
			{
				if ( $i > 0 )
				{
					$sql .= ", " ;
				}

				$sql .= $key ;		
			}	

			$i++ ;
		}

		$sql .= ") VALUES (SEQ_HB_CARD.NEXTVAL, " ;

		$i = 0 ;
		foreach ($data as $key => $value) 
		{
			if ( $value != '' )
			{
				if ( $i > 0 )
				{
					$sql .= ", " ;
				}

				$sql .= "'".$value."'" ;				
			}

			$i++ ;		
		}

		$sql .= ")" ;

		return $this->db->query($sql) ;
	}

	function update($in_cd_id, $in_img_path) 
	{	
		$data = array(			
			'CD_NAME' 					=> $this->input->post('CD_NAME'),
			'CD_COMPANY' 				=> $this->input->post('CD_COMPANY'),
			'TEL_COMPANY' 				=> $this->input->post('TEL_COMPANY'),			
			'SAVE_LIMIT' 				=> $this->input->post('SAVE_LIMIT'),
			'SAVE_COMMISSION' 			=> $this->input->post('SAVE_COMMISSION'),			
			'IS_SAVE_CARD' 				=> $this->input->post('IS_SAVE_CARD'),			
			'SAVE_POINT_TERMS1_OVER' 	=> $this->input->post('SAVE_POINT_TERMS1_OVER'),
			'SAVE_POINT_TERMS1_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS1_UNDER'),
			'SAVE_POINT_TERMS1_POINT' 	=> $this->input->post('SAVE_POINT_TERMS1_POINT'),
			'SAVE_POINT_TERMS2_OVER' 	=> $this->input->post('SAVE_POINT_TERMS2_OVER'),
			'SAVE_POINT_TERMS2_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS2_UNDER'),
			'SAVE_POINT_TERMS2_POINT' 	=> $this->input->post('SAVE_POINT_TERMS2_POINT'),
			'SAVE_POINT_TERMS3_OVER' 	=> $this->input->post('SAVE_POINT_TERMS3_OVER'),
			'SAVE_POINT_TERMS3_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS3_UNDER'),
			'SAVE_POINT_TERMS3_POINT' 	=> $this->input->post('SAVE_POINT_TERMS3_POINT'),
			'ANNUAL_FEE' 				=> $this->input->post('ANNUAL_FEE'),
			'JOIN_POINT' 				=> $this->input->post('JOIN_POINT'),
			'ADD_BENEFIT' 				=> $this->input->post('ADD_BENEFIT'),
			'ETC_MSG' 					=> $this->input->post('ETC_MSG'),
			'IMG_PATH'					=> $in_img_path
		);

		$this->db->where('CD_ID', $in_cd_id) ;
        return $this->db->update('HB_CARD', $data) ;
	}

	function del($in_cd_id)
	{
		$this->db->where('CD_ID', $in_cd_id);
		return $this->db->delete('HB_CARD') ;		
	}

	function list_result($limit, $offset, $vender)
    {    
        $this->db->limit($limit, $offset) ;

        $this->db->from('HB_CARD') ;       
        $this->db->where('TEL_COMPANY', $vender) ;          

        return $this->db->get()->result_array() ;
    }

    function list_count($vender) 
    {   
        $this->db->where('TEL_COMPANY', $vender) ;          

        return $this->db->count_all_results('HB_CARD') ;
    }

    function get_card_info($in_cd_id)
    {
    	$this->db->where('CD_ID', $in_cd_id) ;

    	return $this->db->get('HB_CARD')->row_array() ;
    }

    function get_card_list_by_vender($vender)
    {                  
        $this->db->where('TEL_COMPANY', $vender) ;          

        return $this->db->get('HB_CARD')->result_array() ;
    }
}
?>