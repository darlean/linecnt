<?php
class Boardgroup_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function list_result($sst, $sod, $sfl, $stx, $limit, $offset) {
		//$this->db->start_cache();
		if ($stx) {
			switch ($sfl) {
				case "gr_id" :
				case "gr_admin" :
					$this->db->where('a.'.$sfl, $stx);
				break;
				default :
					$this->db->like('a.'.$sfl, $stx, 'both');
				break;
			}
		}
		$this->db->stop_cache();

		$result['total_cnt'] = $this->db->count_all_results('HB_BOARD_GROUP a');

		$sql =  'SELECT * FROM (select inner_query.*, rownum rnum FROM 
				(SELECT a."gr_id", a."gr_subject", a."gr_admin", count(b."bo_table") as "bo_cnt" FROM "HB_BOARD_GROUP" a LEFT JOIN "HB_BOARD" b ON a."gr_id" = b."gr_id" GROUP BY a."gr_id", a."gr_subject", a."gr_admin" ORDER BY a."gr_id" asc) 
				inner_query WHERE rownum <= 15)';
		
		/*$this->db->select('a.gr_id,a.gr_subject,a.gr_admin, count(b.bo_table) as bo_cnt');
		$this->db->join('HB_BOARD b', 'a.gr_id = b.gr_id', 'left');
		$this->db->order_by($sst, $sod);
		$this->db->group_by('a.gr_id,a.gr_subject,a.gr_admin' );
		$qry = $this->db->get('HB_BOARD_GROUP a', $limit, $offset);*/
		$result['qry'] = $this->db->query( $sql )->result_array();

		$this->db->flush_cache();

		return $result;
	}

	function get_group($gr_id) {
		$this->db->select('gr_id,gr_subject,gr_admin');
		$query = $this->db->get_where('HB_BOARD_GROUP', array('gr_id' => $gr_id));
		return $query->row_array();
	}

	function insert() {
		$sql = array(
			'gr_id' => $this->input->post('gr_id'),
			'gr_subject' => $this->input->post('gr_subject'),
			'gr_admin' => $this->input->post('gr_admin')
		);
		$this->db->insert('HB_BOARD_GROUP', $sql);
	}

	function update() {
		$sql = array(
			'gr_subject' => $this->input->post('gr_subject'),
			'gr_admin' => $this->input->post('gr_admin')
		);
		$this->db->update('HB_BOARD_GROUP', $sql, array('gr_id' => $this->input->post('gr_id')));
	}

	function list_update($gr_id, $gr_subject, $gr_admin) {
		$sql = array(
			'gr_subject' => $gr_subject,
			'gr_admin' => $gr_admin
		);
		$this->db->update('HB_BOARD_GROUP', $sql, array('gr_id' => $gr_id));
	}

	function delete() {
		$this->db->where('gr_id', $this->input->post('gr_id', TRUE));
		$row = $this->db->count_all_results('HB_BOARD');
		if ($row > 0)
			return FALSE;
		else
			return $this->db->delete('HB_BOARD_GROUP', array('gr_id' => $this->input->post('gr_id', TRUE)));
	}
}
?>