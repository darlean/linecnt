<?php
class Member_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
		$this->load->library('encrypt');
	}

	function get_login($in_mb_id)
	{
		$this->db->select("*") ;
		$this->db->from("SM_USER") ;

		$this->db->where( "USERID", $in_mb_id );

		return $this->db->get()->row_array();		
	}

	// 회원 정보
	function get_member($mb_id) 
	{
		if (!$mb_id) return FALSE;

		$fields = "USERID, PASSWD, USERNAME, AGENCY_CD, CENTER_CD";
		$this->db->select($fields, false);
		
		return $this->db->get_where('SM_USER', array('USERID' => $mb_id))->row_array();
	}
}
?>