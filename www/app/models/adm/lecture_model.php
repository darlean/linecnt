<?php
class Lecture_model extends MY_Model
{
    /**
     * 테이블명
     */
    public $_table = 'HB_LECTURE';

    /**
     * 사용되는 테이블의 프라이머리키
     */
    public $primary_key = 'LECTURE_IDX'; // 사용되는 테이블의 프라이머리키

    function __construct()
    {
        parent::__construct();
    }

    function list_result($sst, $sod, $sfl, $stx, $limit, $offset) 
    {
        /*$this->db->start_cache();
        if ($stx) {
            switch ($sfl) {                
                case "por_hp" :
                    $this->db->like($sfl, $stx, 'brfore');
                break;
                default :
                    $this->db->like($sfl, $stx, 'after');
                break;
            }
        }
        $this->db->stop_cache();*/

        $result['total_cnt'] = $this->db->count_all_results($this->_table);

        $this->db->select("LECTURE_IDX, SUBJECT, URL, THUMB_URL, IFRAME_URL, IS_USE, REG_USER, VIEW_COUNT, TO_CHAR(REG_DATE, 'YYYY-MM-DD') as REG_DATE", false);
        $this->db->order_by($sst, $sod);
        $qry = $this->db->get($this->_table, $limit, $offset);
        $result['qry'] = $qry->result_array();

        //$this->db->where('mb_leave_date <>', '');
        //$result['leave_cnt'] = $this->db->count_all_results($this->_table);

        //$this->db->flush_cache();

        return $result;
    }

    public function insert($data)
    {
        if ( ! empty($data)) 
        {
            return $this->db->insert($this->_table, $data);
        } else {
            return false;
        }
    }
}

?>