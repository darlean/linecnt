<?php
class Order_delivery_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function insert($in_sql) 
    {
        $this->db->insert('hb_order_delivery', $in_sql) ;

        return mysql_insert_id() ;
    }

    function update($in_odv_id, $in_sql)
    {
        $this->db->where('odv_id', $in_odv_id) ;
        
        return $this->db->update('hb_order_delivery', $in_sql) ;
    }
}
?>