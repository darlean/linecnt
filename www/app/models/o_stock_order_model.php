<?php
class O_Stock_order_model extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

    function get_stock_count($in_center = "")
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        if ( IS_CENTER )
        {
            $in_center = $this->session->userdata('CENTER_CD') ;
        }

        if ( $in_center != "" )
        {
            $this->db->or_where("S_CENTER_CD =", $in_center);
            $this->db->or_where("T_CENTER_CD =", $in_center);
        }

        return $this->db->count_all_results('O_STOCKORDER') ;
    }

	function get_stock_list($limit, $offset, $in_center = "")
	{
        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        $select = "A.STOCKORDER_NO, A.WORK_REMARK, A.OUT_DATE,
                    (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.S_CENTER_CD) AS S_CENTER_NAME, 
                    (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.T_CENTER_CD) AS T_CENTER_NAME" ;

		$this->db->select($select, false) ;
        $this->db->order_by("A.STOCKORDER_NO", "DESC");

        if ( IS_CENTER )
        {
            $in_center = $this->session->userdata('CENTER_CD') ;
        }

        if ( $in_center != "" )
        {
            $this->db->or_where("A.S_CENTER_CD =", $in_center);
            $this->db->or_where("A.T_CENTER_CD =", $in_center);
        }

        return $this->db->get('O_STOCKORDER A ')->result_array() ;
	}

    function get_stock($in_stockorder_no)
    {
        $select = "A.PDT_CD, A.PDTDETAIL_NO, COUNT(A.PDT_CD) AS CNT,
                    (SELECT PDT_NAME FROM P_PDTMASTER WHERE PDT_CD = A.PDT_CD) AS PDT_NAME,
                    (SELECT OPT_VALUE FROM P_PDTDETAIL WHERE PDT_CD = A.PDT_CD AND PDTDETAIL_NO = A.PDTDETAIL_NO) AS OPT_VALUE" ;

        $this->db->select($select, false) ;
        $this->db->where("A.STOCKORDER_NO", $in_stockorder_no) ;
        $this->db->group_by("A.PDT_CD, A.PDTDETAIL_NO") ;
        $this->db->order_by("A.PDT_CD", "ASC");

        return $this->db->get('O_STOCKORDERBARCODE A ')->result_array() ;
    }

    function _get_search_date_range()
    {
        $sdt = $this->input->get('sdt') ;
        $start_date = $this->input->get('start');   // 시작날짜
        $finish_date = $this->input->get('finish'); // 종료날짜

        if ( $start_date != '' )
        {
            $this->db->where($sdt.' >= ', "'".$start_date."'", false) ;
        }

        if ( $finish_date != '' )
        {
            $Y = substr($finish_date, 0, 4) ;
            $M = substr($finish_date, 4, 2) ;
            $D = substr($finish_date, -2) ;
            $finish = date("Ymd", mktime(0, 0, 0, $M, $D + 1, $Y)) ;

            $this->db->where($sdt.' < ', "'".$finish."'", false) ;
        }
    }
}
?>