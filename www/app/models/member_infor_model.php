<?php
class Member_infor_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function fillable_supporter($in_mb_supporter_no)
	{
		$this->db->select('st_left_no, st_right_no') ;
		$this->db->where('mb_no', $in_mb_supporter_no) ;
		$result = $this->db->get('hb_sub_tree_by_mb_no')->row_array() ;

		if ( !is_null($result) && isset($result['st_left_no']) && $result['st_left_no'] > 0 && $result['st_right_no'] > 0 )
			return false ;

		return true ;
	}

	function insert($in_group, $in_center) 
	{
		$sql = array(
			'mb_name' => $this->input->post('mb_name'),
			'mb_jumin_no' => $this->input->post('mb_jumin_no'),
			'mb_sex' => $this->input->post('mb_sex'),
			'mb_birth' => $this->input->post('mb_birth'),
			'mb_password' => $this->encrypt->encode($this->input->post('mb_password')),
			'mb_tel' => $this->input->post('mb_tel_1').'-'.$this->input->post('mb_tel_2').'-'.$this->input->post('mb_tel_3'),
			'mb_hp' => $this->input->post('mb_hp_1').'-'.$this->input->post('mb_hp_2').'-'.$this->input->post('mb_hp_3'),

			'mb_email' => $this->input->post('mb_email'),
			'mb_zip' => $this->input->post('mb_zip1').'-'.$this->input->post('mb_zip2'),
			'mb_addr1' => $this->input->post('mb_addr1'),
			'mb_addr2' => $this->input->post('mb_addr2'),

			'mb_bank' => $this->input->post('mb_bank'),
			'mb_bank_owner' => $this->input->post('mb_bank_owner'),
			'mb_bank_serial' => $this->input->post('mb_bank_serial'),

			'mb_supporter_name' => $this->input->post('mb_supporter_name'),
			'mb_supporter_id' => $this->input->post('mb_supporter_id'),
			'mb_recommander_name' => $this->input->post('mb_recommander_name'),
			'mb_recommander_id' => $this->input->post('mb_recommander_id'),
			'mb_center' => $this->input->post('mb_center'),

			'mb_today_login' => TIME_YMDHIS,
			'mb_datetime' => TIME_YMDHIS,
			'mb_ip' => $this->input->server('REMOTE_ADDR'),
			'mb_login_ip' => $this->input->server('REMOTE_ADDR'), 
			'mb_level' => 2, 

			'mb_supporter_no' => $this->input->post('mb_supporter_no')
		);

		// 이메일 인증을 사용하지 않는다면 이메일 인증시간을 바로 넣는다
		if (!$this->config->item('cf_use_email_certify'))
			$sql['mb_email_certify'] = TIME_YMDHIS;

		$this->db->insert('hb_member', $sql, false) ;

		$mb_no = mysql_insert_id() ;
		$mb_id = $in_group."-".$in_center."-".str_pad($mb_no, 5, "0", STR_PAD_LEFT) ;

		$this->db->where('mb_no', $mb_no) ;	
		$this->db->update('hb_member', array('mb_id' => $mb_id)) ;

		$this->session->set_userdata('ss_mb_no', $mb_no) ;

		return $mb_id ; 	
	}

	function update() 
	{
		$sql = array(
			'mb_sex' => $this->input->post('mb_sex'),
			'mb_birth' => $this->input->post('mb_birth'),
			'mb_tel' => $this->input->post('mb_tel_1').'-'.$this->input->post('mb_tel_2').'-'.$this->input->post('mb_tel_3'),
			'mb_hp' => $this->input->post('mb_hp_1').'-'.$this->input->post('mb_hp_2').'-'.$this->input->post('mb_hp_3'),

			'mb_email' => $this->input->post('mb_email'),
			'mb_zip' => $this->input->post('mb_zip1').'-'.$this->input->post('mb_zip2'),
			'mb_addr1' => $this->input->post('mb_addr1'),
			'mb_addr2' => $this->input->post('mb_addr2'),

			'mb_bank' => $this->input->post('mb_bank'),
			'mb_bank_owner' => $this->input->post('mb_bank_owner'),
			'mb_bank_serial' => $this->input->post('mb_bank_serial'),
		);

        if ($this->input->post('mb_password'))   $sql['mb_password'] = $this->encrypt->encode($this->input->post('mb_password')) ;
   
		$this->db->where('mb_id', $this->input->post('mb_id'));

		return $this->db->update('hb_member', $sql);
	}

	function delete_member($in_mb_no)
	{
		$this->db->where('mb_no', $in_mb_no);
		return $this->db->update('hb_member', array('mb_is_deleted' => 1, 'mb_leave_date' => date('Ymd', time())));
	}

	function get_member_info_by_name_id($in_member_name, $in_member_id)
	{
		$this->db->select('mb_name, mb_id, mb_no') ;
		$this->db->where(array('mb_name' => $in_member_name, 'mb_id' => $in_member_id)) ;

		return $this->db->get('hb_member')->row_array() ;
	}

	function get_member_info_by_id($in_member_id)
	{
		$this->db->from('hb_member') ;
        $this->db->join('hb_date_of_position_up', 'hb_date_of_position_up.mb_no = hb_member.mb_no', 'left') ;
		$this->db->where('mb_id', $in_member_id) ;

		return $this->db->get()->row_array() ;
	}

	function list_result($sst, $sod, $sfl, $stx, $limit, $offset, $bDelete = false)
    {    
        $this->_get_search_cache($sfl, $stx);

        if ($sst && $sod)
            $this->db->order_by($sst, $sod);

        // 기간 검색
		$this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;
        $this->db->where(array('mb_level <' => 9, 'mb_is_deleted' => $bDelete), false) ;

        if( !IS_MANAGER )
        {
        	$this->db->join('member_nodes_paths', 'hb_member.mb_no = member_nodes_paths.descendant_mb_no', 'left') ;
            $this->db->where('member_nodes_paths.ancestor_mb_no', $this->session->userdata('ss_mb_no')) ;
        }

        return $this->db->get('hb_member')->result_array() ;
    }

    function list_count($sfl, $stx, $bDelete = false) 
    {       
        // 기간 검색
		$this->_get_search_date_range() ;
        $this->_get_search_cache($sfl, $stx);
        $this->db->where(array('mb_level <' => 9, 'mb_is_deleted' => $bDelete), false) ;

        if( !IS_MANAGER )
        {
        	$this->db->join('member_nodes_paths', 'hb_member.mb_no = member_nodes_paths.descendant_mb_no', 'left') ;
            $this->db->where('member_nodes_paths.ancestor_mb_no', $this->session->userdata('ss_mb_no')) ;
        }

        return $this->db->count_all_results('hb_member');
    }

    // 검색 구문을 얻는다.
    function _get_search_cache($search_field, $search_text) 
    {        
        if (!$search_field || !$search_text)
            return FALSE;

        $search_field = $search_field ;

        $where = '' ;

        if (preg_match('/[a-zA-Z]/', $search_text))
            $where .= 'INSTR(LOWER('.$this->db->protect_identifiers($search_field).'), LOWER('.$this->db->escape($search_text).'))';
        else
            $where .= 'INSTR('.$this->db->protect_identifiers($search_field).', '.$this->db->escape($search_text).')';
            
        $this->db->where($where, null, FALSE);
    }

    // 기간 검색
    function _get_search_date_range()
	{
    	$sdt = $this->input->get('sdt') ;
    	$start_date = $this->input->get('start');   // 시작날짜
        $finish_date = $this->input->get('finish'); // 종료날짜

        if ( $sdt == 'mb_leave_date' || $sdt == 'mb_edu_date' )
        {
        	$sdt = "date_format(".$sdt.", '%Y-%m-%d')" ;
        }

        if ( $start_date != '' )
        {
        	$this->db->where($sdt.' >= ', "'".$start_date."'", false) ;
        }

        if ( $finish_date != '' )
        {
        	$this->db->where($sdt.' <= ', "'".$finish_date."'", false) ;
        }
    }

    function get_search_name_list_by_name($in_member_name)
	{
		$this->db->select('mb_id') ;
		$this->db->where(array('mb_name' => $in_member_name)) ;

        if( !IS_MANAGER )
        {
        	$this->db->join('member_nodes_paths', 'hb_member.mb_no = member_nodes_paths.descendant_mb_no', 'left') ;
            $this->db->where('member_nodes_paths.ancestor_mb_no', $this->session->userdata('ss_mb_no')) ;
        }

		return $this->db->get('hb_member')->result_array() ;
	}
}
?>