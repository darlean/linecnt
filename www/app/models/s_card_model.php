<?php
class S_Card_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function get_card_list()
	{
		$this->db->select('CARD_CD, CARD_NAME') ;
		$this->db->where('ISUSE', 'O') ;

		$result = $this->db->get('S_CARD')->result_array() ;

		$list = array() ;
		foreach ($result as $key => $row) 
		{
			$list[$row['CARD_CD']] = $row['CARD_NAME'] ;
		}

		return $list ;
	}
}
?>