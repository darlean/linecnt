<?php
class Calling_plan_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function get_tag()
	{
		$this->db->select('cp_idx, cp_name, cp_price') ;
		$result = $this->db->get('hb_calling_plan')->result_array() ;

		$cp_tag = array() ;

		foreach ($result as $key => $row) 
		{
			$cp_tag[$row['cp_idx']] = $row['cp_name'].' / '.number_format($row['cp_price']).' 원' ;
		}

		return $cp_tag ;
	}
}
?>