<?php
class O_Order_Acard_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function get_order_info($in_ord_no)
    {        
        $select = "A.CENTER_CD, A.RECEPTION_ID, B.PV1, B.PV2, C.PDT_NAME, D.REG_NAME, D.BANK_DATE, D.SAVE_AMT, 
        		   D.IS_TRAFFIC_CARD, D.HOME_ZIPCODE, D.HOME_ADDR1, D.OFFICE_ZIPCODE, D.OFFICE_ADDR1, D.EMAIL,
        		   PKG_CRYPTO_FIELD.get('REG_MOBILE', D.REG_MOBILE, ,'9','".G_CRYPTO."') as REG_MOBILE,
        		   PKG_CRYPTO_FIELD.get('BANK_ACCOUNT', D.BANK_ACCOUNT, ,'9','".G_CRYPTO."') as BANK_ACCOUNT,
                   PKG_CRYPTO_FIELD.get('HOME_ADDR2', D.HOME_ADDR2, ,'9','".G_CRYPTO."') as HOME_ADDR2,
                   PKG_CRYPTO_FIELD.get('OFFICE_ADDR2', D.OFFICE_ADDR2, ,'9','".G_CRYPTO."') as OFFICE_ADDR2,
        		   DECODE(D.DELI_TYPE, '1', '자택', '직장') AS DELI_TYPES,
        		   DECODE(D.BILL_TYPE, '1', '자택', '직장') AS BILL_TYPES,
        		   (SELECT BANK_NAME FROM S_BANK WHERE BANK_CD = D.BANK_CD) as BANK_NAME,
        		   (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.CENTER_CD) as CENTER_NAME,
                   (SELECT PKG_CRYPTO_FIELD.get('USERNAME', USERNAME, ,'9','".G_CRYPTO."') FROM D_MEMBER WHERE USERID = A.RECEPTION_ID) as RECEPTION_NAME,
                   E.*, F.ORD_MSG" ;

        $this->db->select($select, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;
        $this->db->join('O_ORDER_ACARD D', 'A.ORDNO = D.ORDNO') ;                               
        $this->db->join('HB_CARD2 E', 'E.PDT_CD = B.PDT_CD', 'LEFT') ;   
        $this->db->join('O_ORDERPERSON F', 'A.ORDNO = F.ORDNO') ;                            

        $this->db->where('A.ORDNO', $in_ord_no) ;        

        return $this->db->get()->row_array() ;       
    }
}
?>