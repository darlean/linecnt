<?php
class P_Pdtrecommand_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function insert($in_pdt_cd)
	{
		$data = array('PDT_CD' => $in_pdt_cd) ;
		
        return $this->db->insert('P_PDTRECOMMAND', $data) ;
	}

	function delete($in_pdt_cd)
	{
		$this->db->where('PDT_CD', $in_pdt_cd) ;

        return $this->db->delete('P_PDTRECOMMAND') ;
	}

	function recommand_list($limit, $offset)
    {    
        $this->db->limit($limit, $offset) ;

        $this->db->select ( "/*+ index(P_PDTTELPRICE P_PDTTELPRICE_PK)*/ A.PDT_CD, B.PDT_NAME, B.PDT_MODEL, MAX(A.S_DATE) AS S_DATE, C.IMG_PATH", false ) ;
        $this->db->from ( "P_PDTTELPRICE A" ) ;
        $this->db->join( 'P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD', 'left') ;
        $this->db->join( 'P_PDTIMAGE C', 'A.PDT_CD = C.PDT_CD', 'left') ;
        $this->db->join( 'P_PDTRECOMMAND D', 'A.PDT_CD = D.PDT_CD', 'left') ;
        
        $this->db->where('B.ISUSE', 'O') ;
        $this->db->where('D.IS_RECOMMAND', 'O') ;
        
        $this->db->group_by('A.PDT_CD, B.PDT_NAME, B.PDT_MODEL, C.IMG_PATH') ;

        return $this->db->get()->result_array() ;
    }
}
?>