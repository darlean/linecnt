<?php
class Order_Image_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

    function insert_order_image( $in_ORDNO, $in_img_path )
    {
        $sql = "MERGE INTO O_ORDERIMAGE trg  
                USING (SELECT '$in_ORDNO' as ORDNO, '$in_img_path' as IMG_PATH FROM DUAL) src ON (src.ORDNO = trg.ORDNO)
                WHEN NOT MATCHED THEN 
                    INSERT(ORDNO, IMG_PATH) VALUES (src.ORDNO, src.IMG_PATH)
                WHEN MATCHED THEN 
                    UPDATE SET trg.IMG_PATH = src.IMG_PATH";

        return $this->db->query($sql);       
    }
}