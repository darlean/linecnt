<?php
class Pay_week_pt_model extends CI_Model 
{
    function __construct() {
        parent::__construct();
    }

    function get_pay_week_pt_list($in_user_id, $in_pay_date)
    {                        
        $sql = "A.USERID, PKG_CRYPTO_FIELD.get('USERNAME', Z.USERNAME,'9','".G_CRYPTO."') AS USERNAME, A.CV, A.P_LV, A.PAY_RATE, A.AMT_WPT" ;

        $this->db->select($sql, false) ;   
        $this->db->from("PAY_WEEK_PT A") ;
        $this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID', 'left') ;        
        $this->db->where("A.TG_USERID", $in_user_id) ;        
        $this->db->where("A.PAY_DATE", $in_pay_date) ;
        
        return $this->db->get()->result_array() ;
    }
}
?>