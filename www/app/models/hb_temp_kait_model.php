<?php
class HB_temp_kait_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function _insert()
	{			
		$data = array(
		    'USERID'       	 => $this->input->post("USERID"),
	        'LINK_PATH'      => $this->input->post("LINK_PATH"),
	        'KAIT_NO'        => $this->input->post("KAIT_NO"),
	        'KT_REG_CHECK'   => ($this->input->post('KT_REG_CHECK') == 'on') ? 'O' : 'X',
	        'SKT_REG_CHECK'  => ($this->input->post('SKT_REG_CHECK') == 'on') ? 'O' : 'X',
	        'LGU_REG_CHECK'  => ($this->input->post('LGU_REG_CHECK') == 'on') ? 'O' : 'X',	         
	        ) ;

		return $this->db->insert('HB_TEMP_KAIT', $data) ;		
	}

	function _update()
    {    
    	$data = array(		    
	        'LINK_PATH'      => $this->input->post("LINK_PATH"),
	        'KAIT_NO'        => $this->input->post("KAIT_NO"),
	        'KT_REG_CHECK'   => ($this->input->post('KT_REG_CHECK') == 'on') ? 'O' : 'X',
	        'SKT_REG_CHECK'  => ($this->input->post('SKT_REG_CHECK') == 'on') ? 'O' : 'X',
	        'LGU_REG_CHECK'  => ($this->input->post('LGU_REG_CHECK') == 'on') ? 'O' : 'X',	     
	        'ON_UPDATE'		 => 'O',   
	        'ON_REJECT'		 => 'X',  
	        ) ;

    	$this->db->where('USERID', $this->input->post("USERID")) ;

		return $this->db->update('HB_TEMP_KAIT', $data) ;	
    }

    function update()
    {
    	if ( $this->get_count($this->input->post("USERID")) > 0 )
    	{
    		return $this->_update() ;
    	}

    	return $this->_insert() ;
    }

    function reject($in_userid, $in_reject_msg)
    {    
    	$data = array(		    
	        'REJECT_MSG'     => $in_reject_msg,    
	        'ON_UPDATE'		 => 'X',   
	        'ON_REJECT'		 => 'O',  
	        ) ;

    	$this->db->where('USERID', $in_userid) ;

		return $this->db->update('HB_TEMP_KAIT', $data) ;	
    }

    function get_info($in_userid)
    {
    	$this->db->where('USERID', $in_userid) ;

		$result = $this->db->get('HB_TEMP_KAIT')->row_array() ;	

		if ( $result == null )
		{
			$result = array(
		    'USERID'       	 => $in_userid,
	        'LINK_PATH'      => "",
	        'KAIT_NO'        => "",
	        'KT_REG_CHECK'   => "",
	        'SKT_REG_CHECK'  => "",
	        'LGU_REG_CHECK'  => "",	         
	        ) ;
		}

		return $result ;
    }

    private function get_count($in_userid)
    {
    	$this->db->where('USERID', $in_userid) ;

		return $this->db->count_all_results('HB_TEMP_KAIT') ;
    }

    function list_count($tab)
    {    	
    	if ( $tab == 1 )
    	{
    		$this->db->where('ON_REJECT', 'O') ;
    	}
    	else
    	{
    		$this->db->where('ON_UPDATE', 'O') ;
    	}

    	return $this->db->count_all_results('HB_TEMP_KAIT') ;
    }

    function list_result($limit, $offset, $tab)
    {
    	$this->db->limit($limit, $offset) ;
    	
    	$this->db->select("A.USERID, A.KAIT_NO, A.LINK_PATH, A.KT_REG_CHECK, A.SKT_REG_CHECK, A.LGU_REG_CHECK, PKG_CRYPTO_FIELD.get('USERNAME',B.USERNAME,'9','".G_CRYPTO."') as USERNAME", false) ;
    	$this->db->join('D_MEMBER B', 'A.USERID = B.USERID', 'left') ;    	

    	if ( $tab == 1 )
    	{
    		$this->db->where('ON_REJECT', 'O') ;
    	}
    	else
    	{
    		$this->db->where('ON_UPDATE', 'O') ;
    	}

    	return $this->db->get('HB_TEMP_KAIT A')->result_array() ;
    }

    function update_real_kait_info($in_userid)
    {
    	if ( $this->get_count($in_userid) > 0 )
    	{
	    	$kait_info = $this->get_info($in_userid) ;

	    	$data = array(		    	        
		        'KAIT_NO'        => $kait_info['KAIT_NO'],
		        'KT_REG_CHECK'   => $kait_info['KT_REG_CHECK'],
		        'SKT_REG_CHECK'  => $kait_info['SKT_REG_CHECK'],
		        'LGU_REG_CHECK'  => $kait_info['LGU_REG_CHECK'],	     	        
		        ) ;

	    	$this->db->where('USERID', $in_userid) ;

	    	$result = $this->db->update('D_MEMBER', $data) ;

			if ( $result )
			{
				$data2 = array(		    			        
			        'ON_UPDATE'		 => 'X',   
			        ) ;

		    	$this->db->where('USERID', $in_userid) ;
				$this->db->update('HB_TEMP_KAIT', $data2) ;	
			}

			return $result ;
		}

		return null ;
    }

    function is_inserted_kaitno($in_kaitno, $in_userid)
    {
    	$this->db->where('KAIT_NO', $in_kaitno) ;
    	$this->db->where('USERID != ', $in_userid) ;

		return $this->db->count_all_results('D_MEMBER') ;
    }
}
?>