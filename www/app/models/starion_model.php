<?php
class Starion_model extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

	function list_result($limit, $offset)
    {
        if ( $limit > -1 && $offset > -1 )
            $this->db->limit($limit, $offset) ;
        
        $this->db->from ( "P_PDTMASTER A" ) ;        
        $this->db->join( 'P_PDTIMAGE C', 'A.PDT_CD = C.PDT_CD', 'left') ;
        $this->db->join( 'P_PDTSELL D', 'A.PDT_CD = D.PDT_CD', 'left') ;
        $this->db->select ( "/*+ index(P_PDTMASTER P_PDTMASTER_PK)*/ A.SELL_FORCED, A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, C.IMG_PATH, D.AMT", false ) ;
        $this->db->where( 'A.TYPE_CD', '2501' ) ;
        $this->db->where('A.ISUSE', 'O') ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN D.S_DATE AND D.E_DATE") ;
        
        $this->db->group_by('A.SELL_FORCED, A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, C.IMG_PATH, D.AMT') ;
        $this->db->order_by('A.PDT_CD', 'desc') ;

        return $this->db->get()->result_array() ;
    }

    function list_count()
    {
        $this->db->select('COUNT(DISTINCT PDT_CD) AS CNT') ;
        $this->db->where( 'TYPE_CD', '2501' ) ;
        $this->db->where('ISUSE', 'O') ;

        return $this->db->get('P_PDTMASTER')->row()->CNT ;
    }

    function get_policy_info($in_pdt_cd)
    {
        $this->db->select ( "A.SELL_FORCED, A.ISSELL_CASH, A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, B.*, C.IMG_PATH, C.DETAIL_IMG_PATH" ) ;
        $this->db->from ( "P_PDTMASTER A" ) ;
        $this->db->join( 'P_PDTSELL B', 'A.PDT_CD = B.PDT_CD') ;
        $this->db->join( 'P_PDTIMAGE C', 'A.PDT_CD = C.PDT_CD', 'left') ;

        $this->db->where('A.PDT_CD', $in_pdt_cd) ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN B.S_DATE AND B.E_DATE") ;
        $this->db->where('A.ISUSE', 'O') ;

        return $this->db->get()->row_array() ;
    }
}
?>