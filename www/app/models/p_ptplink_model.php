<?php
class P_ptplink_model extends CI_Model 
{
    function __construct() {
        parent::__construct();
    }

    function get_ptp_list($in_pdt_cd)
    {
        $this->db->select('A.PTP_ID, A.PRICE_KIND, A.REG_KIND, B.CP_NAME, C.REASON_NAME, D.LINK_PATH') ;
        $this->db->from('P_PDTTELPRICE A') ;
        $this->db->join('P_CALLING_PLAN B', 'B.CP_CD = A.PRICE_KIND', 'left') ;
        $this->db->join('S_REASON C', 'C.REASON_CD = A.REG_KIND', 'left') ;
        $this->db->join('P_PTPLINK D', 'A.PTP_ID = D.PTP_ID OR ( A.PRICE_KIND = D.PRICE_KIND AND A.REG_KIND = D.REG_KIND AND A.PDT_CD = D.PDT_CD )', 'left') ;
        $this->db->join('P_PDTMASTER E', 'A.PDT_CD = E.PDT_CD', 'left') ;     
        
        $this->db->where('A.PDT_CD', $in_pdt_cd) ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN A.S_DATE AND A.E_DATE") ;
        $this->db->where('E.ISUSE', 'O') ;        

        $this->db->order_by("B.CP_NAME", "ASC") ;
        $this->db->order_by("A.REG_KIND", "ASC") ;         

        return $this->db->get()->result_array() ;
    }

    function update($in_link_path, $in_pdt_cd, $in_price_kind, $reg_kind)
    {  
        $where = array('PDT_CD' => $in_pdt_cd, 'PRICE_KIND' => $in_price_kind, 'REG_KIND' => $reg_kind) ;        
        $this->db->where($where) ;
        $CNT = $this->db->count_all_results('P_PTPLINK') ;

        if ( $CNT > 0 )
        {
            $data = array('LINK_PATH' => $in_link_path) ;            
            $this->db->where($where);
            return $this->db->update('P_PTPLINK', $data) ; 
        }
        else
        {
            $data = $where ;
            $data['LINK_PATH'] = $in_link_path ;
            return $this->db->insert('P_PTPLINK', $data) ; 
        }
    }

    function get_link($in_pdt_cd, $in_price_kind, $reg_kind/*, $in_ptp_id*/)
    {
        $this->db->select('LINK_PATH') ;

        $where = array('PDT_CD' => $in_pdt_cd, 'PRICE_KIND' => $in_price_kind, 'REG_KIND' => $reg_kind) ;        
        $this->db->where($where) ;        
        //$this->db->or_where('PTP_ID', $in_ptp_id) ; 

        $result = $this->db->get('P_PTPLINK')->row() ;

        return ($result) ? $result->LINK_PATH : '' ;        
    }
}
?>