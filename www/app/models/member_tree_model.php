<?php
class member_tree_model extends CI_Model 
{
    function __construct()
    {
    	parent::__construct() ;
    }

    public function get_search_name_list_in_subtree($in_member_name)
    {
        if( IS_MANAGER )
        {
            $mb_id = 'D0000000' ;
        }
        else
        {
            $mb_id = $this->session->userdata('ss_mb_id') ; 
        }
        
        $sql = "SELECT USERID FROM D_MEMBER 
                WHERE USERNAME = PKG_CRYPTO.encrypt3('$in_member_name','".G_CRYPTO."')
                START WITH USERID = '{$mb_id}'" ;   

        /*if($userKind == "P") 
        {
            $sql .= " CONNECT BY PRIOR USERID = P_ID" ;          
        } 
        else 
        {
            $sql .= " CONNECT BY PRIOR USERID = R_ID" ;  
        }*/

        $sql .= " CONNECT BY PRIOR USERID = P_ID
                ORDER SIBLINGS BY P_POS" ;

        return $this->db->query($sql)->result_array() ;
    }

    public function get_member_nodes_subtree( $in_user_id, $in_level = 0, $in_user_kind = 'P' )
    {
       $sql = "SELECT DM.USERID, DM.P_ID, DM.R_ID, DM.REG_DATE, DM.ISCLASS, DM.CENTER_CD, DM.RANK_CD,
               PKG_CRYPTO_FIELD.get('USERNAME',DM.USERNAME,'9','".G_CRYPTO."') as USERNAME,
               (SELECT NVL(SUM(PV2),0) FROM O_ORDERMASTER WHERE ORD_TYPE IN('1', '2') AND ISORDER_OK = 'O' AND USERID = DM.USERID ) as PV
               FROM D_MEMBER DM" ;
               
        if($in_level > 0)
        { 
            $sql .= " WHERE LEVEL <= $in_level" ;
        }

        $sql .= " START WITH USERID = '$in_user_id'" ;   

        if( $in_user_kind == 'P' ) 
        {
            $sql .= " CONNECT BY PRIOR USERID = P_ID" ;          
            $sql .= " ORDER SIBLINGS BY P_POS" ;
        } 
        else 
        {
            $sql .= " CONNECT BY PRIOR USERID = R_ID" ;  
            $sql .= " ORDER SIBLINGS BY R_POS" ;
        }        

        return $this->db->query($sql)->result_array() ;
    }

    function get_member_count_subtree( $in_user_id, $in_level = 0, $in_user_kind = 'P' )
    {
        $sql = "SELECT count(*) AS CNT FROM D_MEMBER" ;

        if($in_level > 0)
        { 
            $sql .= " WHERE LEVEL <= $in_level" ;
        }

        $sql .= " START WITH USERID = '$in_user_id'" ;   

        if( $in_user_kind == 'P' ) 
        {
            $sql .= " CONNECT BY PRIOR USERID = P_ID" ;          
            $sql .= " ORDER SIBLINGS BY P_POS" ;
        } 
        else 
        {
            $sql .= " CONNECT BY PRIOR USERID = R_ID" ; 
            $sql .= " ORDER SIBLINGS BY R_POS" ; 
        }        
        
        $cnt_mb =  $this->db->query($sql)->row()->CNT ;        

        return ( $cnt_mb > 0 ) ? $cnt_mb - 1 : $cnt_mb ;
    }

    function is_member_in_my_sub_tree($in_user_id, $in_my_id)
    {
        $sql = "SELECT count(*) as CNT FROM D_MEMBER WHERE USERID = '$in_user_id'" ;                
        $sql .= " START WITH USERID = '$in_my_id' CONNECT BY PRIOR USERID = P_ID
                ORDER SIBLINGS BY P_POS" ;

        return $this->db->query($sql)->row()->CNT ;
    }

    function get_rank_date_list($in_user_id)
    {
        $this->db->select('RANK_CD, REG_DATE') ;
        $this->db->from('D_RANK') ;
        $this->db->where('USERID', $in_user_id) ;
        $this->db->where('REG_DATE <= ', "TO_CHAR(SYSDATE, 'YYYYMMDD')", false) ;
        $this->db->order_by('REG_DATE', 'asc') ;

        $result =  $this->db->get()->result_array() ;

        $arr_list = array() ;
        foreach ($result as $key => $value) 
        {
            $arr_list[$value['RANK_CD']] = $value['REG_DATE'] ;
        }

        return $arr_list ;
    }
}
?>