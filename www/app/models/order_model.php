<?php
class Order_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function execute_procedure($p_name, $params, $bCommit = true)
    {
        $pramList = "";

        foreach ($params as $key => $value) 
        {
            if($pramList!="") $pramList .= ", " ;
            $pramList .= ":" . $key ;
        }

        $query = "begin ".$p_name."(" . $pramList . "); end;" ;

        $stmt = oci_parse($this->db->conn_id, $query) ;
        
        foreach ($params as $key => $value) 
        {
            if($key=="STATUS" || $key=="MESSAGE" || substr($key,0,3)=="NEW")
                oci_bind_by_name($stmt, ":" . $key, $params->$key, 100) ;
            else
                oci_bind_by_name($stmt, ":" . $key, $params->$key) ;
        }

        if(!ociexecute($stmt, OCI_DEFAULT))
        {
            oci_rollback($this->db->conn_id) ;
            die ;
        }

        if ( $params->STATUS == '1' && $bCommit )
        {
            oci_commit($this->db->conn_id) ;
        }
        
        return $params ;
    }

    function create_order($in_pdt_master_info, $in_stockprice_info, $in_sum_price_info, $in_type = '')
    {
        $RECEPTION_ID = ($this->input->post('reception_id')) ? $this->input->post('reception_id') : 'D0000000' ;

        if ( $in_type == 'wired' )
        {
            $RECEPTION_ID = ($this->input->post('seller_id')) ? $this->input->post('seller_id') : 'D0000000' ;            
        }

        // 2016년 9월 21일부터 택배 수령시 주문 센터 본사로~ (KT가 아닌경우)
        /*if ( $this->input->post('odv_type') != '0701' && $in_pdt_master_info->TEL_COMPANY != '3201' )
        {
            $CENTER_CD = '0000' ;
        }
        else*/
        {
            $CENTER_CD = ($this->input->post('ord_center')) ? $this->input->post('ord_center') : '0000' ;
        }

        $params->USERID     = ( $this->input->post('hb_recipient_id') != '' ) ? $this->input->post('hb_recipient_id') : $this->session->userdata('ss_mb_id') ; 
        $params->RECEPTION_ID = $RECEPTION_ID ;        
        $params->ORD_KIND   = '2601'; // 주문구분(초도, 재구매, 초도+재구매 등), S_REAND.REASON_KIND:26
        $params->AGENCY_CD  = $in_pdt_master_info->TEL_COMPANY ;
        $params->CENTER_CD        = $CENTER_CD ;

        if ( $this->input->post('odv_type') == '0701' )  // 직접수령
        {
            $params->START_CENTER     = ($this->input->post('sel_center') ) ? $this->input->post('sel_center')  : '9990' ;
        }
        else // 택배
        {  
            $params->START_CENTER     = '9990' ;
        }        
                
        $params->PRICE      = (int)$in_stockprice_info->PRICE ;
        $params->VAT        = (int)$in_stockprice_info->VAT ;
        $params->AMT        = (int)$in_stockprice_info->AMT ;
        $params->PV1        = (int)$in_sum_price_info->POINT1 ;
        $params->PV2        = (int)$in_sum_price_info->POINT2;
        $params->PV3        = (int)$in_sum_price_info->POINT3 ;
        $params->PV4        = (int)$in_sum_price_info->POINT4 ;        
        $params->CRYPTO     = G_CRYPTO ;
        $params->NEW_ORDER  = "" ;
        $params->STATUS     = "" ;
        $params->MESSAGE    = "" ;     
    
        return $this->execute_procedure("PKG_WEB.CREATE_ORDER", $params, false) ;
    }

    function create_detail($in_ord_no, $in_pdt_master_info, $in_stockprice_info, $in_sum_price_info)
    {
        $PDTDETAIL_NO = ($this->input->post('pt_color')) ? $this->input->post('pt_color') : $in_pdt_master_info->PDTDETAIL_NO ;

        $params->ORDNO          = $in_ord_no ;
        $params->PDT_CD         = $this->input->post('pdt_cd') ;
        $params->PDTDETAIL_NO   = $PDTDETAIL_NO ;
        $params->QTY            = 1 ;
        $params->PRICE          = (int)$in_stockprice_info->PRICE ;
        $params->VAT            = (int)$in_stockprice_info->VAT ;
        $params->AMT            = (int)$in_stockprice_info->AMT ;
        $params->PV1            = (int)$in_sum_price_info->POINT1 ;
        $params->PV2            = (int)$in_sum_price_info->POINT2;
        $params->PV3            = (int)$in_sum_price_info->POINT3 ;
        $params->PV4            = (int)$in_sum_price_info->POINT4 ; 
        $params->COMPANY_CD     = $in_pdt_master_info->COMPANY_CD ;
        $params->RECEIPT_KIND   = $this->input->post('odv_type') ;
        $params->BARCODE        = '' ;
        $params->IS_TELPRODUCT  = $in_pdt_master_info->IS_TELPRODUCT ; // 미출고상품유무(PDTMASTER.IS_TELPRODUCT)
        $params->WORK_USER      = ( $this->input->post('hb_recipient_id') != '' ) ? $this->input->post('hb_recipient_id') : $this->session->userdata('ss_mb_id') ;
        $params->NEW_ORDPDT     = "" ;
        $params->STATUS         = "" ;
        $params->MESSAGE        = "" ;      
    
        return $this->execute_procedure("PKG_ORDER_TEL.CREATE_DETAIL", $params, false) ;
    }

    function create_usim_detail($in_ord_no, $in_usim_info, $in_pdt_master_info)
    {    
        $params->ORDNO          = $in_ord_no ;
        $params->PDT_CD         = $in_usim_info['PDT_CD'] ;
        $params->PDTDETAIL_NO   = $in_usim_info['PDTDETAIL_NO'] ;
        $params->QTY            = 1 ;
        $params->PRICE          = (int)$in_usim_info['PRICE'] ;
        $params->VAT            = (int)$in_usim_info['VAT'] ;
        $params->AMT            = (int)$in_usim_info['AMT'] ;
        $params->PV1            = (int)$in_usim_info['POINT1'] ;
        $params->PV2            = (int)$in_usim_info['POINT2'] ;
        $params->PV3            = (int)$in_usim_info['POINT3'] ;
        $params->PV4            = (int)$in_usim_info['POINT4'] ; 
        $params->COMPANY_CD     = $in_pdt_master_info->COMPANY_CD ;
        $params->RECEIPT_KIND   = $this->input->post('odv_type') ;
        $params->BARCODE        = '' ;
        $params->IS_TELPRODUCT  = $in_pdt_master_info->IS_TELPRODUCT ; // 미출고상품유무(PDTMASTER.IS_TELPRODUCT)
        $params->WORK_USER      = ( $this->input->post('hb_recipient_id') != '' ) ? $this->input->post('hb_recipient_id') : $this->session->userdata('ss_mb_id') ;
        $params->NEW_ORDPDT     = "" ;
        $params->STATUS         = "" ;
        $params->MESSAGE        = "" ;      
    
        return $this->execute_procedure("PKG_ORDER_TEL.CREATE_DETAIL", $params, false) ;
    }

    function create_delivery($in_ord_no, $in_type = 'mobile')
    {
        $params->ORDNO          = $in_ord_no ;

        if ( $in_type == 'mobile' )
        {
            $params->ORD_TEL        = $this->input->post('ORD_TEL') ;
            $params->ORD_MOBILE     = $this->input->post('ORD_MOBILE') ;
            $params->ORD_EMAIL      = $this->input->post('ORD_EMAIL') ;
            $params->RECV_NAME      = $this->input->post('odv_recipient') ;
            $params->RECV_TEL       = $this->input->post('odv_recipient_tel') ;
            $params->RECV_MOBILE    = $this->input->post('odv_recipient_hp') ;  
            $params->RECV_ZIPCODE   = $this->input->post('odv_zonecode') ;  
            $params->RECV_ADDR1     = $this->input->post('odv_recipient_addr1') ;
            $params->RECV_ADDR2     = $this->input->post('odv_recipient_addr2') ;
        }
        else
        {
            $params->ORD_TEL        = $this->input->post('reg_tel') ;
            $params->ORD_MOBILE     = $this->input->post('reg_mobile') ;
            $params->ORD_EMAIL      = $this->input->post('reg_email') ;
            $params->RECV_NAME      = $this->input->post('reg_name') ;
            $params->RECV_TEL       = $this->input->post('reg_tel') ;
            $params->RECV_MOBILE    = $this->input->post('reg_mobile') ;  
            $params->RECV_ZIPCODE   = $this->input->post('home_zonecode') ;            
            $params->RECV_ADDR1     = $this->input->post('home_addr1') ;
            $params->RECV_ADDR2     = $this->input->post('home_addr2') ;
        }

        $params->ORD_MSG        = $this->input->post('ord_msg') ;
        $params->CRYPTO         = G_CRYPTO ;       
        $params->STATUS         = "" ;
        $params->MESSAGE        = "" ;      
    
        return $this->execute_procedure("PKG_ORDER_TEL.CREATE_DELIVERY", $params, false) ;
    }

    function create_tel($in_ord_no, $in_pdt_master_info, $in_pdt_telprice_info, $in_stockprice_info)
    {
        $legal_check = ($this->input->post('LEGAL_NAME') != '') ? '0' : '1' ;

        $params->ORDNO          = $in_ord_no ;
        $params->TEL_COMPANY    = $in_pdt_master_info->TEL_COMPANY ;
        $params->BRAND_CD       = '' ;
        $params->CHANNEL_TYPE   = '' ;
        $params->SAVE_KIND      = '' ;
        $params->PDT_PRICE      = 0 ;
        $params->PRICE_MONTH    = 0 ;
        $params->PRICE_KIND     = $this->input->post('select_calling_plan') ;
        $params->REG_KIND       = $this->input->post('select_join_type') ;
        $params->STOCK_PRICE    = (int)$in_stockprice_info->AMT ;
        $params->PRICE_DISCOUNT = $in_pdt_telprice_info->PRICE_DISCOUNT ;
        $params->PRICE_DISCOUNT2 = $in_pdt_telprice_info->PRICE_DISCOUNT2 ;
        $params->LEGAL_NAME     = $this->input->post('legal_name') ;
        $params->LEGAL_BIRTH    = '' ;
        $params->LEGAL_RELATION = $this->input->post('legal_relation') ;
        $params->LEGAL_TEL      = $this->input->post('legal_tel') ;
        $params->LEGAL_SIGN     = '' ;
        $params->LEGAL_CHECK    = $legal_check ;
        $params->PAY_KIND       = $this->input->post('select_pay_kind') ;
        $params->REG_NAME       = $this->input->post('reg_name') ;
        $params->REG_EMAIL      = $this->input->post('reg_email') ;
        $params->REG_BIRTH      = $this->input->post('reg_birth') ;
        $params->OLD_TEL_COMP   = $this->input->post('old_tel_comp') ; ;
        $params->OLD_TEL_NAME   = $this->input->post('old_tel_name') ; ;
        $params->OLD_TEL        = $this->input->post('old_tel') ;
        $params->BILL_KIND      = $this->input->post('bill_kind') ;
        $params->BILL_EMAIL     = $this->input->post('reg_email') ;
        $params->WORK_USER      = ( $this->input->post('hb_recipient_id') != '' ) ? $this->input->post('hb_recipient_id') : $this->session->userdata('ss_mb_id') ;
        $params->STATUS         = "" ;
        $params->MESSAGE        = "" ;      

        //var_dump($params) ;

        return $this->execute_procedure("PKG_ORDER_TEL.CREATE_TEL", $params, true) ;
    }

    // 주문 제휴카드 신청
    function save_acard($in_ord_no)
    {                
        $add_traffic_card = ($this->input->post('add_traffic_card') == '2') ? 'O' : 'X' ;

        $params->ORDNO                  = $in_ord_no ;
        $params->REG_NAME               = $this->input->post('reg_name') ;
        $params->REG_MOBILE             = $this->input->post('reg_mobile') ;
        $params->BANK_CD                = $this->input->post('pay_bank_name') ;
        $params->BANK_ACCOUNT           = $this->input->post('pay_account') ;
        $params->BANK_DATE              = $this->input->post('pay_date') ;
        $params->IS_TRAFFIC_CARD        = $add_traffic_card ;
        $params->SAVE_AMT               = (int)$this->input->post('save_price') ;
        $params->HOME_ZIPCODE           = $this->input->post('home_zonecode') ;
        $params->HOME_ADDR1             = $this->input->post('home_addr1') ;
        $params->HOME_ADDR2             = $this->input->post('home_addr2') ;
        $params->OFFICE_ZIPCODE         = $this->input->post('job_zip1').'-'.$this->input->post('job_zip2') ;
        $params->OFFICE_ADDR1           = $this->input->post('job_addr1') ;
        $params->OFFICE_ADDR2           = $this->input->post('job_addr2') ;        
        $params->DELI_TYPE              = ($this->input->post('sel_delivery_addr')) ? $this->input->post('sel_delivery_addr') : '1' ;        
        $params->BILL_TYPE              = ($this->input->post('sel_delivery_bill')) ? $this->input->post('sel_delivery_bill') : '1' ;
        $params->EMAIL                  = $this->input->post('reg_email') ;       
        $params->REAL_OPEN_DT4          = "" ;                     
        $params->REAL_OPEN_AGENCY       = "" ;   
        $params->WORK_USER              = $this->session->userdata('ss_mb_id') ; 
        $params->CRYPTO                 = G_CRYPTO ;       
        $params->NEW_CLASSIFY           = 1 ;
        $params->STATUS                 = "" ;
        $params->MESSAGE                = "" ;                      

        return $this->execute_procedure("PKG_ORDER_TEL.SAVE_ACARD", $params, true) ;
    }

    // 주문 유선상품 신청
    function save_ctel($in_ord_no, $in_pdt_master_info, $in_pdt_telprice_info)
    {        
        $params->ORDNO                  = $in_ord_no ;
        $params->TEL_COMPANY            = $in_pdt_master_info->TEL_COMPANY ;
        $params->PRICE_KIND             = $in_pdt_telprice_info->PRICE_KIND ;
        $params->REG_KIND               = $in_pdt_telprice_info->REG_KIND ;
        $params->REG_NAME               = $this->input->post('reg_name') ;
        $params->REG_BIRTHDAY           = $this->input->post('reg_birth') ;
        $params->REG_MOBILE             = $this->input->post('reg_mobile') ;
        $params->REG_TEL                = $this->input->post('reg_tel') ;
        $params->INS_ZIPCODE            = $this->input->post('home_zonecode') ;
        $params->INS_ADDR1              = $this->input->post('home_addr1') ;
        $params->INS_ADDR2              = $this->input->post('home_addr2') ;
        $params->INS_DATE               = $this->input->post('reg_date') ;
        $params->AUTO_TRANS_TYPE        = ($this->input->post('sel_pay_type')) ? $this->input->post('sel_pay_type') : '1' ;
        $params->DEPOSITOR              = $this->input->post('pay_owner') ;
        $params->DEP_BIRTHDAY           = $this->input->post('pay_birth') ;

        // 통장 자동 이체
        if ( $params->AUTO_TRANS_TYPE == '1' )
        {
            $params->BANK_CD                = $this->input->post('pay_bank') ;
            $params->BANK_ACCOUNT           = $this->input->post('pay_account') ;
            $params->CARD_CD                = "" ;
            $params->CARD_ACCOUNT           = "" ;
            $params->CARD_VALID_DATE        = "" ;
        }
        else // 카드 
        {
            $params->BANK_CD                = "" ;
            $params->BANK_ACCOUNT           = "" ;
            $params->CARD_CD                = $this->input->post('pay_bank') ;
            $params->CARD_ACCOUNT           = $this->input->post('pay_account') ;
            $params->CARD_VALID_DATE        = $this->input->post('pay_limit_month').$this->input->post('pay_limit_year') ;
        }

        $params->EMAIL                  = $this->input->post('reg_email') ;                     
        $params->REAL_OPEN_DT4          = "" ;                     
        $params->REAL_OPEN_AGENCY       = "" ;                     
        $params->WORK_USER              = $this->session->userdata('ss_mb_id') ; 
        $params->CRYPTO                 = G_CRYPTO ;       
        $params->NEW_CLASSIFY           = 1 ;
        $params->STATUS                 = "" ;
        $params->MESSAGE                = "" ;                      

        return $this->execute_procedure("PKG_ORDER_TEL.SAVE_CTEL", $params, true) ;
    }

    function update_msg($in_ord_no, $in_sql)
    {
        $this->db->where('ORDNO', $in_ord_no) ;
        return $this->db->update('O_ORDERPERSON', $in_sql) ;
    }

    function list_result($sst, $sod, $sfl, $stx, $limit, $offset, $vender, $state, $rkind, $in_type, $in_type_cd)
    {    
        $this->_get_search_cache($sfl, $stx);

        if ($sst && $sod)
        {
            $sst = ($sst == 'ORDNO') ? 'A.ORDNO' : $sst ;

            $this->db->order_by($sst, $sod);
        }

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        $select = "A.ORDNO, A.ORD_DATE, B.PDT_CD, C.PDT_NAME, E.OPT_VALUE, Z.USERID,
                PKG_CRYPTO_FIELD.get('ORD_NAME', A.ORD_NAME, ,'9','".G_CRYPTO."') as ORD_NAME,                                
                J.COMPANY_NAME, DELI_NO, DELI_DATE, I.STATE_CD, J.DELIVERY_URL, F.ORD_MSG, I.ORD_MANAGER_MSG, I.REQUEST_OPEN,
                PKG_CRYPTO_FIELD.get('MOBILE', Z.MOBILE, ,'9','".G_CRYPTO."') as MOBILE, I.UPPER_REG_NO,
                (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.CENTER_CD) as ORDER_CENTER" ;

        $group_by = "A.ORDNO, A.ORD_DATE, A.ORD_NAME, B.PDT_CD, C.PDT_NAME, E.OPT_VALUE, 
                    J.COMPANY_NAME, DELI_NO, DELI_DATE, I.STATE_CD, A.CENTER_CD,
                    J.DELIVERY_URL, F.ORD_MSG, I.ORD_MANAGER_MSG, I.REQUEST_OPEN, Z.USERID, Z.MOBILE, I.UPPER_REG_NO" ;
        
        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;
        $this->db->join('O_ORDERPERSON F', 'A.ORDNO = F.ORDNO') ;

        if ( $in_type_cd == 'opened' )
        {
            $this->db->join('HB_USE_UCUBE_CENTER M', "(( M.HELP_CENTER_CDS LIKE ('%'||A.CENTER_CD||'%') ) OR M.CENTER_CD = A.CENTER_CD) AND M.USE_UCUBE = 'O'", 'left', false) ;

            $this->db->where('I.REQUEST_OPEN', 'O') ;
            $this->db->where('(I.STATE_CD = 2 or I.STATE_CD = 4)', NULL, false) ;
            $this->db->where("(M.USE_UCUBE IS NULL OR M.USE_UCUBE = 'X')", NULL, false) ;
        }
        else if ( $in_type_cd == 'none' )
        {            
            $this->db->where('A.GT_CD is', ' null', false) ;
            $this->db->where('B.PV1 > ', '0') ;
            $this->db->where('I.STATE_CD !=', '3') ;
            $this->db->where('I.STATE_CD !=', '7') ;
        }
        else if ( $in_type_cd == 'reject' )
        {
            $this->db->where('I.STATE_CD =', '3') ;
        }
        else if ( $in_type_cd == 'center' )
        {
            if ( IS_CENTER )
            {
                $this->db->join('HB_USE_UCUBE_CENTER M', "((( M.HELP_CENTER_CDS LIKE ('%'||A.CENTER_CD||'%') ) AND M.CENTER_CD = ".$this->session->userdata('CENTER_CD').") OR ( M.CENTER_CD = A.CENTER_CD) ) AND M.USE_UCUBE = 'O'", '', false);
            }
            else
            {
                $this->db->join('HB_USE_UCUBE_CENTER M', "(( M.HELP_CENTER_CDS LIKE ('%'||A.CENTER_CD||'%') ) OR M.CENTER_CD = A.CENTER_CD) AND M.USE_UCUBE = 'O'", '', false);
            }

            $this->db->where('I.REQUEST_OPEN', 'O') ;
            $this->db->where('(I.STATE_CD = 2 or I.STATE_CD = 4)', NULL, false) ;
        }

        $tmp_type_cd = $in_type_cd ;

        if ( $in_type_cd == '2502' || $in_type_cd == 'opened' || $in_type_cd == 'none' || $in_type_cd == 'reject' || $in_type_cd == 'center' )       // mobile
        {        
            $in_type_cd = '2502' ;

            $this->db->join('O_ORDERTEL D', 'A.ORDNO = D.ORDNO') ;            
            $this->db->join('S_REASON G', 'G.REASON_CD = B.RECEIPT_KIND', 'left') ;
            $this->db->join('S_REASON K', 'K.REASON_CD = D.REG_KIND', 'left') ;

            $select .= " , D.TEL_COMPANY, D.PRICE_KIND, D.REG_KIND AS REGKIND, D.REG_NAME, D.OLD_TEL, F.RECV_ZIPCODE, F.RECV_ADDR1, A.RECEPTION_ID,
                        PKG_CRYPTO_FIELD.get('RECV_MOBILE', F.RECV_MOBILE, ,'9','".G_CRYPTO."') as RECV_MOBILE, 
                        PKG_CRYPTO_FIELD.get('RECV_ADDR2', F.RECV_ADDR2, ,'9','".G_CRYPTO."') as RECV_ADDR2, 
                        TO_CHAR(TO_DATE(D.REAL_OPEN_DT4, 'YYYYMMDDHH24MISS'), 'YYYY-MM-DD HH:MM') as REAL_OPEN_DT4, D.REAL_OPEN_AGENCY,
                        (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = DECODE(A.START_CENTER, '', F.RECV_ADDR1, A.START_CENTER)) as CENTER_NAME,                        
                        G.REASON_NAME AS RECEIPT_KIND, DECODE(A.ORD_TYPE, '1', NVL(A.GT_CD, '재발급요청필요'), GT_CD) AS GT_CDS,
                        (SELECT PKG_CRYPTO_FIELD.get('USERNAME', USERNAME, ,'9','".G_CRYPTO."') FROM D_MEMBER WHERE USERID = A.RECEPTION_ID) as RECEPTION_NAME,
                        K.REASON_NAME AS REG_KIND, I.BENEFIT_SERVICE, I.ADD_SERVICE" ;

            $group_by .= " , A.ORD_TYPE, D.TEL_COMPANY, D.PRICE_KIND, D.REG_KIND, D.REG_NAME, D.OLD_TEL, F.RECV_MOBILE, D.REAL_OPEN_DT4, D.REAL_OPEN_AGENCY, F.RECV_ZIPCODE, F.RECV_ADDR1, F.RECV_ADDR2, 
                            G.REASON_NAME, A.GT_CD, A.START_CENTER, A.RECEPTION_ID, A.CENTER_CD, K.REASON_NAME, I.BENEFIT_SERVICE, I.ADD_SERVICE" ;

            $this->db->where('C.IS_TELPRICE', 'O') ;  
        }
        else if ( $in_type_cd == '2504' )       // wired
        {
            $this->db->join('O_ORDER_CTEL K', 'A.ORDNO = K.ORDNO') ;
            $this->db->join('P_CALLING_PLAN L', 'L.CP_CD = K.PRICE_KIND', 'left') ;

            $select .= " , K.REG_NAME, PKG_CRYPTO_FIELD.get('REG_MOBILE', K.REG_MOBILE, ,'9','".G_CRYPTO."') as OLD_TEL, L.CP_NAME AS PRICE_KIND" ;

            $group_by .= " , K.REG_NAME, K.REG_MOBILE, L.CP_NAME" ;
        }
        else if ( $in_type_cd == '2506' )       // card
        {
            $this->db->join('O_ORDER_ACARD K', 'A.ORDNO = K.ORDNO') ;

            $select .= " , K.REG_NAME, PKG_CRYPTO_FIELD.get('REG_MOBILE', K.REG_MOBILE, ,'9','".G_CRYPTO."') as OLD_TEL, 
                        DECODE(K.DELI_TYPE, '1', '자택', '직장') AS DELI_TYPES" ;

            $group_by .= " , K.REG_NAME, K.DELI_TYPE, K.REG_MOBILE" ;            
        }
        else if ( $in_type_cd == '2501')
        {
            $select .= ", DECODE(A.ORD_TYPE, '1', NVL(A.GT_CD, '재발급요청필요'), GT_CD) AS GT_CDS" ;
            $group_by .= ", A.ORD_TYPE, A.GT_CD" ;
        }

        $this->db->select($select, false) ;

        $this->db->join('P_PDTDETAIL E', 'E.PDTDETAIL_NO = B.PDTDETAIL_NO') ;            
        $this->db->join('O_STOCKPRODUCT H', 'H.ORDNO = A.ORDNO', 'left') ;
        $this->db->join('O_ORDERSTATE I', 'I.ORDNO = A.ORDNO', 'left') ;
        $this->db->join('P_COMPANY J', 'J.COMPANY_CD = H.DELI_CD', 'left') ;

        $this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID') ;  

        $this->db->group_by($group_by) ;        

        if ( $vender != '' )
        {
            $this->db->where('C.TEL_COMPANY', $vender) ;
        }

        if ( $state != '' )
        {
            $this->db->where('I.STATE_CD', $state) ;
        }

        if ( $rkind != '' )
        {
            $this->db->where('B.RECEIPT_KIND', $rkind) ;
        }        

        $USERID = ( $this->input->post('USERID') != '' ) ? $this->input->post('USERID') : $this->session->userdata('ss_mb_id') ;       

        if ( IS_AGENCY )
        {
            $this->db->where('A.AGENCY_CD', $this->session->userdata('AGENCY_CD')) ;    
        }
        else if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            if ( $tmp_type_cd != 'center' )
            {
                $this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD'));
            }
        }
        else if ( !IS_MANAGER && !IS_CARD_AGENCY )
        {
            if ( $in_type == "sub" )
            {   
                $this->db->where('A.USERID <>', $USERID) ; 

                //$this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID') ;            
                $subquery = "SELECT USERID FROM D_MEMBER START WITH USERID = '".$this->session->userdata('ss_mb_id')."' CONNECT BY P_ID = PRIOR USERID" ;
                $this->db->where_in('A.USERID', $subquery, FALSE) ; 
            }
            else if ( $in_type == "reception" )            
            {
                $this->db->where('A.RECEPTION_ID', $USERID) ;
                $this->db->where('ORD_DATE >= ', "20160401") ;
            }
            else
            {
                $this->db->where('A.USERID', $USERID) ;
            }                
        }

        $this->db->where( 'C.TYPE_CD', $in_type_cd ) ;

        return $this->db->get()->result_array() ;
    }

    function list_count($sfl, $stx, $vender, $state, $rkind, $in_type, $in_type_cd, $start_date='', $finish_date = '')
    {   
        $this->_get_search_cache($sfl, $stx);

        // 기간 검색
        $this->_get_search_date_range($start_date, $finish_date) ;

        if ( $in_type == "sub_main" )
        {
            $this->db->where('ORD_DATE >= ', date('Ymd')) ;
            $this->db->where('ORD_DATE <= ', date('Ymd')) ;
        }                    

        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;        
        $this->db->join('P_PDTDETAIL E', 'E.PDTDETAIL_NO = B.PDTDETAIL_NO') ;        
        $this->db->join('O_ORDERSTATE G', 'G.ORDNO = A.ORDNO', 'left') ;                

        if ( $in_type_cd == 'opened' )
        {
            $this->db->join('HB_USE_UCUBE_CENTER L', "(( L.HELP_CENTER_CDS LIKE ('%'||A.CENTER_CD||'%') ) OR L.CENTER_CD = A.CENTER_CD) AND L.USE_UCUBE = 'O'", 'left', false) ;

            $this->db->where('G.REQUEST_OPEN', 'O') ;
            $this->db->where('(G.STATE_CD = 2 or G.STATE_CD = 4)', NULL, false) ;
            $this->db->where("(L.USE_UCUBE IS NULL OR L.USE_UCUBE = 'X')", NULL, false) ;
        }
        else if ( $in_type_cd == 'none' )
        {            
            //$this->db->where('A.GT_CD is', ' null', false) ;
            $this->db->where('B.PV1 > ', '0') ;
            $this->db->where('G.STATE_CD !=', '3') ;
            $this->db->where('G.STATE_CD !=', '7') ;
        }
        else if ( $in_type_cd == 'reject' )
        {
            $this->db->where('G.STATE_CD =', '3') ;
        }
        else if ( $in_type_cd == 'center' )
        {
            if ( IS_CENTER )
            {
                $this->db->join('HB_USE_UCUBE_CENTER L', "((( L.HELP_CENTER_CDS LIKE ('%'||A.CENTER_CD||'%') ) AND L.CENTER_CD = ".$this->session->userdata('CENTER_CD').") OR ( L.CENTER_CD = A.CENTER_CD) ) AND L.USE_UCUBE = 'O'", '', false);
            }
            else
            {
                $this->db->join('HB_USE_UCUBE_CENTER L', "(( L.HELP_CENTER_CDS LIKE ('%'||A.CENTER_CD||'%') ) OR L.CENTER_CD = A.CENTER_CD) AND L.USE_UCUBE = 'O'", '', false);
            }

            $this->db->where('G.REQUEST_OPEN', 'O') ;
            $this->db->where('(G.STATE_CD = 2 or G.STATE_CD = 4)', NULL, false) ;
        }

        $tmp_type_cd = $in_type_cd ;

        if ( $in_type_cd == '2502' || $in_type_cd == 'opened' || $in_type_cd == 'none' || $in_type_cd == 'reject' || $in_type_cd == 'center' )       // mobile
        {
            $in_type_cd = '2502' ;

            $this->db->join('O_ORDERTEL D', 'A.ORDNO = D.ORDNO') ;
            $this->db->join('O_ORDERPERSON F', 'A.ORDNO = F.ORDNO') ;                        

            $this->db->where('C.IS_TELPRICE', 'O') ;  
        }
        else if ( $in_type_cd == '2504' )       // wired
        {
            $this->db->join('O_ORDER_CTEL K', 'A.ORDNO = K.ORDNO') ;
        }
        else if ( $in_type_cd == '2506' )       // card
        {
            $this->db->join('O_ORDER_ACARD K', 'A.ORDNO = K.ORDNO') ;
        }

        if ( $vender != '' )
        {
            $this->db->where('C.TEL_COMPANY', $vender) ;
        }

        if ( $state != '' )
        {
            $this->db->where('G.STATE_CD', $state) ;
        }

        if ( $rkind != '' )
        {
            $this->db->where('B.RECEIPT_KIND', $rkind) ;
        }        

        $USERID = ( $this->input->post('USERID') != '' ) ? $this->input->post('USERID') : $this->session->userdata('ss_mb_id') ;
        
        if ( IS_AGENCY )
        {
            $this->db->where('A.AGENCY_CD', $this->session->userdata('AGENCY_CD')) ;    
        }
        else if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            if ( $tmp_type_cd != 'center' )
            {
                $this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD'));
            }
        }
        else if ( !IS_MANAGER && !IS_CARD_AGENCY )
        {
            if ( $in_type == "sub" || $in_type == "sub_main" )
            {    
                $this->db->where('A.USERID <>', $USERID) ;
                
                $this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID') ;            
                $subquery = "SELECT USERID FROM D_MEMBER START WITH USERID = '".$this->session->userdata('ss_mb_id')."' CONNECT BY P_ID = PRIOR USERID" ;
                $this->db->where_in('A.USERID', $subquery, FALSE) ; 
            }
            else if ( $in_type == "reception" )            
            {
                $this->db->where('A.RECEPTION_ID', $USERID) ;
                $this->db->where('ORD_DATE >= ', "20160401") ;
            }
            else
            {
                $this->db->where('A.USERID', $USERID) ;
            }             
        }    

        $this->db->where( 'C.TYPE_CD', $in_type_cd ) ;    

        return $this->db->count_all_results('O_ORDERMASTER A') ;
    }

    // 검색 구문을 얻는다.
    function _get_search_cache($search_field, $search_text) 
    {        
        if (!$search_field || !$search_text)
            return FALSE;

        $search_field = $search_field ;

        $where = '' ;

        if ( $search_field == 'ORD_NAME' )
        {
            $where .= "PKG_CRYPTO_FIELD.get('ORD_NAME', A.ORD_NAME, '9','".G_CRYPTO."') LIKE ".$this->db->escape('%'.$search_text.'%');
        }
        else if ( $search_field == 'D.OLD_TEL' || $search_field == 'A.USERID' || $search_field == 'RECEPTION_ID' )
        {
            $where .= $this->db->protect_identifiers($search_field).' LIKE \'%'.$search_text.'%\'';
        }
        else if ( $search_field == 'RECV_MOBILE' )
        {
            $where .= "PKG_CRYPTO_FIELD.get('RECV_MOBILE', F.RECV_MOBILE, '9','".G_CRYPTO."') LIKE '%".$search_text."%'";
        }
        else
        {
            if (preg_match('/[a-zA-Z]/', $search_text))
                $where .= 'LOWER('.$this->db->protect_identifiers($search_field).') LIKE LOWER('.$this->db->escape('%'.$search_text.'%').')';
            else
                $where .= $this->db->protect_identifiers($search_field).' LIKE '.$this->db->escape('%'.$search_text.'%');
        }
            
        $this->db->where($where, null, FALSE);
    }

    // 기간 검색
    function _get_search_date_range($start_date='', $finish_date = '')
    {
        $sdt = $this->input->get('sdt') ;

        if ( $sdt == '' )
        {
            $sdt = 'ORD_DATE' ;
        }

        if ( $start_date == '' ) {
            $start_date = $this->input->get('start');   // 시작날짜
        }

        if ( $finish_date == '' ) {
            $finish_date = $this->input->get('finish'); // 종료날짜
        }

        if ( $start_date != '' )
        {
            $this->db->where($sdt.' >= ', "'".$start_date."'", false) ;
        }

        if ( $finish_date != '' )
        {
            $Y = substr($finish_date, 0, 4) ;
            $M = substr($finish_date, 4, 2) ;
            $D = substr($finish_date, -2) ;
            $finish = date("Ymd", mktime(0, 0, 0, $M, $D + 1, $Y)) ;         

            $this->db->where($sdt.' < ', "'".$finish."'", false) ;
        }
    }

    function get_order_info($in_ord_no)
    {
        $select = "A.ORDNO, A.ORD_DATE, A.CENTER_CD, B.AMT, B.PV1, B.PV2, C.PDT_NAME, C.PDT_MODEL, A.RECEPTION_ID, A.START_CENTER, A.AMT,
                D.REG_NAME, D.OLD_TEL_COMP, D.OLD_TEL, D.REG_EMAIL, D.REG_BIRTH, D.BILL_KIND, D.PAY_KIND, L.UPPER_REG_NO, L.SELLER_ID, L.REQUEST_OPEN,
                E.OPT_VALUE, F.ORD_MSG, F.RECV_ZIPCODE, F.RECV_ADDR1, H.CP_NAME AS PRICE_KIND,
                D.LEGAL_NAME, D.LEGAL_TEL, D.LEGAL_RELRATION, 
                PKG_CRYPTO_FIELD.get('RECV_NAME', F.RECV_NAME, ,'9','".G_CRYPTO."') as RECV_NAME, 
                PKG_CRYPTO_FIELD.get('RECV_TEL', F.RECV_TEL, ,'9','".G_CRYPTO."') as RECV_TEL,
                PKG_CRYPTO_FIELD.get('RECV_MOBILE', F.RECV_MOBILE, ,'9','".G_CRYPTO."') as RECV_MOBILE, 
                PKG_CRYPTO_FIELD.get('RECV_ADDR2', F.RECV_ADDR2, ,'9','".G_CRYPTO."') as RECV_ADDR2, 
                PKG_CRYPTO_FIELD.get('ORD_EMAIL', F.ORD_EMAIL, ,'9','".G_CRYPTO."') as ORD_EMAIL, 
                G.REASON_NAME AS RECEIPT_KIND, I.IMG_PATH AS ORDER_IMG, L.ORD_MANAGER_MSG,
                K.REASON_NAME AS REG_KIND, DECODE(A.ORD_TYPE, '1', NVL(A.GT_CD, '재발급요청필요'), GT_CD) AS GT_CDS, A.GT_CD,
                (SELECT PKG_CRYPTO_FIELD.get('USERNAME', USERNAME, ,'9','".G_CRYPTO."') FROM D_MEMBER WHERE USERID = A.RECEPTION_ID) as RECEPTION_NAME,
                L.BENEFIT_SERVICE, L.ADD_SERVICE" ;

        $this->db->select($select, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;
        $this->db->join('O_ORDERTEL D', 'A.ORDNO = D.ORDNO') ;
        $this->db->join('P_PDTDETAIL E', 'E.PDTDETAIL_NO = B.PDTDETAIL_NO') ;
        $this->db->join('O_ORDERPERSON F', 'A.ORDNO = F.ORDNO') ;
        $this->db->join('S_REASON G', 'G.REASON_CD = B.RECEIPT_KIND', 'left') ;
        $this->db->join('P_CALLING_PLAN H', 'H.CP_CD = D.PRICE_KIND', 'left') ;
        $this->db->join('O_ORDERIMAGE I', 'I.ORDNO = A.ORDNO', 'left') ;
        $this->db->join('P_PDTIMAGE J', 'J.PDT_CD = B.PDT_CD', 'left') ;
        $this->db->join('S_REASON K', 'K.REASON_CD = D.REG_KIND', 'left') ;        
        $this->db->join('O_ORDERSTATE L', 'L.ORDNO = A.ORDNO', 'left') ;

        $this->db->where('A.ORDNO', $in_ord_no) ;
        $this->db->where('C.IS_TELPRICE', 'O') ;

        return $this->db->get()->row_array() ;       
    }

    function get_order_open_count($in_USERID, $in_Y, $in_M, $in_type_cd) 
    {   
        $start = $in_Y.$in_M.'01' ; 
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;        

        $this->db->select("COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;        
        $this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;

        if ( $in_type_cd == '2502' )       // mobile
        {        
            $this->db->join('O_ORDERTEL D', 'A.ORDNO = D.ORDNO') ;       

            $this->db->where('D.REAL_OPEN_DT4 >=', $start) ; 
            $this->db->where('D.REAL_OPEN_DT4 <', $finish) ; 
            $this->db->where('C.IS_TELPRICE', 'O') ;  
            $this->db->where('A.IS_TEL_PDT', 'O') ;  
            //$this->db->where('A.GT_STATUS', '3803') ; 
            $this->db->where('A.ORD_TYPE', '1') ; 
        }
        else if ( $in_type_cd == '2504' )       // wired
        {
            $this->db->where('A.ORD_DATE >=', $start) ; 
            $this->db->where('A.ORD_DATE <', $finish) ; 
            $this->db->join('O_ORDER_CTEL K', 'A.ORDNO = K.ORDNO') ;
        }
        else if ( $in_type_cd == '2506' )       // card
        {
            $this->db->where('A.ORD_DATE >=', $start) ; 
            $this->db->where('A.ORD_DATE <', $finish) ; 
            $this->db->join('O_ORDER_ACARD K', 'A.ORDNO = K.ORDNO') ;
        }
         
        $subquery = "SELECT USERID FROM D_MEMBER START WITH USERID = '".$in_USERID."' CONNECT BY P_ID = PRIOR USERID" ;
        $this->db->where_in('A.USERID', $subquery, FALSE) ; 

        $result = $this->db->get()->row_array() ; 

        return  ( $result && isset($result['CNT']) ) ? $result['CNT'] : 0 ;        
    }

    function get_order_open_list($in_USERID, $in_Y, $in_M, $limit, $offset, $in_type_cd) 
    {   
        $start = $in_Y.$in_M.'01' ;   
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;       

        $this->db->limit($limit, $offset) ;

        $select = "A.ORDNO, A.ORD_DATE, A.GT_CD, C.PDT_NAME, D.REG_NAME, D.OLD_TEL, E.OPT_VALUE, 
                    PKG_CRYPTO_FIELD.get('ORD_NAME', A.ORD_NAME, ,'9','".G_CRYPTO."') as ORD_NAME,
                    PKG_CRYPTO_FIELD.get('RECV_MOBILE', F.RECV_MOBILE, ,'9','".G_CRYPTO."') as RECV_MOBILE" ;

        $this->db->select($select, false) ;
        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;
        $this->db->join('O_ORDERTEL D', 'A.ORDNO = D.ORDNO') ;
        $this->db->join('P_PDTDETAIL E', 'E.PDTDETAIL_NO = B.PDTDETAIL_NO') ;
        $this->db->join('O_ORDERPERSON F', 'A.ORDNO = F.ORDNO') ;        
        $this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID') ;

        $this->db->group_by('A.ORDNO, A.ORD_DATE, A.GT_CD, A.ORD_NAME, C.PDT_NAME, D.REG_NAME, D.OLD_TEL, E.OPT_VALUE, F.RECV_MOBILE') ;

        //$this->db->where('C.IS_TELPRICE', 'O') ;  
        //$this->db->where('A.IS_TEL_PDT', 'O') ;  
        //$this->db->where('A.GT_STATUS', '3803') ; 
        //$this->db->where('A.ORD_TYPE', '1') ; 
        $this->db->where('A.ORD_DATE >=', $start) ; 
        $this->db->where('A.ORD_DATE <', $finish) ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
         
        $subquery = "SELECT USERID FROM D_MEMBER START WITH USERID = '".$in_USERID."' CONNECT BY P_ID = PRIOR USERID" ;
        $this->db->where_in('A.USERID', $subquery, FALSE) ; 

        return $this->db->get()->result_array() ;   
    }

    function get_order_daily_count_list($in_Y, $in_M) 
    {   
        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;

        $this->db->select("TO_CHAR(TO_DATE(D.REAL_OPEN_DT4, 'YYYYMMDDHH24MISS'), 'YYYYMMDD') as REAL_OPEN_DT4, C.TEL_COMPANY, COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;               
        $this->db->join('O_ORDERTEL D', 'D.ORDNO = B.ORDNO') ;               

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        //$this->db->where('A.GT_STATUS', '3803') ; 
        $this->db->where('A.ORD_TYPE', '1') ; 
        $this->db->where('D.REAL_OPEN_DT4 >=', $start) ; 
        $this->db->where('D.REAL_OPEN_DT4 <', $finish) ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;

        $this->db->group_by( array ( "TO_CHAR(TO_DATE(D.REAL_OPEN_DT4, 'YYYYMMDDHH24MISS'), 'YYYYMMDD')", "C.TEL_COMPANY") ) ;         
        $this->db->order_by("REAL_OPEN_DT4", 'DESC') ;         

        $result = $this->db->get()->result_array() ; 

        $list = array() ;
        $list['TOTAL'] = 0 ;
        foreach ($result as $key => $value) 
        {
            $value['REAL_OPEN_DT4'] = substr($value['REAL_OPEN_DT4'], 0, 8) ;

            $list[$value['REAL_OPEN_DT4']][$value['TEL_COMPANY']] = $value['CNT'] ;            

            if ( isset($list[$value['REAL_OPEN_DT4']]['TOTAL']) )
            {
                $list[$value['REAL_OPEN_DT4']]['TOTAL'] += $value['CNT'] ;
            }
            else
            {
                $list[$value['REAL_OPEN_DT4']]['TOTAL'] = $value['CNT'] ;   
            }

            $list['TOTAL'] += $value['CNT'] ;         
        }

        return $list ;
    }

    function get_order_direct_recomend_count_total() 
    {   
        // 기간 검색
        $this->_get_search_date_range() ;      

        $this->db->select("COUNT(DISTINCT E.USERID) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;               
        $this->db->join('D_MEMBER D', 'D.USERID = A.USERID') ;               
        $this->db->join('D_MEMBER E', 'E.USERID = D.R_ID') ;               

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        $this->db->where('A.GT_STATUS', '3803') ; 
        $this->db->where('A.ORD_TYPE', '1') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('E.USERID <>', 'D0000000') ; 

        return $this->db->get()->row()->CNT ; 
    }

    function get_order_direct_recomend_count_list($limit, $offset) 
    {   
        // 기간 검색
        $this->_get_search_date_range() ;      

        $this->db->limit($limit, $offset) ;

        $select = "D.R_ID, COUNT(DISTINCT A.ORDNO) AS CNT, E.USERID, E.REG_DATE,
               PKG_CRYPTO_FIELD.get('USERNAME', E.USERNAME, '9','".G_CRYPTO."') as USERNAME, 
               PKG_CRYPTO_FIELD.get('TEL', E.TEL, '9','".G_CRYPTO."') as TEL, 
               PKG_CRYPTO_FIELD.get('MOBILE', E.MOBILE, '9','".G_CRYPTO."') as MOBILE, 
               (SELECT FULL_NAME FROM S_RANK WHERE RANK_CD = E.RANK_CD) as RANK_NAME,
               (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = E.CENTER_CD) as CENTER_NAME" ;

        $this->db->select($select, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;               
        $this->db->join('D_MEMBER D', 'D.USERID = A.USERID') ;               
        $this->db->join('D_MEMBER E', 'E.USERID = D.R_ID') ;               

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        $this->db->where('A.GT_STATUS', '3803') ; 
        $this->db->where('A.ORD_TYPE', '1') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('E.USERID <>', 'D0000000') ; 
        $this->db->where('C.TYPE_CD', '2502') ;

        $this->db->group_by('D.R_ID, E.USERNAME, E.USERID, E.TEL, E.MOBILE, E.REG_DATE, E.RANK_CD, E.CENTER_CD') ;         
        $this->db->order_by('CNT', 'DESC') ;         

        return $this->db->get()->result_array() ;         
    }

    function get_user_order_direct_recomend_list($in_mb_id) 
    {   
        // 기간 검색
        $this->_get_search_date_range() ;              

        $select = "A.ORDNO, A.ORD_DATE, C.PDT_NAME, E.OPT_VALUE, D.REG_NAME, D.OLD_TEL, G.STATE_CD,
                PKG_CRYPTO_FIELD.get('ORD_NAME', A.ORD_NAME, ,'9','".G_CRYPTO."') as ORD_NAME,                
                TO_CHAR(TO_DATE(D.REAL_OPEN_DT4, 'YYYYMMDDHH24MISS'), 'YYYY-MM-DD HH:MM') as REAL_OPEN_DT4,                       
                F.REASON_NAME AS RECEIPT_KIND, DECODE(A.ORD_TYPE, '1', NVL(A.GT_CD, '재발급요청필요'), GT_CD) AS GT_CDS" ;                

        $this->db->select($select, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;               
        $this->db->join('O_ORDERTEL D', 'A.ORDNO = D.ORDNO') ; 
        $this->db->join('P_PDTDETAIL E', 'E.PDTDETAIL_NO = B.PDTDETAIL_NO') ;        
        $this->db->join('S_REASON F', 'F.REASON_CD = B.RECEIPT_KIND', 'left') ;   
        $this->db->join('O_ORDERSTATE G', 'G.ORDNO = A.ORDNO', 'left') ;                   
        $this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID') ;               

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        $this->db->where('A.GT_STATUS', '3803') ; 
        $this->db->where('A.ORD_TYPE', '1') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('Z.R_ID', $in_mb_id ) ; 
        $this->db->where('C.TYPE_CD', '2502') ;
      
        $this->db->order_by('A.ORDNO', 'DESC') ;         

        return $this->db->get()->result_array() ;         
    }

    function get_model_list_by_vender($vender, $in_Y, $in_M) 
    {   
        $start = $in_Y.$in_M.'01' ; 
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;            

        return $this->_get_model_list_by_vender($vender, $start, $finish) ;            
    }    

    function _get_model_list_by_vender($vender, $in_start_date, $in_finish_date) 
    {   
        $this->db->select("E.REASON_NAME, C.PDT_MODEL, C.PDT_NAME", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;               
        $this->db->join('O_ORDERTEL D', 'D.ORDNO = B.ORDNO') ;               
        $this->db->join('S_REASON E', 'E.REASON_CD = C.BRAND_CD', 'left') ;               

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        $this->db->where('A.GT_STATUS', '3803') ; 
        $this->db->where('A.ORD_TYPE', '1') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('C.TEL_COMPANY', $vender) ;                 

        if ( $in_start_date != '' )
        {
            $this->db->where('D.REAL_OPEN_DT4 >= ', "'".$in_start_date."'", false) ;
        }

        if ( $in_finish_date != '' )
        {
            $this->db->where('D.REAL_OPEN_DT4 < ', "'".$in_finish_date."'", false) ;
        }             

        $this->db->group_by('E.REASON_NAME, C.PDT_MODEL, C.PDT_NAME') ;         
        $this->db->order_by('C.PDT_MODEL', 'DESC') ;         

        $result = $this->db->get()->result_array() ;         

        $list = array() ;          
        $list['BRAND_LIST'] = array() ;
        $list['MODEL_LIST'] = array() ;
        foreach ($result as $key => $value) 
        {   
            $value['REASON_NAME'] = ($value['REASON_NAME']) ? $value['REASON_NAME'] : '없음' ;

            if ( !isset($list['BRAND_LIST'][$value['REASON_NAME']]) )
            {
                $list['BRAND_LIST'][$value['REASON_NAME']] = 0 ;                   
            }
            
            $list['BRAND_LIST'][$value['REASON_NAME']]++ ;  

            $model = array( 'MODEL' => $value['PDT_MODEL'], 'NAME' => $value['PDT_NAME'] ) ;         

            array_push($list['MODEL_LIST'], $model) ;                                
        }                

        return $list ;        
    }

    function get_calling_plan_list_by_vender($vender, $in_Y, $in_M) 
    {
        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;  

        return $this->_get_calling_plan_list_by_vender($vender, $start, $finish) ;
    }

    function _get_calling_plan_list_by_vender($vender, $in_start_date, $in_finish_date) 
    {                   
        $this->db->select("B.CP_CD, B.CP_NAME") ;

        $this->db->from('O_ORDERTEL A') ;   
        $this->db->join('P_CALLING_PLAN B', 'A.PRICE_KIND = B.CP_CD', 'left') ;
        $this->db->join('O_ORDERMASTER C', 'C.ORDNO = A.ORDNO') ;     

        $this->db->where('B.CP_COMM', $vender) ;                         
        $this->db->where('C.IS_TEL_PDT', 'O') ;  
        $this->db->where('C.GT_STATUS', '3803') ; 
        $this->db->where('C.ORD_TYPE', '1') ;
        $this->db->where('C.ORD_STATE >=', 10) ;
        $this->db->where('C.ORD_STATE <', 20) ;

        if ( $in_start_date != '' )
        {
            $this->db->where('A.REAL_OPEN_DT4 >= ', "'".$in_start_date."'", false) ;
        }

        if ( $in_finish_date != '' )
        {
            $this->db->where('A.REAL_OPEN_DT4 < ', "'".$in_finish_date."'", false) ;
        }    
        
        $this->db->order_by("B.CP_NAME", "ASC") ;

        $result = $this->db->get()->result_array() ;         

        $list = array() ;
        foreach ($result as $key => $value) 
        {
            $list[$value['CP_CD']] = $value['CP_NAME'] ;
        }

        return $list ;       
    }   

    function get_reg_kind_list_by_vender($vender, $in_start_date, $in_finish_date) 
    {                   
        $this->db->select("B.REASON_CD, B.REASON_NAME") ;

        $this->db->from('O_ORDERTEL A') ;   
        $this->db->join('S_REASON B', 'A.REG_KIND = B.REASON_CD', 'left') ;
        $this->db->join('O_ORDERMASTER C', 'C.ORDNO = A.ORDNO') ;     

        $this->db->where('A.TEL_COMPANY', $vender) ;                         
        $this->db->where('C.IS_TEL_PDT', 'O') ;  
        $this->db->where('C.GT_STATUS', '3803') ; 
        $this->db->where('C.ORD_TYPE', '1') ;
        $this->db->where('C.ORD_STATE >=', 10) ;
        $this->db->where('C.ORD_STATE <', 20) ;
        $this->db->order_by('B.REASON_CD', 'ASC') ;

        if ( $in_start_date != '' )
        {
            $this->db->where('A.REAL_OPEN_DT4 >= ', "'".$in_start_date."'", false) ;
        }

        if ( $in_finish_date != '' )
        {
            $this->db->where('A.REAL_OPEN_DT4 <= ', "'".$in_finish_date."'", false) ;
        }    
        
        $this->db->order_by("B.REASON_NAME", "ASC") ;

        $result = $this->db->get()->result_array() ;         

        $list = array() ;
        foreach ($result as $key => $value) 
        {
            $list[$value['REASON_CD']] = $value['REASON_NAME'] ;
        }

        return $list ;       
    }  

    function get_order_daily_count_list_by_vender($vender, $in_Y, $in_M) 
    {   
        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;           

        $this->db->select("TO_CHAR(TO_DATE(D.REAL_OPEN_DT4, 'YYYYMMDDHH24MISS'), 'YYYYMMDD') as REAL_OPEN_DT4, C.PDT_MODEL, COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;               
        $this->db->join('O_ORDERTEL D', 'D.ORDNO = B.ORDNO') ;               

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        $this->db->where('A.GT_STATUS', '3803') ; 
        $this->db->where('A.ORD_TYPE', '1') ; 
        $this->db->where('D.REAL_OPEN_DT4 >=', $start) ; 
        $this->db->where('D.REAL_OPEN_DT4 <', $finish) ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('C.TEL_COMPANY', $vender) ; 

        $this->db->group_by( array ( "TO_CHAR(TO_DATE(D.REAL_OPEN_DT4, 'YYYYMMDDHH24MISS'), 'YYYYMMDD')", "C.PDT_MODEL") ) ;                 
        $this->db->order_by('REAL_OPEN_DT4', 'DESC') ;         

        $result = $this->db->get()->result_array() ; 

        $list = array() ;  
        $list['TOTAL'] = 0 ;          
        foreach ($result as $key => $value) 
        {
            $value['REAL_OPEN_DT4'] = substr($value['REAL_OPEN_DT4'], 0, 8) ;

            $list[$value['REAL_OPEN_DT4']][$value['PDT_MODEL']] = $value['CNT'] ;           

            if ( isset($list['MODEL_TOTAL'][$value['PDT_MODEL']]) && $list['MODEL_TOTAL'][$value['PDT_MODEL']] >= 0 )
            {
                $list['MODEL_TOTAL'][$value['PDT_MODEL']] += $value['CNT'] ;         
            }
            else
            {
                $list['MODEL_TOTAL'][$value['PDT_MODEL']] = $value['CNT'] ;         
            }

            $list['TOTAL'] += $value['CNT'] ;    
        }

        return $list ;
    }   

    function get_order_daily_count_list_by_vender2($vender, $in_Y, $in_M) 
    {   
        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;            

        $this->db->select("TO_CHAR(TO_DATE(D.REAL_OPEN_DT4, 'YYYYMMDDHH24MISS'), 'YYYYMMDD') as REAL_OPEN_DT4, C.PDT_MODEL, D.PRICE_KIND, COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;               
        $this->db->join('O_ORDERTEL D', 'D.ORDNO = B.ORDNO') ;               

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        //$this->db->where('A.GT_STATUS', '3803') ; 
        $this->db->where('A.ORD_TYPE', '1') ; 
        $this->db->where('D.REAL_OPEN_DT4 >=', $start) ; 
        $this->db->where('D.REAL_OPEN_DT4 <', $finish) ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('C.TEL_COMPANY', $vender) ; 

        $this->db->group_by( array ( "TO_CHAR(TO_DATE(D.REAL_OPEN_DT4, 'YYYYMMDDHH24MISS'), 'YYYYMMDD')", "C.PDT_MODEL", "D.PRICE_KIND") ) ;                 
        $this->db->order_by('REAL_OPEN_DT4', 'DESC') ;         

        $result = $this->db->get()->result_array() ; 

        $list = array() ;  
        $list['TOTAL'] = 0 ;          
        foreach ($result as $key => $value) 
        {
            $value['REAL_OPEN_DT4'] = substr($value['REAL_OPEN_DT4'], 0, 8) ;

            $list[$value['REAL_OPEN_DT4']][$value['PDT_MODEL']][$value['PRICE_KIND']] = $value['CNT'] ;       

            if ( isset($list[$value['REAL_OPEN_DT4']][$value['PDT_MODEL']]['TOTAL'] ) && $list[$value['REAL_OPEN_DT4']][$value['PDT_MODEL']]['TOTAL'] >= 0 )
            {
                $list[$value['REAL_OPEN_DT4']][$value['PDT_MODEL']]['TOTAL'] += $value['CNT'] ;         
            }
            else
            {
                $list[$value['REAL_OPEN_DT4']][$value['PDT_MODEL']]['TOTAL'] = $value['CNT'] ;         
            } 

            if ( isset($list['MODEL_TOTAL'][$value['PDT_MODEL']] ) && $list['MODEL_TOTAL'][$value['PDT_MODEL']] >= 0 )
            {
                $list['MODEL_TOTAL'][$value['PDT_MODEL']] += $value['CNT'] ;         
            }
            else
            {
                $list['MODEL_TOTAL'][$value['PDT_MODEL']] = $value['CNT'] ;         
            }

            $list['TOTAL'] += $value['CNT'] ;    
        }

        return $list ;
    }   

    function get_order_period_count_list_by_vender($vender, $in_start_date, $in_finish_date) 
    {       
        $this->db->select("C.PDT_MODEL, D.PRICE_KIND, D.REG_KIND, COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;               
        $this->db->join('O_ORDERTEL D', 'D.ORDNO = B.ORDNO') ;               

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        //$this->db->where('A.GT_STATUS', '3803') ; 
        $this->db->where('A.ORD_TYPE', '1') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('C.TEL_COMPANY', $vender) ; 

        if ( $in_start_date != '' )
        {
            $this->db->where('D.REAL_OPEN_DT4 >= ', "'".$in_start_date."'", false) ;
        }

        if ( $in_finish_date != '' )
        {            
            $Y = substr($in_finish_date, 0, 4) ;
            $M = substr($in_finish_date, 4, 2) ;
            $D = substr($in_finish_date, -2) ;
            $finish = date("Ymd", mktime(0, 0, 0, $M, $D + 1, $Y)) ;                         

            $this->db->where('D.REAL_OPEN_DT4 < ', "'".$finish."'", false) ;            
        }      

        $this->db->group_by( array ("C.PDT_MODEL", "D.PRICE_KIND", "D.REG_KIND") ) ;                         

        $result = $this->db->get()->result_array() ; 

        $list = array() ;  
        $list['TOTAL'] = 0 ;          
        foreach ($result as $key => $value) 
        {            
            $list[$value['PDT_MODEL']][$value['PRICE_KIND']][$value['REG_KIND']] = $value['CNT'] ;       

            if ( isset($list['MODEL_TOTAL'][$value['PDT_MODEL']][$value['REG_KIND']] ) && $list['MODEL_TOTAL'][$value['PDT_MODEL']][$value['REG_KIND']] >= 0 )
            {
                $list['MODEL_TOTAL'][$value['PDT_MODEL']][$value['REG_KIND']] += $value['CNT'] ;         
            }
            else
            {
                $list['MODEL_TOTAL'][$value['PDT_MODEL']][$value['REG_KIND']] = $value['CNT'] ;         
            }

            if ( isset($list[$value['PDT_MODEL']]['TOTAL'] ) && $list[$value['PDT_MODEL']]['TOTAL'] >= 0 )
            {
                $list[$value['PDT_MODEL']]['TOTAL'] += $value['CNT'] ;         
            }
            else
            {
                $list[$value['PDT_MODEL']]['TOTAL'] = $value['CNT'] ;         
            } 

            $list['TOTAL'] += $value['CNT'] ;    
        }

        return $list ;
    } 

    function get_order_count_by_datas($in_reception_id, $in_order_name, $in_credit_result_date)
    {        
        $year = substr($in_credit_result_date, 0, 4) ;
        $month = substr($in_credit_result_date, 5, 2) ;
        $day = substr($in_credit_result_date, 8, 2) ;

        $date = date("Ymd", mktime(0, 0, 0, $month, $day, $year)) ;             

        $this->db->select("A.ORDNO, A.GT_CD") ;

        $this->db->join('O_ORDERTEL B', 'A.ORDNO = B.ORDNO') ;            

        $this->db->where('A.RECEPTION_ID', $in_reception_id) ;
        $this->db->where('B.REG_NAME', $in_order_name) ;        
        $this->db->where("A.ORD_DATE <=", $date, false) ; 
                
        return $this->db->count_all_results('O_ORDERMASTER A') ;  
    }

    function get_order_info_by_datas($in_reception_id, $in_order_name, $in_credit_result_date)
    {        
        $year = substr($in_credit_result_date, 0, 4) ;
        $month = substr($in_credit_result_date, 5, 2) ;
        $day = substr($in_credit_result_date, 8, 2) ;

        $date = date("Ymd", mktime(0, 0, 0, $month, $day, $year)) ;             

        $this->db->select("A.ORDNO, A.GT_CD") ;

        $this->db->join('O_ORDERTEL B', 'A.ORDNO = B.ORDNO') ;            

        $this->db->where('A.RECEPTION_ID', $in_reception_id) ;
        $this->db->where('B.REG_NAME', $in_order_name) ;        
        $this->db->where("A.ORD_DATE <=", $date, false) ; 

        return $this->db->get('O_ORDERMASTER A')->row_array() ;       
    }

    function is_mobile_order($in_ord_no)
    {
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;

        $this->db->where('A.ORDNO', $in_ord_no) ;
        $this->db->where('C.TYPE_CD', '2502') ;

        return ( $this->db->count_all_results('O_ORDERMASTER A') > 0 ) ? true : false ;
    }

    function get_order_mobile_model_count_list($in_start_date, $in_finish_date)
    {
        $this->db->select("C.PDT_MODEL, C.PDT_NAME, C.TEL_COMPANY, COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;
        $this->db->join('O_ORDERTEL D', 'D.ORDNO = B.ORDNO') ;
        $this->db->join('O_ORDERSTATE E', 'E.ORDNO = A.ORDNO') ;

        $this->db->where('C.IS_TELPRICE', 'O') ;
        $this->db->where('A.IS_TEL_PDT', 'O') ;
        $this->db->where('A.ORD_TYPE', '1') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('E.STATE_CD !=', '7', false) ;

        if ( $in_start_date != '' )
        {
            $this->db->where('D.REAL_OPEN_DT4 >= ', "'".$in_start_date."'", false) ;
        }

        if ( $in_finish_date != '' )
        {
            $Y = substr($in_finish_date, 0, 4) ;
            $M = substr($in_finish_date, 4, 2) ;
            $D = substr($in_finish_date, -2) ;
            $finish = date("Ymd", mktime(0, 0, 0, $M, $D + 1, $Y)) ;

            $this->db->where('D.REAL_OPEN_DT4 < ', "'".$finish."'", false) ;
        }

        $this->db->group_by( array ("C.PDT_MODEL", "C.PDT_NAME", "C.TEL_COMPANY") ) ;
        $this->db->order_by ( 'CNT', 'desc' ) ;

        $result = $this->db->get()->result_array() ;

        $list = array() ;
        $list['TOTAL'] = 0 ;
        foreach ($result as $key => $value)
        {
            $list[$value['TEL_COMPANY']][$value['PDT_MODEL']]['PDT_NAME'] = $value['PDT_NAME'] ;
            $list[$value['TEL_COMPANY']][$value['PDT_MODEL']]['COUNT'] = $value['CNT'] ;

            if ( isset($list[$value['TEL_COMPANY']]['TOTAL']) && $list[$value['TEL_COMPANY']]['TOTAL'] >= 0 )
            {
                $list[$value['TEL_COMPANY']]['TOTAL'] += $value['CNT'] ;
            }
            else
            {
                $list[$value['TEL_COMPANY']]['TOTAL']= $value['CNT'] ;
            }

            $list['TOTAL'] += $value['CNT'] ;
        }

        return $list ;
    }

    function get_order_mobile_accumulate_count_list($in_start_date, $in_finish_date)
    {
        $this->db->select("B.REG_KIND, B.TEL_COMPANY, COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERTEL B', 'B.ORDNO = A.ORDNO') ;
        $this->db->join('O_ORDERSTATE C', 'C.ORDNO = A.ORDNO') ;

        $this->db->where('A.IS_TEL_PDT', 'O') ;
        $this->db->where('A.ORD_TYPE', '1') ;
        $this->db->where('A.ORD_STATE >=', 10) ;
        $this->db->where('A.ORD_STATE <', 20) ;
        $this->db->where('C.STATE_CD !=', '7', false) ;

        if ( $in_start_date != '' )
        {
            $this->db->where('B.REAL_OPEN_DT4 >= ', "'".$in_start_date."'", false) ;
        }

        if ( $in_finish_date != '' )
        {
            $Y = substr($in_finish_date, 0, 4) ;
            $M = substr($in_finish_date, 4, 2) ;
            $D = substr($in_finish_date, -2) ;
            $finish = date("Ymd", mktime(0, 0, 0, $M, $D + 1, $Y)) ;

            $this->db->where('B.REAL_OPEN_DT4 < ', "'".$finish."'", false) ;
        }

        $this->db->group_by( array ("B.REG_KIND", "B.TEL_COMPANY") ) ;

        $result = $this->db->get()->result_array() ;

        $list = array() ;
        foreach ($result as $key => $value)
        {
            if ( isset($list[$value['TEL_COMPANY']][$value['REG_KIND']] ) && $list[$value['TEL_COMPANY']][$value['REG_KIND']]  >= 0 )
            {
                $list[$value['TEL_COMPANY']][$value['REG_KIND']]  += $value['CNT'] ;
            }
            else
            {
                $list[$value['TEL_COMPANY']][$value['REG_KIND']] = $value['CNT'] ;
            }

            if ( isset($list[$value['TEL_COMPANY']]['TOTAL'] ) && $list[$value['TEL_COMPANY']]['TOTAL']  >= 0 )
            {
                $list[$value['TEL_COMPANY']]['TOTAL'] += $value['CNT'] ;
            }
            else
            {
                $list[$value['TEL_COMPANY']]['TOTAL'] = $value['CNT'] ;
            }
        }

        return $list ;
    }

    function get_order_mobile_count_list($in_start_date, $in_finish_date)
    {
        $this->db->select("B.REG_KIND, B.TEL_COMPANY, COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERTEL B', 'B.ORDNO = A.ORDNO') ;

        $this->db->where('A.IS_TEL_PDT', 'O') ;
        $this->db->where('A.ORD_TYPE', '1') ;

        if ( $in_start_date != '' )
        {
            $this->db->where('A.ORD_DATE >= ', "'".$in_start_date."'", false) ;
        }

        if ( $in_finish_date != '' )
        {
            $Y = substr($in_finish_date, 0, 4) ;
            $M = substr($in_finish_date, 4, 2) ;
            $D = substr($in_finish_date, -2) ;
            $finish = date("Ymd", mktime(0, 0, 0, $M, $D + 1, $Y)) ;

            $this->db->where('A.ORD_DATE < ', "'".$finish."'", false) ;
        }

        $this->db->group_by( array ("B.REG_KIND", "B.TEL_COMPANY") ) ;

        $result = $this->db->get()->result_array() ;

        $list = array() ;
        foreach ($result as $key => $value)
        {
            if ( isset($list[$value['TEL_COMPANY']][$value['REG_KIND']] ) && $list[$value['TEL_COMPANY']][$value['REG_KIND']]  >= 0 )
            {
                $list[$value['TEL_COMPANY']][$value['REG_KIND']]  += $value['CNT'] ;
            }
            else
            {
                $list[$value['TEL_COMPANY']][$value['REG_KIND']] = $value['CNT'] ;
            }

            if ( isset($list[$value['TEL_COMPANY']]['TOTAL'] ) && $list[$value['TEL_COMPANY']]['TOTAL']  >= 0 )
            {
                $list[$value['TEL_COMPANY']]['TOTAL'] += $value['CNT'] ;
            }
            else
            {
                $list[$value['TEL_COMPANY']]['TOTAL'] = $value['CNT'] ;
            }
        }

        return $list ;
    }

    function get_order_mobile_count_list2($in_start_date, $in_finish_date, $in_type = '')
    {
        $this->db->select("B.REG_KIND, B.TEL_COMPANY, COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERTEL B', 'B.ORDNO = A.ORDNO') ;
        $this->db->join('O_ORDERSTATE C', 'C.ORDNO = A.ORDNO') ;

        $this->db->where('A.IS_TEL_PDT', 'O') ;
        $this->db->where('A.ORD_TYPE', '1') ;

        if ( $in_type == 'not_opened' ) {
            $this->db->where('(C.STATE_CD = 2 OR C.STATE_CD = 4)', '', false);
        }
        else if ( $in_type == 'canceled' )
        {
            $this->db->where('C.STATE_CD', '7') ;
        }

        if ( $in_start_date != '' )
        {
            if ( $in_type == 'not_opened' ) {
                $this->db->where('A.ORD_DATE >= ', "'" . $in_start_date . "'", false);
            }
            else if ( $in_type == 'canceled' )
            {
                $this->db->where('B.REAL_OPEN_DT4 >= ', "'".$in_start_date."'", false) ;
            }
        }

        if ( $in_finish_date != '' )
        {
            $Y = substr($in_finish_date, 0, 4) ;
            $M = substr($in_finish_date, 4, 2) ;
            $D = substr($in_finish_date, -2) ;
            $finish = date("Ymd", mktime(0, 0, 0, $M, $D + 1, $Y)) ;

            if ( $in_type == 'not_opened' ) {
                $this->db->where('A.ORD_DATE < ', "'" . $finish . "'", false);
            }
            else if ( $in_type == 'canceled' )
            {
                $this->db->where('B.REAL_OPEN_DT4 < ', "'".$finish."'", false) ;
            }
        }

        $this->db->group_by( array ("B.REG_KIND", "B.TEL_COMPANY") ) ;

        $result = $this->db->get()->result_array() ;

        $list = array() ;
        foreach ($result as $key => $value)
        {
            if ( isset($list[$value['TEL_COMPANY']][$value['REG_KIND']] ) && $list[$value['TEL_COMPANY']][$value['REG_KIND']]  >= 0 )
            {
                $list[$value['TEL_COMPANY']][$value['REG_KIND']]  += $value['CNT'] ;
            }
            else
            {
                $list[$value['TEL_COMPANY']][$value['REG_KIND']] = $value['CNT'] ;
            }

            if ( isset($list[$value['TEL_COMPANY']]['TOTAL'] ) && $list[$value['TEL_COMPANY']]['TOTAL']  >= 0 )
            {
                $list[$value['TEL_COMPANY']]['TOTAL'] += $value['CNT'] ;
            }
            else
            {
                $list[$value['TEL_COMPANY']]['TOTAL'] = $value['CNT'] ;
            }
        }

        return $list ;
    }
}
?>