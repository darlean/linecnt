<?php
class S_Bank_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function get_bank_list()
	{
		$this->db->select('BANK_CD, BANK_NAME') ;
		$this->db->where('ISUSE', 'O') ;

		$result = $this->db->get('S_BANK')->result_array() ;

		$list = array() ;
		foreach ($result as $key => $row) 
		{
			$list[$row['BANK_CD']] = $row['BANK_NAME'] ;
		}

		return $list ;
	}
}
?>