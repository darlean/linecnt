<?php
class HB_join_temp_line_model extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

    function insert($in_userid)
    {
        $data = array(
            'USERID'         => $in_userid,
            'TEMP_LINE'      => $this->input->post('TEMP_LINE')
        ) ;

        return $this->db->insert('HB_JOIN_TEMP_LINE', $data) ;
    }

    function list_result($sfl, $stx, $limit, $offset)
    {
        $this->_get_search_cache($sfl, $stx);

        $this->db->limit($limit, $offset) ;

        $select = "A.USERID, A.TEMP_LINE, TO_CHAR(A.JOIN_TIME, 'YYYY-MM-DD HH24:MI:SS') AS JOIN_TIME, PKG_CRYPTO_FIELD.get('USERNAME', B.USERNAME, ,'9','".G_CRYPTO."') as USERNAME" ;

        $this->db->select($select, false) ;
        $this->db->join('D_MEMBER B', 'B.USERID = A.USERID') ;
        $this->db->order_by('A.JOIN_TIME', 'DESC') ;

        return $this->db->get('HB_JOIN_TEMP_LINE A')->result_array() ;
    }

    function list_count($sfl, $stx)
    {
        $this->_get_search_cache($sfl, $stx);

        return $this->db->count_all_results('HB_JOIN_TEMP_LINE A') ;
    }

    // 검색 구문을 얻는다.
    function _get_search_cache($search_field, $search_text)
    {
        if (!$search_field || !$search_text)
            return FALSE;

        $search_field = $search_field ;

        $where = $this->db->protect_identifiers($search_field)." LIKE '%".$search_text."%'" ;

        $this->db->where($where, null, FALSE);
    }
}
?>