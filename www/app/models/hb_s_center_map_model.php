<?php
class HB_s_center_map_model extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

	function insert($in_img_path) 
	{	
		$data = array(			
			'CENTER_CD' 				=> $this->input->post('CENTER_CD'),
			'SEARCHCOORD' 				=> $this->input->post('SEARCHCOORD'),
			'QUERY' 				    => $this->input->post('QUERY'),
			'LNG' 				        => $this->input->post('LNG'),
			'MPX' 			            => $this->input->post('MPX'),
			'LAT' 				        => $this->input->post('LAT'),
            'PRINT_KEY'                 => $this->input->post('PRINT_KEY'),
		);

		return $this->db->insert("HB_S_CENTER_MAP", $data) ;
	}

	function update($in_center_cd)
	{	
		$data = array(
            'SEARCHCOORD' 				=> $this->input->post('SEARCHCOORD'),
            'QUERY' 				    => $this->input->post('QUERY'),
            'LNG' 				        => $this->input->post('LNG'),
            'MPX' 			            => $this->input->post('MPX'),
            'LAT' 				        => $this->input->post('LAT'),
            'PRINT_KEY'                 => $this->input->post('PRINT_KEY'),
		);

		$this->db->where('CENTER_CD', $in_center_cd) ;
        return $this->db->update('HB_S_CENTER_MAP', $data) ;
	}

	function del($in_center_cd)
	{
		$this->db->where('CENTER_CD', $in_center_cd);
		return $this->db->delete('HB_S_CENTER_MAP') ;
	}

	function list_result($limit = -1, $offset = -1)
    {
        if ( $limit != -1 && $offset != -1) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->get('HB_S_CENTER_MAP')->result_array() ;
    }

    function list_count()
    {
        return $this->db->count_all_results('HB_S_CENTER_MAP') ;
    }

    function get_map_info($in_center_cd)
    {
    	$this->db->where('CENTER_CD', $in_center_cd) ;

    	return $this->db->get('HB_S_CENTER_MAP')->row_array() ;
    }
}
?>