<?php
class HB_pdt_link_model extends CI_Model
{
    function __construct() {
        parent::__construct();
    }

    function list_count($in_vender)
    {
        $this->db->where('TEL_COMPANY', $in_vender) ;
        $this->db->where('TYPE_CD', '2502') ;
        $this->db->where('ISUSE', 'O') ;

        return $this->db->count_all_results('P_PDTMASTER') ;
    }

    function list_result($limit, $offset, $in_vender)
    {
        $this->db->limit($limit, $offset) ;

        $this->db->select('A.PDT_NAME, A.PDT_CD, B.INSERT_LINK_TYPE, B.NEW_LINK_PATH, B.TR_LINK_PATH, B.RE_LINK_PATH') ;
        $this->db->from('P_PDTMASTER A') ;
        $this->db->join('HB_PDT_LINK B', 'A.PDT_CD = B.PDT_CD', 'left') ;
        
        $this->db->where('A.TEL_COMPANY', $in_vender) ;
        $this->db->where('A.TYPE_CD', '2502') ;
        $this->db->where('A.ISUSE', 'O') ;

        $this->db->order_by("A.PDT_CD", "ASC") ;

        return $this->db->get()->result_array() ;
    }

    function update($data)
    {
        $this->db->where('PDT_CD', $data['PDT_CD']) ;
        $CNT = $this->db->count_all_results('HB_PDT_LINK') ;

        if ( $CNT > 0 )
        {
            $this->db->where('PDT_CD', $data['PDT_CD']) ;
            return $this->db->update('HB_PDT_LINK', $data) ;
        }
        else
        {
            return $this->db->insert('HB_PDT_LINK', $data) ;
        }
    }
}
?>