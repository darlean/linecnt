<?php
class S_Rank_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function get_rank_list($bSimple=false)
	{
		$this->db->select('RANK_CD, RANK_NAME, FULL_NAME') ;
		$result = $this->db->get('S_RANK')->result_array() ;

		$list = array() ;
		foreach ($result as $key => $row) 
		{
			if ( $row['RANK_CD'] != 1 && $row['RANK_CD'] != 5 && $row['RANK_CD'] != 11 )
			{
				if ( $bSimple )
				{
					$list[$row['RANK_CD']] = $row['RANK_NAME'] ;
				}
				else
				{
					$list[$row['RANK_CD']] = $row['FULL_NAME'] ;
				}
			}
		}

		return $list ;
	}
}
?>