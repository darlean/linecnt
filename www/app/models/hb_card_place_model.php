<?php
class HB_card_place_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function insert($in_card_place) 
	{	
		$sql = "INSERT INTO HB_CARD_PLACE (CDP_ID, CDP_NAME) VALUES (SEQ_HB_CARD_PLACE.NEXTVAL, '".$in_card_place."')" ;

		return $this->db->query($sql) ;
	}

	function del($in_cdp_id)
	{
		$this->db->where('CDP_ID', $in_cdp_id);
		return $this->db->delete('HB_CARD_PLACE') ;		
	}

	function get_card_place_list()
    {   
    	$this->db->order_by('CDP_ID', 'asc') ;
    	
        return $this->db->get('HB_CARD_PLACE')->result_array() ;
    }

	function update($in_cdp_id, $in_card_place) 
	{	
		$this->db->where('CDP_ID', $in_cdp_id);
		return $this->db->update('HB_CARD_PLACE', array('CDP_NAME' => $in_card_place)) ;		
	}    
}
?>