<?php
class HB_temp_order_wired extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

	function insert($in_stockprice_info, $in_point_info)
	{
		$data = array(
		    'PDT_CD'       		=> $_POST["PDT_CD"],
	        'PTP_ID'     		=> $_POST["PTP_ID"],
	        'USERID'      		=> $_POST["USERID"],
	        'ORD_NAME'     		=> $_POST["ORD_NAME"],
	        'ORD_BIRTHDAY'  	=> $_POST["ORD_BIRTHDAY"],
			'ORD_MOBILE'    	=> $_POST["ORD_MOBILE"],
	        'ORD_TEL'     		=> $_POST["ORD_TEL"],
	        'ORD_EMAIL'    		=> $_POST["ORD_EMAIL"],
	        'INS_ZIPCODE'   	=> $_POST["INS_ZIPCODE"],
	        'INS_ADDR1'     	=> $_POST["INS_ADDR1"],
	        'INS_ADDR2' 		=> $_POST["INS_ADDR2"],
	        'INS_DATE' 			=> $_POST["INS_DATE"],
	        'AUTO_TRANS_TYPE'   => $_POST["AUTO_TRANS_TYPE"],
	        'DEPOSITOR'		 	=> $_POST["DEPOSITOR"],
	        'DEP_BIRTHDAY'	 	=> $_POST["DEP_BIRTHDAY"],
	        'BANK_CD'	 		=> $_POST["BANK_CD"],
			'BANK_ACCOUNT'	 	=> $_POST["BANK_ACCOUNT"],
			'CARD_CD'	 		=> $_POST["CARD_CD"],
			'CARD_ACCOUNT'	 	=> $_POST["CARD_ACCOUNT"],
			'CARD_VALID_DATE'	=> $_POST["CARD_VALID_DATE"],
			'ORD_MSG'	 		=> $_POST["ORD_MSG"],
			'CENTER_CD'	 		=> $_POST["CENTER_CD"],
			'RECEPTION_ID'	 	=> $_POST["RECEPTION_ID"],
			'PRICE'				=> $in_stockprice_info->PRICE,
			'VAT'				=> $in_stockprice_info->VAT,
			'AMT'				=> $in_stockprice_info->AMT,
			'POINT1'			=> $in_point_info->POINT1,
			'POINT2'			=> $in_point_info->POINT2,
			'POINT3'			=> $in_point_info->POINT3,
			'POINT4'			=> $in_point_info->POINT4,
	        ) ;

		$result->STATUS = $this->db->insert('HB_TEMP_ORDER_WIRED', $data) ;
		$result->NEW_ORDERID = "" ;
		$result->MESSAGE = $_POST ;

		if ( $result->STATUS )
		{
			$result->STATUS = 1 ;
			$rst = $this->db->query("SELECT HB_TEMP_ORDER_WIRED_SEQ2.currval as IDX FROM dual")->row() ;
			
			if ( $rst )
			{
				$result->NEW_ORDERID = $rst->IDX ;
			}
		}	
		else
		{
			$result->STATUS = 0 ;
			$result->MESSAGE = "유선 상품 신청이 실패하였습니다. 잠시 후 다시 시도해 주세요." ;
		}

		return $result ;
	}

	function list_count($sfl, $stx, $vender, $state, $type = "")
	{
		$this->_get_search_cache($sfl, $stx);

		// 기간 검색
		$this->_get_search_date_range() ;

		$this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD') ;
		$this->db->join('D_MEMBER C', 'C.USERID = A.USERID') ;

        $this->db->where('A.STATE_CD != ', '6', false) ;

		if ( $vender != '' )
		{
			$this->db->where('B.TEL_COMPANY', $vender) ;
		}

		if ( $state != '' )
		{
			$this->db->where('A.STATE_CD', $state) ;
		}

		$USERID = ( $this->input->post('USERID') != '' ) ? $this->input->post('USERID') : $this->session->userdata('ss_mb_id') ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
		{
			$this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD')) ;
		}
		else if ( !IS_MANAGER )
		{
		    if ( $type == "reception" )
            {
                $this->db->where('A.RECEPTION_ID', $USERID) ;
            }
            else
            {
                $this->db->where('A.USERID', $USERID) ;
            }
		}

		return $this->db->count_all_results('HB_TEMP_ORDER_WIRED A') ;
	}

	function list_result($sst, $sod, $sfl, $stx, $limit, $offset, $vender, $state, $type = "")
	{
		$this->_get_search_cache($sfl, $stx);

		// 기간 검색
		$this->_get_search_date_range() ;

		if ($sst && $sod)
		{
			$sst = ($sst == 'TMP_ORDNO') ? 'A.TMP_ORDNO' : $sst ;

			$this->db->order_by($sst, $sod);
		}

		$this->db->limit($limit, $offset) ;

		$select = "A.TMP_ORDNO, A.USERID, A.ORD_NAME, A.ORD_TEL, A.ORD_MSG, A.CENTER_CD, TO_CHAR(A.ORD_DATE, 'YYYY-MM-DD HH24:MI:SS') AS ORD_DATE, A.STATE_CD,
				TO_CHAR(A.ORD_DATE, 'YYYYMMDD') AS ORD_DATE2, E.CP_NAME AS PRICE_KIND, (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.CENTER_CD) as CENTER_NAME,
				B.PDT_NAME, PKG_CRYPTO_FIELD.get('MOBILE', C.MOBILE, ,'9','".G_CRYPTO."') as MOBILE, PKG_CRYPTO_FIELD.get('USERNAME', C.USERNAME, ,'9','".G_CRYPTO."') as USERNAME" ;

		$this->db->select( $select, false ) ;

		$this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD') ;
		$this->db->join('D_MEMBER C', 'C.USERID = A.USERID') ;
		$this->db->join('P_PDTTELPRICE D', 'A.PTP_ID = D.PTP_ID') ;
		$this->db->join('P_CALLING_PLAN E', 'E.CP_CD = D.PRICE_KIND', 'left') ;

        $this->db->where('A.STATE_CD != ', '6', false) ;

		if ( $vender != '' )
		{
			$this->db->where('B.TEL_COMPANY', $vender) ;
		}

		if ( $state != '' )
		{
			$this->db->where('A.STATE_CD', $state) ;
		}

        $USERID = ( $this->input->post('USERID') != '' ) ? $this->input->post('USERID') : $this->session->userdata('ss_mb_id') ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD')) ;
        }
        else if ( !IS_MANAGER && !IS_CARD_AGENCY )
        {
            if ( $type == "reception" )
            {
                $this->db->where('A.RECEPTION_ID', $USERID) ;
            }
            else
            {
                $this->db->where('A.USERID', $USERID) ;
            }
        }

		return $this->db->get('HB_TEMP_ORDER_WIRED A')->result_array() ;
	}

	// 검색 구문을 얻는다.
	function _get_search_cache($search_field, $search_text)
	{
		if (!$search_field || !$search_text)
			return FALSE;

		$search_field = $search_field ;
		$where = '' ;

		if ( $search_field == 'USERNAME' )
		{
			$where .= "PKG_CRYPTO_FIELD.get('USERNAME', C.USERNAME, '9','".G_CRYPTO."') LIKE ".$this->db->escape('%'.$search_text.'%');
		}
		else
		{
			$where .= $this->db->protect_identifiers($search_field) . " LIKE '%" . $search_text . "%'";
		}

		$this->db->where($where, null, FALSE);
	}

	// 기간 검색
	function _get_search_date_range()
	{
		$sdt = $this->input->get('sdt') ;
		$start_date = $this->input->get('start');   // 시작날짜
		$finish_date = $this->input->get('finish'); // 종료날짜

		if ( $start_date != '' )
		{
			$this->db->where("TO_CHAR(".$sdt.", 'YYYYMMDD') >= ", "'".$start_date."'", false) ;
		}

		if ( $finish_date != '' )
		{
			$Y = substr($finish_date, 0, 4) ;
			$M = substr($finish_date, 4, 2) ;
			$D = substr($finish_date, -2) ;
			$finish = date("Ymd", mktime(0, 0, 0, $M, $D + 1, $Y)) ;

			$this->db->where("TO_CHAR(".$sdt.", 'YYYYMMDD') < ", "'".$finish."'", false) ;
		}
	}

	function update($in_ord_no, $in_sql)
	{
		$this->db->where('TMP_ORDNO', $in_ord_no) ;
		return $this->db->update('HB_TEMP_ORDER_WIRED', $in_sql) ;
	}

    function delete($in_ord_no)
    {
        $this->db->where('TMP_ORDNO', $in_ord_no) ;
        return $this->db->delete('HB_TEMP_ORDER_WIRED') ;
    }

	function get_order_info($in_ord_no)
	{
		$select = "A.TMP_ORDNO, A.ORD_NAME AS REG_NAME, A.ORD_BIRTHDAY AS REG_BIRTHDAY, A.ORD_MOBILE AS REG_MOBILE, A.ORD_TEL AS REG_TEL, A.INS_ZIPCODE, A.INS_ADDR1, A.INS_ADDR2, A.INS_DATE,
					(SELECT PKG_CRYPTO_FIELD.get('USERNAME', USERNAME,'9','".G_CRYPTO."') FROM D_MEMBER WHERE USERID = A.RECEPTION_ID) AS RECEPTION_NAME, A.RECEPTION_ID, A.POINT2,
					A.DEPOSITOR, A.DEP_BIRTHDAY, A.CARD_VALID_DATE, A.ORD_EMAIL AS EMAIL, A.ORD_MSG, A.PDT_CD, A.PTP_ID, TO_CHAR(A.ORD_DATE, 'YYYY-MM-DD HH24:MI:SS') AS ORD_DATE,
					DECODE(A.AUTO_TRANS_TYPE, '1', '통장자동이체', '카드자동이체') AS AUTO_TRANS_TYPE,	
					DECODE(A.AUTO_TRANS_TYPE, '1', (SELECT BANK_NAME FROM S_BANK WHERE BANK_CD = A.BANK_CD), 
					(SELECT CARD_NAME FROM S_CARD WHERE CARD_CD = A.CARD_CD)) AS PAY_NAME,
					DECODE(A.AUTO_TRANS_TYPE, '1', A.BANK_ACCOUNT, A.CARD_ACCOUNT) AS PAY_ACCOUNT,
					(SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.CENTER_CD) as CENTER_NAME,
                   	B.PDT_NAME, C.IMG_PATH, E.CP_NAME AS PRICE_KIND" ;

		$this->db->select($select, false) ;

		$this->db->from('HB_TEMP_ORDER_WIRED A') ;
		$this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD') ;
		$this->db->join('P_PDTIMAGE C', 'A.PDT_CD = C.PDT_CD', 'left') ;
		$this->db->join('P_PDTTELPRICE D', 'A.PTP_ID = D.PTP_ID') ;
		$this->db->join('P_CALLING_PLAN E', 'E.CP_CD = D.PRICE_KIND', 'left') ;

		$this->db->where('A.TMP_ORDNO', $in_ord_no) ;

		return $this->db->get()->row_array() ;
	}

	function get_temp_order_info($in_ord_no)
	{
		$this->db->where('TMP_ORDNO', $in_ord_no) ;

		return $this->db->get('HB_TEMP_ORDER_WIRED')->row_array() ;
	}

    function execute_procedure($p_name, $params, $bCommit = true)
    {
        $pramList = "";

        foreach ($params as $key => $value)
        {
            if($pramList!="") $pramList .= ", " ;
            $pramList .= ":" . $key ;
        }

        $query = "begin ".$p_name."(" . $pramList . "); end;" ;

        $stmt = oci_parse($this->db->conn_id, $query) ;

        foreach ($params as $key => $value)
        {
            if($key=="STATUS" || $key=="MESSAGE" || substr($key,0,3)=="NEW")
                oci_bind_by_name($stmt, ":" . $key, $params->$key, 100) ;
            else
                oci_bind_by_name($stmt, ":" . $key, $params->$key) ;
        }

        if(!ociexecute($stmt, OCI_DEFAULT))
        {
            oci_rollback($this->db->conn_id) ;
            die ;
        }

        if ( $params->STATUS == '1' && $bCommit )
        {
            oci_commit($this->db->conn_id) ;
        }

        return $params ;
    }

    function create_order($order_info, $in_pdt_master_info)
    {
        $params->USERID         = $order_info['USERID'] ;
        $params->RECEPTION_ID   = $order_info['RECEPTION_ID'] ;
        $params->ORD_KIND   = '2601'; // 주문구분(초도, 재구매, 초도+재구매 등), S_REAND.REASON_KIND:26
        $params->AGENCY_CD  = $in_pdt_master_info->TEL_COMPANY ;
        $params->CENTER_CD  = $order_info['CENTER_CD'] ;
        $params->START_CENTER = '9990' ;
        $params->PRICE      = (int)$order_info['PRICE'] ;
        $params->VAT        = (int)$order_info['VAT'] ;
        $params->AMT        = (int)$order_info['AMT'] ;
        $params->PV1        = (int)$order_info['POINT1'] ;
        $params->PV2        = (int)$order_info['POINT2'] ;
        $params->PV3        = (int)$order_info['POINT3'] ;
        $params->PV4        = (int)$order_info['POINT4'] ;
        $params->CRYPTO     = G_CRYPTO ;
        $params->NEW_ORDER  = "" ;
        $params->STATUS     = "" ;
        $params->MESSAGE    = "" ;

        return $this->execute_procedure("PKG_WEB.CREATE_ORDER", $params, false) ;
    }

    function create_detail($in_ord_no, $order_info, $in_pdt_master_info)
    {
        $params->ORDNO          = $in_ord_no ;
        $params->PDT_CD         = $order_info['PDT_CD'] ;
        $params->PDTDETAIL_NO   = $in_pdt_master_info->PDTDETAIL_NO ;
        $params->QTY            = 1 ;
        $params->PRICE          = (int)$order_info['PRICE'] ;
        $params->VAT            = (int)$order_info['VAT'] ;
        $params->AMT            = (int)$order_info['AMT'] ;
        $params->PV1        	= (int)$order_info['POINT1'] ;
        $params->PV2        	= (int)$order_info['POINT2'] ;
        $params->PV3        	= (int)$order_info['POINT3'] ;
        $params->PV4        	= (int)$order_info['POINT4'] ;
        $params->COMPANY_CD     = $in_pdt_master_info->COMPANY_CD ;
        $params->RECEIPT_KIND   = '0702' ;
        $params->BARCODE        = '' ;
        $params->IS_TELPRODUCT  = $in_pdt_master_info->IS_TELPRODUCT ; // 미출고상품유무(PDTMASTER.IS_TELPRODUCT)
        $params->WORK_USER      = $order_info['USERID'] ;
        $params->NEW_ORDPDT     = "" ;
        $params->STATUS         = "" ;
        $params->MESSAGE        = "" ;

        return $this->execute_procedure("PKG_ORDER_TEL.CREATE_DETAIL", $params, false) ;
    }

    function create_delivery($in_ord_no, $order_info)
    {
        $params->ORDNO          = $in_ord_no ;
        $params->ORD_TEL        = $order_info['ORD_TEL'] ;
        $params->ORD_MOBILE     = $order_info['ORD_MOBILE'] ;
        $params->ORD_EMAIL      = $order_info['ORD_EMAIL'] ;
        $params->RECV_NAME      = $order_info['ORD_TEL'] ;
        $params->RECV_TEL       = $order_info['ORD_TEL'] ;
        $params->RECV_MOBILE    = $order_info['ORD_MOBILE'] ;
        $params->RECV_ZIPCODE   = $order_info['INS_ZIPCODE'] ;
        $params->RECV_ADDR1     = $order_info['INS_ADDR1'] ;
        $params->RECV_ADDR2     = $order_info['INS_ADDR2'] ;
        $params->ORD_MSG        = $order_info['ORD_MSG'] ;
        $params->CRYPTO         = G_CRYPTO ;
        $params->STATUS         = "" ;
        $params->MESSAGE        = "" ;

        return $this->execute_procedure("PKG_ORDER_TEL.CREATE_DELIVERY", $params, false) ;
    }

    // 주문 유선상품 신청
    function save_ctel($in_ord_no, $order_info, $in_pdt_master_info, $in_pdt_telprice_info)
    {
        $params->ORDNO                  = $in_ord_no ;
        $params->TEL_COMPANY            = $in_pdt_master_info->TEL_COMPANY ;
        $params->PRICE_KIND             = $in_pdt_telprice_info->PRICE_KIND ;
        $params->REG_KIND               = $in_pdt_telprice_info->REG_KIND ;
        $params->REG_NAME               = $order_info['ORD_NAME'] ;
        $params->REG_BIRTHDAY           = $order_info['ORD_BIRTHDAY'] ;
        $params->REG_MOBILE             = $order_info['ORD_MOBILE'] ;
        $params->REG_TEL                = $order_info['ORD_TEL'] ;
        $params->INS_ZIPCODE            = $order_info['INS_ZIPCODE'] ;
        $params->INS_ADDR1              = $order_info['INS_ADDR1'] ;
        $params->INS_ADDR2              = $order_info['INS_ADDR2'] ;
        $params->INS_DATE               = $order_info['INS_DATE'] ;
        $params->AUTO_TRANS_TYPE        = $order_info['AUTO_TRANS_TYPE'] ;
        $params->DEPOSITOR              = $order_info['DEPOSITOR'] ;
        $params->DEP_BIRTHDAY           = $order_info['DEP_BIRTHDAY'] ;
        $params->BANK_CD                = $order_info['BANK_CD'] ;
        $params->BANK_ACCOUNT           = $order_info['BANK_ACCOUNT'] ;
        $params->CARD_CD                = $order_info['CARD_CD'] ;
        $params->CARD_ACCOUNT           = $order_info['CARD_ACCOUNT'] ;
        $params->CARD_VALID_DATE        = $order_info['CARD_VALID_DATE'] ;
        $params->EMAIL                  = $order_info['ORD_EMAIL'] ;
        $params->REAL_OPEN_DT4          = "" ;
        $params->REAL_OPEN_AGENCY       = "" ;
        $params->WORK_USER              = $this->session->userdata('ss_mb_id') ;
        $params->CRYPTO                 = G_CRYPTO ;
        $params->NEW_CLASSIFY           = 1 ;
        $params->STATUS                 = "" ;
        $params->MESSAGE                = "" ;

        return $this->execute_procedure("PKG_ORDER_TEL.SAVE_CTEL", $params, true) ;
    }
}
?>