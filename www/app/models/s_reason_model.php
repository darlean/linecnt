<?php
class S_Reason_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function get_reason_list($in_reason_kind = '', $in_isuse = false)
	{
		$this->db->select('REASON_CD, REASON_NAME') ;

		if ( $in_reason_kind != '' )
		{
			$this->db->where('REASON_KIND', $in_reason_kind) ;
		}

		if ( $in_isuse )
        {
            $this->db->where('ISUSE', 'O') ;
        }

        $this->db->order_by('REASON_CD', 'ASC') ;

		$result = $this->db->get('S_REASON')->result_array() ;

		$list = array() ;
		foreach ($result as $key => $row) 
		{
			$list[$row['REASON_CD']] = $row['REASON_NAME'] ;			
		}

		return $list ;
	}

	function get_vender_list($bAllView = false, $bConsult = false)
	{
		$this->db->select('REASON_CD, REASON_NAME') ;
		$this->db->where('REASON_KIND', '32') ;

		if ( $bAllView == false )
		{
			$this->db->where('ISUSE', 'O') ;
		}

		$this->db->order_by('REASON_CD', 'DESC') ;

		$result = $this->db->get('S_REASON')->result_array() ;

		$list = array() ;
		foreach ($result as $key => $row) 
		{
            $list[$row['REASON_CD']] = $row['REASON_NAME'];
		}

        if ( $bAllView == false )
        {
		    $temp_vender = array('3203', '3202', '3201', '3207', '3208', '3209') ;

            $list2 = array() ;
            foreach ($temp_vender as $row)
            {
                $list2[$row] = $list[$row] ;
            }

		    return $list2 ;
        }

		return $list ;
	}

	function get_vender_list2($bAllView = false)
	{
		$this->db->select('REASON_CD, REASON_NAME') ;
		$this->db->where('REASON_KIND', '32') ;

		if ( $bAllView == false )
		{
			$this->db->where('ISUSE', 'O') ;
		}

		$this->db->order_by('REASON_CD', 'DESC') ;

		return $this->db->get('S_REASON')->result_array() ;
	}
}
?>