<?php
class Pay_week_n_detail_model extends CI_Model
{
    function __construct() {
        parent::__construct();
    }

    function get_n_detail_info($in_pay_date)
    {
        $select = "SUM(AMT_WCY_N) AS AMT_WCY_N, SUM(AMT_WCY_N_GD) AS AMT_WCY_N_GD, SUM(AMT_WCY_N_RB) AS AMT_WCY_N_RB, SUM(AMT_WCY_N_SP) AS AMT_WCY_N_SP, SUM(AMT_WCY_N_DD) AS AMT_WCY_N_DD, SUM(AMT_WCY_N_CW) AS AMT_WCY_N_CW, 
                   SUM(AMT_WCY_USE_CV) AS AMT_WCY_USE_CV, SUM(AMT_WCY_GRP_CV) AS AMT_WCY_GRP_CV, SUM(AMT_WCY_GRP_CV_GD) AS AMT_WCY_GRP_CV_GD, SUM(AMT_WCY_GRP_CV_RB) AS AMT_WCY_GRP_CV_RB, SUM(AMT_WCY_GRP_CV_SP) AS AMT_WCY_GRP_CV_SP, SUM(AMT_WCY_GRP_CV_DD) AS AMT_WCY_GRP_CV_DD, 
                   SUM(AMT_WCY_GRP_CV_CW) AS AMT_WCY_GRP_CV_CW" ;

        $this->db->select($select, false) ;
        $this->db->where('PAY_DATE', $in_pay_date) ; 

        return $this->db->get('PAY_WEEK_N_DETAIL')->row_array() ;
    }

    function get_n_detail_info_list($in_pay_date)
    {
        $this->db->where('PAY_DATE', $in_pay_date) ;

        return $this->db->get('PAY_WEEK_N_DETAIL')->result_array() ;
    }
}
?>