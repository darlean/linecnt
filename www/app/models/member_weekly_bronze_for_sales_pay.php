<?php
class Member_weekly_bronze_for_sales_pay extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
    }

    private function _get_weekly_acount($wa_id)
    {
        $this->db->where('wa_id', $wa_id);

        return $this->db->get('hb_weekly_account')->row_array();
    }

    private function _get_bronze_member_list()
    {
    	$this->db->select( 'hb_member.mb_no, hb_member.mb_name, hb_member.mb_id, hb_member_weekly_achievement.mbac_last_recommand_date' );
    	$this->db->where( 'mb_position', BRONZE );
    	$this->db->join( 'hb_member_weekly_achievement', 'hb_member_weekly_achievement.mb_no = hb_member.mb_no', 'left' );

    	return $this->db->get('hb_member')->result_array();
    }

    private function _insert_pay($mb_no, $mb_id, $wa_id, $or_id, $payment_cv, $payment_type, $payment_state )
    {
        $this->load->config('cf_weekly_account') ;
        $cf_payment_state = $this->config->item('payment_state') ;

        $in_data = array( 
                            'mb_id'                 => $mb_id, 
                            'mb_no'                 => $mb_no,
                            'wa_id'                 => $wa_id,
                            'mbwa_payment_amount'   => $payment_cv,
                            'mbwa_payment_type'     => $payment_type,
                            'or_id'                 => $or_id,
                            'mbwa_payment_state'    => $payment_state,
                            'mbwa_payment_result'   => $cf_payment_state[$payment_state],
                        );      

        $this->db->insert('hb_member_weekly_account', $in_data );

        return $this->db->insert_id();
    }

    private function _update_bfs_for_weekly_account($wa_id, $payment_cv, $non_payment_cv)
    {
        $in_data = array( 
                            'wa_bfs_payment_cv'          => $payment_cv,
                            'wa_bfs_non_payment_cv'      => $non_payment_cv,
                        );

             
        $this->db->where('wa_id', $wa_id) ;

        return $this->db->update('hb_weekly_account', $in_data );
    }

    private function _get_sum_bfs_payment_cv($wa_id, $payment_type)
    {
        $this->db->where('wa_id', $wa_id) ;
        $this->db->where('mbwa_payment_type', $payment_type);
        $this->db->where('mbwa_payment_state', PAYMENT_SUCCESS);
        
        $this->db->select_sum('mbwa_payment_amount', 'sum_payment');

        $query = $this->db->get('hb_member_weekly_account');

        return $query->row_array();   
    }

    function update($wa_id)
    {
    	$weekly_account = $this->_get_weekly_acount($wa_id);

    	if( FALSE == $weekly_account )
    		return FALSE;

    	$total_cv = $weekly_account['wa_total_cv'] * BRONZE_FOR_SALES_PAYMENT_RATIO;

    	$member_list = $this->_get_bronze_member_list();
    	$member_count = count($member_list);

        $total_payment_cv = 0;

    	if( $member_count  )
        {
        	$payment_cv = $total_cv / $member_count;
        	$payment_cv = floor($payment_cv/10)*10;

        	foreach ($member_list as $mb)
            {
            	// 추천 날짜가 지났음
            	if( $mb['mbac_last_recommand_date'] >= RECOMMAND_DAY_LIMIT )
            	{
            		$this->_insert_pay($mb['mb_no'], $mb['mb_id'], $wa_id, NULL, $payment_cv, SUPPORT_SALES_FOR_BRONZE, PAYMENT_RECOMMAND_DATE_EXPIRED );
            	}
            	else
            	{
            		$this->_insert_pay($mb['mb_no'], $mb['mb_id'], $wa_id, NULL, $payment_cv, SUPPORT_SALES_FOR_BRONZE, PAYMENT_SUCCESS );

            		$total_payment_cv += $payment_cv;
            	}
            }
        }

        $sum_payment = $this->_get_sum_bfs_payment_cv( $wa_id, SUPPORT_SALES_FOR_BRONZE );
        if( FALSE == $sum_payment )
        {
            $sum_payment = array();
            $sum_payment['sum_payment'] = 0;
        }

        // DB에 지급 금액과 코드상 지급금액과 같은지 체크
        if( $sum_payment['sum_payment'] != $total_payment_cv )
            return FALSE;

        // 지급 금액이 정산했던 금액보다 크면 실패
        if( $sum_payment['sum_payment'] > $weekly_account['wa_bfs_cv'] )
            return FALSE;

        // 지급 금액이 총 재원보다 많으면 실패
        if( $total_payment_cv > $total_cv )
        	return FALSE;

        if( FALSE == $this->_update_bfs_for_weekly_account($wa_id, $sum_payment['sum_payment'], $weekly_account['wa_bfs_cv'] - $sum_payment['sum_payment'] ) )
        {
            return FALSE;
        }

    	return TRUE;
    }
}