<?php
class S_CenterUser_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function is_center_head($in_userid)
    {
        $this->db->select('CENTER_CD') ;
        $this->db->where('HEAD_ID', $in_userid) ;

        $result = $this->db->get('S_CENTERUSER')->row_array() ;

        return isset($result['CENTER_CD']) ? $result['CENTER_CD'] : '' ;
    }
}
?>