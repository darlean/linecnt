<?php
class HB_credit_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function insert() 
	{	
		$result = array() ;
		$result['CREDIT_IDX'] = -1 ;

		if ( $this->get_my_request_credit_limit($_POST["USERID"]) >= 10 )
		{
			$result['rst_code'] = false ;			
			$result['msg'] = '오늘은 더 이상 신용 조회를 이용 하실 수 없습니다.' ;

			return $result ;
		}

		$data = array(
		    'TYPE_NEW'       => $_POST["TYPE_NEW"],
	        'TYPE_OTHER'     => $_POST["TYPE_OTHER"],
	        'TYPE_HOME'      => $_POST["TYPE_HOME"],
	        'TYPE_LEGAL'     => $_POST["TYPE_LEGAL"],
	        'USERNAME'       => $_POST["USERNAME"],
	        'MOBILE'         => $_POST["MOBILE"],
	        'LEGAL_NAME'     => $_POST["LEGAL_NAME"],
	        'LEGAL_JUMIN'    => $_POST["LEGAL_JUMIN"],
	        'TYPE_FOREIGN'   => $_POST["TYPE_FOREIGN"],
	        'REG_NUM'        => $_POST["REG_NUM"],
	        'FOREIGN_DATE_S' => $_POST["FOREIGN_DATE_S"],
	        'FOREIGN_DATE_F' => $_POST["FOREIGN_DATE_F"],
	        'MEMO'           => $_POST["MEMO"],
	        'USERID'		 => $_POST["USERID"],
	        'USED_VENDER'	 => $_POST["USED_VENDER"],
	        'WILL_VENDER'	 => $_POST["WILL_VENDER"],
	        ) ;

		$result['rst_code'] = $this->db->insert('HB_CREDIT', $data) ;
		$result['subject'] = $_POST["USERNAME"].' 님께서 신용조회를 신청 하셨습니다.' ;
		$result['msg'] = '' ;

		if ( $result['rst_code'] )
		{
			$rst = $this->db->query("SELECT HB_CREDIT_SEQ3.currval as idx FROM dual")->row() ;		
			
			if ( $rst )
			{
				$result['CREDIT_IDX'] = $rst->IDX ;
			}
		}	
		else
		{
			$result['msg'] = "신용조회 신청에 실패하였습니다. 잠시 후 다시 시도해 주세요." ;
		}

		return $result ;
	}

	function del($in_credit_idx)
	{
		$this->db->where('CREDIT_IDX', $in_credit_idx);
		return $this->db->delete('HB_CREDIT') ;		
	}

	function insert_credit_result( $in_data )
    {
		return $this->db->insert('HB_CREDIT_RESULT', $in_data) ;		
    }

    function update_credit_result( $in_data )
    {
    	$this->db->where('RESULT_IDX', $in_data['RESULT_IDX'] ) ;
		return $this->db->update('HB_CREDIT_RESULT', $in_data) ;		
    }

	function list_count($sfl, $stx, $tab, $userid = '', $will_vender = '')
	{		
		$this->_get_search_cache($sfl, $stx);

		if ($userid != '')
		{
			$this->db->join('HB_CREDIT_RESULT B', 'A.CREDIT_IDX = B.RESULT_IDX', 'left') ;
			$this->db->where('A.USERID', $userid) ;
		}
		else
		{
			if ( $tab == '0' )
			{
				$this->db->join('HB_CREDIT_RESULT B', 'A.CREDIT_IDX = B.RESULT_IDX', 'left') ;
				$this->db->where('B.RESULT_IDX', NULL) ;
			}
			else
			{
				$this->db->join('HB_CREDIT_RESULT B', 'A.CREDIT_IDX = B.RESULT_IDX') ;
			}
		}

		if($this->session->userdata('ss_mb_id') == 'ANYPLUS')
		{
			$this->db->where('A.WILL_VENDER', '2') ;
		}

		if ( $will_vender != '' )
		{
			$this->db->where('A.WILL_VENDER', $will_vender) ;	
		}

		return $this->db->count_all_results('HB_CREDIT A') ;
	}

	function list_result($sfl, $stx, $tab, $limit, $offset, $userid = '', $will_vender = '')
	{
		$this->_get_search_cache($sfl, $stx);

		$this->db->select( "A.*, B.RESULT_IDX, TO_CHAR(A.REQUEST_DATE, 'YYYY-MM-DD HH24:MI:SS') AS REQ_DATE, TO_CHAR(B.RESULT_DATE, 'YYYY-MM-DD HH24:MI:SS') AS RES_DATE, PKG_CRYPTO_FIELD.get('C.USERNAME',C.USERNAME,'9','".G_CRYPTO."') AS MEMBERNAME", false ) ;

		if ($userid != '')
		{
			$this->db->join('HB_CREDIT_RESULT B', 'A.CREDIT_IDX = B.RESULT_IDX', 'left') ;
			$this->db->where('A.USERID', $userid) ;
		}
		else
		{
			if ( $tab == '0' )
			{
				$this->db->join('HB_CREDIT_RESULT B', 'A.CREDIT_IDX = B.RESULT_IDX', 'left') ;
				$this->db->where('B.RESULT_IDX', NULL) ;
			}
			else
			{
				$this->db->join('HB_CREDIT_RESULT B', 'A.CREDIT_IDX = B.RESULT_IDX') ;
			}
		}

		if($this->session->userdata('ss_mb_id') == 'ANYPLUS')
		{
			$this->db->where('A.WILL_VENDER', '2') ;
		}

		if ( $will_vender != '' )
		{
			$this->db->where('A.WILL_VENDER', $will_vender) ;	
		}

		$this->db->join('D_MEMBER C', 'A.USERID = C.USERID', 'left') ;

		$this->db->limit($limit, $offset) ;
		$this->db->order_by('A.CREDIT_IDX', 'DESC') ;

		return $this->db->get('HB_CREDIT A')->result_array() ;
	}

	// 검색 구문을 얻는다.
    function _get_search_cache($search_field, $search_text) 
    {        
        if (!$search_field || !$search_text)
            return FALSE;

        $search_field = $search_field ;

        $where = "A.".$this->db->protect_identifiers($search_field)." LIKE '%".$search_text."%'" ;

        $this->db->where($where, null, FALSE);
    }

	function get_credit_info($in_credit_idx)
	{
		$this->db->select("A.*, B.*, TO_CHAR(A.REQUEST_DATE, 'YYYY-MM-DD HH24:MI:SS') AS REQUEST_DATE, TO_CHAR(B.RESULT_DATE, 'YYYY-MM-DD HH24:MI:SS') AS RESULT_DATE", false) ;
		$this->db->join('HB_CREDIT_RESULT B', 'A.CREDIT_IDX = B.RESULT_IDX', 'left') ;
		$this->db->where("A.CREDIT_IDX", $in_credit_idx) ;
		
		return $this->db->get('HB_CREDIT A')->row_array() ;
	}

	function get_my_request_credit_limit($userid)
	{
		$this->db->where("TO_CHAR(REQUEST_DATE,'YYYYMMDD')", "TO_CHAR(SYSDATE,'YYYYMMDD')", false) ;
		$this->db->where('USERID', $userid) ;

		return $this->db->count_all_results('HB_CREDIT') ;
	}

	function completed_credit_today($userid, $username, $order_date)
	{		
		$year = substr($order_date, 0, 4) ;
		$month = substr($order_date, 4, 2) ;
		$day = substr($order_date, -2) ;

		$date = date("Ymd", mktime(0, 0, 0, $month, $day - 3, $year)) ;		

		$this->db->join('HB_CREDIT_RESULT B', 'A.CREDIT_IDX = B.RESULT_IDX', 'left') ;
		$this->db->where('A.USERID', $userid) ;
		$this->db->where('A.USERNAME', $username) ;
		$this->db->where("TO_CHAR(B.RESULT_DATE, 'YYYYMMDD') >=", $date, false) ; 		
		$this->db->where('B.RESULT_OK', 'O') ;

		return $this->db->count_all_results('HB_CREDIT A') ;
	}

	function delete_credit_last_day($in_last_day)
	{
		$count = -1 ;
		$date = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - $in_last_day, date("Y"))) ;   		

		$this->db->where("TO_CHAR(REQUEST_DATE, 'YYYYMMDD') < ", $date, false) ; 		
		$count = $this->db->count_all_results('HB_CREDIT') ;

		$this->db->where("TO_CHAR(REQUEST_DATE, 'YYYYMMDD') < ", $date, false) ; 		
		
		if( $this->db->delete('HB_CREDIT') )
		{
			$sql = "DELETE FROM (SELECT * FROM HB_CREDIT_RESULT A
					LEFT JOIN HB_CREDIT B ON A.RESULT_IDX = B.CREDIT_IDX
					WHERE B.CREDIT_IDX IS NULL AND TO_CHAR(A.RESULT_DATE, 'YYYYMMDD') < '".$date."')" ;

			return $count ;
		}

		return $count ;
	}
}
?>