<?php
class Board_notice_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}	

	function get_main_notice()
	{
		$sql = "
			SELECT A.*
			  FROM (SELECT BBS_NO, BBS_GROUP, BBS_LEVEL, BBS_SORT, BBS_ISNOTE, BBS_TITLE,
						   BBS_SEARCH, WORK_USER, to_char(WORK_DATE,'yyyy-mm-dd') as WORK_DATE
					  FROM BBS_BOARD_NOTICE
					 ORDER BY BBS_SORT, BBS_NO) A
			 WHERE ROWNUM <= 2
		";

		$result = $this->db->query($sql)->result_array() ;
		
		$list = array() ;
		$this->load->helper('textual');
		foreach($result as $i => $row) 
		{				
			$list[$i] = new stdClass();
			$list[$i]->href = RT_PATH.'/bbs/view/page/1?BBS_NO='.$row['BBS_NO'].'&BBS_ID=NOTICE' ;
			$list[$i]->subject = cut_str(get_text($row["BBS_TITLE"]	), 60) ;
			$list[$i]->date = $row['WORK_DATE'] ;
		}

		return $list ;
	}

	function get_popup_notice()
	{
		$this->db->select('wr_subject, wr_content') ;		
		$this->db->where('bo_table', 'POPUP') ;

		$result = $this->db->get('HB_WRITE')->result_array() ;
		
		foreach ($result as $key => $row) 
		{			
			if( isset( $row['wr_content'] ) )
			{
				$content_size = $row['wr_content']->size() ;
				$result[$key]['wr_content'] = $row['wr_content']->read( $content_size ) ;	
			}
		}

		return $result ;
	}
}
?>