<?php
class Order_state_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function insert($in_ord_no, $in_or_state, $in_upper_reg_no = '', $in_seller_id = '', $in_benefit_service = 'X', $in_add_service = 'X')
	{
		$this->db->insert('O_ORDERSTATE', array('STATE_CD' => $in_or_state, 'ORDNO' => $in_ord_no, 'UPPER_REG_NO' => $in_upper_reg_no, 'SELLER_ID' => $in_seller_id, 'BENEFIT_SERVICE' => $in_benefit_service, 'ADD_SERVICE' => $in_add_service)) ;
	}

	function update($in_ord_no, $in_or_state)
    {
        $sql = "MERGE INTO O_ORDERSTATE trg  
                USING (SELECT '$in_ord_no' as ORDNO, '$in_or_state' as STATE_CD FROM DUAL) src ON (src.ORDNO = trg.ORDNO)
                WHEN NOT MATCHED THEN 
                    INSERT(ORDNO, STATE_CD) VALUES (src.ORDNO, src.STATE_CD)
                WHEN MATCHED THEN 
               	UPDATE SET trg.STATE_CD = src.STATE_CD";

        return $this->db->query($sql);
    }

    function update_msg($in_ord_no, $in_sql)
    {
        $this->db->where('ORDNO', $in_ord_no) ;
        return $this->db->update('O_ORDERSTATE', $in_sql) ;
    }

    function get_ord_manager_msg($in_ord_no)
    {
        $this->db->where('ORDNO', $in_ord_no) ;

        $row = $this->db->get('O_ORDERSTATE')->row() ;

        return $row ? $row->ORD_MANAGER_MSG : "" ;
    }

    function request_open($in_ord_no)
    {
        $data['REQUEST_OPEN'] = 'O' ;

        $this->db->where('ORDNO', $in_ord_no) ;
        return $this->db->update('O_ORDERSTATE', $data) ;
    }
}
?>