<?php
class Board_comment_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function insert( $BBS_ID, $BBS_NO, $BBS_MEMO, $WORK_USER )
	{
		$sql = "
			INSERT INTO BBS_COMMENT_$BBS_ID (
				BBS_NO,
				BBS_SEQ,
				BBS_MEMO,
				WORK_USER,
				WORK_DATE
			) values (
				$BBS_NO,
				(select nvl(max(BBS_SEQ),0)+1 from BBS_COMMENT_$BBS_ID),
				'$BBS_MEMO',
				'$WORK_USER',
				sysdate
			)
		";

		$this->db->query($sql);
	}

	function delete( $BBS_ID, $BBS_NO, $BBS_SEQ )
	{
		$sql = "
			DELETE FROM BBS_COMMENT_$BBS_ID
			 WHERE BBS_NO = $BBS_NO
			   AND BBS_SEQ = $BBS_SEQ
		";

		$this->db->query($sql);
	}
}