<?php
class Product_image_upload_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function list_result($limit, $offset, $vender)
    {    
        $this->db->limit($limit, $offset) ;
    
        $this->db->select ( "/*+ index(P_PDTMASTER P_PDTMASTER_PK)*/ A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, B.IMG_PATH, C.IS_RECOMMAND", false ) ;
        $this->db->from ( "P_PDTMASTER A" ) ;
        
        $this->db->join( 'P_PDTIMAGE B', 'A.PDT_CD = B.PDT_CD', 'left') ;
        $this->db->join( 'P_PDTRECOMMAND C', 'A.PDT_CD = C.PDT_CD', 'left') ;

        if ( $vender == '2501' )
        {
            $this->db->where( 'A.TYPE_CD', '2501' ) ;
        }
        else
        {
            $this->db->where('TEL_COMPANY', $vender);
        }
        
        $this->db->where( 'A.ISUSE', 'O' ) ;
        $this->db->where( 'A.TYPE_CD != ', '2506' ) ;

        $this->db->group_by('A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, B.IMG_PATH, C.IS_RECOMMAND') ;
        $this->db->order_by('A.PDT_CD', 'desc') ;

        return $this->db->get()->result_array() ;
    }

    function list_count($vender) 
    {
        if ( $vender == '2501' )
        {
            $this->db->where( 'TYPE_CD', '2501' ) ;
        }
        else
        {
            $this->db->where('TEL_COMPANY', $vender);
        }

        $this->db->where( 'ISUSE', 'O' ) ;
        $this->db->where( 'TYPE_CD != ', '2506' ) ;
        
        return $this->db->count_all_results('P_PDTMASTER') ;
    } 

    function insert_product_image( $in_pdt_cd, $in_img_path )
    {
        //$sql = "INSERT INTO 'P_PDTIMAGE' ('PDT_CD', 'IMG_PATH') VALUES ('$in_pdt_cd', '$in_img_path') ON DUPLICATE KEY UPDATE 'IMG_PATH'='$in_img_path'";
        //$sql = "SELECT count( PDT_CD ) as CNT FROM P_PDTIMAGE WHERE PDT_CD='$in_pdt_cd'";
        //$count = $this->db->query($sql)->row_array() ;        //var_dump($count);

        //$sql = "UPDATE P_PDTIMAGE SET IMG_PATH='$in_img_path' WHERE PDT_CD = '$in_pdt_cd'";

        //var_dump($sql);
        //$this->db->query("UPDATE P_PDTIMAGE SET IMG_PATH='/data/product_image/product_image_1434264477.jpg' WHERE PDT_CD = '000000000000055'");

        //if( $count['CNT'] > 0 )
        //{
            //return $this->db->update( 'P_PDTIMAGE', array( 'IMG_PATH' => $in_img_path ), array( 'PDT_CD' => $in_pdt_cd ) );
        //}
        /*else
        {
            return $this->db->insert( 'P_PDTIMAGE', array( 'PDT_CD' => $in_pdt_cd, 'IMG_PATH' => $in_img_path ) );
        }*/

        //
        // 이 SQL 구문은 오라클에서 워닝을 내뱉는다.
        // CI 에서만의 문제인가? SQL Developer 에서는 제대로 동작한다.
        //
        $sql = "MERGE INTO P_PDTIMAGE trg  
                USING (SELECT '$in_pdt_cd' as PDT_CD, '$in_img_path' as IMG_PATH FROM DUAL) src ON (src.PDT_CD = trg.PDT_CD)
                WHEN NOT MATCHED THEN 
                    INSERT(PDT_CD, IMG_PATH) VALUES (src.PDT_CD, src.IMG_PATH)
                WHEN MATCHED THEN 
                    UPDATE SET trg.IMG_PATH = src.IMG_PATH";

        return $this->db->query($sql);
        //return $this->db->insert( 'P_PDTIMAGE', array( 'PDT_CD' => $in_pdt_cd, 'IMG_PATH' => $in_img_path ) );
    }

    function get_product_info($in_pdt_cd)
    {
        $this->db->select ( "/*+ index(P_PDTTELPRICE P_PDTTELPRICE_PK)*/ A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, B.IMG_PATH, B.DETAIL_IMG_PATH", false ) ;
        $this->db->from ( "P_PDTMASTER A" ) ;
        $this->db->join( 'P_PDTIMAGE B', 'A.PDT_CD = B.PDT_CD', 'left') ;        

        $this->db->where('A.PDT_CD', $in_pdt_cd) ;        

        return $this->db->get()->row_array() ;
    }

    function insert_detail_image( $in_pdt_cd, $in_img_path )
    {        
        $sql = "MERGE INTO P_PDTIMAGE trg  
                USING (SELECT '$in_pdt_cd' as PDT_CD, '$in_img_path' as DETAIL_IMG_PATH FROM DUAL) src ON (src.PDT_CD = trg.PDT_CD)
                WHEN NOT MATCHED THEN 
                    INSERT(PDT_CD, DETAIL_IMG_PATH) VALUES (src.PDT_CD, src.DETAIL_IMG_PATH)
                WHEN MATCHED THEN 
                    UPDATE SET trg.DETAIL_IMG_PATH = src.DETAIL_IMG_PATH";

        return $this->db->query($sql);        
    }
}