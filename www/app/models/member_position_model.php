<?php
class Member_position_model extends CI_Model 
{
    function __construct() {
        parent::__construct();
    }

    // 개통 완료 시 해당 회원 cv, pv set!!
    function set_mb_cv_pv($in_or_id)
    {
        $mb_no = $this->get_mb_no_by_or_id($in_or_id) ;

        $this->db->select('sum(op_commission_value) as mb_cv, sum(op_promotion_value) as mb_pv') ;
        $this->db->from('hb_order_for_policy') ;
        $this->db->join('hb_order', 'hb_order.or_id = hb_order_for_policy.or_id', 'left') ;
        $this->db->where(array('mb_no' => $mb_no, 'or_state' => '5')) ;

        $data = $this->db->get()->row_array() ;

        $this->db->where('mb_no', $mb_no) ;
        return $this->db->update('hb_member', $data) ;
    }

    // 개통 완료 시 즉시 승급
    function immediately_level_up($in_or_id)
    {
        $mb_no = $this->get_mb_no_by_or_id($in_or_id) ; 
        $cur_mb_position = $this->get_mb_positon($mb_no) ;

        // 브론즈, 실버 승급
        if ( $cur_mb_position >= SILVER )
            return false ;

        // 개통 완료 해당 회원 자동 승급
        if ( $this->_level_up($mb_no) )       
        {
            // 해당 회원 상위 리스트 자동 승급  
            $upper_list = $this->get_all_upper_member($mb_no) ;

            foreach ($upper_list as $key => $value) 
            {
                $this->_level_up($value['ancestor_mb_no']) ;
            }
        }
    }

    // 회차 승급
    function weekly_level_up($in_wa_id)
    {
        $this->db->select('hb_member.mb_no, mb_position') ;
        $this->db->join('hb_member', 'hb_order.mb_no = hb_member.mb_no') ;
        $this->db->where('wa_id', $in_wa_id) ;

        $result = $this->db->get('hb_order')->result_array() ;

        foreach ($result as $value) 
        {
            // 브론즈, 실버 승급
            if ( $value['mb_position'] > SILVER )
            {
                if ( $this->_level_up($value['mb_no']) )       
                {
                    // 해당 회원 상위 리스트 자동 승급  
                    $upper_list = $this->get_all_upper_member($value['mb_no']) ;

                    foreach ($upper_list as $row) 
                    {
                        if ( $this->_level_up($row['ancestor_mb_no']) == false )
                        {
                            return false ;
                        }
                    }
                }
            }
        }

        return true ;
    }

    private function _level_up($in_mb_no)
    {
        $result = $this->check_terms_of_position_up($in_mb_no) ;
        echo $result['msg'] ;

        if ( $result['result'] )
        {
            $cur_mb_position = $this->get_mb_positon($in_mb_no) ;

            $this->db->where('mb_no', $in_mb_no) ;
            $this->db->update('hb_member', array('mb_position' => $cur_mb_position + 1)) ;

            $this->load->config('cf_position') ;
            $mb_position_eng = $this->config->item('mb_position_eng') ;
            $field_name = $mb_position_eng[$cur_mb_position + 1] ;

            $this->db->where('mb_no', $in_mb_no) ;
            $this->db->update('hb_date_of_position_up', array($field_name => TIME_YMDHIS, $field_name.'_terms' => $result['terms'])) ;

            return true ;
        }

        return false ;
    }

    // 승급 조건 따지기
    private function check_terms_of_position_up($in_mb_no)
    {
        $output = array() ;
        $output['result'] = FALSE ;
        $output['msg'] = '' ;
        $output['terms'] = '' ;

        // 30만 cv 조건 따지기(전체)
        $result = $this->check_30cv_terms($in_mb_no) ;
        if ( $result['result'] == false )
        {
            $output['msg'] = 'check_30cv_terms : false' ;
            return $output ;
        }

        $output['terms'] .= $result['msg'] ;

        $mb_position = $this->get_mb_positon($in_mb_no) ;

        if ( $mb_position < SILVER ) // 브론즈, 실버 승급
        {
            // 45만 pv 조건 체크
            $result = $this->check_45pv_terms($in_mb_no) ;
            if ( $result['result'] == false )
            {
                $output['msg'] = 'check_45pv_terms : false' ;
                return $output ;
            }

            $output['terms'] .= $result['msg'] ;
        }
        else
        {
            // 좌 450만 pv 조건 체크
            $result = $this->check_450pv_terms($in_mb_no, true) ;
            if ( $result['result'] == false )
            {
                $output['msg'] = 'left check_450pv_terms : false' ;
                return $output ;
            }

            $output['terms'] .= $result['msg'] ;

            // 우 450만 pv 조건 체크
            $result = $this->check_450pv_terms($in_mb_no, false) ;
            if ( $result['result'] == false )
            {
                $output['msg'] = 'right check_450pv_terms : false' ;
                return $output ;
            }

            $output['terms'] .= $result['msg'] ;
        }

        // 2 : 1조건 따지기 
        if ( $mb_position >= BRONZE ) // For 실버 이상 승급
        {
            if ( $this->check_2_to_1_terms($in_mb_no, $mb_position) == false )
            {
                $output['msg'] = 'check_2_to_1_terms : false' ;
                return $output ;
            }

            $output['terms'] .= '2to1' ;
        }

        $output['result'] = TRUE ;
        $output['msg'] = 'check_terms_of_position_up : true' ;

        return $output ;
    }

    // 주문 건으로 mb_no 가져오기
    function get_mb_no_by_or_id($in_or_id)
    {
        $this->db->select('mb_no') ;
        $this->db->where('or_id', $in_or_id) ;
        
        return $this->db->get('hb_order')->row()->mb_no ;
    }

    // 내 등급 가져오기
    function get_mb_positon($in_mb_no)
    {
        $this->db->select('mb_position') ;
        $this->db->where('mb_no', $in_mb_no) ;

        return $this->db->get('hb_member')->row()->mb_position ;
    }

    // 30만 cv 조건 체크
    function check_30cv_terms($in_mb_no)
    {
        $this->db->select('mb_cv') ;
        $this->db->where('mb_no', $in_mb_no) ;

        $output = array() ;
        $output['result'] = FALSE ;
        $output['msg'] = '' ;

        $mb_cv = $this->db->get('hb_member')->row()->mb_cv ;

        if ( $mb_cv >= 300000 )
        {
            $output['result'] = TRUE ;
            $output['msg'] = 'cv:'.$mb_cv.',' ;
        }

        return $output ;
    }

    // 45만 pv 조건 체크
    function check_45pv_terms($in_mb_no)
    {
        $this->db->select('mb_pv') ;
        $this->db->where('mb_no', $in_mb_no) ;

        $output = array() ;
        $output['result'] = FALSE ;
        $output['msg'] = '' ;

        $mb_pv = $this->db->get('hb_member')->row()->mb_pv ;      

        if ( $mb_pv >= 450000 )
        {
            $output['result'] = TRUE ;
            $output['msg'] = 'pv:'.$mb_pv.',' ;
        }

        return $output ;
    }

    // 450만 pv 조건 체크
    function check_450pv_terms($in_mb_no, $b_left)
    {
        $this->db->select('sum(mb_pv) as sum_mb_pv') ;
        $this->db->from('hb_member') ;
        $this->db->join('member_nodes_paths', 'member_nodes_paths.descendant_mb_no = hb_member.mb_no') ;

        if ( $b_left )
        {
            $this->db->join('hb_sub_tree_by_mb_no', 'member_nodes_paths.descendant_mb_no = hb_sub_tree_by_mb_no.st_left_no') ;
        }
        else
        {
            $this->db->join('hb_sub_tree_by_mb_no', 'member_nodes_paths.descendant_mb_no = hb_sub_tree_by_mb_no.st_right_no') ;
        }

        $this->db->where(array('ancestor_mb_no' => $in_mb_no, 'path_length != ' => '0')) ;
        
        $output = array() ;
        $output['result'] = FALSE ;
        $output['msg'] = '' ;

        $sum_mb_pv = $this->db->get()->row()->sum_mb_pv ; 
       
        if ( $sum_mb_pv > 450000 )
        {
            $output['result'] = TRUE ;

            if ( $b_left )
            {
                $output['msg'] = 'left_' ;
            }
            else
            {
                $output['msg'] = 'right_' ;
            }

            $output['msg'] .= 'sum_pv:'.$sum_mb_pv.',' ;
        }

        return $output ;
    }

    // 하위직급 2 : 1조건 따지기
    function check_2_to_1_terms($in_mb_no, $in_mb_position)
    {
        $cnt_left_sub_tree = $this->get_count_sub_member($in_mb_no, $in_mb_position, true) ;
        $cnt_right_sub_tree = $this->get_count_sub_member($in_mb_no, $in_mb_position, false) ;

        if ( $cnt_left_sub_tree == 0 || $cnt_right_sub_tree == 0 )
        {
            return false ;
        }

        if ( floor( $cnt_left_sub_tree / $cnt_right_sub_tree ) == 2 || floor( $cnt_right_sub_tree / $cnt_left_sub_tree ) == 2 )
        {
            return true ;
        }

        return false ;
    }

    // 하위 직급 멤버 수 가져오기
    function get_count_sub_member($in_mb_no, $in_mb_position, $b_left)
    {
        $this->db->join('member_nodes_paths', 'member_nodes_paths.descendant_mb_no = hb_member.mb_no') ;

        if ( $b_left )
        {
            $this->db->join('hb_sub_tree_by_mb_no', 'member_nodes_paths.descendant_mb_no = hb_sub_tree_by_mb_no.st_left_no') ;
        }
        else
        {
            $this->db->join('hb_sub_tree_by_mb_no', 'member_nodes_paths.descendant_mb_no = hb_sub_tree_by_mb_no.st_right_no') ;
        }

        $this->db->where(array('ancestor_mb_no' => $in_mb_no, 'mb_position' => ($in_mb_position - 1), 'path_length != ' => '0' )) ;
        
        return $this->db->count_all_results('hb_member') ;
    }

    function get_all_upper_member($in_mb_no)
    {
        $this->db->select('ancestor_mb_no') ;
        $this->db->where(array('descendant_mb_no' => $in_mb_no, 'path_length != ' => '0')) ;
        return $this->db->get('member_nodes_paths')->result_array() ;
    }
}
?>