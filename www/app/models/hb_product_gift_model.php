<?php
class HB_product_gift_model extends CI_Model
{
    function __construct() {
        parent::__construct();
    }

    function list_count()
    {
        $this->db->where('TYPE_CD', '2501') ;
        $this->db->where('ISUSE', 'O') ;

        return $this->db->count_all_results('P_PDTMASTER') ;
    }

    function list_result($limit, $offset)
    {
        $this->db->limit($limit, $offset) ;

        $this->db->select('A.PDT_NAME, A.PDT_CD, B.GIFT_1, B.GIFT_2, B.GIFT_3') ;
        $this->db->from('P_PDTMASTER A') ;
        $this->db->join('HB_PRODUCT_GIFT B', 'A.PDT_CD = B.PDT_CD', 'left') ;

        $this->db->where('A.TYPE_CD', '2501') ;
        $this->db->where('A.ISUSE', 'O') ;

        $this->db->order_by("A.PDT_CD", "DESC") ;

        return $this->db->get()->result_array() ;
    }

    function update($data)
    {
        $this->db->where('PDT_CD', $data['PDT_CD']) ;
        $CNT = $this->db->count_all_results('HB_PRODUCT_GIFT') ;

        if ( $CNT > 0 )
        {
            $this->db->where('PDT_CD', $data['PDT_CD']) ;
            return $this->db->update('HB_PRODUCT_GIFT', $data) ;
        }
        else
        {
            return $this->db->insert('HB_PRODUCT_GIFT', $data) ;
        }
    }

    function get_gift_type($in_pdt_cd)
    {
        $this->db->where('PDT_CD', $in_pdt_cd) ;
        $result = $this->db->get('HB_PRODUCT_GIFT')->row_array() ;

        $list = array() ;

        if ( $result && isset($result['GIFT_1']) )
        {
            $list[0] = isset($result['GIFT_1']) ? $result['GIFT_1'] : '' ;
            $list[1] = isset($result['GIFT_2']) ? $result['GIFT_2'] : '' ;
            $list[2] = isset($result['GIFT_3']) ? $result['GIFT_3'] : '' ;
        }

         return $list ;
    }
}
?>