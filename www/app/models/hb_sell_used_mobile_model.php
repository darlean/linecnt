<?php
class HB_sell_used_mobile_model extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

    function insert()
    {
        $data = array(
            'PDT_CD' 		=> $this->input->post('PDT_CD'),
            'USERID' 		=> $this->session->userdata('ss_mb_id'),
            'ORD_NAME' 	    => $this->input->post('ORD_NAME'),
            'ORD_MOBILE' 	=> $this->input->post('ORD_MOBILE'),
            'ORD_MSG' 	    => $this->input->post('ORD_MSG'),
            'ORD_MOBILE_ID' => $this->input->post('ORD_MOBILE_ID'),
            'ORD_CENTER'   => $this->input->post('ORD_CENTER'),
            'SEND_TYPE'     => $this->input->post('SEND_TYPE'),
            'DELI_CD'       => $this->input->post('DELI_CD'),
            'DELI_NO'       => $this->input->post('DELI_NO'),
            'DELI_DATE'     => $this->input->post('DELI_DATE'),
        );

        $sql = "INSERT INTO HB_SELL_USED_MOBILE (ORDNO, PDT_CD, USERID, ORD_NAME, ORD_MOBILE, ORD_MSG, ORD_MOBILE_ID, ORD_CENTER, SEND_TYPE, DELI_CD, DELI_NO, DELI_DATE) 
                VALUES (TO_CHAR(SYSDATE, 'YYYYMMDD') || 'S' || TRIM(TO_CHAR(SEQ_HB_SELL_USED_MOBILE.NEXTVAL,'00000')),
				 '".$data['PDT_CD']."', '".$data['USERID']."', '".$data['ORD_NAME']."', '".$data['ORD_MOBILE']."', '".$data['ORD_MSG']."', '".$data['ORD_MOBILE_ID']."', '".$data['ORD_CENTER']."'
				 , '".$data['SEND_TYPE']."', '".$data['DELI_CD']."', '".$data['DELI_NO']."', '".$data['DELI_DATE']."')" ;

        if ( $this->db->query($sql) )
        {
            $rst = $this->db->query("SELECT TO_CHAR(SYSDATE, 'YYYYMMDD') || 'S' || TRIM(TO_CHAR(SEQ_HB_SELL_USED_MOBILE.CURRVAL,'00000')) AS ORDNO FROM dual")->row();

            if ($rst)
            {
                return $rst->ORDNO ;
            }
        }

        return 0 ;
    }

    function insert_mobile_image( $in_ord_no, $in_img_path )
    {
        $sql = "MERGE INTO HB_SELL_USED_MOBILE trg  
                USING (SELECT '$in_ord_no' as ORDNO, '$in_img_path' as ORD_IMG FROM DUAL) src ON (src.ORDNO = trg.ORDNO)
                WHEN NOT MATCHED THEN 
                    INSERT(ORDNO, ORD_IMG) VALUES (src.ORDNO, src.ORD_IMG)
                WHEN MATCHED THEN 
                    UPDATE SET trg.ORD_IMG = src.ORD_IMG";

        return $this->db->query($sql);
    }

    function delete($in_ord_no)
    {
        $this->db->where('ORDNO', $in_ord_no) ;
        return $this->db->delete('HB_SELL_USED_MOBILE') ;
    }

    function update($in_ord_no, $in_sql)
    {
        $this->db->where('ORDNO', $in_ord_no) ;
        return $this->db->update('HB_SELL_USED_MOBILE', $in_sql, false) ;
    }

    function complete($in_ord_no)
    {
        $confirm_amt = $this->input->post('CONFIRM_AMT') ;
        $result_msg = $this->input->post('RESULT_MSG') ;

        $sql = "UPDATE HB_SELL_USED_MOBILE SET CONFIRM_AMT = '".$confirm_amt."', RESULT_MSG = '".$result_msg."', CONFIRM_DATE = CURRENT_TIMESTAMP, ORD_STATE = '2' WHERE ORDNO = '".$in_ord_no."'" ;
        return $this->db->query($sql) ;
    }

    function list_count($sfl, $stx, $vender, $state, $center = "", $type = "")
    {
        $this->_get_search_cache($sfl, $stx);

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD') ;
        $this->db->join('D_MEMBER C', 'C.USERID = A.USERID') ;

        if ( $vender != '' )
        {
            $this->db->where('B.TEL_COMPANY', $vender) ;
        }

        if ( $state != -1 )
        {
            $this->db->where('A.ORD_STATE', $state) ;
        }

        if ( $type == 'completed' )
        {
            $this->db->where('A.CONFIRM_AMT > ', 0, false) ;
        }

        if ( $center != '' )
        {
            $this->db->where('A.ORD_CENTER', $center) ;
        }

        $USERID = ( $this->input->post('USERID') != '' ) ? $this->input->post('USERID') : $this->session->userdata('ss_mb_id') ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $this->db->where('A.ORD_CENTER', $this->session->userdata('CENTER_CD')) ;
        }
        else if ( !IS_MANAGER && !IS_CARD_AGENCY && !IS_RECYCLE )
        {
            $this->db->where('A.USERID', $USERID) ;
        }

        return $this->db->count_all_results('HB_SELL_USED_MOBILE A') ;
    }

    function list_result($sst, $sod, $sfl, $stx, $limit, $offset, $vender, $state, $center = "", $type = "")
    {
        $this->_get_search_cache($sfl, $stx);

        // 기간 검색
        $this->_get_search_date_range() ;

        if ($sst && $sod)
        {
            $this->db->order_by($sst, $sod);
        }

        $this->db->limit($limit, $offset) ;

        $select = "A.ORDNO, A.USERID, A.ORD_NAME, A.ORD_MOBILE, A.ORD_MSG, TO_CHAR(A.ORD_DATE, 'YYYY-MM-DD HH24:MI:SS') AS ORD_DATE, 
                A.ORD_STATE, A.SEND_TYPE, A.CONFIRM_AMT, TO_CHAR(A.CONFIRM_DATE, 'YYYY-MM-DD HH24:MI:SS') AS CONFIRM_DATE,
                A.DELI_CD, A.DELI_NO, A.DELI_DATE, A.IS_DEPOSIT,
				B.PDT_NAME, PKG_CRYPTO_FIELD.get('USERNAME', C.USERNAME, ,'9','".G_CRYPTO."') as USERNAME,
				PKG_CRYPTO_FIELD.get('ACCOUNT', C.ACCOUNT, ,'9','".G_CRYPTO."') as ACCOUNT,
				(SELECT BANK_NAME FROM S_BANK WHERE BANK_CD = C.BANK_CD) as BANK,
				(SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.ORD_CENTER) as ORD_CENTER" ;

        $this->db->select( $select, false ) ;

        $this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD') ;
        $this->db->join('D_MEMBER C', 'C.USERID = A.USERID') ;

        if ( $vender != '' )
        {
            $this->db->where('B.TEL_COMPANY', $vender) ;
        }

        if ( $state != -1 )
        {
            $this->db->where('A.ORD_STATE', $state) ;
        }

        if ( $type == 'completed' )
        {
            $this->db->where('A.CONFIRM_AMT > ', 0, false) ;
        }

        if ( $center != '' )
        {
            $this->db->where('A.ORD_CENTER', $center) ;
        }

        $USERID = ( $this->input->post('USERID') != '' ) ? $this->input->post('USERID') : $this->session->userdata('ss_mb_id') ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $this->db->where('A.ORD_CENTER', $this->session->userdata('CENTER_CD')) ;
        }
        else if ( !IS_MANAGER && !IS_CARD_AGENCY && !IS_RECYCLE )
        {
            $this->db->where('A.USERID', $USERID) ;
        }

        return $this->db->get('HB_SELL_USED_MOBILE A')->result_array() ;
    }

    // 검색 구문을 얻는다.
    function _get_search_cache($search_field, $search_text)
    {
        if (!$search_field || !$search_text)
            return FALSE;

        $search_field = $search_field ;
        $where = '' ;

        if ( $search_field == 'USERNAME' )
        {
            $where .= "PKG_CRYPTO_FIELD.get('USERNAME', C.USERNAME, '9','".G_CRYPTO."') LIKE ".$this->db->escape('%'.$search_text.'%');
        }
        else
        {
            $where .= $this->db->protect_identifiers($search_field) . " LIKE '%" . $search_text . "%'";
        }

        $this->db->where($where, null, FALSE);
    }

    // 기간 검색
    function _get_search_date_range()
    {
        $sdt = $this->input->get('sdt') ;
        $start_date = $this->input->get('start');   // 시작날짜
        $finish_date = $this->input->get('finish'); // 종료날짜

        if ( $start_date != '' )
        {
            $this->db->where("TO_CHAR(".$sdt.", 'YYYYMMDD') >= ", "'".$start_date."'", false) ;
        }

        if ( $finish_date != '' )
        {
            $Y = substr($finish_date, 0, 4) ;
            $M = substr($finish_date, 4, 2) ;
            $D = substr($finish_date, -2) ;
            $finish = date("Ymd", mktime(0, 0, 0, $M, $D + 1, $Y)) ;

            $this->db->where("TO_CHAR(".$sdt.", 'YYYYMMDD') < ", "'".$finish."'", false) ;
        }
    }

    function get_order_info($in_ord_no)
    {
        $select = "A.*, TO_CHAR(A.CONFIRM_DATE, 'YYYY-MM-DD HH24:MI:SS') AS CONFIRM_DATE, (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.ORD_CENTER) as ORD_CENTER, B.PDT_NAME, B.PDT_MODEL, C.AMT, D.IMG_PATH" ;
        $this->db->select($select, false) ;

        $this->db->from('HB_SELL_USED_MOBILE A') ;
        $this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD') ;
        $this->db->join( 'P_PDTSELL C', 'A.PDT_CD = C.PDT_CD', 'left') ;
        $this->db->join( 'P_PDTIMAGE D', 'A.PDT_CD = D.PDT_CD', 'left') ;

        $this->db->where('A.ORDNO', $in_ord_no) ;

        return $this->db->get()->row_array() ;
    }

    function center_list_count()
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->select("COUNT( DISTINCT ORD_CENTER) AS CNT", false) ;

        $this->db->where('ORD_STATE', '2') ;
        $this->db->where('CONFIRM_AMT > ', 0, false) ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $this->db->where('ORD_CENTER', $this->session->userdata('CENTER_CD')) ;
        }

        return $this->db->get('HB_SELL_USED_MOBILE')->row()->CNT ;
    }

    function center_list_result($limit, $offset)
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        $select = "COUNT(A.ORDNO) AS CNT, B.CENTER_NAME, B.TEL, C.ACCOUNT, (SELECT BANK_NAME FROM S_BANK WHERE BANK_CD = C.BANK_CD) as BANK, A.ORD_CENTER, 
        COUNT ( CASE WHEN A.IS_DEPOSIT = 'X' then 1 end ) AS NO_DEPOSIT, COUNT ( CASE WHEN A.IS_DEPOSIT = 'O' then 1 end ) AS YES_DEPOSIT" ;

        $this->db->select( $select, false ) ;

        $this->db->join('S_CENTER B', 'A.ORD_CENTER = B.CENTER_CD') ;
        $this->db->join('S_CENTERBANK C', 'C.CENTER_CD = A.ORD_CENTER', 'LEFT') ;

        $this->db->where('A.ORD_STATE', '2') ;
        $this->db->where('A.CONFIRM_AMT > ', 0, false) ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $this->db->where('A.ORD_CENTER', $this->session->userdata('CENTER_CD')) ;
        }

        $this->db->group_by('A.ORD_CENTER, B.CENTER_NAME, B.TEL, C.ACCOUNT, C.BANK_CD') ;

        return $this->db->get('HB_SELL_USED_MOBILE A')->result_array() ;
    }
}
?>