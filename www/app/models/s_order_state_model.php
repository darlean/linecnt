<?php
class S_Order_state_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function get_order_state_list($bMobile = true)
	{
		$this->db->select('STATE_CD, STATE_NAME') ;
		$result = $this->db->get('S_ORDERSTATE')->result_array() ;

		$list = array() ;
		foreach ($result as $key => $row) 
		{
            if ( $bMobile || ( $row['STATE_NAME'] == '신청완료' || $row['STATE_NAME'] == '신청반려' || $row['STATE_NAME'] == '개통대기' || $row['STATE_NAME'] == '개통완료' || $row['STATE_NAME'] == '개통취소' ) ) {
                $list[$row['STATE_CD']] = $row['STATE_NAME'];
            }
		}

		return $list ;
	}

	function get_starion_order_state_list()
    {
        $list = array(1 => "주문접수", 2 => "주문확인", 3 => "주문취소", 4 => "설치완료") ;

        return $list ;
    }
}
?>