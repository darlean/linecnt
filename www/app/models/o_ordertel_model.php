<?php
class O_Ordertel_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function update($in_ord_no)
    {
        $sql = "UPDATE O_ORDERTEL SET REAL_OPEN_DT4 = TO_CHAR(SYSDATE, 'YYYYMMDDHHMM') WHERE ORDNO = '$in_ord_no'";

        return $this->db->query($sql);        
    }

    function back_update($in_ord_no)
    {
        $sql = "UPDATE O_ORDERTEL SET REAL_OPEN_DT4 = '' WHERE ORDNO = '$in_ord_no'";

        return $this->db->query($sql);
    }

    function update_open_agency($in_ord_no, $in_open_agency)
    {
        $sql = "UPDATE O_ORDERTEL SET REAL_OPEN_AGENCY = '".$in_open_agency."' WHERE ORDNO = '$in_ord_no'" ;

        return $this->db->query($sql);
    }
}
?>