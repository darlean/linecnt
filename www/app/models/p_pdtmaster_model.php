<?php
class P_pdtmaster_model extends CI_Model 
{
    function __construct() {
        parent::__construct();
    }

    function get_pdt_info($in_pdt_cd)
    {        
        $this->db->where('PDT_CD', $in_pdt_cd) ;         

        return $this->db->get('P_PDTMASTER')->row_array() ;
    }    

    function mobile_list_for_hbplanner($vender = '')
    {
    	$this->db->select ( "/*+ index(P_PDTMASTER P_PDTMASTER_PK)*/ A.SELL_FORCED, A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, MAX(B.S_DATE) AS S_DATE, C.IMG_PATH, D.AMT", false ) ;
        $this->db->from ( "P_PDTMASTER A" ) ;
        $this->db->join( 'P_PDTTELPRICE B', 'A.PDT_CD = B.PDT_CD', 'left') ;
        $this->db->join( 'P_PDTIMAGE C', 'A.PDT_CD = C.PDT_CD', 'left') ;
        $this->db->join( 'P_PDTSELL D', 'A.PDT_CD = D.PDT_CD', 'left') ;
        $this->db->where( 'A.TYPE_CD', '2502' ) ;
		$this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN B.S_DATE AND B.E_DATE") ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN D.S_DATE AND D.E_DATE") ;
        $this->db->where('A.ISUSE', 'O') ;

        if ( $vender != '' )
        {
            $this->db->where( 'TEL_COMPANY', $vender ) ;
        }

        $this->db->group_by('A.SELL_FORCED, A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, C.IMG_PATH, D.AMT') ;
        $this->db->order_by('A.PDT_CD', 'desc') ;

        return $this->db->get()->result_array() ;
    }

    function product_list_for_hbplanner($vender, $product_type)
    {
        $this->db->select ( "/*+ index(P_PDTMASTER P_PDTMASTER_PK)*/ A.SELL_FORCED, A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, MAX(B.S_DATE) AS S_DATE, C.IMG_PATH, D.AMT, E.INSERT_LINK_TYPE, E.NEW_LINK_PATH, E.TR_LINK_PATH, E.RE_LINK_PATH", false ) ;
        $this->db->from ( "P_PDTMASTER A" ) ;
        $this->db->join( 'P_PDTTELPRICE B', 'A.PDT_CD = B.PDT_CD', 'left') ;
        $this->db->join( 'P_PDTIMAGE C', 'A.PDT_CD = C.PDT_CD', 'left') ;
        $this->db->join( 'P_PDTSELL D', 'A.PDT_CD = D.PDT_CD', 'left') ;
        $this->db->join('HB_PDT_LINK E', 'A.PDT_CD = E.PDT_CD', 'left') ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN B.S_DATE AND B.E_DATE") ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN D.S_DATE AND D.E_DATE") ;
        $this->db->where('A.ISUSE', 'O') ;
        $this->db->where( 'A.TEL_COMPANY', $vender ) ;
        $this->db->where( 'A.TYPE_CD', $product_type ) ;

        $this->db->group_by('A.SELL_FORCED, A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, C.IMG_PATH, D.AMT, E.INSERT_LINK_TYPE, E.NEW_LINK_PATH, E.TR_LINK_PATH, E.RE_LINK_PATH') ;
        $this->db->order_by('A.PDT_CD', 'desc') ;

        return $this->db->get()->result_array() ;
    }

    function mobile_info_for_hbplanner($pdt_cd, $vender='3203')
    {
    	$this->db->select("A.PTP_ID, A.PRICE_KIND, A.REG_KIND, A.PRICE_DISCOUNT, A.POINT1, A.POINT2, B.CP_NAME, C.REASON_NAME, D.CP_PRICE, D.CP_DISCOUNT, D.FREE_CALL, D.FREE_SMS, D.FREE_DATA") ;
    	$this->db->from("P_PDTTELPRICE A") ;
    	$this->db->join("P_CALLING_PLAN B", "A.PRICE_KIND = B.CP_CD", "left") ;
    	$this->db->join("S_REASON C", "A.REG_KIND = C.REASON_CD", "left") ;
    	$this->db->join("HB_CALLING_PLAN D", "B.CP_NAME = D.CP_NAME", "left") ;
    	$this->db->where("A.PDT_CD", $pdt_cd) ;
        $this->db->where('A.ISUSE', 'O') ;
        $this->db->where('B.CP_USE', 'O') ;
        $this->db->where('C.ISUSE', 'O') ;
        $this->db->where('A.POINT1 >= ', 0, false) ;  
    	$this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN A.S_DATE AND A.E_DATE") ;
    	$this->db->order_by('A.REG_KIND', 'asc') ;
    	$this->db->order_by('B.CP_NAME', 'asc') ;

    	$result = $this->db->get()->result_array() ;

    	$list = array() ;

    	$detail = $this->mobile_detail_info_for_hbplanner($pdt_cd) ; 
		foreach ($detail as $key2 => $value2) {
			$list['COLOR'][$value2['PDTDETAIL_NO']] = $value2['OPT_VALUE'] ;
		}

        $price_types = array( '선택약정 LTE', '선택약정 음성무한데이터', 'LTE', '음성무한데이터' ) ;

        if ( $vender == '3201')
        {
            $price_types = array( '선택약정 LTE', '선택약정 순', 'LTE', '순' ) ;            
        }

    	foreach ($result as $key => $value) 
    	{
            if ( !isset($list[$value['REG_KIND']]['REASON_NAME']) )
            {
                $list[$value['REG_KIND']]['REASON_NAME'] = $value['REASON_NAME'] ;
            }

            foreach ($price_types as $idx => $price_type)
            {
                if(strpos($value['CP_NAME'], $price_type) !== false) 
                {
                    $FREE_CALL = ($value['FREE_CALL']) ? $value['FREE_CALL'] : 0 ;
                    $FREE_CALL = ($FREE_CALL > 9999) ? "무제한" : $FREE_CALL ;

                    $FREE_SMS = ($value['FREE_SMS']) ? $value['FREE_SMS'] : 0 ;
                    $FREE_SMS = ($FREE_SMS > 9999) ? "무제한" : $FREE_SMS ;

                    $FREE_DATA = ($value['FREE_DATA']) ? $value['FREE_DATA'] : 0 ;
                    $FREE_DATA = ($FREE_DATA > 9999) ? "무제한" : (float)$FREE_DATA ;

                    if ( !isset($list['PRICE_TYPE'][$price_type]) )
                    {
                        $list['PRICE_TYPE'][$price_type] = $price_type ;
                    }
                    
                    $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['PTP_ID'] = $value['PTP_ID'] ;
                    $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['CP_NAME'] = $value['CP_NAME'] ;
                    $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['PRICE_DISCOUNT'] = $value['PRICE_DISCOUNT'] ;
                    $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['CP_PRICE'] = ($value['CP_PRICE']) ? $value['CP_PRICE'] : 0 ;
                    $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['CP_DISCOUNT'] = ($value['CP_DISCOUNT']) ? $value['CP_DISCOUNT'] : 0 ;
                    $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['FREE_CALL'] = $FREE_CALL ;
                    $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['FREE_SMS'] = $FREE_SMS ;
                    $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['FREE_DATA'] = $FREE_DATA ;
                    $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['HB'] = $value['POINT1'] ;

                    break ;
                }
            }
    	}

    	return $list ;
    }

    function mobile_info_for_hbplanner2($pdt_cd, $vender='3203')
    {
        $this->db->select("A.PTP_ID, A.PRICE_KIND, A.REG_KIND, A.PRICE_DISCOUNT, A.POINT1, A.POINT2, B.CP_NAME, C.REASON_NAME, D.CP_PRICE, D.CP_DISCOUNT, D.FREE_CALL, D.FREE_SMS, D.FREE_DATA") ;
        $this->db->from("P_PDTTELPRICE A") ;
        $this->db->join("P_CALLING_PLAN B", "A.PRICE_KIND = B.CP_CD", "left") ;
        $this->db->join("S_REASON C", "A.REG_KIND = C.REASON_CD", "left") ;
        $this->db->join("HB_CALLING_PLAN D", "B.CP_NAME = D.CP_NAME", "left") ;
        $this->db->where("A.PDT_CD", $pdt_cd) ;
        $this->db->where('A.ISUSE', 'O') ;
        $this->db->where('B.CP_USE', 'O') ;
        $this->db->where('C.ISUSE', 'O') ;
        $this->db->where('A.POINT1 >= ', 0, false) ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN A.S_DATE AND A.E_DATE") ;
        $this->db->order_by('A.REG_KIND', 'asc') ;
        $this->db->order_by('B.CP_NAME', 'asc') ;

        $result = $this->db->get()->result_array() ;

        $list = array() ;

        $detail = $this->mobile_detail_info_for_hbplanner($pdt_cd) ;
        foreach ($detail as $key2 => $value2) {
            $list['COLOR'][$value2['PDTDETAIL_NO']] = $value2['OPT_VALUE'] ;
        }

        foreach ($result as $key => $value)
        {
            if ( !isset($list[$value['REG_KIND']]['REASON_NAME']) )
            {
                $list[$value['REG_KIND']]['REASON_NAME'] = $value['REASON_NAME'] ;
            }

            $price_type = "" ;

            if(strpos($value['CP_NAME'], "청소년") !== false)
            {
                if(strpos($value['CP_NAME'], "선택약정") !== false)
                {
                    $price_type = "선택약정 청소년" ;
                }
                else
                {
                    $price_type = "공시지원 청소년" ;
                }
            }
            else if(strpos($value['CP_NAME'], "GB") !== false || strpos($value['CP_NAME'], "egg") !== false)
            {
                $price_type = "데이터" ;
            }
            else
            {
                if(strpos($value['CP_NAME'], "선택약정") !== false)
                {
                    $price_type = "선택약정" ;
                }
                else
                {
                    $price_type = "공시지원" ;
                }
            }

            if ( !isset($list['PRICE_TYPE'][$price_type]) )
            {
                $list['PRICE_TYPE'][$price_type] = $price_type ;
            }

            {
                $FREE_CALL = ($value['FREE_CALL']) ? $value['FREE_CALL'] : 0 ;
                $FREE_CALL = ($FREE_CALL > 9999) ? "무제한" : $FREE_CALL ;

                $FREE_SMS = ($value['FREE_SMS']) ? $value['FREE_SMS'] : 0 ;
                $FREE_SMS = ($FREE_SMS > 9999) ? "무제한" : $FREE_SMS ;

                $FREE_DATA = ($value['FREE_DATA']) ? $value['FREE_DATA'] : 0 ;
                $FREE_DATA = ($FREE_DATA > 9999) ? "무제한" : (float)$FREE_DATA ;

                if ( $vender == '3207' && (strpos($value['CP_NAME'], "선불") !== false) )
                {
                    $FREE_CALL = ( $FREE_CALL == "무제한" ) ? "기본" : $FREE_CALL ;
                    $FREE_SMS = ( $FREE_SMS == '무제한' ) ? "기본" : $FREE_SMS ;
                    $FREE_DATA = ( $FREE_DATA == '무제한' ) ? "기본" : $FREE_DATA ;
                }

                $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['PTP_ID'] = $value['PTP_ID'] ;
                $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['CP_NAME'] = $value['CP_NAME'] ;
                $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['PRICE_DISCOUNT'] = $value['PRICE_DISCOUNT'] ;
                $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['CP_PRICE'] = ($value['CP_PRICE']) ? $value['CP_PRICE'] : 0 ;
                $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['CP_DISCOUNT'] = ($value['CP_DISCOUNT']) ? $value['CP_DISCOUNT'] : 0 ;
                $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['FREE_CALL'] = $FREE_CALL ;
                $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['FREE_SMS'] = $FREE_SMS ;
                $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['FREE_DATA'] = $FREE_DATA ;
                $list[$value['REG_KIND']][$price_type][$value['PRICE_KIND']]['HB'] = $value['POINT1'] ;
            }
        }

        return $list ;
    }

    function mobile_detail_info_for_hbplanner($pdt_cd)
    {
    	$this->db->select("PDTDETAIL_NO, OPT_VALUE") ;
    	$this->db->from("P_PDTDETAIL") ;
    	$this->db->where("PDT_CD", $pdt_cd) ;
    	//$this->db->where("STOCKCOUNT >", "0",  false) ;

    	return $this->db->get()->result_array() ;
    }

    function wired_info_for_hbplanner($pdt_cd, $vender='3203')
    {
        $this->db->select("A.PTP_ID, A.PRICE_KIND, A.REG_KIND, A.PRICE_DISCOUNT, A.POINT1, A.POINT2, B.CP_NAME, C.REASON_NAME, D.CP_PRICE, D.CP_DISCOUNT, D.FREE_CALL, D.FREE_SMS, D.FREE_DATA, E.*") ;
        $this->db->from("P_PDTTELPRICE A") ;
        $this->db->join("P_CALLING_PLAN B", "A.PRICE_KIND = B.CP_CD", "left") ;
        $this->db->join("S_REASON C", "A.REG_KIND = C.REASON_CD", "left") ;
        $this->db->join("HB_CALLING_PLAN D", "B.CP_NAME = D.CP_NAME", "left") ;
        $this->db->join("HB_WIRED_GIFT E", "E.PDT_CD = A.PDT_CD", "left") ;
        $this->db->where("A.PDT_CD", $pdt_cd) ;
        $this->db->where('A.ISUSE', 'O') ;
        $this->db->where('B.CP_USE', 'O') ;
        //$this->db->where('C.ISUSE', 'O') ;
        $this->db->where('A.POINT1 >= ', 0, false) ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN A.S_DATE AND A.E_DATE") ;
        $this->db->order_by('A.REG_KIND', 'asc') ;
        $this->db->order_by('A.PTP_ID', 'asc') ;
        $this->db->order_by('B.CP_NAME', 'asc') ;

        return $this->db->get()->result_array() ;
    }
}
?>