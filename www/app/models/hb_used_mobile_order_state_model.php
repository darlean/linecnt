<?php
class HB_Used_mobile_order_state_model extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

	function get_used_mobile_order_state_list()
	{
		$this->db->select('STATE_CD, STATE_NAME') ;
		$result = $this->db->get('HB_USED_MOBILE_ORDERSTATE')->result_array() ;

		$list = array() ;
		foreach ($result as $key => $row) 
		{
			$list[$row['STATE_CD']] = $row['STATE_NAME'] ;
		}

		return $list ;
	}
}
?>