<?php
class Board_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function invalid_table($BBS_ID)
	{
		$row = $this->db->query( "select count(*) as cnt from tab where tname = 'BBS_BOARD_$BBS_ID'" )->row_array();

		return $row['CNT'] != 0;
	}

	function get_lists_count( $sql )
	{
		        // 페이지 정보 얻기
		$query = "SELECT count(*) as CNT FROM ($sql)";
		$count = $this->db->query($query)->row_array() ;		//var_dump($count);

		return $count['CNT'];
	}

	function get_lists( $BBS_ID, $page, $per_page, $sfl = "", $stx = "" )
	{
		// 기본쿼리
		$sql="
			SELECT XA.*,
				   (SELECT COUNT(*) FROM BBS_FILE_$BBS_ID BF WHERE BF.BBS_NO = XA.BBS_NO) as FILE_CNT 
			  FROM (
				SELECT BBS_NO, BBS_GROUP, BBS_LEVEL, 0 as BBS_SORT, BBS_ISNOTE, BBS_TITLE, BBS_SEARCH, WORK_USER, to_char(WORK_DATE,'yyyy-mm-dd') as WORK_DATE,
					   TRUNC(SYSDATE) - TRUNC(WORK_DATE) AS DAY					   
				  FROM BBS_BOARD_$BBS_ID
				WHERE BBS_ISNOTE = 'Y'
				UNION ALL
				SELECT BBS_NO, BBS_GROUP, BBS_LEVEL, BBS_SORT, BBS_ISNOTE, BBS_TITLE, BBS_SEARCH, WORK_USER, to_char(WORK_DATE,'yyyy-mm-dd') as WORK_DATE,
					   TRUNC(SYSDATE) - TRUNC(WORK_DATE) AS DAY
				  FROM BBS_BOARD_$BBS_ID
				 WHERE BBS_ISNOTE = 'N'
		";

		if($stx!="") 
		{
			if($sfl      =="BBS_TITLE") 			$sql .= "   AND BBS_TITLE like '%" . $stx ."%'";
			else if($sfl =="BBS_CONTEXT") 			$sql .= "   AND BBS_CONTEXT like '%" . $stx ."%'";
			else if($sfl =="WORK_USER") 			$sql .= "   AND WORK_USER like '%" . $stx ."%'";
			else if($sfl =="BBS_TITLE.BBS_CONTEXT") $sql .= "   AND (BBS_TITLE like '%" . $stx ."%') or (BBS_CONTEXT like '%" . $stx ."%')";
		}

		$sql .= ") XA ORDER BY XA.BBS_SORT, XA.BBS_NO";

		$qry = "SELECT X.* FROM (\n";
        $qry .= "\tSELECT A.*,ceil(ROWNUM/$per_page) as PAGE FROM ($sql\n";
        $qry .= "\t) A\n";
        $qry .= ") X\n";
        $qry .= "WHERE page = $page";


        $result_array = $this->db->query($qry)->result_array() ;
        $result['total_count'] = $this->get_lists_count( $sql );

        // 일반 리스트
        $list = array();
        foreach ($result_array as $i => $row) 
        {
			$list[$i]           = new stdClass();
			$list[$i]->num      = $result['total_count'] - ($page - 1) * $per_page - $i;
			$list[$i]->title    = $row['BBS_TITLE'];
			$list[$i]->name     = $row['WORK_USER'];
			$list[$i]->datetime = $row['WORK_DATE'];
			$list[$i]->hit      = $row['BBS_SEARCH'];
			$list[$i]->href     = RT_PATH.'/bbs/view/page/'.$page.'?BBS_NO='.$row['BBS_NO'].'&BBS_ID='.$BBS_ID ;
			$list[$i]->isnote   = $row['BBS_ISNOTE'];
        }

        $result['list'] = $list;

        return $result;
	}

	function get_row( $BBS_ID, $BBS_NO )
	{
		$sql = "update BBS_BOARD_$BBS_ID set BBS_SEARCH = BBS_SEARCH +1 where BBS_NO = $BBS_NO";
		$this->db->query($sql);

		$sql="
				SELECT BBS_NO, BBS_GROUP, BBS_LEVEL, BBS_SORT, BBS_TITLE, DBMS_LOB.SUBSTR( BBS_CONTEXT, 3000 ) as BBS_CONTEXT, BBS_ISNOTE, BBS_SEARCH, WORK_USER, to_char(WORK_DATE,'yyyy-mm-dd') as WORK_DATE
				  FROM BBS_BOARD_$BBS_ID
				 WHERE BBS_NO = $BBS_NO
			";

		// 글 데이터
		$data = $this->db->query($sql)->row_array() ;

		// 파일
		$file = $this->db->query("select * from BBS_FILE_$BBS_ID where BBS_NO = $BBS_NO")->result_array() ;

		// 댓글
		$sql= "
				select BBS_SEQ,DBMS_LOB.SUBSTR( BBS_MEMO, 3000 ) as BBS_MEMO,WORK_USER, to_char(WORK_DATE,'yyyy-mm-dd hh24:mi:ss') as WORK_DATE 
				  from BBS_COMMENT_$BBS_ID 
				 where BBS_NO = $BBS_NO order by BBS_SEQ desc
			  ";

		// 글 데이터
		$comment               = $this->db->query($sql)->result_array() ;
		
		
		$result['BBS_TITLE']   = $data['BBS_TITLE'];
		$result['BBS_CONTEXT'] = $data['BBS_CONTEXT'];
		
		$result['WORK_USER']   = $data['WORK_USER'];
		$result['WORK_DATE']   = $data['WORK_DATE'];
		
		$result['COMMENT']     = $comment;
		$result['FILE']        = $file;



		return $result;
	}
}
?>