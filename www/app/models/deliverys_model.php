<?php
class Deliverys_model extends CI_Model 
{
    function __construct() {
        parent::__construct();
    }

    function get_tag()
    {
        $this->db->select('dv_name') ;
        $result = $this->db->get('hb_deliverys')->result_array() ;

        $dv_tag = array() ;

        foreach ($result as $key => $row) 
        {
            $dv_tag[$row['dv_name']] = $row['dv_name'] ;
        }

        return $dv_tag ;
    }
}
?>