<?php
class Policy_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function list_result($sst, $sod, $sfl, $stx, $limit, $offset, $vender = '', $in_type = '')
    {    
        $this->db->limit($limit, $offset) ;
        
        $this->db->from ( "P_PDTMASTER A" ) ;        
        $this->db->join( 'P_PDTIMAGE C', 'A.PDT_CD = C.PDT_CD', 'left') ;
        $this->db->join( 'P_PDTSELL D', 'A.PDT_CD = D.PDT_CD', 'left') ;

        if ( $vender != '' )
        {
            $this->db->where( 'TEL_COMPANY', $vender ) ;
        }

        if ( $in_type == 'mobile' || $in_type == 'wired' )
        {
            $this->db->select ( "/*+ index(P_PDTMASTER P_PDTMASTER_PK)*/ A.SELL_FORCED, A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, MAX(B.S_DATE) AS S_DATE, C.IMG_PATH, D.AMT", false ) ;
            $this->db->join( 'P_PDTTELPRICE B', 'A.PDT_CD = B.PDT_CD', 'left') ;

            if ( $in_type == 'mobile' )
            {
                $this->db->where( 'A.TYPE_CD', '2502' ) ;
            }
            else if ( $in_type == 'wired' )
            {
                $this->db->where( 'A.TYPE_CD', '2504' ) ;   
            }

            $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN D.S_DATE AND D.E_DATE") ;
        }
        else if ( $in_type == 'card' )
        {
            $this->db->select ( "/*+ index(P_PDTMASTER P_PDTMASTER_PK)*/ A.SELL_FORCED, A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, C.IMG_PATH, D.AMT", false ) ;
            $this->db->where( 'A.TYPE_CD', '2506' ) ;
        }
        else if ( $in_type == 'used_mobile' )
        {
            $this->db->select ( "/*+ index(P_PDTMASTER P_PDTMASTER_PK)*/ A.SELL_FORCED, A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, C.IMG_PATH, D.AMT", false ) ;
            $this->db->where( 'A.TYPE_CD', '2505' ) ;
        }

        $this->db->where('A.ISUSE', 'O') ;
        
        $this->db->group_by('A.SELL_FORCED, A.PDT_CD, A.PDT_NAME, A.PDT_MODEL, C.IMG_PATH, D.AMT') ;
        $this->db->order_by('A.PDT_CD', 'desc') ;

        return $this->db->get()->result_array() ;
    }

    function list_count($sfl, $stx, $vender = '', $in_type = '') 
    {       
        //$this->_get_search_cache($sfl, $stx);
        
        $this->db->select('COUNT(DISTINCT A.PDT_CD) AS CNT') ;    
        $this->db->from('P_PDTMASTER A') ;        

        if ( $vender != '' )
        {
            $this->db->where( 'TEL_COMPANY', $vender ) ;
        }

        if ( $in_type == 'mobile' || $in_type == 'wired' )
        {
            $this->db->join( 'P_PDTTELPRICE B', 'A.PDT_CD = B.PDT_CD', 'left') ;

            if ( $in_type == 'mobile' )
            {
                $this->db->where( 'A.TYPE_CD', '2502' ) ;
            }
            else if ( $in_type == 'wired' )
            {
                $this->db->where( 'A.TYPE_CD', '2504' ) ;
            }
            
            $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN B.S_DATE AND B.E_DATE") ;
        }
        else if ( $in_type == 'card' )
        {
            $this->db->where( 'A.TYPE_CD', '2506' ) ;
        }
        else if ( $in_type == 'used_mobile' )
        {
            $this->db->where( 'A.TYPE_CD', '2505' ) ;
        }
        
        $this->db->where('A.ISUSE', 'O') ;

        return $this->db->get()->row()->CNT ;
    }  

    function get_policy_info_list($in_pdt_cd)
    {
        $select = "/*+ index(P_PDTTELPRICE P_PDTTELPRICE_PK)*/ 
                    B.SELL_FORCED, A.PDT_CD, S_DATE, E_DATE, PRICE_KIND, C.CP_NAME AS PRICE_NAME, B.TEL_COMPANY,
                    REG_KIND, D.REASON_NAME AS REG_NAME, PDT_NAME, PDT_MODEL, PRICE_DISCOUNT, PRICE_DISCOUNT2, E.IMG_PATH, E.DETAIL_IMG_PATH, 
                    (SELECT (POINT1 + A.POINT1) as POINT1 FROM P_PDTSELL WHERE A.PDT_CD = PDT_CD AND TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN S_DATE AND E_DATE) as POINT1,
                    (SELECT (POINT2 + A.POINT2) as POINT2 FROM P_PDTSELL WHERE A.PDT_CD = PDT_CD AND TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN S_DATE AND E_DATE) as POINT2" ;                

        $this->db->select ( $select, false ) ;
        $this->db->from ( "P_PDTTELPRICE A" ) ;
        $this->db->join( 'P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD', 'left') ;     
        $this->db->join('P_CALLING_PLAN C', 'C.CP_CD = A.PRICE_KIND', 'left') ;
        $this->db->join( 'S_REASON D', 'A.REG_KIND = D.REASON_CD', 'left') ;   
        $this->db->join( 'P_PDTIMAGE E', 'A.PDT_CD = E.PDT_CD', 'left') ;

        $this->db->where('A.PDT_CD', $in_pdt_cd) ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN A.S_DATE AND A.E_DATE") ;
        $this->db->where('A.ISUSE', 'O') ;                
        $this->db->where('B.ISUSE', 'O') ;                
        $this->db->where('C.CP_USE', 'O') ;
        $this->db->where('D.ISUSE', 'O') ;
        $this->db->where('(POINT1 + A.POINT1) >= ', 0, false) ;                

        $this->db->order_by("CP_NAME", "ASC") ;
        $this->db->order_by("REG_KIND", "ASC") ; 

        return $this->db->get()->result_array() ;
    }

    function get_used_mobile_policy_info($in_pdt_cd)
    {
        $select = "A.PDT_CD, A.TEL_COMPANY, A.PDT_NAME, A.PDT_MODEL, B.AMT, B.S_DATE, B.E_DATE, B.POINT1, B.POINT2, C.IMG_PATH, C.DETAIL_IMG_PATH" ;

        $this->db->select ( $select, false ) ;
        $this->db->from ( "P_PDTMASTER A" ) ;
        $this->db->join( 'P_PDTSELL B', 'A.PDT_CD = B.PDT_CD', 'left') ;
        $this->db->join( 'P_PDTIMAGE C', 'A.PDT_CD = C.PDT_CD', 'left') ;

        $this->db->where('A.PDT_CD', $in_pdt_cd) ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN B.S_DATE AND B.E_DATE") ;
        $this->db->where('A.ISUSE', 'O') ;

        return $this->db->get()->row_array() ;
    }

    function get_color_info($in_pdt_cd)
    {
        $this->db->select ( "/*+ index(P_PDTDETAIL P_PDTDETAIL_PK)*/ PDTDETAIL_NO, OPT_VALUE", false ) ;
        $this->db->from ( "P_PDTDETAIL" ) ;        
        $this->db->where('PDT_CD', $in_pdt_cd) ;

        $result = $this->db->get()->result_array() ;

        $colors = array() ;
        foreach ($result as $key => $value) 
        {
            $colors[$key] = $value ;

            if ( $value['OPT_VALUE'] == '블랙' )
            {
                $colors[$key]['class'] = 'color-black' ;
            }
            else if ( $value['OPT_VALUE'] == '화이트' )
            {
                $colors[$key]['class'] = 'color-white' ;
            }   
            else if ( $value['OPT_VALUE'] == '골드' )
            {
                $colors[$key]['class'] = 'bg-color-yellow' ;
            } 
            else if ( $value['OPT_VALUE'] == '블루' )
            {
                $colors[$key]['class'] = 'bg-color-blue' ;
            } 
            else if ( $value['OPT_VALUE'] == '로즈골드' )
            {
                $colors[$key]['class'] = 'bg-color-pink' ;
            }             
            else
            {
                $colors[$key]['class'] = 'color-two' ;
            }            
        }

        return $colors ;
    }

    function get_stock_price($in_pdt_cd)
    {
        $this->db->limit(1, 0) ;

        $this->db->select ( "/*+ index(P_PDTSELL P_PDTSELL_IDX1)*/ PRICE, VAT, AMT", false ) ;
        $this->db->from ( "P_PDTSELL" ) ;        
        $this->db->where('PDT_CD', $in_pdt_cd) ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN S_DATE AND E_DATE") ;
        $this->db->order_by('S_DATE', 'DESC') ;

        return $this->db->get()->row() ;
    }

    function get_point_info($in_pdt_cd)
    {
        $this->db->limit(1, 0) ;

        $this->db->select ( "/*+ index(P_PDTSELL P_PDTSELL_IDX1)*/ POINT1, POINT2, POINT3, POINT4", false ) ;
        $this->db->from ( "P_PDTSELL" ) ;        
        $this->db->where('PDT_CD', $in_pdt_cd) ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN S_DATE AND E_DATE") ;
        $this->db->order_by('S_DATE', 'DESC') ;

        return $this->db->get()->row() ;
    }

    function get_detail_info($in_pdt_cd, $in_price_kind, $in_reg_kind = '')
    {
        $price_info = $this->get_point_info($in_pdt_cd) ;

        $this->db->limit(1, 0) ;

        $select = "/*+ index(P_PDTTELPRICE)*/ 
                    PTP_ID, POINT1, POINT2, POINT3, POINT4, S_DATE, E_DATE, PRICE_DISCOUNT" ;

        $this->db->select ( $select, false ) ;
        $this->db->from ( "P_PDTTELPRICE A" ) ;
        $this->db->where(array('PDT_CD' => $in_pdt_cd, 'PRICE_KIND' => $in_price_kind)) ;

        if ( $in_reg_kind != '' )
        {
            $this->db->where('REG_KIND', $in_reg_kind) ;
        }

        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN S_DATE AND E_DATE") ;
        $this->db->order_by('S_DATE', 'DESC') ;

        $result = $this->db->get()->row_array() ;

        if ( $price_info )
        {
            $result['POINT1'] += $price_info->POINT1 ;
            $result['POINT2'] += $price_info->POINT2 ;
        }

        return $result ; 
    }

    function get_receipt_kind()
    {
        $this->db->select( "REASON_CD, REASON_NAME" ) ;
        $this->db->where( "REASON_KIND", 7 ) ;

        $result = $this->db->get('S_REASON')->result_array() ;

        $receipt_kind = array() ;

        foreach ($result as $value) 
        {
            if ( $value['REASON_NAME'] != '직배' )
            {
                $receipt_kind[$value['REASON_CD']] = $value['REASON_NAME'] ;
            }
        }

        return $receipt_kind ;
    }

    function get_master_info($in_pdt_cd)
    {
        $this->db->select ( "/*+ index(P_PDTMASTER_PK)*/ A.TEL_COMPANY, A.PDT_NAME, A.COMPANY_CD, A.BRAND_CD, A.IS_TELPRODUCT, B.PDTDETAIL_NO", false ) ;
        $this->db->from('P_PDTMASTER A') ;
        $this->db->join('P_PDTDETAIL B', 'A.PDT_CD = B.PDT_CD', 'left') ;
        $this->db->where('A.PDT_CD', $in_pdt_cd) ;

        return $this->db->get()->row() ;
    }

    function get_telprice_info($in_ptp_id)
    {
        $this->db->select ( "/*+ index(P_PDTTELPRICE_PK)*/ PRICE_KIND, REG_KIND, PRICE_DISCOUNT, PRICE_DISCOUNT2, POINT1, POINT2, POINT3, POINT4", false ) ;
        $this->db->from ( "P_PDTTELPRICE" ) ;        
        $this->db->where('PTP_ID', $in_ptp_id) ;

        return $this->db->get()->row() ;        
    }

    function get_sum_point_info($in_ptp_id, $in_pdt_cd)
    {
        $telprice_info = $this->get_telprice_info($in_ptp_id) ;
        $point_info = $this->get_point_info($in_pdt_cd) ;

        if ( $point_info )
        {
            $telprice_info->POINT1 += $point_info->POINT1 ;
            $telprice_info->POINT2 += $point_info->POINT2 ;
            $telprice_info->POINT3 += $point_info->POINT3 ;
            $telprice_info->POINT4 += $point_info->POINT4 ;
        }

        return $telprice_info ;
    }

    function get_usim_info($in_tel_company, $is_prepayment = false)
    {
        $this->db->select('A.PDT_CD, A.PDT_NAME, B.PDTDETAIL_NO, C.PRICE, C.VAT, C.AMT, C.POINT1, C.POINT2, C.POINT3, C.POINT4') ;
        $this->db->from('P_PDTMASTER A') ;
        $this->db->join('P_PDTDETAIL B', 'A.PDT_CD = B.PDT_CD', 'left') ;
        $this->db->join('P_PDTSELL C', 'A.PDT_CD = C.PDT_CD', 'left') ;
        $this->db->where(array('A.TYPE_CD' => '2503', 'A.TEL_COMPANY' => $in_tel_company, 'A.ISUSE' => 'O')) ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN C.S_DATE AND C.E_DATE") ;

        if ( $in_tel_company == '3207')
        {
            if ($is_prepayment) {
                $this->db->where("A.PDT_NAME LIKE '%선불%'");
            }
            else {
                //$this->db->where("A.PDT_NAME NOT LIKE '%선불%'") ;
            }
        }

        return $this->db->get()->row_array() ;
    }

    function get_pdt_star_count_list()
    {
        $this->db->select('PDT_CD, COUNT(*) AS CNT') ;
        $this->db->from('P_PDTTELPRICE') ;
        $this->db->group_by('PDT_CD') ;
        $this->db->order_by('CNT', 'DESC') ;

        $result = $this->db->get()->result_array() ;

        $list = array() ;
        foreach ($result as $key => $value) 
        {
            $list[$value['PDT_CD']] = $value['CNT'] / 50 ;
        }

        return $list ;
    }

    function get_pdt_star_count($in_pdt_cd)
    {
        $this->db->select('COUNT(*) AS CNT') ;
        $this->db->from('P_PDTTELPRICE') ;
        $this->db->where('PDT_CD', $in_pdt_cd ) ;

        $result = $this->db->get()->row_array() ;

        return isset($result['CNT']) ? ($result['CNT'] / 50) : 0 ;
    }
}
?>