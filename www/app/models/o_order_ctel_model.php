<?php
class O_Order_Ctel_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function get_order_info($in_ord_no)
    {        
        $select = "A.ORDNO, A.RECEPTION_ID, B.PV1, B.PV2, C.PDT_NAME, D.REG_NAME, D.REG_BIRTHDAY, D.INS_ZIPCODE, D.INS_ADDR1,
                   D.INS_DATE, D.DEP_BIRTHDAY, D.CARD_VALID_DATE, E.IMG_PATH, A.ORD_DATE,
                   PKG_CRYPTO_FIELD.get('REG_MOBILE', D.REG_MOBILE, ,'9','".G_CRYPTO."') as REG_MOBILE,
                   PKG_CRYPTO_FIELD.get('REG_TEL', D.REG_TEL, ,'9','".G_CRYPTO."') as REG_TEL,
                   PKG_CRYPTO_FIELD.get('INS_ADDR2', D.INS_ADDR2, ,'9','".G_CRYPTO."') as INS_ADDR2,
                   PKG_CRYPTO_FIELD.get('EMAIL, ', D.EMAIL, ,'9','".G_CRYPTO."') as EMAIL,
                   DECODE(D.AUTO_TRANS_TYPE, '1', '통장자동이체', '카드자동이체') AS AUTO_TRANS_TYPE,
                   PKG_CRYPTO_FIELD.get('DEPOSITOR', D.DEPOSITOR, ,'9','".G_CRYPTO."') as DEPOSITOR,
                   DECODE(D.AUTO_TRANS_TYPE, '1', (SELECT BANK_NAME FROM S_BANK WHERE BANK_CD = D.BANK_CD), 
                   (SELECT CARD_NAME FROM S_CARD WHERE CARD_CD = D.CARD_CD)) AS PAY_NAME,
                   DECODE(D.AUTO_TRANS_TYPE, '1', PKG_CRYPTO_FIELD.get('BANK_ACCOUNT', D.BANK_ACCOUNT, ,'9','".G_CRYPTO."'), 
                   PKG_CRYPTO_FIELD.get('CARD_ACCOUNT', D.CARD_ACCOUNT, ,'9','".G_CRYPTO."')) AS PAY_ACCOUNT,
                   (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.CENTER_CD) as CENTER_NAME,
                   (SELECT PKG_CRYPTO_FIELD.get('USERNAME', USERNAME, ,'9','".G_CRYPTO."') FROM D_MEMBER WHERE USERID = A.RECEPTION_ID) as RECEPTION_NAME, 
                   F.ORD_MSG, G.CP_NAME AS PRICE_KIND, H.SELLER_ID" ;

        $this->db->select($select, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;
        $this->db->join('O_ORDER_CTEL D', 'A.ORDNO = D.ORDNO') ;                                                                   
        $this->db->join('P_PDTIMAGE E', 'B.PDT_CD = E.PDT_CD', 'left') ;  
        $this->db->join('O_ORDERPERSON F', 'A.ORDNO = F.ORDNO') ;   
        $this->db->join('P_CALLING_PLAN G', 'G.CP_CD = D.PRICE_KIND', 'left') ;    
        $this->db->join('O_ORDERSTATE H', 'H.ORDNO = A.ORDNO', 'left') ;                                                                                      

        $this->db->where('A.ORDNO', $in_ord_no) ;        

        return $this->db->get()->row_array() ;       
    }
}
?>