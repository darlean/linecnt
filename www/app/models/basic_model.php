<?php
class Basic_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get_login($in_mb_id, $in_mb_pw)
	{
		$fields = "A.USERID, B.CENTER_CD, 
				   A.PASSWD, B.LGU_REG_NO, B.KAIT_NO,
				   B.LGU_REG_CHECK, B.SKT_REG_CHECK, B.KT_REG_CHECK,
				   PKG_CRYPTO.ONE_ENCRYPT('$in_mb_pw') as PASSWD2,						   
				   PKG_CRYPTO_FIELD.get('USERNAME',B.USERNAME,'9','".G_CRYPTO."') as USERNAME, 
				   PKG_CRYPTO_FIELD.get('MOBILE',B.MOBILE,'9','".G_CRYPTO."') as MOBILE,
				   B.RANK_CD, B.REG_DATE,
				   C.FULL_NAME AS RANK_NAME, B.ISCLASS, B.IS_RECEPTION,
				   (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = B.CENTER_CD) as CENTER_NAME" ;

		$this->db->select($fields, false) ;
		$this->db->from('D_DISTRIBUTE A, D_MEMBER B, S_RANK C') ;

		$where = "A.USERID = B.USERID 
				  AND B.RANK_CD = C.RANK_CD
				  AND B.ISCLASS = 'O' 
				  AND A.USERID = '$in_mb_id'" ;

		$this->db->where($where, NULL, false) ;

		return $this->db->get()->row_array() ;
	}

	// 회원 정보
	function get_member($mb_id, $fields='*') 
	{
		if (!$mb_id) return FALSE;

		if ( $fields != '*' )
		{
			$arr_field = explode(',', $fields) ;
		}
		else
		{
			$query = "select COLUMN_NAME from user_tab_columns WHERE table_name='D_MEMBER'" ;
			$result = $this->db->query($query)->result_array() ;

			$arr_field = '' ;
			foreach ($result as $key => $value) 
			{
				$arr_field[$key] = $value['COLUMN_NAME'] ;
			}
		}

		$fields = '' ;
		foreach ($arr_field as $key => $value) 
		{				
			if ( $value == 'ACCOUNT' || $value == 'DEPOSITOR' || $value == 'TEL' || $value == 'MOBILE'
				 || $value == 'MOBILE' || $value == 'ADDR2' || $value == 'EMAIL' || $value == 'USERNAME' )
			{
				$fields .= "PKG_CRYPTO_FIELD.get('{$value}',{$value},'9','".G_CRYPTO."') as {$value}," ;
			}
			else if ( $value == 'JUMIN_NO' )
			{
				$fields .= "PKG_CRYPTO_FIELD.get('{$value}',{$value},'1','".G_CRYPTO."') as {$value}," ;
			}
			else
			{
				$fields .= $value."," ;
			}
		}
		
		$this->db->select($fields, false);
		return $this->db->get_where('D_MEMBER', array('USERID' => $mb_id))->row_array();
	}

	// 게시판 정보
	function get_board($bo_table, $fields='*', $gr_join='') 
	{
        if (!$bo_table) return FALSE;

        if ($gr_join) 
        {
	        $sql = 'SELECT '.$fields.', 
					b."gr_id", b."gr_subject", b."gr_admin" 
					FROM "HB_BOARD" a 
					JOIN "HB_BOARD_GROUP" b ON a."gr_id" = b."gr_id"
					WHERE "bo_table" = \''.$bo_table.'\'';
		}
		else
		{
			$sql = 'SELECT '.$fields.'					
					FROM "HB_BOARD" 					
					WHERE "bo_table" = \''.$bo_table.'\'';
		}

		return $this->db->query($sql)->row_array();
        
		/*$gr_field = '';
		if ($gr_join) {
			$gr_join = 'a';
			$this->db->join('HB_BOARD_GROUP b', 'a.gr_id = b.gr_id');
			$gr_field = ', b.gr_id, b.gr_subject, b.gr_admin ';
		}

		$this->db->select($fields.$gr_field);
		return $this->db->get_where('HB_BOARD '.$gr_join, array('bo_table' => $bo_table))->row_array();*/
	}
	
	// 게시글 정보
	function get_write($bo_table, $wr_ids, $fields) {
		if (!$wr_ids) return FALSE;

		$this->db->select($fields, false);
		$this->db->where('bo_table', $bo_table);
		if (is_array($wr_ids)) {
			$this->db->where_in('wr_id', $wr_ids);
			return $this->db->get('HB_WRITE')->result_array();
		}
		else {
			return $this->db->get_where('HB_WRITE', array(
				'wr_id' => $wr_ids
			))->row_array();
		}
	}
}
?>