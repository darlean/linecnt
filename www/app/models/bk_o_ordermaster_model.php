<?php
class Bk_o_ordermaster_model extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

    function list_result($sst, $sod, $sfl, $stx, $limit, $offset, $vender)
    {
        $this->_get_search_cache($sfl, $stx);

        if ($sst && $sod)
        {
            $sst = ($sst == 'ORDNO') ? 'A.ORDNO' : $sst ;

            $this->db->order_by($sst, $sod);
        }

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

       $select = "A.ORDNO, A.ORD_DATE, Z.USERID, A.ORD_MSG, A.RECEPTION_ID, A.REG_NAME, A.DEV_NAME AS PDT_NAME,
                PKG_CRYPTO_FIELD.get('ORD_NAME', A.ORD_NAME, ,'9','".G_CRYPTO."') as ORD_NAME,
                PKG_CRYPTO_FIELD.get('MOBILE', Z.MOBILE, ,'9','".G_CRYPTO."') as MOBILE,
                (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.CENTER_CD) as ORDER_CENTER,
                DECODE(A.ORD_TYPE, '1', NVL(A.GT_CD, '재발급요청필요'), GT_CD) AS GT_CDS,
                (SELECT PKG_CRYPTO_FIELD.get('USERNAME', USERNAME, ,'9','".G_CRYPTO."') FROM D_MEMBER WHERE USERID = A.RECEPTION_ID) as RECEPTION_NAME" ;

        $this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID') ;

        $this->db->select($select, false) ;

        if ( $vender != '' )
        {
            $this->db->where('A.AGENCY_CD', $vender) ;
        }

        if ( IS_AGENCY )
        {
            $this->db->where('A.AGENCY_CD', $this->session->userdata('AGENCY_CD')) ;
        }
        else if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD')) ;
        }

        return $this->db->get('BK_O_ORDERMASTER A')->result_array() ;
    }

    function list_count($sfl, $stx, $vender)
    {
        $this->_get_search_cache($sfl, $stx);

        // 기간 검색
        $this->_get_search_date_range() ;

        if ( $vender != '' )
        {
            $this->db->where('A.AGENCY_CD', $vender) ;
        }

        if ( IS_AGENCY )
        {
            $this->db->where('A.AGENCY_CD', $this->session->userdata('AGENCY_CD')) ;
        }
        else if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD')) ;
        }

        return $this->db->count_all_results('BK_O_ORDERMASTER A') ;
    }

    // 검색 구문을 얻는다.
    function _get_search_cache($search_field, $search_text)
    {
        if (!$search_field || !$search_text)
            return FALSE;

        $search_field = $search_field ;

        $where = '' ;

        if ( $search_field == 'ORD_NAME' )
        {
            $where .= "PKG_CRYPTO_FIELD.get('ORD_NAME', A.ORD_NAME, '9','".G_CRYPTO."') LIKE ".$this->db->escape('%'.$search_text.'%');
        }
        else if ( $search_field == 'A.USERID' || $search_field == 'RECEPTION_ID' )
        {
            $where .= $this->db->protect_identifiers($search_field).' LIKE \'%'.$search_text.'%\'';
        }
        else if ( $search_field == 'PDT_NAME' )
        {
            $where .= $this->db->protect_identifiers('A.DEV_NAME').' LIKE \'%'.$search_text.'%\'';
        }
        else
        {
            if (preg_match('/[a-zA-Z]/', $search_text))
                $where .= 'LOWER('.$this->db->protect_identifiers($search_field).') LIKE LOWER('.$this->db->escape('%'.$search_text.'%').')';
            else
                $where .= $this->db->protect_identifiers($search_field).' LIKE '.$this->db->escape('%'.$search_text.'%');
        }

        $this->db->where($where, null, FALSE);
    }

    // 기간 검색
    function _get_search_date_range()
    {
        $sdt = $this->input->get('sdt') ;

        if ( $sdt == "REAL_OPEN_DT4" )
            return ;
        
        $start_date = $this->input->get('start');   // 시작날짜
        $finish_date = $this->input->get('finish'); // 종료날짜

        if ( $start_date != '' )
        {
            $this->db->where($sdt.' >= ', "'".$start_date."'", false) ;
        }

        if ( $finish_date != '' )
        {
            $Y = substr($finish_date, 0, 4) ;
            $M = substr($finish_date, 4, 2) ;
            $D = substr($finish_date, -2) ;
            $finish = date("Ymd", mktime(0, 0, 0, $M, $D + 1, $Y)) ;

            $this->db->where($sdt.' < ', "'".$finish."'", false) ;
        }
    }
}
?>