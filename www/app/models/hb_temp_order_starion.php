<?php
class HB_temp_order_starion extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

	function insert($in_stockprice_info, $in_point_info)
	{
		$data = array(
		    'PDT_CD'       		=> $_POST["PDT_CD"],
	        'USERID'      		=> $_POST["hb_recipient_id"],
	        'ORD_NAME'     		=> $_POST["reg_name"],
			'ORD_MOBILE'    	=> $_POST["reg_mobile"],
	        'INS_ZIPCODE'   	=> $_POST["home_zonecode"],
	        'INS_ADDR1'     	=> $_POST["home_addr1"],
	        'INS_ADDR2' 		=> $_POST["home_addr2"],
	        'INS_DATE' 			=> $_POST["reg_date"],
	        'PAY_TYPE'          => $_POST["sel_pay_type"],
	        'DEPOSITOR'		 	=> $_POST["bank_pay_owner"],
	        'DEP_BIRTHDAY'	 	=> "",
			'CARD_CD'	 		=> "",
			'CARD_ACCOUNT'	 	=> "",
			'CARD_VALID_DATE'	=> "",
            'CARD_CVC'          => "",
            'CARD_INSTALMENT_TYPE'  => '0',
			'ORD_MSG'	 		=> $_POST["ord_msg"],
			'CENTER_CD'	 		=> $_POST["ord_center"],
			'RECEPTION_ID'	 	=> $_POST["USERID"],
            'GIFT_TYPE'         => isset($_POST["gift_type"]) ? $_POST["gift_type"] : 0,
			'PRICE'				=> $in_stockprice_info->PRICE,
			'VAT'				=> $in_stockprice_info->VAT,
			'AMT'				=> $in_stockprice_info->AMT,
            'ORDER_BY_WEB'      => 'O',
            'POINT1'            => $in_point_info->POINT1,
            'POINT2'            => $in_point_info->POINT2,
            'POINT3'            => $in_point_info->POINT3,
            'POINT4'            => $in_point_info->POINT4,
	        ) ;

		if ( $_POST["sel_pay_type"] == '2' )
        {
            $data['DEPOSITOR']	 	        = $_POST["card_pay_owner"] ;
            $data['DEP_BIRTHDAY']	 	    = $_POST["pay_birth"] ;
            $data['CARD_CD']                = $_POST["pay_card"] ;
            $data['CARD_ACCOUNT']           = $_POST["pay_account"] ;
            $data['CARD_VALID_DATE']        = $_POST["pay_limit_month"].$_POST["pay_limit_year"] ;
            $data['CARD_CVC']               = $_POST["CARD_CVC"] ;
            $data['CARD_INSTALMENT_TYPE']   = $_POST["CARD_INSTALMENT_TYPE"] ;
        }

		$result->STATUS = $this->db->insert('HB_TEMP_ORDER_STARION', $data) ;

		if ( $result->STATUS )
		{
			$result->STATUS = 1 ;
            $result->MESSAGE = "상품 신청이 완료되었습니다." ;
		}	
		else
		{
			$result->STATUS = 0 ;
			$result->MESSAGE = "상품 신청이 실패하였습니다. 잠시 후 다시 시도해 주세요." ;
		}

		return $result ;
	}

    function insert2($in_stockprice_info, $in_point_info)
    {
        $data = array(
            'PDT_CD'       		=> $_POST["PDT_CD"],
            'USERID'      		=> $_POST["USERID"],
            'ORD_NAME'     		=> $_POST["ORD_NAME"],
            'ORD_MOBILE'    	=> $_POST["ORD_MOBILE"],
            'INS_ZIPCODE'   	=> $_POST["INS_ZIPCODE"],
            'INS_ADDR1'     	=> $_POST["INS_ADDR1"],
            'INS_ADDR2' 		=> $_POST["INS_ADDR2"],
            'INS_DATE' 			=> $_POST["INS_DATE"],
            'PAY_TYPE'          => $_POST["PAY_TYPE"],
            'DEPOSITOR'		 	=> $_POST["DEPOSITOR"],
            'DEP_BIRTHDAY'	 	=> $_POST["DEP_BIRTHDAY"],
            'CARD_CD'	 		=> $_POST["CARD_CD"],
            'CARD_ACCOUNT'	 	=> $_POST["CARD_ACCOUNT"],
            'CARD_VALID_DATE'	=> $_POST["CARD_VALID_DATE"],
            'CARD_CVC'          => $_POST["CARD_CVC"],
            'CARD_INSTALMENT_TYPE'  => $_POST["CARD_INSTALMENT_TYPE"],
            'ORD_MSG'	 		=> $_POST["ORD_MSG"],
            'CENTER_CD'	 		=> $_POST["CENTER_CD"],
            'RECEPTION_ID'	 	=> $_POST["RECEPTION_ID"],
            'GIFT_TYPE'         => $_POST["GIFT_TYPE"],
            'PRICE'				=> $in_stockprice_info->PRICE,
            'VAT'				=> $in_stockprice_info->VAT,
            'AMT'				=> $in_stockprice_info->AMT,
            'POINT1'            => $in_point_info->POINT1,
            'POINT2'            => $in_point_info->POINT2,
            'POINT3'            => $in_point_info->POINT3,
            'POINT4'            => $in_point_info->POINT4,
        ) ;

        $result->STATUS = $this->db->insert('HB_TEMP_ORDER_STARION', $data) ;

        if ( $result->STATUS )
        {
            $result->STATUS = 1 ;
            $result->MESSAGE = "상품 신청이 완료되었습니다." ;
        }
        else
        {
            $result->STATUS = 0 ;
            $result->MESSAGE = "상품 신청이 실패하였습니다. 잠시 후 다시 시도해 주세요." ;
        }

        return $result ;
    }

	function list_count($sfl, $stx, $state, $type = "")
	{
		$this->_get_search_cache($sfl, $stx);

		// 기간 검색
		$this->_get_search_date_range() ;

		$this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD') ;
		$this->db->join('D_MEMBER C', 'C.USERID = A.USERID') ;

        $this->db->where('A.STATE_CD != ', '4', false) ;

		if ( $state != '' )
		{
			$this->db->where('A.STATE_CD', $state) ;
		}

		$USERID = ( $this->input->post('USERID') != '' ) ? $this->input->post('USERID') : $this->session->userdata('ss_mb_id') ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
		{
			$this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD')) ;
		}
		else if ( !IS_MANAGER )
		{
            if ( $type == "reception" )
            {
                $this->db->where('A.RECEPTION_ID', $USERID) ;
            }
            else
            {
                $this->db->where('A.USERID', $USERID) ;
            }
		}

		return $this->db->count_all_results('HB_TEMP_ORDER_STARION A') ;
	}

	function list_result($sst, $sod, $sfl, $stx, $limit, $offset, $state, $type = "")
	{
		$this->_get_search_cache($sfl, $stx);

		// 기간 검색
		$this->_get_search_date_range() ;

		if ($sst && $sod)
		{
			$sst = ($sst == 'TMP_ORDNO') ? 'A.TMP_ORDNO' : $sst ;

			$this->db->order_by($sst, $sod);
		}

		$this->db->limit($limit, $offset) ;

		$select = "A.TMP_ORDNO, A.USERID, A.ORD_NAME, A.ORD_MOBILE, A.ORD_MSG, A.CENTER_CD, TO_CHAR(A.ORD_DATE, 'YYYY-MM-DD HH24:MI:SS') AS ORD_DATE, A.STATE_CD, A.GIFT_TYPE, A.ORDER_BY_WEB, A.PDT_CD, 
				TO_CHAR(A.ORD_DATE, 'YYYYMMDD') AS ORD_DATE2, (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.CENTER_CD) as CENTER_NAME, DECODE(A.PAY_TYPE, '1', '계좌이체', '카드결제') AS PAY_TYPE,	
				B.PDT_NAME, PKG_CRYPTO_FIELD.get('MOBILE', C.MOBILE, ,'9','".G_CRYPTO."') as MOBILE, PKG_CRYPTO_FIELD.get('USERNAME', C.USERNAME, ,'9','".G_CRYPTO."') as USERNAME" ;

		$this->db->select( $select, false ) ;

		$this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD') ;
		$this->db->join('D_MEMBER C', 'C.USERID = A.USERID') ;

        $this->db->where('A.STATE_CD != ', '4', false) ;

		if ( $state != '' )
		{
			$this->db->where('A.STATE_CD', $state) ;
		}

        $USERID = ( $this->input->post('USERID') != '' ) ? $this->input->post('USERID') : $this->session->userdata('ss_mb_id') ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD')) ;
        }
        else if ( !IS_MANAGER && !IS_CARD_AGENCY )
        {
            if ( $type == "reception" )
            {
                $this->db->where('A.RECEPTION_ID', $USERID) ;
            }
            else
            {
                $this->db->where('A.USERID', $USERID) ;
            }
        }

		return $this->db->get('HB_TEMP_ORDER_STARION A')->result_array() ;
	}

	// 검색 구문을 얻는다.
	function _get_search_cache($search_field, $search_text)
	{
		if (!$search_field || !$search_text)
			return FALSE;

		$search_field = $search_field ;
		$where = '' ;

		if ( $search_field == 'USERNAME' )
		{
			$where .= "PKG_CRYPTO_FIELD.get('USERNAME', C.USERNAME, '9','".G_CRYPTO."') LIKE ".$this->db->escape('%'.$search_text.'%');
		}
		else
		{
			$where .= $this->db->protect_identifiers($search_field) . " LIKE '%" . $search_text . "%'";
		}

		$this->db->where($where, null, FALSE);
	}

	// 기간 검색
	function _get_search_date_range()
	{
		$sdt = $this->input->get('sdt') ;
		$start_date = $this->input->get('start');   // 시작날짜
		$finish_date = $this->input->get('finish'); // 종료날짜

		if ( $start_date != '' )
		{
			$this->db->where("TO_CHAR(".$sdt.", 'YYYYMMDD') >= ", "'".$start_date."'", false) ;
		}

		if ( $finish_date != '' )
		{
			$Y = substr($finish_date, 0, 4) ;
			$M = substr($finish_date, 4, 2) ;
			$D = substr($finish_date, -2) ;
			$finish = date("Ymd", mktime(0, 0, 0, $M, $D + 1, $Y)) ;

			$this->db->where("TO_CHAR(".$sdt.", 'YYYYMMDD') < ", "'".$finish."'", false) ;
		}
	}

	function update($in_ord_no, $in_sql)
	{
		$this->db->where('TMP_ORDNO', $in_ord_no) ;
		return $this->db->update('HB_TEMP_ORDER_STARION', $in_sql) ;
	}

    function delete($in_ord_no)
    {
        $this->db->where('TMP_ORDNO', $in_ord_no) ;
        return $this->db->delete('HB_TEMP_ORDER_STARION') ;
    }

	function get_order_info($in_ord_no)
	{
		$select = "A.TMP_ORDNO, A.ORD_NAME AS REG_NAME, A.ORD_MOBILE AS REG_MOBILE, A.INS_ZIPCODE, A.INS_ADDR1, A.INS_ADDR2, A.INS_DATE, A.STATE_CD, A.GIFT_TYPE,
					(SELECT PKG_CRYPTO_FIELD.get('USERNAME', USERNAME,'9','".G_CRYPTO."') FROM D_MEMBER WHERE USERID = A.RECEPTION_ID) AS RECEPTION_NAME, A.RECEPTION_ID,					
					(SELECT PKG_CRYPTO_FIELD.get('USERNAME', USERNAME,'9','".G_CRYPTO."') FROM D_MEMBER WHERE USERID = A.USERID) AS USER_NAME, A.USERID,
					A.DEPOSITOR, A.DEP_BIRTHDAY, A.CARD_VALID_DATE, A.ORD_MSG, A.PDT_CD, TO_CHAR(A.ORD_DATE, 'YYYY-MM-DD HH24:MI:SS') AS ORD_DATE,
					DECODE(A.PAY_TYPE, '1', '계좌이체', '카드결제') AS PAY_TYPE,					 
					(SELECT CARD_NAME FROM S_CARD WHERE CARD_CD = A.CARD_CD) AS PAY_NAME,
					A.CARD_ACCOUNT AS PAY_ACCOUNT, A.CARD_CVC, A.CARD_INSTALMENT_TYPE,
					(SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.CENTER_CD) as CENTER_NAME,
                   	B.PDT_NAME, B.PDT_MODEL, C.AMT, D.IMG_PATH" ;

		$this->db->select($select, false) ;

		$this->db->from('HB_TEMP_ORDER_STARION A') ;
		$this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD') ;
        $this->db->join('P_PDTSELL C', 'A.PDT_CD = C.PDT_CD') ;
		$this->db->join('P_PDTIMAGE D', 'A.PDT_CD = D.PDT_CD', 'left') ;

		$this->db->where('A.TMP_ORDNO', $in_ord_no) ;
        $this->db->where("TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN C.S_DATE AND C.E_DATE") ;

        return $this->db->get()->row_array() ;
	}

	function get_temp_order_info($in_ord_no)
	{
		$this->db->where('TMP_ORDNO', $in_ord_no) ;

		return $this->db->get('HB_TEMP_ORDER_STARION')->row_array() ;
	}

    function get_real_order_info($in_ord_no)
    {
        $select = "A.ORDNO, A.ORD_DATE, A.RECEPTION_ID, A.AMT, A.USERID,
                B.AMT, C.PDT_NAME, C.PDT_MODEL, E.IMG_PATH, C.PDT_CD, 
                D.ORD_MSG, D.RECV_ZIPCODE AS INS_ZIPCODE, D.RECV_ADDR1 AS INS_ADDR1, 
                PKG_CRYPTO_FIELD.get('RECV_NAME', D.RECV_NAME, ,'9','".G_CRYPTO."') as REG_NAME, 
                PKG_CRYPTO_FIELD.get('RECV_MOBILE', D.RECV_MOBILE, ,'9','".G_CRYPTO."') as REG_MOBILE, 
                PKG_CRYPTO_FIELD.get('RECV_ADDR2', D.RECV_ADDR2, ,'9','".G_CRYPTO."') as INS_ADDR2,
                (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.CENTER_CD) as CENTER_NAME,
                DECODE(A.ORD_TYPE, '1', NVL(A.GT_CD, '재발급요청필요'), GT_CD) AS GT_CDS, A.GT_CD,
                (SELECT PKG_CRYPTO_FIELD.get('USERNAME', USERNAME, ,'9','".G_CRYPTO."') FROM D_MEMBER WHERE USERID = A.USERID) as USER_NAME,
                (SELECT PKG_CRYPTO_FIELD.get('USERNAME', USERNAME, ,'9','".G_CRYPTO."') FROM D_MEMBER WHERE USERID = A.RECEPTION_ID) as RECEPTION_NAME" ;

        $this->db->select($select, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;
        $this->db->join('O_ORDERPERSON D', 'A.ORDNO = D.ORDNO') ;
        $this->db->join('P_PDTIMAGE E', 'E.PDT_CD = B.PDT_CD', 'left') ;

        $this->db->where('A.ORDNO', $in_ord_no) ;

        return $this->db->get()->row_array() ;
    }

    function execute_procedure($p_name, $params, $bCommit = true)
    {
        $pramList = "";

        foreach ($params as $key => $value)
        {
            if($pramList!="") $pramList .= ", " ;
            $pramList .= ":" . $key ;
        }

        $query = "begin ".$p_name."(" . $pramList . "); end;" ;

        $stmt = oci_parse($this->db->conn_id, $query) ;

        foreach ($params as $key => $value)
        {
            if($key=="STATUS" || $key=="MESSAGE" || substr($key,0,3)=="NEW")
                oci_bind_by_name($stmt, ":" . $key, $params->$key, 100) ;
            else
                oci_bind_by_name($stmt, ":" . $key, $params->$key) ;
        }

        if(!ociexecute($stmt, OCI_DEFAULT))
        {
            oci_rollback($this->db->conn_id) ;
            die ;
        }

        if ( $params->STATUS == '1' && $bCommit )
        {
            oci_commit($this->db->conn_id) ;
        }

        return $params ;
    }

    function create_order_normal($order_info, $in_pdt_master_info)
    {
        $params->USERID         = $order_info['USERID'] ;
        $params->RECEPTION_ID   = $order_info['RECEPTION_ID'] ;
        $params->ORD_KIND   = '2601'; // 주문구분(초도, 재구매, 초도+재구매 등), S_REAND.REASON_KIND:26
        $params->CENTER_CD  = $order_info['CENTER_CD'] ;
        $params->START_CENTER = '9990' ;
        $params->PRICE      = (int)$order_info['PRICE'] ;
        $params->VAT        = (int)$order_info['VAT'] ;
        $params->AMT        = (int)$order_info['AMT'] ;
        $params->PV1        = (int)$order_info['POINT1'] ;
        $params->PV2        = (int)$order_info['POINT2'] ;
        $params->PV3        = (int)$order_info['POINT3'] ;
        $params->PV4        = (int)$order_info['POINT4'] ;
        $params->CRYPTO     = G_CRYPTO ;
        $params->NEW_ORDER  = "" ;
        $params->STATUS     = "" ;
        $params->MESSAGE    = "" ;

        return $this->execute_procedure("PKG_WEB.CREATE_ORDER_NORMAL", $params, false) ;
    }

    function create_detail($in_ord_no, $order_info, $in_pdt_master_info)
    {
        $params->ORDNO          = $in_ord_no ;
        $params->PDT_CD         = $order_info['PDT_CD'] ;
        $params->PDTDETAIL_NO   = $in_pdt_master_info->PDTDETAIL_NO ;
        $params->QTY            = 1 ;
        $params->PRICE          = (int)$order_info['PRICE'] ;
        $params->VAT            = (int)$order_info['VAT'] ;
        $params->AMT            = (int)$order_info['AMT'] ;
        $params->PV1        	= (int)$order_info['POINT1'] ;
        $params->PV2        	= (int)$order_info['POINT2'] ;
        $params->PV3        	= (int)$order_info['POINT3'] ;
        $params->PV4        	= (int)$order_info['POINT4'] ;
        $params->COMPANY_CD     = $in_pdt_master_info->COMPANY_CD ;
        $params->RECEIPT_KIND   = '0702' ;
        $params->BARCODE        = '' ;
        $params->IS_TELPRODUCT  = $in_pdt_master_info->IS_TELPRODUCT ; // 미출고상품유무(PDTMASTER.IS_TELPRODUCT)
        $params->WORK_USER      = $order_info['USERID'] ;
        $params->NEW_ORDPDT     = "" ;
        $params->STATUS         = "" ;
        $params->MESSAGE        = "" ;

        return $this->execute_procedure("PKG_ORDER_TEL.CREATE_DETAIL", $params, false) ;
    }

    function create_delivery($in_ord_no, $order_info)
    {
        $params->ORDNO          = $in_ord_no ;
        $params->ORD_TEL        = $order_info['ORD_MOBILE'] ;
        $params->ORD_MOBILE     = $order_info['ORD_MOBILE'] ;
        $params->ORD_EMAIL      = isset($order_info['ORD_EMAIL']) ? $order_info['ORD_EMAIL'] : '' ;
        $params->RECV_NAME      = $order_info['ORD_NAME'] ;
        $params->RECV_TEL       = $order_info['ORD_MOBILE'] ;
        $params->RECV_MOBILE    = $order_info['ORD_MOBILE'] ;
        $params->RECV_ZIPCODE   = $order_info['INS_ZIPCODE'] ;
        $params->RECV_ADDR1     = $order_info['INS_ADDR1'] ;
        $params->RECV_ADDR2     = $order_info['INS_ADDR2'] ;
        $params->ORD_MSG        = $order_info['TMP_ORDNO'] ; // 기존 temp 정보를 읽어오기위함
        $params->CRYPTO         = G_CRYPTO ;
        $params->STATUS         = "" ;
        $params->MESSAGE        = "" ;

        return $this->execute_procedure("PKG_ORDER_TEL.CREATE_DELIVERY", $params, false) ;
    }

    // 결제 정보 입력
    function add_order_money($in_ord_no, $order_info)
    {
        $order_info['PAY_TYPE'] = ($order_info['PAY_TYPE'] == '2') ? '3' : $order_info['PAY_TYPE'] ;

        $params->ORDNO                  = $in_ord_no ;
        $params->KIND         	        = $order_info['PAY_TYPE'] ;   // 결제구분(1:현금,2:무통장,3:신용카드)
        $params->AMT          	        = $order_info['AMT'] ;        // 결제금액(입금액)

        // 카드
        if ( $order_info['PAY_TYPE'] == '3' )
        {
            $params->ADMIT_NO           = "" ;		                            // 계좌번호(s_admitbank.abank_cd)
            $params->ADMIT_NAME         = "" ;                                  // 입금자
            $params->ADMIT_DATE         = "" ;                                  // 입금일
            $params->CARD_CD            = $order_info['CARD_CD'] ;              // 카드사코드(s_card.card_cd)
            $params->CARD_INST          = $order_info['CARD_INSTALMENT_TYPE'] ; // 할부개월수(일시불=0, 그외 개월수 입력)
            $params->CARD_NO            = $order_info['CARD_ACCOUNT'] ;         // 카드번호(암호화됨)
            $params->CARD_OWNER         = $order_info['DEPOSITOR'] ;            // 카드소유자
        }
        else // 1:현금,2:무통장
        {
            $params->ADMIT_NO           = "" ;		                    // 계좌번호(s_admitbank.abank_cd)
            $params->ADMIT_NAME         = $order_info['DEPOSITOR'] ;    // 입금자
            $params->ADMIT_DATE         = "" ;                          // 입금일
            $params->CARD_CD            = "" ;                          // 카드사코드(s_card.card_cd)
            $params->CARD_INST          = "" ;                          // 할부개월수(일시불=0, 그외 개월수 입력)
            $params->CARD_NO            = "" ;                          // 카드번호(암호화됨)
            $params->CARD_OWNER         = "" ;                          // 카드소유자
        }

        $params->I_CARD_APP_NO          = "" ; //  	string	15		승인번호
        $params->CARD_APP_DATE          = "" ; //	string	8		승인일
        $params->CRYPTO                 = G_CRYPTO ;
        $params->STATUS                 = "" ;
        $params->MESSAGE                = "" ;

        return $this->execute_procedure("PKG_ORDER_TEL.ADD_ORDER_MONEY", $params, true) ;
    }
}
?>