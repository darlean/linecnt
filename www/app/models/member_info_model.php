<?php
class Member_info_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function fillable_supporter($in_member_id)
	{
		$this->db->where('P_ID', $in_member_id) ;
		$cnt = $this->db->count_all_results('D_MEMBER') ;

		if ( $cnt == 2 )		
			return false ;

		return true ;
	}

    function fillable_supporter2($in_member_id)
    {
        $this->db->where('P_ID', $in_member_id) ;
        $cnt = $this->db->count_all_results('D_MEMBER') ;

        return $cnt ;
    }

	function fillable_recommender($in_recommender_id, $in_supporter_id)
	{
		$sql = "SELECT COUNT(1) AS CNT
				FROM D_MEMBER
       			WHERE USERID = '{$in_recommender_id}'
       			START WITH USERID = '{$in_supporter_id}'
       			CONNECT BY USERID = PRIOR P_ID" ;

   		return $this->db->query($sql)->row()->CNT ;    
	}

	function execute_procedure($p_name, $params)
	{
		$pramList = "";

        foreach ($params as $key => $value) 
        {
            if($pramList!="") $pramList .= ", " ;
            $pramList .= ":" . $key ;
        }

        $query = "begin ".$p_name."(" . $pramList . "); end;" ;

		$stmt = oci_parse($this->db->conn_id, $query) ;
		
		foreach ($params as $key => $value) 
		{
            if($key=="STATUS" || $key=="MESSAGE" || substr($key,0,3)=="NEW")
                oci_bind_by_name($stmt, ":" . $key, $params->$key, 100) ;
            else
                oci_bind_by_name($stmt, ":" . $key, $params->$key) ;
        }

		if(!ociexecute($stmt, OCI_DEFAULT))
        {
            oci_rollback($this->db->conn_id) ;
            die ;
        }

        oci_commit($this->db->conn_id) ;
		
		return $params ;
	}

	function check_jumin()
	{
		$params->JUMIN_NO	= $this->input->post('mb_jumin_no') ;
		$params->CRYPTO		= G_CRYPTO ;
		$params->STATUS		= "" ;
		$params->MESSAGE	= "" ;

		return $this->execute_procedure("PKG_WEB.CHECK_JUMIN", $params) ;
	}

	function insert() 
	{
		$ismailing    = ($this->input->post('is_mailing') == 'on') ? 'O' : 'X' ;

		$params->USERNAME		= $this->input->post('mb_name') ;
		$params->JUMIN_NO		= $this->input->post('mb_birth') ;
		$params->LOGIN_ID		= '';
		$params->PASSWD			= $this->input->post('mb_password') ;
		$params->TEL			= "{$this->input->post('mb_tel_1')}-{$this->input->post('mb_tel_2')}-{$this->input->post('mb_tel_3')}" ;
		$params->MOBILE			= "{$this->input->post('mb_hp_1')}-{$this->input->post('mb_hp_2')}-{$this->input->post('mb_hp_3')}" ;
		$params->EMAIL			= $this->input->post('mb_email') ;
		$params->ISMAILING		= $ismailing;
		$params->ZIPCODE		= $this->input->post('mb_zonecode') ;
		$params->ADDR1			= $this->input->post('mb_addr1') ;
		$params->ADDR2			= $this->input->post('mb_addr2') ;
		$params->P_ID			= $this->input->post('mb_supporter_id') ;
		$params->R_ID			= $this->input->post('mb_recommander_id') ;
		$params->CENTER_CD		= $this->input->post('mb_center') ;
		$params->BANK_CD		= $this->input->post('mb_bank') ;
		$params->ACCOUNT		= $this->input->post('mb_bank_serial') ;
		$params->DEPOSITOR		= $this->input->post('mb_bank_owner') ;
		$params->RELATION		= $this->input->post('mb_owner_relation') ;
		$params->IS_RECEPTION	= 'X' ;
		$params->CRYPTO			= G_CRYPTO ;
		$params->NEW_USERID		= "" ;
		$params->STATUS			= "" ;
		$params->MESSAGE		= "" ;
	
		return $this->execute_procedure("PKG_WEB.REG_MEMBER", $params) ;	
	}

	function update() 
	{
		$value = array(
			'TEL' => $this->input->post('mb_tel_1').'-'.$this->input->post('mb_tel_2').'-'.$this->input->post('mb_tel_3'),
			'MOBILE' => $this->input->post('mb_hp_1').'-'.$this->input->post('mb_hp_2').'-'.$this->input->post('mb_hp_3'),
			'EMAIL' => $this->input->post('mb_email'),
			'ZIPCODE' => $this->input->post('mb_zonecode'),
			'ADDR1' => $this->input->post('mb_addr1'),
			'ADDR2' => $this->input->post('mb_addr2'),
		);

		$sql = "UPDATE D_MEMBER SET TEL = PKG_CRYPTO.encrypt3('{$value['TEL']}', '".G_CRYPTO."'), 
				MOBILE = PKG_CRYPTO.encrypt3('{$value['MOBILE']}', '".G_CRYPTO."'), 
				EMAIL = PKG_CRYPTO.encrypt3('{$value['EMAIL']}', '".G_CRYPTO."'), 
				ZIPCODE = '{$value['ZIPCODE']}', 
				ADDR1 = '{$value['ADDR1']}', 
				ADDR2 = PKG_CRYPTO.encrypt3('{$value['ADDR2']}', '".G_CRYPTO."') 
				WHERE USERID = '{$this->input->post('mb_id')}'" ;

		$result = $this->db->query($sql) ;

		if ( $result && $this->input->post('password') )
        {
        	$sql = "UPDATE D_DISTRIBUTE SET PASSWD = PKG_CRYPTO.ONE_ENCRYPT('{$this->input->post('password')}') 
        			WHERE USERID = '{$this->input->post('mb_id')}'" ;

        	$result = $this->db->query($sql) ;
        }

		return $result ;
	}

	function get_member_name_by_id($in_member_id, $bfullname=false)
	{
		if ( $bfullname )
		{
			$this->db->select("PKG_CRYPTO_FIELD.get('USERNAME',USERNAME,'9','".G_CRYPTO."') as USERNAME", false) ;
		}
		else
		{
			$this->db->select("V_USERNAME as USERNAME") ;
		}

		$this->db->where(array('USERID' => $in_member_id)) ;
		$this->db->from('D_MEMBER') ;

		$username = $this->db->get()->row()->USERNAME ;

		return $username ? $username : '' ;
	}

	function get_member_info_by_name_id(/*$in_member_name,*/ $in_member_id)
	{
		$this->db->where(array(/*'USERNAME' => "PKG_CRYPTO.ENCRYPT3('$in_member_name','".G_CRYPTO."')", */'USERID' => "'$in_member_id'"), NULL, false) ;
		$this->db->from('D_MEMBER') ;

		return $this->db->get()->row_array() ;
	}

	function get_reg_no_by_id_pw($in_mb_id, $in_mb_pw)
	{
		$select = "A.PASSWD, B.LGU_REG_NO, B.KAIT_NO,
				   PKG_CRYPTO.ONE_ENCRYPT('$in_mb_pw') as PASSWD2" ;

		$this->db->select($select, false) ;
		$this->db->from('D_DISTRIBUTE A, D_MEMBER B') ;

		$where = "A.USERID = B.USERID 
				  AND B.ISCLASS = 'O' 
				  AND A.USERID = '$in_mb_id'" ;

		$this->db->where($where, NULL, false) ;

		return $this->db->get()->row_array() ;
	}

	function get_member_info_by_id($in_member_id)
	{
		$select = "REG_DATE, USERID, ZIPCODE, ADDR1, P_ID, R_ID, RANK_CD, IS_RECEPTION, LGU_REG_NO, KAIT_NO,
				   PKG_CRYPTO_FIELD.get('USERNAME', USERNAME,'9','".G_CRYPTO."') as USERNAME, 
				   PKG_CRYPTO_FIELD.get('JUMIN_NO', JUMIN_NO,'1','".G_CRYPTO."') as JUMIN_NO, 
				   PKG_CRYPTO_FIELD.get('TEL', TEL,'9','".G_CRYPTO."') as TEL,
				   PKG_CRYPTO_FIELD.get('MOBILE', MOBILE,'9','".G_CRYPTO."') as MOBILE,
				   PKG_CRYPTO_FIELD.get('EMAIL', EMAIL,'9','".G_CRYPTO."') as EMAIL,
				   PKG_CRYPTO_FIELD.get('ADDR2', ADDR2,'9','".G_CRYPTO."') as ADDR2,
				  (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.CENTER_CD) as CENTER_NAME,
				  (SELECT FULL_NAME FROM S_RANK WHERE RANK_CD = A.RANK_CD) as RANK_NAME,
				  (SELECT NVL(SUM(PV1),0) FROM O_ORDERMASTER WHERE ORD_TYPE IN('1', '2') AND ISORDER_OK = 'O' AND USERID = A.USERID ) as CV,
               (SELECT NVL(SUM(PV2),0) FROM O_ORDERMASTER WHERE ORD_TYPE IN('1', '2') AND ISORDER_OK = 'O' AND USERID = A.USERID ) as PV" ;

		$this->db->select($select, false) ;
		$this->db->where(array('A.USERID' => $in_member_id)) ;
		$this->db->from('D_MEMBER A') ;

		return $this->db->get()->row_array() ;
	}

	function get_detail_member_info_by_id($in_member_id)
	{
		$select = "REG_DATE, USERID, ZIPCODE, ADDR1, P_ID, R_ID,
				   PKG_CRYPTO_FIELD.get('USERNAME', USERNAME,'9','".G_CRYPTO."') as USERNAME, 
				   PKG_CRYPTO_FIELD.get('JUMIN_NO', JUMIN_NO,'1','".G_CRYPTO."') as JUMIN_NO, 
				   PKG_CRYPTO_FIELD.get('TEL',TEL,'9','".G_CRYPTO."') as TEL,
				   PKG_CRYPTO_FIELD.get('MOBILE', MOBILE,'9','".G_CRYPTO."') as MOBILE,
				   PKG_CRYPTO_FIELD.get('EMAIL', EMAIL,'9','".G_CRYPTO."') as EMAIL,
				   PKG_CRYPTO_FIELD.get('ADDR2', ADDR2,'9','".G_CRYPTO."') as ADDR2,
				   PKG_CRYPTO_FIELD.get('DEPOSITER', DEPOSITOR,'9','".G_CRYPTO."') as DEPOSITOR,
				   PKG_CRYPTO_FIELD.get('ACCOUNT', ACCOUNT,'9','".G_CRYPTO."') as ACCOUNT,
				  (SELECT CENTER_NAME FROM S_CENTER WHERE CENTER_CD = A.CENTER_CD) as CENTER_NAME,
				  (SELECT BANK_NAME FROM S_BANK WHERE BANK_CD = A.BANK_CD) as BANK_NAME,
				  (SELECT FULL_NAME FROM S_RANK WHERE RANK_CD = A.RANK_CD) as RANK_NAME" ;

		$this->db->select($select, false) ;
		$this->db->where(array('A.USERID' => $in_member_id)) ;
		$this->db->from('D_MEMBER A') ;

		return $this->db->get()->row_array() ;
	}

	function find_member_list( $in_username, $in_userid )
	{
		$sql = "SELECT USERID, 
			   	PKG_CRYPTO_FIELD.get('USERNAME',USERNAME,'1','".G_CRYPTO."') as USERNAME, 
			   	PKG_CRYPTO_FIELD.get('JUMIN_NO',JUMIN_NO,'1','".G_CRYPTO."') as JUMIN_NO, 
			   	ADDR1, 
			   	TO_CHAR(TO_DATE(REG_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') as REGDATES,
			   	PKG_CRYPTO_FIELD.get('TEL',TEL,'1','".G_CRYPTO."') as TEL,
			   	PKG_CRYPTO_FIELD.get('MOBILE',MOBILE,'1','".G_CRYPTO."') as MOBILE
			  	FROM D_Member
			 	WHERE IsClass   = 'O' 
			   	AND USERNAME = PKG_CRYPTO.encrypt3('$in_username','".G_CRYPTO."') 
			   	AND USERID = '$in_userid'";

		return $this->db->query($sql)->result_array();
	}

	function sub_member_two_my_tree( $in_user_id )
	{
		$this->db->select('USERID') ;
		$this->db->where('P_ID', $in_user_id ) ;
		$this->db->order_by('P_POS, P_ID') ;

		return $this->db->get('D_MEMBER')->result_array() ;
	}


	function list_result($sst, $sod, $sfl, $stx, $limit, $offset, $in_type, $sido = '')
    {        	
        $this->_get_search_cache($sfl, $stx);

        if ($sst && $sod)
        {           
            $this->db->order_by($sst, $sod);
        }

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        $select = "A.USERID, A.ZIPCODE, A.ADDR1, A.P_ID, A.R_ID, A.RANK_CD, CENTER_NAME,
        		   TO_CHAR(TO_DATE(A.REG_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') AS REG_DATE,
				   PKG_CRYPTO_FIELD.get('USERNAME', A.USERNAME,'9','".G_CRYPTO."') as USERNAME, 
				   PKG_CRYPTO_FIELD.get('TEL', A.TEL,'9','".G_CRYPTO."') as TEL,
				   PKG_CRYPTO_FIELD.get('MOBILE', A.MOBILE,'9','".G_CRYPTO."') as MOBILE,
                  (SELECT FULL_NAME FROM S_RANK WHERE RANK_CD = A.RANK_CD) as RANK_NAME,				  
               	  (SELECT NVL(SUM(PV2),0) FROM O_ORDERMASTER WHERE ORD_TYPE IN('1', '2') AND USERID = A.USERID ) as PV, 
               	  A.IS_RECEPTION, A.ISCLASS, A.LGU_REG_NO, A.KAIT_NO" ;

		$this->db->select($select, false) ;

		$this->db->from('D_MEMBER A') ;
        $this->db->join('S_CENTER B', 'B.CENTER_CD = A.CENTER_CD', 'LEFT') ;

        if ( $in_type == "reception" )
        {
        	$this->db->where('A.IS_RECEPTION', 'O') ;
        }       

        if ( $sido != '' )
        {
            $this->db->where('A.ADDR1 LIKE', '%'.$sido.'%') ;            
        }

        $this->db->where('A.USERID != ', 'D0000000') ;

        if ( IS_CENTER )
        {
        	$this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD')) ;            
        }        

        return $this->db->get()->result_array() ;
    }

    function list_count($sfl, $stx, $in_type, $sido = '') 
    {   
        $this->_get_search_cache($sfl, $stx);

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->from('D_MEMBER A') ;
        $this->db->join('S_CENTER B', 'B.CENTER_CD = A.CENTER_CD', 'LEFT') ;

        if ( $in_type == "reception" )
        {
        	$this->db->where('A.IS_RECEPTION', 'O') ;
        }

        if ( $sido != '' )
        {
            $this->db->where('A.ADDR1 LIKE', '%'.$sido.'%') ;            
        }        

        if ( IS_CENTER )
        {
        	$this->db->where('A.CENTER_CD', $this->session->userdata('CENTER_CD')) ;            
        }

        return $this->db->count_all_results() ;
    }

    // 검색 구문을 얻는다.
    function _get_search_cache($search_field, $search_text) 
    {        
        if (!$search_field || !$search_text)
            return FALSE;

        $search_field = $search_field ;

        $where = '' ;

        if ( $search_field == 'USERNAME' )
        {
        	//$where .= "USERNAME = PKG_CRYPTO.encrypt3('$search_text','".G_CRYPTO."')" ;

            $where .= "V_USERNAME LIKE '%".$search_text."%'" ;
        }
        else
        {
	        if (preg_match('/[a-zA-Z]/', $search_text))
	            $where .= "LOWER(".$this->db->protect_identifiers($search_field).") LIKE LOWER('%".$search_text."%')" ;
	        else
	            $where .= $this->db->protect_identifiers($search_field)." LIKE '%".$search_text."%'" ;
	    }
            
        $this->db->where($where, null, FALSE);
    }

    // 기간 검색
    function _get_search_date_range()
    {
        $sdt = $this->input->get('sdt') ;
        $start_date = $this->input->get('start');   // 시작날짜
        $finish_date = $this->input->get('finish'); // 종료날짜

        if ( $start_date != '' )
        {
            $this->db->where($sdt.' >= ', "'".$start_date."'", false) ;
        }

        if ( $finish_date != '' )
        {
            $this->db->where($sdt.' <= ', "'".$finish_date."'", false) ;
        }
    }

    function change_member_sp($in_user_id, $in_gubun, $in_old_id, $in_new_id )
    {
        $params->SP_Userid	    = $in_user_id ;
        $params->SP_Gubun       = $in_gubun ; // 1:후원인변경 2:추천인변경
        $params->SP_Old_ID      = $in_old_id ;
        $params->SP_New_ID      = $in_new_id ;
        $params->SP_Reason_CD   = "1400" ;
        $params->SP_Remark      = "" ;
        $params->SP_Work_User   = $in_user_id ;
        $params->SP_User_Kind   = "0901" ;
        $params->STATUS		= "" ;
        $params->MESSAGE	= "" ;

        return $this->execute_procedure("D_CHANGEMEMBER_SP", $params) ;
    }

    function get_member_count($in_Y, $in_M, $in_center = '')
    {
        if ( $in_M < 10 )
        {
            $in_M = '0'.$in_M ;
        }

        $start = $in_Y.$in_M.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_M + 1, 1, $in_Y)) ;

        $this->db->where('REG_DATE >=', $start) ;
        $this->db->where('REG_DATE <', $finish) ;

        if ( $in_center != '' )
        {
            $this->db->where('CENTER_CD', $in_center);
        }

        return $this->db->count_all_results('D_MEMBER') ;
    }

    function add_jumin_no($in_user_id, $in_jumin_no)
    {
        $sql = "UPDATE D_MEMBER SET JUMIN_NO = PKG_CRYPTO.encrypt3('".$in_jumin_no."','".G_CRYPTO."'), IS_INPUT = 'O' WHERE USERID = '{$in_user_id}'" ;
        return $this->db->query($sql) ;
    }
}
?>