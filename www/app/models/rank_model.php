<?php
class Rank_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	// 전월직급가져오기
    function get_prev_month_rank($in_user_id)
    {
        $this->db->select('A.FULL_NAME') ;
        $this->db->from('S_RANK A, PAY_MONTH B') ;

        $where = "A.RANK_CD = B.RANK_CD
                   AND B.USERID = '$in_user_id'
                   AND B.PAY_DATE = 
                   (SELECT MAX(PAY_DATE) FROM PAY_CALC_INFO 
                    WHERE PAY_KIND = '4' AND PAY_DATE < TO_CHAR(SYSDATE, 'YYYYMM'))" ;

        $this->db->where($where, NULL, false) ;

        $row = $this->db->get()->row() ;
        return ($row) ? $row->FULL_NAME : '' ;
    }

    // 현재직급 가져오기
    function get_cur_month_rank($in_user_id, $in_rank_name)
    {
        $this->db->select('A.FULL_NAME') ;
        /*$this->db->from('S_RANK A, PAY_MONTH B') ;

        $where = "A.RANK_CD = B.RANK_CD
                   AND B.USERID = '$in_user_id'
                   AND B.PAY_DATE = 
                   (SELECT MAX(PAY_DATE) FROM PAY_CALC_INFO 
                    WHERE PAY_KIND = '4' AND PAY_DATE <= TO_CHAR(SYSDATE, 'YYYYMM'))" ;*/

        $this->db->from('S_RANK A, D_MEMBER B') ;

        $where = "A.RANK_CD = B.RANK_CD
                   AND B.USERID = '$in_user_id'" ;  

        $this->db->where($where, NULL, false) ;

        $row = $this->db->get()->row() ;
        return ($row) ? $row->FULL_NAME : $in_rank_name ;
    }
}
?>