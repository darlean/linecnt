<?php
class Policy_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function list_result($in_pt_id, $sst, $sod, $sfl, $stx, $limit, $offset)
    {    
        $this->_get_search_cache($sfl, $stx);

        if ($sst && $sod)
            $this->db->order_by($sst, $sod);

        $this->db->limit($limit, $offset) ;
        $this->db->where('hb_product_for_policy.pt_id', $in_pt_id) ;
        $this->db->join('hb_calling_plan', 'hb_calling_plan.cp_idx = hb_product_for_policy.pc_calling_plan', 'left') ;

        return $this->db->get('hb_product_for_policy')->result_array() ;
    }

    function list_count($in_pt_id, $sfl, $stx) 
    {       
        $this->_get_search_cache($sfl, $stx);
        $this->db->where('pt_id', $in_pt_id) ;

        return $this->db->count_all_results('hb_product_for_policy');
    }

    // 검색 구문을 얻는다.
    function _get_search_cache($search_field, $search_text) 
    {        
        if (!$search_field || !$search_text)
            return FALSE;

        $search_field = 'hb_product_for_policy.'.$search_field ;

        $where = '' ;

        if (preg_match('/[a-zA-Z]/', $search_text))
            $where .= 'INSTR(LOWER('.$this->db->protect_identifiers($search_field).'), LOWER('.$this->db->escape($search_text).'))';
        else
            $where .= 'INSTR('.$this->db->protect_identifiers($search_field).', '.$this->db->escape($search_text).')';
            
        $this->db->where($where, null, FALSE);
    }

    function insert($in_sql) 
    {
        $this->db->insert('hb_product_for_policy', $in_sql) ;
    }

    function update($in_pc_id, $in_sql)
    {
        $this->db->where('pc_id', $in_pc_id) ;
        $this->db->update('hb_product_for_policy', $in_sql) ;
    }

    function get_policy_info($in_pc_id)
    {
        $this->db->join('hb_calling_plan', 'hb_calling_plan.cp_idx = hb_product_for_policy.pc_calling_plan', 'left') ;
        $this->db->where('pc_id', $in_pc_id) ;
        return $this->db->get('hb_product_for_policy')->row_array() ;
    }
}
?>