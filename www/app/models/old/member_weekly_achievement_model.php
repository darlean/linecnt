<?php
class Member_weekly_achievement_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
    }

    // 정산 ID 및 실적을 update
    function update_achievement ( $in_start_date, $in_end_date, $in_wa_id )
    {
    	// 좌실적 가져오기
    	$achievement_info_list = $this->_get_achievement_info_list($in_start_date, $in_end_date, $in_wa_id) ;
        if( FALSE == $achievement_info_list )
        { 
           return false ;
        }

        // 실적 내역 추가
        foreach ($achievement_info_list as $key => $value) 
        {
        	$result = $this->db->insert('hb_member_weekly_achievement', $value) ;
        	if( FALSE == $result )
            { 
               return false ;
            }
        }

        return true ;
    }

    private function _get_achievement_info_list( $in_start_date, $in_end_date, $in_wa_id )
    {
    	$left_achievement = $this->_get_achievement_sub_member($in_wa_id, true) ;
    	$right_achievement = $this->_get_achievement_sub_member($in_wa_id, false) ;
    	$lasted_days_list = $this->_get_last_recommand_date_list() ;
        $over_equal_position = $this->_get_over_equal_position() ;
        $total_cv_sub_member = $this->_get_total_cv_sub_member($in_wa_id) ;
        
    	$result = array() ;

    	foreach ($left_achievement as $key => $value) 
    	{
    		$result[$value['mb_no']] = $value ;
    	}

    	foreach ($right_achievement as $key => $value) 
    	{
    		$result[$value['mb_no']] += $value ;
    	}

        foreach ($result as $key => $value) 
        {
            $result[$key]['mbac_start_date'] = $in_start_date ;
            $result[$key]['mbac_end_date'] = $in_end_date ;
            $result[$key]['mbac_last_recommand_date'] = isset($lasted_days_list[$key]) ? $lasted_days_list[$key] : '' ;
            $result[$key]['mbac_over_position'] = isset($over_equal_position[$key]['over']) ? $over_equal_position[$key]['over'] : 0 ;
            $result[$key]['mbac_equal_position'] = isset($over_equal_position[$key]['equal']) ? $over_equal_position[$key]['equal'] : 0 ;
            $result[$key]['mbac_group_total_cv'] = isset($total_cv_sub_member[$key]) ? $total_cv_sub_member[$key] : 0 ;
        }

    	return $result ;
    }

    // 하위 직급 실적 가져오기
    private function _get_achievement_sub_member($in_wa_id, $b_left)
    {
        $this->db->from('hb_order') ;
        $this->db->join('member_nodes_paths', 'member_nodes_paths.descendant_mb_no = hb_order.mb_no') ;

        if ( $b_left )
        {
            $this->db->select('ancestor_mb_no as mb_no, count(hb_order.or_id) as mbac_left_achieve, wa_id') ;
            $this->db->join('hb_sub_tree_by_mb_no', 'member_nodes_paths.descendant_mb_no = hb_sub_tree_by_mb_no.st_left_no') ;
        }
        else
        {
            $this->db->select('ancestor_mb_no as mb_no, count(hb_order.or_id) as mbac_right_achieve, wa_id') ;
            $this->db->join('hb_sub_tree_by_mb_no', 'member_nodes_paths.descendant_mb_no = hb_sub_tree_by_mb_no.st_right_no') ;
        }
        
        $where = array(	'or_state' => '5', 'path_length != ' => '0', 'wa_id' => $in_wa_id ) ;

        $this->db->where($where, false) ;
        $this->db->group_by('ancestor_mb_no') ;
        
        return $this->db->get()->result_array() ;
    }

    // 하위 직급 total cv 가져오기
    private function _get_total_cv_sub_member($in_wa_id)
    {
        $this->db->select('ancestor_mb_no as mb_no, sum(op_commission_value) as mbac_group_total_cv') ;
        $this->db->from('hb_order_for_policy') ;
        $this->db->join('hb_order', 'hb_order.or_id = hb_order_for_policy.or_id') ;
        $this->db->join('member_nodes_paths', 'member_nodes_paths.descendant_mb_no = hb_order.mb_no') ;
        
        $where = array( 'or_state' => '5', 'path_length != ' => '0', 'wa_id' => $in_wa_id ) ;

        $this->db->where($where, false) ;
        $this->db->group_by('ancestor_mb_no') ;
        
        $result = $this->db->get()->result_array() ;

        $output = array() ;
        foreach ($result as $key => $value) 
        {
            $output[$value['mb_no']] = $value['mbac_group_total_cv'] ;
        }

        return $output ; 
    }

    // 최근 추천 받은 일자 가져오기
    private function _get_last_recommand_date_list()
    {
    	$select = "hb_member.mb_no as recomnander_mb_no, 
    			   max(recommander.mb_no) as mb_no, 
    			   max(recommander.mb_datetime) as mb_datetime" ;

    	$this->db->select($select) ;
    	$this->db->from('hb_member') ;
    	$this->db->join('hb_member as recommander', 'hb_member.mb_id = recommander.mb_recommander_id') ;
    	$this->db->group_by('hb_member.mb_no') ;

    	$result = $this->db->get()->result_array() ;

    	$lasted_days_list = array() ;
    	foreach ($result as $key => $value) 
    	{
    		$lasted_days = isset($value['mb_datetime']) ? floor((time() - strtotime($value['mb_datetime']))/60/60/24) : 0 ;
    		$lasted_days_list[$value['recomnander_mb_no']] = $lasted_days ;
    	}

    	return $lasted_days_list ; 
    }

    // 직급 역전 및 직급 동일 여부
     function _get_over_equal_position()
    {
        $select = "hb_member.mb_no, 
                   IF((hb_member.mb_position - max(supporter.mb_position)) < 0, 1, 0) as over, 
                   IF((hb_member.mb_position - max(supporter.mb_position)) = 0, 1, 0) as equal" ;

        $this->db->select($select, false) ;
        $this->db->from('hb_member') ;
        $this->db->join('hb_member as supporter', 'hb_member.mb_id = supporter.mb_supporter_id') ;
        $this->db->group_by('hb_member.mb_no') ;

        $result = $this->db->get()->result_array() ;

        $output = array() ;
        foreach ($result as $key => $value) 
        {
            $output[$value['mb_no']] = $value ;
        }

        return $output ; 
    }
}
?>