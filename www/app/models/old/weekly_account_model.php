<?php
class Weekly_account_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array(
                        'Member_weekly_achievement_model', 
                        'Member_weekly_direct_pay', 
                        'Member_weekly_bronze_for_sales_pay',
                        'Member_weekly_silver_for_sales_pay',
                        'Member_position_model',
                        )) ;
    }

    private function _get_order_info($start_date, $end_date)
    {
    	$start_date = date("Y-m-d", strtotime($start_date));
    	$end_daet = date("Y-m-d", strtotime($end_date.'+1 day'));
    	$sql 	=  "SELECT count(distinct(hb_order.or_id)) as cnt, SUM(`op_commission_value`) AS sum_cv, SUM(`op_promotion_value`) AS sum_pv
    				FROM (`hb_order`)
					LEFT JOIN `hb_order_delivery` ON `hb_order`.`or_id` = `hb_order_delivery`.`or_id`
					LEFT JOIN `hb_order_for_policy` ON `hb_order`.`or_id` = `hb_order_for_policy`.`or_id`
					LEFT JOIN `hb_member` ON `hb_order`.`mb_no` = `hb_member`.`mb_no`
					WHERE `wa_id` is NULL
					AND `or_open_date` >= '".$start_date."'
					AND `or_open_date` < '".$end_daet."' FOR UPDATE";

		$qry = $this->db->query($sql) ;
        return $qry->row() ;        
    }

    private function _get_confirm_order_info($start_date, $end_date, $wa_id)
    {
    	$start_date = date("Y-m-d", strtotime($start_date));
    	$end_daet = date("Y-m-d", strtotime($end_date.'+1 day'));
    	$sql 	=  "SELECT count(distinct(hb_order.or_id)) as cnt, SUM(`op_commission_value`) AS sum_cv, SUM(`op_promotion_value`) AS sum_pv
    				FROM (`hb_order`)
					LEFT JOIN `hb_order_delivery` ON `hb_order`.`or_id` = `hb_order_delivery`.`or_id`
					LEFT JOIN `hb_order_for_policy` ON `hb_order`.`or_id` = `hb_order_for_policy`.`or_id`
					LEFT JOIN `hb_member` ON `hb_order`.`mb_no` = `hb_member`.`mb_no`
					WHERE `wa_id` = '".$wa_id."'
					AND `or_open_date` >= '".$start_date."'
					AND `or_open_date` < '".$end_daet."'";

		$qry = $this->db->query($sql) ;
        return $qry->row() ;        
    }



    private function _insert_weekly_account($cnt, $sum_cv, $sum_pv, $payment_ratio, $start_date, $end_date)
    {
        $total_ratio = DIRECT_PAYMENT_RATIO + BRONZE_FOR_SALES_PAYMENT_RATIO + SILVER_FOR_SALES_PAYMENT_RATIO + GOLD_UPPER_BONUS_RATIO;
        if( $total_ratio != 1.0 )
            return FALSE;

    	$payment_date = date("Y-m-d", strtotime($end_date.'+2 week +2 day') );
    	$in_data = array( 
                            'wa_start_date' 		=> date("Y-m-d", strtotime($start_date) ), 
                            'wa_end_date'  			=> date("Y-m-d", strtotime($end_date) ),
                            'wa_total_order_count'  => $cnt,
                            'wa_total_cv'      		=> $sum_cv,
                            'wa_total_pv'      		=> $sum_pv,
                            'wa_direct_cv'          => $sum_cv * DIRECT_PAYMENT_RATIO,
                            'wa_bfs_cv'             => $sum_cv * BRONZE_FOR_SALES_PAYMENT_RATIO,
                            'wa_sfs_cv'             => $sum_cv * SILVER_FOR_SALES_PAYMENT_RATIO,

                            'wa_gold_upper_cv'      => $sum_cv * GOLD_UPPER_BONUS_RATIO * GOLD_UPPER_BONUS_RATIO_ALL,
                            'wa_gold_cv'            => $sum_cv * GOLD_UPPER_BONUS_RATIO * GOLD_UPPER_BONUS_RATIO_ONLY_GOLD,
                            'wa_platinum_cv'        => $sum_cv * GOLD_UPPER_BONUS_RATIO * GOLD_UPPER_BONUS_RATIO_ONLY_PLATINUM,

                            'wa_payment_date'      	=> $payment_date,
                        );    	

        $this->db->insert('hb_weekly_account', $in_data );

        return $this->db->insert_id();
    }

    private function _update_order_list($start_date, $end_date, $wa_id)
    {
    	$in_data = array( 'wa_id'    => $wa_id );

    	$where['or_open_date >='] = date("Y-m-d", strtotime($start_date) ) ;       
        $where['or_open_date <'] = date("Y-m-d", strtotime($end_date.'+1 day')) ;          
        $this->db->where($where) ;        
        $this->db->where('wa_id is NULL') ;

    	return $this->db->update('hb_order', $in_data );
    }

    function account($start_date, $end_date)
    {
    	$output = array();
        $output['result'] = FALSE;
        $output['msg'] = '';

    	// 트랜지션 시작
        $this->db->trans_begin();

        try
        {
        	// 주문 내역의 건수, 총 cv, pv 가져온다.
            $order_info = $this->_get_order_info($start_date, $end_date);                        
            if( FALSE == $order_info )
            { 
               throw new Exception('주문 내역을 가져오는데 실패했습니다.');
            }

            // 주간 정산 추가
            $wa_id = $this->_insert_weekly_account($order_info->cnt, $order_info->sum_cv, $order_info->sum_pv, DIRECT_PAYMENT_RATIO, $start_date, $end_date );
            if( FALSE == $wa_id )
            { 
               throw new Exception('주간 정산내역을 입력하는데 실패했습니다.');
            }

            // 정산할 주문건에 weekly id 추가한다.
            $this->_update_order_list($start_date, $end_date, $wa_id);

            // 처음에 가져온 건수, 총(cv, pv) 와 일치하는지 한번더 검사한다.
            $confirm_order_info = $this->_get_confirm_order_info($start_date, $end_date, $wa_id);
            if( $confirm_order_info->cnt != $order_info->cnt || 
            	$confirm_order_info->sum_cv != $order_info->sum_cv ||
            	$confirm_order_info->sum_pv != $order_info->sum_pv
            	)
            {
            	throw new Exception('주간 정산 오류(리스트와 업데이트 내역이 맞지 않습니다)');
            }           

            
            if ( FALSE == $this->Member_weekly_achievement_model->update_achievement($start_date, $end_date, $wa_id) )
            {
                throw new Exception("실적 내역 업데이트가 실패하였습니다.") ;
            }

            // 다이렉트 정착지원 수당 계산
            if( FALSE == $this->Member_weekly_direct_pay->update( $wa_id, PAYMENT_TERMS_COUNT, DIRECT_PAYMENT_RATIO ) )
            {
                throw new Exception("다이렉트 정착지원 수당 지급에 실패했습니다.");
            }

            // 다이렉트 정착지원 수당 계산
            if( FALSE == $this->Member_weekly_bronze_for_sales_pay->update( $wa_id ) )
            {
                throw new Exception("브론즈 영업지원 지급에 실패했습니다.");
            }

            // 다이렉트 정착지원 수당 계산
            if( FALSE == $this->Member_weekly_silver_for_sales_pay->update( $wa_id ) )
            {
                throw new Exception("실버 영업지원 지급에 실패했습니다.");
            }

            if ( FALSE == $this->Member_position_model->weekly_level_up($wa_id) )
            {
                throw new Exception("회차 자동 승급이 실패했습니다.") ;
            }

        	// 트랜지션 커밋!!
        	$this->db->trans_commit();

        	$output['result'] = TRUE;
            $output['msg'] = '주간 정산을 완료했습니다.';

            return $output;
        }
        catch( Exception $e )
        {
        	$this->db->trans_rollback();

            $output['msg'] = $e->getMessage();

            return $output;
        }        
    }
}