<?php
class Order_for_policy_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function insert($in_sql) 
    {
        $this->db->insert('hb_order_for_policy', $in_sql) ;

        return mysql_insert_id() ;
    }

    function update($in_op_id, $in_sql)
    {
        $this->db->where('op_id', $in_op_id) ;
        $this->db->update('hb_order_for_policy', $in_sql) ;
    }
}
?>