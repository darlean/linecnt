<?php
class Member_weekly_account_model extends CI_Model 
{
    function __construct() {
        parent::__construct();
    }

    function list_result($in_mb_id, $sst, $sod, $sfl, $stx, $limit, $offset)
    {    
        $this->_get_search_cache($sfl, $stx);

        if ($sst && $sod)
            $this->db->order_by($sst, $sod);

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        if( !IS_MANAGER )
        {
            $this->db->where('hb_member_weekly_account.mb_id', $in_mb_id) ;
        }

        $this->db->join('hb_weekly_account', 'hb_member_weekly_account.wa_id = hb_weekly_account.wa_id') ;
        $this->db->join('hb_member','hb_member.mb_no = hb_member_weekly_account.mb_no', 'left' );

        return $this->db->get('hb_member_weekly_account')->result_array() ;
    }

    function list_count($in_mb_id, $sfl, $stx) 
    {       
        $this->_get_search_cache($sfl, $stx);

        // 기간 검색
        $this->_get_search_date_range() ;

        if( !IS_MANAGER )
        {
            $this->db->where('mb_id', $in_mb_id) ;
        }

        $this->db->join('hb_weekly_account', 'hb_member_weekly_account.wa_id = hb_weekly_account.wa_id') ;

        return $this->db->count_all_results('hb_member_weekly_account');
    }

    // 검색 구문을 얻는다.
    function _get_search_cache($search_field, $search_text) 
    {        
        if (!$search_field || !$search_text)
            return FALSE;

        $search_field = $search_field ;

        $where = '' ;

        if (preg_match('/[a-zA-Z]/', $search_text))
            $where .= 'INSTR(LOWER('.$this->db->protect_identifiers($search_field).'), LOWER('.$this->db->escape($search_text).'))';
        else
            $where .= 'INSTR('.$this->db->protect_identifiers($search_field).', '.$this->db->escape($search_text).')';
            
        $this->db->where($where, null, FALSE);
    }

    // 기간 검색
    function _get_search_date_range()
    {
        $sdt = $this->input->get('sdt') ;
        $start_date = $this->input->get('start');   // 시작날짜
        $finish_date = $this->input->get('finish'); // 종료날짜

        if ( $start_date != '' )
        {
            $this->db->where($sdt.' >= ', "'".$start_date."'", false) ;
        }

        if ( $finish_date != '' )
        {
            $this->db->where($sdt.' <= ', "'".$finish_date."'", false) ;
        }
    }

    function get_sum_payment_by_type($in_mb_id)
    {
        $this->db->select('mbwa_payment_type, sum(mbwa_payment_amount) as amount') ;
        $this->db->group_by('mbwa_payment_type') ;

        if( !IS_MANAGER )
        {
            $this->db->where('mb_id', $in_mb_id) ;
        }

        $result = $this->db->get('hb_member_weekly_account')->result_array() ;

        $list = array() ;
        foreach ($result as $key => $value) 
        {
            $list[$value['mbwa_payment_type']] = $value['amount'] ;
        }

        return $list ;
    }
}
?>