<?php
class Member_weekly_direct_pay extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
    }

    private function _get_order_list($wa_id)
    {
        $sql    =  "SELECT op_commission_value, op_promotion_value, mb_no, hb_order.or_id
                    FROM (`hb_order`)
                    LEFT JOIN `hb_order_for_policy` ON `hb_order`.`or_id` = `hb_order_for_policy`.`or_id`
                    WHERE `wa_id` = '".$wa_id."'";

        $qry = $this->db->query($sql) ;

        return $qry->result_array() ;
    }

    private function _get_weekly_acount($wa_id)
    {
        $this->db->where('wa_id', $wa_id);

        return $this->db->get('hb_weekly_account')->row_array();
    }

    private function _get_sum_direct_payment_cv($wa_id, $payment_type)
    {
        $this->db->where('wa_id', $wa_id) ;
        $this->db->where('mbwa_payment_type', $payment_type);
        $this->db->where('mbwa_payment_state', PAYMENT_SUCCESS);
        
        $this->db->select_sum('mbwa_payment_amount', 'sum_payment');

        $query = $this->db->get('hb_member_weekly_account');

        return $query->row_array();   
    }

    private function _update_direct_cv_for_weekly_account($wa_id, $payment_cv, $non_payment_cv)
    {
        $in_data = array( 
                            'wa_direct_payment_cv'          => $payment_cv,
                            'wa_direct_non_payment_cv'      => $non_payment_cv,
                             );

             
        $this->db->where('wa_id', $wa_id) ;

        return $this->db->update('hb_weekly_account', $in_data );
    }

    function update($wa_id, $terms_count, $payment_ratio)
    {
        $weekly_account = $this->_get_weekly_acount($wa_id);
        if( FALSE == $weekly_account )
        {
            return FALSE;
        }

        // 미 지급 금액
        $non_payment_cv = 0;
        // 지급 금액
        $total_payment_cv = 0;
        // 주문 총 cv
        $total_order_payment_cv = 0;

        $order_list = $this->_get_order_list($wa_id);
        foreach ($order_list as $value)
        {
            // 1원단위 절삭
            $order_payment_cv = ( $value[ 'op_commission_value' ] * $payment_ratio );
            $payment_cv = $order_payment_cv / $terms_count;
            $payment_cv = floor($payment_cv/10)*10;

            $total_order_payment_cv += $order_payment_cv;

            $payment_cv_of_order = 0;

            $member_list = $this->_get_upper_member_list($value['mb_no'], $terms_count ) ;
            if( $member_list )
            {
                foreach ($member_list as $mb)
                {
                    // 비기너는 지급 안함
                    if( $mb['mb_position'] < BRONZE )
                    {
                        //$non_payment_cv_of_order += $payment_cv;
                        continue;
                    }

                    $payment_cv_of_order += $payment_cv;

                    $this->_insert_direct_pay($mb['mb_no'], $mb['mb_id'], $wa_id, $value['or_id'], $payment_cv, 
                                              SUPPORT_DIRECT, PAYMENT_SUCCESS );
                }

                $total_payment_cv += $payment_cv_of_order;
                $non_payment_cv += ($order_payment_cv - $payment_cv_of_order);
            }
        }

        
        $sum_payment = $this->_get_sum_direct_payment_cv( $wa_id, SUPPORT_DIRECT );
        if( FALSE == $sum_payment )
        {
            $sum_payment = array();
            $sum_payment['sum_payment'] = 0;
        }


        // DB에 지급 금액과 코드상 지급금액과 같은지 체크
        if( $sum_payment['sum_payment'] != $total_payment_cv )
            return FALSE;

        // 지급 금액이 정산했던 금액보다 크면 실패
        if( $sum_payment['sum_payment'] > $weekly_account['wa_direct_cv'] )
            return FALSE;

        if( FALSE == $this->_update_direct_cv_for_weekly_account($wa_id, $sum_payment['sum_payment'], $weekly_account['wa_direct_cv'] - $sum_payment['sum_payment'] ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    private function _insert_direct_pay($mb_no, $mb_id, $wa_id, $or_id, $payment_cv, $payment_type, $payment_state )
    {
        $this->load->config('cf_weekly_account') ;
        $cf_payment_state = $this->config->item('payment_state') ;

        $in_data = array( 
                            'mb_id'                 => $mb_id, 
                            'mb_no'                 => $mb_no,
                            'wa_id'                 => $wa_id,
                            'mbwa_payment_amount'   => $payment_cv,
                            'mbwa_payment_type'     => $payment_type,
                            'or_id'                 => $or_id,
                            'mbwa_payment_state'    => $payment_state,
                            'mbwa_payment_result'   => $cf_payment_state[$payment_state],
                        );      

        $this->db->insert('hb_member_weekly_account', $in_data );

        return $this->db->insert_id();
    }

    private function _get_upper_member_list($in_mb_no, $in_terms_count)
    {
        $this->db->select('hb_member.mb_no, hb_member.mb_position, hb_member.mb_id') ;
        $this->db->where(array('descendant_mb_no' => $in_mb_no, 'path_length != ' => '0', 'path_length <= ' => $in_terms_count)) ;
        $this->db->join('hb_member', 'hb_member.mb_no = member_nodes_paths.ancestor_mb_no', 'left');
        return $this->db->get('member_nodes_paths')->result_array() ;
    }
}