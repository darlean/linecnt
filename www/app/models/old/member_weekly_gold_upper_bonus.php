<?php
class Member_weekly_gold_upper_bonus extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
    }

    private function _get_weekly_acount($wa_id)
    {
        $this->db->where('wa_id', $wa_id);

        return $this->db->get('hb_weekly_account')->row_array();
    }

    private function _get_sum_group_total_cv($wa_id, $where)
    {
    	$this->db->select_sum('mbac_group_total_cv', 'sum_group_total_cv');

    	$this->db->where('wa_id', $wa_id);
    	$this->db->where( $where );

    	$this->db->join( 'hb_member', 'hb_member.mb_no = hb_member_weekly_achievement.mb_no', 'left' );
    	return $this->db->get('hb_member_weekly_achievement')->row_array();
    }

    private function _get_member_list($mb_position)
    {
    	$this->db->select( 'hb_member.mb_no, hb_member.mb_name, hb_member.mb_id, hb_member_weekly_achievement.mbac_group_total_cv' );
    	$this->db->where( $mb_position );
    	$this->db->join( 'hb_member_weekly_achievement', 'hb_member_weekly_achievement.mb_no = hb_member.mb_no', 'left' );

    	return $this->db->get('hb_member')->result_array();
    }

    private function _get_gold_uppper_member_list()
    {
    	$this->db->select( 'hb_member.mb_no, hb_member.mb_name, hb_member.mb_id, hb_member_weekly_achievement.mbac_group_total_cv' );
    	$this->db->where( 'mb_position >=', GOLD );
    	$this->db->join( 'hb_member_weekly_achievement', 'hb_member_weekly_achievement.mb_no = hb_member.mb_no', 'left' );

    	return $this->db->get('hb_member')->result_array();
    }

    private function _insert_pay($mb_no, $mb_id, $wa_id, $or_id, $payment_cv, $payment_type, $payment_state )
    {
        $this->load->config('cf_weekly_account') ;
        $cf_payment_state = $this->config->item('payment_state') ;

        $in_data = array( 
                            'mb_id'                 => $mb_id, 
                            'mb_no'                 => $mb_no,
                            'wa_id'                 => $wa_id,
                            'mbwa_payment_amount'   => $payment_cv,
                            'mbwa_payment_type'     => $payment_type,
                            'or_id'                 => $or_id,
                            'mbwa_payment_state'    => $payment_state,
                            'mbwa_payment_result'   => $cf_payment_state[$payment_state],
                        );      

        $this->db->insert('hb_member_weekly_account', $in_data );

        return $this->db->insert_id();
    }

    private function _get_sum_payment_cv($wa_id, $payment_type)
    {
        $this->db->where('wa_id', $wa_id) ;
        $this->db->where('mbwa_payment_type', $payment_type);
        $this->db->where('mbwa_payment_state', PAYMENT_SUCCESS);
        
        $this->db->select_sum('mbwa_payment_amount', 'sum_payment');

        $query = $this->db->get('hb_member_weekly_account');

        return $query->row_array();   
    }

    private function _update_gold_upper_for_weekly_account($wa_id, $payment_cv, $non_payment_cv)
    {
        $in_data = array( 
                            'wa_gold_upper_payment_cv'          => $payment_cv,
                            'wa_gold_upper_non_payment_cv'      => $non_payment_cv,
                        );

             
        $this->db->where('wa_id', $wa_id) ;

        return $this->db->update('hb_weekly_account', $in_data );
    }

    private function _update_gold_for_weekly_account($wa_id, $payment_cv, $non_payment_cv)
    {
        $in_data = array( 
                            'wa_gold_payment_cv'          => $payment_cv,
                            'wa_gold_non_payment_cv'      => $non_payment_cv,
                        );

             
        $this->db->where('wa_id', $wa_id) ;

        return $this->db->update('hb_weekly_account', $in_data );
    }

    private function _update_platinum_for_weekly_account($wa_id, $payment_cv, $non_payment_cv)
    {
        $in_data = array( 
                            'wa_platinum_payment_cv'          => $payment_cv,
                            'wa_platinum_non_payment_cv'      => $non_payment_cv,
                        );

             
        $this->db->where('wa_id', $wa_id) ;

        return $this->db->update('hb_weekly_account', $in_data );
    }

   	private function _payment_gold_upper($wa_id, $total_cv, $group_total_cv, $weekly_account)
   	{
   		$payment_cv = $total_cv * GOLD_UPPER_BONUS_RATIO_ALL;
   		$payment_cv = $payment_cv / $group_total_cv;

   


   		$member_list = $this->_get_gold_uppper_member_list();
    	$member_count = count($member_list);

    	$total_payment_cv = 0;

    	if( $member_count )
    	{
            foreach ($member_list as $mb)
            {
        		$mb['mbac_group_total_cv'] = floor($mb['mbac_group_total_cv']/100000)*100000;

        		$mb_cv = $payment_cv * $mb['mbac_group_total_cv'];
        		$mb_cv = floor($mb_cv/10)*10;

        		//var_dump($mb_cv);
            	$this->_insert_pay($mb['mb_no'], $mb['mb_id'], $wa_id, NULL, $mb_cv, SUPPORT_GOLD_UPPER_BONUS, PAYMENT_SUCCESS );

            	$total_payment_cv += $mb_cv;

            }
    	}

    	$sum_payment = $this->_get_sum_payment_cv($wa_id, SUPPORT_GOLD_UPPER_BONUS);
    	if( FALSE == $sum_payment )
        {
            $sum_payment = array();
            $sum_payment['sum_payment'] = 0;
        }

        // DB에 지급 금액과 코드상 지급금액과 같은지 체크
        if( $sum_payment['sum_payment'] != $total_payment_cv )
            return FALSE;

           // 지급 금액이 정산했던 금액보다 크면 실패
        if( $sum_payment['sum_payment'] > $weekly_account['wa_gold_upper_cv'] )
            return FALSE;

        // 지급 금액이 총 재원보다 많으면 실패
        if( $total_payment_cv > $total_cv )
        	return FALSE;

        if( FALSE == $this->_update_gold_upper_for_weekly_account($wa_id, $sum_payment['sum_payment'], $weekly_account['wa_gold_upper_cv'] - $sum_payment['sum_payment'] ) )
        {
            return FALSE;
        }

    	return TRUE;
   	}

   	private function _payment($wa_id, $total_cv, $group_total_cv, $weekly_account, $ratio, $position, $payment_type  )
   	{
   		$member_list = $this->_get_member_list( $position );
    	$member_count = count($member_list);

    	$total_payment_cv = 0;

    	if( $member_count )
    	{
    		$payment_cv = $total_cv * $ratio;
   			$payment_cv = $payment_cv / $group_total_cv; 

            foreach ($member_list as $mb)
            {
        		//$mb['mbac_group_total_cv'] = floor($mb['mbac_group_total_cv']/100000)*100000;

        		$mb_cv = $payment_cv * $mb['mbac_group_total_cv'];
        		//$mb_cv = floor($mb_cv/10)*10;

        		//var_dump($mb_cv);
            	$this->_insert_pay($mb['mb_no'], $mb['mb_id'], $wa_id, NULL, $mb_cv, $payment_type, PAYMENT_SUCCESS );

            	$total_payment_cv += $mb_cv;

            }
    	}

    	$sum_payment = $this->_get_sum_payment_cv($wa_id, $payment_type );
    	if( FALSE == $sum_payment )
        {
            $sum_payment = array();
            $sum_payment['sum_payment'] = 0;
        }

        // 지급 금액이 정산했던 금액보다 크면 실패
        if( $sum_payment['sum_payment'] > $weekly_account )
            return FALSE;

        // 지급 금액이 총 재원보다 많으면 실패
        if( $total_payment_cv > $total_cv )
        	return FALSE;

        switch ($payment_type) 
        {
        	case SUPPORT_GOLD_UPPER_BONUS:
        		if( FALSE == $this->_update_gold_upper_for_weekly_account($wa_id, $sum_payment['sum_payment'], $weekly_account - $sum_payment['sum_payment'] ) )
		        {
		            return FALSE;
		        }

        		break;

        	case SUPPORT_GOLD_BONUS:
        		if( FALSE == $this->_update_gold_for_weekly_account($wa_id, $sum_payment['sum_payment'], $weekly_account - $sum_payment['sum_payment'] ) )
		        {
		            return FALSE;
		        }
		        
        		break;

        	case SUPPORT_PLATINUM_BONUS:
        		if( FALSE == $this->_update_platinum_for_weekly_account($wa_id, $sum_payment['sum_payment'], $weekly_account - $sum_payment['sum_payment'] ) )
		        {
		            return FALSE;
		        }
		        
        		break;
        }

        

    	return TRUE;
   	}

    function update($wa_id)
    {
    	$weekly_account = $this->_get_weekly_acount($wa_id);
    	if( FALSE == $weekly_account )
    		return FALSE;

    	

    	//if( FALSE == $this->_payment_gold_upper( $wa_id, $total_cv, $sum_group_total_cv['sum_group_total_cv'], $weekly_account ) )
    	//	return FALSE;

    	// 골드 이상 공유 보너스
    	$sum_group_total_cv = $this->_get_sum_group_total_cv($wa_id, array( 'mb_position >= ' => GOLD ) );
		if( FALSE == $sum_group_total_cv )    		return FALSE;
    	$total_cv = $weekly_account['wa_total_cv'] * GOLD_UPPER_BONUS_RATIO;
    	if( FALSE == $this->_payment( 
    									$wa_id, 
    									$total_cv, 
    									$sum_group_total_cv['sum_group_total_cv'], 
    									$weekly_account['wa_gold_upper_cv'],
    									GOLD_UPPER_BONUS_RATIO_ALL,
    									array( 'mb_position >=' => GOLD ),
    									SUPPORT_GOLD_UPPER_BONUS
    									) )
    		return FALSE;

    	// 골드 공유 보너스
    	$sum_group_total_cv = $this->_get_sum_group_total_cv($wa_id, array( 'mb_position' => GOLD ) );
		if( FALSE == $sum_group_total_cv )    		return FALSE;
    	$total_cv = $weekly_account['wa_total_cv'] * GOLD_UPPER_BONUS_RATIO;
    	if( FALSE == $this->_payment( 
    									$wa_id, 
    									$total_cv, 
    									$sum_group_total_cv['sum_group_total_cv'], 
    									$weekly_account['wa_gold_cv'],
    									GOLD_UPPER_BONUS_RATIO_ONLY_GOLD,
    									array( 'mb_position' => GOLD ),
    									SUPPORT_GOLD_BONUS
    									) )
    		return FALSE;

    	// 플래티넘 공유 보너스
    	$sum_group_total_cv = $this->_get_sum_group_total_cv($wa_id, array( 'mb_position' => PLATINUM ) );
		if( FALSE == $sum_group_total_cv )    		return FALSE;
    	$total_cv = $weekly_account['wa_total_cv'] * GOLD_UPPER_BONUS_RATIO;
    	if( FALSE == $this->_payment( 
    									$wa_id, 
    									$total_cv, 
    									$sum_group_total_cv['sum_group_total_cv'], 
    									$weekly_account['wa_platinum_cv'],
    									GOLD_UPPER_BONUS_RATIO_ONLY_PLATINUM,
    									array( 'mb_position' => PLATINUM ),
    									SUPPORT_PLATINUM_BONUS
    									) )
    		return FALSE;

    	return TRUE;
		

    }
}