<?php
class Order_for_weekly_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function list_result($start_date, $end_date, $limit, $offset)
    {    
        // 기간 검색
        $this->_get_search_condition($start_date, $end_date) ;


        $this->db->limit($limit, $offset) ;

        $this->db->from('hb_order') ;
        $this->db->join('hb_order_delivery', 'hb_order.or_id = hb_order_delivery.or_id', 'left') ;
        $this->db->join('hb_order_for_policy', 'hb_order.or_id = hb_order_for_policy.or_id', 'left') ;
        $this->db->join('hb_member', 'hb_order.mb_no = hb_member.mb_no', 'left') ;

        $this->db->order_by('hb_order.or_open_date', 'desc');

        return $this->db->get()->result_array() ;
    }

    function list_count($start_date, $end_date) 
    {       
        // 기간 검색
        $this->_get_search_condition($start_date, $end_date) ;
        
        
        $this->db->select('count(distinct(hb_order.or_id)) as cnt') ;
        $this->db->join('hb_order_delivery', 'hb_order.or_id = hb_order_delivery.or_id', 'left') ;
        $this->db->join('hb_order_for_policy', 'hb_order.or_id = hb_order_for_policy.or_id', 'left') ;


        return $this->db->get('hb_order')->row()->cnt ;
    }

    function list_sum($start_date, $end_date)
    {
        // 기간 검색
        $this->_get_search_condition($start_date, $end_date) ;
        $this->db->where('wa_id is NULL') ;
        
        $this->db->select_sum('op_commission_value', 'sum_cv');
        $this->db->select_sum('op_promotion_value', 'sum_pv');
        
        $this->db->join('hb_order_delivery', 'hb_order.or_id = hb_order_delivery.or_id', 'left') ;
        $this->db->join('hb_order_for_policy', 'hb_order.or_id = hb_order_for_policy.or_id', 'left') ;
        
        $query = $this->db->get('hb_order');

        return $query->row_array();        
    }

    // 기간 검색
    function _get_search_condition($start_date, $end_date)
    {        
        $where = array();
        if ( $start_date != '' &&  $end_date != '' )
        {
            $where['or_open_date >='] = date("Y-m-d", strtotime($start_date) ) ;       
            $where['or_open_date <'] = date("Y-m-d", strtotime($end_date.'+1 day')) ;          
        }

        //$where['wa_id is NULL']; 
        $this->db->where($where) ;
        $this->db->where('wa_id is NULL') ;
    }

}
?>