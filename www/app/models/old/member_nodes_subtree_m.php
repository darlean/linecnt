<?php
class member_nodes_subtree_m extends CI_Model 
{
    function __construct()
    {
    	parent::__construct() ;
    }

    public function get_member_nodes_subtree( $in_mb_no, $in_search_fields )
    {
        foreach ($in_search_fields as $key => $value) 
        {
            if ($key && $value)
                $this->_get_search_cache($key, $value);
        }
        
        $select = "mb_no, mb_is_deleted, mb_supporter_no, mb_name, mb_id, mb_position, mb_center, mb_cv, mb_pv, 
                SUBSTRING( mb_datetime, 1, 10 ) AS mb_datetime, p.path_length,
                GROUP_CONCAT(crumbs.ancestor_mb_no SEPARATOR ',') AS breadcrumbs" ;

        $this->db->select($select, false) ;
        $this->db->join('member_nodes_paths AS p', 'hb_member.mb_no = p.descendant_mb_no') ;
        $this->db->join('member_nodes_paths AS crumbs', 'crumbs.descendant_mb_no = p.descendant_mb_no') ;
        $this->db->where(array('p.ancestor_mb_no' => $in_mb_no)) ;
        $this->db->group_by('hb_member.mb_no') ;
        //$this->db->order_by('breadcrumbs') ;
        $this->db->order_by('hb_member.mb_no') ;

        return $this->db->get('hb_member')->result_array() ;
    }

    function get_member_count_subtree( $in_mb_no )
    {
        $this->db->join('hb_member', 'hb_member.mb_no = descendant_mb_no', 'left') ;
        $this->db->where('ancestor_mb_no', $in_mb_no) ;

        $cnt_mb = $this->db->count_all_results('member_nodes_paths') ;

        return ( $cnt_mb > 0 ) ? $cnt_mb - 1 : $cnt_mb ;
    }

    // 검색 구문을 얻는다.
    function _get_search_cache( $search_field, $search_text ) 
    {        
        if (!$search_field || !$search_text)
            return FALSE;

        $where = '' ;

        if (preg_match('/[a-zA-Z]/', $search_text))
            $where .= 'INSTR(LOWER('.$this->db->protect_identifiers($search_field).'), LOWER('.$this->db->escape($search_text).'))';
        else
            $where .= 'INSTR('.$this->db->protect_identifiers($search_field).', '.$this->db->escape($search_text).')';
            
        $this->db->where($where, null, FALSE);
    }

    function get_member_info_in_subtree( $in_mb_no, $mb_name, $mb_id )
    {
        $this->db->select('mb_no, mb_supporter_name') ;
        $this->db->join('hb_member', 'hb_member.mb_no = descendant_mb_no', 'left') ;
        $this->db->where(array('ancestor_mb_no' => $in_mb_no, 'mb_name' => $mb_name)) ;

        if ( $mb_id != '' )
        {
            $this->db->where(array('mb_id' => $mb_id)) ;
        }

        return $this->db->get('member_nodes_paths')->row_array() ;
    }
}
?>