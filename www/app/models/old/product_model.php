<?php
class Product_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function list_result($sst, $sod, $sfl, $stx, $limit, $offset, $bHasPolicy = false)
    {    
        $this->_get_search_cache($sfl, $stx);

        if ($sst && $sod)
            $this->db->order_by($sst, $sod);

        $this->db->limit($limit, $offset) ;

        $select = "hb_product.pt_id, hb_product.pc_id, pt_name, pt_model_name, pt_color, pt_thumbnail, pc_commission_value, pc_promotion_value, pc_start_date, pc_end_date, pt_contents, cp_name, cp_price" ;
        
        $this->db->select ( $select ) ;
        $this->db->from('hb_product') ;
        $this->db->join('hb_product_for_policy', 'hb_product.pc_id = hb_product_for_policy.pc_id', 'left') ;
        $this->db->join('hb_calling_plan', 'hb_calling_plan.cp_idx = hb_product_for_policy.pc_calling_plan', 'left') ;

        if ( $bHasPolicy )
        {
            $this->db->where('hb_product.pc_id != ', '') ;
        }

        return $this->db->get()->result_array() ;
    }

    function list_count($sfl, $stx, $bHasPolicy = false) 
    {       
        $this->_get_search_cache($sfl, $stx);

        if ( $bHasPolicy )
        {
            $this->db->where('pc_id != ', '') ;
        }

        return $this->db->count_all_results('hb_product');
    }

    // 검색 구문을 얻는다.
    function _get_search_cache($search_field, $search_text) 
    {        
        if (!$search_field || !$search_text)
            return FALSE;

        $search_field = 'hb_product.'.$search_field ;

        $where = '' ;

        if (preg_match('/[a-zA-Z]/', $search_text))
            $where .= 'INSTR(LOWER('.$this->db->protect_identifiers($search_field).'), LOWER('.$this->db->escape($search_text).'))';
        else
            $where .= 'INSTR('.$this->db->protect_identifiers($search_field).', '.$this->db->escape($search_text).')';
            
        $this->db->where($where, null, FALSE);
    }

	function insert($in_sql) 
	{
		$this->db->insert('hb_product', $in_sql, false) ;
	}

    function update($in_pt_id, $in_sql)
    {
        $this->db->where('pt_id', $in_pt_id) ;
        $this->db->update('hb_product', $in_sql, false) ;
    }

    function delete($in_pt_id)
    {
        $this->db->where('pt_id', $in_pt_id) ;
        $this->db->delete('hb_product') ;
    }

	function get_product_info($in_pt_id)
	{
		$this->db->where('pt_id', $in_pt_id) ;
		return $this->db->get('hb_product')->row_array() ;
	}

    function get_product_info_with_policy_info($in_pt_id)
    {
        $this->db->from('hb_product') ;
        $this->db->join('hb_product_for_policy', 'hb_product.pc_id = hb_product_for_policy.pc_id', 'left') ;
        $this->db->join('hb_calling_plan', 'hb_calling_plan.cp_idx = hb_product_for_policy.pc_calling_plan', 'left') ;
        $this->db->where('hb_product.pt_id', $in_pt_id) ;

        return $this->db->get()->row_array() ;
    }
}
?>