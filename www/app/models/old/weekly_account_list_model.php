<?php
class Weekly_account_list_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
    }

    function list_result($limit, $offset)
    {    
        $this->db->limit($limit, $offset) ;

        $this->db->from('hb_weekly_account') ;
        $this->db->order_by( 'wa_end_date', 'desc');

        return $this->db->get()->result_array() ;
    }

    function list_count() 
    {        
        return $this->db->count_all_results('hb_weekly_account');        
    }
}

?>