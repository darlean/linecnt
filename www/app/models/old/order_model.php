<?php
class Order_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function insert($in_sql) 
    {
        $this->db->insert('hb_order', $in_sql) ;

        return mysql_insert_id() ;
    }

    function update($in_or_id, $in_sql)
    {
        $this->db->where('or_id', $in_or_id) ;
        return $this->db->update('hb_order', $in_sql) ;
    }

    function list_result($sst, $sod, $sfl, $stx, $limit, $offset)
    {    
        $this->_get_search_cache($sfl, $stx);

        if ($sst && $sod)
        {
            $sst = ($sst == 'or_id') ? 'hb_order.or_id' : $sst ;

            $this->db->order_by($sst, $sod);
        }

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        $this->db->from('hb_order') ;
        $this->db->join('hb_order_delivery', 'hb_order.or_id = hb_order_delivery.or_id', 'left') ;
        $this->db->join('hb_order_for_policy', 'hb_order.or_id = hb_order_for_policy.or_id', 'left') ;

        if( IS_MANAGER )
        {
            $this->db->join('hb_member', 'hb_order.mb_no = hb_member.mb_no', 'left') ;
        }
        else
        {
            $this->db->join('member_nodes_paths', 'hb_order.mb_no = member_nodes_paths.descendant_mb_no', 'left') ;
            $this->db->where('member_nodes_paths.ancestor_mb_no', $this->session->userdata('ss_mb_no')) ;
        }

        $this->db->group_by('hb_order.or_id') ;

        return $this->db->get()->result_array() ;
    }

    function list_count($sfl, $stx) 
    {       
        $this->_get_search_cache($sfl, $stx);

        // 기간 검색
        $this->_get_search_date_range() ;
        
        $this->db->select('count(distinct(hb_order.or_id)) as cnt') ;
        $this->db->join('hb_order_delivery', 'hb_order.or_id = hb_order_delivery.or_id', 'left') ;
        $this->db->join('hb_order_for_policy', 'hb_order.or_id = hb_order_for_policy.or_id', 'left') ;

        if( !IS_MANAGER )
        {
            $this->db->join('member_nodes_paths', 'hb_order.mb_no = member_nodes_paths.descendant_mb_no', 'left') ;
            $this->db->where('member_nodes_paths.ancestor_mb_no', $this->session->userdata('ss_mb_no')) ;
        }

        return $this->db->get('hb_order')->row()->cnt ;
    }

    // 검색 구문을 얻는다.
    function _get_search_cache($search_field, $search_text) 
    {        
        if (!$search_field || !$search_text)
            return FALSE;

        $search_field = $search_field ;

        $where = '' ;

        if (preg_match('/[a-zA-Z]/', $search_text))
            $where .= 'INSTR(LOWER('.$this->db->protect_identifiers($search_field).'), LOWER('.$this->db->escape($search_text).'))';
        else
            $where .= 'INSTR('.$this->db->protect_identifiers($search_field).', '.$this->db->escape($search_text).')';
            
        $this->db->where($where, null, FALSE);
    }

    // 기간 검색
    function _get_search_date_range()
    {
        $sdt = $this->input->get('sdt') ;
        $start_date = $this->input->get('start');   // 시작날짜
        $finish_date = $this->input->get('finish'); // 종료날짜

        if ( $start_date != '' )
        {
            $this->db->where($sdt.' >= ', "'".$start_date."'", false) ;
        }

        if ( $finish_date != '' )
        {
            $this->db->where($sdt.' <= ', "'".$finish_date."'", false) ;
        }
    }

    function get_order_info($in_or_id)
    {
        $this->db->join('hb_order_delivery', 'hb_order.or_id = hb_order_delivery.or_id', 'left') ;
        $this->db->join('hb_order_for_policy', 'hb_order.or_id = hb_order_for_policy.or_id', 'left') ;
        $this->db->join('hb_product', 'hb_order.pt_id = hb_product.pt_id', 'left') ;
        $this->db->join('hb_calling_plan', 'hb_calling_plan.cp_idx = hb_order_for_policy.op_calling_plan', 'left') ;

        $this->db->where('hb_order.or_id', $in_or_id) ;

        return $this->db->get('hb_order')->row_array() ;       
    }
}
?>