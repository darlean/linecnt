<?php
class Member_forget_model extends CI_Model 
{
	function __construct() 
	{
		parent::__construct();
	}

	function find_id( $in_username, $in_jumin_no )
	{
		$sql = "select PKG_CRYPTO.DECRYPT3(a.LOGIN_ID,'".G_CRYPTO."') as LOGIN_ID
          		from D_DISTRIBUTE a, d_member b 
         		where a.userid = b.userid
           		and b.username = PKG_CRYPTO.encrypt3('$in_username','".G_CRYPTO."')
           		and substr(PKG_CRYPTO.DECRYPT3(b.jumin_no,'".G_CRYPTO."'),1,6) = '$in_jumin_no'";

		return $this->db->query($sql)->row();
	}

	function check( $in_login_id, $in_e_mail )
	{
		if (!$this->input->post('w'))
			return FALSE;
			
		$sql = "select count(*) as CNT
                  from D_DISTRIBUTE a, d_member b 
                 where a.userid = b.userid
                   and PKG_CRYPTO.DECRYPT3(a.LOGIN_ID,'".G_CRYPTO."') = '$in_login_id'
				   and PKG_CRYPTO.DECRYPT3(b.email,'".G_CRYPTO."') = '$in_e_mail'";

		return $this->db->query($sql)->row()->CNT > 0;
	}

	function generateRandomPassword($length=8, $strength=0)
	{
		$vowels = 'aeuy';
		$consonants = 'bdghjmnpqrstvz';
		if ($strength & 1) {
			$consonants .= 'BDGHJLMNPQRSTVWXZ';
		}
		if ($strength & 2) {
			$vowels .= "AEUY";
		}
		if ($strength & 4) {
			$consonants .= '23456789';
		}
		if ($strength & 8) {
			$consonants .= '@#$%';
		}

		$password = '';
		$alt = time() % 2;
		for ($i = 0; $i < $length; $i++) {
			if ($alt == 1) {
				$password .= $consonants[(rand() % strlen($consonants))];
				$alt = 0;
			} else {
				$password .= $vowels[(rand() % strlen($vowels))];
				$alt = 1;
			}
		}
		return $password;
		//return "1234";
	}
	
	function new_pwd( $in_login_id, $in_e_mail )
	{
		$login_passwd = $this->generateRandomPassword(10,15);

		$sql = "update D_DISTRIBUTE
           		set passwd = PKG_CRYPTO.ONE_ENCRYPT('$login_passwd')
		 		where userid = (
			  	select a.userid
			  	from D_DISTRIBUTE a, d_member b 
			 	where a.userid = b.userid
			   	and PKG_CRYPTO.DECRYPT3(a.LOGIN_ID,'".G_CRYPTO."') = '$in_login_id'
			   	and PKG_CRYPTO.DECRYPT3(b.email,'".G_CRYPTO."') = '$in_e_mail'
		)";
		$this->db->query($sql);

		$mailto = $in_e_mail;
		$subject = "[긴급]HBNetworks에서 알려드립니다.";
		$content="요청하신 임시 암호는 [" . $login_passwd . "]입니다.";
		$result=mail($mailto, $subject, $content);

		if($result)
		{
			$msg = "임시암호를 작성하여 메일로 전송하였습니다.";
		} 
		else  
		{
			$msg = "메일로 전송하는데 에러가 발생하였습니다!";
		}

		return $msg;
	}
}
?>