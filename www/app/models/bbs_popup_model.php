<?php
class Popup_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}	

	function get_bbs_popup()
	{
		$sql="
		SELECT A.*,
		       TO_CHAR(TO_DATE(BBS_ST_DATE,'YYYYMMDD'),'YYYY-MM-DD') AS BBS_ST_DATE2,
			   TO_CHAR(TO_DATE(BBS_ED_DATE,'YYYYMMDD'),'YYYY-MM-DD') AS BBS_ED_DATE2
		FROM BBS_POPUP A
		WHERE IS_USE = 'O'
		   		AND TO_CHAR(SYSDATE,'YYYYMMDD') BETWEEN BBS_ST_DATE AND BBS_ED_DATE
		";

		return $this->db->query($sql)->result_array() ;
	}
}
?>