<?php
class HB_wired_gift extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

	function insert() 
	{
		$data = array(
		    'PDT_CD'       		=> $_POST["PDT_CD"],
	        'PTP_ID'     		=> $_POST["PTP_ID"],
	        'USERID'      		=> $_POST["USERID"],
	        'ORD_NAME'     		=> $_POST["ORD_NAME"],
	        'ORD_BIRTHDAY'  	=> $_POST["ORD_BIRTHDAY"],
			'ORD_MOBILE'    	=> $_POST["ORD_MOBILE"],
	        'ORD_TEL'     		=> $_POST["ORD_TEL"],
	        'ORD_EMAIL'    		=> $_POST["ORD_EMAIL"],
	        'INS_ZIPCODE'   	=> $_POST["INS_ZIPCODE"],
	        'INS_ADDR1'     	=> $_POST["INS_ADDR1"],
	        'INS_ADDR2' 		=> $_POST["INS_ADDR2"],
	        'INS_DATE' 			=> $_POST["INS_DATE"],
	        'AUTO_TRANS_TYPE'   => $_POST["AUTO_TRANS_TYPE"],
	        'DEPOSITOR'		 	=> $_POST["DEPOSITOR"],
	        'DEP_BIRTHDAY'	 	=> $_POST["DEP_BIRTHDAY"],
	        'BANK_CD'	 		=> $_POST["BANK_CD"],
			'BANK_ACCOUNT'	 	=> $_POST["BANK_ACCOUNT"],
			'CARD_CD'	 		=> $_POST["CARD_CD"],
			'CARD_ACCOUNT'	 	=> $_POST["CARD_ACCOUNT"],
			'CARD_VALID_DATE'	=> $_POST["CARD_VALID_DATE"],
			'ORD_MSG'	 		=> $_POST["ORD_MSG"],
			'CENTER_CD'	 		=> $_POST["CENTER_CD"],
			'RECEPTION_ID'	 	=> $_POST["RECEPTION_ID"]
	        ) ;

		$result->STATUS = $this->db->insert('HB_TEMP_ORDER_WIRED', $data) ;
		$result->NEW_ORDERID = "" ;
		$result->MESSAGE = $_POST ;

		if ( $result->STATUS )
		{
			$result->STATUS = 1 ;
			$rst = $this->db->query("SELECT HB_TEMP_ORDER_WIRED_SEQ1.currval as IDX FROM dual")->row() ;
			
			if ( $rst )
			{
				$result->NEW_ORDERID = $rst->IDX ;
			}
		}	
		else
		{
			$result->STATUS = 0 ;
			$result->MESSAGE = "유선 상품 신청이 실패하였습니다. 잠시 후 다시 시도해 주세요." ;
		}

		return $result ;
	}

	function list_count($vender)
	{
		$this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD', 'right') ;

		if ( $vender != '' )
		{
			$this->db->where('B.TEL_COMPANY', $vender) ;
		}

		$this->db->where('B.TYPE_CD', '2504') ;
		$this->db->where('B.ISUSE', 'O') ;

		return $this->db->count_all_results('HB_WIRED_GIFT A') ;
	}

	function list_result($limit, $offset, $vender)
	{
		$this->db->limit($limit, $offset) ;

		$this->db->select("A.IS_SET, A.IS_INTERNET, A.IS_HOME_TEL, A.IS_INTERNET_TEL, A.IS_SMART_HOME, A.IS_IPTV, A.IS_SKYLIFE, A.GIFT_BASIC, A.GIFT_PLUS, B.PDT_NAME, B.PDT_MODEL, B.PDT_CD, B.TEL_COMPANY", false ) ;

		$this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD', 'right') ;

		if ( $vender != '' )
		{
			$this->db->where('B.TEL_COMPANY', $vender) ;
		}

		$this->db->where('B.TYPE_CD', '2504') ;
		$this->db->where('B.ISUSE', 'O') ;

		$this->db->order_by('B.PDT_CD', 'DESC') ;

		return $this->db->get('HB_WIRED_GIFT A')->result_array() ;
	}

	function update($pdt_cd)
	{
		$isSet    		= ($this->input->post('IS_SET') == 'on') ? 'O' : 'X' ;
		$isInternet    	= ($this->input->post('IS_INTERNET') == 'on') ? 'O' : 'X' ;
		$isHomeTel    	= ($this->input->post('IS_HOME_TEL') == 'on') ? 'O' : 'X' ;
		$isInternetTel  = ($this->input->post('IS_INTERNET_TEL') == 'on') ? 'O' : 'X' ;
		$isSmartHome    = ($this->input->post('IS_SMART_HOME') == 'on') ? 'O' : 'X' ;
		$isIptv    		= ($this->input->post('IS_IPTV') == 'on') ? 'O' : 'X' ;
		$isSkyLife   	= ($this->input->post('IS_SKYLIFE') == 'on') ? 'O' : 'X' ;

		$sql = array(
			'PDT_CD' 			=> $this->input->post('PDT_CD'),
			'IS_SET' 			=> $isSet,
			'IS_INTERNET' 		=> $isInternet,
			'IS_HOME_TEL' 		=> $isHomeTel,
			'IS_INTERNET_TEL' 	=> $isInternetTel,
			'IS_SMART_HOME' 	=> $isSmartHome,
			'IS_IPTV' 			=> $isIptv,
			'IS_SKYLIFE' 		=> $isSkyLife,
			'GIFT_BASIC' 		=> $this->input->post('GIFT_BASIC'),
			'GIFT_PLUS' 		=> $this->input->post('GIFT_PLUS'),
		);

		$count = $this->get_count($pdt_cd) ;

		$this->db->where('PDT_CD', $pdt_cd);

		if ( $count > 0 )
			return $this->db->update('HB_WIRED_GIFT', $sql) ;

		return $this->db->insert('HB_WIRED_GIFT', $sql) ;
	}

	function get_count($pdt_cd)
	{
		$this->db->where('PDT_CD', $pdt_cd) ;

		return $this->db->count_all_results('HB_WIRED_GIFT') ;
	}

	function get_info($pdt_cd)
	{
		$this->db->select("A.*, B.PDT_NAME, B.PDT_MODEL, B.PDT_CD, B.TEL_COMPANY", false ) ;

		$this->db->from('HB_WIRED_GIFT A') ;
		$this->db->join('P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD', 'right') ;

		$this->db->where('B.PDT_CD', $pdt_cd) ;

		return $this->db->get()->row_array() ;
	}
}
?>