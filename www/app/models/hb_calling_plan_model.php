<?php
class HB_calling_plan_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function insert() 
	{	
		$isuse    = ($this->input->post('IS_USE') == 'on') ? 'O' : 'X' ;
		
		$data = array(			
			'CP_NAME' 		=> $this->input->post('CP_NAME'),
			'CP_PRICE' 		=> $this->input->post('CP_PRICE'),
			'CP_DISCOUNT' 	=> $this->input->post('CP_DISCOUNT'),
			'FREE_CALL' 	=> $this->input->post('FREE_CALL'),
			'FREE_SMS' 		=> $this->input->post('FREE_SMS'),			
			'FREE_DATA' 	=> $this->input->post('FREE_DATA'),
			'PLUS_CALL_PRICE' 	=> $this->input->post('PLUS_CALL_PRICE'),
			'PLUS_SMS_PRICE' 	=> $this->input->post('PLUS_SMS_PRICE'),
			'PLUS_DATA_PRICE' 	=> $this->input->post('PLUS_DATA_PRICE'),
			'NET_COMBI_DISCOUNT' => $this->input->post('NET_COMBI_DISCOUNT'),
			'TEL_COMPANY' 		=> $this->input->post('TEL_COMPANY'),
			'ETC_MSG' 			=> $this->input->post('ETC_MSG'),
			'IS_USE' 			=> $isuse
		);

		$sql = "INSERT INTO HB_CALLING_PLAN (CP_ID, CP_NAME, CP_PRICE, CP_DISCOUNT, FREE_CALL, FREE_SMS, FREE_DATA, PLUS_CALL_PRICE, 
				PLUS_SMS_PRICE, PLUS_DATA_PRICE, NET_COMBI_DISCOUNT, TEL_COMPANY, ETC_MSG) VALUES (SEQ_HB_CALLING_PLAN.NEXTVAL,
				 '".$data['CP_NAME']."', '".$data['CP_PRICE']."', '".$data['CP_DISCOUNT']."', '".$data['FREE_CALL']."', '".$data['FREE_SMS']."',
				 '".$data['FREE_DATA']."', '".$data['PLUS_CALL_PRICE']."', '".$data['PLUS_SMS_PRICE']."', '".$data['PLUS_DATA_PRICE']."', 
				 '".$data['NET_COMBI_DISCOUNT']."', '".$data['TEL_COMPANY']."', '".$data['ETC_MSG']."')" ;

		return $this->db->query($sql) ;
	}

	function update($in_cp_id) 
	{
		$isuse    = ($this->input->post('IS_USE') == 'on') ? 'O' : 'X' ;
		
		$sql = array(			
			'CP_NAME' 		=> $this->input->post('CP_NAME'),
			'CP_PRICE' 		=> $this->input->post('CP_PRICE'),
			'CP_DISCOUNT' 	=> $this->input->post('CP_DISCOUNT'),
			'FREE_CALL' 	=> $this->input->post('FREE_CALL'),
			'FREE_SMS' 		=> $this->input->post('FREE_SMS'),			
			'FREE_DATA' 	=> $this->input->post('FREE_DATA'),
			'PLUS_CALL_PRICE' 	=> $this->input->post('PLUS_CALL_PRICE'),
			'PLUS_SMS_PRICE' 	=> $this->input->post('PLUS_SMS_PRICE'),
			'PLUS_DATA_PRICE' 	=> $this->input->post('PLUS_DATA_PRICE'),
			'NET_COMBI_DISCOUNT' => $this->input->post('NET_COMBI_DISCOUNT'),
			'TEL_COMPANY' 		=> $this->input->post('TEL_COMPANY'),
			'ETC_MSG' 			=> $this->input->post('ETC_MSG'),
			'IS_USE' 			=> $isuse
		);

		$this->db->where('CP_ID', $in_cp_id);
		return $this->db->update('HB_CALLING_PLAN', $sql) ;		
	}

	function del($in_cp_id)
	{
		$this->db->where('CP_ID', $in_cp_id);
		return $this->db->delete('HB_CALLING_PLAN') ;		
	}

	function list_result($limit, $offset, $vender)
    {    
        $this->db->limit($limit, $offset) ;

        $this->db->from('HB_CALLING_PLAN') ;       
        $this->db->where('TEL_COMPANY', $vender) ;                  
        $this->db->order_by('CP_NAME', 'ASC') ;			

        return $this->db->get()->result_array() ;
    }

    function list_count($vender) 
    {   
        $this->db->where('TEL_COMPANY', $vender) ;                     

        return $this->db->count_all_results('HB_CALLING_PLAN') ;
    }

    function get_calling_plan_list($in_vender)
	{
		$this->db->select('CP_ID, CP_NAME') ;
		$this->db->where(array('TEL_COMPANY' => $in_vender) ) ;		
		$this->db->where('IS_USE', 'O') ;          

		$this->db->order_by('CP_NAME', 'ASC') ;			

		return $this->db->get('HB_CALLING_PLAN')->result_array() ;
	}

    function get_cp_info($in_cp_id)
    {
    	$this->db->from('HB_CALLING_PLAN') ;
    	$this->db->where('CP_ID', $in_cp_id) ;     

    	return $this->db->get()->row_array() ;
    }

    function get_cp_info_list_by_vender($in_vender)
    {
    	$this->db->from('HB_CALLING_PLAN') ;
    	$this->db->where('TEL_COMPANY', $in_vender) ;
        $this->db->where('IS_USE', 'O') ;

        return $this->db->get()->result_array() ;
    }
}
?>