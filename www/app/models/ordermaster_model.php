<?php
class OrderMaster_model extends CI_Model 
{
    function __construct() {
        parent::__construct();
    }

    // 누적매출
    function get_total_amt($in_user_id)
    {
        $fields = "NVL(SUM(AMT + DELI_AMT), 0) AS TOTAL_AMT,
                   NVL(SUM(PV1),0)             AS TOTAL_PV1,
                   NVL(SUM(PV2),0)             AS TOTAL_PV2" ;

        $this->db->select($fields, false) ;
        $this->db->from('O_ORDERMASTER') ;

        $where = "ORD_TYPE IN ('1','2')
           AND ISORDER_OK = 'O'           
           AND USERID = '$in_user_id'" ;

        $this->db->where($where, NULL, false) ;

        return $this->db->get()->row_array() ;
    }

    function get_pv_list_count($in_user_id)
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->where('USERID', $in_user_id) ;

        return $this->db->count_all_results('O_ORDERMASTER') ;
    }

    function get_pv_list($in_user_id, $sst, $sod, $limit, $offset)
    {
        if ($sst && $sod)
        {
            $this->db->order_by($sst, $sod);
        }

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        $sql = "A.*, B.REASON_NAME, DECODE(A.ORD_TYPE, '1', NVL(A.GT_CD, '재발급요청필요'), GT_CD) AS GT_CDS,
                TO_CHAR(TO_DATE(A.ORD_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') AS ORD_DATES, 
                DECODE(A.ORD_TYPE, '1', '주문', '2', '반품', '3', '교환', '4', '취소') AS ORD_TYPES" ;

        $this->db->select($sql, false) ;
        $this->db->from('O_ORDERMASTER A, S_REASON B') ;
        $this->db->where(array('A.USERID' => $in_user_id, 'A.GT_STATUS' => 'B.REASON_CD'), NULL, false ) ;

        return $this->db->get()->result_array() ;
    }

    function get_od_list_count($in_user_id, $in_ord_type)
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->where('USERID', $in_user_id) ;

        if ( $in_ord_type == 0 ) // 개통완료
        {
            $this->db->where('A.ORD_TYPE', 1) ;            
            $this->db->where('A.ORD_STATE >', 10) ; 
            $this->db->where('A.ORD_STATE <', 20) ;
        }
        else
        {
            $this->db->where('A.ORD_TYPE', $in_ord_type) ;

            if ( $in_ord_type == 1 )
            {
                $this->db->or_where('(A.ORD_STATE <= 10 AND A.ORD_STATE >= 20)', NULL, false) ;
            }
        }
        
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;

        return $this->db->count_all_results('O_ORDERMASTER A') ;
    }

    function get_od_list($in_user_id, $sst, $sod, $limit, $offset, $in_ord_type)
    {
        if ($sst && $sod)
        {
            $this->db->order_by($sst, $sod);
        }

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        $sql = "A.ORDNO, A.ORD_TYPE, D.REASON_NAME, DECODE(A.ORD_TYPE, '1', NVL(A.GT_CD, '재발급요청필요'), A.GT_CD) AS GT_CDS,
               TO_CHAR(TO_DATE(A.ORD_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') AS ORD_DATES, 
               DECODE(A.ORD_KIND, '1', 'BV', '2', 'SV', '3', '재구매') AS ORD_KINDS,
               DECODE(A.ORD_TYPE, '1', '주문', '2', '반품', '3', '교환', '4', '취소') AS ORD_TYPES,
               B.PRICE, B.VAT, B.AMT, B.QTY, (B.PV1 * B.QTY) AS PV1, (B.PV2 * B.QTY) AS PV2, B.PDT_CD, C.PDT_NAME" ;

        $this->db->select($sql, false) ;
        $this->db->from('O_ORDERMASTER A' ) ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;
        $this->db->join('S_REASON D', 'D.REASON_CD = A.GT_STATUS') ;        

        $this->db->where('USERID', $in_user_id) ;
        
        if ( $in_ord_type == 0 ) // 개통완료
        {
            $this->db->where('A.ORD_TYPE', 1) ;            
            $this->db->where('A.ORD_STATE >', 10) ; 
            $this->db->where('A.ORD_STATE <', 20) ;
        }
        else
        {
            $this->db->where('A.ORD_TYPE', $in_ord_type) ;

            if ( $in_ord_type == 1 )
            {
                $this->db->or_where('(A.ORD_STATE <= 10 AND A.ORD_STATE >= 20)', NULL, false) ;
            }
        }

        return $this->db->get()->result_array() ;
    }

    function get_support_sub_list_count($in_user_id, $in_ord_type)
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->select("COUNT(DISTINCT A.ORDNO) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;

        //$this->db->where('A.IS_TEL_PDT', 'O') ;
         
        $subquery = "SELECT USERID FROM D_MEMBER START WITH USERID = '".$in_user_id."' CONNECT BY P_ID = PRIOR USERID" ;
        $this->db->where_in('A.USERID', $subquery, FALSE) ; 

        if ( $in_ord_type == 0 ) // 개통완료
        {
            $this->db->where('A.ORD_TYPE', 1) ;            
            $this->db->where('A.ORD_STATE >', 10) ; 
            $this->db->where('A.ORD_STATE <', 20) ;
        }
        else
        {
            $this->db->where('A.ORD_TYPE', $in_ord_type) ;
        }     

        $result = $this->db->get()->row_array() ; 

        return  ( $result && isset($result['CNT']) ) ? $result['CNT'] : 0 ;  
    }

    function get_support_sub_list($in_user_id, $limit, $offset, $in_ord_type)
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        $sql = "A.PV1, A.PV2, PKG_CRYPTO_FIELD.get('USERNAME', Z.USERNAME,'9','".G_CRYPTO."')||'('||A.USERID||')' AS USERTEXT,
               TO_CHAR(TO_DATE(A.ORD_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') AS ORD_DATES, 
               DECODE(A.ORD_KIND, '1', 'BV', '2', 'SV', '3', '재구매') AS ORD_KINDS,
               DECODE(A.ORD_TYPE, '1', '주문', '2', '반품', '3', '교환', '4', '취소') AS ORD_TYPES" ;

        $this->db->select($sql, false) ;

        $this->db->from('O_ORDERMASTER A') ;    
        $this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID', 'left') ;        
 
        //$this->db->where('A.IS_TEL_PDT', 'O') ;
         
        $subquery = "SELECT USERID FROM D_MEMBER START WITH USERID = '".$in_user_id."' CONNECT BY P_ID = PRIOR USERID" ;
        $this->db->where_in('A.USERID', $subquery, FALSE) ; 

        if ( $in_ord_type == 0 ) // 개통완료
        {
            $this->db->where('A.ORD_TYPE', 1) ;            
            $this->db->where('A.ORD_STATE >', 10) ; 
            $this->db->where('A.ORD_STATE <', 20) ; 
        }
        else
        {
            $this->db->where('A.ORD_TYPE', $in_ord_type) ;
        }

        $this->db->group_by('A.PV1, A.PV2, Z.USERNAME, A.USERID, A.ORD_DATE, A.ORD_KIND, A.ORD_TYPE, A.ORDNO') ;
        $this->db->order_by('A.USERID', 'desc') ;
        $this->db->order_by('A.ORD_DATE', 'desc') ;    

        return $this->db->get()->result_array() ;
    }

    function get_recommand_sub_list_count($in_user_id, $in_ord_type)
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;            

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;          
         
        if( !IS_MANAGER )
        {
            $subquery = "SELECT USERID FROM D_MEMBER WHERE USERID <> '".$in_user_id."' START WITH USERID = '".$in_user_id."' CONNECT BY R_ID = PRIOR USERID" ;
            $this->db->where_in('A.USERID', $subquery, FALSE) ; 
        }

        if ( $in_ord_type == 0 ) // 개통완료
        {
            $this->db->where('A.ORD_TYPE', 1) ;            
            $this->db->where('A.ORD_STATE >', 10) ; 
            $this->db->where('A.ORD_STATE <', 20) ; 
        }
        else
        {
            $this->db->where('A.ORD_TYPE', $in_ord_type) ;

            if ( $in_ord_type == 1 )
            {
                $this->db->or_where('(A.ORD_STATE <= 10 AND A.ORD_STATE >= 20)', NULL, false) ;
            }
        }

        return $this->db->count_all_results() ;
    }

    function get_recommand_sub_list($in_user_id, $sst, $sod, $limit, $offset, $in_ord_type)
    {
        if ($sst && $sod)
        {
            $this->db->order_by($sst, $sod);
        }

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        $sql = "A.*, PKG_CRYPTO_FIELD.get('USERNAME', Z.USERNAME,'9','".G_CRYPTO."')||'('||A.USERID||')' AS USERTEXT,
               TO_CHAR(TO_DATE(A.ORD_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') AS ORD_DATES, 
               DECODE(A.ORD_KIND, '1', 'BV', '2', 'SV', '3', '재구매') AS ORD_KINDS,
               DECODE(A.ORD_TYPE, '1', '주문', '2', '반품', '3', '교환', '4', '취소') AS ORD_TYPES" ;

        if( !IS_MANAGER )
        {
            $sql .= ", D.RLV" ;
        }

        $this->db->select($sql, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;        
        $this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID') ;        

        if( !IS_MANAGER )  
        {
            $this->db->join("(SELECT USERID, (LEVEL-1) AS RLV FROM D_MEMBER 
                                 WHERE USERID <> '$in_user_id'
                                 START WITH USERID = '$in_user_id' 
                                 CONNECT BY R_ID = PRIOR USERID) D", 'D.USERID = A.USERID') ;  
        }

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;          

        if ( $in_ord_type == 0 ) // 개통완료
        {
            $this->db->where('A.ORD_TYPE', 1) ;            
            $this->db->where('A.ORD_STATE >', 10) ; 
            $this->db->where('A.ORD_STATE <', 20) ; 
        }
        else
        {
            $this->db->where('A.ORD_TYPE', $in_ord_type) ;

            if ( $in_ord_type == 1 )
            {
                $this->db->or_where('(A.ORD_STATE <= 10 AND A.ORD_STATE >= 20)', NULL, false) ;
            }
        }

        return $this->db->get()->result_array() ;
    }

    function get_reception_sub_list_count($in_user_id, $in_ord_type)
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;            

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;   
        $this->db->where('A.RECEPTION_ID', $in_user_id) ; 

        if ( $in_ord_type == 0 ) // 개통완료
        {
            $this->db->where('A.ORD_TYPE', 1) ;            
            $this->db->where('A.ORD_STATE >', 10) ; 
            $this->db->where('A.ORD_STATE <', 20) ; 
        }
        else
        {
            $this->db->where('A.ORD_TYPE', $in_ord_type) ;

            if ( $in_ord_type == 1 )
            {
                $this->db->or_where('(A.ORD_STATE <= 10 AND A.ORD_STATE >= 20)', NULL, false) ;
            }
        }

        return $this->db->count_all_results() ;
    }

    function get_reception_sub_list($in_user_id, $sst, $sod, $limit, $offset, $in_ord_type)
    {
        if ($sst && $sod)
        {
            $this->db->order_by($sst, $sod);
        }

        // 기간 검색
        $this->_get_search_date_range() ;

        $this->db->limit($limit, $offset) ;

        $sql = "A.*, PKG_CRYPTO_FIELD.get('USERNAME', Z.USERNAME, '9', '".G_CRYPTO."')||' ('||A.USERID||')' AS USERTEXT,
               TO_CHAR(TO_DATE(A.ORD_DATE, 'YYYYMMDD'), 'YYYY-MM-DD') AS ORD_DATES, 
               DECODE(A.ORD_KIND, '1', 'BV', '2', 'SV', '3', '재구매') AS ORD_KINDS,
               DECODE(A.ORD_TYPE, '1', '주문', '2', '반품', '3', '교환', '4', '취소') AS ORD_TYPES" ;

        $this->db->select($sql, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;        
        $this->db->join('D_MEMBER Z', 'Z.USERID = A.USERID') ;        

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;                                  
        $this->db->where('A.RECEPTION_ID', $in_user_id) ; 

        if ( $in_ord_type == 0 ) // 개통완료
        {
            $this->db->where('A.ORD_TYPE', 1) ;            
            $this->db->where('A.ORD_STATE >', 10) ; 
            $this->db->where('A.ORD_STATE <', 20) ; 
        }
        else
        {
            $this->db->where('A.ORD_TYPE', $in_ord_type) ;

            if ( $in_ord_type == 1 )
            {
                $this->db->or_where('(A.ORD_STATE <= 10 AND A.ORD_STATE >= 20)', NULL, false) ;
            }
        }

        return $this->db->get()->result_array() ;
    }

    function get_reception_all_list_count($in_year, $in_month)
    {
        $start = $in_year.$in_month.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_month + 1, 1, $in_year)) ;  

        $this->db->select("COUNT(DISTINCT A.RECEPTION_ID) AS CNT", false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;            

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;           
        $this->db->where('A.ORD_TYPE', 1) ;            
        $this->db->where('A.ORD_STATE >', 10) ; 
        $this->db->where('A.ORD_STATE <', 20) ; 

        $this->db->where("A.ORD_DATE BETWEEN {$start} AND {$finish}") ; 
        
        $result = $this->db->get()->row_array() ; 

        return  ( $result && isset($result['CNT']) ) ? $result['CNT'] : 0 ;                
    }

    function get_reception_all_list($in_year, $in_month, $sst, $sod, $limit, $offset)
    {
        $start = $in_year.$in_month.'01' ;
        $finish = date("Ymd", mktime(0, 0, 0, $in_month + 1, 1, $in_year)) ;  

        if ($sst && $sod)
        {
            $this->db->order_by($sst, $sod);
        }

        $this->db->limit($limit, $offset) ;

        $sql = "PKG_CRYPTO_FIELD.get('USERNAME', Z.USERNAME, '9', '".G_CRYPTO."')||' ('||A.RECEPTION_ID||')' AS USERTEXT,        
                SUM(A.PV1) AS PV1, SUM(A.PV2) AS PV2, SUM(A.PV3*(A.SALES_FEE/100)) AS PV3, 
                SUM(A.PRICE) AS PRICE, SUM(A.VAT) AS VAT, SUM(A.AMT) AS AMT" ;

        $this->db->select($sql, false) ;

        $this->db->from('O_ORDERMASTER A') ;        
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;        
        $this->db->join('D_MEMBER Z', 'Z.USERID = A.RECEPTION_ID') ;        

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;                                          
        $this->db->where('A.ORD_TYPE', 1) ;            
        $this->db->where('A.ORD_STATE >', 10) ; 
        $this->db->where('A.ORD_STATE <', 20) ; 

        $this->db->where("A.ORD_DATE BETWEEN {$start} AND {$finish}") ; 

        $this->db->group_by('A.RECEPTION_ID, Z.USERNAME') ; 

        return $this->db->get()->result_array() ;
    }

    // 기간 검색
    function _get_search_date_range()
    {
        $sdt = 'ORD_DATE' ;
        $start_date = $this->input->get('start');   // 시작날짜
        $finish_date = $this->input->get('finish'); // 종료날짜

        if ( $start_date != '' )
        {
            $this->db->where($sdt.' >= ', "'".$start_date."'", false) ;
        }

        if ( $finish_date != '' )
        {
            $this->db->where($sdt.' <= ', "'".$finish_date."'", false) ;
        }
    }

    function get_member_cv_pv($in_user_id)
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        $sql = "SUM(A.PV1) AS CV, SUM(A.PV2) AS PV, SUM(A.PRICE) AS PRICE, SUM(A.VAT) AS VAT, SUM(A.AMT) AS AMT" ;

        $this->db->select($sql, false) ;
        $this->db->from("O_ORDERMASTER A") ;

        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        //$this->db->where('A.GT_STATUS', '3803') ; 
        $this->db->where('A.ORD_TYPE', '1') ; 
        $this->db->where('A.ORD_STATE >', 10) ; 
        $this->db->where('A.ORD_STATE <', 20) ;         

        $this->db->where('USERID', $in_user_id ) ;

        return $this->db->get()->row_array() ;
    }

    function get_support_cv_pv_subtree( $in_user_id, $in_ord_type = 0 )
    {
        // 기간 검색
        $this->_get_search_date_range() ;

        $sql = "SUM(A.PV1) AS CV, SUM(A.PV2) AS PV, SUM(A.PRICE) AS PRICE, SUM(A.VAT) AS VAT, SUM(A.AMT) AS AMT" ;

        $this->db->select($sql, false) ;

        $this->db->from('O_ORDERMASTER A') ;     
        $this->db->join('D_MEMBER B', 'B.USERID = A.USERID') ;        

        //$this->db->where('A.IS_TEL_PDT', 'O') ;
        //$this->db->where('A.GT_STATUS', '3803') ;

        if ( $in_ord_type == 0 ) // 개통완료
        {
            $this->db->where('A.ORD_TYPE', 1) ;
            $this->db->where('A.ORD_STATE >', 10) ;
            $this->db->where('A.ORD_STATE <', 20) ;
        }
        else
        {
            $this->db->where('A.ORD_TYPE', $in_ord_type) ;
        }
         
        $subquery = "SELECT USERID FROM D_MEMBER START WITH USERID = '".$in_user_id."' CONNECT BY P_ID = PRIOR USERID" ;
        $this->db->where_in('A.USERID', $subquery, FALSE) ; 

        return $this->db->get()->row_array() ; 
    }

    function get_recommand_cv_pv_subtree($in_user_id)
    {
        // 기간 검색
        $this->_get_search_date_range() ;
                
        $sql = "SUM(A.PV1) AS CV, SUM(A.PV2) AS PV, SUM(A.PRICE) AS PRICE, SUM(A.VAT) AS VAT, SUM(A.AMT) AS AMT" ;

        $this->db->select($sql, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;            

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        //$this->db->where('A.GT_STATUS', '3803') ; 
        $this->db->where('A.ORD_TYPE', '1') ; 
        $this->db->where('A.ORD_STATE >', 10) ; 
        $this->db->where('A.ORD_STATE <', 20) ;        
         
        if( !IS_MANAGER )
        {
            $subquery = "SELECT USERID FROM D_MEMBER WHERE USERID <> '".$in_user_id."' START WITH USERID = '".$in_user_id."' CONNECT BY R_ID = PRIOR USERID" ;
            $this->db->where_in('A.USERID', $subquery, FALSE) ; 
        }           

        return $this->db->get()->row_array() ;
    }

    function get_reception_cv_pv_sub($in_user_id="")
    {
        // 기간 검색
        $this->_get_search_date_range() ;
                
        $sql = "SUM(A.PV1) AS CV, SUM(A.PV2) AS PV, SUM(A.PRICE) AS PRICE, SUM(A.VAT) AS VAT, SUM(A.AMT) AS AMT, SUM(A.PV3 * ( A.SALES_FEE / 100 )) AS FEE" ;

        $this->db->select($sql, false) ;

        $this->db->from('O_ORDERMASTER A') ;
        $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO') ;
        $this->db->join('P_PDTMASTER C', 'C.PDT_CD = B.PDT_CD') ;            

        $this->db->where('C.IS_TELPRICE', 'O') ;  
        $this->db->where('A.IS_TEL_PDT', 'O') ;  
        //$this->db->where('A.GT_STATUS', '3803') ; 
        $this->db->where('A.ORD_TYPE', '1') ; 
        $this->db->where('A.ORD_STATE >', 10) ; 
        $this->db->where('A.ORD_STATE <', 20) ;   

        if ( $in_user_id != "" )
        {
            $this->db->where('A.RECEPTION_ID', $in_user_id) ;      
        }

        return $this->db->get()->row_array() ;
    }    

    // 재고 카운팅
    function get_stock_count($in_pdt_cd, $in_pdtdetail_no)
    {
        $count = 0 ;

        { // 총 재고 갯수
            $sql = "SUM(DECODE(INOUT_TYPE, 'I', 1, -1)) AS CNT" ;

            $this->db->select($sql, false) ;
            $this->db->from('O_STOCKPRODUCT') ;

            $this->db->where('PDT_CD', $in_pdt_cd) ;  
            $this->db->where('PDTDETAIL_NO', $in_pdtdetail_no) ;    

            $result = $this->db->get()->row() ;  

            $count = isset($result->CNT) ? $result->CNT : 0 ;
        }

        { // 총 재고 갯수
            $sql = "COUNT(DISTINCT A.ORDNO) AS CNT" ;

            $this->db->select($sql, false) ;
            $this->db->from('O_ORDERMASTER A') ;
            $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO', 'left') ;
            $this->db->join('O_ORDERSTATE C', 'A.ORDNO = C.ORDNO', 'left') ;

            $this->db->where('B.PDT_CD', $in_pdt_cd) ;  
            $this->db->where('B.PDTDETAIL_NO', $in_pdtdetail_no) ;    
            $this->db->where('A.ORD_STATE <= ', 11, false) ;    
            $this->db->where('A.ORD_TYPE', 1) ;    
            $this->db->where('C.STATE_CD <>', 6, false) ;    

            $result = $this->db->get()->row() ;  

            $count -= isset($result->CNT) ? $result->CNT : 0 ;
        }

        return $count ;
    }

    // 재고 카운팅 ( without option ) 
    function get_stock_total_count($in_pdt_cd)
    {
        $count = 0 ;

        { // 총 재고 갯수
            $sql = "SUM(DECODE(INOUT_TYPE, 'I', 1, -1)) AS CNT" ;

            $this->db->select($sql, false) ;
            $this->db->from('O_STOCKPRODUCT') ;

            $this->db->where('PDT_CD', $in_pdt_cd) ;              

            $result = $this->db->get()->row() ;  

            $count = isset($result->CNT) ? $result->CNT : 0 ;
        }

        { // 총 재고 갯수
            $sql = "COUNT(DISTINCT A.ORDNO) AS CNT" ;

            $this->db->select($sql, false) ;
            $this->db->from('O_ORDERMASTER A') ;
            $this->db->join('O_ORDERPRODUCT B', 'A.ORDNO = B.ORDNO', 'left') ;
            $this->db->join('O_ORDERSTATE C', 'A.ORDNO = C.ORDNO', 'left') ;

            $this->db->where('B.PDT_CD', $in_pdt_cd) ;              
            $this->db->where('A.ORD_STATE <= ', 11, false) ;    
            $this->db->where('A.ORD_TYPE', 1) ;    
            $this->db->where('C.STATE_CD <>', 6, false) ;    

            $result = $this->db->get()->row() ;  

            $count -= isset($result->CNT) ? $result->CNT : 0 ;
        }

        return $count ;
    }
}
?>