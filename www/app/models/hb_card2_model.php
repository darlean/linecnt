<?php
class HB_card2_model extends CI_Model 
{
	function __construct() {
		parent::__construct();
	}

	function insert($in_img_path) 
	{	
		$data = array(			
			'PDT_CD' 					=> $this->input->post('PDT_CD'),			
			'CD_COMPANY' 				=> $this->input->post('CD_COMPANY'),									
			'SAVE_LIMIT' 				=> $this->input->post('SAVE_LIMIT'),			
			'SAVE_COMMISSION' 			=> $this->input->post('SAVE_COMMISSION'),
			'SAVE_POINT_TERMS1_OVER' 	=> $this->input->post('SAVE_POINT_TERMS1_OVER'),
			'SAVE_POINT_TERMS1_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS1_UNDER'),
			'SAVE_POINT_TERMS1_POINT' 	=> $this->input->post('SAVE_POINT_TERMS1_POINT'),
			'SAVE_POINT_TERMS2_OVER' 	=> $this->input->post('SAVE_POINT_TERMS2_OVER'),
			'SAVE_POINT_TERMS2_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS2_UNDER'),
			'SAVE_POINT_TERMS2_POINT' 	=> $this->input->post('SAVE_POINT_TERMS2_POINT'),
			'SAVE_POINT_TERMS3_OVER' 	=> $this->input->post('SAVE_POINT_TERMS3_OVER'),
			'SAVE_POINT_TERMS3_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS3_UNDER'),
			'SAVE_POINT_TERMS3_POINT' 	=> $this->input->post('SAVE_POINT_TERMS3_POINT'),
			'ANNUAL_FEE' 				=> $this->input->post('ANNUAL_FEE'),
			'JOIN_POINT' 				=> $this->input->post('JOIN_POINT'),
			'ADD_BENEFIT' 				=> $this->input->post('ADD_BENEFIT'),
			'ETC_MSG' 					=> $this->input->post('ETC_MSG'),
			'IMG_PATH'					=> $in_img_path,
            'CARD_TYPE'                 => $this->input->post('CARD_TYPE'),
            'TOTAL_DISCOUNT_LIMIT1_OVER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT1_OVER'),
            'TOTAL_DISCOUNT_LIMIT1_UNDER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT1_UNDER'),
            'TOTAL_DISCOUNT_LIMIT1_PRICE' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT1_PRICE'),
            'TOTAL_DISCOUNT_LIMIT2_OVER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT2_OVER'),
            'TOTAL_DISCOUNT_LIMIT2_UNDER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT2_UNDER'),
            'TOTAL_DISCOUNT_LIMIT2_PRICE' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT2_PRICE'),
            'TOTAL_DISCOUNT_LIMIT3_OVER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT3_OVER'),
            'TOTAL_DISCOUNT_LIMIT3_UNDER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT3_UNDER'),
            'TOTAL_DISCOUNT_LIMIT3_PRICE' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT3_PRICE'),
            'TOTAL_DISCOUNT_LIMIT4_OVER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT4_OVER'),
            'TOTAL_DISCOUNT_LIMIT4_UNDER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT4_UNDER'),
            'TOTAL_DISCOUNT_LIMIT4_PRICE' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT4_PRICE'),
            'IS_USE_TOTAL_DISCOUNT_LIMIT' 	=> $this->input->post('IS_USE_TOTAL_DISCOUNT_LIMIT'),
		);

		return $this->db->insert('HB_CARD2', $data) ;
	}

	function update($in_pdt_cd, $in_img_path) 
	{	
		$data = array(						
			'CD_COMPANY' 				=> $this->input->post('CD_COMPANY'),			
			'SAVE_LIMIT' 				=> $this->input->post('SAVE_LIMIT'),
			'SAVE_COMMISSION' 			=> $this->input->post('SAVE_COMMISSION'),
			'SAVE_POINT_TERMS1_OVER' 	=> $this->input->post('SAVE_POINT_TERMS1_OVER'),
			'SAVE_POINT_TERMS1_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS1_UNDER'),
			'SAVE_POINT_TERMS1_POINT' 	=> $this->input->post('SAVE_POINT_TERMS1_POINT'),
			'SAVE_POINT_TERMS2_OVER' 	=> $this->input->post('SAVE_POINT_TERMS2_OVER'),
			'SAVE_POINT_TERMS2_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS2_UNDER'),
			'SAVE_POINT_TERMS2_POINT' 	=> $this->input->post('SAVE_POINT_TERMS2_POINT'),
			'SAVE_POINT_TERMS3_OVER' 	=> $this->input->post('SAVE_POINT_TERMS3_OVER'),
			'SAVE_POINT_TERMS3_UNDER' 	=> $this->input->post('SAVE_POINT_TERMS3_UNDER'),
			'SAVE_POINT_TERMS3_POINT' 	=> $this->input->post('SAVE_POINT_TERMS3_POINT'),
			'ANNUAL_FEE' 				=> $this->input->post('ANNUAL_FEE'),
			'JOIN_POINT' 				=> $this->input->post('JOIN_POINT'),
			'ADD_BENEFIT' 				=> $this->input->post('ADD_BENEFIT'),
			'ETC_MSG' 					=> $this->input->post('ETC_MSG'),
			'IMG_PATH'					=> $in_img_path,
            'CARD_TYPE'                 => $this->input->post('CARD_TYPE'),
            'TOTAL_DISCOUNT_LIMIT1_OVER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT1_OVER'),
            'TOTAL_DISCOUNT_LIMIT1_UNDER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT1_UNDER'),
            'TOTAL_DISCOUNT_LIMIT1_PRICE' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT1_PRICE'),
            'TOTAL_DISCOUNT_LIMIT2_OVER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT2_OVER'),
            'TOTAL_DISCOUNT_LIMIT2_UNDER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT2_UNDER'),
            'TOTAL_DISCOUNT_LIMIT2_PRICE' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT2_PRICE'),
            'TOTAL_DISCOUNT_LIMIT3_OVER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT3_OVER'),
            'TOTAL_DISCOUNT_LIMIT3_UNDER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT3_UNDER'),
            'TOTAL_DISCOUNT_LIMIT3_PRICE' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT3_PRICE'),
            'TOTAL_DISCOUNT_LIMIT4_OVER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT4_OVER'),
            'TOTAL_DISCOUNT_LIMIT4_UNDER' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT4_UNDER'),
            'TOTAL_DISCOUNT_LIMIT4_PRICE' 	=> $this->input->post('TOTAL_DISCOUNT_LIMIT4_PRICE'),
            'IS_USE_TOTAL_DISCOUNT_LIMIT' 	=> $this->input->post('IS_USE_TOTAL_DISCOUNT_LIMIT'),
        );

		$this->db->where('PDT_CD', $in_pdt_cd) ;
        return $this->db->update('HB_CARD2', $data) ;
	}

    function get_card_list_by_vender($vender)
    {                         	
        $this->db->join( 'P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD', 'left') ;

        $this->db->where( 'B.TYPE_CD', '2506' ) ;
        $this->db->where( 'B.TEL_COMPANY', $vender ) ;
        $this->db->where( 'B.ISUSE', 'O' ) ;

        return $this->db->get('HB_CARD2 A')->result_array() ;        
    }

    function is_card_info($in_pdt_cd)
    {            	        
        $this->db->where('PDT_CD', $in_pdt_cd) ;         

        return $this->db->get('HB_CARD2')->row_array() ;
    }  

    function get_card_info($in_pdt_cd)
    {            	
        $this->db->join( 'P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD', 'right') ;

        $this->db->where('B.PDT_CD', $in_pdt_cd) ;         

        return $this->db->get('HB_CARD2 A')->row_array() ;
    }    

    function list_result($limit, $offset, $vender)
    {    
        $this->db->limit($limit, $offset) ;
        
        $this->db->join( 'P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD', 'right') ;

        $this->db->where( 'B.TYPE_CD', '2506' ) ;
        $this->db->where( 'B.TEL_COMPANY', $vender ) ;
        $this->db->where( 'B.ISUSE', 'O' ) ;

        $this->db->order_by('A.PDT_CD', 'desc') ;

        return $this->db->get('HB_CARD2 A')->result_array() ; 
    }

    function list_count($vender) 
    {       
        $this->db->join( 'P_PDTMASTER B', 'A.PDT_CD = B.PDT_CD', 'right') ;

        $this->db->where( 'B.TYPE_CD', '2506' ) ;
        $this->db->where( 'B.TEL_COMPANY', $vender ) ;
        $this->db->where( 'B.ISUSE', 'O' ) ;

        return $this->db->count_all_results('HB_CARD2 A') ; 
    }  
}
?>