<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once APPPATH."/third_party/PHPExcel.php"; 
 
class Excel extends PHPExcel 
{ 
    public function __construct() 
    { 
        parent::__construct(); 
    } 

    function CreateExcelFileWithHBDefaultStyle($in_Title, $endRow)
    {
    	$this->SetHBDefaultStyle($in_Title, $endRow) ;
    	$this->CreateExcelFile($in_Title) ;
    }

    function SetHBDefaultStyle($in_Title, $endRow, $SheetIndex = 0)
    {
    	// 셀 style
		{		
			$this->setActiveSheetIndex($SheetIndex);
			$sheet = $this->getActiveSheet();

			// Rename sheet
			$sheet->setTitle($in_Title) ;
	 
			// 문서 열어볼시 미리 선택되어지는 셀 설정
			$sheet->setSelectedCellByColumnAndRow(0, $endRow) ;

			// 보더 스타일 지정
			$defaultBorder = array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb'=>'000000')
			);

			$headBorder = array(
				'borders' => array(
					'bottom' => $defaultBorder,
					'left'   => $defaultBorder,
					'top'    => $defaultBorder,
					'right'  => $defaultBorder
				)
			);			

			$endCol = $sheet->getHighestDataColumn() ;		
			
			$sheet->getStyle("A4:".$endCol.$endRow)->applyFromArray( $headBorder );
			$sheet->getStyle("A4:".$endCol.$endRow)->getAlignment()->setWrapText(true);

			// 셀 정렬
	 		$sheet->getStyle("A3:".$endCol.$endRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER) ;
	 		$sheet->getStyle("A3:".$endCol.$endRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER) ;	 

			// 상단 바
			{
				$objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setName('Logo');
				$objDrawing->setDescription('Logo');
				$objDrawing->setPath('./data/img/login_bg.jpg');
				$objDrawing->setHeight(50);
				$objDrawing->setWorksheet($sheet);
				$sheet->getColumnDimension('A')->setWidth(12);	
				$sheet->getRowDimension(1)->setRowHeight(40);	
	    		
				$sheet->setCellValue('D1', $in_Title);				
				$sheet->setCellValue('F1', PHPExcel_Shared_Date::PHPToExcel( gmmktime(0,0,0,date('m'),date('d'),date('Y')) ));
				$sheet->getStyle('F1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);			


				$sheet->getStyle("D1:F1")->applyFromArray(
					array('font'   => array(
							'size'	 => 14, 
							'bold'   => true,
							'color'  => array('argb'=>'9c8061')))) ;							

				$sheet->getStyle('A1:'.$endCol.'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$sheet->getStyle('A1:'.$endCol.'1')->getFill()->getStartColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);	

				$sheet->getStyle('A1:'.$endCol.'1')->applyFromArray( $headBorder ) ;
			}

			// TR 바
			{
				$sheet->getStyle("A3:{$endCol}3")->applyFromArray(
					array(
						'font'    => array(
							'bold'      => true,
							'color' => array('argb'=>'FFFFFFFF')
						),
						'borders' => array(
							'bottom' => $defaultBorder,
							'left'   => $defaultBorder,
							'top'    => $defaultBorder,
							'right'  => $defaultBorder
						),
						'fill' => array(
				 			'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
				  			'rotation'   => 90,
				 			'startcolor' => array(
				 				'argb' => 'FFFFFFFF'
				 			),
				 			'endcolor'   => array(
				 				'argb' => '4765a0'
				 			)
				 		)
					)
				);
			}

			foreach (range('A', $endCol) as $col) 
		 	{		 		
		 		if ( $col == 'J' || $col == 'L' )
		 		{
		 			$sheet->getColumnDimension($col)->setWidth('12');
		 		}
		 		else
		 		{
		 			$sheet->getColumnDimension($col)->setAutoSize(true);	
		 		}
    		} 	

    		$sheet->calculateColumnWidths();     	
		}
    }

    function CreateExcelFile($in_title)
    {
    	// 엑셀 파일 오픈시 활성화될 시트
    	$this->setActiveSheetIndex(0);

    	$filename = $in_title.'_'.gmdate('Ymd') ;

		// 파일의 저장형식이 utf-8일 경우 한글파일 이름은 깨지므로 euc-kr로 변환해준다.
		//$filename = iconv("UTF-8", "EUC-KR", "한글 파일명");

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');		
		header('Cache-Control: max-age=0');
 
		//echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' ;
 
		$objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel2007');
		$objWriter->save('php://output');    
    }

    function SetStyleNoTileBar($in_Title, $endRow, $SheetIndex = 0)
    {
    	// 셀 style
		{		
			$this->setActiveSheetIndex($SheetIndex);
			$sheet = $this->getActiveSheet();

			// Rename sheet
			$sheet->setTitle($in_Title) ;
	 
			// 문서 열어볼시 미리 선택되어지는 셀 설정
			$sheet->setSelectedCellByColumnAndRow(0, $endRow) ;

			// 보더 스타일 지정
			$defaultBorder = array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb'=>'000000'),
			);

			$headBorder = array(
				'font'    => array(							
							'color' => array('argb'=>'00000000'),
							'size'  => 9,
        					'name'  => '맑은 고딕'
						),
				'borders' => array(
					'bottom' => $defaultBorder,
					'left'   => $defaultBorder,
					'top'    => $defaultBorder,
					'right'  => $defaultBorder
				)
			);			

			$endCol = $sheet->getHighestDataColumn() ;		
			
			$sheet->getStyle("A1:".$endCol.$endRow)->applyFromArray( $headBorder );
			$sheet->getStyle("A1:".$endCol.$endRow)->getAlignment()->setWrapText(true);			

			$sheet->getStyle("A1:".$endCol.$endRow)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

			// 셀 정렬
	 		$sheet->getStyle("A1:".$endCol.$endRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER) ;
	 		$sheet->getStyle("A1:".$endCol.$endRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER) ;	 

			// TR 바
			{
				$sheet->getStyle("A1:{$endCol}1")->applyFromArray(
					array(
						'font'    => array(
							'bold'      => true,
							'color' => array('argb'=>'00000000'),
							'size'  => 9,
        					'name'  => '맑은 고딕'
						),
						'borders' => array(
							'bottom' => $defaultBorder,
							'left'   => $defaultBorder,
							'top'    => $defaultBorder,
							'right'  => $defaultBorder
						),
						/*'fill' => array(
				 			'type'       => PHPExcel_Style_Fill::FILL_SOLID,	
				 			'color' => array('argb'=>'D9D9D9')			  							 			
				 		)*/
					)
				);
			}			

			foreach (range('A', $endCol) as $col) 
		 	{		 		
		 		$sheet->getColumnDimension($col)->setAutoSize(true);			 		
    		} 	

    		$sheet->calculateColumnWidths();     	
		}
    }
}