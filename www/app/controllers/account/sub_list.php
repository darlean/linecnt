<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sub_list extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('Member_info_model', 'OrderMaster_model'));
        $this->load->helper(array( 'url' ));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function support()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;
        
        $ord_type  = $seg->get('ord_type', 0); // tab
        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();

        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜*/

        $mb_id = $this->session->userdata('ss_mb_id') ;

        $result = $this->Member_info_model->sub_member_two_my_tree($mb_id) ;
        $a_user_id = '' ;
        $b_user_id = '' ;
        
        foreach ($result as $key => $row) 
        {
            if ( $key == 0 )
            {
                $a_user_id = $row['USERID'] ;
            }
            else if ( $key == 1 )
            {
                $b_user_id = $row['USERID'] ;
            }
        }

        $a_total_count = $this->OrderMaster_model->get_support_sub_list_count($a_user_id, $ord_type) ;
        $b_total_count = $this->OrderMaster_model->get_support_sub_list_count($b_user_id, $ord_type) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/account/sub_list/support/ord_type/'.$ord_type.'/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = max($a_total_count, $b_total_count) ;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $a_result = $this->OrderMaster_model->get_support_sub_list($a_user_id, $config['per_page'], $offset, $ord_type) ;
        $b_result = $this->OrderMaster_model->get_support_sub_list($b_user_id, $config['per_page'], $offset, $ord_type) ;

        // 일반 리스트
        $a_list = array();
        foreach ($a_result as $i => $row) 
        {
            $a_list[$i]                       = $row ;
            $a_list[$i]['num']                = $a_total_count - ($page - 1) * $config['per_page'] - $i ;
        }

        $b_list = array();
        foreach ($b_result as $i => $row) 
        {
            $b_list[$i]                       = $row ;
            $b_list[$i]['num']                = $b_total_count - ($page - 1) * $config['per_page'] - $i ;
        }

        $a_total = $this->OrderMaster_model->get_support_cv_pv_subtree($a_user_id, $ord_type) ;
        $b_total = $this->OrderMaster_model->get_support_cv_pv_subtree($b_user_id, $ord_type) ;

        $a_sub_total_cv = isset($a_total['CV']) ? number_format($a_total['CV']) : 0 ;
        $a_sub_total_pv = isset($a_total['PV']) ? number_format($a_total['PV']) : 0 ;
        $b_sub_total_cv = isset($b_total['CV']) ? number_format($b_total['CV']) : 0 ;
        $b_sub_total_pv = isset($b_total['PV']) ? number_format($b_total['PV']) : 0 ; 

        $head = array('title' => '후원하선매출');
        $data = array(  
            'a_user_id' => $a_user_id,
            'b_user_id' => $b_user_id,
            'a_list' => $a_list,
            'b_list' => $b_list,
            'a_sub_total_cv' => $a_sub_total_cv,
            'a_sub_total_pv' => $a_sub_total_pv,
            'b_sub_total_cv' => $b_sub_total_cv,
            'b_sub_total_pv' => $b_sub_total_pv,
            'paging' => $CI->pagination->create_links(),  

            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜

            'a_total_count'     => $a_total_count,
            'b_total_count'     => $b_total_count,

            'ord_type_list' => array( 0 => '개통완료', 1 => '주문', 2 => '반품', 3 => '교환', 4 => '취소'),
            'ord_type'      => $ord_type,
            'qstr'          => $qstr,
        );

        widget::run('head', $head);
        $this->load->view('account/support', $data);
        widget::run('tail');
    }

    function recommand()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $ord_type  = $seg->get('ord_type', 0); // tab
        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sca   = $param->get('sca');   // 분류

        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜*/

        // 정렬
        if (!$sst) 
        {
            $sst = 'A.ORDNO';
            $sod = 'desc';            
        }
        else
        {
            $sst = preg_match("/^(A.ORDNO|A.ORD_DATE|A.PV1|A.PV2|A.PRICE|A.VAT|A.ORD_TYPE|A.AMT|RLV)$/i", $sst) ? $sst : FALSE;
        }

        $mb_id = $this->session->userdata('ss_mb_id') ;
        
        $total_count = $this->OrderMaster_model->get_recommand_sub_list_count($mb_id, $ord_type) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/account/sub_list/recommand/ord_type/'.$ord_type.'/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->OrderMaster_model->get_recommand_sub_list($mb_id, $sst, $sod, $config['per_page'], $offset, $ord_type) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i ;
        }

        if( IS_MANAGER )
        {
            $head = array('title' => '전체매출');
        }
        else
        {
            $head = array('title' => '추천하선매출');
        }

        $data = array(
            'total_count' => $total_count,  
            'sst' => $sst,
            'sod' => $sod,          
            'sca' => $sca,
            'list' => $list,
            'sub_total' => $this->OrderMaster_model->get_recommand_cv_pv_subtree($mb_id),
            'paging' => $CI->pagination->create_links(),  

            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜
    
            'sort_no' => $param->sort('A.ORDNO', 'desc'), 
            'sort_rlv' => $param->sort('RLV', 'desc'), 
            'sort_ord_date' => $param->sort('A.ORD_DATE', 'desc'), 
            'sort_cv' => $param->sort('A.PV1', 'desc'), 
            'sort_pv' => $param->sort('A.PV2', 'desc'), 
            'sort_price' => $param->sort('A.PRICE', 'desc'), 
            'sort_vat' => $param->sort('A.VAT', 'desc'), 
            'sort_ord_type' => $param->sort('A.ORD_TYPE', 'desc'), 
            'sort_amt' => $param->sort('A.AMT', 'desc'), 

            'ord_type_list' => array( 0 => '개통완료', 1 => '주문', 2 => '반품', 3 => '교환', 4 => '취소'),
            'ord_type'      => $ord_type,
            'qstr'          => $qstr,
        );

        widget::run('head', $head);
        $this->load->view('account/recommand', $data);
        widget::run('tail');
    }
}
?>