<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Used_mobile extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('HB_sell_used_mobile_model', 'S_Center_model'));
        $this->load->helper(array( 'url' ));
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function excel_seller()
    {
        $this->load->library('excel') ;

        // Create new PHPExcel object
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");
 
        $i = 3 ;
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$i", "NO")
                    ->setCellValue("B$i", "주문번호")
                    ->setCellValue("C$i", "상품명")
                    ->setCellValue("D$i", "사업자명")
                    ->setCellValue("E$i", "연락처")
                    ->setCellValue("F$i", "은행")
                    ->setCellValue("G$i", "계좌번호")
                    ->setCellValue("H$i", "접수일자")
                    ->setCellValue("I$i", "매입일자")
                    ->setCellValue("J$i", "확정매입가")
                    ->setCellValue("K$i", "판매센터");

        $data = $this->seller('excel') ;
        $i++ ;
        foreach ($data['list'] as $key => $row) 
        {            
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A$i", "$row[num]")
                        ->setCellValue("B$i", "$row[ORDNO]")
                        ->setCellValue("C$i", "$row[PDT_NAME]")
                        ->setCellValue("D$i", $row['USERNAME'].'('.$row['USERID'].')')
                        ->setCellValue("E$i", "$row[ORD_MOBILE]")
                        ->setCellValue("F$i", "$row[BANK]은행")
                        ->setCellValueExplicit("G$i", "$row[ACCOUNT]", PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValue("H$i", "$row[ORD_DATE]")
                        ->setCellValue("I$i", "$row[CONFIRM_DATE]")
                        ->setCellValue("J$i", "$row[CONFIRM_AMT]")
                        ->setCellValue("K$i", "$row[ORD_CENTER]");

            $i++ ;
        }

        $title = '회원정산내역';
        $i-- ;

        $objPHPExcel->getActiveSheet()->getStyle('F4:K'.$i)->getNumberFormat()->setFormatCode('#,##0') ;
        $objPHPExcel->CreateExcelFileWithHBDefaultStyle($title, $i) ;
    }

    function seller($excel='')
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sca   = $param->get('sca');   // 분류
        $sfl   = $param->get('sfl');   // 검색필드
        $stx   = $param->get('stx');   // 검색어

        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜

        // 정렬
        if (!$sst)
        {
            $sst = 'ORDNO';
            $sod = 'desc';
        }

        $total_count = $this->HB_sell_used_mobile_model->list_count($sfl, $stx, '', '2', '', 'completed');

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/account/used_mobile/seller/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        //  페이지 선택 후,  검색시 오류 방지
        $page = ( $total_count < ($page - 1) * $config['per_page'] ) ? 1 : $page ;

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        if ( $excel == 'excel' )
        {
            $limit = $total_count ;
            $offset = 0 ;
        }
        else
        {   
            $limit = $config['per_page'] ;
            $offset = ($page - 1) * $config['per_page'];
        }

        $result = $this->HB_sell_used_mobile_model->list_result($sst, $sod, $sfl, $stx, $limit, $offset, '', '2', '', 'completed');

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i ;
            $list[$i]['href']               = '/order/view/used_mobile/ord_no/' . $row['ORDNO'];
        }

        $data = array(
            'total_count' => $total_count,  
            'sst' => $sst,
            'sod' => $sod,          
            'sca' => $sca,
            'list' => $list,
            'paging' => $CI->pagination->create_links(),

            'qstr'  => $qstr,

            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜

            'sort_no' => $param->sort('ORDNO', 'desc'),
        );

        if ( $excel == 'excel' )
        {
            return $data ;
        }
        else
        {
            $head = array('title' => '회원정산내역') ;
            widget::run('head', $head);
            $this->load->view('account/used_mobile_seller', $data);
            widget::run('tail');
        }
    }

    function excel_center()
    {
        $this->load->library('excel') ;

        // Create new PHPExcel object
        $objPHPExcel = $this->excel ;

        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");

        $i = 3 ;
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$i", "NO")
            ->setCellValue("B$i", "센터명")
            ->setCellValue("C$i", "연락처")
            ->setCellValue("D$i", "은행")
            ->setCellValue("E$i", "계좌번호")
            ->setCellValue("F$i", "정산건수")
            ->setCellValue("G$i", "정산금액");

        $data = $this->center('excel') ;
        $i++ ;
        foreach ($data['list'] as $key => $row)
        {
            $account_price = number_format($row['CNT']*10000)." 원" ;

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A$i", "$row[num]")
                ->setCellValue("B$i", "$row[CENTER_NAME]")
                ->setCellValue("C$i", "$row[TEL]")
                ->setCellValue("D$i", "$row[BANK]은행")
                ->setCellValueExplicit("E$i", "$row[ACCOUNT]", PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("F$i", "$row[CNT]")
                ->setCellValueExplicit("G$i", "$account_price", PHPExcel_Cell_DataType::TYPE_STRING) ;

            $i++ ;
        }

        $title = '센터정산내역';
        $i-- ;

        $objPHPExcel->getActiveSheet()->getStyle('F4:G'.$i)->getNumberFormat()->setFormatCode('#,##0') ;
        $objPHPExcel->CreateExcelFileWithHBDefaultStyle($title, $i) ;
    }

    function center($excel='')
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();

        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜
        
        $total_count = $this->HB_sell_used_mobile_model->center_list_count() ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/account/used_mobile/center/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        //  페이지 선택 후,  검색시 오류 방지
        $page = ( $total_count < ($page - 1) * $config['per_page'] ) ? 1 : $page ;

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        if ( $excel == 'excel' )
        {
            $limit = $total_count ;
            $offset = 0 ;
        }
        else
        {
            $limit = $config['per_page'] ;
            $offset = ($page - 1) * $config['per_page'];
        }

        $result = $this->HB_sell_used_mobile_model->center_list_result($limit, $offset) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row)
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i ;
            $list[$i]['href']               = '/order/lists_used_mobile/center/' . $row['ORD_CENTER'];
        }

        $data = array(
            'total_count' => $total_count,  
            'list' => $list,
            'paging' => $CI->pagination->create_links(),

            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜

            'qstr'  => $qstr,

            'sort_no' => $param->sort('ORD_CENTER', 'desc'),
        );

        if ( $excel == 'excel' )
        {
            return $data ;
        }
        else
        {
            $head = array('title' => '센터정산내역') ;
            widget::run('head', $head);
            $this->load->view('account/used_mobile_center', $data);
            widget::run('tail');
        }
    }

    function set_deposit()
    {
        $is_deposit = $this->param->get('is_deposit') ;
        $arr_ordno = $this->param->get('arr_ordno') ;
        $arr_ordno = explode(".", $arr_ordno) ;

        $is_deposit = ($is_deposit == 'true') ? 'O' : 'X' ;

        $modify_or_data['IS_DEPOSIT'] = $is_deposit ;

        foreach ($arr_ordno as $key => $value)
        {
            if ( $value != '' )
            {
                $result = $this->HB_sell_used_mobile_model->update($value, $modify_or_data) ;
            }
        }

        if ( $result == true )
        {
            alert('정상적으로 정보가 수정되었습니다.', URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }
}
?>