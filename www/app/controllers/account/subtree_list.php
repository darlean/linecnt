<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sub_list extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('OrderMaster_model'));
        $this->load->helper(array( 'url' ));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'page':
            case 'index':
                $this->_list();
            break;
            default:
                show_404();
            break;
        }
    }

    function _list() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $cs_no = $seg->get('cs_no');   // 게시물아이디
        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $seg->replace('cs_no').$param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sfl   = $param->get('sfl');   // 검색필드
        $stx   = $param->get('stx');   // 검색어
        $sca   = $param->get('sca');   // 분류

        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜*/

        // 정렬
        if (!$sst) 
        {
            $sst = 'ORDNO';
            $sod = 'desc';            
        }
        else
        {
            $sst = preg_match("/^(ORDNO|ORDNO_ORG|GT_CDS|ORD_DATES|PV1|PRICE|VAT|ORD_TYPES|AMT)$/i", $sst) ? $sst : FALSE;
        }

        $mb_id = $this->session->userdata('ss_mb_id') ;
        
        $total_count = $this->OrderMaster_model->get_pv_list_count($mb_id, $sfl, $stx) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/account/lists/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->OrderMaster_model->get_pv_list($mb_id, $sst, $sod, $sfl, $stx, $config['per_page'], $offset) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i ;
        }

        $head = array('title' => '본인실적(PV)내역서');
        $data = array(
            'total_count' => $total_count,  
            'sst' => $sst,
            'sod' => $sod,          
            'sca' => $sca,
            'sfl' => $sfl,
            'stx' => $stx,
            'list' => $list,
            'paging' => $CI->pagination->create_links(),  

            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜
    
            'sort_no' => $param->sort('ORDNO', 'desc'), 
            'sort_ordno_org' => $param->sort('ORDNO_ORG', 'desc'), 
            'sort_gt_cds' => $param->sort('GT_CD', 'desc'), 
            'sort_ord_date' => $param->sort('ORD_DATE', 'desc'), 
            'sort_pv' => $param->sort('PV1', 'desc'), 
            'sort_price' => $param->sort('PRICE', 'desc'), 
            'sort_vat' => $param->sort('VAT', 'desc'), 
            'sort_ord_type' => $param->sort('ORD_TYPE', 'desc'), 
            'sort_amt' => $param->sort('AMT', 'desc'), 
        );

        widget::run('head', $head);
        $this->load->view('account/pv_list', $data);
        widget::run('tail');
    }
}
?>