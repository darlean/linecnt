<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grp_CV extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('Pay_model', 'Pay_week_n_detail_model'));
        $this->load->helper(array( 'url' ));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'pay_date':            
            case 'index':
                $this->index();
            break;
            default:
                show_404();
            break;
        }
    }

    function index()
    {       
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소        
        $seg      =& $this->seg;                

        $pay_date_list = $this->Pay_model->get_pay_date_list() ;
        $pay_date  = $seg->get('pay_date', $pay_date_list[0]['PAY_DATE']);        

        $head = array('title' => 'N값');        
        widget::run('head', $head);

        if ( $pay_date < '20151123' )
        {
            $data = $this->getData($pay_date) ;
            $data['pay_date_list'] = $pay_date_list ;

            $this->load->view('account/grp_cv', $data);
        }
        else if ( $pay_date >= '20161219' )
        {
            $data = $this->getData3($pay_date) ;
            $data['str_n'] = array('골드', '루비', '사파이어', '다이아몬드', '크라운') ;

            $data['pay_date_list'] = $pay_date_list ;

            $this->load->view('account/grp_cv3', $data);
        }
        else if ( $pay_date >= '20160620' )
        {
            $data = $this->getData2($pay_date) ;
            $data['str_n'] = array('전체', '골드', '루비', '사파이어', '다이아몬드', '크라운') ;

            $data['pay_date_list'] = $pay_date_list ;

            $this->load->view('account/grp_cv2', $data);
        }
        else
        {
            // 날짜 큰 순서부터
            if ( $pay_date >= '20151221' )
            {            
                $data = $this->getData1($pay_date, 0.7, 0.5, 0.5, 0.3, 0.2, true, true) ;                         
                $data['str_n'] = array('전체', '골드', '루비', '사파이어') ;
            }    
            else if ( $pay_date >= '20151214' )
            {            
                $data = $this->getData1($pay_date, 0.7, 0.5, 0.5, 0.3, 0.2, true) ;                         
                $data['str_n'] = array('전체', '골드', '루비', '사파이어') ;
            }    
            else if ( $pay_date >= '20151130' )
            {
                $data = $this->getData1($pay_date, 0.5, 1, 0.53, 0.27, 0.20) ;                
                $data['str_n'] = array('골드', '루비', '사파이어') ;
            }
            else if ( $pay_date >= '20151123' )
            {            
                $data = $this->getData1($pay_date, 0.5, 1, 0.45, 0.35, 0.20) ;                
                $data['str_n'] = array('골드', '루비', '사파이어') ;
            }                    
            
            $data['pay_date_list'] = $pay_date_list ;

            $this->load->view('account/grp_cv1', $data);
        }
                
        widget::run('tail');
    }

    function getData3($in_pay_date)
    {
        $result = $this->Pay_week_n_detail_model->get_n_detail_info_list($in_pay_date) ;

        $n_gd = $n_rb = $n_sp = $n_dd = $n_cw = 0 ;
        $grp_gd = $grp_rb = $grp_sp = $grp_dd = $grp_cw = 0 ;
        $total_amt_wcy_grp = 0 ;
        $cnt_under_20 = 0 ;
        $amt_total = 0 ;

        $cnt = count($result) ;
        $run_seq = $result[$cnt - 1]['RUN_SEQ'] ;

        $pay_detail_list = array() ;
        foreach ($result as $row)
        {
            if ( $row['RUN_SEQ'] >= $run_seq - 5 && ( $row['RUN_SEQ'] < $run_seq || $run_seq == 1 ) )
            {
                $n_gd += $row['AMT_WCY_N_GD'];
                $n_rb += $row['AMT_WCY_N_RB'];
                $n_sp += $row['AMT_WCY_N_SP'];
                $n_dd += $row['AMT_WCY_N_DD'];
                $n_cw += $row['AMT_WCY_N_CW'];

                array_push($pay_detail_list, $row['AMT_WCY_USE_CV']) ;
            }

            if ( $row['RUN_SEQ'] == 1 )
            {
                $grp_gd = $row['AMT_WCY_GRP_CV_GD'];
                $grp_rb = $row['AMT_WCY_GRP_CV_RB'];
                $grp_sp = $row['AMT_WCY_GRP_CV_SP'];
                $grp_dd = $row['AMT_WCY_GRP_CV_DD'];
                $grp_cw = $row['AMT_WCY_GRP_CV_CW'];

                $total_amt_wcy_grp = ($grp_gd + $grp_rb + $grp_sp + $grp_dd + $grp_cw);
            }

            if ( ($row['RUN_SEQ'] == 0 ) || ( ($row['RUN_SEQ'] % 10 ) == 6 ) )
            {
                if ($row['RUN_SEQ'] == 0 )
                {
                    $amt_total = $row['AMT_WCY_USE_CV'] ;
                }

                $cnt_under_20 += $row['FORCED_GD'] ;
            }
        }

        $n_total =  $n_gd + $n_rb + $n_sp + $n_dd + $n_cw ;
        $arr_n = array($n_gd, $n_rb, $n_sp, $n_dd, $n_cw) ;
        $arr_grp = array($grp_gd, $grp_rb, $grp_sp, $grp_dd, $grp_cw) ;

        $data = array(
            'pay_date'      => $in_pay_date,
            'pay_detail_list' => $pay_detail_list,

            'cnt_under_20'  => $cnt_under_20,
            'amt_total'     => $amt_total,

            'arr_n'         => $arr_n,
            'n_total'       => $n_total,

            'arr_grp'       => $arr_grp,
            'total_amt_wcy_grp' => $total_amt_wcy_grp,
        );

        return $data ;
    }

    function getData2($in_pay_date)
    {
        $pay_calc_info = $this->Pay_model->get_pay_calc_info($in_pay_date) ;

        $fund_all = $pay_calc_info['AMT_WCY_USE_CV'] ;
        $fund_gd = $pay_calc_info['AMT_WCY_USE_CV_GD'] ;
        $fund_rb = $pay_calc_info['AMT_WCY_USE_CV_RB'] ;
        $fund_sp = $pay_calc_info['AMT_WCY_USE_CV_SP'] ;
        $fund_dd = $pay_calc_info['AMT_WCY_USE_CV_DD'] ;
        $fund_cw = $pay_calc_info['AMT_WCY_USE_CV_CW'] ;

        $n_all = $pay_calc_info['AMT_WCY_N'] ;
        $n_gd = $pay_calc_info['AMT_WCY_N_GD'] ;
        $n_rb = $pay_calc_info['AMT_WCY_N_RB'] ;
        $n_sp = $pay_calc_info['AMT_WCY_N_SP'] ;
        $n_dd = $pay_calc_info['AMT_WCY_N_DD'] ;
        $n_cw = $pay_calc_info['AMT_WCY_N_CW'] ;

        $n_total = $n_all + $n_gd + $n_rb + $n_sp + $n_dd + $n_cw ;

        $arr_n = array($n_all, $n_gd, $n_rb, $n_sp, $n_dd, $n_cw) ;
        $arr_fund = array($fund_all, $fund_gd, $fund_rb, $fund_sp, $fund_dd, $fund_cw) ;

        $arr_grp = array($pay_calc_info['AMT_WCY_GRP_CV'], $pay_calc_info['AMT_WCY_GRP_CV_GD'], $pay_calc_info['AMT_WCY_GRP_CV_RB'], $pay_calc_info['AMT_WCY_GRP_CV_SP'], $pay_calc_info['AMT_WCY_GRP_CV_DD'], $pay_calc_info['AMT_WCY_GRP_CV_CW']) ;
        $total_amt_wcy_grp = $pay_calc_info['AMT_WCY_GRP_CV'] + $pay_calc_info['AMT_WCY_GRP_CV_GD'] + $pay_calc_info['AMT_WCY_GRP_CV_RB'] + $pay_calc_info['AMT_WCY_GRP_CV_SP'] + $pay_calc_info['AMT_WCY_GRP_CV_DD'] + $pay_calc_info['AMT_WCY_GRP_CV_CW'] ;

        $data = array(
            'pay_date'      => $in_pay_date,
            'pay_calc_info' => $pay_calc_info,

            'arr_n'         => $arr_n,
            'n_total'       => $n_total,

            'arr_fund'      => $arr_fund,
            'fund_total'    => $fund_all,

            'arr_grp'       => $arr_grp,
            'total_amt_wcy_grp' => $total_amt_wcy_grp,
        );

        return $data ;
    }

    function getData1($in_pay_date, $in_com_rate, $in_rank_rate, $in_gd_rate, $in_rb_rate, $in_sp_rate, $in_view_all = false, $in_vision_calculated = false)
    {
        $pay_calc_info = $this->Pay_model->get_pay_calc_info($in_pay_date) ;

        $arr_save_cv = array() ;
        
        if ( $in_vision_calculated )
        {            
            $fund_all = $pay_calc_info['AMT_WCY_USE_CV'] ;// + $pay_calc_info['AMT_WCY_SAVE_CV'] ;    
            $fund_gd = $pay_calc_info['AMT_WCY_USE_CV_GD'] ;// + $pay_calc_info['AMT_WCY_SAVE_CV_GD'] ;    
            $fund_rb = $pay_calc_info['AMT_WCY_USE_CV_RB'] ;// + $pay_calc_info['AMT_WCY_SAVE_CV_RB'] ;    
            $fund_sp = $pay_calc_info['AMT_WCY_USE_CV_SP'] ;// + $pay_calc_info['AMT_WCY_SAVE_CV_SP'] ;
            $fund_dd = $pay_calc_info['AMT_WCY_USE_CV_DD'] ;// + $pay_calc_info['AMT_WCY_SAVE_CV_SP'] ;
            
            $n_all = $pay_calc_info['AMT_WCY_N'] ;
            $n_gd = $pay_calc_info['AMT_WCY_N_GD'] ;
            $n_rb = $pay_calc_info['AMT_WCY_N_RB'] ;
            $n_sp = $pay_calc_info['AMT_WCY_N_SP'] ;
            $n_dd = $pay_calc_info['AMT_WCY_N_DD'] ;

            $arr_save_cv = array($pay_calc_info['AMT_WCY_SAVE_CV'], $pay_calc_info['AMT_WCY_SAVE_CV_GD'], $pay_calc_info['AMT_WCY_SAVE_CV_RB'], $pay_calc_info['AMT_WCY_SAVE_CV_SP'], $pay_calc_info['AMT_WCY_SAVE_CV_DD']) ;
        }
        else
        {
            $fund_all = $pay_calc_info['COM_PV2'] * $in_com_rate * $in_rank_rate ;    
            $fund_gd = $pay_calc_info['COM_PV2'] * $in_com_rate * $in_rank_rate * $in_gd_rate ;
            $fund_rb = $pay_calc_info['COM_PV2'] * $in_com_rate * $in_rank_rate * $in_rb_rate ;      
            $fund_sp = $pay_calc_info['COM_PV2'] * $in_com_rate * $in_rank_rate * $in_sp_rate ;
            //$fund_dd = $pay_calc_info['COM_PV2'] * $in_com_rate * $in_rank_rate * $in_dd_rate ;

            $n_all = floor(($pay_calc_info['AMT_WCY_GRP_CV'] > 0 ) ? ($fund_all / $pay_calc_info['AMT_WCY_GRP_CV']) : 0) ;
            $n_gd = floor(($pay_calc_info['AMT_WCY_GRP_CV_GD'] > 0 ) ? ($fund_gd / $pay_calc_info['AMT_WCY_GRP_CV_GD']) : 0) ;
            $n_rb = floor(($pay_calc_info['AMT_WCY_GRP_CV_RB'] > 0 ) ? ($fund_rb / $pay_calc_info['AMT_WCY_GRP_CV_RB']) : 0) ;        
            $n_sp = floor(($pay_calc_info['AMT_WCY_GRP_CV_SP'] > 0 ) ? ($fund_sp / $pay_calc_info['AMT_WCY_GRP_CV_SP']) : 0) ;
            $n_dd = floor(($pay_calc_info['AMT_WCY_GRP_CV_DD'] > 0 ) ? ($fund_sp / $pay_calc_info['AMT_WCY_GRP_CV_DD']) : 0) ;
        }

        if ( $in_view_all )
        {
            $n_total = $n_all + $n_gd + $n_rb + $n_sp ;        
            
            $arr_n = array($n_all, $n_gd, $n_rb, $n_sp) ;
            $arr_fund = array($fund_all, $fund_gd, $fund_rb, $fund_sp) ;        
        }
        else
        {
            $n_total = $n_gd + $n_rb + $n_sp ;        
            
            $arr_n = array($n_gd, $n_rb, $n_sp) ;
            $arr_fund = array($fund_gd, $fund_rb, $fund_sp) ;   
        }

        $user_grv_cv = 0 ;
        $user_pay = 0 ;

        if ( !IS_MANAGER && !IS_AGENCY )
        {
            $user_id = $this->session->userdata('ss_mb_id') ; 

            $user_grp_info = $this->Pay_model->get_user_grp_info($user_id, $in_pay_date) ;

            if ( isset($user_grp_info) && $user_grp_info )        
            {
                $user_grv_cv = $user_grp_info['CALC_GRP_CV'] ;
                $user_pay += $n * $user_grv_cv ;     

                if ( $user_grp_info['OLD_RANK_CD'] == '31' )   
                {
                    $user_pay += $n_gd * $user_grv_cv ;
                }
                else if ( $user_grp_info['OLD_RANK_CD'] == '41' )   
                {
                    $user_pay += $n_rb * $user_grv_cv ;
                }
                else if ( $user_grp_info['OLD_RANK_CD'] == '51' )   
                {
                    $user_pay += $n_sp * $user_grv_cv ;
                }                
            }
        }    

        if ( $in_view_all )
        {
            $arr_grp = array($pay_calc_info['AMT_WCY_GRP_CV'], $pay_calc_info['AMT_WCY_GRP_CV_GD'], $pay_calc_info['AMT_WCY_GRP_CV_RB'], $pay_calc_info['AMT_WCY_GRP_CV_SP'], $user_grv_cv) ;
            $total_amt_wcy_grp = $pay_calc_info['AMT_WCY_GRP_CV'] + $pay_calc_info['AMT_WCY_GRP_CV_GD'] + $pay_calc_info['AMT_WCY_GRP_CV_RB'] + $pay_calc_info['AMT_WCY_GRP_CV_SP'] + $user_grv_cv ;                    

            $arr_rate = array(1, $in_gd_rate, $in_rb_rate, $in_sp_rate) ;
        }
        else
        {
            $arr_grp = array($pay_calc_info['AMT_WCY_GRP_CV_GD'], $pay_calc_info['AMT_WCY_GRP_CV_RB'], $pay_calc_info['AMT_WCY_GRP_CV_SP'], $user_grv_cv) ;
            $total_amt_wcy_grp = $pay_calc_info['AMT_WCY_GRP_CV_GD'] + $pay_calc_info['AMT_WCY_GRP_CV_RB'] + $pay_calc_info['AMT_WCY_GRP_CV_SP'] + $user_grv_cv ;                    

            $arr_rate = array($in_gd_rate, $in_rb_rate, $in_sp_rate) ;
        }
        
        $data = array(            
            'pay_date'      => $in_pay_date,       

            'pay_calc_info' => $pay_calc_info,                         

            'com_rate'      => $in_com_rate,
            'rank_rate'     => $in_rank_rate,   
            'arr_rate'      => $arr_rate,         

            'arr_n'         => $arr_n,
            'n_total'       => $n_total,

            'arr_fund'      => $arr_fund,
            'fund_total'    => $fund_all,

            'arr_grp'       => $arr_grp,
            'total_amt_wcy_grp' => $total_amt_wcy_grp,

            'user_grv_cv'   => $user_grv_cv,       
            'user_pay'      => $user_pay,

            'vision_calculated'  => $in_vision_calculated,
            'arr_save_cv'        => $arr_save_cv,
        );

        return $data ;
    }

    function getData($in_pay_date)
    {    
        $pay_calc_info = $this->Pay_model->get_pay_calc_info($in_pay_date) ;
    
        $fund = $pay_calc_info['COM_PV2'] / 2 * 0.6 ;
        $fund_gd = $pay_calc_info['COM_PV2'] / 2 * 0.25 ;
        $fund_rb = $pay_calc_info['COM_PV2'] / 2 * 0.15 ;      

        $n = floor(($pay_calc_info['AMT_WCY_GRP_CV'] > 0 ) ? ($fund / $pay_calc_info['AMT_WCY_GRP_CV']) : 0) ;
        $n_gd = floor(($pay_calc_info['AMT_WCY_GRP_CV_GD'] > 0 ) ? ($fund_gd / $pay_calc_info['AMT_WCY_GRP_CV_GD']) : 0) ;
        $n_rb = floor(($pay_calc_info['AMT_WCY_GRP_CV_RB'] > 0 ) ? ($fund_rb / $pay_calc_info['AMT_WCY_GRP_CV_RB']) : 0) ;        

        $n_total = $n + $n_gd + $n_rb ;

        $user_grv_cv = 0 ;
        $user_pay = 0 ;

        if ( !IS_MANAGER && !IS_AGENCY )
        {
            $user_id = $this->session->userdata('ss_mb_id') ; 

            $user_grp_info = $this->Pay_model->get_user_grp_info($user_id, $in_pay_date) ;

            if ( isset($user_grp_info) && $user_grp_info )        
            {
                $user_grv_cv = $user_grp_info['CALC_GRP_CV'] ;
                $user_pay += $n * $user_grv_cv ;     

                if ( $user_grp_info['OLD_RANK_CD'] == '31' )   
                {
                    $user_pay += $n_gd * $user_grv_cv ;
                }
                else if ( $user_grp_info['OLD_RANK_CD'] == '41' )   
                {
                    $user_pay += $n_rb * $user_grv_cv ;
                }
            }
        }
        
        $data = array(            
            'pay_date'      => $in_pay_date,       

            'pay_calc_info' => $pay_calc_info,  

            'fund'          => $fund,
            'fund_gd'       => $fund_gd,
            'fund_rb'       => $fund_rb,

            'n'             => $n,
            'n_gd'          => $n_gd,
            'n_rb'          => $n_rb,        
            'n_total'       => $n_total,

            'user_grv_cv'   => $user_grv_cv,       
            'user_pay'      => $user_pay,
        );

        return $data ;
    }
}
?>