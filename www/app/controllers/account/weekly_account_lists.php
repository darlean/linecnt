<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Weekly_account_lists extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('Weekly_account_list_model'));
        $this->load->helper(array( 'url','date' ));
        $this->load->config('cf_order') ;

        define('WIDGET_SKIN', 'main');
        
        $this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
        {
            show_404();
            return;
        }

        if( !IS_MANAGER )
        {
            show_404();
            return;
        }

        switch($index)
        {
            case 'page':
            case 'index':
                $this->_list();
            break;
            default:
                show_404();
            break;
        }
    }

    function _list() 
    {       
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;
        
        $page  = $seg->get('page', 1); // 페이지

        // 총 주문건수     
        $total_count = $this->Weekly_account_list_model->list_count() ;

        $config['suffix']      = '';
        $config['base_url']    = RT_PATH.'/account/weekly_account_lists/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);
        $offset = ($page - 1) * $config['per_page'];

        $result = $this->Weekly_account_list_model->list_result($config['per_page'], $offset) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;            
        }

        $head = array('title' => '주간 정산');
        $data = array(
            'total_count' => $total_count,  
            'list' => $list,
            'paging' => $CI->pagination->create_links(), 
        );

        widget::run('head', $head);
        $this->load->view('account/weekly_account_list', $data);
        widget::run('tail');
    }
}
?>