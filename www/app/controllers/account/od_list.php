<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Od_list extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('OrderMaster_model'));
        $this->load->helper(array( 'url' ));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'ord_type':
            case 'page':
            case 'index':
                $this->_list();
            break;
            default:
                show_404();
            break;
        }
    }

    function _list() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $ord_type  = $seg->get('ord_type', 0); // tab
        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sca   = $param->get('sca');   // 분류

        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜*/

        // 정렬
        if (!$sst) 
        {
            $sst = 'A.ORDNO';
            $sod = 'desc';            
        }
        else
        {
            $sst = preg_match("/^(A.ORDNO|A.GT_CDS|A.ORD_DATES|PV1|PRICE|VAT|AMT)$/i", $sst) ? $sst : FALSE;
        }

        $mb_id = $this->session->userdata('ss_mb_id') ;
        
        $total_count = $this->OrderMaster_model->get_od_list_count($mb_id, $ord_type) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/account/od_list/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->OrderMaster_model->get_od_list($mb_id, $sst, $sod, $config['per_page'], $offset, $ord_type) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i ;
        }

        $head = array('title' => '본인주문내역');
        $data = array(
            'total_count' => $total_count,  
            'sst' => $sst,
            'sod' => $sod,          
            'sca' => $sca,
            'list' => $list,
            'sub_total' => $this->OrderMaster_model->get_member_cv_pv($mb_id),
            'paging' => $CI->pagination->create_links(),  

            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜
    
            'sort_no' => $param->sort('A.ORDNO', 'desc'),  
            'sort_gt_cds' => $param->sort('A.GT_CD', 'desc'), 
            'sort_ord_date' => $param->sort('A.ORD_DATE', 'desc'), 
            'sort_cv' => $param->sort('PV1', 'desc'), 
            'sort_pv' => $param->sort('PV2', 'desc'), 
            'sort_price' => $param->sort('PRICE', 'desc'), 
            'sort_vat' => $param->sort('VAT', 'desc'), 
            'sort_amt' => $param->sort('AMT', 'desc'), 

            'ord_type_list' => array( 0 => '개통완료', 1 => '주문', 2 => '반품', 3 => '교환', 4 => '취소'),
            'ord_type'      => $ord_type,
            'qstr'          => $qstr,
        );

        widget::run('head', $head);
        $this->load->view('account/od_list', $data);
        widget::run('tail');
    }
}
?>