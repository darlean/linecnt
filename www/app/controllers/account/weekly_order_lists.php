<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Weekly_order_lists extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('Order_for_weekly_model', 'Deliverys_model'));
        $this->load->helper(array( 'url','date' ));
        $this->load->config('cf_order') ;

        define('WIDGET_SKIN', 'main');
        
        $this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        if( !IS_MANAGER )
        {
            show_404();
            return;
        }

        switch($index)
        {
            case 'page':
            case 'index':
                $this->_list();
            break;
            default:
                show_404();
            break;
        }
    }

    function _list() 
    {        
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $start_date = $param->get('start_date', FALSE);   // 시작날짜        
        $end_date = $param->get('end_date', FALSE); // 종료날짜
        if( $start_date == FALSE || $end_date == FALSE )
        {
            $last_week = get_week();

            $start_date = $last_week['start_date'];
            $end_date   = $last_week['end_date'];
        }

        $start_day_name = get_day_string( $start_date );
        $end_day_name   = get_day_string( $end_date );

        $last_week = get_last_week($start_date);

        $last_start_date = $last_week['start_date'];
        $last_end_date   = $last_week['end_date'];

        $next_week = get_next_week($start_date);
        if( strtotime( $next_week['end_date'] ) >= strtotime( TIME_YMD ) )
        {
            $next_start_date = '';
            $next_end_date   = '';
        }
        else
        {
            $next_start_date = $next_week['start_date'];
            $next_end_date   = $next_week['end_date'];
        }        

        $seg      =& $this->seg;
        $param    =& $this->param;
        $qstr      = $param->output();

        $page      = $seg->get('page', 1); // 페이지               

        // 총 주문건수     
        $total_count = $this->Order_for_weekly_model->list_count($start_date, $end_date) ;
        // 총 주문 cv, pv 합계
        $sum_value   = $this->Order_for_weekly_model->list_sum($start_date, $end_date) ;

        $config['suffix']      = $qstr;
        $config['base_url']    = RT_PATH.'/account/weekly_order_lists/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->Order_for_weekly_model->list_result($start_date, $end_date, $config['per_page'], $offset) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
            $list[$i]['href']                 = '/order/view/or_id/'.$row['or_id'];
        }

        $head = array('title' => '주간 정산');
        $data = array(
            'total_count' => $total_count,  
            'list' => $list,
            'paging' => $CI->pagination->create_links(),  

            'start_date' => $start_date,
            'end_date' => $end_date,

            'last_start_date' => $last_start_date,
            'last_end_date' => $last_end_date,

            'next_start_date' => $next_start_date,
            'next_end_date' => $next_end_date,

            'start_day_name' => $start_day_name,
            'end_day_name' => $end_day_name,

            'sum_value' => $sum_value,          
        );

        widget::run('head', $head);
        $this->load->view('account/weekly_account', $data);
        widget::run('tail');
    }
}
?>