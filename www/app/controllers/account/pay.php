<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pay extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('Pay_model', 'Pay_week_pt_model'));
        $this->load->helper(array( 'url' ));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function week()
    {
        $head = array('title' => '주마감내역서') ;
        $this->_list('week', $head) ;
    }

    function week_d()
    {
        $head = array('title' => '주마감내역서') ;
        $this->_week('week', $head) ;
    }

    function month()
    {
        $head = array('title' => '월마감내역서') ;
        $this->_list('month', $head) ;
    }

    function excel()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소   
        $seg      =& $this->seg;    
        $type  = $seg->get('type'); 

        $this->load->library('excel') ;

        // Create new PHPExcel object
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");
 
        $i = 3 ;
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$i", "NO")
                    ->setCellValue("B$i", "회원정보")
                    ->setCellValue("C$i", "마감일자")
                    ->setCellValue("D$i", "지급일자")
                    ->setCellValue("E$i", "마감직급")                    
                    ->setCellValue("F$i", "매출HB")
                    ->setCellValue("G$i", "정착지원보너스")
                    ->setCellValue("H$i", "공유보너스")
                    ->setCellValue("I$i", "수당합계")
                    ->setCellValue("J$i", "세금합계")
                    ->setCellValue("K$i", "실지급액");                                    

        if ( $type == 'month' )
        {
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("H$i", "통신 재구매 보너스")
                        ->setCellValue("I$i", "유선 직추천 보너스");
        }

        $data = $this->_list($type, '', 'excel') ;     
        $i++ ;
        foreach ($data['list'] as $key => $row) 
        {            
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A$i", "$row[num]")
                        ->setCellValue("B$i", $row['V_USERNAME'].'('.$row['USERID'].')')                        
                        ->setCellValue("C$i", "$row[PAY_DATES]")                        
                        ->setCellValue("D$i", "$row[GIVE_DATES]")
                        ->setCellValue("E$i", "$row[OLD_FULL_NAME]")                        
                        ->setCellValue("F$i", "$row[PV]")
                        ->setCellValue("G$i", "$row[TOTAL_AMT]")
                        ->setCellValue("J$i", "$row[TOTAL_TAX]")
                        ->setCellValue("K$i", "$row[TRUE_AMT]") ;                                    

            if ( $type == 'month' )
            {
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue("G$i", "$row[AMT_MFEE]")
                            ->setCellValue("H$i", "$row[AMT_MDIRECT]") ;
            }
            else
            {
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("G$i", "$row[AMT_WPT]")
                        ->setCellValue("H$i", "$row[AMT_WCY]") ;
            }

            $i++ ;
        }
        
        if ( $type == 'week' )
        {
            $title = '주마감내역';
        }
        else if ( $type == 'month' )
        {
            $title = '월마감내역';            
        }  

        $i-- ;

        $objPHPExcel->getActiveSheet()->getStyle('F4:L'.$i)->getNumberFormat()->setFormatCode('#,##0') ;        
        $objPHPExcel->CreateExcelFileWithHBDefaultStyle($title, $i) ;
    }

    function _list($in_type, $in_head, $excel='') 
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sca   = $param->get('sca');   // 분류

        $pay_date = '' ;
        $pay_date_list = array() ;

        if ( IS_MANAGER && $in_type == 'week' )
        {
            $pay_date_list = $this->Pay_model->get_pay_date_list() ;
            $pay_date  = $seg->get('pay_date', $pay_date_list[0]['PAY_DATE']) ;  
        }

        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜

        // 정렬
        if (!$sst) 
        {
            $sst = 'A.PAY_DATE';
            $sod = 'desc';            
        }
        else
        {
            $sst = preg_match("/^(A.PAY_DATE|A.CV|A.PV|A.TOTAL_AMT|A.TAX1|A.TRUE_AMT)$/i", $sst) ? $sst : FALSE;
        }

        $mb_id = $this->session->userdata('ss_mb_id') ;
        
        $total_count = $this->Pay_model->get_pay_list_count($in_type, $mb_id, $pay_date) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/account/pay/'.$in_type.'/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        if ( IS_MANAGER && $in_type == 'week' )
        {
            $config['base_url']    = RT_PATH.'/account/pay/'.$in_type.'/pay_date/'.$pay_date.'/page/' ;
        }

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        if ( $excel == 'excel' )
        {
            $limit = $total_count ;
            $offset = 0 ;
        }
        else
        {   
            $limit = $config['per_page'] ;
            $offset = ($page - 1) * $config['per_page'];
        }

        $result = $this->Pay_model->get_pay_list($in_type, $mb_id, $sst, $sod, $limit, $offset, $pay_date) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i ;
        }

        $data = array(
            'total_count' => $total_count,  
            'sst' => $sst,
            'sod' => $sod,          
            'sca' => $sca,
            'list' => $list,
            'pay_date_list' => $pay_date_list,
            'pay_date'  => $pay_date,
            'paging' => $CI->pagination->create_links(),  
            'pay_week_pt_list' => array(),

            'type' => $in_type,
            'qstr'  => $qstr,

            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜
    
            'sort_pay_date' => $param->sort('A.PAY_DATE', 'desc'), 
            'sort_cv' => $param->sort('A.CV', 'desc'), 
            'sort_pv' => $param->sort('A.PV', 'desc'),     
            'sort_total_amt' => $param->sort('A.TOTAL_AMT', 'desc'), 
            'sort_tax1' => $param->sort('A.TAX1', 'desc'), 
            'sort_true_amt' => $param->sort('A.TRUE_AMT', 'desc'), 
        );

        if ( $excel == 'excel' )
        {
            return $data ;
        }
        else
        {
            widget::run('head', $in_head);

            if ( IS_MANAGER && $in_type == 'week' )
            {
                $this->load->view('account/pay1', $data);
            }
            else
            {
                $this->load->view('account/pay', $data);
            }

            widget::run('tail');
        }
    }

    function _week($in_type, $in_head, $excel='') 
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sca   = $param->get('sca');   // 분류

        $pay_date_list = $this->Pay_model->get_pay_date_list() ;
        $pay_date  = $seg->get('pay_date', $pay_date_list[0]['PAY_DATE']) ;  

        $sst = 'A.PAY_DATE';
        $sod = 'desc';            

        $mb_id = $this->session->userdata('ss_mb_id') ;
        
        $total_count = $this->Pay_model->get_pay_list_count($in_type, $mb_id, $pay_date) ;

        $limit = $total_count ;
        $offset = 0 ;

        $result = $this->Pay_model->get_pay_list($in_type, $mb_id, $sst, $sod, $limit, $offset, $pay_date) ;

        $data = array(
            'total_count' => $total_count,  
            'list' => $result,
            'pay_date_list' => $pay_date_list,
            'pay_date'  => $pay_date, 
            'pay_week_pt_list' => $this->Pay_week_pt_model->get_pay_week_pt_list($mb_id, $pay_date),

            'type' => $in_type,
            'qstr'  => $qstr,
        );

        if ( $excel == 'excel' )
        {
            return $data ;
        }
        else
        {            
            widget::run('head', $in_head);
            $this->load->view('account/week_d', $data);
            widget::run('tail');
        }
    }    
}
?>