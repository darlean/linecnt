<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lists extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        //$this->load->model(array('Member_weekly_account_model'));
        $this->load->helper(array( 'url' ));
        //$this->load->config('cf_weekly_account') ;

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'page':
            case 'index':
                $this->_list();
            break;
            default:
                show_404();
            break;
        }
    }

    function _list() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $cs_no = $seg->get('cs_no');   // 게시물아이디
        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $seg->replace('cs_no').$param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sfl   = $param->get('sfl');   // 검색필드
        $stx   = $param->get('stx');   // 검색어
        $sca   = $param->get('sca');   // 분류

        $sdt   = $param->get('sdt');        // 날짜검색타입
        $sdt_s   = $param->get('sdt_s');    // 날짜검색타입명
        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜

        // 정렬
        if (!$sst) 
        {
            $sst = 'mbwa_id';
            $sod = 'desc';            
        }
        else
        {
            $sst = preg_match("/^(mbwa_id|wa_end_date|mbwa_payment_amount|mbwa_payment_type|wa_payment_date|mbwa_payment_result)$/i", $sst) ? $sst : FALSE;
        }

        $mb_id = $this->session->userdata('ss_mb_id') ;
        
        $total_count = $this->Member_weekly_account_model->list_count($mb_id, $sfl, $stx) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/account/lists/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->Member_weekly_account_model->list_result($mb_id, $sst, $sod, $sfl, $stx, $config['per_page'], $offset) ;

        $odv_type = $this->config->item('odv_type') ;
        $or_state = $this->config->item('or_state') ;
        $odv_state = $this->config->item('odv_state') ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i ;
        }

        $head = array('title' => '본인실적(PV)내역서');
        $data = array(
            'total_count' => $total_count,  
            'sst' => $sst,
            'sod' => $sod,          
            'sca' => $sca,
            'sfl' => $sfl,
            'stx' => $stx,
            'list' => $list,
            'paging' => $CI->pagination->create_links(),  

            'sdt'           => $sdt,        // 날짜검색타입
            'sdt_s'         => $sdt_s,      // 날짜검색타입명
            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜

            'weekly_acount' => $this->config->item('weekly_acount'),
            //'sum_payment_by_type' => $this->Member_weekly_account_model->get_sum_payment_by_type($mb_id),

            'sort_no' => $param->sort('mbwa_id', 'desc'), 
            'sort_end_date' => $param->sort('wa_end_date', 'desc'), 
            'sort_payment_amount' => $param->sort('mbwa_payment_amount', 'desc'), 
            'sort_payment_type' => $param->sort('mbwa_payment_type', 'desc'), 
            'sort_payment_date' => $param->sort('wa_payment_date', 'desc'), 
            'sort_payment_result' => $param->sort('mbwa_payment_result', 'desc'), 
        );

        widget::run('head', $head);
        $this->load->view('account/self', $data);
        widget::run('tail');
    }
}
?>