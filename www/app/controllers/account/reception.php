<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reception extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('Member_info_model', 'OrderMaster_model'));
        $this->load->helper(array( 'url' ));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }    

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'page':
            case 'index':
                $this->all();
            break;
            case 'sub':
                $this->sub();
            break;    
            case 'excel':
                $this->excel();
            break ;      
            default:
                show_404();
            break;
        }
    }    

    function excel()
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소   
        $seg      =& $this->seg;    
        $type  = $seg->get('excel'); 

        if ( $type == 'all' )
        {
            $this->all_excel() ;
        }
    }

    function all_excel()
    {
        $this->load->library('excel') ;
        
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");
 
        $i = 3 ;        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$i", "NO")
                    ->setCellValue("B$i", "접수처 정보")
                    ->setCellValue("C$i", "매출CV")
                    ->setCellValue("D$i", "매출PV")
                    ->setCellValue("E$i", "접수처 수수료")
                    ->setCellValue("F$i", "단가")
                    ->setCellValue("G$i", "부가세")
                    ->setCellValue("H$i", "결제금액") ;                                  

        $data = $this->all('excel') ;     
        $i++ ;
        foreach ($data['list'] as $key => $row) 
        {            
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A$i", "$row[num]")
                        ->setCellValue("B$i", "$row[USERTEXT]")                        
                        ->setCellValue("C$i", "$row[PV1]")                        
                        ->setCellValue("D$i", "$row[PV2]")
                        ->setCellValue("E$i", "$row[PV3]")
                        ->setCellValue("F$i", "$row[PRICE]")
                        ->setCellValue("G$i", "$row[VAT]")
                        ->setCellValue("H$i", "$row[AMT]") ;
            $i++ ;
        }

        $title = '접수처 매출('.$data['year'].'년 '.$data['month'].'월)' ;        
        $i-- ;

        $objPHPExcel->getActiveSheet()->getStyle('C4:H'.$i)->getNumberFormat()->setFormatCode('#,##0') ;        
        $objPHPExcel->CreateExcelFileWithHBDefaultStyle($title, $i) ;
    }

    function all($in_type='')
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $ord_type  = $seg->get('ord_type', 0); // tab
        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서

        $year   = $param->get('year', date('Y'));   
        $month  = $param->get('month', date('m'));   

        // 정렬
        if (!$sst) 
        {
            $sst = 'PV3';
            $sod = 'desc';            
        }
        else
        {
            $sst = preg_match("/^(A.RECEPTION_ID|PV1|PV2|PV3|PRICE|VAT|AMT)$/i", $sst) ? $sst : FALSE;
        }
        
        $total_count = $this->OrderMaster_model->get_reception_all_list_count($year, $month) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/account/reception/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);        

        if ( $in_type == 'excel' )
        {
            $limit = $total_count ;
            $offset = 0 ;
        }
        else
        {   
            $limit = $config['per_page'] ;
            $offset = ($page - 1) * $config['per_page'];
        }        

        $result = $this->OrderMaster_model->get_reception_all_list($year, $month, $sst, $sod, $limit, $offset) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i ;
        }

        $head = array('title' => '접수처 매출');
        $data = array(
            'total_count' => $total_count,  
            'sst' => $sst,
            'sod' => $sod,          
            'list' => $list,
            'sub_total' => $this->OrderMaster_model->get_reception_cv_pv_sub(),
            'paging' => $CI->pagination->create_links(),  

            'year'    => $year,
            'month'   => $month, 
    
            'sort_no' => $param->sort('RECEPTION_ID', 'desc'), 
            'sort_cv' => $param->sort('PV1', 'desc'), 
            'sort_pv' => $param->sort('PV2', 'desc'), 
            'sort_reception' => $param->sort('PV3', 'desc'), 
            'sort_price' => $param->sort('PRICE', 'desc'), 
            'sort_vat' => $param->sort('VAT', 'desc'), 
            'sort_amt' => $param->sort('AMT', 'desc'), 
            
            'qstr'          => $qstr,
        );

        if ( $in_type == 'excel' )
        {
            return $data ;
        }
        else
        {
            widget::run('head', $head);
            $this->load->view('account/reception_all', $data);
            widget::run('tail');
        }
    }

    function sub()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $ord_type  = $seg->get('ord_type', 0); // tab
        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sca   = $param->get('sca');   // 분류

        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜*/

        // 정렬
        if (!$sst) 
        {
            $sst = 'A.ORDNO';
            $sod = 'desc';            
        }
        else
        {
            $sst = preg_match("/^(A.ORDNO|A.ORD_DATE|A.PV1|A.PV2|A.PRICE|A.VAT|A.ORD_TYPE|A.AMT|RLV)$/i", $sst) ? $sst : FALSE;
        }

        if ( IS_MANAGER )
        {
            $mb_id = 'D0000000' ;
        }
        else
        {
            $mb_id = $this->session->userdata('ss_mb_id') ;
        }
        
        $total_count = $this->OrderMaster_model->get_reception_sub_list_count($mb_id, $ord_type) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/account/reception/sub/ord_type/'.$ord_type.'/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->OrderMaster_model->get_reception_sub_list($mb_id, $sst, $sod, $config['per_page'], $offset, $ord_type) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i ;
        }

        $head = array('title' => '접수처 매출');
        $data = array(
            'total_count' => $total_count,  
            'sst' => $sst,
            'sod' => $sod,          
            'sca' => $sca,
            'list' => $list,
            'sub_total' => $this->OrderMaster_model->get_reception_cv_pv_sub($mb_id),
            'paging' => $CI->pagination->create_links(),  

            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜
    
            'sort_no' => $param->sort('A.ORDNO', 'desc'), 
            'sort_rlv' => $param->sort('RLV', 'desc'), 
            'sort_ord_date' => $param->sort('A.ORD_DATE', 'desc'), 
            'sort_cv' => $param->sort('A.PV1', 'desc'), 
            'sort_pv' => $param->sort('A.PV2', 'desc'), 
            'sort_price' => $param->sort('A.PRICE', 'desc'), 
            'sort_vat' => $param->sort('A.VAT', 'desc'), 
            'sort_ord_type' => $param->sort('A.ORD_TYPE', 'desc'), 
            'sort_amt' => $param->sort('A.AMT', 'desc'), 

            'ord_type_list' => array( 0 => '개통완료', 1 => '주문', 2 => '반품', 3 => '교환', 4 => '취소'),
            'ord_type'      => $ord_type,
            'qstr'          => $qstr,
        );

        widget::run('head', $head);
        $this->load->view('account/reception_sub', $data);
        widget::run('tail');
    }
}
?>