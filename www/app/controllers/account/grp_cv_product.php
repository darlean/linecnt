<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grp_cv_product extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('Pay_model'));
        $this->load->helper(array( 'url' ));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'pay_date':            
            case 'index':
                $this->index();
            break;
            default:
                show_404();
            break;
        }
    }

    function index()
    {       
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소        
        $seg      =& $this->seg;                

        $pay_date_list = $this->Pay_model->get_pay_date_list() ;
        $pay_date  = $seg->get('pay_date', $pay_date_list[0]['PAY_DATE']);        

        $head = array('title' => '일반 상품 N값');
        widget::run('head', $head);
        $data = $this->getData($pay_date) ;
        $data['str_n'] = array('골드', '루비', '사파이어', '다이아몬드', '크라운') ;

        $data['pay_date_list'] = $pay_date_list ;

        $this->load->view('account/grp_cv_product', $data);
                
        widget::run('tail');
    }

    function getData($in_pay_date)
    {
        $pay_calc_info = $this->Pay_model->get_pay_calc_info($in_pay_date) ;

        $fund_all = $pay_calc_info['AMT_WCY_USE_CV'] ;
        $fund_gd = $pay_calc_info['AMT_WCY_USE_CV_GD'] ;
        $fund_rb = $pay_calc_info['AMT_WCY_USE_CV_RB'] ;
        $fund_sp = $pay_calc_info['AMT_WCY_USE_CV_SP'] ;
        $fund_dd = $pay_calc_info['AMT_WCY_USE_CV_DD'] ;
        $fund_cw = $pay_calc_info['AMT_WCY_USE_CV_CW'] ;

        $n_all = $pay_calc_info['AMT_WCY_N'] ;
        $n_gd = $pay_calc_info['AMT_WCY_N_GD'] ;
        $n_rb = $pay_calc_info['AMT_WCY_N_RB'] ;
        $n_sp = $pay_calc_info['AMT_WCY_N_SP'] ;
        $n_dd = $pay_calc_info['AMT_WCY_N_DD'] ;
        $n_cw = $pay_calc_info['AMT_WCY_N_CW'] ;

        $n_total = $n_all + $n_gd + $n_rb + $n_sp + $n_dd + $n_cw ;

        $arr_n = array($n_all, $n_gd, $n_rb, $n_sp, $n_dd, $n_cw) ;
        $arr_fund = array($fund_all, $fund_gd, $fund_rb, $fund_sp, $fund_dd, $fund_cw) ;

        $arr_grp = array($pay_calc_info['AMT_WCY_GRP_CV'], $pay_calc_info['AMT_WCY_GRP_CV_GD'], $pay_calc_info['AMT_WCY_GRP_CV_RB'], $pay_calc_info['AMT_WCY_GRP_CV_SP'], $pay_calc_info['AMT_WCY_GRP_CV_DD'], $pay_calc_info['AMT_WCY_GRP_CV_CW']) ;
        $total_amt_wcy_grp = $pay_calc_info['AMT_WCY_GRP_CV'] + $pay_calc_info['AMT_WCY_GRP_CV_GD'] + $pay_calc_info['AMT_WCY_GRP_CV_RB'] + $pay_calc_info['AMT_WCY_GRP_CV_SP'] + $pay_calc_info['AMT_WCY_GRP_CV_DD'] + $pay_calc_info['AMT_WCY_GRP_CV_CW'] ;

        $data = array(
            'pay_date'      => $in_pay_date,
            'pay_calc_info' => $pay_calc_info,

            'arr_n'         => $arr_n,
            'n_total'       => $n_total,

            'arr_fund'      => $arr_fund,
            'fund_total'    => $fund_all,

            'arr_grp'       => $arr_grp,
            'total_amt_wcy_grp' => $total_amt_wcy_grp,
        );

        return $data ;
    }
}
?>