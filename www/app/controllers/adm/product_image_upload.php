<?php
class Product_image_upload extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model('Product_image_upload_model');

		define('WIDGET_SKIN', 'main');

		//$this->output->enable_profiler(TRUE);
	}

	function lists()
	{
		if( !IS_MANAGER )
			show_404();

		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sfl   = $param->get('sfl');   // 검색필드
        $stx   = $param->get('stx');   // 검색어
        $sca   = $param->get('sca');   // 분류

        $per_page = 2;

        $total_count = $this->Product_image_upload_model->list_count($sfl, $stx) ;

		$config['suffix']      = $qstr;
		$config['base_url']    = '/adm/product_image_upload/lists/page/';
		$config['per_page']    = $per_page;
		$config['total_rows']  = $total_count;
		$config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->Product_image_upload_model->list_result($sst, $sod, $sfl, $stx, $config['per_page'], $offset) ;
        $data = array
        (
        	'lists'	=> $result,
        	'paging' => $CI->pagination->create_links(),  
        );

		$head = array('title' => '제품관리') ;
        widget::run('head', $head ) ;
    	$this->load->view(ADM_F.'/product_image_upload', $data) ;
        widget::run('tail') ;
	}

	public function edit()
    {
		$PDT_CD     = $this->uri->segment(4) ;        
		$IMAGE_PATH = '' ;
        
        if ( count($_FILES) > 0 )
        {      
            $this->load->library('upload') ;

            $config['upload_path'] = "./data/product_image/" ;
            $config['open_file_path'] = "/data/product_image/" ;

            $config['allowed_types'] = 'gif|jpg|png|gif|bmp|pdf';
            $config['max_size'] = '1536' ;
            $config['max_width'] = '3200';
            $config['max_height'] = '3200';
            
            if(!is_dir($config['upload_path']))
            {
                umask( 0 );
                mkdir($config['upload_path'], 0777) ;
            }

            $file_form_name = 'product_image_'.$PDT_CD ;

            if ( $_FILES[$file_form_name]["size"] > 0 && $_FILES[$file_form_name]["name"] != 'none' )
            {
                if ($_FILES[$file_form_name]["error"] <= 0)
                {
                    $ext = array_pop(explode(".", $_FILES[$file_form_name]['name'])) ; // 확장자
                    $config['file_name'] = "product_image_".time().".".$ext ;  

                    $this->upload->initialize($config) ;

                    if($this->upload->do_upload($file_form_name))
                    {
                        $upload_data = array() ;
                        
                        $IMAGE_PATH = $config['open_file_path'].$config['file_name'] ;                        
                    }
                    else
                    {
                        alert($this->upload->display_errors());
                    }
                }
            }
        }

        $this->Product_image_upload_model->insert_product_image( $PDT_CD, $IMAGE_PATH ) ;

       	alert('정상적으로 등록되었습니다.', URL );
    }


}