<?php
class Login extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();
        $this->load->helper('cookie');
        $this->load->model(ADM_F.'/Member_model');
		define('WIDGET_SKIN', 'main');
	}

	function index() 
	{
		$this->qry();
	}

	function qry($msg=FALSE) 
	{
		if (IS_MEMBER)
			goto_url(URL);

		if ($this->input->post('url'))
			$url = $this->input->post('url');
		else
			$url = (is_numeric($msg)) ? URL : urldecode(str_replace('.', '%', $msg));

		$reId = get_cookie('ck_mb_id');

		$head = array('title' => '로그인');
		$data = array(
			'url'      => $url,
			'msg'      => ($msg == 1) ? TRUE : FALSE,
			'reId'     => $reId,
			'chk_reId' => $reId ? 1 : 0
		);
		
		widget::run('head', $head);
		$this->load->view('adm/login', $data);
		widget::run('tail');
	}

	function in() 
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules(array(
			array('field'=>'mb_id', 'label'=>'아이디', 'rules'=>'trim|required|min_length[2]|max_length[20]|alpha_dash|xss_clean'),
			array('field'=>'mb_password', 'label'=>'비밀번호', 'rules'=>'trim|required')
		));

		if ($this->form_validation->run() !== FALSE)
		{
			$mb = $this->Member_model->get_login( $this->input->post('mb_id') );

			if ( !$mb || ($mb['PASSWD'] != $this->input->post('mb_password') ))
				goto_url('adm/login/qry/1');

			if ( $this->input->post('mb_id') == 'INCENT' )
			{
				$this->session->set_userdata('is_card_agency', TRUE );
			}
			else if ( $this->input->post('mb_id') == 'LG' )
            {
                $this->session->set_userdata('only_view_notice', TRUE );
            }
			else if ( $mb['AGENCY_CD'] != '' )
			{
				$this->session->set_userdata('is_agency', TRUE );
				$this->session->set_userdata('AGENCY_CD', $mb['AGENCY_CD']);
			}
			else if ( $mb['CENTER_CD'] > '0000' && $mb['CENTER_CD'] < '1000' )
			{
				$this->session->set_userdata('is_center', TRUE );
				$this->session->set_userdata('CENTER_CD', $mb['CENTER_CD']);
			}
			else if ( $mb['CENTER_CD'] > '1000' && $mb['CENTER_CD'] < '2000' )
			{
				if ( $mb['USERID'] == 'HBN1002' ) // 본사직영지사
				{
					$this->session->set_userdata('is_direct_branch', TRUE );					
				}
				else
				{
					$this->session->set_userdata('is_direct_branch', TRUE );
				}

				$this->session->set_userdata('CENTER_CD', $mb['CENTER_CD']);
			}			
			else if ( $mb['PMS_CD'] >= 3333 )			
			{
				$this->session->set_userdata('is_manager', TRUE );
			}
            else if ( $this->input->post('mb_id') == 'HBNCAST' )
            {
                $this->session->set_userdata('is_hbn_cast', TRUE );
                $this->session->set_userdata('only_view_notice', TRUE );
            }
            else if ( $this->input->post('mb_id') == 'RECYCLE' )
            {
                $this->session->set_userdata('is_recycle', TRUE );
                $this->session->set_userdata('only_view_notice', TRUE );
            }
			else
			{
				goto_url('adm/login/qry/1');
			}	
			
			$this->session->set_userdata('ss_mb_id', $mb['USERID']);
			$this->session->set_userdata('USERNAME', $mb['USERNAME']);
			
			goto_url($this->input->post('url'));
		}
		
		goto_url('/');
	}

	function out() 
	{
		if (IS_MEMBER) 
		{
			$this->session->sess_destroy();
		}
		goto_url('/');
	}
}
?>