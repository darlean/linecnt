<?php
class Product extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('Product_image_upload_model', 'S_Reason_model', 'P_Pdtrecommand_model'));
        $this->load->helper(array( 'url' ));

		define('WIDGET_SKIN', 'main');
        define('CSS_SKIN', 'shop.style');

		//$this->output->enable_profiler(TRUE);
	}

    function _remap($index)
    {        
        if (!IS_MANAGER)
            show_404();

        switch($index)
        {
            case 'page':
            case 'lists':
                $this->lists();
            break;
            case 'recommand':
                $this->recommand();
            break;
            case 'del':
                $this->del();
            break;
            case 'set':
                $this->set();
            break;                        
            default:
                show_404();
            break;
        }
    }

    function lists()
    {
        $data = $this->_data() ;

        $head = array('title' => '상품 정보 등록') ;
        widget::run('head', $head ) ;
        $this->load->view(ADM_F.'/product', $data) ;
        widget::run('tail') ;
    }

    function recommand()
    {
        $data = $this->_data('recommand') ;
        
        $head = array('title' => '추천 상품 관리') ;
        widget::run('head', $head ) ;
        $this->load->view(ADM_F.'/recommand', $data) ;
        widget::run('tail') ;
    }

    function set()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $pdt_cd  = $seg->get('pdt_cd'); 

        $result = $this->P_Pdtrecommand_model->insert($pdt_cd) ;

        if ( $result )
        {
            alert('추천 상품으로 등록되었습니다.', URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }        
    }

    function del()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $pdt_cd  = $seg->get('pdt_cd');    

        $result = $this->P_Pdtrecommand_model->delete($pdt_cd) ;     

        if ( $result )
        {
            alert('추천 상품 등록이 해제되었습니다.', URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }          
    }    

	function _data($in_type='')
	{
		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $vender  = $seg->get('vender'); // 통신사
        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();

        $per_page = 16;

        $vender_list = $this->S_Reason_model->get_vender_list() ;

        if ( $vender == '' )
        {
            $vender = key($vender_list) ;
        }

        $vender_list['2501'] = "일반상품" ;

        $total_count = $this->Product_image_upload_model->list_count($vender) ;

		$config['suffix']      = $qstr;
		$config['base_url']    = '/adm/product/lists/vender/'.$vender.'/page/';
		$config['per_page']    = $per_page;
		$config['total_rows']  = $total_count;
		$config['uri_segment'] = $seg->pos('page');

        if ( $in_type == 'recommand' )
        {
            $config['base_url']    = '/adm/product/recommand/vender/'.$vender.'/page/';
        }

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->Product_image_upload_model->list_result($config['per_page'], $offset, $vender) ;

        foreach ($result as $key => $product) 
        {   
            if ( $product['IMG_PATH'] != '' )
            {
                $IMG_PATH = explode (",", $product['IMG_PATH']);

                $result[$key]['IMG_PATH'] = $IMG_PATH[0] ;
            }            
        }

        $data = array
        (
            'total_count' => $total_count,
        	'lists'	=> $result,
        	'paging' => $CI->pagination->create_links(),  
            'vender' => $vender,
            'vender_list' => $vender_list,
        );

        return $data ;
	}

}