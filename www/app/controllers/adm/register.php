<?php
class Register extends CI_Controller 
{
	function __construct() {
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model(array('Product_image_upload_model', 'P_ptplink_model'));

		define('CSS_SKIN', 'ace_file_input');
	}

    function _remap($index)
    {         
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'index':            
            case 'PDT_CD':
                $this->_modify() ;
            break;            
            case 'update':
                $this->_update() ;
            break;
            case 'link':
                $this->_link() ;
            break;
            case 'detail':
                $this->_detail() ;
            break;
            default:                
                show_404();
            break;
        }
    }

    function _modify() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $PDT_CD  = $seg->get('PDT_CD', 0);

        $product_info = $this->Product_image_upload_model->get_product_info($PDT_CD) ;
        //var_dump($product_info);

        $token = get_token();

        $thumbnails = explode (",", $product_info['IMG_PATH'] );

        $head = array('title' => '상품수정 / 상품삭제');
        $data = array(            
            'PDT_CD'        => $product_info['PDT_CD'],
            'PDT_NAME'      => $product_info['PDT_NAME'],
            'PDT_MODEL'     => $product_info['PDT_MODEL'],            
            'IMG_PATH'      => $product_info['IMG_PATH'],
            'DETAIL_IMG_PATH' => $product_info['DETAIL_IMG_PATH'],
            'THUMBNAILS'    => $thumbnails,
            'policy_list'   => $this->P_ptplink_model->get_ptp_list($PDT_CD),
            'btn_contents'  => '이미지 등록',
            'token'         => $token,
        );

        widget::run('head', $head);
        $this->load->view('adm/register', $data);
        widget::run('tail');
    }

	function _update()
	{
		check_token('adm/register') ;

		$this->load->helper('chkstr');	

    	$rst_data = $this->upload_files() ;

        if ( $rst_data['result'] == FALSE )
        {
           alert($rst_data['error']) ;
        }

        $this->Product_image_upload_model->insert_product_image($this->input->post('PDT_CD'), $rst_data['IMG_PATH'] ) ;

        alert_parent_reload('상품정보가 수정되었습니다.') ;
	}

    private function _link()
    {
        check_token('adm/register') ;

        $arr_ptp_id = $this->input->post('PTP_ID') ;        
        $arr_price_kind = $this->input->post('PRICE_KIND') ;
        $arr_reg_kind = $this->input->post('REG_KIND') ;

        $arr_link_path = $this->input->post('LINK_PATH') ;
        $arr_or_link_path = $this->input->post('OR_LINK_PATH') ;

        foreach ($arr_ptp_id as $key => $value) 
        {
            if ( ($arr_or_link_path[$key] != $arr_link_path[$key]) || ( $arr_link_path[$key] != '') )
            {
                $this->P_ptplink_model->update($arr_link_path[$key], $this->input->post('PDT_CD'), $arr_price_kind[$key], $arr_reg_kind[$key] ) ;   
            }
        }        

        alert_parent_reload('상품정보가 수정되었습니다.') ;
    }

    private function _detail()
    {
        check_token('adm/register') ;

        $this->load->helper('chkstr');  

        $rst_data = $this->upload_files(3) ;

        if ( $rst_data['result'] == FALSE )
        {
           alert($rst_data['error']) ;
        }

        $this->Product_image_upload_model->insert_detail_image($this->input->post('PDT_CD'), $rst_data['IMG_PATH'] ) ;

        alert_parent_reload('상품정보가 수정되었습니다.') ;
    }    

	public function upload_files($base_n = 0)
    {
        $IMG_PATH = explode (",", $this->input->post('IMG_PATH'));

        $rst_data['result'] = TRUE ;
        $rst_data['IMG_PATH'] = '' ;

        if ( count($_FILES) > 0 )
        {      
            $this->load->library('upload') ;

            $config['upload_path'] = "./data/product_image/" ;
            $config['open_file_path'] = "/data/product_image/" ;

            $config['allowed_types'] = 'gif|jpg|png|gif|bmp|pdf';
            //$config['max_size'] = '1536' ;
            $config['max_width'] = '3200';
            //$config['max_height'] = '3200';
            
            if(!is_dir($config['upload_path']))
            {
                umask( 0 );
                mkdir($config['upload_path'], 0777) ;
            }

            for ( $i = 1 ; $i <= count($_FILES) ; $i++ ) 
            {
                $file_form_name = 'file'.($base_n+$i) ;

                if ( $_FILES[$file_form_name]["size"] > 0 && $_FILES[$file_form_name]["name"] != 'none' )
                {
                    if ($_FILES[$file_form_name]["error"] <= 0)
                    {
                        $ext = array_pop(explode(".", $_FILES[$file_form_name]['name'])) ; // 확장자
                        $config['file_name'] = "product_img_".time()."_".$i.".".$ext ;  

                        $this->upload->initialize($config) ;

                        if($this->upload->do_upload($file_form_name))
                        {
                            $rst_data['IMG_PATH'] .= $config['open_file_path'].$config['file_name'] ;    

                            if ( (count($_FILES) > 1) )
                            {
                                $rst_data['IMG_PATH'] .= ',' ;
                            }           
                        }
                        else
                        {
                            $rst_data['result'] = FALSE ;
                            $rst_data['error'] = $this->upload->display_errors() ;

                            return $rst_data ;
                        }
                    }
                }
                else
                {
                    $idx = $i-1 ;

                    if ( isset($IMG_PATH) && isset($IMG_PATH[$idx]) && $IMG_PATH[$idx] != '' && $IMG_PATH[$idx] != ',' )
                    {
                        $rst_data['IMG_PATH'] .= $IMG_PATH[$idx] ;

                        if ( (count($_FILES) > 1) )
                        {
                            $rst_data['IMG_PATH'] .= ',' ;
                        }
                    }
                }
            }
        }

        return $rst_data ;
    }

}
?>