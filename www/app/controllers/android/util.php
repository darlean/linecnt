<?php
class Util extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('S_Reason_model', 'HB_card_place_model', 'Member_info_model', 'Member_forget_model', 'S_Bank_model'));
	}

    function get_vender_list()
    {
        $vender_list = $this->S_Reason_model->get_vender_list2() ;
        $data = array('result' => $vender_list);

        echo json_encode($data);        
    }

    function get_benefit_list()
    {
        $benefit_list = array( "해당없음", "장애우/국가유공자", "기초생활수급자", "차상위계층" ) ;
        $data = array('result' => $benefit_list);

        echo json_encode($data);        
    }

    function get_card_place_list()
    {
        $card_place_list = $this->HB_card_place_model->get_card_place_list() ;
        echo json_encode($card_place_list) ;        
    }

    function get_member_info()
    {        
        $supporter_id = $this->input->post('supporter_id') ;
        $recommender_id = $this->input->post('recommender_id') ;    

        $find_supporter = $this->input->post('find_supporter') ;

        if ( $supporter_id != '' )
        {
            if ( $find_supporter == 'true' )
            {
                $member = $this->Member_info_model->get_member_info_by_id($supporter_id) ;

                if ( $member )
                {
                    $fillable_supporter = $this->Member_info_model->fillable_supporter($supporter_id) ;

                    if ( !$fillable_supporter )
                    {
                        echo 'CANT' ;
                        return FALSE ;
                    }

                    echo json_encode($member) ;
                    return TRUE ;
                }                
            }
            else
            {
                if ( $recommender_id != '' )
                {
                    $member = $this->Member_info_model->get_member_info_by_id($recommender_id) ;

                    if ( $member )
                    {
                        $fillable_recommender = $this->Member_info_model->fillable_recommender($recommender_id, $supporter_id) ;

                        if ( $fillable_recommender == 0 )
                        {
                            echo 'CANT' ;
                            return FALSE ;
                        }

                        echo json_encode($member) ;
                        return TRUE ;
                    }
                }
            }
        }    

        echo 'NULL' ;
        return FALSE ;
    }

    function confrim_bank_account()
    {
        $posts = Array( // 포스트 
            //##################################################
            //###### ▣ 회원사 ID 설정   - 계약시에 발급된 회원사 ID를 설정하십시오. ▣
            //###### ▣ 회원사 PW 설정   - 계약시에 발급된 회원사 PASSWORD를 설정하십시오. ▣
            //###### ▣ 조회사유  설정   - 10:회원가입 20:기존회원가입 30:성인인증 40:비회원확인 90:기타사유 ▣
            //###### ▣ 개인/사업자 설정 - 1:개인 2:사업자 ▣
            //##################################################
            "niceUid"=>"Nhbn00",                        // 나이스평가정보에서 고객사에 부여한 구분 id
            "svcPwd"=>"mFaOqWUv",                       // 나이스평가정보에서 고객사에 부여한 서비스 이용 패스워드
            "inq_rsn"=>"10",                            // 조회사유 - 10:회원가입 20:기존회원가입 30:성인인증 40:비회원확인 90:기타사유
            "strGbn"=>"1",                              // 1 : 개인, 2: 사업자
            //##################################################
            //###### 위의 값을 알맞게 수정해 주세요.
            //##################################################
            "strResId"=>$_POST["jumin"],                  // 주민등록번호
            "strNm"=>$_POST["name"],                      // 이름
            "strBankCode"=>$_POST["bank"],                // 은행코드
            "strAccountNo"=>$_POST["account"],            // 계좌번호
            "service"=>"1",                             // 서비스구분
            "svcGbn"=>"5",                              // 업무구분
            "svc_cls"=>"",                              // 내/외국인 구분
            "strOrderNo"=>date("Ymd") . rand(1000000000,9999999999),            //주문번호 : 매 요청마다 중복되지 않도록 유의(수정불필요)
        );

        $cookies = array();     // 쿠키
        $referer = "";              // 리퍼러

        $host = "secure.nuguya.com";                                                                                                                        // 호스트 
        //$page = "https://secure.nuguya.com/nuguya/service/realname/sprealnameactconfirm.do";      // URL
        $page = "https://secure.nuguya.com/nuguya2/service/realname/sprealnameactconfirm.do";       // UTF-8 URL

        $retval = $this->sock_post($host,$page,$posts,$cookies,$referer); 

        if( $retval == false) 
        {
            echo "E999" ;
            return FALSE ;
        }
        else
        {
            // 결과값 처리
            $NewInfo = explode("\r\n", $retval);            // 헤더정보에서 뉴라인 체크
            $infoValue = explode("|",$NewInfo[8]);      // 9번째 줄의 결과값만 가져온다.
        
            $OrderNum   = $infoValue[0];        // 주문번호
            $ResultCD   = $infoValue[1];        // 결과코드
            $Msg        = $infoValue[2];        // 메세지

            if( $ResultCD == '0000' )
            {
                echo $ResultCD ;
            }
            else
            {                            
                //echo iconv("UTF-8", "EUC-KR", urldecode($infoValue[2])); 
                echo $infoValue[2]; 
            }
            
            return TRUE ;                    
        }

        echo "Error" ;
        return FALSE ;
    }

    ################################################################################################
    // sock_post() 함수 시작
    ################################################################################################
    function sock_post($host,$target,$posts,$cookies,$referer='',$port=443) 
    { 
        //var_dump($posts);
        $postValues = '';
        if(is_array($posts)) 
        { 
            foreach($posts AS $name=>$value) 
            {
                $postValues .= urlencode($name) . "=" . urlencode($value) . '&'; 
            }

            //var_dump($postValues);
            $postValues = substr($postValues, 0, -1); 
        } 

        $postLength = strlen($postValues); 

        $cookieValues = '';
        if(is_array($cookies)) 
        { 
            foreach($cookies AS $name=>$value) 
            {
                $cookieValues .= urlencode($name) . "=" . urlencode($value) . ';'; 
            }
            $cookieValues = substr($cookieValues, 0, -1); 
        } 

        $request  = "POST $target HTTP/1.1\r\n"; 
        $request .= "Host: $host\r\n"; 
        $request .= "Content-Type: application/x-www-form-urlencoded\r\n"; 
        $request .= "Content-Length: " . $postLength . "\r\n"; 
        $request .= "Connection: close\r\n"; 
        $request .= "\r\n"; 
        $request .= $postValues; 

        $ret = ''; 
        $socket  = fsockopen("ssl://".$host, $port, $errno, $errstr, 10); // 소켓 타임아웃 10초
        
        if ( ! $socket )
        {           
            return false;
        }
        
        fputs($socket, $request); 
            while(!feof($socket)) $ret .= fgets($socket, 1024); 
        fclose($socket);         

        return $ret; 
    } 
    //################################################################################################

    function find_address()
    {
        $search = $this->input->post('addr') ;               // json은 UTF-8만 지원합니다. (결과값도 UTF-8로 리턴)
        $url  = "http://post.phpschool.com/json.phps.kr";
        $data = array("addr"=>$search, "ipkey"=>"1676267", "type"=>"new");
        // ipkey는 인증메일에서 안내합니다.
        // 구주소검색일경우 array("addr"=>$search, "ipkey"=>"XXX",  "type"=>"old");  
        // 지번주소로 도로명주소를 찾기위한 검색 "type"=>"newdong"  /* "가산동 371-50" 식으로 동/번지입력 */
        // 지번주소로 찾기의 경우  http://post.phpschool.com/post.html 예를 참조바랍니다.
        // 구주소검색일경우(구 우편번호로만 찾기) "type"=>"old"  
        

        $output = ($this->HTTP_Post($url, $data));
        $output = substr($output, strpos($output,"\r\n\r\n")+4);

        $json = json_decode($output);

        if ($json->result > 0) {

            /*echo "검색건수 : {$json->result}\n";
            echo "검색시간 : {$json->time}\n";
            echo "조회횟수 : {$json->cnt}\n";
            echo "조회한도 : {$json->maxcnt}\n";*/

            $list = array() ;
            foreach ($json->post as $key=>$value) {
                     //$value->postnew;             // 새우편번호 (5자리)
                     //$value->post;                // 우편번호   (6자리)
                     //$value->addr_1;              // 시/도
                     //$value->addr_2;              // 구
                     //$value->addr_3;              // 도로명
                     //$value->addr_4;              // 동/건물
                     //$value->addr_5;              // 구주소 (도로명주소1:1매칭) // 도로명주소검색일경우만 리턴
                     //$value->addr_eng;            // 영문주소 // 도로명주소검색일경우만 리턴

                     //print_r($value);
                    
                    $list[$key]["postnew"] = $value->postnew ;
                    $list[$key]["addr"] = $value->addr_1." ".$value->addr_2." ".$value->addr_3." ".$value->addr_4 ;

                    if ( $list[$key]["addr"] == '' )
                        $list[$key]["addr"] = $value->addr_5 ;
                    //echo json_encode($value) ;
                    //echo json_encode($list) ;
            }
            echo json_encode($list) ;
            //echo json_encode($json->post) ;
        } else if ($json->result == 0) {
            echo "검색결과가 없습니다.";
        } else if ($json->result == -1) {
            echo "검색결과가 너무 많습니다. 입력하신 검색어 $search 뒤에 단어를 추가해서 검색해보세요.";
        } else if ($json->result < 0) {
            echo "검색실패 : ".$json->message;
        }

        // $result  "-1"  일경우 :  너무많은검색결과 1000건이상
        // $result  "-2"  일경우 :  서버 IP 미인증
        // $result  "-3"  일경우 :  조회횟수초과
        // $result  "-4"  일경우 :  미인증 사용자        
    }

    // 실제 구현 소스는 위에까지입니다. 아래는 소켓함수(curl로 구현가능)
    function HTTP_Post($URL,$data) 
    {
        $str = "" ;
        $URL_Info=parse_url($URL);
        if(!empty($data)) foreach($data AS $k => $v) $str .= urlencode($k).'='.urlencode($v).'&';
        $path = $URL_Info["path"];
        $host = $URL_Info["host"];
        //$port = $URL_Info["port"];
        if (empty($port)) $port=80;
        $result = "";
        $fp = fsockopen($host, $port, $errno, $errstr, 30);
        $http  = "POST $path HTTP/1.0\r\n";
        $http .= "Host: $host\r\n";
        $http .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $http .= "Content-length: " . strlen($str) . "\r\n";
        $http .= "Connection: close\r\n\r\n";
        $http .= $str . "\r\n\r\n";
        fwrite($fp, $http);
        while (!feof($fp)) { $result .= fgets($fp, 4096); }
        fclose($fp);
        return $result;
    }

    function join_member()  
    {
        $username = $_POST['username'] ;
        $password = $_POST['password'] ;

        echo $username ;
    }

    function find_id()
    {
        $USERNAME = $_POST['USERNAME'] ;
        $BIRTH = $_POST['BIRTH'] ;

        $mb = $this->Member_forget_model->find_id( $USERNAME, $BIRTH ) ;

        if ($mb==FALSE) 
        {
            echo "NULL" ;
            return ;
        }   

        echo $mb->LOGIN_ID ;
    }

    function find_pw()
    {
        $USERID = $_POST['USERID'] ;
        $EMAIL = $_POST['EMAIL'] ;
                
        if( $this->Member_forget_model->check( $USERID, $EMAIL ) == FALSE )
        {
            echo "NULL" ;
            return ;
        }

        echo "CHANGED" ;//$this->Member_forget_model->new_pwd( $login_id, $e_mail );
    }

    function get_bank_list()
    {
        $bank_list = $this->S_Bank_model->get_bank_list() ;

        echo json_encode($bank_list);   
    }

    function test()
    {
        echo "TEST";
    }
}
?>