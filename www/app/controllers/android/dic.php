<?php
class Dic extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();		
	}

    function index()
    {
        widget::run('head', $head);
        $this->load->view('android/dic');
        widget::run('tail');
    }
}
?>