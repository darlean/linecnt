<?php
class icon extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();		
	}

    function index()
    {
        $head = array('title' => 'icon');
        widget::run('head', $head);
        $this->load->view('android/icon');
        widget::run('tail');
    }
}
?>