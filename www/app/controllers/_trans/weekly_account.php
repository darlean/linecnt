<?php
class Weekly_account extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model( array('Weekly_account_model', 'Member_weekly_gold_upper_bonus'));


		//$this->output->enable_profiler(TRUE);
	}

	function update() 
	{
		$start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $this->Weekly_account_model->account( $start_date, $end_date );
		//goto_url('member/memo/lists/'.$flag);
	}

	function update_direct() 
	{
        $this->Member_weekly_gold_upper_bonus->update( 20 );

		//goto_url('member/memo/lists/'.$flag);
	}
}
?>