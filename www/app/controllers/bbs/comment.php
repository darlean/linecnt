<?php
class Comment extends CI_Controller 
{
	function __construct() {
		parent::__construct();
		$this->load->model(array( 'Board_model', 'Board_comment_model' ) );	
		$this->load->helper(array('board', 'textual'));

		 $this->output->enable_profiler(TRUE);
		
	}

	function _remap($index)
    {       
        switch($index)
        {
        	case 'index':
            case 'page':            
                $this->_comment();
            break;
            case 'delete':
            	$this->_delete();
            break;
            default:
                show_404();
            break;
        }
    }

	function _comment()
	{
		$this->load->library('segment', array('offset'=>3), 'seg'); 		// 세그먼트 주소
		$this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

		$page     = $this->seg->get('page', 1); 			// 페이지	
			
		$qstr     = $this->input->post('qstr', TRUE );
		
		$BBS_ID   = $this->input->post('BBS_ID', TRUE );
		$BBS_NO   = $this->input->post('BBS_NO', TRUE );
		
		$BBS_MEMO = $this->input->post('BBS_MEMO', TRUE );		

		if( !$this->Board_model->invalid_table( $BBS_ID ) )
			alert('잘못된 접근입니다.');
		
		$this->Board_comment_model->insert( $BBS_ID, $BBS_NO, $BBS_MEMO, $this->session->userdata('ss_mb_id') );
		
		goto_url('bbs/view/page/'.$page.$qstr);
	}

	function _delete()
	{
		$this->load->library('segment', array('offset'=>3), 'seg'); 		// 세그먼트 주소
		$this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

		$page     = $this->seg->get('page', 1); 			// 페이지	
			
		$qstr     = $this->input->post('qstr', TRUE );
		
		$BBS_ID   = $this->input->post('BBS_ID', TRUE );
		$BBS_NO   = $this->input->post('BBS_NO', TRUE );
		
		$BBS_SEQ = $this->input->post('BBS_SEQ', TRUE );		

		if( !$this->Board_model->invalid_table( $BBS_ID ) )
			alert('잘못된 접근입니다.');
		
		$this->Board_comment_model->delete( $BBS_ID, $BBS_NO, $BBS_SEQ );

		goto_url('bbs/view/page/'.$page.$qstr);
	}
}

?>