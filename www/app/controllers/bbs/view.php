<?php
class View extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();
		$this->load->model('Board_model');	

		define('WIDGET_SKIN', 'home');

		//$this->output->enable_profiler(TRUE);
	}

	function _remap($index)
    {       
        switch($index)
        {
        	case 'index':
            case 'page':            
                $this->_view();
            break;
            default:
                show_404();
            break;
        }
    }

	function _view()
	{
		$this->load->library('segment', array('offset'=>3), 'seg'); 		// 세그먼트 주소
		$this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

		$page     = $this->seg->get('page', 1); 			// 페이지	
		$sfl      = $this->param->get('sfl');   			// 검색필드
		$stx      = $this->param->get('stx');   			// 검색어
		$BBS_ID   = $this->param->get('BBS_ID', FALSE ); 	// 테이블 이름
		$BBS_NO   = $this->param->get('BBS_NO', FALSE ); 	// 테이블 이름
		$qstr  	  = $this->param->output();

		if( $BBS_ID == FALSE || $BBS_NO == FALSE )
			alert( '잘못된 접근입니다.' );

		$result = $this->Board_model->get_row( $BBS_ID, $BBS_NO );
		if( !$result )
			alert( '잘못된 접근입니다.' );

		//var_dump($result['FILE']);
		$result['BBS_CONTEXT'] = str_replace( "/upload_image/", 'http://hbnetwork2.cafe24.com/upload_image/', $result['BBS_CONTEXT']);

		$data = array
		(
			'subject'  => $result['BBS_TITLE'],
			'name'     => $result['WORK_USER'],
			'datetime' => $result['WORK_DATE'],
			'content'  => $result['BBS_CONTEXT'],
			'comment'  => $result['COMMENT'],
			'file'     => $result['FILE'],
			'userid'   => $this->session->userdata('ss_mb_id'),
			
			'qstr'     => $qstr,
			'sfl'      => $sfl,
			'stx'      => $stx,
			'page'     => $page,
			
			'BBS_ID'   => $BBS_ID,
			'BBS_NO'   => $BBS_NO,
		);

		//var_dump($result['BBS_CONTEXT']);


		$head = array('title' => '글 보기');
		widget::run('head', $head);
        $this->load->view('board/hbsolution/view', $data);
        widget::run('tail');
	}
}
?>