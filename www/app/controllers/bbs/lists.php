<?php
class Lists extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();
		$this->load->model('Board_model');	

		define('WIDGET_SKIN', 'home');

		//$this->output->enable_profiler(TRUE);
	}

	function _remap($index)
    {       
        switch($index)
        {
        	case 'index':
            case 'page':            
                $this->_list();
            break;
            default:
                show_404();
            break;
        }
    }

	function _list()
	{
		$this->load->library('segment', array('offset'=>3), 'seg'); 		// 세그먼트 주소
		$this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

		$page     = $this->seg->get('page', 1); 			// 페이지	
		$sfl      = $this->param->get('sfl');   			// 검색필드
		$stx      = $this->param->get('stx');   			// 검색어
		$BBS_ID   = $this->param->get('BBS_ID', FALSE ); 	// 테이블 이름
		$qstr  	  = $this->param->output();
		$per_page = 10;										// 페이지당 노출 글수

		if( $BBS_ID == FALSE )
			alert( '잘못된 접근입니다.' );

		$result = $this->Board_model->get_lists( $BBS_ID, $page, $per_page, $sfl, $stx );		
		
		$config['suffix']	   = $qstr;
		$config['base_url']    = RT_PATH.'/bbs/lists/page/';
		$config['per_page']    = $per_page;
		$config['total_rows']  = $result['total_count'];
		$config['uri_segment'] = $this->seg->pos('page');
		// 검색 파트 ADD
		$config['full_tag_open']  = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';

		$this->load->library('pagination', $config);

		$data = array
		(
			'BBS_ID'  	  => $BBS_ID,
			'list'        => $result['list'],
			'total_count' => $result['total_count'],
			'sfl'         => $sfl,
			'stx'         => $stx,
			'paging'      => $this->pagination->create_links(),  
		);

		$head = array('title' => '게시판');
		widget::run('head', $head);
        $this->load->view('board/hbsolution/list', $data);
        widget::run('tail');
	}
}
?>