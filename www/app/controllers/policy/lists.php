<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lists extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

		$this->load->library('form_validation');
		$this->load->model(array('Policy_model', 'Product_model'));
        $this->load->helper(array( 'url' ));

        define('WIDGET_SKIN', 'main');
        define('CSS_SKIN', 'editor,shop.style');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
    	if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'page':
            case 'pt_id':
                $this->_list();
            break;
            default:
                show_404();
            break;
        }
    }

    function _list() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $pt_id  = $seg->get('pt_id', 0); 

        $cs_no = $seg->get('cs_no');   // 게시물아이디
        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $seg->replace('cs_no').$param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sfl   = $param->get('sfl');   // 검색필드
        $stx   = $param->get('stx');   // 검색어
        $sca   = $param->get('sca');   // 분류

        // 정렬
        if (!$sst) 
        {
            $sst = 'pc_id';
            $sod = 'desc';            
        }
        else
        {
            $sst = preg_match("/^(pc_id|pc_commission_value|pc_promotion_value|pc_start_date|pc_end_date|pc_price|pc_instalment_plan|pc_membership_cost|pc_usim_cost|pc_agree_plan)$/i", $sst) ? $sst : FALSE;
        }
        
        $total_count = $this->Policy_model->list_count($pt_id, $sfl, $stx) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/product/lists/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->Policy_model->list_result($pt_id, $sst, $sod, $sfl, $stx, $config['per_page'], $offset) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = new stdClass();
            $list[$i]->num                  = $total_count - ($page - 1) * $config['per_page'] - $i;
            $list[$i]->href                 = '/policy/register/pc_id/'.$row['pc_id'];
            $list[$i]->pc_id                = $row['pc_id'] ;
            $list[$i]->cv                 	= $row['pc_commission_value'] ;
            $list[$i]->pv                	= $row['pc_promotion_value'] ;
            $list[$i]->start_date          	= $row['pc_start_date'] ;
            $list[$i]->end_date             = $row['pc_end_date'];
            $list[$i]->price           		= number_format($row['pc_price']). ' 원' ;
            $list[$i]->instalment_plan      = $row['pc_instalment_plan']. ' 개월' ;
            $list[$i]->join_cost         	= $row['pc_join_cost'] ;
            $list[$i]->usim_cost            = $row['pc_usim_cost'] ;
            $list[$i]->agree_plan           = $row['pc_agree_plan']. ' 개월' ;
            $list[$i]->calling_plan         = ($row['cp_name'] != '') ? $row['cp_name'].' / '.number_format($row['cp_price']).' 원' : '' ;
        }

        // product_info
        {
            $product_info = $this->Product_model->get_product_info_with_policy_info($pt_id) ;

            $product_info['pc_agree_plan'] .= ($product_info['pc_agree_plan'] != '') ? ' 개월' : '' ;
            $product_info['pc_price'] = ($product_info['pc_price'] != '') ? number_format($product_info['pc_price']).' 원' : '' ;
            $product_info['pc_instalment_plan'] .= ($product_info['pc_instalment_plan'] != '') ? ' 개월' : '' ;
            $product_info['calling_plan'] = ($product_info['cp_name'] != '') ? $product_info['cp_name'].' / '.number_format($product_info['cp_price']).' 원' : '' ;
        }

        $head = array('title' => '판매정책관리');
        $data = array(
            'total_count' => $total_count,  
            'sst' => $sst,
            'sod' => $sod,          
            'sca' => $sca,
            'sfl' => $sfl,
            'stx' => $stx,
            'list' => $list,
            'pt_id' => $pt_id,
            'product_info' => $product_info,
            'thumbnails' => explode (",", $product_info['pt_thumbnail']),
            'paging' => $CI->pagination->create_links(),  

            'sort_no' => $param->sort('pc_id', 'desc'), 
            'sort_rp' => $param->sort('pc_commission_value', 'desc'), 
            'sort_exp' => $param->sort('pc_promotion_value', 'desc'), 
            'sort_start_date' => $param->sort('pc_start_date', 'desc'), 
            'sort_end_date' => $param->sort('pc_end_date', 'desc'), 
            'sort_price' => $param->sort('pc_price', 'desc'), 
            'sort_instalment' => $param->sort('pc_instalment_plan', 'desc'), 
            'sort_join_cost' => $param->sort('pc_join_cost', 'desc'), 
            'sort_usim_cost' => $param->sort('pc_usim_cost', 'desc'), 
            'sort_agree_plan' => $param->sort('pc_agree_plan', 'desc'), 
        );

        widget::run('head', $head);
        $this->load->view('policy/lists', $data);
        widget::run('tail');
    }
}
?>