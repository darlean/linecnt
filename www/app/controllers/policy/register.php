<?php
class Register extends CI_Controller {
	function __construct() {
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model(array('Policy_model', 'Product_model', 'Calling_plan_model'));

		define('CSS_SKIN', 'editor,shop.style');
	}

    function _remap($index)
    {         
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'pt_id':
                define('WIDGET_SKIN', 'main');
                $this->_form();
            break;
            case 'update':
                $this->_update() ;
            break;
            default:
                define('WIDGET_SKIN', 'main');
                show_404();
            break;
        }
    }

	function _form() 
	{
		$token = get_token();

        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $pt_id  = $seg->get('pt_id', 0); 
        $pt_id  = ( $pt_id == 0 ) ? $this->input->post('pt_id') : $pt_id ;

        if ( $pt_id == 0 )
        {
            alert('잘못된 정보입니다.', '/policy/lists');
            return false ;
        }

        $product_info = $this->Product_model->get_product_info($pt_id) ;
        $product_info['pt_thumbnail'] = explode (",", $product_info['pt_thumbnail']);
        $product_info['pt_colors'] = explode (",", $product_info['pt_color']);

		$head = array('title' => '정책등록');
		$data = array(
            'pt_id' => $pt_id,
            'pc_id' => 0,
            'product_info'           => $product_info,
            'cp_tag'                 => $this->Calling_plan_model->get_tag(),
            'pc_commission_value'    => $this->input->post('pt_commission_value'),
			'pc_promotion_value'     => $this->input->post('pc_promotion_value'),
			'pc_start_date'          => $this->input->post('pc_start_date'),
			'pc_end_date'            => $this->input->post('pc_end_date'),
			'pc_price'               => $this->input->post('pc_price'),
            'pc_instalment_plan'     => $this->input->post('pc_instalment_plan'),
            'pc_join_cost'           => $this->input->post('pc_join_cost'),
            'pc_usim_cost'           => $this->input->post('pc_usim_cost'),
            'pc_agree_plan'          => $this->input->post('pc_agree_plan'),
            'pc_calling_plan'        => $this->input->post('pc_calling_plan'),
            'btn_contents'           => '정책등록',
			'token'                  => $token,
		);
        
        widget::run('head', $head);
		$this->load->view('policy/register', $data);
		widget::run('tail');
	}

	function _update()
	{
		//check_token('policy/register/pt_id/'.$pt_id) ;

		$this->load->helper('chkstr');	
		$config = array(
			array('field'=>'pc_commission_value', 'label'=>'cv', 'rules'=>'trim|required|numeric'),
			array('field'=>'pc_promotion_value', 'label'=>'pv', 'rules'=>'trim|required|numeric'),
			array('field'=>'pc_start_date', 'label'=>'정책 시작일', 'rules'=>'trim|required'),
			array('field'=>'pc_end_date', 'label'=>'정책 종료일', 'rules'=>'trim|required'),
            array('field'=>'pc_price', 'label'=>'출고가', 'rules'=>'trim|required|numeric'),
            array('field'=>'pc_instalment_plan', 'label'=>'할부개월수', 'rules'=>'trim|required|numeric'),
            array('field'=>'pc_join_cost', 'label'=>'가입비', 'rules'=>'trim|required'),
            array('field'=>'pc_usim_cost', 'label'=>'유심비', 'rules'=>'trim|required'),
            array('field'=>'pc_agree_plan', 'label'=>'약정기간', 'rules'=>'trim|required|numeric'),
            array('field'=>'pc_calling_plan', 'label'=>'요금제', 'rules'=>'trim|required'),
		);

		$this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) 
        {
            $this->_form();
        }
        else
        {
            $pt_id = $this->input->post('pt_id') ;

            $sql = array(
                'pt_id'                  => $pt_id,
                'pc_commission_value'    => $this->input->post('pc_commission_value'),
                'pc_promotion_value'     => $this->input->post('pc_promotion_value'),
                'pc_start_date'          => $this->input->post('pc_start_date'),
                'pc_end_date'            => $this->input->post('pc_end_date'),
                'pc_price'               => $this->input->post('pc_price'),
                'pc_instalment_plan'     => $this->input->post('pc_instalment_plan'),
                'pc_join_cost'           => $this->input->post('pc_join_cost'),
                'pc_usim_cost'           => $this->input->post('pc_usim_cost'),
                'pc_agree_plan'          => $this->input->post('pc_agree_plan'),
                'pc_calling_plan'        => $this->input->post('pc_calling_plan'),
            );

            $pc_id = $this->input->post('pc_id') ;

            if ( $pc_id == 0 )
            {
           	    $this->Policy_model->insert($sql) ;
                alert('정책이 등록되었습니다.', 'policy/lists/pt_id/'.$pt_id) ;
            }
            else
            {
                //$this->Policy_model->update($pc_id, $sql) ;
            }
        }
	}
}
?>