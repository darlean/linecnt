<?php
class Kt_wired extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('S_Reason_model', 'HB_calling_plan_model', 'HB_card_place_model', 'HB_card2_model', 'HB_card_detail2_model'));

        $this->load->library('user_agent');

        if (!$this->agent->is_mobile())
        {
            define('WIDGET_SKIN', 'main');
        }

		define('CSS_SKIN', 'pages/page_pricing');

		//$this->output->enable_profiler(TRUE);
	}

	function index() 
	{
        if (!$this->agent->is_mobile() && !IS_MEMBER)
        {
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');
        }

        $head = array('title' => 'KT 멀티룸팩 요금계산기') ;

		widget::run('head', $head);
		$this->load->view('consult/kt_wired');
		widget::run('tail');
	}

	function get_wired_result_price()
    {
        $result_price = array() ;

        $select_type = $this->input->post('select_type') ;
        $tv_count = $this->input->post('tv_count') ;

        $line_count = ceil($tv_count / 5) ;

        $internet_set_price = $line_count * 22000 ;
        $tv_set_price = $tv_count * 11000 ;

        $result_price['total_price'] = number_format($internet_set_price + $tv_set_price) ;
        $result_price['internet_set_price'] = number_format($internet_set_price) ;
        $result_price['tv_set_price'] = number_format($tv_set_price) ;

        if ( $select_type == '0' ) // 개인사업자
        {
            $result_price['internet_gift'] = number_format($line_count * 60000) ;
        }
        else
        {
            $result_price['internet_gift'] = number_format($line_count * 40000) ;
        }

        //$result_price['cash_gift'] = number_format(($tv_count * 70000) - ($line_count * 60000)) ;
        //$result_price['hb_price'] = number_format($tv_count * 10000) ;

        $result_price['hb_or_cash_price'] = number_format( $tv_count * 110000 ) ;

        $result_price['div_result_price'] = $this->_get_wired_result_price() ;

        echo json_encode($result_price) ;
    }

	function _get_wired_result_price()
    {
        $div_wired_result_price_info = '' ;

        $select_type = $this->input->post('select_type') ;
        $tv_count = $this->input->post('tv_count') ;
        $calling_plan = $this->input->post('calling_plan') ;

        $internet_basic_price = 0 ;
        $internet_discount_price = 0 ;
        $internet_direct = 0 ;

        $arr_tv_basic_price = array() ;
        $arr_tv_basic_price[10] = 15000 ;
        $arr_tv_basic_price[12] = 18000 ;
        $arr_tv_basic_price[15] = 23000 ;

        $arr_olleh_set_home = array() ;
        $arr_olleh_set_home[10] = 10000 ;
        $arr_olleh_set_home[12] = 12000 ;
        $arr_olleh_set_home[15] = 15000 ;

        if ( $tv_count != "" ) {
            $internet_basic_price = 36000 ;
            $internet_discount_price = 10500 ;
            $internet_direct = 20000 ;
        }

        $line_count = ceil($tv_count / 5) ;

        $internet_price = $internet_basic_price - $internet_discount_price ;
        $internet_price_vat = $internet_direct * 1.1 ;
        $total_internet_price = $internet_price_vat * $line_count ;

        $div_wired_result_price_info .= '        
            <tr>
                <td class="text-center"><strong>인터넷</strong></td>
                <td class="text-center">'.number_format($internet_basic_price).'</td>
                <td class="text-center color-red">'.number_format($internet_discount_price).'</td>
                <td class="text-center">'.number_format($internet_price).'</td>
                <td class="text-center">'.number_format($internet_direct).'</td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center">0</td>
                <td class="text-center">'.number_format($internet_price_vat).'</td>
                <td class="text-center">'.number_format($line_count).'</td>
                <td class="text-center">'.number_format($total_internet_price).'</td>
            </tr>' ;

        $tv_basic_price = $arr_tv_basic_price[$calling_plan] ;
        $tv_discount_price = $tv_basic_price * 0.2 ;
        $tv_price = $tv_basic_price - $tv_discount_price ;
        $stb_price = 2000 ;
        $tv_direct = $arr_olleh_set_home[$calling_plan] ;

        if ( $tv_direct == 0 )
        {
            $tv_price_vat = ($tv_price + $stb_price) * 1.1 ;
        }
        else
        {
            $tv_price_vat = ($tv_direct + $stb_price) * 1.1 ;
        }

        $total_tv_price = $tv_price_vat * $line_count ;

        $div_wired_result_price_info .= '        
            <tr>
                <td class="text-center"><strong>TV기본</strong></td>
                <td class="text-center">'.number_format($tv_basic_price).'</td>
                <td class="text-center color-red">'.number_format($tv_discount_price).'</td>
                <td class="text-center">'.number_format($tv_price).'</td>
                <td class="text-center">'.number_format($tv_direct).'</td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center">'.number_format($stb_price).'</td>
                <td class="text-center">'.number_format($tv_price_vat).'</td>
                <td class="text-center">'.number_format($line_count).'</td>
                <td class="text-center">'.number_format($total_tv_price).'</td>
            </tr>' ;

        $all_discount = $tv_price * 0.5 ;
        $public_discount = 0 ;
        $tv_add_count = $tv_count - $line_count ;

        // tv 30회선 이상
        if ( $line_count >= 30 )
        {
            $public_discount = $all_discount * 0.1 ;
        }

        $tv_add_price_vat = ($all_discount + $public_discount + 1000 ) * 1.1 ;
        $total_tv_add_price = $tv_add_price_vat * $tv_add_count ;

        $div_wired_result_price_info .= '        
            <tr>
                <td class="text-center"><strong>TV추가</strong></td>
                <td class="text-center">'.number_format($tv_basic_price).'</td>
                <td class="text-center color-red">'.number_format($tv_discount_price).'</td>
                <td class="text-center">'.number_format($tv_price).'</td>
                <td class="text-center"></td>
                <td class="text-center">'.number_format($all_discount).'</td>
                <td class="text-center">'.number_format($public_discount).'</td>
                <td class="text-center">1000</td>
                <td class="text-center">'.number_format($tv_add_price_vat).'</td>
                <td class="text-center">'.number_format($tv_add_count).'</td>
                <td class="text-center">'.number_format($total_tv_add_price).'</td>
            </tr>' ;

        $total_price = $total_internet_price + $total_tv_price + $total_tv_add_price ;

        $div_wired_result_price_info .= '        
            <tr>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"><strong>월 요금 합계</strong></td>
                <td class="text-center"><strong>'.number_format($total_price).'</strong></td>
            </tr>' ;

        $div_wired_result_price_info .= '        
            <tr>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"><strong>룸 당월 요금</strong></td>
                <td class="text-center"><strong>'.number_format($total_price / $tv_count).'</strong></td>
            </tr>' ;

        return  $div_wired_result_price_info ;
    }
}
?>