<?php
class Place extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('S_Reason_model', 'HB_card_place_model'));

		define('WIDGET_SKIN', 'main');
		$this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

		//$this->output->enable_profiler(TRUE);
	}

	function index()
	{
		$head = array('title' => '사용처 관리') ;
        $data = array(        
        	'card_place_list' => $this->HB_card_place_model->get_card_place_list(),
        ) ;

		widget::run('head', $head);
		$this->load->view('consult/place', $data);
		widget::run('tail');
	}

	function add()
	{
		$card_place = $this->param->get('card_place') ;		

        $result = $this->HB_card_place_model->insert($card_place) ;

        if ( $result )
        {        
            alert('등록되었습니다.', URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
	}

	function del()
	{
		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $cdp_id  = $seg->get('cdp_id');    

    	$result = $this->HB_card_place_model->del($cdp_id) ;    	

    	if ( $result )
    	{
    		alert('사용처가 삭제되었습니다.', 'consult/place') ;
    	}  		
	}

	function edit()
	{
		$cdp_id = $this->param->get('cdp_id') ;		
		$card_place = $this->param->get('card_place') ;		

        $result = $this->HB_card_place_model->update($cdp_id, $card_place) ;

        if ( $result )
        {        
            alert('수정되었습니다.', URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
	}
}
?>