<?php
class Wireless extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('S_Reason_model', 'HB_calling_plan_model', 'HB_card_place_model', 'HB_card2_model', 'HB_card_detail2_model'));

        $this->load->library('user_agent');

        if (!$this->agent->is_mobile())
        {
            define('WIDGET_SKIN', 'main');
        }

		define('CSS_SKIN', 'pages/page_pricing');

		//$this->output->enable_profiler(TRUE);
	}

	function index() 
	{
        if (!$this->agent->is_mobile() && !IS_MEMBER)
        {
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');
        }

        $select_combi = array('미결합', '무선할인', '유선할인') ;        
        $select_benefit = array('해당없음', '장애우/국가유공자', '기초생활수급자', '차상위계층') ;

        $card_place_list = $this->HB_card_place_model->get_card_place_list() ;

        $ids = '' ;

        foreach ($card_place_list as $key => $value)
        {
            $ids .= '#'.$value['CDP_ID'] ;

            if ( $key < (count($card_place_list) - 1) )
            {
                $ids .= "," ;
            }
        } 

        $vender_list = $this->S_Reason_model->get_vender_list(false, true) ;

        $head = array('title' => '무선 컨설팅') ;
        $data = array(
        	'vender_list' => $vender_list,        
            'select_combi' => $select_combi,            
            'select_benefit' => $select_benefit,
            'card_place_list' 	=> $card_place_list,    
            'ids' => $ids,
        ) ;

		widget::run('head', $head);
		$this->load->view('consult/wireless', $data);
		widget::run('tail');
	}

	function test()
    {
       // if (!IS_MEMBER)
        //    alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        $select_combi = array('미결합', '무선할인', '유선할인') ;
        $select_benefit = array('해당없음', '장애우/국가유공자', '기초생활수급자', '차상위계층') ;

        $card_place_list = $this->HB_card_place_model->get_card_place_list() ;

        $ids = '' ;

        foreach ($card_place_list as $key => $value)
        {
            $ids .= '#'.$value['CDP_ID'] ;

            if ( $key < (count($card_place_list) - 1) )
            {
                $ids .= "," ;
            }
        }

        $vender_list = $this->S_Reason_model->get_vender_list(false, true) ;

        $head = array('title' => '무선 컨설팅') ;
        $data = array(
            'vender_list' => $vender_list,
            'select_combi' => $select_combi,
            'select_benefit' => $select_benefit,
            'card_place_list' 	=> $card_place_list,
            'ids' => $ids,
        ) ;

        widget::run('head', $head);
        $this->load->view('consult/wireless_test', $data);
        widget::run('tail');
    }

	function get_recommand_calling_plan()
    {
        $vender = $this->input->post('vender') ;
        $call_using = $this->input->post('call_using') ;
        $sms_using = $this->input->post('sms_using') ;
        $data_using = $this->input->post('data_using') ;                  
        $benefit = $this->input->post('benefit') ; 

        $calling_info = array() ;

        if ( $vender != '' )
        {
            $result = $this->HB_calling_plan_model->get_cp_info_list_by_vender($vender) ;

            foreach ($result as $idx => $row) 
            {
                $recommand_calling_info = array() ;

                $plus_call_price = ( $call_using - $row['FREE_CALL'] ) * $row['PLUS_CALL_PRICE'] ; 
                $plus_sms_price = ( $sms_using - $row['FREE_SMS'] ) * $row['PLUS_SMS_PRICE'] ; 
                $plus_data_price = ( $data_using - $row['FREE_DATA'] ) * $row['PLUS_DATA_PRICE'] ; 

                $plus_call_price = ($plus_call_price > 0) ? $plus_call_price : 0 ;
                $plus_sms_price = ($plus_sms_price > 0) ? $plus_sms_price : 0 ;
                $plus_data_price = ($plus_data_price > 0) ? $plus_data_price : 0 ;                      

                $monthly_price = $row['CP_PRICE'] - $row['CP_DISCOUNT'] ;
                $monthly_price += ( $plus_call_price + $plus_sms_price + $plus_data_price ) ;

                $benefit_price = 0 ;
                if ( $benefit == '장애우/국가유공자' )
                {
                    $benefit_price = ($row['CP_PRICE'] - $row['CP_DISCOUNT'])*0.35 ;    
                }
                else if ( $benefit == '차상위계층' )
                {
                    $benefit_price = $row['CP_PRICE'] * 0.35 ;

                    if ( $benefit_price > 15000 )
                    {
                        $benefit_price = 15000 ;
                    }                                     
                }     
                else if ( $benefit == '기초생활수급자' )
                {
                    $benefit_price = 15000 + $plus_call_price/2 ;
                } 

                $monthly_price -= $benefit_price ;                
                $monthly_price *= 1.1 ;                

                $recommand_calling_info['calling_plan'] = $row['CP_NAME'] ;
                $recommand_calling_info['calling_plan_id'] = $row['CP_ID'] ;
                $recommand_calling_info['monthly_price'] = $monthly_price ;

                if ( !isset($calling_info['monthly_price']) || $calling_info['monthly_price'] > $monthly_price )
                {
                    $calling_info = $recommand_calling_info ;
                }                    
            }                
        }

        echo json_encode($calling_info) ;
        return TRUE ;  
    }

    function get_card_plan_list ()
    {
    	$vender = $this->input->post('vender') ;
        $sum_price = (int)$this->input->post('sum_price') ;
        $arr_using_price = $this->input->post('arr_using_price') ;
        $mobile_amt = $this->input->post('mobile_amt') ;
        $discount_price = $this->input->post('discount_price') ;     

        $bJson = $this->input->post('bJson') ;

        if ( $bJson )                  
        {
            $arr_using_price = json_decode($arr_using_price) ;
        }        

        $arr_card_info_list = array() ;

        if ( $vender != '' )
        {
    		$card_list = $this->HB_card2_model->get_card_list_by_vender($vender) ;
    		$month_list = array(24, 30, 36) ;

    		foreach ($card_list as $key => $card) 
    		{
    			$card_detail_info = $this->HB_card_detail2_model->get_card_detail_info($card['PDT_CD']) ;    			

                $arr_card_info_list[$card['PDT_CD']]['CD_ID'] = $card['PDT_CD'] ;
    			$arr_card_info_list[$card['PDT_CD']]['CD_NAME'] = $card['PDT_NAME'] ;
                $arr_card_info_list[$card['PDT_CD']]['CD_COMPANY'] = $card['CD_COMPANY'] ;
    			$arr_card_info_list[$card['PDT_CD']]['IMG_PATH'] = $card['IMG_PATH'] ;

                if ( $card['CARD_TYPE'] == '0')
                {
                    $arr_card_info_list[$card['PDT_CD']]['CD_TYPE'] = "세이브" ; 
                }     
                else if ( $card['CARD_TYPE'] == '1')
                {
                    $arr_card_info_list[$card['PDT_CD']]['CD_TYPE'] = "후청구" ;
                }
                else
                {
                    $arr_card_info_list[$card['PDT_CD']]['CD_TYPE'] = "체크카드" ;
                }  

    			{ // 월 적립 포인트 / 할인금액
	    			$arr_card_info_list[$card['PDT_CD']]['sum_point'] = 0 ;
	    			$total_price = 0 ;

                    foreach ($arr_using_price as $cdp_id => $price) 
                    {
                        // 실적 금액 계산
                        if ( isset($card_detail_info[$cdp_id]) && $card_detail_info[$cdp_id]['IS_ACCEPT_RESULT'] == 'O' )
                        {
                            $total_price += $price ;
                        }
                    }

	    			foreach ($arr_using_price as $cdp_id => $price) 
	    			{
	    				if ( isset($card_detail_info[$cdp_id]) )
	    				{
                            $last_month_result = $card_detail_info[$cdp_id]['LAST_MONTH_RESULT'] ;

	    					// 적립률 계산
                            if ( $total_price >= $last_month_result || $last_month_result == '' )
                            {
                                $card_place_result = $card_detail_info[$cdp_id]['CARD_PLACE_RESULT'] ;
                                
                                // 사용처에서 사용처 실적 이상 사용시에만 포인트 적립
                                if ( $price >= $card_place_result || $card_place_result == '' ) 
                                {
                                    $point = ($card_detail_info[$cdp_id]['SAVE_RATE'] * $price) / 100;

                                    if ($point > $card_detail_info[$cdp_id]['SAVE_LIMIT']) // 적립 한도보다 포인트가 큰가?
                                    {
                                        if ($card_detail_info[$cdp_id]['SAVE_LIMIT'] != 0 && $card_detail_info[$cdp_id]['SAVE_LIMIT'] != "") {
                                            $point = $card_detail_info[$cdp_id]['SAVE_LIMIT'];
                                        }
                                    }

                                    $arr_card_info_list[$card['PDT_CD']]['sum_point'] += $point;
                                }
			    			}
		    			}		    			
	    			}

	    			// 실적 인정 적용(추가 적립 포인트)
	    			for ($i = 1 ; $i < 4 ; $i++ )
	    			{		    			
		    			if ( $total_price >= $card['SAVE_POINT_TERMS'.$i.'_OVER'] && $total_price < $card['SAVE_POINT_TERMS'.$i.'_UNDER'] )
		    			{
		    				$arr_card_info_list[$card['PDT_CD']]['sum_point'] += $card['SAVE_POINT_TERMS'.$i.'_POINT'] ;
		    			}
		    		}

		    		// 통합 할인 한도 적용
		    		if ( $card['IS_USE_TOTAL_DISCOUNT_LIMIT'] == 'O' ) {
                        for ($i = 1; $i < 5; $i++) {
                            if ($total_price >= $card['TOTAL_DISCOUNT_LIMIT' . $i . '_OVER'] && $total_price < $card['TOTAL_DISCOUNT_LIMIT' . $i . '_UNDER']) {
                                if ( $arr_card_info_list[$card['PDT_CD']]['sum_point'] > $card['TOTAL_DISCOUNT_LIMIT'.$i.'_PRICE'] )
                                    $arr_card_info_list[$card['PDT_CD']]['sum_point'] = $card['TOTAL_DISCOUNT_LIMIT'.$i.'_PRICE'] ;
                            }
                        }
                    }
	    		} 

	    		// 세이브 추천 금액
    			foreach ($month_list as $key => $month) 
    			{    				
                    if ( $card['IS_SAVE_CARD'] == 'O' )
                    {
        				$tmp_save_price = $arr_card_info_list[$card['PDT_CD']]['sum_point'] * $month ;

                        if ( $tmp_save_price > $card['SAVE_LIMIT'] )
                        {
                            $tmp_save_price = $card['SAVE_LIMIT'] ;
                        }

    	    			if ( ($mobile_amt - $discount_price) > $tmp_save_price )
    	    			{
    	    				$arr_card_info_list[$card['PDT_CD']]['SAVE_TYPE'][$month] = $tmp_save_price ;
    	    			}
    	    			else
    	    			{
    	    				$arr_card_info_list[$card['PDT_CD']]['SAVE_TYPE'][$month] = ($mobile_amt - $discount_price) ;
    	    			}
                    }
                    else
                    {
                        $arr_card_info_list[$card['PDT_CD']]['SAVE_TYPE'][$month] = 0 ;         
                    }
    			}    		    

    			// 후청구
                if ( $card['IS_SAVE_CARD'] == 'X' )
                {
    			    $arr_card_info_list[$card['PDT_CD']]['AFTER_TYPE'] = $arr_card_info_list[$card['PDT_CD']]['sum_point'] ;    				    	
                }
                else
                {
                    $arr_card_info_list[$card['PDT_CD']]['AFTER_TYPE'] = 0 ;   
                }

                if ( $bJson )
                {
                    $card_info = $this->HB_card2_model->get_card_info($card['PDT_CD']) ;
                    $arr_card_info_list[$card['PDT_CD']] = array_merge($arr_card_info_list[$card['PDT_CD']], $card_info) ;
                }
    		}          		
    	}

        if ( $bJson )  
        {
            $tmp_arr_card_info_list = array() ;

            foreach ($arr_card_info_list as $key => $value) 
            {
                array_push($tmp_arr_card_info_list, $value) ;
            }

            $arr_card_info_list = $tmp_arr_card_info_list ;
        }

        echo json_encode($arr_card_info_list) ;
        return TRUE ;      	
    }

    function get_monthly_price()
    {
        $monthly_price = 0 ;

        $cp_id = $this->input->post('cp_id') ;

        if ( $cp_id != '' )
        {
            $call_using = $this->input->post('call_using') ;
            $sms_using = $this->input->post('sms_using') ;
            $data_using = $this->input->post('data_using') ;
            $s_benefit = $this->input->post('s_benefit') ;

            $mobile_amt = $this->input->post('mobile_amt') ;
            $discount_price = $this->input->post('discount_price') ;
            $card_save_discount = $this->input->post('card_save_discount') ;
            $card_save_discount = str_replace(',', '', $card_save_discount) ;
            $card_after_discount = $this->input->post('card_after_discount') ;
            $card_after_discount = str_replace(',', '', $card_after_discount) ;
            $instalment_period =  $this->input->post('instalment_period') ;
            $selected_agreement = $this->input->post('selected_agreement') ;

            $cp_info = $this->HB_calling_plan_model->get_cp_info($cp_id) ;

            //var_dump($cp_info) ;

            $plus_call_price = ( $call_using - $cp_info['FREE_CALL'] ) * $cp_info['PLUS_CALL_PRICE'] ; 
            $plus_sms_price = ( $sms_using - $cp_info['FREE_SMS'] ) * $cp_info['PLUS_SMS_PRICE'] ; 
            $plus_data_price = ( $data_using - $cp_info['FREE_DATA'] ) * $cp_info['PLUS_DATA_PRICE'] ; 

            $plus_call_price = ($plus_call_price > 0) ? $plus_call_price : 0 ;
            $plus_sms_price = ($plus_sms_price > 0) ? $plus_sms_price : 0 ;
            $plus_data_price = ($plus_data_price > 0) ? $plus_data_price : 0 ;     

            /*echo 'plus_call_price : '.$plus_call_price.'<BR>' ; 
            echo 'plus_sms_price : '.$plus_sms_price.'<BR>' ;
            echo 'plus_data_price : '.$plus_data_price.'<BR>' ;*/

            $benefit = 0 ;
            if ( $s_benefit == '장애우/국가유공자' )
            {
                $benefit = ($cp_info['CP_PRICE'] - $cp_info['CP_DISCOUNT'])*0.35 ;    
            }
            else if ( $s_benefit == '차상위계층' )
            {
                $benefit = $cp_info['CP_PRICE'] * 0.35 ;

                if ( $benefit > 15000 )
                {
                    $benefit = 15000 ;
                }                                     
            }     
            else if ( $s_benefit == '기초생활수급자' )
            {
                $benefit = 15000 + ($plus_call_price / 2) ;
            } 

            $monthly_price = 0 ;

            // 요금 
            $call_price = $cp_info['CP_PRICE'] - $cp_info['CP_DISCOUNT'] ;

            if ( $selected_agreement == '사용' )
            {
                $call_price *= 0.8 ;
            }

            $call_price += $plus_call_price + $plus_sms_price + $plus_data_price - $benefit ;
            $call_price *= 1.1 ;

            //$instalment_price = 1.0027 * (($mobile_amt - $discount_price) / $instalment_period) ;
            $instalment_price = (($mobile_amt - $discount_price) / $instalment_period) + (0.0027 * ($mobile_amt - $discount_price)) ;

            //if ( $card_discount == '사용' )
            {
                if ( $card_save_discount > 0 )
                {
                    $instalment_price -= 1.0027 * ($card_save_discount / $instalment_period) ;
                }

                $monthly_price -= $card_after_discount ;
            }
                          
            $monthly_price += $call_price + $instalment_price ;

            /*echo 'benefit : '.$benefit.'<BR>' ;
            echo 'call_price : '.$call_price.'<BR>' ;
            echo 'instalment_price : '.$instalment_price.'<BR>' ;
            echo 'monthly_price : '.$monthly_price.'<BR>' ;*/
        }

        echo $monthly_price ;
        return TRUE ;       
    }

    function get_card_info()
    {
        $div_card_info = '' ;

        $pdt_cd = $this->input->post('cd_id') ;        

        if ( $pdt_cd != '' )
        {
            $vender_list = $this->S_Reason_model->get_vender_list(false, true) ;
            $card_info = $this->HB_card2_model->get_card_info($pdt_cd) ;

            $card_info['SAVE_LIMIT'] = ( $card_info['SAVE_LIMIT'] >= 9999999 ) ? '본인 할부 한도 내' : number_format($card_info['SAVE_LIMIT']) ;

            $div_card_info = "<dt><strong>통신사 </strong></dt>
                            <dd>".$vender_list[$card_info['TEL_COMPANY']]."</dd>
                            <hr>
                            <dt><strong>카드 타입 </strong></dt>" ;

            if ( $card_info['IS_SAVE_CARD'] == 'O')
            {
                $div_card_info .= "<dd>세이브</dd>" ; 
            }     
            else
            {
                $div_card_info .= "<dd>후청구</dd>" ; 
            }  

            $div_card_info .= "<hr>
                            <dt><strong>세이브 한도 </strong></dt>
                            <dd>".$card_info['SAVE_LIMIT']."</dd>
                            <hr>
                            <dt><strong>세이브 수수료 </strong></dt>
                            <dd>".$card_info['SAVE_COMMISSION']."</dd>
                            <hr>
                            <dt><strong>추가 적립 포인트 </strong></dt>" ;

            for ( $i = 1 ; $i < 4 ; $i++ )
            {
                if ( $card_info['SAVE_POINT_TERMS'.$i.'_OVER'] > 0 )
                {
                    $div_card_info .= "<dd>".number_format($card_info['SAVE_POINT_TERMS'.$i.'_OVER'])." 이상 " ;
                    $div_card_info .= number_format($card_info['SAVE_POINT_TERMS'.$i.'_UNDER'])." 미만 사용시 " ;
                    $div_card_info .= number_format($card_info['SAVE_POINT_TERMS'.$i.'_POINT'])." 적립" ;
                }
            }
                            
            $div_card_info .= "
                            <dd></dd><hr>    
                            <dt><strong>연회비</strong></dt>
                            <dd>".$card_info['ANNUAL_FEE']."</dd>
                            <hr>
                            <dt><strong>가입 축하 포인트</strong></dt>
                            <dd>".$card_info['JOIN_POINT']."</dd>
                            <hr>
                            <dt><strong>추가 혜택</strong></dt>
                            <dd>".nl2br($card_info['ADD_BENEFIT'])."</dd>
                            <hr>
                            <dt><strong>기타 내용</strong></dt>
                            <dd>".nl2br($card_info['ETC_MSG'])."</dd>" ;
        }

        echo $div_card_info ;
        return TRUE ;
    }

    function get_calling_plan_info()
    {
        $div_calling_plan_info = '' ;

        $cp_id = $this->input->post('cp_id') ;

        if ( $cp_id != '' )
        {           
            $vender_list = $this->S_Reason_model->get_vender_list(false, true) ;
            $cp_info = $this->HB_calling_plan_model->get_cp_info($cp_id) ;

            $cp_info['FREE_CALL'] = ( $cp_info['FREE_CALL'] > 9999 ) ? '무제한' : $cp_info['FREE_CALL'] ;
            $cp_info['FREE_DATA'] = ( $cp_info['FREE_DATA'] > 9999 ) ? '무제한' : $cp_info['FREE_DATA'] ;
            $cp_info['FREE_SMS'] = ( $cp_info['FREE_SMS'] > 9999 ) ? '무제한' : $cp_info['FREE_SMS'] ;

            $div_calling_plan_info = "<dt><strong>기본료 </strong></dt>
                                    <dd>".number_format($cp_info['CP_PRICE'])."</dd>
                                    <hr>      
                                    <dt><strong>요금할인</strong></dt>
                                    <dd>".number_format($cp_info['CP_DISCOUNT'])."</dd>
                                    <hr>                      
                                    <dt><strong>기본 제공 통화량(분)</strong></dt>
                                    <dd>".$cp_info['FREE_CALL']."</dd>
                                    <hr>
                                    <dt><strong>기본 제공 데이터(GB)</strong></dt>
                                    <dd>".(float)$cp_info['FREE_DATA']."</dd>
                                    <hr>
                                    <dt><strong>기본 제공 문자(건)</strong></dt>
                                    <dd>".$cp_info['FREE_SMS']."</dd>
                                    <hr>
                                    <dt><strong>기타 내용</strong></dt>
                                    <dd>".nl2br($cp_info['ETC_MSG'])."</dd>" ;
        }

        echo $div_calling_plan_info ;
        return TRUE ;
    }

    function get_card_detail_info()
    {
        $div_card_detail_info = '' ;

        $pdt_cd = $this->input->post('cd_id') ;
        $bJson = $this->input->post('bJson') ;

        if ( $pdt_cd != '' )
        {
            $card_place_list = $this->HB_card_place_model->get_card_place_list() ;            
            $card_detail_info = $this->HB_card_detail2_model->get_card_detail_info($pdt_cd) ;            

            foreach ($card_place_list as $key => $row)
            {            
                $CDD_ID = isset($card_detail_info[$row['CDP_ID']]) ? $card_detail_info[$row['CDP_ID']]['CDD_ID'] : '' ; 
                $SAVE_RATE = isset($card_detail_info[$row['CDP_ID']]) ? $card_detail_info[$row['CDP_ID']]['SAVE_RATE'] : '' ; 
                $SAVE_LIMIT = isset($card_detail_info[$row['CDP_ID']]) ? $card_detail_info[$row['CDP_ID']]['SAVE_LIMIT'] : '' ;        
                $LAST_MONTH_RESULT = isset($card_detail_info[$row['CDP_ID']]) ? $card_detail_info[$row['CDP_ID']]['LAST_MONTH_RESULT'] : '' ;
                $CARD_PLACE_RESULT = isset($card_detail_info[$row['CDP_ID']]) ? $card_detail_info[$row['CDP_ID']]['CARD_PLACE_RESULT'] : '' ;
                $ETC = isset($card_detail_info[$row['CDP_ID']]) ? $card_detail_info[$row['CDP_ID']]['ETC'] : '' ;                 
                
                if ( $bJson )  
                {                           
                    $card_place_list[$key]['SAVE_RATE'] = (float)$SAVE_RATE ;
                    $card_place_list[$key]['SAVE_LIMIT'] = $SAVE_LIMIT ;
                    $card_place_list[$key]['ETC'] = $ETC ;
                }
                else
                {
                    $div_card_detail_info .= '        
                        <tr>
                            <td class="text-center">'.$row['CDP_NAME'].'</td>
                            <td class="text-center">'.(float)$SAVE_RATE.'</td>
                            <td class="text-center">'.(( $SAVE_LIMIT == 0 || $SAVE_LIMIT == '' ) ? '제한없음' : $SAVE_LIMIT).'</td>
                            <td class="text-center">'.(( $LAST_MONTH_RESULT == 0 || $LAST_MONTH_RESULT == '' ) ? '제한없음' : $LAST_MONTH_RESULT).'</td>
                            <td class="text-center">'.(( $CARD_PLACE_RESULT == 0 || $CARD_PLACE_RESULT == '' ) ? '제한없음' : $CARD_PLACE_RESULT).'</td>
                            <td class="text-center">'.$ETC.'</td>
                        </tr>' ;
                }                   
            }

            if ( $bJson )                  
            {                
                $div_card_detail_info = json_encode($card_place_list) ;
            }   
        }                     

        echo $div_card_detail_info ;
        return TRUE ;
    }
}
?>