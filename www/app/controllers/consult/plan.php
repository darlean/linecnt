<?php
class Plan extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('S_Reason_model', 'HB_calling_plan_model'));

		define('WIDGET_SKIN', 'main');
		define('CSS_SKIN', 'pages/page_pricing');

		//$this->output->enable_profiler(TRUE);
	}

	function index() 
	{
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

		$menu_title = array('본인', '가족1', '가족2', '가족3', '가족4') ;
        $select_combi = array('미결합', '무선할인', '유선할인') ;
        $select_card_combi = array('결합 원함', '결합 원치않음') ;
        $select_benefit = array('해당없음', '장애우/국가유공자', '기초생활수급자', '차상위계층') ;

        $head = array('title' => '가계통신비 진단') ;
        $data = array(
        	'vender_list' => $this->S_Reason_model->get_vender_list(false, true),
        	'menu_title' => $menu_title,
            'select_combi' => $select_combi,
            'select_card_combi' => $select_card_combi,
            'select_benefit' => $select_benefit,
        ) ;

		widget::run('head', $head);
		$this->load->view('consult/calling_plan', $data);
		widget::run('tail');
	}

	function get_calling_plan()
	{
		$vender_cd = $this->input->post('vender_cd') ;

		if ( $vender_cd != '' )
		{
			$calling_plan_list = $this->HB_calling_plan_model->get_calling_plan_list($vender_cd) ;

			if ( $calling_plan_list )
			{
				echo json_encode($calling_plan_list) ;
        		return TRUE ;
			}
		}

        echo 'NULL' ;
    	return FALSE ;		
	}

    function get_calling_plan_list()
    {
        $arr_cp_id = $this->input->post('arr_cp_id') ;
        $arr_combi = $this->input->post('arr_combi') ;
        $arr_call_using = $this->input->post('arr_call_using') ;
        $arr_sms_using = $this->input->post('arr_sms_using') ;                  
        $arr_data_using = $this->input->post('arr_data_using') ;
        $arr_benefit = $this->input->post('arr_benefit') ;
        $arr_card_discount = $this->input->post('arr_card_discount') ;                  
        $arr_monthly_price = $this->input->post('arr_monthly_price') ;                  

        $arr_calling_info = array() ;

        foreach ($arr_cp_id as $key => $value) 
        {
            if ( $value != '' )
            {
                $row = $this->HB_calling_plan_model->get_cp_info($value) ;

                $calling_info = array() ;

                $plus_call_price = ( $arr_call_using[$key] - $row['FREE_CALL'] ) * $row['PLUS_CALL_PRICE'] ; 
                $plus_sms_price = ( $arr_sms_using[$key] - $row['FREE_SMS'] ) * $row['PLUS_SMS_PRICE'] ; 
                $plus_data_price = ( $arr_data_using[$key] - $row['FREE_DATA'] ) * $row['PLUS_DATA_PRICE'] ; 

                $plus_call_price = ($plus_call_price > 0) ? $plus_call_price : 0 ;
                $plus_sms_price = ($plus_sms_price > 0) ? $plus_sms_price : 0 ;
                $plus_data_price = ($plus_data_price > 0) ? $plus_data_price : 0 ;                    

                $monthly_price = $row['CP_PRICE'] ;
                $monthly_price -= $row['CP_DISCOUNT'] ;

                if ( $arr_combi[$key] != '미결합' )
                {
                    $monthly_price -= $row['NET_COMBI_DISCOUNT'] ;
                    $calling_info['before_combi_discount_'.$key] = $row['NET_COMBI_DISCOUNT'] ;
                }

                $monthly_price += $plus_call_price ;
                $monthly_price += $plus_sms_price ;
                $monthly_price += $plus_data_price ;

                $benefit = 0 ;
                if ( $arr_benefit[$key] == '장애우/국가유공자' )
                {
                    $benefit = ($row['CP_PRICE'] - $row['CP_DISCOUNT'])*0.35 ;    
                }
                else if ( $arr_benefit[$key] == '차상위계층' )
                {
                    $benefit = $row['CP_PRICE'] * 0.35 ;

                    if ( $benefit > 15000 )
                    {
                        $benefit = 15000 ;
                    }                                     
                }     
                else if ( $arr_benefit[$key] == '기초생활수급자' )
                {
                    $benefit = 15000 + $plus_call_price/2 ;
                } 

                $monthly_price -= $benefit ;
                $monthly_price -= $arr_card_discount[$key] ;
                $monthly_price *= 1.1 ;

                $etc_price = $arr_monthly_price[$key] - $monthly_price ;
                $etc_price = ( $etc_price > 0 ) ? $etc_price : '' ;            

                $monthly_price +=  $etc_price ;

                $calling_info['before_calling_plan_'.$key] = $row['CP_NAME'] ;
                $calling_info['before_basic_price_'.$key] = $row['CP_PRICE'] ;
                $calling_info['before_price_discount_'.$key] = $row['CP_DISCOUNT'] ;                
                $calling_info['before_benefit_discount_'.$key] = $benefit ;
                $calling_info['before_plus_call_'.$key] = $plus_call_price ;
                $calling_info['before_plus_sms_'.$key] = $plus_sms_price ;
                $calling_info['before_plus_data_'.$key] = $plus_data_price ;
                $calling_info['before_card_discount_'.$key] = $arr_card_discount[$key] ;
                $calling_info['before_etc_price_'.$key] = $etc_price ;
                $calling_info['before_monthly_price_'.$key] = $monthly_price ;
                        
                $arr_calling_info[$key] = $calling_info ;                
            }
        }
        
        echo json_encode($arr_calling_info) ;
        return TRUE ;  
    }

    function get_recommand_calling_plan_list()
    {
        $arr_recommand_vender = $this->input->post('arr_recommand_vender') ;
        $arr_combi = $this->input->post('arr_combi') ;
        $arr_call_using = $this->input->post('arr_call_using') ;
        $arr_sms_using = $this->input->post('arr_sms_using') ;                  
        $arr_data_using = $this->input->post('arr_data_using') ;
        $arr_benefit = $this->input->post('arr_benefit') ;
        $arr_card_discount = $this->input->post('arr_card_discount') ;                  
        $arr_card_using = $this->input->post('arr_card_using') ;
        $arr_combi_card = $this->input->post('arr_combi_card') ;      
        $arr_etc_price = $this->input->post('arr_etc_price') ;    

        $arr_etc_price = str_replace(',', '', $arr_etc_price) ; 

        $arr_calling_info = array() ;

        foreach ($arr_recommand_vender as $key => $value) 
        {
            if ( $value != '' )
            {
                $result = $this->HB_calling_plan_model->get_cp_info_list_by_vender($value) ;

                foreach ($result as $idx => $row) 
                {
                    $recommand_calling_info = array() ;

                    $plus_call_price = ( $arr_call_using[$key] - $row['FREE_CALL'] ) * $row['PLUS_CALL_PRICE'] ; 
                    $plus_sms_price = ( $arr_sms_using[$key] - $row['FREE_SMS'] ) * $row['PLUS_SMS_PRICE'] ; 
                    $plus_data_price = ( $arr_data_using[$key] - $row['FREE_DATA'] ) * $row['PLUS_DATA_PRICE'] ; 

                    $plus_call_price = ($plus_call_price > 0) ? $plus_call_price : 0 ;
                    $plus_sms_price = ($plus_sms_price > 0) ? $plus_sms_price : 0 ;
                    $plus_data_price = ($plus_data_price > 0) ? $plus_data_price : 0 ;                      

                    $monthly_price = $row['CP_PRICE'] ;
                    $monthly_price -= $row['CP_DISCOUNT'] ;

                    if ( $arr_combi[$key] != '미결합' )
                    {
                        $monthly_price -= $row['NET_COMBI_DISCOUNT'] ;
                        $recommand_calling_info['after_combi_discount_'.$key] = $row['NET_COMBI_DISCOUNT'] ;
                    }

                    $monthly_price += $plus_call_price ;
                    $monthly_price += $plus_sms_price ;
                    $monthly_price += $plus_data_price ;

                    $benefit = 0 ;
                    if ( $arr_benefit[$key] == '장애우/국가유공자' )
                    {
                        $benefit = ($row['CP_PRICE'] - $row['CP_DISCOUNT'])*0.35 ;    
                    }
                    else if ( $arr_benefit[$key] == '차상위계층' )
                    {
                        $benefit = $row['CP_PRICE'] * 0.35 ;

                        if ( $benefit > 15000 )
                        {
                            $benefit = 15000 ;
                        }                                     
                    }     
                    else if ( $arr_benefit[$key] == '기초생활수급자' )
                    {
                        $benefit = 15000 + $plus_call_price/2 ;
                    } 

                    $monthly_price -= $benefit ;

                    $after_card_discount = $arr_card_discount[$key] ;                    

                    if ( $arr_combi_card[$key] == '결합 원함' )
                    {
                        $card_using = $arr_card_using[$key] ;                    
                        
                        if ( $after_card_discount < 15000 && $card_using >= 700000 )
                        {
                            $after_card_discount = 15000 ;
                        }
                        else if ( $after_card_discount < 7000 && $card_using >= 300000 )
                        {
                            $after_card_discount = 7000 ;
                        }                       
                    }

                    $monthly_price -= $after_card_discount ;

                    $etc_price = $arr_etc_price[$key] ;
                    $monthly_price +=  $etc_price ;

                    $monthly_price *= 1.1 ;

                    $recommand_calling_info['after_calling_plan_'.$key] = $row['CP_NAME'] ;
                    $recommand_calling_info['after_basic_price_'.$key] = $row['CP_PRICE'] ;
                    $recommand_calling_info['after_price_discount_'.$key] = $row['CP_DISCOUNT'] ;                    
                    $recommand_calling_info['after_benefit_discount_'.$key] = $benefit ;
                    $recommand_calling_info['after_plus_call_'.$key] = $plus_call_price ;
                    $recommand_calling_info['after_plus_sms_'.$key] = $plus_sms_price ;
                    $recommand_calling_info['after_plus_data_'.$key] = $plus_data_price ;
                    $recommand_calling_info['after_card_discount_'.$key] = $after_card_discount ;
                    $recommand_calling_info['after_etc_price_'.$key] = $etc_price ;
                    $recommand_calling_info['after_monthly_price_'.$key] = $monthly_price ;

                    if ( !isset($arr_calling_info[$key]) || $arr_calling_info[$key]['after_monthly_price_'.$key] > $monthly_price )
                    {
                        $arr_calling_info[$key] = $recommand_calling_info ;
                    }                    
                }                
            }
        }

        echo json_encode($arr_calling_info) ;
        return TRUE ;  
    }

	function lists()
	{
		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $vender  = $seg->get('vender'); // 통신사
        $page  = $seg->get('page', 1); // 페이지
        
        $vender_list = $this->S_Reason_model->get_vender_list(false, true) ;

        if ( $vender == '' )
        {
            $vender = key($vender_list) ;
        }
        
        $total_count = $this->HB_calling_plan_model->list_count($vender) ;

        $config['suffix']       = '';
        $config['base_url']    = RT_PATH.'/consult/plan/lists/vender/'.$vender.'/page/';
        $config['per_page']    = 10 ;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->HB_calling_plan_model->list_result($config['per_page'], $offset, $vender) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
            $list[$i]['href']               = '/consult/plan/view/cp_id/'.$row['CP_ID'];
        }

        $head = array('title' => '요금제 관리') ;
        $data = array(
            'total_count' => $total_count,   
            'list' => $list,
            'paging' => $CI->pagination->create_links(),  
            'vender' => $vender,
            'vender_list' => $vender_list,
        );

		widget::run('head', $head);
		$this->load->view('consult/lists', $data);
		widget::run('tail');
	}

	function add()
	{
		$head = array('title' => '요금제 추가') ;
        $data = array(
        	'vender_list' => $this->S_Reason_model->get_vender_list(false, true),
        ) ;

		widget::run('head', $head);
		$this->load->view('consult/add_calling_plan', $data);
		widget::run('tail');
	}

	function insert()
	{
		//check_token('consult/plan');

    	$result = $this->HB_calling_plan_model->insert() ;    	

    	if ( $result )
    	{
    		alert('요금제가 등록되었습니다.', 'consult/plan/lists') ;
    	}        
	}

	function view()
	{
		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $cp_id  = $seg->get('cp_id');        

		$head = array('title' => '요금제 수정') ;
        $data = array(
        	'cp_info' 	  => $this->HB_calling_plan_model->get_cp_info($cp_id),
        	'vender_list' => $this->S_Reason_model->get_vender_list(false, true),
        ) ;

		widget::run('head', $head);
		$this->load->view('consult/view_calling_plan', $data);
		widget::run('tail');		
	}

	function update()
	{
		//check_token('consult/plan');

		$cp_id = $this->input->post('CP_ID') ;
		$vender = $this->input->post('TEL_COMPANY') ;

    	$result = $this->HB_calling_plan_model->update($cp_id) ;    	

    	if ( $result )
    	{
    		alert('요금제가 수정되었습니다.', 'consult/plan/lists/vender/'.$vender) ;
    	}  		
	}

	function del()
	{
		//check_token('consult/plan');

		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $cp_id  = $seg->get('cp_id');    

    	$result = $this->HB_calling_plan_model->del($cp_id) ;    	

    	if ( $result )
    	{
    		alert('요금제가 삭제되었습니다.', 'consult/plan/lists') ;
    	}  		
	}
}
?>