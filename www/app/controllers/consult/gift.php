<?php
class Gift extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('S_Reason_model', 'HB_calling_plan_model', 'HB_wired_gift'));

		define('WIDGET_SKIN', 'main');
		define('CSS_SKIN', 'pages/page_pricing');

		//this->output->enable_profiler(TRUE);
	}

	function lists()
	{
		if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $vender  = $seg->get('vender'); // 통신사
        $page  = $seg->get('page', 1); // 페이지
        
        $vender_list = $this->S_Reason_model->get_vender_list(false, true) ;

        if ( $vender == '' )
        {
            $vender = key($vender_list) ;
        }
        
        $total_count = $this->HB_wired_gift->list_count($vender) ;

        $config['suffix']       = '';
        $config['base_url']    = RT_PATH.'/consult/plan/lists/vender/'.$vender.'/page/';
        $config['per_page']    = 10 ;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->HB_wired_gift->list_result($config['per_page'], $offset, $vender) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
            $list[$i]['href']               = '/consult/gift/view/pdt_cd/'.$row['PDT_CD'];
        }

        $head = array('title' => '유선 상품 관리') ;
        $data = array(
            'total_count' => $total_count,   
            'list' => $list,
            'paging' => $CI->pagination->create_links(),  
            'vender' => $vender,
            'vender_list' => $vender_list,
        );

		widget::run('head', $head);
		$this->load->view('consult/gift_lists', $data);
		widget::run('tail');
	}

	function view()
	{
		if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $pdt_cd  = $seg->get('pdt_cd');

		$head = array('title' => '사은품 정보 수정') ;
        $data = array(
        	'gift_info'   => $this->HB_wired_gift->get_info($pdt_cd),
        ) ;

		widget::run('head', $head);
		$this->load->view('consult/view_wired_gift', $data);
		widget::run('tail');		
	}

	function update()
	{
		//check_token('consult/plan');

        $pdt_cd = $this->input->post('PDT_CD') ;
        $vender = $this->input->post('TEL_COMPANY') ;

    	$result = $this->HB_wired_gift->update($pdt_cd) ;

    	if ( $result )
    	{
    		alert('사은품 정보가 수정되었습니다.', 'consult/gift/lists/vender/'.$vender) ;
    	}  		
	}
}
?>