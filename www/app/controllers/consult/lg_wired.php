<?php
class Lg_wired extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('S_Reason_model', 'HB_calling_plan_model', 'HB_card_place_model', 'HB_card2_model', 'HB_card_detail2_model'));

        $this->load->library('user_agent');

        if (!$this->agent->is_mobile())
        {
            define('WIDGET_SKIN', 'main');
        }

		define('CSS_SKIN', 'pages/page_pricing');

		//$this->output->enable_profiler(TRUE);
	}

	function index() 
	{
        if (!$this->agent->is_mobile() && !IS_MEMBER)
        {
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');
        }

        $head = array('title' => 'LG 멀티룸팩 요금계산기') ;

		widget::run('head', $head);
		$this->load->view('consult/lg_wired');
		widget::run('tail');
	}

	function get_wired_result_price()
    {
        $result_price = array() ;

        $tv_count = $this->input->post('tv_count') ;

        $result_price['total_price'] = 0 ;
        $result_price['internet_set_price'] = 0 ;
        $result_price['tv_set_price'] = 0 ;
        $result_price['internet_gift'] = 0 ;
        $result_price['hb_or_cash_price'] = number_format( $tv_count * 160000 ) ;

        $result_price['div_result_price'] = $this->_get_wired_result_price() ;

        echo json_encode($result_price) ;
    }

	function _get_wired_result_price()
    {
        $div_wired_result_price_info = '' ;

        $tv_count = $this->input->post('tv_count') ;

        $internet_basic_price = 0 ;
        $internet_discount_price = 0 ;

        if ( $tv_count != "" ) {
            $internet_basic_price = 35000 ;
            $internet_discount_price = 10000 ;
        }

        $line_count = ceil($tv_count / 6) ;

        $internet_price = $internet_basic_price - $internet_discount_price ;
        $internet_price_vat = $internet_price * 1.1 ;
        $total_internet_price = $internet_price_vat * $line_count ;

        $div_wired_result_price_info .= '        
            <tr>
                <td class="text-center"><strong>인터넷</strong></td>
                <td class="text-center">'.number_format($internet_basic_price).'</td>
                <td class="text-center color-red">'.number_format($internet_discount_price).'</td>
                <td class="text-center">'.number_format($internet_price).'</td>
                <td class="text-center">'.number_format($internet_price).'</td>
                <td class="text-center"></td>
                <td class="text-center">0</td>
                <td class="text-center">'.number_format($internet_price_vat).'</td>
                <td class="text-center">'.number_format($line_count).'</td>
                <td class="text-center">'.number_format($total_internet_price).'</td>
            </tr>' ;

        $tv_basic_price = 14900 ;
        $tv_discount_price = 9000 ;
        $tv_price = $tv_basic_price - $tv_discount_price ;
        $stb_price = 0 ;
        $tv_price_vat = ($tv_price + $stb_price) * 1.1 ;

        $total_tv_price = $tv_price_vat * $tv_count ;

        $div_wired_result_price_info .= '        
            <tr>
                <td class="text-center"><strong>TV기본</strong></td>
                <td class="text-center">'.number_format($tv_basic_price).'</td>
                <td class="text-center color-red">'.number_format($tv_discount_price).'</td>
                <td class="text-center">'.number_format($tv_price).'</td>
                <td class="text-center">'.number_format($tv_price).'</td>
                <td class="text-center"></td>
                <td class="text-center">'.number_format($stb_price).'</td>
                <td class="text-center">'.number_format($tv_price_vat).'</td>
                <td class="text-center">'.number_format($tv_count).'</td>
                <td class="text-center">'.number_format($total_tv_price).'</td>
            </tr>' ;

        $wifi_basic_price = 1000 ;
        $wifi_discount_price = 0 ;
        $wifi_price = $wifi_basic_price - $wifi_discount_price ;
        $stb_price = 0 ;
        $wifi_price_vat = ($wifi_price + $stb_price) * 1.1 ;

        $total_wifi_price = $wifi_price_vat * $tv_count ;

        $div_wired_result_price_info .= '        
            <tr>
                <td class="text-center"><strong>WIFI</strong></td>
                <td class="text-center">'.number_format($wifi_basic_price).'</td>
                <td class="text-center color-red">'.number_format($wifi_discount_price).'</td>
                <td class="text-center">'.number_format($wifi_price).'</td>
                <td class="text-center">'.number_format($wifi_price).'</td>
                <td class="text-center"></td>
                <td class="text-center">'.number_format($stb_price).'</td>
                <td class="text-center">'.number_format($wifi_price_vat).'</td>
                <td class="text-center">'.number_format($tv_count).'</td>
                <td class="text-center">'.number_format($total_wifi_price).'</td>
            </tr>' ;

        $total_price = $total_internet_price + $total_tv_price + $total_wifi_price ;

        $div_wired_result_price_info .= '        
            <tr>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"><strong>월 요금 합계</strong></td>
                <td class="text-center"><strong>'.number_format($total_price).'</strong></td>
            </tr>' ;

        $div_wired_result_price_info .= '        
            <tr>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"><strong>룸 당월 요금</strong></td>
                <td class="text-center"><strong>'.number_format($total_price / $tv_count).'</strong></td>
            </tr>' ;

        return  $div_wired_result_price_info ;
    }
}
?>