<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
	function __construct() {
		parent::__construct();
		define('WIDGET_SKIN', 'home');
					
		$this->load->helper('cookie');
		$this->load->model(array('Popup_model', 'P_Pdtrecommand_model', 'Rank_model', 'OrderMaster_model', 'Board_notice_model', 'Pay_model', 'Order_model'));
		// $this->output->enable_profiler(TRUE);
	}

	function index() 
	{
		//$popup = $this->Popup_model->output();

		/*$pubasic = $pulayer = array();

		foreach ($popup as $i => $row) 
		{
			$id = $row['pu_id'];
			$skin = 'popup/'.$row['pu_file'];

			if (!$this->input->cookie('popup'.$id) && file_exists(SKIN_PATH.$skin.'.html')) {
				if ($row['pu_type'] == 1) {
					$pubasic[] = "<div id='popup".$id."' style='position:absolute; width:".$row['pu_width']."px; height:".$row['pu_height']."px; top:".$row['pu_y']."px; left:".$row['pu_x']."px; z-index:100; overflow:hidden;'>".$this->load->view($skin, array('id'=>'popup'.$id), TRUE)."</div>";
				}
				else {
					$pulayer[$i]->id = $id;
					$pulayer[$i]->html = "win_open('popup/".$id."', 'popup".$id."', 'left=".$row['pu_x']."px,top=".$row['pu_y']."px,width=".$row['pu_width']."px,height=".$row['pu_height']."px,scrollbars=0');";
				}
			}
		}*/

		$result = $this->P_Pdtrecommand_model->recommand_list(4, 0) ;		

        foreach ($result as $key => $product) 
        {   
            if ( $product['IMG_PATH'] != '' )
            {
            	$thumbnails = explode (",", $product['IMG_PATH']) ;
                $result[$key]['pt_thumbnail'] = $thumbnails[0] ;
            }
        }
		
		$user_id = $this->session->userdata('ss_mb_id') ;
		$rank_name = $this->session->userdata('RANK_NAME') ;

		$total_amt = $this->OrderMaster_model->get_total_amt($user_id) ;

		$popup_list = $this->Board_notice_model->get_popup_notice() ;

		$cookies = array() ;

		foreach ($popup_list as $key => $value) 
		{			
			$cookies[$key] = get_cookie('notice'.$key) ? 0 : 1 ;
		}		 

		$pay_week = $this->Pay_model->get_pay_list('week', $user_id, 'A.PAY_DATE', 'desc', 1, 0) ;
		$give_date = isset($pay_week[0]) ? $pay_week[0]['GIVE_DATES'] : '' ;
		$pay_week = isset($pay_week[0]) ? $pay_week[0]['TRUE_AMT'] : 0 ;		

		$data = array(
			'policy_list' => $result,
			//'prev_month_rank' => $this->Rank_model->get_prev_month_rank($user_id),
			'cur_month_rank' => $this->Rank_model->get_cur_month_rank($user_id, $rank_name),
			'pay_week'		=> $pay_week,
			'count_week'	=> $this->Order_model->list_count('', '', '', '', '', 'sub_main', ''),
			//'sum_amt'	=> $total_amt['TOTAL_AMT'], 
    		//'sum_cv' 	=> $total_amt['TOTAL_PV1'],
    		//'sum_pv' 	=> $total_amt['TOTAL_PV2'],
    		'notice_list' => $this->Board_notice_model->get_main_notice(),
    		'popup_list' => $popup_list,
    		'cookies' => $cookies,
		);

		if ( IS_MEMBER && !IS_MANAGER && !IS_CENTER )
        {
            $member = $this->Basic_model->get_member($user_id, 'IS_INPUT') ;
            $data['IS_INPUT'] = $member['IS_INPUT'] ;
            $data['GIVE_DATE'] = ($give_date != '') ? floor((strtotime($give_date) - time())/60/60/24) : 0 ;
        }

		widget::run('head');
		$this->load->view('main/main', $data);
		widget::run('tail');
	}

	function set_notice_view()
	{
		$notice_id = $this->input->post('notice_id') ;
		$today_no_view = $this->input->post('today_no_view') ;

		if ($notice_id != '') 
		{
			if ( $today_no_view != '' )
			{
				$cookie = array(
				   'name'   => $notice_id,
				   'value'  => $today_no_view,
				   'expire' => time()+60*60*24,
				   'domain' => $this->config->item('cookie_domain')
			    );
			   set_cookie($cookie);
			}
			else if (get_cookie($notice_id))
			{
				delete_cookie($notice_id) ;
			}
		}
	}
}