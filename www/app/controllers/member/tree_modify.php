<?php
class Tree_modify extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->config->load('cf_register');
		$this->config->load('cf_icon');
		$this->load->library(array('form_validation'));
		$this->load->model(array('Member_info_model', 'Basic_model'));
		
		define('CSS_SKIN', 'jquery');
	}

    function _remap($index)
    {
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'supporter':
            case 'recommender':
                $this->_index();
                break;
            case 'update' :
                $this->_update() ;
                break;
            default:
                show_404();
                break;
        }
    }

	function _index()
	{
		define('WIDGET_SKIN', 'main');

		if (!IS_MEMBER)
			alert('로그인 후 이용하여 주십시오.');

		if (SU_ADMIN)
			alert('관리자 아이디는 접근 불가합니다.', '/');

		if (!$this->input->post('mb_password'))
			goto_url('/');

		$member = $this->Basic_model->get_login($this->input->post('mb_id'), $this->input->post('mb_password')) ;

		if ($member['USERID'] != $this->input->post('mb_id'))
			alert('로그인된 회원과 넘어온 정보가 서로 다릅니다.');
		
		$mb_password = ($this->session->userdata('ss_tmp_password')) ? $this->session->userdata('ss_tmp_password') : $this->input->post('mb_password');

        $this->load->library('segment', array('offset'=>2), 'seg'); // 세그먼트 주소

        $seg   =& $this->seg;
        $type  = $seg->get('tree_modify');
		
		if ($member['PASSWD'] != $member['PASSWD2'])
			alert('비밀번호가 맞지 않습니다.', 'member/confirm/qry/member.tree_modify/'.$type);

		// 수정 후 다시 이 폼으로 돌아오기 위해 임시로 저장해 놓음
		$this->session->set_userdata('ss_tmp_password', $mb_password);

		$this->_form($member['USERID'], $type) ;
	}

	function _form($in_member_id, $in_type)
	{
		$token = get_token();

		$member = $this->Member_info_model->get_detail_member_info_by_id($in_member_id) ;

        $type = "후원인" ;
        $old_id = $member["P_ID"] ;

        if ( $in_type == "recommender" )
        {
            $type = "추천인" ;
            $old_id = $member["R_ID"] ;
        }

		$head = array('title' => $type.' 정보 수정');

		$data = array_merge(array(
			'token' => $token,
            'type' => $type,
            's_type' => $in_type,
            'old_id' => $old_id,
		), $member);

		widget::run('head', $head);
		$this->load->view('member/tree_modify', $data);
		widget::run('tail');
	}

	function _update()
	{
		define('WIDGET_SKIN', 'main');

        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소

        $seg   =& $this->seg;
        $type  = $seg->get('update');

		check_token('member/confirm/qry/member.tree_modify/'.$type);

		$this->load->helper('chkstr');
		$config = array(
			array('field'=>'new_id', 'label'=>'후원인', 'rules'=>'trim|required|exact_length[8]|numeric'),
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) 
		{
			$this->_form($this->input->post('mb_id'), $type);
		}
		else 
		{
			if (!IS_MEMBER)
				alert("로그인 되어 있지 않습니다.");
				
			if ( IS_MANAGER )
			{
				$this->Member_info_model->update();
				alert_parent_reload("수정되었습니다.");
			}
			else
			{
				$mb_id = $this->input->post('mb_id');

				if ($this->session->userdata('ss_mb_id') != $mb_id)
					alert("로그인된 정보와 수정하려는 정보가 틀리므로 수정할 수 없습니다.");

                $s_type = "후원인" ;
                $gubun = 1 ;

                if ( $type == "recommender" )
                {
                    $s_type = "추천인" ;
                    $gubun = 2 ;
                }

                $old_id = $this->input->post('old_id');
                $new_id = $this->input->post('new_id');

				$this->Member_info_model->change_member_sp($mb_id, $gubun, $old_id, $new_id ) ;

				{
					echo "<html>
							<body>
								<form name='fupdate' method='post' action='".RT_PATH."/member/tree_modify/".$type."'>
									<input type='hidden' name='mb_id' value='".$mb_id."'>
									<input type='hidden' name='mb_password' value='".$this->session->userdata('ss_tmp_password')."'>
									<input type='hidden' name='token' value='".get_token()."'>
								</form>
								<script language='JavaScript'>
									alert('".$s_type." 정보가 수정 되었습니다.');
									document.fupdate.submit();
								</script>
							</body>
						</html>";
				}
			}
		}
	}
}
?>