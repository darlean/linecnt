<?php
class Lost_pw extends CI_Controller 
{
	function __construct() {
		parent::__construct();
		$this->config->load('cf_register');
		$this->load->model('Member_forget_model');
		define('WIDGET_SKIN', 'main');

		//$this->output->enable_profiler(TRUE);
	}

	function index() 
	{
		if (IS_MEMBER)
			alert('이미 로그인 중입니다.');

		$head = array('title' => '비밀번호 찾기');
		$data = array();

		widget::run('head', $head);
		$this->load->view('member/lost_pw', $data);
		widget::run('tail');
	}

	function result()
	{
		if (!$this->input->post('w'))
			goto_url('/');

		$login_id = $this->input->post('login_id');
		$e_mail = $this->input->post('e_mail');
				
		if( $this->Member_forget_model->check( $login_id, $e_mail ) == FALSE )
		{
			alert('입력하신 내용으로는 회원정보가 존재하지 않습니다.', 'member/lost_pw') ;
			//return;
		}

		$msg = $this->Member_forget_model->new_pwd( $login_id, $e_mail );

		$head = array('title' => '비밀번호 찾기 결과');
		$data = array(
			'mb_id' => $login_id,
			'msg' => $msg
		);

		widget::run('head', $head);
		$this->load->view('member/result_pwd', $data);
		widget::run('tail');
	}
}
?>