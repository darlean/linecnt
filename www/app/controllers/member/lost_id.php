<?php
class Lost_id extends CI_Controller 
{
	function __construct() {
		parent::__construct();
		$this->config->load('cf_register');
		$this->load->model('Member_forget_model');
		define('WIDGET_SKIN', 'main');

		//$this->output->enable_profiler(TRUE);
	}

	function index() 
	{
		if (IS_MEMBER)
			alert('이미 로그인 중입니다.');

		$head = array('title' => '아이디 찾기');
		$data = array();

		widget::run('head', $head);
		$this->load->view('member/lost_id', $data);
		widget::run('tail');
	}

	function result()
	{
		if (!$this->input->post('w'))
			goto_url('/');

		$username = $this->input->post('username');
		$jumin_no = $this->input->post('jumin_no');
				
		$mb = $this->Member_forget_model->find_id( $username, $jumin_no ) ;

		if ($mb==FALSE) 
		{
			alert('입력하신 내용으로는 회원정보가 존재하지 않습니다.', 'member/lost_id') ;
		}	

		$this->result_find_id( $mb->LOGIN_ID );
	}

	private function result_find_id($in_login_id)
	{
		$head = array('title' => '아이디 찾기 결과');
		$data = array('login_id' => $in_login_id);

		widget::run('head', $head);
		$this->load->view('member/result_id', $data);
		widget::run('tail');
	}
}
?>