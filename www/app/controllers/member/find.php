<?php
class Find extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();
		$this->config->load('cf_register');
		$this->load->model('Member_info_model');
		//define('WIDGET_SKIN', 'main');

		//$this->output->enable_profiler(TRUE);
	}

	function index() 
	{		
		$head = array('title' => '회원명부 조회');
		$data = array();

		widget::run('head', $head);
		$this->load->view('member/find', $data);
		widget::run('tail');
	}

	function result()
	{
		$username = $this->input->post('username');
		$userid = $this->input->post('userid');
		$list = $this->Member_info_model->find_member_list( $username, $userid );

		$head = array('title' => '회원명부 조회');
		$data = array(
			'list' => $list
		);

		widget::run('head', $head);
		$this->load->view('member/result_find', $data);
		widget::run('tail');
	}
}
?>