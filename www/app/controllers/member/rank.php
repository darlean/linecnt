<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rank extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model(array('D_Rank_model', 'S_Rank_model', 'Basic_model', 'Pay_model', 'Member_info_model'));
		$this->load->helper(array( 'url' ));
		$this->load->config('cf_position') ;

		define('WIDGET_SKIN', 'main');
		
		//$this->output->enable_profiler(TRUE);
	}

	function _remap($index)
	{        
		if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

		switch($index)
		{
			case 'page':
			case 'tab':
			case 'index':			
				$this->index() ;
			break;		
            case 'grp':	
                $this->grp() ;
            break ;
            case 'excel': 
                $this->excel() ;
            break ;            
			default:
					show_404();
			break;
		}
	}

	function index()
	{
		$this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소   
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $year   = $param->get('year', date('Y'));   
        $month  = $param->get('month', date('m'));          
        $show  = $param->get('show', "all"); 
        $qstr  = $param->output();

        $page  = $seg->get('page', 1); // 페이지        

        $tab_idx = $seg->get('tab', 9);

        $rank_list = $this->S_Rank_model->get_rank_list(true) ;

        $rank_count_list = array() ;
        foreach ($rank_list as $key => $value) 
        {
        	$rank_count_list[$key] = $this->D_Rank_model->get_rank_count($year, $month, $key, $show) ;

        	if ( $key == $tab_idx )
        	{
        		$total_count = $rank_count_list[$key] ;
        	}
        }        

        $config['suffix']       = $qstr;        
        $config['base_url']    = RT_PATH.'/member/rank/tab/'.$tab_idx.'/page/';       
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page'); 

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->D_Rank_model->get_rank_list($year, $month, $tab_idx, $show, $config['per_page'], $offset) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;            
        }

        $head = array('title' => '회원직급현황');
        $data = array
        (   
        	'rank_list'			=> $rank_list,         
        	'rank_count_list'	=> $rank_count_list,
            'year'              => $year,
            'month'             => $month, 
            'tab'               => $tab_idx,
            'show'              => $show,
            'show_list'         => array('all' => '전체', '0' => '변동없음', '1' => '승급', '2' => '강등'),
            'qstr'              => $qstr,
            'list'              => $list,
            'paging'            => $CI->pagination->create_links(),
        ) ;

        widget::run('head', $head);
        $this->load->view('member/rank', $data);
        widget::run('tail');
    }

    function grp($in_type = '')
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소   
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $qstr  = $param->output();      

        $page  = $seg->get('page', 1); // 페이지        
        $tab_idx = $seg->get('tab', 0);        

        $pay_date_list = $this->Pay_model->get_pay_date_list() ;
        $pay_date  = $seg->get('pay_date', $pay_date_list[0]['PAY_DATE']);        

        $result = $this->S_Rank_model->get_rank_list(true) ;
        
        $rank_list = array() ;
        $rank_list[0] = 'All' ;
        foreach ($result as $key => $value) 
        {
            if ( $key >= 31 )
            {
                $rank_list[$key] = $value ;
            }
        }

        $pay_calc_info = $this->Pay_model->get_pay_calc_info($pay_date) ;
        $pay_calc_idx = array('AMT_WCY_GRP_CV', 'AMT_WCY_GRP_CV_GD', 'AMT_WCY_GRP_CV_RB', 'AMT_WCY_GRP_CV_SP') ;

        $rank_count_list = array() ;
        $rank_total_grp_list = array() ;
        $rank_grp_list = array() ;
        $pay_calc_info_list = array() ;        

        $i = 0 ;
        foreach ($rank_list as $key => $value) 
        {
            $rank_count_list[$key] = $this->Pay_model->get_rank_count($key, $pay_date) ;
            $rank_total_grp_list[$key] = $this->Pay_model->get_total_grp($key, $pay_date) ;
            $pay_calc_info_list[$key] = isset($pay_calc_idx[$i]) ? $pay_calc_info[$pay_calc_idx[$i]] : 0 ;

            if ( $key == $tab_idx )
            {
                $total_count = $rank_count_list[$key] ;
            }

            if ( $in_type == 'excel' )
            {
                $rank_grp_list[$key] = $this->Pay_model->get_rank_list($key, $rank_count_list[$key], 0, $pay_date) ;

                foreach ($rank_grp_list[$key] as $idx => $row) 
                {                
                    $rank_grp_list[$key][$idx]['num'] = $rank_count_list[$key] - $idx ;          

                    $rank_result = $this->Pay_model->get_member_count_subtree_classified_by_rank($pay_date, $row['USERID']) ;    

                    $rank_grp_list[$key][$idx]['GD']                 = isset($rank_result['31']) ? $rank_result['31'] : 0 ;
                    $rank_grp_list[$key][$idx]['RB']                 = isset($rank_result['41']) ? $rank_result['41'] : 0 ;
                    $rank_grp_list[$key][$idx]['SP']                 = isset($rank_result['51']) ? $rank_result['51'] : 0 ;
                    $rank_grp_list[$key][$idx]['DD']                 = isset($rank_result['61']) ? $rank_result['61'] : 0 ;
                    $rank_grp_list[$key][$idx]['CW']                 = isset($rank_result['71']) ? $rank_result['71'] : 0 ;

                    $tmp_result = $this->Member_info_model->sub_member_two_my_tree($row['USERID']) ;
                    $a_sub_total_pv = 0 ;
                    $b_sub_total_pv = 0 ;
                   
                    if ( isset($tmp_result[0]) && isset($tmp_result[0]['USERID']) )
                    {                
                        $a_sub_total_pv = $this->Pay_model->get_member_subtree_total_pv($pay_date, $tmp_result[0]['USERID']) ;                                  
                    }

                    if ( isset($tmp_result[1]) && isset($tmp_result[1]['USERID']) )
                    {                
                        $b_sub_total_pv = $this->Pay_model->get_member_subtree_total_pv($pay_date, $tmp_result[1]['USERID']) ;                  
                    }

                    $rank_grp_list[$key][$idx]['a_sub_total_pv'] = $a_sub_total_pv ; 
                    $rank_grp_list[$key][$idx]['b_sub_total_pv'] = $b_sub_total_pv ;       
                }
            }

            $i++ ;
        }        

        $config['suffix']       = $qstr;        
        $config['base_url']    = RT_PATH.'/member/rank/grp/tab/'.$tab_idx.'/pay_date/'.$pay_date.'/page/';       
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page'); 

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        if ( $in_type == 'excel' )
        {
            $limit = $total_count ;
            $offset = 0 ;
        }
        else
        {   
            $limit = $config['per_page'] ;
            $offset = ($page - 1) * $config['per_page'];
        }        

        $result = $this->Pay_model->get_rank_list($tab_idx, $limit, $offset, $pay_date) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;        

            $rank_result = $this->Pay_model->get_member_count_subtree_classified_by_rank($pay_date, $row['USERID']) ;    

            $list[$i]['GD']                 = isset($rank_result['31']) ? $rank_result['31'] : 0 ;
            $list[$i]['RB']                 = isset($rank_result['41']) ? $rank_result['41'] : 0 ;
            $list[$i]['SP']                 = isset($rank_result['51']) ? $rank_result['51'] : 0 ;
            $list[$i]['DD']                 = isset($rank_result['61']) ? $rank_result['61'] : 0 ;
            $list[$i]['CW']                 = isset($rank_result['71']) ? $rank_result['71'] : 0 ;

            $tmp_result = $this->Member_info_model->sub_member_two_my_tree($row['USERID']) ;
            $a_sub_total_pv = 0 ;
            $b_sub_total_pv = 0 ;
           
            if ( isset($tmp_result[0]) && isset($tmp_result[0]['USERID']) )
            {                
                $a_sub_total_pv = $this->Pay_model->get_member_subtree_total_pv($pay_date, $tmp_result[0]['USERID']) ;                                  
            }

            if ( isset($tmp_result[1]) && isset($tmp_result[1]['USERID']) )
            {                
                $b_sub_total_pv = $this->Pay_model->get_member_subtree_total_pv($pay_date, $tmp_result[1]['USERID']) ;                  
            }

            $list[$i]['a_sub_total_pv'] = $a_sub_total_pv ; 
            $list[$i]['b_sub_total_pv'] = $b_sub_total_pv ; 
        }

        $head = array('title' => '회원점수현황');
        $data = array
        (   
            'rank_list'             => $rank_list,         
            'rank_count_list'       => $rank_count_list,      
            'rank_total_grp_list'   => $rank_total_grp_list, 
            'pay_calc_info_list'    => $pay_calc_info_list,
            'rank_grp_list'     => $rank_grp_list,     
            'tab'               => $tab_idx,            
            'qstr'              => $qstr,
            'list'              => $list,
            'paging'            => $CI->pagination->create_links(),

            'pay_date_list'     => $pay_date_list,
            'pay_date'          => $pay_date,        
        ) ;

        if ( $in_type == 'excel' )
        {
            return $data ;
        }
        else
        {
            widget::run('head', $head);
            $this->load->view('member/rank_grp', $data);
            widget::run('tail');
        }
    }
    
    function excel()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소   
        $seg      =& $this->seg;    
        $type  = $seg->get('type'); 
        
        if ( $type == 'grp' )
        {
            return $this->excel_grp() ;
        }
    }

    function excel_grp()
    {
        $this->load->library('excel') ;
        
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");

        $cell_list = range('A', 'Z') ;

        $str_list = array( 'NO', '회원ID', '회원명', '가입일자', '마감직급', '현재직급', '산하GD', '산하RB', '산하SP', '산하DD', '산하CW', 'A라인', 'B라인', '점수' ) ;
        $val_list = array( 'num', 'USERID', 'USERNAME', 'REG_DATE', 'OLD_RANK_CD', 'RANK_CD', 'GD', 'RB', 'SP', 'DD', 'CW', 'a_sub_total_pv', 'b_sub_total_pv', 'CALC_GRP_CV' ) ;        
 
        $data = $this->grp('excel') ;     

        $sheet = 0 ;
        foreach ($data['rank_list'] as $tab => $rank) 
        {
            // Add new sheet
            if ( $sheet > 0 )
            {
                $objWorkSheet = $objPHPExcel->createSheet();
            }

            $i = 1 ;
            foreach ($str_list as $key => $row) 
            {            
                $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue($cell_list[$key]."$i", $str_list[$key]) ;
            }      
                    
            $i++ ;            
            foreach ($data['rank_grp_list'][$tab] as $key => $row) 
            {            
                foreach ($val_list as $idx => $value) 
                {
                    if ( $value == 'USERID' || $value == 'CALC_GRP_CV' )
                    {
                        $objPHPExcel->setActiveSheetIndex($sheet)->setCellValueExplicit($cell_list[$idx]."$i", $row[$value], PHPExcel_Cell_DataType::TYPE_STRING) ;
                    }
                    else
                    {
                        $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue($cell_list[$idx]."$i", $row[$value]) ;
                    }
                }

                $i++ ;
            }

            if ( $data['rank_total_grp_list'][$tab] > 0 )
            {
                $endCol = $objPHPExcel->getActiveSheet()->getHighestDataColumn() ;

                $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue("A$i", "합계") ;
                $objPHPExcel->getActiveSheet()->mergeCells("A$i:F$i") ;                
                $objPHPExcel->setActiveSheetIndex($sheet)->setCellValueExplicit($endCol."$i", $data['rank_total_grp_list'][$tab], PHPExcel_Cell_DataType::TYPE_STRING) ;                      

                $objPHPExcel->getActiveSheet()->getStyle("A$i:$endCol$i")->getFont()->setBold(true) ;
            }
            else
            {
                $i-- ;
            }
            
            $objPHPExcel->SetStyleNoTileBar($rank, $i, $sheet) ;

            $sheet++ ;
        }

        $title = '회원점수현황('.$data['pay_date'].')' ;                    
        $objPHPExcel->CreateExcelFile($title) ; 
    } 
}
?>