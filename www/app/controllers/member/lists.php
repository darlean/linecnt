<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lists extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model(array('Member_info_model', 'S_Rank_model', 'Basic_model'));
		$this->load->helper(array( 'url' ));
		$this->load->config('cf_position') ;
		
		//$this->output->enable_profiler(TRUE);
	}

	function _remap($index)
	{        
		if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

		switch($index)
		{
			case 'page':
			case 'index':
			case 'reception':
					define('WIDGET_SKIN', 'main');
					$this->member_list($index);
			break;
			case 'excel' :
					$this->excel() ;
			break;
			case 'move' :
					$this->move() ;
			break;	
			case 'lg' :
					$this->excel_for_LG() ;
			break;		
			case 'registration' :
					$this->registration() ;
			break ;
			default:
					show_404();
			break;
		}
	}

	function member_list($in_type)
	{
		$data = $this->_list($in_type) ;

		if ( $in_type == 'reception' )
		{
			$head = array('title' => '접수처 정보 조회');
		}
		else
		{
			$head = array('title' => '회원 정보 조회');
		}

		widget::run('head', $head);
		$this->load->view('member/member_list', $data);
		widget::run('tail');
	}

	function excel()
	{        
		$this->load->library('excel') ;

		// Create new PHPExcel object
		$objPHPExcel = $this->excel ;
		
		header("Content-Type: text/html; charset=utf-8");
		header("Content-Encoding: utf-8");

		// Set properties
		// Excel 문서 속성을 지정해주는 부분이다. 적당히 수정하면 된다.
		/*$objPHPExcel->getProperties()
					->setCreator("작성자")  //작성자
					->setLastModifiedBy("최종 수정자")  //최종수정자
					->setTitle("타이틀")  //타이틀
					->setSubject("주제")  //주제
					->setDescription("설명")  //설명
					->setKeywords("키워드")  //키워드
					->setCategory("라이센스");  //License*/
 
 		$i = 3 ;
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue("A$i", "NO")
					->setCellValue("B$i", "회원명")
					->setCellValue("C$i", "회원ID")
					->setCellValue("D$i", "연락처")
					->setCellValue("E$i", "비상연락처")
					->setCellValue("F$i", "주소")
					->setCellValue("G$i", "직급")
					->setCellValue("H$i", "가입일자")
					->setCellValue("I$i", "센터")
					->setCellValue("J$i", "HB") ;

		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소	
		$seg   = & $this->seg;
		$type  = $seg->get('type', '');

		$data = $this->_list($type, 'excel') ;		
		$i++ ;
		foreach ($data['list'] as $key => $row) 
		{
			$row['PV'] = str_replace(',', '', $row['PV']) ;

			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("A$i", "$row[num]")
		                ->setCellValue("B$i", "$row[USERNAME]")
		                ->setCellValueExplicit("C$i", "$row[USERID]", PHPExcel_Cell_DataType::TYPE_STRING)
		                ->setCellValueExplicit("D$i", "$row[TEL]", PHPExcel_Cell_DataType::TYPE_STRING)
		                ->setCellValueExplicit("E$i", "$row[MOBILE]", PHPExcel_Cell_DataType::TYPE_STRING)
		                ->setCellValue("F$i", "$row[ADDR1]")
		                ->setCellValue("G$i", "$row[RANK_NAME]")
		                ->setCellValue("H$i", "$row[REG_DATE]")
		                ->setCellValue("I$i", "$row[CENTER_NAME]")
		                ->setCellValue("J$i", "$row[PV]") ;
			$i++ ;
		}
	
		$i-- ;

		if ( $type == 'reception' )
		{
			$title = '접수처 정보 조회' ;
		}
		else
		{
			$title = '회원 정보 조회' ;			
		}

		$objPHPExcel->getActiveSheet()->getStyle('M4:N'.$i)->getNumberFormat()->setFormatCode('#,##0') ;		
		$objPHPExcel->CreateExcelFileWithHBDefaultStyle($title, $i) ;
	}

	function _list($in_type, $in_excel = '') 
	{
		if ( $in_type == "reception" )
		{
			$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소	
			$config['base_url']    = RT_PATH.'/member/lists/reception/page/';
		}
		else
		{
			$this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
			$config['base_url']    = RT_PATH.'/member/lists/page/';
		}

		$this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

		$seg      =& $this->seg;
		$param    =& $this->param;
		
		$page  = $seg->get('page', 1); // 페이지
		$qstr  = $param->output();
		$sst   = $param->get('sst');   // 정렬필드
		$sod   = $param->get('sod');   // 정렬순서
		$sfl   = $param->get('sfl');   // 검색필드
		$stx   = $param->get('stx');   // 검색어
		$sca   = $param->get('sca');   // 분류

		$sdt   = $param->get('sdt');        // 날짜검색타입
		$sdt_s   = $param->get('sdt_s');    // 날짜검색타입명
		$start_date = $param->get('start');   // 시작날짜
		$finish_date = $param->get('finish'); // 종료날짜

		// 정렬
		if (!$sst) 
		{
			$sst = 'USERID';
			$sod = 'desc';            
		}
		else
		{
			$sst = preg_match("/^(USERID|RANK_CD|REG_DATE)$/i", $sst) ? $sst : FALSE;
		}
		
		$total_count = $this->Member_info_model->list_count($sfl, $stx, $in_type) ;

		$config['suffix']       = $qstr;
		$config['per_page']    = 10;
		$config['total_rows']  = $total_count;
		$config['uri_segment'] = $seg->pos('page');		

		$CI =& get_instance();
		$CI->load->library('pagination', $config);
		
		$excel_num  = $seg->get('excel_num', 1);

		//  페이지 선택 후,  검색시 오류 방지
		$page = ( $total_count < ($page - 1) * $config['per_page'] ) ? 1 : $page ;

		if ( $in_excel == 'excel' )
		{			
			$limit = $total_count ;
			$offset = 0 ;
		}
		else
		{	
			$limit = $config['per_page'] ;
			$offset = ($page - 1) * $config['per_page'];
		}

		$result = $this->Member_info_model->list_result($sst, $sod, $sfl, $stx, $limit, $offset, $in_type) ;

		// 일반 리스트
		$list = array();
		foreach ($result as $i => $row) 
		{
			$list[$i]                       = $row ;
			$list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
			$list[$i]['href']               = '/member/lists/move/'.$row['USERID'];
			$list[$i]['PV']                 = number_format($row['PV']) ;
		}

		$excel_limit = 1500 ;

		$data = array(
				'total_count' => $total_count,  
				'sst' => $sst,
				'sod' => $sod,          
				'sca' => $sca,
				'sfl' => $sfl,
				'stx' => $stx,
				'list' => $list,
				'paging' => $CI->pagination->create_links(),  
				'qstr' => $qstr,
				'type' => $in_type,

				'mb_position'   => $this->S_Rank_model->get_rank_list(),
				'mb_position_color' => $this->config->item('mb_position_color'),

				'sdt'           => $sdt,        // 날짜검색타입
				'sdt_s'         => $sdt_s,      // 날짜검색타입명
				'start_date'    => $start_date,  // 시작날짜
				'finish_date'   => $finish_date, // 종료날짜

				'sort_no' => $param->sort('USERID', 'desc'), 
				'sort_user_id' => $param->sort('USERID', 'desc'),
				'sort_rank' => $param->sort('RANK_CD', 'desc'), 
				'sort_reg_date' => $param->sort('REG_DATE', 'desc'), 
				'sort_center' => $param->sort('CENTER_CD', 'desc'),
				'sort_mb_cv' => $param->sort('mb_cv', 'desc'),
				'sort_mb_pv' => $param->sort('mb_pv', 'desc'),

				'excel_limit' => $excel_limit,
				'excel_count' => ($total_count / $excel_limit) + ( ($total_count % $excel_limit) > 0 ? 1 : 0 ),
				'excel_num' => $excel_num,
		);

		return $data ;
	}

	function move() 
	{
		if (!IS_MANAGER && !IS_DIRECT_BRANCH && !IS_CENTER) 
		{
			alert('잘못 된 접근입니다.', '/') ;
		}
	
		$this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소

		$seg      =& $this->seg ;
		$userid  = $seg->get('move') ;              
	
		$mb = $this->Basic_model->get_member($userid);      

		if ( !$mb )
		{
			alert('잘못 된 접근입니다.', '/') ;
		}

		$this->session->set_userdata('is_manager', FALSE );
		$this->session->set_userdata('is_center', FALSE );
		$this->session->set_userdata('is_direct_branch', FALSE );
		
		$this->session->set_userdata('ss_mb_id', $mb['USERID']);
		$this->session->set_userdata('USERNAME', $mb['USERNAME']);
		$this->session->set_userdata('KT_REG_NO', $mb['KT_REG_NO']);
		$this->session->set_userdata('SKT_REG_NO', $mb['SKT_REG_NO']);
		$this->session->set_userdata('LGU_REG_NO', $mb['LGU_REG_NO']);
		$this->session->set_userdata('KAIT_NO', $mb['KAIT_NO']);
	    $this->session->set_userdata('LGU_REG_CHECK', $mb['LGU_REG_CHECK']);
        $this->session->set_userdata('SKT_REG_CHECK', $mb['SKT_REG_CHECK']);
        $this->session->set_userdata('KT_REG_CHECK', $mb['KT_REG_CHECK']);

		if ( $mb['IS_RECEPTION'] == 'O' )
		{
			$this->session->set_userdata('is_reception', TRUE);				
		}		

		goto_url('/');
	}

	function excel_for_LG_backup()
	{        
		$this->load->library('excel') ;

		// Create new PHPExcel object
		$objPHPExcel = $this->excel ;
		
		header("Content-Type: text/html; charset=utf-8");
		header("Content-Encoding: utf-8");
 
 		$i = 1 ;
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue("A$i", "사전승낙코드")
					->setCellValue("B$i", "일련번호")
					->setCellValue("C$i", "신청일자")
					->setCellValue("D$i", "구분")
					->setCellValue("E$i", "분류")
					->setCellValue("F$i", "판매점명(개인성명)")
					->setCellValue("G$i", "주소(개인주소)")
					->setCellValue("H$i", "연락처")
					->setCellValue("I$i", "유선전화")
					->setCellValue("J$i", "전자메일")
					->setCellValue("K$i", "등록번호")
					->setCellValue("L$i", "직급")
					->setCellValue("M$i", "사업자등록번호(법인등록번호)")
					->setCellValue("N$i", "판매형태")
					->setCellValue("O$i", "상호")
					->setCellValue("P$i", "주소")
					->setCellValue("Q$i", "연락처")
					->setCellValue("R$i", "판매원증")
					->setCellValue("S$i", "사업자등록증")
					->setCellValue("T$i", "통신판매사")
					->setCellValue("U$i", "사전승낙위임여부")
					->setCellValue("V$i", "개인정보수집이용동의여부") ;

		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소	
		$seg   = & $this->seg;
		$type  = $seg->get('type', '');

		$data = $this->_list($type, 'excel') ;		
		$i++ ;
		foreach ($data['list'] as $key => $row) 
		{			
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("A$i", "")
						->setCellValue("B$i", "$row[num]")
						->setCellValue("C$i", "")
						->setCellValue("D$i", "신규")
						->setCellValue("E$i", "개인")
		                ->setCellValue("F$i", "$row[USERNAME]")
		                ->setCellValue("G$i", "$row[ADDR1] $row[ADDR2]")
		                ->setCellValueExplicit("H$i", "$row[MOBILE]", PHPExcel_Cell_DataType::TYPE_STRING)
		                ->setCellValueExplicit("I$i", "$row[TEL]", PHPExcel_Cell_DataType::TYPE_STRING)
		                ->setCellValueExplicit("J$i", "$row[EMAIL]", PHPExcel_Cell_DataType::TYPE_STRING)
		                ->setCellValueExplicit("K$i", "$row[USERID]", PHPExcel_Cell_DataType::TYPE_STRING)
		                ->setCellValue("L$i", "$row[RANK_NAME]")
		                ->setCellValue("M$i", "000-00-00000")
		                ->setCellValue("N$i", "대리점")
		                ->setCellValue("O$i", "주식회사 에이치비네트웍스")
		                ->setCellValue("P$i", "서울시 강남구 테헤란로70길 12, 4층(대치동, H-타워)")
		                ->setCellValue("Q$i", "1899-5990")
		                ->setCellValue("R$i", "O")
		                ->setCellValue("S$i", "X")
		                ->setCellValue("T$i", "X")
		                ->setCellValue("U$i", "X")
		                ->setCellValue("V$i", "O") ;

			$i++ ;
		}
	
		$i-- ;

		$title = '다단계 판매원 사전승낙 신청 관리 양식' ;			
		
    	$objPHPExcel->SetStyleNoTileBar($title, $i) ;
    	$objPHPExcel->CreateExcelFile($title) ;		
	}

	function excel_for_LG()
	{        	
		$this->load->library('excel') ;

        // Create new PHPExcel object
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");     
         
        $cell_list = range('A', 'Z') ;
        $str_list = array('분류', '직급(숨김)', '이름', '우편번호(신)', '주소(도로명)', '상세주소', '휴대폰', '유선전화', '전자메일', 
        					'판매원등록번호', '유무선구분', '온/오프라인구분', '거래통신사', '거래대리점', '거래대리점코드', 
        					'판매점p코드', '판매형태', '판매업자명', '우편번호(신)', '주소(도로명)', '상세주소', '연락처') ;
        $val_list = array('LGU_REG_NO', '', '신규', '개인', 'RANK_NAME', 'USERNAME', 'ZIPCODE', 'ADDR1', 'ADDR2', 'MOBILE', 'TEL', 'EMAIL', 'USERID', 
        					'무선', '오프라인', 'LGU+', '주식회사 에이치비네트웍스', '312535', 'USERID', '대리점', '주식회사 에이치비네트웍스', '06167', 
        					'서울특별시 강남구 테헤란로 87길 33', '4층(삼성동, 융전빌딩)', '1899-5990') ;

        $i = 1 ;
        $objPHPExcel->setActiveSheetIndex(0) ;
        $objPHPExcel->getActiveSheet()->setCellValue("A$i", "승낙코드(U+)") ;
        $objPHPExcel->getActiveSheet()->mergeCells('A1:A2') ;

        $objPHPExcel->getActiveSheet()->setCellValue("B$i", "신청차수(숨김)") ;
        $objPHPExcel->getActiveSheet()->mergeCells('B1:B2') ;

        $objPHPExcel->getActiveSheet()->setCellValue("C$i", "구분") ;
        $objPHPExcel->getActiveSheet()->mergeCells('C1:C2') ;

        $objPHPExcel->getActiveSheet()->setCellValue("D$i", "다단계판매원정보") ;
        $objPHPExcel->getActiveSheet()->mergeCells('D1:R1') ;

        $objPHPExcel->getActiveSheet()->setCellValue("S$i", "다단계판매업자") ;
        $objPHPExcel->getActiveSheet()->mergeCells('S1:Y1') ;

        $i++ ;
        foreach ($str_list as $key => $value) 
        {            
        	$objPHPExcel->getActiveSheet()->setCellValue($cell_list[$key + 3]."$i", $value) ;
        }

        $objPHPExcel->getActiveSheet()->getStyle("A1:Y2")->getFont()->setBold(true) ;

        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소	
		$seg   = & $this->seg;
		$type  = $seg->get('type', '');

		$sheetIndex = $objPHPExcel->getActiveSheet(); ;

		$data = $this->_list($type, 'excel') ;		

		$start = $data['excel_limit'] * ($data['excel_num'] - 1) ;
		$finish = $data['excel_limit'] * $data['excel_num'] ;	

		$i++ ;
		foreach ($data['list'] as $idx => $row) 
		{				
			if ( ( $idx >= $start && $idx < $finish ) && ($row['USERID'] != 'D0000000') )
			{
				foreach ($val_list as $key => $value) 
				{				
					if ( $value == 'LGU_REG_NO' || $value == 'USERID' || $value == 'MOBILE' || $value == 'TEL' || $value == 'EMAIL' || $value == 'ZIPCODE' )
	                {
	                	$val = isset($row[$value]) ? $row[$value] : "" ;
	                }      
	                else
	                {
	                	$val = isset($row[$value]) ? $row[$value] : $value ;                 
	                }

	                $sheetIndex->setCellValueExplicit($cell_list[$key].$i, $val, PHPExcel_Cell_DataType::TYPE_STRING) ;
				}	

				/*$LGU_REG_NO = isset($row['LGU_REG_NO']) ? $row['LGU_REG_NO'] : "" ;
				$ZIPCODE = isset($row['ZIPCODE']) ? $row['ZIPCODE'] : "" ;
				$MOBILE = isset($row['MOBILE']) ? $row['MOBILE'] : "" ;
				$TEL = isset($row['TEL']) ? $row['TEL'] : "" ;
				$EMAIL = isset($row['EMAIL']) ? $row['EMAIL'] : "" ;
				$ADDR1 = isset($row['ADDR1']) ? $row['ADDR1'] : "" ;
				$ADDR2 = isset($row['ADDR2']) ? $row['ADDR2'] : "" ;

				$sheetIndex->setCellValueExplicit("A$i", "$LGU_REG_NO", PHPExcel_Cell_DataType::TYPE_STRING)
			                ->setCellValue("B$i", "")
			                ->setCellValue("C$i", "신규")
			                ->setCellValue("D$i", "개인")
			                ->setCellValue("E$i", "$row[RANK_NAME]")
			                ->setCellValue("F$i", "$row[USERNAME]")
			                ->setCellValueExplicit("G$i", "$ZIPCODE", PHPExcel_Cell_DataType::TYPE_STRING)			          
			                ->setCellValue("H$i", "$ADDR1")
			                ->setCellValue("I$i", "$ADDR2")
			                ->setCellValueExplicit("J$i", "$MOBILE", PHPExcel_Cell_DataType::TYPE_STRING)	
			                ->setCellValueExplicit("K$i", "$TEL", PHPExcel_Cell_DataType::TYPE_STRING)
			                ->setCellValue("L$i", "$EMAIL")
			                ->setCellValueExplicit("M$i", "$row[USERID]", PHPExcel_Cell_DataType::TYPE_STRING)
			                ->setCellValue("N$i", "무선")
			                ->setCellValue("O$i", "오프라인") 
			                ->setCellValue("P$i", "LGU+")
			                ->setCellValue("Q$i", "주식회사 에이치비네트웍스")
			                ->setCellValue("R$i", "312535")
			                ->setCellValueExplicit("S$i", "$row[USERID]", PHPExcel_Cell_DataType::TYPE_STRING)	
			                ->setCellValue("T$i", "대리점")
			                ->setCellValue("U$i", "주식회사 에이치비네트웍스")
			                ->setCellValueExplicit("V$i", "06167", PHPExcel_Cell_DataType::TYPE_STRING)	
			                ->setCellValue("W$i", "서울특별시 강남구 테헤란로 87길 33")
			                ->setCellValue("X$i", "4층(삼성동, 융전빌딩)")
			                ->setCellValue("Y$i", "1899-5990") ;*/

				$i++ ;
			}
		}
                
		$i-- ;

		$title = '다단계 판매원 사전승낙 신청 관리 양식_'.$data['excel_num'] ;			
		
    	$objPHPExcel->SetStyleNoTileBar($title, $i) ;
    	$objPHPExcel->CreateExcelFile($title) ;		
	}

	function registration()
	{
		if (!IS_MEMBER) 
		{
			alert('잘못 된 접근입니다.', '/') ;
		}
	
		$this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소

		$seg      =& $this->seg ;
		$userid  = $seg->get('registration') ;  
		$type  = $seg->get('type') ;                 
	
		$mb = $this->Basic_model->get_member($userid);      

		if ( !$mb )
		{
			alert('잘못 된 접근입니다.', '/') ;
		}

		$mb['type'] = $type ;
        $mb['JUMIN_NO'] = substr($mb['JUMIN_NO'], 0, 6) ;

		$head = array('title' => $mb['USERNAME'].' 회원등록증');

		widget::run('head', $head);
		$this->load->view('member/print_registration', $mb);
		widget::run('tail');
	}
}
?>