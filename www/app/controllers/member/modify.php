<?php
class Modify extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->config->load('cf_register');
		$this->config->load('cf_icon');
		$this->load->library(array('form_validation'));
		$this->load->model(array('Member_info_model', 'Basic_model'));
		
		define('CSS_SKIN', 'jquery');
	}

	function index() 
	{
		define('WIDGET_SKIN', 'main');

		if (!IS_MEMBER)
			alert('로그인 후 이용하여 주십시오.');

		if (SU_ADMIN)
			alert('관리자 아이디는 접근 불가합니다.', '/');

		if (!$this->input->post('mb_password'))
			goto_url('/');

		$member = $this->Basic_model->get_login($this->input->post('mb_id'), $this->input->post('mb_password')) ;

		if ($member['USERID'] != $this->input->post('mb_id'))
			alert('로그인된 회원과 넘어온 정보가 서로 다릅니다.');
		
		$mb_password = ($this->session->userdata('ss_tmp_password')) ? $this->session->userdata('ss_tmp_password') : $this->input->post('mb_password');
		
		if ($member['PASSWD'] != $member['PASSWD2'])
			alert('비밀번호가 맞지 않습니다.', 'member/confirm/qry/member.modify');

		// 수정 후 다시 이 폼으로 돌아오기 위해 임시로 저장해 놓음
		$this->session->set_userdata('ss_tmp_password', $mb_password);

		$this->_form($member['USERID']) ;
	}

	function mb_id()
	{
		if (!IS_MANAGER)
			alert('잘못된 접근입니다.', '/');
		
		$this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소

		$seg   =& $this->seg;
		$mb_id  = $seg->get('mb_id');

		$member = $this->Member_infor_model->get_member_info_by_id($mb_id) ;
		$this->_form($member);
	}

	function _form($in_member_id) 
	{
		$token = get_token();

		$member = $this->Member_info_model->get_detail_member_info_by_id($in_member_id) ;               

		$member['mb_tel_1'] = substr($member['TEL'], 0, 3) ;
        $member['mb_tel_2'] = substr($member['TEL'], 4, -5) ;
        $member['mb_tel_3'] = substr($member['TEL'], -4, 4) ;

        $member['mb_hp_1'] = substr($member['MOBILE'], 0, 3) ;
        $member['mb_hp_2'] = substr($member['MOBILE'], 4, -5) ;
        $member['mb_hp_3'] = substr($member['MOBILE'], -4, 4) ;

        $member['mb_birth'] = substr($member['JUMIN_NO'], 0, 6) ;

		$head = array('title' => '회원 정보 수정');
		$data = array_merge(array(
			'token' => $token
		), $member);

		widget::run('head', $head);
		$this->load->view('member/modify', $data);
		widget::run('tail');
	}

	function update() 
	{
		define('WIDGET_SKIN', 'main');

		check_token('member/confirm/qry/member.modify');

		/*if (IS_MANAGER)
		{
			$member = $this->Member_infor_model->get_member_info_by_id($this->input->post('mb_id')) ;
		}
		else
		{
			$member = unserialize(MEMBER);
		}*/ 

		$this->load->helper('chkstr');
		$config = array(
			array('field'=>'mb_tel_1', 'label'=>'전화 번호', 'rules'=>'trim|required|exact_length[3]|numeric'),
            array('field'=>'mb_tel_2', 'label'=>'전화 번호', 'rules'=>'trim|required|min_length[3]|max_length[4]|numeric'),
            array('field'=>'mb_tel_3', 'label'=>'전화 번호', 'rules'=>'trim|required|exact_length[4]|numeric'),
			array('field'=>'mb_hp_1', 'label'=>'휴대폰 번호', 'rules'=>'trim|required|exact_length[3]|numeric'),
            array('field'=>'mb_hp_2', 'label'=>'휴대폰 번호', 'rules'=>'trim|required|min_length[3]|max_length[4]|numeric'),
            array('field'=>'mb_hp_3', 'label'=>'휴대폰 번호', 'rules'=>'trim|required|exact_length[4]|numeric'),
			array('field'=>'mb_email', 'label'=>'이메일', 'rules'=>'trim|required|max_length[50]|valid_email'),
			array('field'=>'mb_addr1', 'lebel'=>'주소', 'rules'=>'trim|required'),
			array('field'=>'mb_addr2', 'lebel'=>'주소', 'rules'=>'trim|required'),
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) 
		{
			$this->_form($this->input->post('mb_id'));
		}
		else 
		{
			if (!IS_MEMBER)
				alert("로그인 되어 있지 않습니다.");
				
			if ( IS_MANAGER )
			{
				$this->Member_info_model->update();
				alert_parent_reload("수정되었습니다.");
			}
			else
			{
				$mb_id = $this->input->post('mb_id');

				if ($this->session->userdata('ss_mb_id') != $mb_id)
					alert("로그인된 정보와 수정하려는 정보가 틀리므로 수정할 수 없습니다.");
	            
				$this->Member_info_model->update() ;

				{
					echo "<html>
							<head>
								<title>회원정보수정</title>
								<meta http-equiv=\"content-type\" content=\"text/html; charset=".$this->config->item('charset')."\">
							</head>
							<body>
								<form name='fupdate' method='post' action='".RT_PATH."/member/modify'>
									<input type='hidden' name='mb_id' value='".$mb_id."'>
									<input type='hidden' name='mb_password' value='".$this->session->userdata('ss_tmp_password')."'>
									<input type='hidden' name='token' value='".get_token()."'>
								</form>
								<script language='JavaScript'>
									alert('회원 정보가 수정 되었습니다.');
									document.fupdate.submit();
								</script>
							</body>
						</html>";
				}
			}
		}
	}
}
?>