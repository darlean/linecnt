<?php
class Login extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();
        $this->load->helper('cookie');
		define('WIDGET_SKIN', 'main');
	}

	function index() 
	{
		$this->qry();
	}

	function non_masking()
	{
		if (IS_MEMBER)
			goto_url(URL);
		
		$url = URL;
		$reId = get_cookie('ck_mb_id');

		$head = array('title' => '로그인');
		$data = array(
			'url'      => $url,
			'msg'      => FALSE,
			'reId'     => $reId,
			'chk_reId' => $reId ? 1 : 0,
			'non_masking' => TRUE,
		);
		
		widget::run('head', $head);
		$this->load->view('member/login', $data);
		widget::run('tail');
	}

	function qry($msg=FALSE) 
	{
		if (IS_MEMBER)
			goto_url(URL);

		if ($this->input->post('url'))
			$url = $this->input->post('url');
		else
			$url = (is_numeric($msg)) ? URL : urldecode(str_replace('.', '%', $msg));

		$reId = get_cookie('ck_mb_id');

		$head = array('title' => '로그인');
		$data = array(
			'url'      => $url,
			'msg'      => ($msg == 1) ? TRUE : FALSE,
			'reId'     => $reId,
			'chk_reId' => $reId ? 1 : 0
		);
		
		widget::run('head', $head);
		$this->load->view('member/login', $data);
		widget::run('tail');
	}

	function in() 
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules(array(
			array('field'=>'mb_id', 'label'=>'아이디', 'rules'=>'trim|required|min_length[3]|max_length[20]|alpha_dash|xss_clean'),
			array('field'=>'mb_password', 'label'=>'비밀번호', 'rules'=>'trim|required')
		));

		if ($this->form_validation->run() !== FALSE)
		{
			$mb = $this->Basic_model->get_login($this->input->post('mb_id'), $this->input->post('mb_password'));

			if ( !$mb || ($mb['PASSWD'] != $mb['PASSWD2']))
				goto_url('member/login/qry/1');

			if ( $mb['ISCLASS'] == 'X' )
				alert('사업 철회 회원입니다.', '/') ;

			$this->session->set_userdata('ss_mb_id', $mb['USERID']);
			$this->session->set_userdata('USERNAME', $mb['USERNAME']);
			$this->session->set_userdata('RANK_NAME', $mb['RANK_NAME']);
			//$this->session->set_userdata('KT_REG_NO', $mb['KT_REG_NO']);
			//$this->session->set_userdata('SKT_REG_NO', $mb['SKT_REG_NO']);
			$this->session->set_userdata('LGU_REG_NO', $mb['LGU_REG_NO']);
			$this->session->set_userdata('KAIT_NO', $mb['KAIT_NO']);
			$this->session->set_userdata('LGU_REG_CHECK', $mb['LGU_REG_CHECK']);
	        $this->session->set_userdata('SKT_REG_CHECK', $mb['SKT_REG_CHECK']);
	        $this->session->set_userdata('KT_REG_CHECK', $mb['KT_REG_CHECK']);

			if ( $mb['IS_RECEPTION'] == 'O' )
			{
				$this->session->set_userdata('is_reception', TRUE);				
			}
			
			if ($this->input->post('chk_reId')) 
			{
				$cookie = array(
				   'name'   => 'ck_mb_id',
				   'value'  => $mb['USERID'],
				   'expire' => 86400*30,
				   'domain' => $this->config->item('cookie_domain')
			   );
			   set_cookie($cookie);
			}
			else if (get_cookie('ck_mb_id'))
			{
				delete_cookie('ck_mb_id');
			}

			goto_url($this->input->post('url'));
		}
		
		goto_url('/');
	}

	function out() 
	{
		if (IS_MEMBER) 
		{
			$this->session->sess_destroy();
			//delete_cookie('ck_mb_id');
		}
		goto_url('/');
	}
}
?>