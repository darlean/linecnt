<?php
class Confirm extends CI_Controller {
	function __construct() {
		parent::__construct();
		define('WIDGET_SKIN', 'main');
	}

	function qry($aurl) 
	{
		if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg   =& $this->seg;
        $type  = $seg->get('member.tree_modify', '');

		$this->session->unset_userdata('ss_tmp_password');
		$head = array('title' => '회원 비밀번호 확인');
		$data = array(
			'token'  => get_token(),
			'mb_id'  => $this->session->userdata('ss_mb_id'),
			'action' => RT_PATH.'/'.str_replace('.', '/', $aurl)
		);

        if ( $type != "" )
        {
            $data['action'] .= "/".$type ;
        }

		widget::run('head', $head);
		$this->load->view('member/confirm', $data);
		widget::run('tail');
	}
}
?>