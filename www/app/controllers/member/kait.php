<?php
class Kait extends CI_Controller {
	function __construct() {
		parent::__construct();		

		$this->load->model(array('HB_temp_kait_model', 'Member_info_model'));

		define('WIDGET_SKIN', 'main');
		define('CSS_SKIN', 'jquery');

		//$this->output->enable_profiler(TRUE);
	}

    function insert()
    {
    	if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

		$this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소   
        $seg      =& $this->seg;    
        $userid  = $seg->get('insert'); 

        $head = array('title' => "사전승낙등록");
        $data = $this->HB_temp_kait_model->get_info($userid) ;

        $data['USERNAME'] = $this->Member_info_model->get_member_name_by_id($userid, true) ;

        widget::run('head', $head);
        $this->load->view('member/kait', $data);
        widget::run('tail');
    }

    function update()
    {    	
    	if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

    	$LINK_PATH = $this->input->post('LINK_PATH') ;
    	$KAIT_NO = $this->input->post('KAIT_NO') ;
    	$USERID = $this->input->post('USERID') ;

    	if ( ( substr($LINK_PATH, -10) != substr($KAIT_NO, -10) ) || $this->HB_temp_kait_model->is_inserted_kaitno($KAIT_NO, $USERID) > 0 )
    	{
    		alert("정확한 정보를 확인 후 입력 하여 주시기 바랍니다.", 'member/kait/insert/'.$USERID);
    	}
    	else
    	{
    		if ( $this->HB_temp_kait_model->update() )
    		{
    			alert("저장되었습니다.", 'member/kait/insert/'.$USERID) ;
    		}
    	}
    }

    function allow()
    {
    	if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

		$this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소
        $param    =& $this->param;
        $userid  = $param->get('userid'); 

        if ( $this->HB_temp_kait_model->update_real_kait_info($userid) != null )
		{
			alert("승인되었습니다.", 'member/kait/lists') ;
		}
		else
		{
			alert("승인이 실패하였습니다.", 'member/kait/insert/'.$userid) ;
		}
    }

    function reject()
    {
    	if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

		$this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소
        $param    =& $this->param;
        $userid  = $param->get('userid'); 
        $reject_msg  = $param->get('reject_msg'); 

        if ( $this->HB_temp_kait_model->reject($userid, $reject_msg) )
		{
			alert("반려되었습니다.", 'member/kait/lists') ;
		}
		else
		{
			alert("반려가 실패하였습니다.", 'member/kait/insert/'.$userid) ;
		}
    }

    function lists()
    {
    	if (!IS_MANAGER)
			alert('잘못된 접근입니다.', '/');

		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소		

		$seg      =& $this->seg;		
		
		$tab  = $seg->get('tab', 0); 
		$page  = $seg->get('page', 1); // 페이지		
		
		$total_count = $this->HB_temp_kait_model->list_count($tab) ;
		
		$config['per_page']    = 10;
		$config['total_rows']  = $total_count;
		$config['uri_segment'] = $seg->pos('page');		
		$config['base_url']    = RT_PATH.'/member/kait/lists/tab/'.$tab.'/page/';

		$CI =& get_instance();
		$CI->load->library('pagination', $config);
		
		$limit = $config['per_page'] ;
		$offset = ($page - 1) * $config['per_page'];
		
		$result = $this->HB_temp_kait_model->list_result($limit, $offset, $tab) ;

		// 일반 리스트
		$list = array();
		foreach ($result as $i => $row) 
		{
			$list[$i]                       = $row ;
			$list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
			$list[$i]['href']               = '/member/kait/insert/'.$row['USERID'];			
		}		

		$data = array(
				'total_count' 	=> $total_count,  				
				'list' 			=> $list,
				'paging' 		=> $CI->pagination->create_links(),  								
				'tab_list' 		=> array("미승인", "반려"),
				'tab'       	=> $tab,
		);		

        $head = array('title' => "사전승낙등록");        

        widget::run('head', $head);
        $this->load->view('member/kait_list', $data);
        widget::run('tail');
    }
}
?>