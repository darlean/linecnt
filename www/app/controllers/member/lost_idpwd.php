<?php
class Lost_idpwd extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->config->load('cf_register');
		$this->load->model('Member_forget_model');
		define('WIDGET_SKIN', 'main');
	}

	function index() {
		if (IS_MEMBER)
			alert('이미 로그인 중입니다.');

		$head = array('title' => '아이디 / 비밀번호 찾기');
		$data = array();

		widget::run('head', $head);
		$this->load->view('member/lost_idpwd', $data);
		widget::run('tail');
	}

	function result()
	{
		if (!$this->input->post('w'))
			goto_url('/');
				
		$mb = $this->Member_forget_model->check() ;

		if (isset($mb['mb_id'])) 
		{
			if ($mb['mb_id'] == ADMIN)
		    	alert('관리자 아이디는 접근 불가합니다.') ;
		}
		else
		{
		    alert('입력하신 내용으로는 회원정보가 존재하지 않습니다.', 'member/lost_idpwd') ;
		}

		$not_mb_id = $this->input->post('not_mb_id');

		if ($not_mb_id)
		{
			$this->result_find_id($mb['mb_id']) ;
		}
		else if (!$not_mb_id || $this->session->flashdata('mb_idpwd'))
		{
			$this->result_find_pwd($mb['mb_id']) ;
		}
	}

	private function result_find_id($in_mb_id)
	{
		$head = array('title' => '아이디 찾기 결과');
		$data = array('mb_id' => $in_mb_id);

		widget::run('head', $head);
		$this->load->view('member/result_id', $data);
		widget::run('tail');
	}

	private function result_find_pwd($in_mb_id)
	{
		$this->session->set_flashdata('mb_idpwd', $in_mb_id) ;

		// 난수 발생
		list($usec, $sec) = explode(' ', microtime());
		$seed = (float)$sec + ((float)$usec * 100000);
		srand($seed);
		$change_pwd = substr(md5($seed), 0, rand(4, 6));

		$this->Member_forget_model->new_pwd($change_pwd);

		$head = array('title' => '비밀번호 찾기 결과');
		$data = array(
			'mb_id' => $in_mb_id,
			'change_pwd' => $change_pwd
		);

		widget::run('head', $head);
		$this->load->view('member/result_pwd', $data);
		widget::run('tail');
	}
}
?>