<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Honor extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('Pay_model', 'S_Rank_model'));
        $this->load->config('cf_position') ;

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'index':
            case 'pay_date':
                $this->_index();
                break;
            default:
                show_404();
                break;
        }
    }

    function _index()
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $pay_date_list = $this->Pay_model->get_pay_date_list() ;
        $pay_date  = $seg->get('pay_date', $pay_date_list[0]['PAY_DATE']) ;

        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜

        $list = $this->Pay_model->get_honor_list($pay_date) ;

        $data = array(
            'list' => $list,
            'pay_date_list' => $pay_date_list,
            'pay_date'  => $pay_date,

            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜

            'rank_cd_list'  => array('31', '41', '51', '61'),
            'rank_list'     => $this->S_Rank_model->get_rank_list(),
            'mb_position_color' => $this->config->item('mb_position_color'),
        );

        $head = array('title' => '명예의전당') ;
        widget::run('head', $head);
        $this->load->view('member/honor', $data);
        widget::run('tail');
    }
}
?>