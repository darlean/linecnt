<?php
class Join extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->config->load('cf_register');
		$this->load->library('form_validation');
		$this->load->model(array('Member_info_model', 'S_Center_model', 'S_Bank_model', 'HB_join_temp_line_model'));
		$this->load->helper(array('form', 'url', 'sms')) ;

		define('WIDGET_SKIN', 'main');
		define('CSS_SKIN', 'jquery');

		//$this->output->enable_profiler(TRUE);
	}

	private function _check_cert()
	{
		if(!extension_loaded('CPClient')) 
        {
            dl('CPClient.'.PHP_SHLIB_SUFFIX);
        }
        $module = 'CPClient';

        session_start() ;        
        
        $data['sReserved1'] = '' ;
        $data['sReserved2'] = '' ;
        $data['sReserved3'] = '' ;
        
        $nice_param['sitecode'] = "G8979" ;             // NICE로부터 부여받은 사이트 코드
        $nice_param['sitepasswd'] = "TLZSMV3RQI8R" ;           // NICE로부터 부여받은 사이트 패스워드
        
        $nice_param['cb_encode_path'] = "/www/data/certify/CPClient" ;     // NICE로부터 받은 암호화 프로그램의 위치 (절대경로+모듈명)
        
        $nice_param['authtype'] = "M" ;         // 없으면 기본 선택화면, X: 공인인증서, M: 핸드폰, C: 카드
            
        $nice_param['popgubun'] = "N" ;      //Y : 취소버튼 있음 / N : 취소버튼 없음
        $nice_param['customize']  = "" ;     //없으면 기본 웹페이지 / Mobile : 모바일페이지
        
        //$nice_param['reqseq'] = "REQ_0123456789" ;     // 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로
                                        // 업체에서 적절하게 변경하여 쓰거나, 아래와 같이 생성한다.
        //$nice_param['reqseq'] = `{$nice_param['cb_encode_path']} SEQ {$nice_param['sitecode']}` ;

        if (extension_loaded($module)) 
        {// 동적으로 모듈 로드 했을경우
            $reqseq = get_cprequest_no($nice_param['sitecode']);
        } 
        else 
        {
          $reqseq = "Module get_request_no is not compiled into PHP";
        }
        
        // CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
        $nice_param['returnurl'] = "http://".$_SERVER['HTTP_HOST']."/cert/cert_success";    // 성공시 이동될 URL
        $nice_param['errorurl'] = "http://".$_SERVER['HTTP_HOST']."/cert/cert_fail" ;        // 실패시 이동될 URL
        
        // reqseq값은 성공페이지로 갈 경우 검증을 위하여 세션에 담아둔다.
        
        //$_SESSION["REQ_SEQ"] = $nice_param['reqseq'] ;
        $_SESSION["REQ_SEQ"] = $reqseq;

        // 입력될 plain 데이타를 만든다.
        $plaindata =  "7:REQ_SEQ" . strlen($reqseq) . ":" . $reqseq .
                                  "8:SITECODE" . strlen($nice_param['sitecode']) . ":" . $nice_param['sitecode'] .
                                  "9:AUTH_TYPE" . strlen($nice_param['authtype']) . ":". $nice_param['authtype'] .
                                  "7:RTN_URL" . strlen($nice_param['returnurl']) . ":" . $nice_param['returnurl'] .
                                  "7:ERR_URL" . strlen($nice_param['errorurl']) . ":" . $nice_param['errorurl'] .
                                  "11:POPUP_GUBUN" . strlen($nice_param['popgubun']) . ":" . $nice_param['popgubun'] .
                                  "9:CUSTOMIZE" . strlen($nice_param['customize']) . ":" . $nice_param['customize'] ;
        
        //$enc_data = `{$nice_param['cb_encode_path']} ENC {$nice_param['sitecode']} {$nice_param['sitepasswd']} $plaindata`;
        //$enc_data = get_encode_data($sitecode, $sitepasswd, $plaindata);                                 
        if (extension_loaded($module)) {// 동적으로 모듈 로드 했을경우
            $enc_data = get_encode_data($nice_param['sitecode'], $nice_param['sitepasswd'], $plaindata);
        } else {
          $enc_data = "Module get_request_data is not compiled into PHP";
        }

        //var_dump($enc_data);

        $returnMsg = '' ;
        if( $enc_data == -1 )
        {
            $returnMsg = "암/복호화 시스템 오류입니다.";
            $enc_data = "";
        }
        else if( $enc_data== -2 )
        {
            $returnMsg = "암호화 처리 오류입니다.";
            $enc_data = "";
        }
        else if( $enc_data== -3 )
        {
            $returnMsg = "암호화 데이터 오류 입니다.";
            $enc_data = "";
        }
        else if( $enc_data== -9 )
        {
            $returnMsg = "입력값 오류 입니다.";
            $enc_data = "";
        }

        $data['returnMsg'] = $returnMsg ; 
        $data['enc_data'] = $enc_data ;     

        return $data;
	}

	private function _check_ipin()
	{
		$nice_param['sitecode'] = "L596" ;             	   // NICE로부터 부여받은 사이트 코드
        $nice_param['sitepasswd'] = "78221612" ;           // NICE로부터 부여받은 사이트 패스워드		

        $nice_param['returnurl'] = "http://".$_SERVER['HTTP_HOST']."/cert/ipin_success";    // 성공시 이동될 URL
			
		$sCPRequest					= "";			// 하단내용 참조
		
		//if (extension_loaded($module)) {// 동적으로 모듈 로드 했을경우
			$sCPRequest = get_request_no($nice_param['sitecode']) ;
		//} else {
		//	$sCPRequest = "Module get_request_no is not compiled into PHP";
		//}
		
		// 현재 예제로 저장한 세션은 ipin_result.php 페이지에서 데이타 위변조 방지를 위해 확인하기 위함입니다.
		// 필수사항은 아니며, 보안을 위한 권고사항입니다.
		$_SESSION['CPREQUEST'] = $sCPRequest;
	    
	    $sEncData					= "";			// 암호화 된 데이타
		$sRtnMsg					= "";			// 처리결과 메세지
		
	    // 리턴 결과값에 따라, 프로세스 진행여부를 파악합니다.
	  
		//if (extension_loaded($module)) {/ 동적으로 모듈 로드 했을경우
			$sEncData = get_request_data($nice_param['sitecode'], $nice_param['sitepasswd'], $sCPRequest, $nice_param['returnurl']);
		//} else {
		//	$sEncData = "Module get_request_data is not compiled into PHP";
		//}
	    
	    // 리턴 결과값에 따른 처리사항
	    if ($sEncData == -9)
	    {
	    	$sRtnMsg = "입력값 오류 : 암호화 처리시, 필요한 파라미터값의 정보를 정확하게 입력해 주시기 바랍니다.";
	    } 
	    else 
	    {
	    	$sRtnMsg = "$sEncData 변수에 암호화 데이타가 확인되면 정상, 정상이 아닌 경우 리턴코드 확인 후 NICE평가정보 개발 담당자에게 문의해 주세요.";
	    }

        $data['ipin_returnMsg'] = $sRtnMsg ; 
        $data['ipin_enc_data'] = $sEncData ;     

        return $data;	    
	}

	function index() 
	{
		if (IS_MEMBER)
			goto_url('/');

		$this->form_validation->set_rules(array(
			array('field'=>'agree', 'label'=>'회원가입약관', 'rules'=>'required'),
			array('field'=>'agree2', 'label'=>'개인정보취급방침', 'rules'=>'required')
		));

		if ( IS_TEST_SERVER )
		{
			$this->form(false);

			return;
		}

		if ($this->form_validation->run() == FALSE) 
		{
			$this->load->helper('file');

			/*$cert = $this->_check_cert();
			if( $cert == "" )
			{
				alert("본인인증 서비스가 일시적으로 점검중입니다.\n빠른시일내에 복구하도록 하겠습니다.");
			}

			$ipin = $this->_check_ipin();
			if( $ipin == "" )
			{
				alert("아이핀 인증 서비스가 일시적으로 점검중입니다.\n빠른시일내에 복구하도록 하겠습니다.");
			}*/

			$head = array('title' => '회원가입 약관동의');
			$data = array(
				'privacy'	  => read_file(SKIN_PATH.'/member/privacy.txt'),
				'stipulation' => read_file(SKIN_PATH.'/member/stipulation.txt')
			);

			/*$data = array_merge($data, $cert);
			$data = array_merge($data, $ipin);*/

			widget::run('head', $head);
			$this->load->view('member/join_check', $data);
			widget::run('tail');
		}
		else
		{
			if ( IS_TEST_SERVER )
			{
				$this->form(false);
			}
			else
			{
				redirect('/cert/cert_join');	
			}				
		}
	}

	################################################################################################
	// sock_post() 함수 시작
	################################################################################################
	function sock_post($host,$target,$posts,$cookies,$referer='',$port=443) 
	{ 
		//var_dump($posts);
		$postValues = '';
        if(is_array($posts)) 
        { 
            foreach($posts AS $name=>$value) 
            {
            	$postValues .= urlencode($name) . "=" . urlencode($value) . '&'; 
            }

            //var_dump($postValues);
            $postValues = substr($postValues, 0, -1); 
        } 

        

	
        $postLength = strlen($postValues); 

		$cookieValues = '';
        if(is_array($cookies)) 
        { 
            foreach($cookies AS $name=>$value) 
            {
            	$cookieValues .= urlencode($name) . "=" . urlencode($value) . ';'; 
            }
            $cookieValues = substr($cookieValues, 0, -1); 
        } 

        $request  = "POST $target HTTP/1.1\r\n"; 
        $request .= "Host: $host\r\n"; 
        $request .= "Content-Type: application/x-www-form-urlencoded\r\n"; 
        $request .= "Content-Length: " . $postLength . "\r\n"; 
        $request .= "Connection: close\r\n"; 
        $request .= "\r\n"; 
        $request .= $postValues; 

        $ret = ''; 
        $socket  = fsockopen("ssl://".$host, $port, $errno, $errstr, 10); // 소켓 타임아웃 10초
		
		if ( ! $socket )
		{			
			return false;
		}
		
        fputs($socket, $request); 
        	while(!feof($socket)) $ret .= fgets($socket, 1024); 
        fclose($socket);         

        return $ret; 
    } 
	//################################################################################################

	/*function bank_account()
	{
		if (IS_MEMBER)
			goto_url('/');

		$is_certification = $this->session->flashdata('is_certification');
		$cert_name = $this->session->flashdata('cert_name');
		$cert_birth = $this->session->flashdata('cert_birth');

		if( $is_certification == FALSE )
			alert('본인 인증 후 이용하세요.', '/');
		
		if( $cert_name == FALSE )
			alert('본인 인증 후 이용하세요.', '/');

		
		if( $cert_birth == FALSE )
			alert('본인 인증 후 이용하세요.', '/');

		//$cert_name = "홍성기";

		$this->bank_account2($cert_name) ;
	}

	function bank_account2($cert_name)
	{
		$head = array('title' => '회원 계좌확인');

		$data = array(
			//'mb_name'             => $this->input->post('mb_name'),
			'mb_name'             => $cert_name,
			'bank_list'           => $this->S_Bank_model->get_bank_list(),
			'cert'				  => 0,
		);

		widget::run('head', $head);
		$this->load->view('member/bank_account', $data);
		widget::run('tail');
	}*/

	function bank_account()
	{
		$head = array('title' => '회원 계좌확인');

		$data = array(
			'bank_list'           => $this->S_Bank_model->get_bank_list(),
			'cert'				  => 0,
		);

		widget::run('head', $head);
		$this->load->view('member/bank_account', $data);
		widget::run('tail');
	}

	function bank_account_request()
	{
		$posts = Array( // 포스트 
			//##################################################
			//###### ▣ 회원사 ID 설정   - 계약시에 발급된 회원사 ID를 설정하십시오. ▣
			//###### ▣ 회원사 PW 설정   - 계약시에 발급된 회원사 PASSWORD를 설정하십시오. ▣
			//###### ▣ 조회사유  설정   - 10:회원가입 20:기존회원가입 30:성인인증 40:비회원확인 90:기타사유 ▣
			//###### ▣ 개인/사업자 설정 - 1:개인 2:사업자 ▣
			//##################################################
			"niceUid"=>"Nhbn00",						// 나이스평가정보에서 고객사에 부여한 구분 id
			"svcPwd"=>"mFaOqWUv",						// 나이스평가정보에서 고객사에 부여한 서비스 이용 패스워드
			"inq_rsn"=>"10",							// 조회사유 - 10:회원가입 20:기존회원가입 30:성인인증 40:비회원확인 90:기타사유
			"strGbn"=>"1",								// 1 : 개인, 2: 사업자
			//##################################################
			//###### 위의 값을 알맞게 수정해 주세요.
			//##################################################
			"strResId"=>$_POST["mb_birth"],					// 생년월일
			"strNm"=>$_POST["USERNM"],							// 이름
			"strBankCode"=>$_POST["strBankCode"],		// 은행코드
			"strAccountNo"=>$_POST["strAccountNo"],	// 계좌번호
			"service"=>$_POST["service"],						// 서비스구분
			"svcGbn"=>$_POST["svcGbn"],							// 업무구분
			"svc_cls"=>$_POST["svc_cls"],						// 내/외국인 구분
			"strOrderNo"=>date("Ymd") . rand(1000000000,9999999999),			//주문번호 : 매 요청마다 중복되지 않도록 유의(수정불필요)
		);

		$cookies = array();		// 쿠키
		$referer = "";				// 리퍼러

		$host = "secure.nuguya.com";																														// 호스트 
		//$page = "https://secure.nuguya.com/nuguya/service/realname/sprealnameactconfirm.do";		// URL
		$page = "https://secure.nuguya.com/nuguya2/service/realname/sprealnameactconfirm.do";		// UTF-8 URL

		$retval = $this->sock_post($host,$page,$posts,$cookies,$referer); 


		
		if( $retval == false) 
		{
			/*$retOrderNo = $posts["strOrderNo"];
			echo "주문번호   : " . $retOrderNo . "<br>";
			echo "응답코드   : E999<br>";
			echo "응답메시지 : 소켓연결에 실패하였습니다.<br>";*/
			alert( '소켓연결에 실패하였습니다.' );
		}
		else
		{
			// 결과값 처리
			$NewInfo = explode("\r\n", $retval);			// 헤더정보에서 뉴라인 체크
			$infoValue = explode("|",$NewInfo[8]);		// 9번째 줄의 결과값만 가져온다.
		
			
			//echo $NewInfo[8];
		
			$OrderNum	= $infoValue[0];		// 주문번호
			$ResultCD	= $infoValue[1];		// 결과코드
			$Msg		= $infoValue[2];		// 메세지
			
			/*echo "주문번호   : " . $OrderNum . "<br>";
			echo "응답코드   : " . $ResultCD . "<br>";
			echo "응답메시지 : " . $Msg . "<br>";*/

			$this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소
			$param    =& $this->param ;				
			$is_only_cert   = $param->get('cert');

			if ( $is_only_cert )
			{
				if( $ResultCD == '0000' )
				{
					alert_close('계좌확인완료') ;
				}
				else
				{
					alert( $infoValue[2] );	
				}
			}	
			else
			{
				if( $ResultCD == '0000' )
				{				
					session_start();

					$this->session->set_flashdata('USERNM', 		$_POST["USERNM"]) ;
					$this->session->set_flashdata('mb_birth', 		$_POST['mb_birth']) ;
	  				$this->session->set_flashdata('strBankCode', 	$_POST['strBankCode']) ;
	  				$this->session->set_flashdata('strAccountNo', 	$_POST['strAccountNo']) ;  				

					alert( $infoValue[2], 'member/join/form' );
				}
				else
				{
					alert( $infoValue[2] );	
					$this->bank_account2($_POST["USERNM"]) ;
				}
				//redirect('/member/form/'.$_POST["USERNM"].'/'.$_POST["strAccountNo"]);
			}
		}
	}

	function form( $is_first = TRUE ) 
	{
		if (IS_MEMBER)
			goto_url('/');

		//if( $is_first )
		{
			/*$USERNM 		= $this->session->flashdata('USERNM');
            $mb_birth 		= $this->session->flashdata('mb_birth');
			$strBankCode 	= $this->session->flashdata('strBankCode');
			$strAccountNo 	= $this->session->flashdata('strAccountNo');*/

			//var_dump($USERNM);

			/*if( $USERNM == FALSE )
				alert('잘못된 접근입니다..', '/');

			if( $JUMINNO == FALSE )
				alert('잘못된 접근입니다..', '/');
			
			if( $strBankCode == TRUE )
				alert('잘못된 접근입니다..', '/');

			
			if( $strAccountNo == TRUE )
				alert('잘못된 접근입니다..', '/');*/
		}
		//else
		{
			$USERNM 		= $this->input->post('mb_name');
			$mb_birth		= $this->input->post('mb_birth');
			$strBankCode 	= $this->input->post('mb_bank');
			$strAccountNo 	= $this->input->post('mb_bank_serial');
		}
		

		$token = get_token();

		$head = array('title' => '회원 가입');
		$data = array(
			//'mb_name'             => $this->input->post('mb_name'),
			'mb_name'             => $USERNM,
			'mb_birth'            => $mb_birth,
			'mb_tel_1'            => $this->input->post('mb_tel_1'),
			'mb_tel_2'            => $this->input->post('mb_tel_2'),
			'mb_tel_3'            => $this->input->post('mb_tel_3'),
			'mb_hp_1'             => $this->input->post('mb_hp_1'),
			'mb_hp_2'             => $this->input->post('mb_hp_2'),
			'mb_hp_3'             => $this->input->post('mb_hp_3'),
			'mb_email'            => $this->input->post('mb_email'),
			'mb_email'            => $this->input->post('is_mailing'),
			'mb_zonecode'         => $this->input->post('mb_zonecode'),			
			'mb_addr1'            => $this->input->post('mb_addr1'),
			'mb_addr2'            => $this->input->post('mb_addr2'),
			
			'mb_bank'             => $strBankCode,
			'mb_bank_owner'       => $USERNM,
			'mb_bank_serial'      => $strAccountNo,
			'mb_owner_relation'	  => $this->input->post('mb_owner_relation'),
			
			'mb_supporter_id'     => $this->input->post('mb_supporter_id'),
			'mb_recommander_id'   => $this->input->post('mb_recommander_id'),
			'mb_center'           => $this->input->post('mb_center'),
			
			'center_list'         => $this->S_Center_model->get_center_list(),
			'bank_list'           => $this->S_Bank_model->get_bank_list(),
			
			'token'               => $token,
			'todays'              => date("Ymd", time())
		);

		widget::run('head', $head);
		$this->load->view('member/join', $data);
		widget::run('tail');
	}

	function update() {
		check_token('member/join');

		$this->load->helper('chkstr');	
		$config = array(
			array('field'=>'mb_name', 'label'=>'이름', 'rules'=>'trim|required|max_length[20]|callback_mb_name_check'),
			array('field' =>'mb_birth', 'label'=>'생년월일', 'rules'=>'trim|exact_length[6]|required'),
			array('field' =>'mb_password', 'label'=>'비밀번호', 'rules'=>'trim|required|max_length[20]'),
			array('field' =>'mb_password_re', 'label'=>'비밀번호 확인', 'rules'=>'trim|required|max_length[20]|matches[mb_password]'),
			array('field' =>'mb_hp_1', 'label'=>'휴대폰 번호', 'rules'=>'trim|required|exact_length[3]|numeric'),
			array('field' =>'mb_hp_2', 'label'=>'휴대폰 번호', 'rules'=>'trim|required|min_length[3]|max_length[4]|numeric'),
			array('field' =>'mb_hp_3', 'label'=>'휴대폰 번호', 'rules'=>'trim|required|exact_length[4]|numeric'),
			array('field' =>'mb_addr1', 'lebel'=>'주소', 'rules'=>'trim|required'),
			array('field' =>'mb_addr2', 'lebel'=>'주소', 'rules'=>'trim|required'),
			array('field' =>'mb_bank', 'lebel'=>'은행명', 'rules'=>'trim|required'),
			array('field' =>'mb_bank_owner', 'lebel'=>'예금주', 'rules'=>'trim|required'),
			array('field' =>'mb_bank_serial', 'lebel'=>'계좌번호', 'rules'=>'trim|required|numeric'),
			array('field' =>'mb_center', 'lebel'=>'가입센터', 'rules'=>'trim|required'),			
		);

		$this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) 
        {
            $this->form( FALSE );
        }
        else 	
        {
        	// 회원 INSERT
        	$result = $this->Member_info_model->insert() ;

        	$mb_id = '' ;

        	if ( $result->STATUS == '1' )
        	{
        		$mb_id = $result->NEW_USERID ;
        		$this->session->set_userdata('ss_mb_id', $mb_id);

                $this->HB_join_temp_line_model->insert($mb_id) ;

        		if ( !IS_TEST_SERVER )
    			{
	        		$mb_name = $this->input->post('mb_name') ;
	        		$mb_password = $this->input->post('mb_password') ;
					$mb_mobile = $this->input->post('mb_hp_1').$this->input->post('mb_hp_2').$this->input->post('mb_hp_3') ;				

	    			$sms_subject = "주식회사 라인커넥션 회원가입을 축하드립니다." ;
					$sms_msg = "회원이름 : ".$mb_name."\n회원계정 : ".$mb_id."\n비밀번호 : ".$mb_password."\n웹페이지 : http://www.linecnt.com/" ;

					//sms_sender($sms_msg, $mb_mobile, "18995990", $sms_subject, "L") ;
				}		
        	}
        	else
        	{
        		alert( $result->MESSAGE ) ;
        		$this->form( FALSE );
        	}

            $this->session->set_flashdata('ss_mb_reg', $mb_id);

            goto_url('member/join/result');
        }
	}

	function result() 
	{
		if (!$this->session->flashdata('ss_mb_reg'))
			goto_url('/');

		$mb = $this->Basic_model->get_member($this->session->flashdata('ss_mb_reg'), "USERID,USERNAME") ;
	
		// 회원정보가 없다면 초기 페이지로 이동
		if (!$mb)
			goto_url('/');

		$head = array('title' => '회원가입 결과');
		$data = array(
			'mb_id' 	=> $mb['USERID'],
			'mb_name' 	=> $mb['USERNAME']
		);

		widget::run('head', $head);
		$this->load->view('member/join_result', $data);
		widget::run('tail');
	}

	function check_jumin()
	{
		$mb_jumin_no = $this->input->post('mb_jumin_no') ;

        if ( $mb_jumin_no != '' )
        {
        	$result = $this->Member_info_model->check_jumin() ;

        	if ( $result )
        	{        		
        		if ( $result->STATUS == 1 )
        		{
        			echo 'OK' ;
        		}
        		else
        		{
        			echo $result->MESSAGE ;      			
        		}
        		
        		return TRUE ;
        	}
        }

        echo 'NULL' ;
    	return FALSE ;
	}

	function find_member()
    {
        //$mb_name = $this->input->post('mb_name') ;
        $mb_id = $this->input->post('mb_id') ;        
        $find_supporter = $this->input->post('find_supporter') ;

        if ( /*$mb_name != '' &&*/ $mb_id != '' )
        {
        	$member = $this->Member_info_model->get_member_info_by_id(/*$mb_name,*/ $mb_id) ;

        	if ( $member )
        	{
        		if ( $find_supporter == 'true' )
        		{	
	        		$fillable_supporter = $this->Member_info_model->fillable_supporter($mb_id) ;

	        		if ( !$fillable_supporter )
	        		{
	        			echo 'CANT' ;
	        			return FALSE ;
	        		}
	        	}
	        	else
	        	{
	        		$mb_supporter_id = $this->input->post('mb_supporter_id') ;	        		

	        		$fillable_recommender = $this->Member_info_model->fillable_recommender($mb_id, $mb_supporter_id) ;

	        		if ( $fillable_recommender == 0 )
	        		{
	        			echo 'CANT' ;
	        			return FALSE ;
	        		}
	        	}

        		echo $member['USERNAME'] ;
        		return TRUE ;
        	}
        }

        echo 'NULL' ;
    	return FALSE ;
    }

    function find_supporter()
    {
        $mb_id = $this->input->post('mb_id') ;

        $result = array() ;
        $result['RST'] = 'NULL' ;

        if ( $mb_id != '' )
        {
            $member = $this->Member_info_model->get_member_info_by_id($mb_id) ;

            if ( $member )
            {
                $fillable_supporter = $this->Member_info_model->fillable_supporter2($mb_id) ;

                if ( $fillable_supporter == 2 )
                {
                    $result['RST'] = 'CANT' ;
                }
                else if ( $fillable_supporter == 1 )
                {
                    $result['RST'] = '' ;
                    $result['USERNAME'] = $member['USERNAME']." (B라인)" ;
                    $result['TEMP_LINE'] = 'B' ;
                }
                else
                {
                    $result['RST'] = '' ;
                    $result['USERNAME'] = $member['USERNAME']." (A라인)" ;
                    $result['TEMP_LINE'] = 'A' ;
                }
            }
        }

        echo json_encode($result) ;
    }

	function mb_name_check($str) 
	{
		/*if (!check_string($str, _RT_HANGUL_))
		{
			$this->form_validation->set_message('mb_name_check', '이름은 공백없이 한글만 입력 가능합니다.');
			return FALSE;
		}*/
		return TRUE;
	}

    function add_jumin_no()
    {
        $mb_jumin_no = $this->input->post('mb_jumin_no') ;
        $user_id = $this->session->userdata('ss_mb_id') ;

        if ( $mb_jumin_no != '' && $user_id != '' )
        {
            $member = $this->Basic_model->get_member($user_id, 'JUMIN_NO') ;

            if ( substr($member['JUMIN_NO'], 0, 6) != substr($mb_jumin_no, 0, 6)  )
            {
                alert('주민등록번호를 확인해 주시기 바랍니다.', './member/certify/add_jumin_no/');
            }
            else
            {
                $result = $this->Member_info_model->check_jumin() ;

                if ( $result && $result->STATUS == 1 )
                {
                    $result = $this->Member_info_model->add_jumin_no($user_id, $mb_jumin_no);

                    if ($result) {
                        alert_close('저장되었습니다.');
                    } else {
                        alert('저장이 실패하였습니다. 다시 시도 해 주세요.', './member/certify/add_jumin_no/');
                    }
                }
                else
                {
                    alert($result->MESSAGE, './member/certify/add_jumin_no/');
                }
            }
        }
        else
        {
            alert_close('잘못된 접근입니다.') ;
        }
    }
}
?>