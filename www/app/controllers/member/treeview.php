<?php
class Treeview extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();
		$this->load->model(array('member_tree_model', 'Member_info_model', 'S_Rank_model', 'OrderMaster_model', 'S_Center_model'));
		$this->load->config('cf_position') ;
		
		define('CSS_SKIN', 'jquery');

		//$this->output->enable_profiler(TRUE);
	}

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {            
            case 'index':
            case 'user_kind':
                define('WIDGET_SKIN', 'main');
                $this->index();
            break;
            case 'fullscreen':
                $this->fullscreen() ;
            break;
            case 'get_member_info':
                $this->get_member_info() ;
            break ;
            case 'get_name_list':
                $this->get_name_list() ;
            break ;
            case 'parser':
                $this->parser() ;
            break ;
            default:
                show_404();
            break;
        }
    }

	function index() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg');
        $seg      =& $this->seg;
        $user_kind = $seg->get('user_kind', 'P');   

        $head = array('title' => '회원트리조회');

        $tree_version = $this->setTreeVersion() ;

        widget::run('head', $head);

        if ( $tree_version )
        {            
            $data = $this->_data($user_kind, false, 5) ;
            $data['is_mobile'] = $this->IsMobile() ;

            $this->load->view('member/treeview', $data);
        }
        else
        {
            $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소   
            $param    =& $this->param;      

            $data = $this->_data($user_kind, false, -1) ; 
            $data['mb_list'] = json_encode($data['mb_list']) ;    
            $data['tree_version'] = $tree_version ;
            $data['path'] = $param->get('path', 5) ;            

            $this->load->view('member/treeview_v2', $data);
        }

        widget::run('tail');
    }

    function test1() 
    {
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소        
        $this->load->library('segment', array('offset'=>4), 'seg');

        $param    =& $this->param;  
        $seg      =& $this->seg;

        $user_kind = $seg->get('user_kind', 'P');           

        $head = array('title' => '회원트리조회');
        $data = $this->_data($user_kind, true, -1) ;
        $data['mb_list'] = json_encode($data['mb_list']) ;    
        $data['tree_version'] = $this->setTreeVersion() ;

        $data['path'] = $param->get('path', 5) ;            

        widget::run('head', $head);
        $this->load->view('member/test', $data);
        widget::run('tail');
    }    

    function fullscreen() 
    {
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소        
        $this->load->library('segment', array('offset'=>4), 'seg');

        $param    =& $this->param;  
        $seg      =& $this->seg;

        $user_kind = $seg->get('user_kind', 'P');    

        $head = array('title' => '회원트리조회(전체화면)');
        $data = $this->_data($user_kind, true, -1) ;
        $data['mb_list'] = json_encode($data['mb_list']) ;    
        $data['tree_version'] = $this->setTreeVersion() ;

        $data['path'] = $param->get('path', 5) ;            

        widget::run('head', $head);
        $this->load->view('member/fullscreen_treeview', $data);
        widget::run('tail');
    }

    private function _data($in_user_kind, $is_fullscreen, $in_path = 0)
	{        
		$this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소        

		$param    =& $this->param;        

        $qstr  	= $param->output();
        $s_path = $param->get('s_path') ;
        $path 	= $param->get('path', $in_path) ;
        $name  	= $param->get('name'); 
        $mb_id  = $param->get('mb_id'); 

        if ( $in_path == -1 )
        {
            $path = 0 ;
        }
        
        $CI =& get_instance();

        $center_list = $this->S_Center_model->get_center_list(true) ;
        $rank_list = $this->S_Rank_model->get_rank_list() ;

        //var_dump($center_list) ;

        if( IS_MANAGER )
        {
            if( $mb_id == '' )
            {
                $mb_id = 'D0000000' ;
            }
        }
        else
        {
            if ( $mb_id == '' )
            {
                $mb_id = $CI->session->userdata('ss_mb_id') ;
            }

            if ( $mb_id != $CI->session->userdata('ss_mb_id') )
            {
                $fcnt = $this->member_tree_model->is_member_in_my_sub_tree($mb_id, $CI->session->userdata('ss_mb_id')) ;

                if ( $fcnt == 0 )
                {
                    $mb_id = $CI->session->userdata('ss_mb_id') ;
                }
            }
        }            

		$mb_count_subtree = $this->member_tree_model->get_member_count_subtree($mb_id, $path, $in_user_kind).' 명' ;	
		$result = $this->member_tree_model->get_member_nodes_subtree($mb_id, $path, $in_user_kind) ;

         // 상위 검색 막기 위함 임시 코드 ㅠㅠ
        if ( !IS_MANAGER && ($mb_id == $CI->session->userdata('ss_mb_id')) )
        {
            $result[0]['P_ID'] = '' ;
            $result[0]['R_ID'] = '' ;
        }

        $tree_version = $this->setTreeVersion() ;

        $mb_position_color = $this->config->item('mb_position_color') ;

        $mb_list = array() ;
        foreach ($result as $key => $row) 
        {
            $mb_list[$key]['USERID'] = $row['USERID'] ;                       
            $mb_list[$key]['P_ID'] = $row['P_ID'] ;                       
            $mb_list[$key]['R_ID'] = $row['R_ID'] ;  
            $mb_list[$key]['ISCLASS'] = $row['ISCLASS'] ;                   

            $drawNode = "<div id=\"".$row['USERID']."\" class=\"service" ;

            if ( $row['ISCLASS'] == 'X' && $row['USERID'] != 'D0000000' )
            {
                //$drawNode .= " bg-color-grey" ;
            }

            if ( !$tree_version || $is_fullscreen )
            {            
                $drawNode .= "\">" ;
            }
            else
            {
                $drawNode .= " cursor-pointer\" ondblclick=\"btn_OnDbClick()\" onclick=\"btn_OnClick(\'".$row['USERID']."\')\">" ;
            }

            if ( $row['ISCLASS'] == 'X' && $row['USERID'] != 'D0000000' )
            {
                $div_contents = "<div class=\"treeview-contents-x eng\">" ;
            }
            else
            {
                $div_contents = "<div class=\"treeview-contents eng\">" ;
            }            

            $drawNode .= "<i class=\"fa fa-user treeview-icon ".$mb_position_color[$row['RANK_CD']]."\"></i>" ;
            $drawNode .= $div_contents."<strong>".$row['USERNAME'] ;

            if ( $row['ISCLASS'] == 'X' && $row['USERID'] != 'D0000000' )
            {
                $drawNode .= " (사업철회)" ;
            }

            $drawNode .=  "</strong></div>" ;

            $drawNode .= $div_contents.$row['USERID']."</div>" ;
            $drawNode .= $div_contents.$center_list[$row['CENTER_CD']]."</div>" ;
            $drawNode .= $div_contents.$rank_list[$row['RANK_CD']]."</div>" ;
            $drawNode .= $div_contents.$row['REG_DATE']."</div>" ;
            $drawNode .= $div_contents."HB : ".number_format($row['PV'])."</div></div>" ;

            $mb_list[$key]['drawNode'] = $drawNode ;

            $textNode = $row['USERNAME'] ;

            if ( $row['ISCLASS'] == 'X' && $row['USERID'] != 'D0000000' )
            {
                $textNode .= " (사업철회)" ;
            }

            $textNode .= "\n".$row['USERID']."\n" ;
            $textNode .= $center_list[$row['CENTER_CD']]."\n" ;
            $textNode .= $rank_list[$row['RANK_CD']]."\n" ;
            $textNode .= $row['REG_DATE']."\n" ;
            $textNode .= "HB : ".number_format($row['PV'])."\n" ;

            $mb_list[$key]['textNode'] = $textNode ;
        }        

		$data = array(
            'qstr' 				=> $qstr,        
            's_path' 			=> $s_path,
            'path' 				=> $path,
            'name' 				=> isset($result[0]) ? $result[0]['USERNAME'] : '',
            'mb_id'             => $mb_id,
            'mb_list' 			=> $mb_list,
 			'mb_count_subtree'	=> $mb_count_subtree,
 			'mb_position'		=> $rank_list,
 			'mb_position_color'	=> $mb_position_color, 
            'user_kind'         => $in_user_kind,  
            'is_fullscreen'     => $is_fullscreen,       
        );

        if ( $in_user_kind == "P" )
        {                
            $data['upper_user'] = $mb_list[0]['P_ID'] ;
        }
        else
        {
            $data['upper_user'] = $mb_list[0]['R_ID'] ;    
        }        

        return $data ;
	}

	function get_member_info()
	{
		$mb_id = $this->input->post('mb_id') ;

		if ( $mb_id != '' )
        {
        	$member = $this->Member_info_model->get_member_info_by_id($mb_id) ;

        	if ( $member )
        	{
                $member['REG_DATE'] = substr($member['REG_DATE'], 0, 4).'-'.substr($member['REG_DATE'], 4, 2).'-'.substr($member['REG_DATE'], -2, 2) ;                
                $member['JUMIN_NO'] = substr($member['JUMIN_NO'], 0, 6) ;

        		$member['mb_count_subtree'] = $this->member_tree_model->get_member_count_subtree($mb_id).' 명' ;   
        		$member['mb_position_str'] = $member['RANK_NAME'] ;

				$mb_position_color = $this->config->item('mb_position_color') ;
        		$member['mb_position_color'] = $mb_position_color[$member['RANK_CD']] ;

                $mb_supporter_name = $this->Member_info_model->get_member_name_by_id($member['P_ID'], true) ;

        		if ( $mb_supporter_name != '' )
        		{
        			$member['mb_supporter'] = $mb_supporter_name.' ('.$member['P_ID'].')' ;
        		}

                $mb_recommander_name = $this->Member_info_model->get_member_name_by_id($member['R_ID'], true) ;
                
                if ( $mb_recommander_name != '' )
                {
                    $member['mb_recommander'] = $mb_recommander_name.' ('.$member['R_ID'].')' ;
                }

        		$member['mb_address'] = '['.$member['ZIPCODE'].'] '.$member['ADDR1'].' '.$member['ADDR2'] ;

                $member['mb_cv'] = number_format($member['CV']) ;
                $member['mb_pv'] = number_format($member['PV']) ;

                $member['rank_date_list'] = $this->member_tree_model->get_rank_date_list($mb_id) ;

                $result = $this->Member_info_model->sub_member_two_my_tree($mb_id) ;
                $a_user_id = '' ;
                $b_user_id = '' ;
                
                foreach ($result as $key => $row) 
                {
                    if ( $key == 0 )
                    {
                        $a_user_id = $row['USERID'] ;
                    }
                    else if ( $key == 1 )
                    {
                        $b_user_id = $row['USERID'] ;
                    }
                }

                $a_total = $this->OrderMaster_model->get_support_cv_pv_subtree($a_user_id) ;    
                $b_total = $this->OrderMaster_model->get_support_cv_pv_subtree($b_user_id) ;

                $member['a_sub_total_cv'] = isset($a_total['CV']) ? number_format($a_total['CV']) : 0 ;
                $member['a_sub_total_pv'] = isset($a_total['PV']) ? number_format($a_total['PV']) : 0 ;
                $member['b_sub_total_cv'] = isset($b_total['CV']) ? number_format($b_total['CV']) : 0 ;
                $member['b_sub_total_pv'] = isset($b_total['PV']) ? number_format($b_total['PV']) : 0 ;                

                $member['a_sub_count'] = $this->member_tree_model->get_member_count_subtree($a_user_id) ;
                $member['a_sub_count'] = ($member['a_sub_count'] > 0) ? ( $member['a_sub_count'] + 1 ) : 0 ;
                $member['a_sub_count'] .= ' 명' ;  

                $member['b_sub_count'] = $this->member_tree_model->get_member_count_subtree($b_user_id) ;
                $member['b_sub_count'] = ($member['b_sub_count'] > 0) ? ( $member['b_sub_count'] + 1 ) : 0 ;
                $member['b_sub_count'] .= ' 명' ;                  

        		echo json_encode($member) ;
        		return TRUE ;
        	}
        }

        echo 'NULL' ;
    	return FALSE ;
	}

    function get_name_list()
    {
        $mb_name = $this->input->post('mb_name') ;

        if ( $mb_name != '' )
        {
            $result = $this->member_tree_model->get_search_name_list_in_subtree($mb_name) ;

            if ( $result )
            {
                $data = array() ;
                foreach ($result as $key => $value) 
                {
                    $data[$key] = $value['USERID'] ;
                }

                echo json_encode($data) ;
                return TRUE ;
            }
        }

        echo 'NULL' ;
        return FALSE ;
    }

    function test()
    {
        $this->load->library('segment', array('offset'=>4), 'seg');
        $seg      =& $this->seg;
        $user_kind = $seg->get('user_kind', 'P');   

        $head = array('title' => '회원트리조회(전체화면)');
        $data = $this->_data($user_kind, false) ;

        widget::run('head', $head);
        $this->load->view('member/mobile_fullscreen_treeview', $data);
        widget::run('tail');
    }  

    function setTreeVersion()
    {
        $IsSimpleTree = false ;

        if ( $this->getBrowserInfo() != "" && $this->getBrowserInfo() <= 11 )
        {
            $IsSimpleTree = true ;
        }

        if ( $this->IsMobile() )
        {
            $IsSimpleTree = true ;
        }

        return $IsSimpleTree ;
    }

    function IsMobile() 
    { 
        $MobileArray  = array("iphone","lgtelecom","skt","mobile","samsung","nokia","blackberry","android","android","sony","phone") ;

        $checkCount = 0 ; 
            
        for($i = 0 ; $i < sizeof($MobileArray) ; $i++)
        { 
            if(preg_match("/$MobileArray[$i]/", strtolower($_SERVER['HTTP_USER_AGENT']))){ $checkCount++; break; } 
        } 

        return ($checkCount >= 1) ? true : false ;
    }

    function getBrowserInfo()
    {
        $version = "" ;

        preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches) ;
        
        if(count($matches) < 2)
        {
            preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches) ;
        }

        if (count($matches) > 1)
        {             
            $version = $matches[1] ; //$matches변수값이 있으면 IE브라우저
                  
        }

        return $version ;
    }
}
?>