<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Temp_line extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model(array('HB_join_temp_line_model'));

		define('WIDGET_SKIN', 'main');

		//$this->output->enable_profiler(TRUE);
	}

	function _remap($index)
	{
		if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

		switch($index)
		{
			case 'page':
			case 'index':
				$this->index() ;
			break;
			default:
					show_404();
			break;
		}
	}

	function index()
	{
		$this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $sfl   = $param->get('sfl', 'A.USERID');   // 검색어
        $stx   = $param->get('stx');   // 분류

        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();

        $total_count = $this->HB_join_temp_line_model->list_count($sfl, $stx) ;

        $config['suffix']       = $qstr;
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        //  페이지 선택 후,  검색시 오류 방지
        $page = ( $total_count < ($page - 1) * $config['per_page'] ) ? 1 : $page ;

        $limit = $config['per_page'] ;
        $offset = ($page - 1) * $config['per_page'];

        $result = $this->HB_join_temp_line_model->list_result($sfl, $stx, $limit, $offset) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row)
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
        }

        $head = array('title' => '회원가입시 라인정보');
        $data = array(
            'total_count'  => $total_count,
            'list'         => $list,
            'paging'       => $CI->pagination->create_links(),
            'qstr'         => $qstr,
            'sfl'          => $sfl,
            'stx'          => $stx,
        );

        widget::run('head', $head);
        $this->load->view('member/temp_line', $data);
        widget::run('tail');
    }
}
?>