<?php
class Business extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		define('WIDGET_SKIN', 'main');
		define('CSS_SKIN', 'jquery,pages/page_pricing');
	}

	function gd1() 
	{
        $head = array('title' => '제품주문') ;

		widget::run('head', $head);
		$this->load->view('business/gd1');
		widget::run('tail');
	}

	function gd2() 
	{
        $head = array('title' => '환불 및 철회') ;

		widget::run('head', $head);
		$this->load->view('business/gd2');
		widget::run('tail');
	}

	function gd3() 
	{
        $head = array('title' => '마케팅 플랜') ;

		widget::run('head', $head);
		$this->load->view('business/gd3');
		widget::run('tail');
	}

	function gd4() 
	{
        $head = array('title' => '평균 후원 수당') ;

		widget::run('head', $head);
		$this->load->view('business/gd4');
		widget::run('tail');
	}

	function gd5() 
	{
        $head = array('title' => '회원 관리 규정') ;

		widget::run('head', $head);
		$this->load->view('business/gd5');
		widget::run('tail');
	}

	function gd6() 
	{
        $head = array('title' => '회원 윤리 규정') ;

		widget::run('head', $head);
		$this->load->view('business/gd6');
		widget::run('tail');
	}

	function gd7() 
	{
        $head = array('title' => '회원 가입') ;

		widget::run('head', $head);
		$this->load->view('business/gd7');
		widget::run('tail');
	}

	function gd8() 
	{
        $head = array('title' => '개인 정보 처리 방침') ;

		widget::run('head', $head);
		$this->load->view('business/gd8');
		widget::run('tail');
	}

	function gd9() 
	{
        $head = array('title' => '평균 후원 수당 공지') ;

		widget::run('head', $head);
		$this->load->view('business/gd9');
		widget::run('tail');
	}
}
?>