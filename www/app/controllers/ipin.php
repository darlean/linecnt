<?php
class ipin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();        

        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소
    }

    public function check()
    {
    	session_start();
		/********************************************************************************************************************************************
			NICE평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED
			
			서비스명 : 가상주민번호서비스 (IPIN) 서비스
			페이지명 : 가상주민번호서비스 (IPIN) 호출 페이지
			
			[ PHP 확장모듈 설치 안내 ]
			1.	Php.ini 파일의 설정 내용 중 확장모듈 경로(extension_dir)로 지정된 위치에 첨부된 IPINClient.so 파일을 복사합니다.
			2.	Php.ini 파일에 다음과 같은 설정을 추가 합니다.
					extension=IPINClient.so
			3.	아파치 재 시작 합니다.
			
		*********************************************************************************************************************************************/
		/*****************************
		//아파치에서 모듈 로드가 되지 않았을경우 동적으로 모듈을 로드합니다.
		if(!extension_loaded('IPINClient')) {
			dl('IPINClient.' . PHP_SHLIB_SUFFIX);
		}
		$module = 'IPINClient';
		*****************************/
		
		$sSiteCode					= "L596";			// IPIN 서비스 사이트 코드		(NICE평가정보에서 발급한 사이트코드)
		$sSitePw					= "78221612";			// IPIN 서비스 사이트 패스워드	(NICE평가정보에서 발급한 사이트패스워드)
		
		$sReturnURL					= "http://".$_SERVER['HTTP_HOST']."/ipin/process";			// 하단내용 참조
		$sCPRequest					= "";			// 하단내용 참조
		
		//if (extension_loaded($module)) {// 동적으로 모듈 로드 했을경우
			$sCPRequest = get_request_no($sSiteCode);
		//} else {
		//	$sCPRequest = "Module get_request_no is not compiled into PHP";
		//}
		
		// 현재 예제로 저장한 세션은 ipin_result.php 페이지에서 데이타 위변조 방지를 위해 확인하기 위함입니다.
		// 필수사항은 아니며, 보안을 위한 권고사항입니다.
		$_SESSION['CPREQUEST'] = $sCPRequest;
	    
	    $sEncData					= "";			// 암호화 된 데이타
		$sRtnMsg					= "";			// 처리결과 메세지
		
	    // 리턴 결과값에 따라, 프로세스 진행여부를 파악합니다.

	  
			//if (extension_loaded($module)) {/ 동적으로 모듈 로드 했을경우
				$sEncData = get_request_data($sSiteCode, $sSitePw, $sCPRequest, $sReturnURL);
			//} else {
			//	$sEncData = "Module get_request_data is not compiled into PHP";
			//}
	    
	    // 리턴 결과값에 따른 처리사항
	    if ($sEncData == -9)
	    {
	    	$sRtnMsg = "입력값 오류 : 암호화 처리시, 필요한 파라미터값의 정보를 정확하게 입력해 주시기 바랍니다.";
	    } else {
	    	$sRtnMsg = "$sEncData 변수에 암호화 데이타가 확인되면 정상, 정상이 아닌 경우 리턴코드 확인 후 NICE평가정보 개발 담당자에게 문의해 주세요.";
	    }

	    //echo $sRtnMsg;

	    	/*
	
		┌ sReturnURL 변수에 대한 설명  ─────────────────────────────────────────────────────
			NICE평가정보 팝업에서 인증받은 사용자 정보를 암호화하여 귀사로 리턴합니다.
			따라서 암호화된 결과 데이타를 리턴받으실 URL 정의해 주세요.
			
			* URL 은 http 부터 입력해 주셔야하며, 외부에서도 접속이 유효한 정보여야 합니다.
			* 당사에서 배포해드린 샘플페이지 중, ipin_process.jsp 페이지가 사용자 정보를 리턴받는 예제 페이지입니다.
			
			아래는 URL 예제이며, 귀사의 서비스 도메인과 서버에 업로드 된 샘플페이지 위치에 따라 경로를 설정하시기 바랍니다.
			예 - http://www.test.co.kr/ipin_process.jsp, https://www.test.co.kr/ipin_process.jsp, https://test.co.kr/ipin_process.jsp
		└────────────────────────────────────────────────────────────────────
		
		┌ sCPRequest 변수에 대한 설명  ─────────────────────────────────────────────────────
			[CP 요청번호]로 귀사에서 데이타를 임의로 정의하거나, 당사에서 배포된 모듈로 데이타를 생성할 수 있습니다. (최대 30byte 까지만 가능)
			
			CP 요청번호는 인증 완료 후, 암호화된 결과 데이타에 함께 제공되며
			데이타 위변조 방지 및 특정 사용자가 요청한 것임을 확인하기 위한 목적으로 이용하실 수 있습니다.
			
			따라서 귀사의 프로세스에 응용하여 이용할 수 있는 데이타이기에, 필수값은 아닙니다.
		└────────────────────────────────────────────────────────────────────
		*/

	    $data = array( 'sEncData' => $sEncData );

	    widget::run('head') ;
        $this->load->view('ipin/ipin_main', $data) ;
        widget::run('tail') ; 
    }

    public function process()
    {
    	// 사용자 정보 및 CP 요청번호를 암호화한 데이타입니다. (ipin_main.php 페이지에서 암호화된 데이타와는 다릅니다.)
		$sResponseData = $_POST['enc_data'];
		
		// ipin_main.php 페이지에서 설정한 데이타가 있다면, 아래와 같이 확인가능합니다.
		$sReservedParam1  = $_POST['param_r1'];
		$sReservedParam2  = $_POST['param_r2'];
		$sReservedParam3  = $_POST['param_r3'];
		
			//////////////////////////////////////////////// 문자열 점검///////////////////////////////////////////////
	    if(preg_match('~[^0-9a-zA-Z+/=]~', $sResponseData, $match)) {echo "입력 값 확인이 필요합니다"; exit;}
	    if(base64_encode(base64_decode($sResponseData))!=$sResponseData) {echo "입력 값 확인이 필요합니다!"; exit;}
	    
	    if(preg_match("/[#\&\\+\-%@=\/\\\:;,\.\'\"\^`~\_|\!\/\?\*$#<>()\[\]\{\}]/i", $sReservedParam1, $match)) {echo "문자열 점검 : ".$match[0]; exit;}
	    if(preg_match("/[#\&\\+\-%@=\/\\\:;,\.\'\"\^`~\_|\!\/\?\*$#<>()\[\]\{\}]/i", $sReservedParam2, $match)) {echo "문자열 점검 : ".$match[0]; exit;}
	    if(preg_match("/[#\&\\+\-%@=\/\\\:;,\.\'\"\^`~\_|\!\/\?\*$#<>()\[\]\{\}]/i", $sReservedParam3, $match)) {echo "문자열 점검 : ".$match[0]; exit;}
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// 암호화된 사용자 정보가 존재하는 경우
		//if ($sResponseData != "")
		$data = array( 
						'sEncData' => $sResponseData,
						'sReservedParam1' => $sReservedParam1,
						'sReservedParam2' => $sReservedParam2,
						'sReservedParam3' => $sReservedParam3
					);

	    widget::run('head') ;
        $this->load->view('ipin/ipin_process', $data) ;
        widget::run('tail') ; 
    }
}