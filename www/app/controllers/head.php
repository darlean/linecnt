<?php if ( ! defined('WIDGET')) exit('No direct script access allowed');

class Head extends Widget {
	function index($data=FALSE) {
		$widget = FALSE;
		if (defined('WIDGET_SKIN'))
			$widget = WIDGET_SKIN;
		
		$css = array();
		if (defined('CSS_SKIN'))
			$css = explode(',', CSS_SKIN);
		
		$var_board = '';
        if (defined('BO_TABLE')) {
            $var_board = "  , rt_bo_table = '".BO_TABLE."'\n";
            if (isset($data['sca']))
                $var_board .= "  , rt_bo_sca = '".$data['sca']."'\n";
            
			if (BO_HEAD) {
				$bo_head = explode('/', BO_HEAD);
				$widget = $bo_head[0];
				if (isset($bo_head[1]))
					$css[] = $bo_head[1];
			}
        }

        if ($widget)
			$widget = $this->$widget($data);

		$head = array(
			'title'		=> isset($data['title']) ? $data['title'] : $this->config->item('cf_title'),
			'charset'   => $this->config->item('charset'),
			'css'       => $css,
            'var_board' => $var_board,
            'widget_skin' => $widget['skin'],
            'widget_body' => $widget['body'] ? ' class="'.$widget['body'].'"' : ''
		);
		$this->load->view('_head', $head);

		if ( isset( $widget['home'] ) && !$widget['home'] )
		{
			$this->load->view('main/breadcrumbs', $head) ;
		}
	}

	// 관리자
	function admin($admin=FALSE) {
		define('ADM_PATH', RT_PATH.'/'.ADM_F);
		
		$admin['use_point'] = $this->config->item('cf_use_point');
		$admin['use_popup'] = $this->config->item('cf_use_popup');
		$admin['current'] = ADM_PATH.$this->uri->slash_segment(2, 'both');
		return array(
			'skin' => $this->load->view(ADM_F.'/head', $admin, TRUE),
			'body' => ''
		);
    }

    // home
	function home($main=FALSE) 
	{
		return $this->_main($main) ;
	}

    // 메인
	function main($main=FALSE) 
	{
		return $this->_main($main) ;
	}

	private function _main($main=FALSE)
	{
		if (IS_MEMBER) 
		{
			$mb = unserialize(MEMBER);
			$main['mb_nick']      = $mb['USERNAME']; 
			$main['mb_userid']    = $mb['USERID']; 
			$main['KT_REG_NO']    = isset($mb['KT_REG_NO']) ? $mb['KT_REG_NO'] : '<a href="/../board/NOTICE/view/wr_id/783">승인코드를 발급받으세요!</a>' ; 
			$main['SKT_REG_NO']   = isset($mb['SKT_REG_NO']) ? $mb['SKT_REG_NO'] : '<a href="/../board/NOTICE/view/wr_id/783">승인코드를 발급받으세요!</a>' ; 
			$main['LGU_REG_NO']   = isset($mb['LGU_REG_NO']) ? $mb['LGU_REG_NO'] : '<a href="/../board/NOTICE/view/wr_id/783">승인코드를 발급받으세요!</a>' ; 	
			$main['KAIT_NO']   = isset($mb['KAIT_NO']) ? $mb['KAIT_NO'] : '<a href="/../board/NOTICE/view/wr_id/1563">승낙코드를 발급받으세요!</a>' ; 					
			$main['KAIT_TIP1'] = "" ;
			$main['KAIT_TIP2'] = "" ;	
			$main['KAIT_HREF'] = "/member/kait/insert/".$mb['USERID'] ;

			if ( isset($mb['KAIT_NO']) )
			{					
				$tmp1 = array ( 'SKT_REG_CHECK', 'KT_REG_CHECK', 'LGU_REG_CHECK' ) ;
				$tmp2 = array ( 'SKT', 'KT', 'LGU+' ) ;

				foreach ($tmp1 as $key => $value) 
				{
					if ( $mb[$value] == 'O' )
					{
						if ( strlen($main['KAIT_TIP1']) > 0 )
						{
							$main['KAIT_TIP1'] .= ", " ;
						}

						$main['KAIT_TIP1'] .= $tmp2[$key] ;
					}
					else
					{
						if ( strlen($main['KAIT_TIP2']) > 0 )
						{
							$main['KAIT_TIP2'] .= ", " ;
						}

						$main['KAIT_TIP2'] .= $tmp2[$key] ;
					}
				}

				if ( strlen($main['KAIT_TIP1']) > 0 )
				{
					$main['KAIT_TIP1'] .= " 사전승낙서 등록 완료" ;
				}

				if ( strlen($main['KAIT_TIP2']) > 0 )
				{
					$main['KAIT_TIP2'] .= " 사전승낙서를 등록하세요" ;
				}
			}
			else 
			{
				$main['KAIT_TIP1'] = "사전승낙서를 등록하세요" ;					
			}

			$CI =& get_instance();
			$CI->load->model('HB_temp_kait_model');				
			$kait_info = $CI->HB_temp_kait_model->get_info($mb['USERID']) ;				

			if ( isset($kait_info['ON_REJECT']) && $kait_info['ON_REJECT'] == 'O' )
			{
				$main['KAIT_TIP2'] = "사전승낙서 등록이 반려 처리되었습니다" ;				
			}
			else if ( isset($kait_info['ON_UPDATE']) && $kait_info['ON_UPDATE'] == 'O' )
			{
				$main['KAIT_TIP2'] = "사전승낙서 확인 중입니다" ;
				$main['KAIT_HREF'] = "javascript:alert('사전승낙서 등록 진행중입니다.')" ;
			}

			if( IS_MANAGER || IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
			{
				$CI =& get_instance();
				$CI->load->model('S_Center_model');
				$main['center_list']  = $CI->S_Center_model->get_center_list() ;
			}
		}

		return array(
			'skin' => $this->load->view('main/head', $main, TRUE),
			'body' => 'body_gray',
			'home' => false
		);
	}
}