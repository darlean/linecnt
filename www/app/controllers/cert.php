<?php
class cert extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();        

        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소
    }

    public function index()
    {
    }

    // 회원 가입시 사용
    function cert_join()
    {               
        //define( 'WIDGET_SKIN', 'empty_skin' );

        $this->cert_main() ;
    }

    // 인증 안한 회원이 인증할 시
    function certification()
    {   
        define( 'WIDGET_SKIN', 'empty_skin' );     
        
        $this->cert_main("seller_certification") ;
    }

    function cert_and_order()
    {
        define( 'WIDGET_SKIN', 'empty_skin' );

        $this->cert_main("order") ;
    }

    // 보통의 경우 예시
    function cert_basic()
    {
        $this->cert_main() ;
    }

    private function cert_main()
    {
        if(!extension_loaded('CPClient')) 
        {
            dl('CPClient.'.PHP_SHLIB_SUFFIX);
        }
        $module = 'CPClient';

        session_start() ;        
        
        $data['sReserved1'] = '' ;
        $data['sReserved2'] = '' ;
        $data['sReserved3'] = '' ;
        
        $nice_param['sitecode'] = "G8979" ;             // NICE로부터 부여받은 사이트 코드
        $nice_param['sitepasswd'] = "TLZSMV3RQI8R" ;           // NICE로부터 부여받은 사이트 패스워드
        
        $nice_param['cb_encode_path'] = "/www/data/certify/CPClient" ;     // NICE로부터 받은 암호화 프로그램의 위치 (절대경로+모듈명)
        
        $nice_param['authtype'] = "M" ;         // 없으면 기본 선택화면, X: 공인인증서, M: 핸드폰, C: 카드
            
        $nice_param['popgubun'] = "N" ;      //Y : 취소버튼 있음 / N : 취소버튼 없음
        $nice_param['customize']  = "" ;     //없으면 기본 웹페이지 / Mobile : 모바일페이지
        
        //$nice_param['reqseq'] = "REQ_0123456789" ;     // 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로
                                        // 업체에서 적절하게 변경하여 쓰거나, 아래와 같이 생성한다.
        //$nice_param['reqseq'] = `{$nice_param['cb_encode_path']} SEQ {$nice_param['sitecode']}` ;

        if (extension_loaded($module)) 
        {// 동적으로 모듈 로드 했을경우
            $reqseq = get_cprequest_no($nice_param['sitecode']);
        } 
        else 
        {
          $reqseq = "Module get_request_no is not compiled into PHP";
        }
        
        // CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
        $nice_param['returnurl'] = "http://".$_SERVER['HTTP_HOST']."/cert/cert_success";    // 성공시 이동될 URL
        $nice_param['errorurl'] = "http://".$_SERVER['HTTP_HOST']."/cert/cert_fail" ;        // 실패시 이동될 URL
        
        // reqseq값은 성공페이지로 갈 경우 검증을 위하여 세션에 담아둔다.
        
        //$_SESSION["REQ_SEQ"] = $nice_param['reqseq'] ;
        $_SESSION["REQ_SEQ"] = $reqseq;

        // 입력될 plain 데이타를 만든다.
        $plaindata =  "7:REQ_SEQ" . strlen($reqseq) . ":" . $reqseq .
                                  "8:SITECODE" . strlen($nice_param['sitecode']) . ":" . $nice_param['sitecode'] .
                                  "9:AUTH_TYPE" . strlen($nice_param['authtype']) . ":". $nice_param['authtype'] .
                                  "7:RTN_URL" . strlen($nice_param['returnurl']) . ":" . $nice_param['returnurl'] .
                                  "7:ERR_URL" . strlen($nice_param['errorurl']) . ":" . $nice_param['errorurl'] .
                                  "11:POPUP_GUBUN" . strlen($nice_param['popgubun']) . ":" . $nice_param['popgubun'] .
                                  "9:CUSTOMIZE" . strlen($nice_param['customize']) . ":" . $nice_param['customize'] ;
        
        //$enc_data = `{$nice_param['cb_encode_path']} ENC {$nice_param['sitecode']} {$nice_param['sitepasswd']} $plaindata`;
        //$enc_data = get_encode_data($sitecode, $sitepasswd, $plaindata);                                 
        if (extension_loaded($module)) {// 동적으로 모듈 로드 했을경우
            $enc_data = get_encode_data($nice_param['sitecode'], $nice_param['sitepasswd'], $plaindata);
        } else {
          $enc_data = "Module get_request_data is not compiled into PHP";
        }

        //var_dump($enc_data);

        $returnMsg = '' ;
        if( $enc_data == -1 )
        {
            $returnMsg = "암/복호화 시스템 오류입니다.";
            $enc_data = "";
        }
        else if( $enc_data== -2 )
        {
            $returnMsg = "암호화 처리 오류입니다.";
            $enc_data = "";
        }
        else if( $enc_data== -3 )
        {
            $returnMsg = "암호화 데이터 오류 입니다.";
            $enc_data = "";
        }
        else if( $enc_data== -9 )
        {
            $returnMsg = "입력값 오류 입니다.";
            $enc_data = "";
        }

        $data['returnMsg'] = $returnMsg ; 
        $data['enc_data'] = $enc_data ;     

        widget::run('head') ;
        $this->load->view('cert/cert_main', $data) ;
        widget::run('tail') ; 
    }

    function cert_success()
    {
        $data = $this->cert_result(true) ;

        

        $this->session->set_flashdata('is_certification', true) ;
        $this->session->set_flashdata('cert_name', $data['name']) ;
        $this->session->set_flashdata('cert_birth', $data['birthdate']) ;

        //$cert_type = $this->param->get('cert_type') ;

        //var_dump($cert_type);

        /*if ( $cert_type == "seller_join" )
        {
            $result_url = '/member/join/form/' ;

            echo ("<script>window.open('".$result_url."');</script>") ;
            echo ("<script>alert('본인인증이 완료되었습니다.');parent.self.close();</script>") ;
        }
        else*/

        /*if( $data['bSuccess'] )
        {
            $result_url = '/member/join/bank_account/' ;
            //var_dump($result_url);
            echo ('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />') ;
            echo ("<script>window.open('".$result_url."');</script>") ;
            echo ("<script>alert('본인인증이 완료되었습니다.');parent.self.close();</script>") ;
        }
        else*/
        {
            widget::run('head');
            $this->load->view('cert/cert_result', $data) ;
            widget::run('tail');
        }
    }

    function cert_fail()
    {
        $data = $this->cert_result(false) ;


        $result_url = '/' ;
            //var_dump($result_url);
        echo ('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />') ;
        echo ("<script>window.open('".$result_url."');</script>") ;
        echo ("<script>alert('본인인증이 실패하였습니다.');parent.self.close();</script>") ;

        /*if ( $cert_type == "seller_join" )
        {
           echo ("<script>alert('본인인증이 실패하였습니다.');parent.self.close();</script>") ;
        }
        else if( $cert_type == 'seller_certification' )
        {
            $this->tank_auth->logout();

            echo ("<script>alert('본인인증이 실패하였습니다.');parent.self.close();</script>") ;
        }
        else if( $cert_type == "order" )
        {
            $this->tank_auth->logout();

            echo ("<script>alert('본인인증이 실패하였습니다.');parent.self.close();</script>") ;
        }
        else
        {
            $bShowData = true ;

            if ( $bShowData )
            {
                widget::run('head');
                $this->load->view('cert/cert_result_v', $data) ;
                widget::run('tail');
            }
            else
            {
                echo ("<script>alert('본인인증이 실패하였습니다.');parent.self.close();</script>") ;
            }
        }*/
    }

    private function cert_result($bSuccess)
    {        
        session_start();
    
        /*****************************
      //아파치에서 모듈 로드가 되지 않았을경우 동적으로 모듈을 로드합니다.
    
        if(!extension_loaded('CPClient')) {
            dl('CPClient.' . PHP_SHLIB_SUFFIX);
        }
        $module = 'CPClient';
        *****************************/
    
        $sitecode = "G8979";                 // NICE로부터 부여받은 사이트 코드
        $sitepasswd = "TLZSMV3RQI8R";               // NICE로부터 부여받은 사이트 패스워드        
        
        
        $enc_data = $_POST["EncodeData"];       // 암호화된 결과 데이타
        $data['sReserved1'] = $_POST['param_r1'] ;       
        $data['sReserved2'] = $_POST['param_r2'] ;
        $data['sReserved3'] = $_POST['param_r3'] ;

            //////////////////////////////////////////////// 문자열 점검///////////////////////////////////////////////
        if(preg_match('~[^0-9a-zA-Z+/=]~', $enc_data, $match)) {echo "입력 값 확인이 필요합니다 : ".$match[0]; exit;} // 문자열 점검 추가. 
        if(base64_encode(base64_decode($enc_data))!=$enc_data) {echo "입력 값 확인이 필요합니다"; exit;}
        
        if(preg_match("/[#\&\\+\-%@=\/\\\:;,\.\'\"\^`~\_|\!\/\?\*$#<>()\[\]\{\}]/i", $data['sReserved1'], $match)) {echo "문자열 점검 : ".$match[0]; exit;}
        if(preg_match("/[#\&\\+\-%@=\/\\\:;,\.\'\"\^`~\_|\!\/\?\*$#<>()\[\]\{\}]/i", $data['sReserved2'], $match)) {echo "문자열 점검 : ".$match[0]; exit;}
        if(preg_match("/[#\&\\+\-%@=\/\\\:;,\.\'\"\^`~\_|\!\/\?\*$#<>()\[\]\{\}]/i", $data['sReserved3'], $match)) {echo "문자열 점검 : ".$match[0]; exit;}
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
            
        if ($enc_data != "") 
        {

                    //if (extension_loaded($module)) {// 동적으로 모듈 로드 했을경우
                        $plaindata = get_decode_data($sitecode, $sitepasswd, $enc_data);// 암호화된 결과 데이터의 복호화
                    //} else {
                    //  $plaindata = "Module get_response_data is not compiled into PHP";
                    //}
            
            //echo "[plaindata]  " . $plaindata . "<br>";

            if ($plaindata == -1){
                $data['returnMsg']  = "암/복호화 시스템 오류";
            }else if ($plaindata == -4){
                $data['returnMsg']  = "복호화 처리 오류";
            }else if ($plaindata == -5){
                $data['returnMsg']  = "HASH값 불일치 - 복호화 데이터는 리턴됨";
            }else if ($plaindata == -6){
                $data['returnMsg']  = "복호화 데이터 오류";
            }else if ($plaindata == -9){
                $data['returnMsg']  = "입력값 오류";
            }else if ($plaindata == -12){
                $data['returnMsg']  = "사이트 비밀번호 오류";
            }else{
                // 복호화가 정상적일 경우 데이터를 파싱합니다.
                  
                $data['requestnumber'] = $this->GetValue($plaindata , "REQ_SEQ");
                $data['responsenumber'] = $this->GetValue($plaindata , "RES_SEQ");
                $data['authtype'] = $this->GetValue($plaindata , "AUTH_TYPE");
                $data['name'] = $this->GetValue($plaindata , "NAME") ;
                $data['birthdate'] = $this->GetValue($plaindata , "BIRTHDATE");
                $data['gender'] = $this->GetValue($plaindata , "GENDER");
                $data['nationalinfo'] = $this->GetValue($plaindata , "NATIONALINFO");  //내/외국인정보(사용자 매뉴얼 참조)
                $data['dupinfo'] = $this->GetValue($plaindata , "DI");
                $data['conninfo'] = $this->GetValue($plaindata , "CI");

                $data['errcode'] = $this->GetValue($plaindata , "ERR_CODE");

                if(strcmp($_SESSION["REQ_SEQ"], $data['requestnumber']) != 0)
                {
                    echo "세션값이 다릅니다. 올바른 경로로 접근하시기 바랍니다.<br>";
                    $requestnumber = "";
                    $responsenumber = "";
                    $authtype = "";
                    $name = "";
                        $birthdate = "";
                        $gender = "";
                        $nationalinfo = "";
                        $dupinfo = "";
                        $conninfo = "";
                }
            }
        }

        $data['enc_data'] = $enc_data ; 
        $data['name'] =  iconv( "EUC-KR", "utf-8", $data['name'] ) ;
        $data['bSuccess'] = $bSuccess ;
        
        return $data ;
    }

    function GetValue($str , $name)
    {
        $pos1 = 0;  //length의 시작 위치
        $pos2 = 0;  //:의 위치

        while( $pos1 <= strlen($str) )
        {
            $pos2 = strpos( $str , ":" , $pos1);
            $len = substr($str , $pos1 , $pos2 - $pos1);
            $key = substr($str , $pos2 + 1 , $len);
            $pos1 = $pos2 + $len + 1;
            if( $key == $name )
            {
                $pos2 = strpos( $str , ":" , $pos1);
                $len = substr($str , $pos1 , $pos2 - $pos1);
                $value = substr($str , $pos2 + 1 , $len);
                return $value;
            }
            else
            {
                // 다르면 스킵한다.
                $pos2 = strpos( $str , ":" , $pos1);
                $len = substr($str , $pos1 , $pos2 - $pos1);
                $pos1 = $pos2 + $len + 1;
            }            
        }
    }

    private function ipin_result()
    {        
        session_start();
    
        /*****************************
        //아파치에서 모듈 로드가 되지 않았을경우 동적으로 모듈을 로드합니다.
    
        if(!extension_loaded(''IPINClient'')) {
            dl(''IPINClient'.' . PHP_SHLIB_SUFFIX);
        }
        $module = ''IPINClient'';
        *****************************/
    
        $sSiteCode = "L596";                 // NICE로부터 부여받은 사이트 코드
        $sSitePw = "78221612";               // NICE로부터 부여받은 사이트 패스워드        
        
        $sEncData   = "";           // 암호화 된 사용자 인증 정보
        $plaindata   = "";           // 복호화 된 사용자 인증 정보
    
        $sRtnMsg    = "";           // 처리결과 메세지

        $sCPRequest = $_SESSION['CPREQUEST'];

        $sEncData = $_POST['enc_data'];      // 인증결과 및 부가정보 복호화

       ///////////////////////////////////// 문자열 점검///////////////////////////////////////////////
        if(preg_match('~[^0-9a-zA-Z+/=]~', $sEncData, $match)) {echo "입력 값 확인이 필요합니다"; exit;}
        if(base64_encode(base64_decode($sEncData))!==$sEncData) {echo "입력 값 확인이 필요합니다!"; exit;}
        ////////////////////////////////////////////////////////////////////////////////////////////
            
        if ($sEncData != "")    // 사용자 정보를 복호화 합니다.
        {                        
            $plaindata = get_response_data($sSiteCode, $sSitePw, $sEncData);// 암호화된 결과 데이터의 복호화            

            if ($plaindata == -1){
                $data['returnMsg']  = "암/복호화 시스템 오류";
            }else if ($plaindata == -4){
                $data['returnMsg']  = "복호화 처리 오류";
            }else if ($plaindata == -5){
                $data['returnMsg']  = "HASH값 불일치 - 복호화 데이터는 리턴됨";
            }else if ($plaindata == -6){
                $data['returnMsg']  = "복호화 데이터 오류";
            }else if ($plaindata == -9){
                $data['returnMsg']  = "입력값 오류";
            }else if ($plaindata == -12){
                $data['returnMsg']  = "사이트 비밀번호 오류";
            }else{
                // 복호화가 정상적일 경우 데이터를 파싱합니다.

                $arrData = split("\^", $plaindata);
                $iCount = count($arrData); 

                if ($iCount >= 5) 
                {
                    $strCPRequest   = $arrData[8];  // CP 요청번호
                    if ($sCPRequest == $strCPRequest) 
                    {
                                
                        $sRtnMsg = "사용자 인증 성공";
                                                                    
                        $data['name']    = $arrData[2];  // 이름                            
                        $data['birthdate'] = $arrData[6];    // 생년월일 (YYYYMMDD)                                                                    
                    } 
                    else 
                    {
                        $sRtnMsg = "CP 요청번호 불일치 : 세션에 넣은 $sCPRequest 데이터를 확인" ;
                    }
                } 
                else 
                {
                    $sRtnMsg = "리턴값 확인 후, NICE평가정보 개발 담당자에게 문의해 주세요.";
                }
            }
        } 
        else 
        {
            $sRtnMsg = "처리할 암호화 데이타가 없습니다.";
        }        

        $data['enc_data'] = $sEncData ; 
        $data['name'] =  iconv( "EUC-KR", "utf-8", $data['name'] ) ;     
        $data['bSuccess'] = true ;   
        
        return $data ;
    }

    function ipin_success()
    {
        $data = $this->ipin_result() ;

        $this->session->set_flashdata('is_certification', true) ;
        $this->session->set_flashdata('cert_name', $data['name']) ;
        $this->session->set_flashdata('cert_birth', $data['birthdate']) ;

        widget::run('head');
        $this->load->view('cert/cert_result', $data) ;
        widget::run('tail');        
    }
}
?>