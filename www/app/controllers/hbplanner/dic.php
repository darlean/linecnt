<?php
class dic extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();		
	}

    function index()
    {
        $head = array('title' => '영어사전');
        widget::run('head', $head);
        $this->load->view('android/dic');
        widget::run('tail');
    }
}
?>