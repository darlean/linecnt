<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Credit extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('HB_credit_model', 'Order_model'));

        $this->load->helper(array('socket_helper', 'mail')) ;
        
        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'tab':
            case 'page':
            case 'index':
                $this->_list($index);
            break;     

            case 'view':
                $this->_view() ;
            break ;      

            case 'result':
                $this->_result() ;
            break ;

            case 'del':
                $this->_del() ;
            break ;

            case 'del_last_day':
                $this->del_last_day() ;
            break ;

            default:
                show_404();
            break;
        }
    } 

    function _list() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소   
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $sfl   = $param->get('sfl');   // 검색필드
        $stx   = $param->get('stx');   // 검색어
        $sca   = $param->get('sca');   // 분류

        $page  = $seg->get('page', 1); // 페이지
        $tab  = $seg->get('tab', 0); 
        $qstr  = $param->output();

        $vender = $param->get('vender') ;

        $total_count = $this->HB_credit_model->list_count($sfl, $stx, $tab, '', $vender) ;

        $config['suffix']       = $qstr;        
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');
        $config['base_url']    = RT_PATH.'/hbplanner/credit/tab/'.$tab.'/page/';        

        //  페이지 선택 후,  검색시 오류 방지
        $page = ( $total_count < ($page - 1) * $config['per_page'] ) ? 1 : $page ;

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $limit = $config['per_page'] ;
        $offset = ($page - 1) * $config['per_page'];

        $result = $this->HB_credit_model->list_result($sfl, $stx, $tab, $limit, $offset, '', $vender) ;
        
        $tab_list = array("조회신청", "조회완료") ;      
    
        $venders = array("LGU+", "SKT", "KT", "기타") ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
            $list[$i]['href']               = '/hbplanner/credit/view/credit_idx/'.$row['CREDIT_IDX'] ;
            $list[$i]['WILL_VENDER']        = $venders[$row['WILL_VENDER']] ;                     

            $last_day = floor((time() - strtotime($row['REQ_DATE']))/60/60/24) ;            

            if ( $tab == 1 && $last_day > 14 )            
                $list[$i]['REG_NUM'] = substr($row['REG_NUM'], 0, 6)."*******" ;

            if ( isset($row['USED_VENDER']) )
            {
                $list[$i]['USED_VENDER'] = $venders[$row['USED_VENDER']] ;
            }

            $filepath = "/data/hbplanner/credit/".$row['CREDIT_IDX'].".png" ;
            $list[$i]['agree_file_path'] = file_exists(".".$filepath) ? $filepath : "" ;
        }
                    
        $data = array(
            'sfl' => $sfl,
            'stx' => $stx,
            'sca' => $sca,
            'vender' => $vender,
            'total_count' => $total_count,  
            'list' => $list,
            'paging' => $CI->pagination->create_links(), 
            'qstr' => $qstr,
            'tab_list'  => $tab_list,
            'tab'       => $tab,
        );

        $head = array('title' => '신용조회');
        widget::run('head', $head);
        $this->load->view("hbplanner/credit/lists", $data);
        widget::run('tail');
    }

    function _view()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소   
        $seg      =& $this->seg;
        $credit_idx  = $seg->get('credit_idx');

        $data = $this->HB_credit_model->get_credit_info($credit_idx) ;

        $venders = array("LGU+", "SKT", "KT", "기타") ;

        if ( isset($data['USED_VENDER']) )
        {
            $data['USED_VENDER'] = $venders[$data['USED_VENDER']] ;
        }
        
        $data['WILL_VENDER'] = $venders[$data['WILL_VENDER']] ;

        $last_day = floor((time() - strtotime($data['REQUEST_DATE']))/60/60/24) ;            
        
        if ( $last_day > 14 )                    
            $data['REG_NUM']     = substr($data['REG_NUM'], 0, 6)."*******" ;

        $head = array('title' => '신용조회결과');
        widget::run('head', $head);
        if ( IS_MANAGER )
        {
            $this->load->view("hbplanner/credit/view_manager", $data);
        }
        else
        {
            $this->load->view("hbplanner/credit/view", $data);
        }
        widget::run('tail');
    }

    function _result()
    {
        $credit_idx = $this->input->post('CREDIT_IDX') ;
        $result_idx = $this->input->post('RESULT_IDX') ;

        $data = array(
            'RESULT_IDX'                => $credit_idx,
            'CREDIT_RESULT'             => $_POST["CREDIT_RESULT"],
            'CUR_OPEN_LINE'             => $_POST["CUR_OPEN_LINE"],
            'MAX_LINE'                  => $_POST["MAX_LINE"],
            'INSTALMENT_LINE'           => $_POST["INSTALMENT_LINE"],
            'MAX_INSTALMENT_LINE'       => $_POST["MAX_INSTALMENT_LINE"],
            'INSTALMENT_MONTH'          => $_POST["INSTALMENT_MONTH"],
            'REMAIN_INSTALMENT_MONTH'   => $_POST["REMAIN_INSTALMENT_MONTH"],
            'REMAIN_INSTALMENT_PRICE'   => $_POST["REMAIN_INSTALMENT_PRICE"],
            'REMAIN_AGREE_PRICE'        => $_POST["REMAIN_AGREE_PRICE"],
            'RESULT_MEMO'               => $_POST["RESULT_MEMO"],            
            'RESULT_OK'                 => $_POST["RESULT_OK"],            
            ) ;

        if ( $credit_idx == $result_idx )
        {                                
            $result = $this->HB_credit_model->update_credit_result($data) ;  
        }
        else
        {       
            $result = $this->HB_credit_model->insert_credit_result($data) ;    
        } 

        if ( $result )
        {
            $credit_info = $this->HB_credit_model->get_credit_info($credit_idx) ;

            if ( $credit_info && $credit_info['RESULT_OK'] == 'O' )
            {
                //$this->publish_mlm_order($credit_info['USERID'], $credit_info['USERNAME'], $credit_info['RESULT_DATE']) ;
            }

            alert('신용 조회 결과가 입력되었습니다.', 'hbplanner/credit/view/credit_idx/'.$credit_idx) ;
        } 
    }

    function _del()
    {        
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $credit_idx  = $seg->get('credit_idx');    

        $result = $this->HB_credit_model->del($credit_idx) ;       

        if ( $result )
        {        
            alert('해당 신용조회 정보가 삭제되었습니다.', 'hbplanner/credit') ;
        }       
    }

    function post_request($url, $data) 
    {
        // parse the given URL
        $url = parse_url($url);

        if ($url['scheme'] != 'http') {
            return "Error:Only HTTP request are supported!";
        }

        // extract host and path:
        $host = $url['host'];
        $path = $url['path'];
        $res = fsk_open($url['host'], $url['path'], $data) ;        
    
        $content = explode("\r\n", $res, 4);     

        $result['CODE'] = isset($content[1]) ? substr($content[1], 0, 4) : '' ;
        $result['GT_CD'] = isset($content[1]) ? substr($content[1], 4) : '' ;
        
        return $result ;
    }

    // 신용조회 완료시 공제 번호 신고 
    function publish_mlm_order($userid, $username, $credit_result_date)
    {
        /*$order_count = $this->Order_model->get_order_count_by_datas($userid, $username, $credit_result_date) ;

        if ( $order_count != 1 )        
            return ;        

        $order_info = $this->Order_model->get_order_info_by_datas($userid, $username, $credit_result_date) ;

        if ( $order_info && isset($order_info['GT_CD']) && $order_info['GT_CD'] == "" && $order_info['AMT'] > 0 )
        {            
            $postValues = "ORDERNO=".$order_info['ORDNO'] ;            

            if ( IS_TEST_SERVER )
            {                                    
                $post_result = $this->post_request("http://openpageutf.cafe24.com/guild/hbtest/mlmorder.php", $postValues) ;
            }
            else
            {
                $post_result = $this->post_request("http://vansales.cafe24.com/guild/hbnetworks/mlmorder.php", $postValues) ;  
            }
        } */
    }

    function del_last_day()
    {
        $result = $this->HB_credit_model->delete_credit_last_day(14) ;       

        if ( $result >= 0 )
        {        
            alert('14일 이전 데이터가 '.$result.'건 삭제되었습니다.', 'hbplanner/credit') ;
        }
        else
        {
            alert('14일 이전 데이터 삭제가 실패하였습니다.', 'hbplanner/credit') ;
        }
    }
}
?>