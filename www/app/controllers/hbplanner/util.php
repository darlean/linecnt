<?php
class Util extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('S_Reason_model', 'HB_card_place_model', 'Member_info_model', 'Member_forget_model', 'S_Bank_model', 'HB_credit_model', 'S_Center_model', 'P_pdtmaster_model', 'Order_model', 'OrderMaster_model', 'S_Order_state_model', 'Order_state_model', 'Board_model', 'HB_temp_order_wired', 'Policy_model', 'O_Order_Ctel_model', 'S_Card_model', 'Starion_model', 'HB_temp_order_starion', 'HB_product_gift_model'));

        $this->load->helper(array('mail', 'utill')) ;
	}

    function get_vender_list()
    {
        $vender_list = $this->S_Reason_model->get_vender_list2() ;
        $data = array('result' => $vender_list);

        echo json_encode($data);        
    }

    function get_benefit_list()
    {
        $benefit_list = array( "해당없음", "장애우/국가유공자", "기초생활수급자", "차상위계층" ) ;
        $data = array('result' => $benefit_list);

        echo json_encode($data);        
    }

    function get_card_place_list()
    {
        $card_place_list = $this->HB_card_place_model->get_card_place_list() ;
        echo json_encode($card_place_list) ;        
    }

    function get_member_info()
    {        
        $supporter_id = $this->input->post('supporter_id') ;
        $recommender_id = $this->input->post('recommender_id') ;    

        $find_supporter = $this->input->post('find_supporter') ;

        if ( $supporter_id != '' )
        {
            if ( $find_supporter == 'true' )
            {
                $member = $this->Member_info_model->get_member_info_by_id($supporter_id) ;

                if ( $member )
                {
                    $fillable_supporter = $this->Member_info_model->fillable_supporter($supporter_id) ;

                    if ( !$fillable_supporter )
                    {
                        echo 'CANT' ;
                        return FALSE ;
                    }

                    echo json_encode($member) ;
                    return TRUE ;
                }                
            }
            else
            {
                if ( $recommender_id != '' )
                {
                    $member = $this->Member_info_model->get_member_info_by_id($recommender_id) ;

                    if ( $member )
                    {
                        $fillable_recommender = $this->Member_info_model->fillable_recommender($recommender_id, $supporter_id) ;

                        if ( $fillable_recommender == 0 )
                        {
                            echo 'CANT' ;
                            return FALSE ;
                        }

                        echo json_encode($member) ;
                        return TRUE ;
                    }
                }
            }
        }    

        echo 'NULL' ;
        return FALSE ;
    }

    function confrim_bank_account()
    {
        $posts = Array( // 포스트 
            //##################################################
            //###### ▣ 회원사 ID 설정   - 계약시에 발급된 회원사 ID를 설정하십시오. ▣
            //###### ▣ 회원사 PW 설정   - 계약시에 발급된 회원사 PASSWORD를 설정하십시오. ▣
            //###### ▣ 조회사유  설정   - 10:회원가입 20:기존회원가입 30:성인인증 40:비회원확인 90:기타사유 ▣
            //###### ▣ 개인/사업자 설정 - 1:개인 2:사업자 ▣
            //##################################################
            "niceUid"=>"Nhbn00",                        // 나이스평가정보에서 고객사에 부여한 구분 id
            "svcPwd"=>"mFaOqWUv",                       // 나이스평가정보에서 고객사에 부여한 서비스 이용 패스워드
            "inq_rsn"=>"10",                            // 조회사유 - 10:회원가입 20:기존회원가입 30:성인인증 40:비회원확인 90:기타사유
            "strGbn"=>"1",                              // 1 : 개인, 2: 사업자
            //##################################################
            //###### 위의 값을 알맞게 수정해 주세요.
            //##################################################
            "strResId"=>$_POST["birth"],                  // 생년월일
            "strNm"=>$_POST["name"],                      // 이름
            "strBankCode"=>$_POST["bank"],                // 은행코드
            "strAccountNo"=>$_POST["account"],            // 계좌번호
            "service"=>"1",                             // 서비스구분
            "svcGbn"=>"5",                              // 업무구분
            "svc_cls"=>"",                              // 내/외국인 구분
            "strOrderNo"=>date("Ymd") . rand(1000000000,9999999999),            //주문번호 : 매 요청마다 중복되지 않도록 유의(수정불필요)
        );

        $cookies = array();     // 쿠키
        $referer = "";              // 리퍼러

        $host = "secure.nuguya.com";                                                                                                                        // 호스트 
        //$page = "https://secure.nuguya.com/nuguya/service/realname/sprealnameactconfirm.do";      // URL
        $page = "https://secure.nuguya.com/nuguya2/service/realname/sprealnameactconfirm.do";       // UTF-8 URL

        $retval = $this->sock_post($host,$page,$posts,$cookies,$referer); 

        if( $retval == false) 
        {
            echo "E999" ;
            return FALSE ;
        }
        else
        {
            // 결과값 처리
            $NewInfo = explode("\r\n", $retval);            // 헤더정보에서 뉴라인 체크
            $infoValue = explode("|",$NewInfo[8]);      // 9번째 줄의 결과값만 가져온다.
        
            $OrderNum   = $infoValue[0];        // 주문번호
            $ResultCD   = $infoValue[1];        // 결과코드
            $Msg        = $infoValue[2];        // 메세지

            if( $ResultCD == '0000' )
            {
                echo $ResultCD ;
            }
            else
            {                            
                //echo iconv("UTF-8", "EUC-KR", urldecode($infoValue[2])); 
                echo $infoValue[2]; 
            }
            
            return TRUE ;                    
        }

        echo "Error" ;
        return FALSE ;
    }

    ################################################################################################
    // sock_post() 함수 시작
    ################################################################################################
    function sock_post($host,$target,$posts,$cookies,$referer='',$port=443) 
    { 
        //var_dump($posts);
        $postValues = '';
        if(is_array($posts)) 
        { 
            foreach($posts AS $name=>$value) 
            {
                $postValues .= urlencode($name) . "=" . urlencode($value) . '&'; 
            }

            //var_dump($postValues);
            $postValues = substr($postValues, 0, -1); 
        } 

        $postLength = strlen($postValues); 

        $cookieValues = '';
        if(is_array($cookies)) 
        { 
            foreach($cookies AS $name=>$value) 
            {
                $cookieValues .= urlencode($name) . "=" . urlencode($value) . ';'; 
            }
            $cookieValues = substr($cookieValues, 0, -1); 
        } 

        $request  = "POST $target HTTP/1.1\r\n"; 
        $request .= "Host: $host\r\n"; 
        $request .= "Content-Type: application/x-www-form-urlencoded\r\n"; 
        $request .= "Content-Length: " . $postLength . "\r\n"; 
        $request .= "Connection: close\r\n"; 
        $request .= "\r\n"; 
        $request .= $postValues; 

        $ret = ''; 
        $socket  = fsockopen("ssl://".$host, $port, $errno, $errstr, 10); // 소켓 타임아웃 10초
        
        if ( ! $socket )
        {           
            return false;
        }
        
        fputs($socket, $request); 
            while(!feof($socket)) $ret .= fgets($socket, 1024); 
        fclose($socket);         

        return $ret; 
    } 
    //################################################################################################

    function find_address()
    {
        $search = $this->input->post('addr') ;               // json은 UTF-8만 지원합니다. (결과값도 UTF-8로 리턴)
        $url  = "http://post.phpschool.com/json.phps.kr";
        $data = array("addr"=>$search, "ipkey"=>"1676267", "type"=>"new");
        // ipkey는 인증메일에서 안내합니다.
        // 구주소검색일경우 array("addr"=>$search, "ipkey"=>"XXX",  "type"=>"old");  
        // 지번주소로 도로명주소를 찾기위한 검색 "type"=>"newdong"  /* "가산동 371-50" 식으로 동/번지입력 */
        // 지번주소로 찾기의 경우  http://post.phpschool.com/post.html 예를 참조바랍니다.
        // 구주소검색일경우(구 우편번호로만 찾기) "type"=>"old"  
        

        $output = ($this->HTTP_Post($url, $data));
        $output = substr($output, strpos($output,"\r\n\r\n")+4);

        $json = json_decode($output);

        if ($json->result > 0) {

            /*echo "검색건수 : {$json->result}\n";
            echo "검색시간 : {$json->time}\n";
            echo "조회횟수 : {$json->cnt}\n";
            echo "조회한도 : {$json->maxcnt}\n";*/

            $list = array() ;
            foreach ($json->post as $key=>$value) {
                     //$value->postnew;             // 새우편번호 (5자리)
                     //$value->post;                // 우편번호   (6자리)
                     //$value->addr_1;              // 시/도
                     //$value->addr_2;              // 구
                     //$value->addr_3;              // 도로명
                     //$value->addr_4;              // 동/건물
                     //$value->addr_5;              // 구주소 (도로명주소1:1매칭) // 도로명주소검색일경우만 리턴
                     //$value->addr_eng;            // 영문주소 // 도로명주소검색일경우만 리턴

                     //print_r($value);
                    
                    $list[$key]["postnew"] = $value->postnew ;
                    $list[$key]["addr"] = $value->addr_1." ".$value->addr_2." ".$value->addr_3." ".$value->addr_4 ;

                    if ( $list[$key]["addr"] == '' )
                        $list[$key]["addr"] = $value->addr_5 ;
                    //echo json_encode($value) ;
                    //echo json_encode($list) ;
            }
            echo json_encode($list) ;
            //echo json_encode($json->post) ;
        } else if ($json->result == 0) {
            echo "검색결과가 없습니다.";
        } else if ($json->result == -1) {
            echo "검색결과가 너무 많습니다. 입력하신 검색어 $search 뒤에 단어를 추가해서 검색해보세요.";
        } else if ($json->result < 0) {
            echo "검색실패 : ".$json->message;
        }

        // $result  "-1"  일경우 :  너무많은검색결과 1000건이상
        // $result  "-2"  일경우 :  서버 IP 미인증
        // $result  "-3"  일경우 :  조회횟수초과
        // $result  "-4"  일경우 :  미인증 사용자        
    }

    // 실제 구현 소스는 위에까지입니다. 아래는 소켓함수(curl로 구현가능)
    function HTTP_Post($URL,$data) 
    {
        $str = "" ;
        $URL_Info=parse_url($URL);
        if(!empty($data)) foreach($data AS $k => $v) $str .= urlencode($k).'='.urlencode($v).'&';
        $path = $URL_Info["path"];
        $host = $URL_Info["host"];
        //$port = $URL_Info["port"];
        if (empty($port)) $port=80;
        $result = "";
        $fp = fsockopen($host, $port, $errno, $errstr, 30);
        $http  = "POST $path HTTP/1.0\r\n";
        $http .= "Host: $host\r\n";
        $http .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $http .= "Content-length: " . strlen($str) . "\r\n";
        $http .= "Connection: close\r\n\r\n";
        $http .= $str . "\r\n\r\n";
        fwrite($fp, $http);
        while (!feof($fp)) { $result .= fgets($fp, 4096); }
        fclose($fp);
        return $result;
    }

    function join_member()  
    {
        $account = $_POST['ACCOUNT'] ;
        $password = substr($account, -4) ;

        $params->USERNAME       = $_POST['USERNAME'] ;
        $params->JUMIN_NO       = $_POST['JUMIN_NO'] ;
        $params->LOGIN_ID       = '';
        $params->PASSWD         = $password ;
        $params->TEL            = $_POST['TEL'] ;
        $params->MOBILE         = $_POST['MOBILE'] ;
        $params->EMAIL          = $_POST['EMAIL'] ;
        $params->ISMAILING      = 'X';
        $params->ZIPCODE        = $_POST['ZIPCODE'] ;
        $params->ADDR1          = $_POST['ADDR1'] ;
        $params->ADDR2          = $_POST['ADDR2'] ;
        $params->P_ID           = $_POST['P_ID'] ;
        $params->R_ID           = $_POST['R_ID'] ;
        $params->CENTER_CD      = $_POST['CENTER_CD'] ;
        $params->BANK_CD        = $_POST['BANK_CD'] ;
        $params->ACCOUNT        = $_POST['ACCOUNT'] ;
        $params->DEPOSITOR      = $_POST['DEPOSITOR'] ;
        $params->RELATION       = "본인" ;
        $params->IS_RECEPTION   = 'X' ;
        $params->CRYPTO         = G_CRYPTO ;
        $params->NEW_USERID     = "" ;
        $params->STATUS         = "" ;
        $params->MESSAGE        = "" ;
    
        // 회원 INSERT
        $result = $this->Member_info_model->execute_procedure("PKG_WEB.REG_MEMBER", $params) ;  

        /*$result->STATUS = 1 ;
        $result->NEW_USERID = "00000196" ;
        $result->MESSAGE = "TEST" ;*/

        echo json_encode($result) ;
    }

    function find_id()
    {
        $USERNAME = $_POST['USERNAME'] ;
        $BIRTH = $_POST['BIRTH'] ;

        $mb = $this->Member_forget_model->find_id( $USERNAME, $BIRTH ) ;

        if ($mb==FALSE) 
        {
            echo "NULL" ;
            return ;
        }   

        echo $mb->LOGIN_ID ;
    }

    function find_pw()
    {
        $USERID = $_POST['USERID'] ;
        $EMAIL = $_POST['EMAIL'] ;
                
        if( $this->Member_forget_model->check( $USERID, $EMAIL ) == FALSE )
        {
            echo "NULL" ;
            return ;
        }

        echo "CHANGED" ;//$this->Member_forget_model->new_pwd( $login_id, $e_mail );
    }

    function get_bank_list()
    {
        $bank_list = $this->S_Bank_model->get_bank_list() ;

        echo json_encode($bank_list);   
    }

    function get_card_list()
    {
        $card_list = $this->S_Card_model->get_card_list() ;

        echo json_encode($card_list);
    }

    function get_center_list()
    {
        $center_list = $this->S_Center_model->get_center_list() ;

        echo json_encode($center_list);   
    }

    function request_credit()
    {
        $result = $this->HB_credit_model->insert() ;        

        if ( $result['CREDIT_IDX'] != -1 )
        {
            //mail_sender("hbncs@naver.com", $result['subject'], "") ;            
        }
        
        echo json_encode($result) ;
    }

    function result_credit()
    {
        $user_id = $_POST['USERID'] ;
        $page = (!empty($_POST['PAGE'])) ? $_POST['PAGE'] : 1 ; 

        $limit = 8 ;
        $offset = ($page - 1) * $limit ;

        $total_count = $this->HB_credit_model->list_count("", "", 0, $user_id) ;
        $result = $this->HB_credit_model->list_result("", "", 0, $limit, $offset, $user_id) ;

        $list = array();
        foreach ($result as $i => $row) 
        {
            $list['RESULT'][$i]                       = $row ;
            $list['RESULT'][$i]['num']                = $total_count - $offset - $i ;

            if ( $row['MOBILE'] == null )
                $list['RESULT'][$i]['MOBILE'] = "" ;
        }

        if ( $total_count > 0 )
        {
            $list['MAX_PAGE'] = (int)floor($total_count / $limit) + ( ($total_count % $limit) ? 1 : 0 ) ;
        }
        else
        {
            $list['MAX_PAGE'] = 1 ;
        }

        echo json_encode($list) ;
    }

    function view_credit()
    {
        $credit_idx = $_POST['CREDIT_IDX'] ;

        $result = $this->HB_credit_model->get_credit_info($credit_idx) ;
        
        echo json_encode($result) ;
    }

    function get_my_request_credit_limit()
    {
        $user_id = $_POST['USERID'] ;

        echo $this->HB_credit_model->get_my_request_credit_limit($user_id) ;
    }

    function get_mobile_list()
    {
        $vender = $_POST['vender'] ;

        $mobile_list = $this->P_pdtmaster_model->mobile_list_for_hbplanner($vender) ;

        echo json_encode($mobile_list);   
    }

    function get_product_list()
    {
        $vender = $_POST['vender'] ;
        $product_type = $_POST['product_type'] ;

        if ( $product_type == '2501' )
        {
            $product_list = $this->Starion_model->list_result(-1, -1);
        }
        else
        {
            $product_list = $this->P_pdtmaster_model->product_list_for_hbplanner($vender, $product_type);
        }

        foreach ($product_list as $key => $mobile)
        {
            if ( ( $mobile['SELL_FORCED'] == 'X' ) && $product_type == '2502' )
            {
                $stock_total_count = $this->OrderMaster_model->get_stock_total_count($mobile['PDT_CD']) ;
                $product_list[$key]['SOLD_OUT'] = ($stock_total_count == 0) ;
                $product_list[$key]['ABOUT_SOLD_OUT'] = ($stock_total_count < 10) ;
            }
            else
            {
                $product_list[$key]['SOLD_OUT'] = false ;
                $product_list[$key]['ABOUT_SOLD_OUT'] = false ;
            }
        }

        echo json_encode($product_list);
    }

    function get_mobile_info()
    {
        $this->output->enable_profiler(TRUE);

        $pdt_cd = $_POST['PDT_CD'] ;
        $vender = '3203' ;

        if ( isset($_POST['VENDER']) )
        {
            $vender = $_POST['VENDER'] ;   
        }

        $mobile_info = $this->P_pdtmaster_model->mobile_info_for_hbplanner($pdt_cd, $vender) ;

        echo json_encode($mobile_info);
    }

    function get_mobile_info2()
    {
        $this->output->enable_profiler(TRUE);

        $pdt_cd = $_POST['PDT_CD'] ;
        $vender = '3203' ;

        if ( isset($_POST['VENDER']) )
        {
            $vender = $_POST['VENDER'] ;
        }

        $mobile_info = $this->P_pdtmaster_model->mobile_info_for_hbplanner2($pdt_cd, $vender) ;

        echo json_encode($mobile_info);
    }

    function get_wired_info()
    {
        $this->output->enable_profiler(TRUE);

        $pdt_cd = $_POST['PDT_CD'] ;
        $vender = '3203' ;

        if ( isset($_POST['VENDER']) )
        {
            $vender = $_POST['VENDER'] ;
        }

        $wired_info = $this->P_pdtmaster_model->wired_info_for_hbplanner($pdt_cd, $vender) ;

        echo json_encode($wired_info);
    }

    function login()
    {
        $this->load->helper('cookie');
        $this->load->library('form_validation');
        $this->form_validation->set_rules(array(
            array('field'=>'mb_id', 'label'=>'아이디', 'rules'=>'trim|required|min_length[3]|max_length[20]|alpha_dash|xss_clean'),
            array('field'=>'mb_password', 'label'=>'비밀번호', 'rules'=>'trim|required')
        ));

        //exit( make_session_result() );

        if ($this->form_validation->run() !== FALSE )
        {
            

            $mb = $this->Basic_model->get_login($this->input->post('mb_id'), $this->input->post('mb_password'));

            

            if ( !$mb || ($mb['PASSWD'] != $mb['PASSWD2']))
            {                
                $result["result"]          = FALSE;
                $result["reason"]          = "비밀번호를 잘못 입력하셨습니다.";

                exit( make_session_result($result) );
            }

            //$result["reason"]          = "1.";
            //exit( make_session_result($result) );

            if ( $mb['ISCLASS'] == 'X' )
            {
                $result["result"]          = FALSE;
                $result["reason"]          = "사업 철회 회원입니다.";

                exit( make_session_result($result) );
            }           

            //exit( make_session_result($mb) );

            $this->session->set_userdata('ss_mb_id', $mb['USERID']);
            $this->session->set_userdata('USERNAME', $mb['USERNAME']);
            $this->session->set_userdata('RANK_NAME', $mb['RANK_NAME']);
            //$this->session->set_userdata('is_mobile', "1", TRUE);
            //$this->session->set_userdata('KT_REG_NO', $mb['KT_REG_NO']);
            //$this->session->set_userdata('SKT_REG_NO', $mb['SKT_REG_NO']);
            //$this->session->set_userdata('LGU_REG_NO', $mb['LGU_REG_NO']);
            $this->session->set_userdata('KAIT_NO', $mb['KAIT_NO']);
            $this->session->set_userdata('LGU_REG_CHECK', $mb['LGU_REG_CHECK']);
            $this->session->set_userdata('SKT_REG_CHECK', $mb['SKT_REG_CHECK']);
            $this->session->set_userdata('KT_REG_CHECK', $mb['KT_REG_CHECK']);
            


            //$result["reason"]          = "2.";
            //exit( make_session_result($result) );


            if ( $mb['IS_RECEPTION'] == 'O' )
            {
                $this->session->set_userdata('is_reception', TRUE);             
            }
            
            if ($this->input->post('chk_reId')) 
            {
                $cookie = array(
                   'name'   => 'ck_mb_id',
                   'value'  => $mb['USERID'],
                   'expire' => 86400*30,
                   'domain' => $this->config->item('cookie_domain')
               );
               set_cookie($cookie);
            }
            else if (get_cookie('ck_mb_id'))
            {
                delete_cookie('ck_mb_id');
            }

            //exit( make_session_result($result) );

            
            $result["result"]           = TRUE;
            $result["reason"]          = "로그인을 성공했습니다.";
            //$result["mobile"]          = $this->session->userdata('is_mobile');

            //$member_info["mem_username"]= $this->member->item('mem_username');
            $member_info                = $mb;

            //$member_info["LGU_REG_NO"]  = $mb['LGU_REG_NO'];

            $result["member_info"]      = $member_info;

            exit( make_session_result($result) );
        }
        
        $result["result"]           = FALSE;
        $result["reason"]          = $this->form_validation->error_string();

        exit( make_session_result($result) );
        //echo make_session_result($result);
    }

    public function home()
    {
        $this->load->helper('cookie');
        $this->load->model(array('Popup_model', 'P_Pdtrecommand_model', 'Rank_model', 'OrderMaster_model', 'Board_notice_model', 'Pay_model', 'Order_model'));

        $user_id = $this->session->userdata('ss_mb_id') ;
        $rank_name = $this->session->userdata('RANK_NAME') ;
        $kait_no = $this->session->userdata('KAIT_NO') ;
        
        $total_amt = $this->OrderMaster_model->get_total_amt($user_id) ;

        $popup_list = $this->Board_notice_model->get_popup_notice() ;

        $cookies = array() ;

        foreach ($popup_list as $key => $value) 
        {           
            $cookies[$key] = get_cookie('notice'.$key) ? 0 : 1 ;
        }        

        $pay_week = $this->Pay_model->get_pay_list('week', $user_id, 'A.PAY_DATE', 'desc', 1, 0) ;
        $pay_week = isset($pay_week[0]) ? $pay_week[0]['TRUE_AMT'] : 0 ;        

        $data = array(
            'cur_month_rank' => $this->Rank_model->get_cur_month_rank($user_id, $rank_name),
            'pay_week'      => number_format($pay_week),
            'count_week'    => $this->Order_model->list_count('', '', '', '', '', 'sub_main', ''),
            'kait_no'    => $kait_no ? $kait_no : "승낙번호를 발급받으세요!",

            //'notice_list' => $this->Board_notice_model->get_main_notice(),
            //'popup_list' => $popup_list,
            //'cookies' => $cookies,
        );      

        exit( make_session_result($data) );
    }

    public function upload_file()
    {
        $target_path = $_POST['upload_path'] ;

        $tmp_img = explode(".",$_FILES['upload_file']['name']) ;
        $img_name = $tmp_img[0].".".$tmp_img[1] ;
        $target_path = $target_path . basename($img_name) ;

        if( move_uploaded_file($_FILES['upload_file']['tmp_name'],$target_path) )
        {
            echo "0000" ;
            //echo "The file ".$img_name." has been uploaded" ;
        }
        else
        {
            echo "9999" ;
            //echo $img_name ;
            //echo "There was an error" ;
        }
    }

    function order_list()
    {
        $user_id = $_POST['USERID'] ;
        $page = (!empty($_POST['PAGE'])) ? $_POST['PAGE'] : 1 ; 

        $limit = 8 ;
        $offset = ($page - 1) * $limit ;

        $type = "" ;
        if ( $_POST['TYPE'] == "O" )
        {
            $type = "reception" ;
        }

        $product_type = "2502" ;

        if ( isset($_POST['PRODUCT_TYPE']) )
        {
            if ( $_POST['PRODUCT_TYPE'] == "wired" )
                $product_type = "2504" ;
            else if ( $_POST['PRODUCT_TYPE'] == "product" )
                $product_type = "2501" ;
        }

        $total_count = $this->Order_model->list_count("", "", "", "", "", $type, $product_type) ;
        $result = $this->Order_model->list_result("ORDNO", "desc", "", "", $limit, $offset, "", "", "", $type, $product_type);

        $o_state_list = $this->S_Order_state_model->get_order_state_list() ;

        if ( $product_type == "2501" )
        {
            $o_state_list = $this->S_Order_state_model->get_starion_order_state_list() ;
        }

        $list = array();
        foreach ($result as $i => $row) 
        {
            if ( $product_type == "2501" )
            {
                $tmp_ord_info = array() ;

                if ( $row['ORD_MSG'] != '' ) // TMP_ORDNO 를 담음
                {
                    $tmp_ord_info = $this->HB_temp_order_starion->get_order_info($row['ORD_MSG']) ;
                }

                $row['REG_NAME'] = $row['ORD_NAME'] ;
                $row['OLD_TEL'] = isset($tmp_ord_info['REG_MOBILE']) ? $tmp_ord_info['REG_MOBILE'] : "" ;
                $row['PAY_TYPE'] = isset($tmp_ord_info['PAY_TYPE']) ? $tmp_ord_info['PAY_TYPE'] : "" ;
            }

            $list['RESULT'][$i]                       = $row ;
            $list['RESULT'][$i]['num']                = $total_count - $offset - $i ;
            $list['RESULT'][$i]['ORD_STATE']          = $o_state_list[$row['STATE_CD']] ;
        }

        if ( $total_count > 0 )
        {
            $list['MAX_PAGE'] = (int)floor($total_count / $limit) + ( ($total_count % $limit) ? 1 : 0 ) ;
        }
        else
        {
            $list['MAX_PAGE'] = 1 ;
        }

        $list['TOTAL_COUNT'] = $total_count ;

        echo json_encode($list) ;
    }

    function check_version()
    {
        $version = $this->input->post('version');
        if( CURR_HBPLANNER_VER != $version )
        {
            $result["result"]          = FALSE;
            $result["reason"]          = "버전이 맞지 않습니다. 최신버전으로 업데이트 해주세요.";
            $result["version"]         = CURR_HBPLANNER_VER;
        }
        else
        {
            $result["result"]          = TRUE;
            $result["reason"]          = "최신버전 입니다.";
            $result["version"]         = CURR_HBPLANNER_VER;
        }
        

        exit( make_session_result($result) );
    }

    function view_order_mobile() 
    {
        $ORDID = $this->input->post('ORDID');

        $order_info = $this->Order_model->get_order_info($ORDID) ;

        $vender_list = array('없음', 'KT', 'SKT', 'LGU+', 'MVNO') ;
        $order_info['OLD_TEL_COMP'] = $vender_list[$order_info['OLD_TEL_COMP']] ;

        $vender_list = array('', '모바일M청구서', '스마트청구서', '이메일') ;
        $order_info['BILL_KIND'] = $vender_list[$order_info['BILL_KIND']] ;

        $center_list = $this->S_Center_model->get_center_list(true) ;

        $order_info['CENTER_NAME'] = $center_list[$order_info['CENTER_CD']] ;
        $order_info['START_CENTER'] = $center_list[$order_info['START_CENTER']] ;

        $order_info['OLD_TEL'] = typedTel($order_info['OLD_TEL']) ;                
        $order_info['RECV_TEL'] = typedTel($order_info['RECV_TEL']) ;                
        $order_info['RECV_MOBILE'] = typedTel($order_info['RECV_MOBILE']) ;

        $order_info['BENEFIT_SERVICE'] = ( $order_info['BENEFIT_SERVICE'] == 'O' ) ? '사용' : '미사용' ;
        $order_info['ADD_SERVICE'] = ( $order_info['ADD_SERVICE'] == 'O' ) ? '사용' : '미사용' ;

        echo json_encode($order_info) ;
    }

    function view_wired() 
    {
        $ORDID = $this->input->post('ORDID');

        $order_info = $this->O_Order_Ctel_model->get_order_info($ORDID) ;

        if ( $order_info['RECEPTION_ID'] == 'D0000000' )
        {
            $order_info['RECEPTION'] = '' ;
        }
        else
        {
            $order_info['RECEPTION'] = $order_info['RECEPTION_NAME']." (".$order_info['RECEPTION_ID'].")" ;
        }

        echo json_encode($order_info) ;
    }

    function view_starion()
    {
        $ORDID = $this->input->post('ORDID');

        $order_info = $this->HB_temp_order_starion->get_real_order_info($ORDID) ;

        $tmp_ord_info = array() ;
        if ( $order_info['ORD_MSG'] != '' ) // TMP_ORDNO 를 담음
        {
            $tmp_ord_info = $this->HB_temp_order_starion->get_order_info($order_info['ORD_MSG']) ;
        }

        $order_info['REG_NAME'] = isset($tmp_ord_info['REG_NAME']) ? $tmp_ord_info['REG_NAME'] : '' ;
        $order_info['INS_DATE'] = isset($tmp_ord_info['INS_DATE']) ? $tmp_ord_info['INS_DATE'] : '' ;
        $order_info['GIFT_TYPE'] = isset($tmp_ord_info['GIFT_TYPE']) ? $tmp_ord_info['GIFT_TYPE'] : '' ;
        $order_info['PAY_TYPE'] = isset($tmp_ord_info['PAY_TYPE']) ? $tmp_ord_info['PAY_TYPE'] : '' ;
        $order_info['DEPOSITOR'] = isset($tmp_ord_info['DEPOSITOR']) ? $tmp_ord_info['DEPOSITOR'] : '' ;
        $order_info['ORD_MSG'] = isset($tmp_ord_info['ORD_MSG']) ? $tmp_ord_info['ORD_MSG'] : '' ;
        $order_info['DEP_BIRTHDAY'] = '' ;

        if ( $order_info['RECEPTION_ID'] == 'D0000000' )
        {
            $order_info['RECEPTION'] = '' ;
        }
        else
        {
            $order_info['RECEPTION'] = $order_info['RECEPTION_NAME']." (".$order_info['RECEPTION_ID'].")" ;
        }

        echo json_encode($order_info) ;
    }

    function request_open()
    {
        $ORDID = $this->input->post('ORDID');

        if ( $ORDID == "" )        
            return ;

        $order_info = $this->Order_model->get_order_info($ORDID) ;

        if( $order_info && $this->Order_state_model->request_open($ORDID) )
        {
            mail_sender("hbncs@naver.com", "명의자 ".$order_info['REG_NAME']."(".$order_info['ORDNO'].") 개통 요청", "") ;                   

            echo "개통요청이 완료되었습니다." ;            
        }
        else
        {
            echo "개통요청이 실패하였습니다." ;            
        }        
    }

    function mail_test()
    {
        $email = "" ;
        $in_ordno = "20160524S13052" ;

        if ( $in_ordno == "" )        
            return ;        

        echo $this->S_Center_model->get_order_center_manager_id($in_ordno) ;     

        $email = $this->S_Center_model->get_order_center_email($in_ordno) ;        

        echo "email : ".$email ;

        if ( $email != "" )
        {
            $order_info = $this->Order_model->get_order_info($in_ordno) ;       

            echo "없다" ;     

            if ( $order_info )
            {                
                echo "있다" ;
                //mail_sender($email, "명의자 ".$order_info['REG_NAME']."(".$order_info['ORDNO'].")가 개통 완료 되었습니다.", "") ;            
                //mail_sender("sadlyfox@gmail.com", "명의자 ".$order_info['REG_NAME']."(".$order_info['ORDNO'].")가 개통 완료 되었습니다.", "") ;            
            }
        }
    }

    function get_policy_list()
    {
        $bo_field = ' "bo_table", "bo_subject", "bo_admin", "bo_list_level", "bo_write_level",
                  "bo_use_private", "bo_use_rss", "bo_use_category", "bo_use_sideview", "bo_subject_len",
                  "bo_page_rows", "bo_new", "bo_hot", "bo_skin", "bo_sort_field", "bo_head", "bo_tail",
                  "bo_use_extra", "bo_count_write", "bo_notice", "BO_MIN_WR_NUM" ';
        $wr_field = ' "wr_id", "wr_subject", "wr_hit", "mb_id", "wr_name", to_char("wr_datetime", \'yyyy-mm-dd\') as wr_datetime ';

        $board = $this->Basic_model->get_board('POLICY', $bo_field, TRUE) ;

        $user_id = $_POST['USERID'] ;
        $page = (!empty($_POST['PAGE'])) ? $_POST['PAGE'] : 1 ;

        $limit = 8 ;
        $offset = ($page - 1) * $limit ;

        $total_count = $board['bo_count_write'];

        $list['RESULT'] = $this->Board_model->list_result('POLICY', '', '', 'wr_id', 'desc', '','', $limit, $offset, $wr_field);

        if ( $total_count > 0 )
        {
            $list['MAX_PAGE'] = (int)floor($total_count / $limit) + ( ($total_count % $limit) ? 1 : 0 ) ;
        }
        else
        {
            $list['MAX_PAGE'] = 1 ;
        }

        $list['TOTAL_COUNT'] = $total_count ;

        echo json_encode($list) ;
    }

    function get_policy()
    {
        $wr_field = ' "wr_content" ';

        $wr_id = $_POST['POLICY_IDX'] ;

        $result = $this->Basic_model->get_write('POLICY', $wr_id, $wr_field) ;

        echo $result['wr_content']->load() ;
    }

    function insert_wired()
    {
        $stockprice_info = $this->Policy_model->get_stock_price($_POST["PDT_CD"]) ;
        $sum_price_info = $this->Policy_model->get_sum_point_info($_POST["PTP_ID"], $_POST["PDT_CD"]) ;

        $result = $this->HB_temp_order_wired->insert($stockprice_info, $sum_price_info) ;

        echo json_encode($result) ;
    }

    function insert_starion()
    {
        $stockprice_info = $this->Policy_model->get_stock_price($_POST["PDT_CD"]) ;
        $point_info = $this->Policy_model->get_point_info($_POST["PDT_CD"]) ;

        $result = $this->HB_temp_order_starion->insert2($stockprice_info, $point_info) ;

        echo json_encode($result) ;
    }

    function get_starion_gift_list()
    {
        $result = $this->HB_product_gift_model->get_gift_type($_POST["PDT_CD"]) ;

        echo json_encode($result) ;
    }
}
?>