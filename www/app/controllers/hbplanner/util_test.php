<?php
class util_test extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array(ADM_F.'/lecture_model', 'HB_temp_order_wired', 'S_Order_state_model', 'Policy_model', 'HB_temp_order_starion'));

        //$this->output->enable_profiler(TRUE);
	}

    function get_lecture_list()
    {
        $page = (!empty($_POST['PAGE'])) ? $_POST['PAGE'] : 1 ;

        $limit = 8 ;
        $offset = ($page - 1) * $limit ;

        $result = $this->lecture_model->list_result('REG_DATE', 'desc', "", "", $limit, $offset) ;

        $total_count = $result['total_cnt'];
        $list['RESULT'] = $result['qry'] ;

        if ( $total_count > 0 )
        {
            $list['MAX_PAGE'] = (int)floor($total_count / $limit) + ( ($total_count % $limit) ? 1 : 0 ) ;
        }
        else
        {
            $list['MAX_PAGE'] = 1 ;
        }

        $list['TOTAL_COUNT'] = $total_count ;

        echo json_encode($list) ;
    }

    function get_lecture()
    {
        $lecture_idx = $_POST['LECTURE_IDX'] ;

        $lecture = $this->lecture_model->get_one($lecture_idx);
        $this->lecture_model->update_plus($lecture_idx, 'VIEW_COUNT', 1);

        $lecture["URL"] = $lecture["URL"] ;

        echo $lecture["URL"] ;
    }

    function wired_order_list()
    {
        $user_id = $_POST['USERID'] ;
        $page = (!empty($_POST['PAGE'])) ? $_POST['PAGE'] : 1 ;

        $limit = 8 ;
        $offset = ($page - 1) * $limit ;

        $bReception = false ;
        if ( isset($_POST['TYPE']) && $_POST['TYPE'] == "O" )
        {
            $bReception = true ;
        }

        $total_count = $this->HB_temp_order_wired->list_count("", "", "", "", $bReception) ;
        $result = $this->HB_temp_order_wired->list_result("TMP_ORDNO", "desc", "", "", $limit, $offset, "", "", $bReception);

        $o_state_list = $this->S_Order_state_model->get_order_state_list() ;

        $list = array();
        foreach ($result as $i => $row)
        {
            $list['RESULT'][$i]                       = $row ;
            $list['RESULT'][$i]['num']                = $total_count - $offset - $i ;
            $list['RESULT'][$i]['ORD_STATE']          = $o_state_list[$row['STATE_CD']] ;
        }

        if ( $total_count > 0 )
        {
            $list['MAX_PAGE'] = (int)floor($total_count / $limit) + ( ($total_count % $limit) ? 1 : 0 ) ;
        }
        else
        {
            $list['MAX_PAGE'] = 1 ;
        }

        $list['TOTAL_COUNT'] = $total_count ;

        echo json_encode($list) ;
    }

    function view_order_wired()
    {
        $ORDID = $this->input->post('ORDID');

        $order_info = $this->HB_temp_order_wired->get_order_info($ORDID) ;
        $order_info['PV2'] = $this->Policy_model->get_sum_point_info($order_info['PTP_ID'], $order_info['PDT_CD'])->POINT2 ;

        echo json_encode($order_info) ;
    }

    function starion_order_list()
    {
        $user_id = $_POST['USERID'] ;
        $page = (!empty($_POST['PAGE'])) ? $_POST['PAGE'] : 1 ;

        $limit = 8 ;
        $offset = ($page - 1) * $limit ;

        $bReception = false ;
        if ( isset($_POST['TYPE']) && $_POST['TYPE'] == "O" )
        {
            $bReception = true ;
        }

        $total_count = $this->HB_temp_order_starion->list_count("", "", "", $bReception) ;
        $result = $this->HB_temp_order_starion->list_result("TMP_ORDNO", "desc", "", "", $limit, $offset, "", $bReception);

        $o_state_list = $this->S_Order_state_model->get_starion_order_state_list() ;

        $list = array();
        foreach ($result as $i => $row)
        {
            $list['RESULT'][$i]                       = $row ;
            $list['RESULT'][$i]['num']                = $total_count - $offset - $i ;
            $list['RESULT'][$i]['ORD_STATE']          = $o_state_list[$row['STATE_CD']] ;
        }

        if ( $total_count > 0 )
        {
            $list['MAX_PAGE'] = (int)floor($total_count / $limit) + ( ($total_count % $limit) ? 1 : 0 ) ;
        }
        else
        {
            $list['MAX_PAGE'] = 1 ;
        }

        $list['TOTAL_COUNT'] = $total_count ;

        echo json_encode($list) ;
    }

    function view_order_starion()
    {
        $ORDID = $this->input->post('ORDID');

        $order_info = $this->HB_temp_order_starion->get_order_info($ORDID) ;

        echo json_encode($order_info) ;
    }
}
?>