<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('Order_state_model'));
		
		//$this->output->enable_profiler(TRUE);
	}

	function view()
	{
		if (!IS_MEMBER) 
		{
			alert('잘못 된 접근입니다.', '/') ;
		}
	
		$this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소

		$seg      =& $this->seg ;

		$view  = $seg->get('view', 'new');
		$ord_no  = $seg->get('ord_no', 0); 
		$vender  = $seg->get('vender', '3203');

        if ( $ord_no == 0 )
        {
            alert_close('잘못된 정보입니다.');
            return false ;
        }

		$head = array('title' => '가입신청서');

		$memo2 = $this->Order_state_model->get_ord_manager_msg($ord_no) ;
		$result = split("/", $memo2) ;

		$data = array() ;
		$data['ORDNO'] = $ord_no ;

		$j = 2 ;
		$i = count($result) - 1 ;
		while ( $j >= 0 ) 
		{
			$data['values'][$j] = isset($result[$i]) ? $result[$i] : "" ;

			$i-- ;
			$j-- ;
		}

		if ( $vender == "3203" )
		{
			$vender = 'lg' ;
		}
		else if ( $vender == "3201" )
		{
			$vender = 'kt' ;
		}
        else if ( $vender == "3207" )
        {
            $vender = 'umobi' ;
        }
        else if ( $vender == "3208" )
        {
            $vender = 'mmobile' ;
        }

		widget::run('head', $head);
        $this->load->view('hbplanner/order/print_'.$vender.'_'.$view, $data);
		widget::run('tail');
	}

    function view_all()
    {
        if (!IS_MEMBER)
        {
            alert('잘못 된 접근입니다.', '/') ;
        }

        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg ;

        $ord_no  = $seg->get('ord_no', 0);
        $vender  = $seg->get('vender', '3203');
        $view  = $seg->get('view_all', 'new');

        if ( $ord_no == 0 )
        {
            alert_close('잘못된 정보입니다.');
            return false ;
        }

        $head = array('title' => "주문서 프린트(".$ord_no.")");

        $memo2 = $this->Order_state_model->get_ord_manager_msg($ord_no) ;
        $result = split("/", $memo2) ;

        $data = array() ;
        $data['ORDNO'] = $ord_no ;
        $data['vender'] = $vender ;
        $data['view'] = $view ;

        $j = 2 ;
        $i = count($result) - 1 ;
        while ( $j >= 0 )
        {
            $data['values'][$j] = isset($result[$i]) ? $result[$i] : "" ;

            $i-- ;
            $j-- ;
        }

        $files = array('order', 'agree', 'private', 'select_agree', 'guide', 'kt_sponsor1', 'kt_sponsor2', 'id_card') ;

        foreach ($files as $key => $file)
        {
            $file_path = "/data/hbplanner/".$file."/".$ord_no.".png" ;

            if ( $file == 'id_card' )
            {
                $file_path = "/data/hbplanner/".$file."/".$ord_no."_card.png" ;
            }
            
            if ( file_exists(".".$file_path) )
            {
                $data['files'][$key] = $file_path ;
            }
        }

        widget::run('head', $head);
        $this->load->view('hbplanner/order/print_universal', $data);
        widget::run('tail');
    }
}