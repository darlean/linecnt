<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lists extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

		$this->load->library('form_validation');
		$this->load->model(array('Product_model'));
        $this->load->helper(array( 'url' ));
        
        define('WIDGET_SKIN', 'main');
        define('CSS_SKIN', 'shop.style');

        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {         
    	if (!IS_MEMBER)
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'page':
            case 'index':
                $this->_list();
            break;
            default:
                show_404();
            break;
        }
    }

    function _list() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $cs_no = $seg->get('cs_no');   // 게시물아이디
        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $seg->replace('cs_no').$param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sfl   = $param->get('sfl');   // 검색필드
        $stx   = $param->get('stx');   // 검색어
        $sca   = $param->get('sca');   // 분류

        // 정렬
        if (!$sst) 
        {
            $sst = 'hb_product.pt_id';
            $sod = 'desc';            
        }
        else
        {
            //$sst = preg_match("/^(pc_id|pc_commission_value|pc_promotion_value|pc_start_date|pc_end_date|pc_price|pc_instalment_plan|pc_membership_cost|pc_usim_cost|pc_agree_plan)$/i", $sst) ? $sst : FALSE;
        }
        
        $total_count = $this->Product_model->list_count($sfl, $stx) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/product/lists/page/';
        $config['per_page']    = 12;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->Product_model->list_result($sst, $sod, $sfl, $stx, $config['per_page'], $offset) ;

        foreach ($result as $key => $product) 
        {   
            if ( $product['pt_thumbnail'] != '' )
            {
                $thumbnails = explode (",", $product['pt_thumbnail']);

                $result[$key]['pt_thumbnail'] = $thumbnails[0] ;
            }

            $started_days = (isset($product['pc_start_date'])) ? floor((time() - strtotime($product['pc_start_date']))/60/60/24) : 0 ;

            $result[$key]['new_item'] = ($started_days < 3) ? true : false ;
            $result[$key]['left_days'] = (isset($product['pc_end_date'])) ? floor((strtotime($product['pc_end_date']) - time())/60/60/24) : 0 ;
        }

        $data = array(
            'total_count' => $total_count,  
            'sst' => $sst,
            'sod' => $sod,          
            'sca' => $sca,
            'sfl' => $sfl,
            'stx' => $stx,
            'list' => $result,
            'paging' => $CI->pagination->create_links(),  
        );

        widget::run('head');
        $this->load->view('product/lists', $data);
        widget::run('tail');
    }
}
?>