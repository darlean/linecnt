<?php
class Card extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('S_Reason_model', 'HB_card_place_model', 'HB_card_detail2_model', 'HB_card2_model', 'P_pdtmaster_model'));

		define('WIDGET_SKIN', 'main');
        define('CSS_SKIN', 'ace_file_input');

		$this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

		//$this->output->enable_profiler(TRUE);
	}

	function lists()	
	{
		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $vender  = $seg->get('vender'); // 통신사
        $page  = $seg->get('page', 1); // 페이지
        
        $vender_list = $this->S_Reason_model->get_vender_list(false, true) ;

        if ( $vender == '' )
        {
            $vender = key($vender_list) ;
        }
                
        $total_count = $this->HB_card2_model->list_count($vender) ;

        $config['suffix']       = '';
        $config['base_url']    = RT_PATH.'/product/card/lists/vender/'.$vender.'/page/';
        $config['per_page']    = 10 ;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];
        
        $result = $this->HB_card2_model->list_result($config['per_page'], $offset, $vender) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
            $list[$i]['href']               = '/product/card/view/pdt_cd/'.$row['PDT_CD'];
        }

        $head = array('title' => '카드 관리') ;
        $data = array(
            'total_count' => $total_count,   
            'list' => $list,
            'paging' => $CI->pagination->create_links(),  
            'vender' => $vender,
            'vender_list' => $vender_list,
        );

		widget::run('head', $head);
		$this->load->view('product/card_lists', $data);
		widget::run('tail');
	}

	function view()
	{
		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $pdt_cd  = $seg->get('pdt_cd');         
        $card_info = $this->HB_card2_model->get_card_info($pdt_cd) ;       
        
		$head = array('title' => '카드 정보 수정') ;
        $data = array(
        	'card_info'   => $card_info,
        	'vender_list' => $this->S_Reason_model->get_vender_list(false, true),
        ) ;

		widget::run('head', $head);
		$this->load->view('product/view_card', $data);
		widget::run('tail');		
	}

	function update()
	{
		$pdt_cd = $this->input->post('PDT_CD') ;
		$vender = $this->input->post('TEL_COMPANY') ;

        $rst_data = $this->upload_files() ;

        if ( $rst_data['result'] == FALSE )
        {
           alert($rst_data['error']) ;
        }

        $card_info = $this->HB_card2_model->is_card_info($pdt_cd) ;

        if ( $card_info )
        {
            $result = $this->HB_card2_model->update($pdt_cd, $rst_data['IMG_PATH']) ;    	
        }
        else
        {
            $result = $this->HB_card2_model->insert($rst_data['IMG_PATH']) ;          
        }

    	if ( $result )
    	{
    		alert('카드 정보가 수정되었습니다.', 'product/card/lists/vender/'.$vender) ;
    	} 
	}

	function card_detail() 	
	{
		$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $pdt_cd  = $seg->get('pdt_cd');        
        
        $card_info = $this->HB_card2_model->get_card_info($pdt_cd) ;       

		$head = array('title' => '카드 상세 정보') ;
        $data = array(        
        	'card_place_list' 	=> $this->HB_card_place_model->get_card_place_list(),
        	'card_info'   		=> $card_info,
        	'card_detail_info'  => $this->HB_card_detail2_model->get_card_detail_info($pdt_cd),
        ) ;

		widget::run('head', $head);
		$this->load->view('product/card_detail', $data);
		widget::run('tail');
	}	

	function add_detail()
	{
        $vender = $this->input->post('TEL_COMPANY') ;
        
		$result = $this->HB_card_detail2_model->insert() ;

        if ( $result )
        {        
            alert('등록되었습니다.', 'product/card/lists/vender/'.$vender) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
	}

    public function upload_files()
    {
        $IMG_PATH = $this->input->post('IMG_PATH') ;

        $rst_data['result'] = TRUE ;
        $rst_data['IMG_PATH'] = '' ;

        if ( count($_FILES) > 0 )
        {      
            $this->load->library('upload') ;

            $config['upload_path'] = "./data/card_image/" ;
            $config['open_file_path'] = "/data/card_image/" ;

            $config['allowed_types'] = 'gif|jpg|png|gif|bmp|pdf';
            $config['max_size'] = '1536' ;
            $config['max_width'] = '3200';
            $config['max_height'] = '3200';
            
            if(!is_dir($config['upload_path']))
            {
                umask( 0 );
                mkdir($config['upload_path'], 0777) ;
            }

            for ( $i = 1 ; $i <= count($_FILES) ; $i++ ) 
            {
                $file_form_name = 'file'.$i ;

                if ( $_FILES[$file_form_name]["size"] > 0 && $_FILES[$file_form_name]["name"] != 'none' )
                {
                    if ($_FILES[$file_form_name]["error"] <= 0)
                    {
                        $ext = array_pop(explode(".", $_FILES[$file_form_name]['name'])) ; // 확장자
                        $config['file_name'] = "card_img_".time()."_".$i.".".$ext ;  

                        $this->upload->initialize($config) ;

                        if($this->upload->do_upload($file_form_name))
                        {
                            $rst_data['IMG_PATH'] = $config['open_file_path'].$config['file_name'] ;               
                        }
                        else
                        {
                            $rst_data['result'] = FALSE ;
                            $rst_data['error'] = $this->upload->display_errors() ;

                            return $rst_data ;
                        }
                    }
                }
                else
                {
                    if ( isset($IMG_PATH) )
                    {
                        $rst_data['IMG_PATH'] = $IMG_PATH ;
                    }
                }
            }
        }

        return $rst_data ;
    }
}
?>