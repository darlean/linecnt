<?php
class Mobile extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('HB_pdt_link_model'));

		define('WIDGET_SKIN', 'main');
		define('CSS_SKIN', 'pages/page_pricing');

		//$this->output->enable_profiler(TRUE);
	}

	function kt()
	{
        $data = $this->_data() ;
        $head = array('title' => 'KT 상품 관리') ;
		widget::run('head', $head);
		$this->load->view('product/mobile', $data);
		widget::run('tail');
	}

	function update_kt()
	{
        $list = array() ;

        foreach($_POST as $key=>$value)
        {
            if(strpos($key, "INSERT_LINK_TYPE_") !== false)
            {
                $pdt_cd = str_replace( "INSERT_LINK_TYPE_", "", $key ) ;

                $list[$pdt_cd]['PDT_CD'] = $pdt_cd ;
                $list[$pdt_cd]['INSERT_LINK_TYPE'] = $value ;
            }
            else
            {
                continue ;
            }
        }

        foreach ($list as $pdt_cd => $row)
        {
            $list[$pdt_cd]['NEW_LINK_PATH'] = $this->input->post('NEW_LINK_PATH_'.$pdt_cd) ;
            $list[$pdt_cd]['TR_LINK_PATH'] = $this->input->post('TR_LINK_PATH_'.$pdt_cd) ;
            $list[$pdt_cd]['RE_LINK_PATH'] = $this->input->post('RE_LINK_PATH_'.$pdt_cd) ;

            $result = $this->HB_pdt_link_model->update($list[$pdt_cd]) ;
        }

    	if ( $result )
    	{
    		alert('정보가 저장되었습니다.', URL) ;
    	}
	}

    function _data()
    {
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $page  = $seg->get('page', 1); // 페이지

        $total_count = $this->HB_pdt_link_model->list_count('3201') ;

        $config['suffix']       = '';
        $config['base_url']    = RT_PATH.'/product/mobile/kt/page/';
        $config['per_page']    = 10 ;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->HB_pdt_link_model->list_result($config['per_page'], $offset, '3201') ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row)
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
        }

        $data = array(
            'total_count' => $total_count,
            'list' => $list,
            'paging' => $CI->pagination->create_links(),
        );

        return $data ;
    }

	function test2()
    {
        $data = $this->_data() ;

        $this->load->library('excel') ;
        $objPHPExcel = PHPExcel_IOFactory::load('./data/excel/b.xlsx');

        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        // 한줄읽기
        for ($row = 2; $row <= $highestRow; $row++)
        {
            // $rowData가 한줄의 데이터를 셀별로 배열처리 됩니다.
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            if ( $rowData[0][0] != "" && $rowData[0][0] != "PDT_CD" )
            {
                foreach ($data['list'] as $i => $row) {
                    if ( $row['PDT_CD'] == $rowData[0][0] )
                    {
                        $data['list'][$i]['INSERT_LINK_TYPE'] = $rowData[0][1] ;
                        $data['list'][$i]['NEW_LINK_PATH'] = $rowData[0][2] ;
                        $data['list'][$i]['TR_LINK_PATH'] = $rowData[0][3] ;
                        $data['list'][$i]['RE_LINK_PATH'] = $rowData[0][4] ;

                        break ;
                    }
                }
            }
        }

        $head = array('title' => 'KT 상품 관리') ;
        widget::run('head', $head);
        $this->load->view('product/mobile', $data);
        widget::run('tail');
    }

    function test()
    {
        //define('WIDGET_SKIN', 'main');

        $this->load->library('excel') ;
        $objPHPExcel = PHPExcel_IOFactory::load('./data/excel/b.xlsx');
        $sheetsCount = $objPHPExcel->getSheetCount();

        // 쉬트별로 읽기
        /*for($i = 0; $i < $sheetsCount; $i++)
        {
            $objPHPExcel->setActiveSheetIndex($i);
            $sheet = $objPHPExcel->getActiveSheet();
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            // 한줄읽기
            for ($row = 1; $row <= $highestRow; $row++)
            {
                // $rowData가 한줄의 데이터를 셀별로 배열처리 됩니다.
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                var_dump($rowData) ;
            }
        }*/

        // 첫번째 시트를 선택
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        echo "<table>" ;

        $rowIterator = $objWorksheet->getRowIterator();

        foreach ($rowIterator as $row)
        {
            echo "<tr>" ;

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            foreach ($cellIterator as $cell) // 해당 열의 모든 셀에 대해서
            {
                echo "<td>" ;
                echo $cell->getValue(); // 해당 셀 값을 표시한다
                echo "</td>" ;
            }

            echo "</tr>" ;
        }

        echo "</table>" ;
    }
}
?>