<?php
class Gifts extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(array('HB_product_gift_model'));

		define('WIDGET_SKIN', 'main');
		define('CSS_SKIN', 'pages/page_pricing');

		//$this->output->enable_profiler(TRUE);
	}

    function _remap($index)
    {
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'page':
            case 'index':
                $this->index() ;
                break;
            case 'update' :
                $this->update() ;
                break;
            default:
                show_404();
                break;
        }
    }

	function index()
	{
        $data = $this->_data() ;
        $head = array('title' => '일반상품 사은품 관리') ;
		widget::run('head', $head);
		$this->load->view('product/gifts', $data);
		widget::run('tail');
	}

	function update()
	{
        $list = array() ;

        foreach($_POST as $key=>$value)
        {
            if(strpos($key, "GIFT_1_") !== false)
            {
                $pdt_cd = str_replace( "GIFT_1_", "", $key ) ;

                $list[$pdt_cd]['PDT_CD'] = $pdt_cd ;
                $list[$pdt_cd]['GIFT_1'] = $value ;
                $list[$pdt_cd]['GIFT_2'] = $this->input->post('GIFT_2_'.$pdt_cd) ;
                $list[$pdt_cd]['GIFT_3'] = $this->input->post('GIFT_3_'.$pdt_cd) ;

                $result = $this->HB_product_gift_model->update($list[$pdt_cd]) ;
            }
            else
            {
                continue ;
            }
        }

        if ( $result )
    	{
    		alert('정보가 저장되었습니다.', URL) ;
    	}
	}

    function _data()
    {
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg;

        $page  = $seg->get('page', 1); // 페이지

        $total_count = $this->HB_product_gift_model->list_count() ;

        $config['suffix']       = '';
        $config['base_url']    = RT_PATH.'/product/gifts/page/';
        $config['per_page']    = 10 ;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->HB_product_gift_model->list_result($config['per_page'], $offset) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row)
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
        }

        $data = array(
            'total_count' => $total_count,
            'list' => $list,
            'paging' => $CI->pagination->create_links(),
        );

        return $data ;
    }
}
?>