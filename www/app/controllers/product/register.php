<?php
class Register extends CI_Controller {
	function __construct() {
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model(array('Product_model'));

		define('CSS_SKIN', 'editor,shop.style,ace_file_input');

		define('BO_TABLE', 'test');
		define('BO_HEAD', FALSE);
		define('BO_TAIL', FALSE);
	}

    function _remap($index)
    {         
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'index':
                define('WIDGET_SKIN', 'main');
                $this->_form();
            break;
            case 'pt_id':
                $this->_modify() ;
            break;
            case 'del':
                $this->_delete() ;
            break;
            case 'update':
                $this->_update() ;
            break;
            default:
                define('WIDGET_SKIN', 'main');
                show_404();
            break;
        }
    }

	function _form() 
	{
		$editorConfig = array( // Editor 설정
				'editor' => array(
					'initializedId' => '1', // 에디터 넘버링
					'wrapper'	    => 'tx_trex_container', // 에디터 컨테이너 박스 아이디
					'form'		    => 'fwrite', // 폼 이름
					'field'			=> 'pt_contents', // 필드 이름
				)
			);

        $editor = $this->load->view('board/editor', $editorConfig['editor'], TRUE);

		$js[] = '../../editor/js/editor_loader'; 
		$js[] = 'editor_config';

		$token = get_token();

		$head = array('title' => '상품등록');
		$data = array(
			'editor' => $editor,
			'editorConfig' => json_encode($editorConfig),
            'pt_id' => 0,
            'pt_thumbnail' => '',
			'pt_name' => $this->input->post('pt_name'),
			'pt_model_name' => $this->input->post('pt_model_name'),
			'pt_color' => $this->input->post('pt_color'),
			'pt_contents' => $this->input->post('pt_contents'),
            'btn_contents' => '상품등록',
			'token'  => $token,
		);

        widget::run('head', $head);
		$this->load->view('product/register', $data);
		widget::run('tail', array('js' => $js));
	}

    function _modify() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $pt_id  = $seg->get('pt_id', 0); 

        if ( $pt_id == 0 )
        {
            alert('잘못된 접근입니다.', '/product/lists');
            return false ;
        }

        $product_info = $this->Product_model->get_product_info($pt_id) ;

        $editorConfig = array( // Editor 설정
                'editor' => array(
                'initializedId' => '1', // 에디터 넘버링
                'wrapper'       => 'tx_trex_container', // 에디터 컨테이너 박스 아이디
                'form'          => 'fwrite', // 폼 이름
                'field'         => 'pt_contents', // 필드 이름
                'content'       => $product_info['pt_contents'],  // 내용
                )
            );

        $editor = $this->load->view('board/editor', $editorConfig['editor'], TRUE);

        $js[] = '../../editor/js/editor_loader'; 
        $js[] = 'editor_config';

        $token = get_token();

        $thumbnails = explode (",", $product_info['pt_thumbnail']);

        $head = array('title' => '상품수정 / 상품삭제');
        $data = array(
            'editor' => $editor,
            'editorConfig' => json_encode($editorConfig),
            'pt_id' => $product_info['pt_id'],
            'pt_name' => $product_info['pt_name'],
            'pt_model_name' => $product_info['pt_model_name'],
            'pt_color' => $product_info['pt_color'],
            'pt_thumbnail' => $product_info['pt_thumbnail'],
            'thumbnails' => $thumbnails,
            'btn_contents' => '상품수정',
            'token'  => $token,
        );

        widget::run('head', $head);
        $this->load->view('product/register', $data);
        widget::run('tail', array('js' => $js));
    }

	function _update()
	{
		check_token('product/register') ;

		$this->load->helper('chkstr');	
		$config = array(
			array('field'=>'pt_name', 'label'=>'상품명', 'rules'=>'trim|required|max_length[45]'),
			array('field'=>'pt_model_name', 'label'=>'모델명', 'rules'=>'trim|required|max_length[45]'),
			array('field'=>'pt_color', 'label'=>'색상', 'rules'=>'trim|required|max_length[45]'),
			array('field'=>'pt_contents', 'label'=>'상품설명', 'rules'=>'trim|required|max_length[500]'),
		);

		$this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) 
        {
            $this->index();
        }
        else 
        {
        	$rst_data = $this->upload_files() ;

            if ( $rst_data['result'] == FALSE )
            {
               alert($rst_data['error']) ;
            }

            $sql = array(
                'pt_name' => $this->input->post('pt_name'),
                'pt_model_name' => $this->input->post('pt_model_name'),
                'pt_color' => $this->input->post('pt_color'),
                'pt_contents' => $this->input->post('pt_contents'),
                'pt_thumbnail' => $rst_data['pt_thumbnail']
            );

            $pt_id = $this->input->post('pt_id') ;

            if ( $pt_id == 0 )
            {
           	    $this->Product_model->insert($sql) ;
                alert('상품이 등록되었습니다.', 'product/lists') ;
            }
            else
            {
                $this->Product_model->update($pt_id, $sql) ;
                alert_parent_reload('상품정보가 수정되었습니다.') ;
            }
        }
	}

	public function upload_files()
    {
        $thumbnails = explode (",", $this->input->post('pt_thumbnail'));

        $rst_data['result'] = TRUE ;
        $rst_data['pt_thumbnail'] = '' ;

        if ( count($_FILES) > 0 )
        {      
            $this->load->library('upload') ;

            $config['upload_path'] = "./data/product/" ;
            $config['open_file_path'] = "/data/product/" ;

            $config['allowed_types'] = 'gif|jpg|png|gif|bmp|pdf';
            $config['max_size'] = '1536' ;
            $config['max_width'] = '3200';
            $config['max_height'] = '3200';
            
            if(!is_dir($config['upload_path']))
            {
                umask( 0 );
                mkdir($config['upload_path'], 0777) ;
            }

            for ( $i = 1 ; $i <= count($_FILES) ; $i++ ) 
            {
                $file_form_name = 'file'.$i ;

                if ( $_FILES[$file_form_name]["size"] > 0 && $_FILES[$file_form_name]["name"] != 'none' )
                {
                    if ($_FILES[$file_form_name]["error"] <= 0)
                    {
                        $ext = array_pop(explode(".", $_FILES[$file_form_name]['name'])) ; // 확장자
                        $config['file_name'] = "order_file_".time()."_".$i.".".$ext ;  

                        $this->upload->initialize($config) ;

                        if($this->upload->do_upload($file_form_name))
                        {
                            $rst_data['pt_thumbnail'] .= $config['open_file_path'].$config['file_name'].',' ;               
                        }
                        else
                        {
                            $rst_data['result'] = FALSE ;
                            $rst_data['error'] = $this->upload->display_errors() ;

                            return $rst_data ;
                        }
                    }
                }
                else
                {
                    $idx = $i-1 ;

                    if ( isset($thumbnails) && isset($thumbnails[$idx]) && $thumbnails[$idx] != '' && $thumbnails[$idx] != ',' )
                    {
                        $rst_data['pt_thumbnail'] .= $thumbnails[$idx].',' ;
                    }
                }
            }
        }

        return $rst_data ;
    }

    function _delete()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $pt_id  = $seg->get('pt_id', 0); 

        if ( $pt_id == 0 )
        {
            alert('잘못된 접근입니다.', '/product/lists');
        }
        else
        {
            $this->Product_model->delete($pt_id) ;
            alert_parent_reload('상품정보가 삭제되었습니다.') ;
        }
    }
}
?>