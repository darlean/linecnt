<?php
class Company extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		define('WIDGET_SKIN', 'main');
		define('CSS_SKIN', 'jquery,pages/page_about');
	}

	function About() 
	{
        $head = array('title' => '회사소개') ;

		widget::run('head', $head);
		$this->load->view('company/about');	
		widget::run('tail');
	}

	function Ceo() 
	{
        $head = array('title' => 'CEO 인사말') ;

		widget::run('head', $head);
		$this->load->view('company/ceo');
		widget::run('tail');
	}

	function Map() 
	{
        $head = array('title' => '오시는길') ;

		widget::run('head', $head);
		$this->load->view('company/map');
		widget::run('tail');
	}

	function Center()
	{
		$head = array('title' => '센터현황') ;

		widget::run('head', $head);
		$this->load->view('company/center');
		widget::run('tail');
	}
}
?>