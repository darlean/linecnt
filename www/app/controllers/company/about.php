<?php
class About extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();
		$this->load->model(array('member_tree_model', 'Member_infor_model'));
		$this->load->config('cf_position') ;

		define('WIDGET_SKIN', 'main');
		define('CSS_SKIN', 'jquery');

		//$this->output->enable_profiler(TRUE);
	}

	function index() 
	{
        $head = array('title' => '회사소개') ;

		widget::run('head', $head);
		$this->load->view('company/about');
		widget::run('tail');
	}
}
?>