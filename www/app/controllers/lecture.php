<?php
class Lecture extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();

		$this->load->model(ADM_F.'/lecture_model');
        $this->load->helper(array( 'url', 'textual' ));

		define('WIDGET_SKIN', 'main');
        define('CSS_SKIN', 'shop.style');

		//$this->output->enable_profiler(TRUE);
	}

    function _remap($index)
    {
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'page':
            case 'lists':
                $this->lists();
            break;
            case 'write':
                $this->write();
            break;
            case 'view':
                $this->view();
            break;
            case 'modify':
                $this->modify();
            break;
            case 'delete':
                $this->delete();
            break;
            case 'set':
                $this->set();
            break;                        
            default:
                show_404();
            break;
        }
    }

    function lists()
    {
        $data = $this->_data() ;

        $btn_write = FALSE;
        if( IS_MANAGER || IS_HBN_CAST )
            $btn_write = '<a href="'.RT_PATH.'/lecture/write" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> 영상 올리기</a>';

        $data = array_merge($data, array(
                                            'btn_write' => $btn_write,
                                            'btn_chkbox' => false,
                                            ));

        $head = array('title' => '강의영상');

        widget::run('head', $head ) ;
        $this->load->view('lecture/lecture_lists', $data) ;
        widget::run('tail') ;
    }

    function write()
    {
        $this->load->library('form_validation');
        $config = array(
            array('field'=>'SUBJECT', 'label'=>'제목', 'rules'=>'trim|required|xss_clean'),
            array('field'=>'CONTENTS', 'label'=>'내용', 'rules'=>'trim|required|xss_clean'),
            array('field'=>'URL', 'label'=>'동영상 주소', 'rules'=>'trim|required|xss_clean'),
            array('field'=>'THUMB_URL', 'label'=>'썸네일 주소', 'rules'=>'trim|xss_clean'),
            array('field'=>'IFRAME_URL', 'label'=>'IFrame 주소', 'rules'=>'trim|required|xss_clean'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) 
        {
            $data = array(
                            'post_data' => $this->input->post()
                            );

            $head = array('title' => '강의 영상 올리기') ;
            widget::run('head', $head ) ;
            $this->load->view('lecture/lecture_write', $data) ;
            widget::run('tail');
        }
        else
        {
            $member = unserialize(MEMBER);
            $lecture = $this->input->post();

            unset( $lecture['LECTURE_IDX'] );

            $lecture['IFRAME_URL'] = str_replace('http:', 'https:', $lecture['IFRAME_URL']);
            $lecture['IS_USE'] = 1;
            $lecture['REG_USER'] = $member['USERNAME'];
            $lecture['VIEW_COUNT'] = 1;
            $this->lecture_model->insert($lecture);

            goto_url('lecture/lists');
        }
    }

    function modify()
    {
        $this->load->library('form_validation');
        $config = array(
            array('field'=>'SUBJECT', 'label'=>'제목', 'rules'=>'trim|required|xss_clean'),
            array('field'=>'CONTENTS', 'label'=>'내용', 'rules'=>'trim|required|xss_clean'),
            array('field'=>'URL', 'label'=>'동영상 주소', 'rules'=>'trim|required|xss_clean'),
            array('field'=>'THUMB_URL', 'label'=>'썸네일 주소', 'rules'=>'trim|xss_clean'),
            array('field'=>'IFRAME_URL', 'label'=>'IFrame 주소', 'rules'=>'trim|required|xss_clean'),
            //array('field'=>'IS_USE', 'label'=>'제목', 'rules'=>'trim|required|xss_clean'),   
            //array('field'=>'me_content', 'label'=>'내용', 'rules'=>'trim|required|xss_clean')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) 
        {
            $post_data = $this->input->post();
            if(!$post_data)
            {
                $lecture_idx = $this->uri->segment(3);
                $post_data = $this->lecture_model->get_one($lecture_idx);
            }

            //var_dump($post_data['IFRAME_URL']);

            $data = array(
                            'post_data' => $post_data
                            );

            $head = array('title' => '강의 영상 수정') ;
            widget::run('head', $head ) ;
            $this->load->view('lecture/lecture_modify', $data) ;
            widget::run('tail');
        }
        else
        {
            $member = unserialize(MEMBER);
            $lecture = $this->input->post();

            $lecture_idx = $lecture['LECTURE_IDX'];
            unset( $lecture['LECTURE_IDX'] );

            $lecture['IFRAME_URL'] = str_replace('http:', 'https:', $lecture['IFRAME_URL']);
            $lecture['IS_USE'] = 1;
            //$lecture['REG_DATE'] = 'sysdate';
            $lecture['REG_USER'] = $member['USERNAME'];
            $lecture['VIEW_COUNT'] = 1;
            $this->lecture_model->update($lecture_idx, $lecture);
            //var_dump($lecture);

            goto_url('lecture/lists');
        }
    }

    function view()
    {
        $lecture_idx = $this->uri->segment(3);

        $lecture = $this->lecture_model->get_one($lecture_idx);
        $this->lecture_model->update_plus($lecture_idx, 'VIEW_COUNT', 1);

        $lecture["IFRAME_URL"] = htmlspecialchars_decode($lecture["IFRAME_URL"]) ;

        $btn_delete = $btn_modify = FALSE;
        if( IS_MANAGER || IS_HBN_CAST )
        {
            $btn_delete = "<button type='button' class='btn btn-danger' onclick=\"javascript:post_send('lecture/delete', {lecture_idx:'".$lecture_idx."'}, true);\">삭제</button>";

            $btn_modify = '<a href="'.RT_PATH.'/lecture/modify/'.$lecture['LECTURE_IDX'].'" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> 수정</a>';
        }

        $data = array(
            'lecture' => $lecture,
            'btn_delete' => $btn_delete,
            'btn_modify' => $btn_modify
        );

        $head = array('title' => '강의 영상 보기') ;
        widget::run('head', $head ) ;
        $this->load->view('lecture/lecture_view', $data) ;
        widget::run('tail');
    }

    function delete()
    {
        $lecture_idx = $this->input->post('lecture_idx');

        $this->lecture_model->delete($lecture_idx);

        goto_url('lecture/lists');
    }
 

	function _data($in_type='')
	{
		$this->load->library(array('pagination', 'querystring'));
        $this->load->helper(array('admin', 'sideview'));

        $param =& $this->querystring;
        $page = $this->uri->segment(4, 1);
        $sst = $param->get('sst', 'REG_DATE');
        $sod = $param->get('sod', 'desc');
        $sfl = $param->get('sfl');
        $stx = $param->get('stx');
        
        $config['suffix'] = $param->output();
        $config['base_url'] = '/lecture/lists/page/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;

        $offset = ($page - 1) * $config['per_page'];
        $result = $this->lecture_model->list_result($sst, $sod, $sfl, $stx, $config['per_page'], $offset);

        $config['total_rows'] = $result['total_cnt'];
        $this->pagination->initialize($config);

        $list = array();
        $token = get_token();
        foreach ($result['qry'] as $i => $row) 
        {
            $list[$i] = new stdClass();
            $list[$i]->idx = $row['LECTURE_IDX'];
            $list[$i]->num = $result['total_cnt'] - ($page - 1) * $config['per_page'] - $i;
            $list[$i]->href = '/lecture/view/'.$row['LECTURE_IDX'];
            
            $list[$i]->subject = $row['SUBJECT'];
            $list[$i]->url = $row['URL'];
            $list[$i]->thumb_url = $row['THUMB_URL'];
            $list[$i]->is_use = $row['IS_USE'];
            $list[$i]->reg_date = $row['REG_DATE'];
            $list[$i]->reg_user = $row['REG_USER'];
            $list[$i]->view_count = $row['VIEW_COUNT'];
 
        }

        $head = array('title' => '회원관리');
        $data = array(
            'token' => $token,
            'list' => $list,

            'sfl' => $sfl,
            'stx' => $stx,

            'total_cnt' => number_format($result['total_cnt']),
            'paging' => $this->pagination->create_links(),
        );

        return $data;
	}

}