<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model(array('O_Stock_order_model', 'S_Center_model'));

        define('WIDGET_SKIN', 'main');

        //$this->output->enable_profiler(TRUE);     
    }

    function _remap($index)
    {
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'page':
            case 'index':
                $this->_list();
                break;
            case 'view' :
                $this->_view() ;
                break;
            default:
                show_404();
                break;
        }
    }

    function _list()
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();

        $center   = $param->get('center');   // 검색필드

        $sdt   = $param->get('sdt');        // 날짜검색타입
        $sdt_s   = $param->get('sdt_s');    // 날짜검색타입명
        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜

        $total_count = $this->O_Stock_order_model->get_stock_count($center) ;

        $config['suffix']       = $qstr;
        $config['per_page']    = 20;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');
        $config['base_url']    = RT_PATH.'/center/stock/page/';

        //  페이지 선택 후,  검색시 오류 방지
        $page = ( $total_count < ($page - 1) * $config['per_page'] ) ? 1 : $page ;

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $limit = $config['per_page'] ;
        $offset = ($page - 1) * $config['per_page'];

        $result = $this->O_Stock_order_model->get_stock_list($limit, $offset, $center);

        $list = array();
        foreach ($result as $i => $row) {
            $list[$i] = $row;
            $list[$i]['num'] = $total_count - ($page - 1) * $config['per_page'] - $i;
        }

        $head = array('title' => '물류현황');
        $data = array
        (
            'center_list'   => $this->S_Center_model->get_center_list(),

            'total_count'   => $total_count,
            'qstr'          => $qstr,
            'list'          => $list,
            'paging'        => $CI->pagination->create_links(),

            'center'        => $center,
            'sdt'           => $sdt,        // 날짜검색타입
            'sdt_s'         => $sdt_s,      // 날짜검색타입명
            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜
        );

        widget::run('head', $head);
        $this->load->view('center/stock_list', $data);
        widget::run('tail');
    }

    function _view()
    {
        $div_stockord_info = '' ;
        $stockord_no = $this->input->post('stockord_no') ;

        $stockord_info_list = $this->O_Stock_order_model->get_stock($stockord_no);

        $i = 1 ;
        foreach ($stockord_info_list as $row)
        {
            $div_stockord_info .= '        
                        <tr>
                            <td class="text-center">'.$i.'</td>
                            <td class="text-center">'.$row['PDT_CD'].'</td>
                            <td class="text-center">'.$row['PDTDETAIL_NO'].'</td>
                            <td class="text-center">'.$row['PDT_NAME'].'</td>
                            <td class="text-center">'.$row['OPT_VALUE'].'</td>
                            <td class="text-center">'.$row['CNT'].'</td>
                        </tr>' ;

            $i++ ;
        }

        echo $div_stockord_info ;
    }
}
?>
