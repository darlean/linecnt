<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lists extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('S_Center_model', 'Member_info_model', ADM_F.'/Member_model', 'HB_s_center_map_model'));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'tab':
            case 'page':
            case 'index':
                $this->_list() ;
            break;       
            case 'move':
                $this->move() ;
            break ;
            default:
                show_404();
            break;
        }
    }

    function _list() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg    =& $this->seg;
        $param  =& $this->param;

        $page  = $seg->get('page', 1); // 페이지        
        $sido  = $param->get('sido');         
        $tab  = $seg->get('tab', 0);     
        $qstr  = $param->output();            

        $total_count_list = array() ;
        for ( $i = 0 ; $i < 2 ; $i++ )
        {
            $total_count_list[$i] = $this->S_Center_model->list_count($sido, $i) ;
        }   

        $total_count_list[2] = $this->Member_info_model->list_count('', '', 'reception', $sido) ;

        $total_count = $total_count_list[$tab] ;        

        $config['suffix']      = $qstr ;
        $config['base_url']    = RT_PATH.'/center/lists/tab/'.$tab.'/page/' ;
        $config['per_page']    = 10 ;
        $config['total_rows']  = $total_count ;
        $config['uri_segment'] = $seg->pos('page') ;

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];     

        if ( $tab < 2 )
        {
            $result = $this->S_Center_model->list_result($config['per_page'], $offset, $sido, $tab) ;

            $list = array();
            foreach ($result as $i => $row)
            {
                $list[$i] = $row;
                $list[$i]['map'] = $this->HB_s_center_map_model->get_map_info($row['CENTER_CD']) ;
            }
        }
        else
        {
            $list = $this->Member_info_model->list_result('USERID', 'asc', '', '', $config['per_page'], $offset, 'reception', $sido) ;
        }            

        $sido_list = array( "서울특별시", "강원도", "인천광역시", "경기도", "충청북도", "세종특별자치시", "충청남도", "경상북도", 
            "대전광역시", "대구광역시", "전라북도", "울산광역시", "경상남도" , "부산광역시", "광주광역시", "전라남도", "제주특별자치도" ) ;

        //$tab_list = array( "지사", "센터", "접수처" ) ;
        $tab_list = array( "지사", "센터" ) ;
            
        $data = array(     
            'total_count_list'  => $total_count_list,       
            'list'              => $list,
            'sido_list'         => $sido_list,
            'tab_list'          => $tab_list,
            'tab'               => $tab,
            'sido'              => $sido,
            'qstr'              => $qstr,
            'paging'            => $CI->pagination->create_links(),             
        );

        $head = array('title' => '지점 정보');
        widget::run('head', $head);
        $this->load->view('center/lists', $data);
        widget::run('tail');
    }

    function move() 
    {
        if (!IS_MANAGER) 
        {
            alert('잘못 된 접근입니다.', '/') ;
        }
    
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소

        $seg      =& $this->seg ;
        $center_cd  = $seg->get('move') ;              
            
        $mb = $this->Member_model->get_login( "HBN".$center_cd );        

        if ( !$mb )
        {
            alert('잘못 된 접근입니다.', '/') ;
        }

        if ( $center_cd > '0000' && $center_cd < '1000' )
        {
            $this->session->set_userdata('is_center', TRUE );            
        }
        else if ( $center_cd > '1000' && $center_cd < '2000' )
        {            
            $this->session->set_userdata('is_direct_branch', TRUE );            
        }   

        $this->session->set_userdata('CENTER_CD', $center_cd);

        $this->session->set_userdata('is_manager', FALSE );        
        $this->session->set_userdata('ss_mb_id', $mb['USERID']);
        $this->session->set_userdata('USERNAME', $mb['USERNAME']);        

        goto_url('/');
    }
}
?>