<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();        

        $this->load->model(array('S_Center_model'));

        define('CSS_SKIN', 'editor,shop.style,ace_file_input');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {   
        if ( $index != 'print' && $index != 'print2' )
        {
            define('WIDGET_SKIN', 'main');
        }     
        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {            
            case 'cd':
            case 'index':
                $this->calendar2() ;
            break;
            case 'main':
            	$this->main_events() ;
            break ;
            case 'save_events':
                $this->save_events() ;
            break;              
            case 'file_exists':
                $this->file_exists() ;
            break;             
            case 'add':
                $this->add() ;
            break; 
            case 'del':
                $this->del() ;
            break;   
            case 'print':
                $this->_print() ;
            break ;
            case 'update':
                $this->upload_image() ;
            break ;
            case 'del_image':
                $this->del_image() ;
                break ;
            case 'print2':
                $this->_print2() ;
                break;
            default:
                show_404();
            break;
        }
    }

    function index()
    {
    	$this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소        
        $seg      =& $this->seg;        

        $center_cd  = $seg->get('cd', '0000');
        $center_list = $this->S_Center_model->get_center_list() ;

        $head = array('title' => $center_list[$center_cd].' 교육일정');        
    	$data = array(    
    		'main_events'	=> $this->get_main_events($center_cd),		
    		'center_list'  	=> $center_list,
    		'center_cd'		=> $center_cd,
    		'default_date'	=> date("Y-m-d"),    		
    		'dir_path'		=> "/data/events/".$center_cd."/",
            'sources_list'  => $this->get_event_sources($center_cd),
    	) ;
    	
        widget::run('head', $head);

        if( IS_MANAGER || IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
            $this->load->view('center/manage_calendar', $data);
        else
            $this->load->view('center/calendar', $data);

        widget::run('tail');
    }

    function get_event_sources($in_center_cd)
    {
        $dir_path = "./data/events/".$in_center_cd ;

        $sources_list = array() ;

        if(is_dir($dir_path) && $dh = opendir($dir_path))
        {
            while(($file = readdir($dh)) != false)
            {
                if($file == "." || $file == "..") continue ;

                $file_path = $dir_path.DIRECTORY_SEPARATOR.$file ;
    
                if(is_file($file_path))
                {                    
                    if ( $file_path != $dir_path."/main.json")
                    {
                        $sources_list[count($sources_list)] = substr($file_path, 1) ;                            
                    }
                }
            }
            
            closedir($dh);
        }                    

        return $sources_list ;
    }

    function main_events()
    {    
    	$this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소        
        $seg      =& $this->seg;        

        $center_cd  = $seg->get('cd', '0000');        

        $colors = array ( 'bg-color-b', 'bg-color-sv', 'bg-color-gd', 'bg-color-rb', 'bg-color-sp', 
                        'bg-color-dd', 'bg-color-cw', 'bg-color-head', 'bg-color-pink', 'bg-color-light-blue', 'bg-color-green' ) ;

        $color_strs = array ( 'B', 'SV', 'GD', 'RB', 'SP', 'DD', 'CW', '본사', '센터장', '조직위원', '교육위원' ) ;
        
    	$head = array('title' => '이벤트 목록');
    	$data = array(    
    		'main_events'	=> $this->get_main_events($center_cd),		
    		'center_list'  	=> $this->S_Center_model->get_center_list(),
    		'center_cd'		=> $center_cd,    		
    		'dir_path'		=> "/data/events/".$center_cd."/",
            'colors'        => $colors,
            'color_strs'    => $color_strs,
    	) ;

    	widget::run('head', $head);
        $this->load->view('center/main_events', $data);
        widget::run('tail');
    }

    function save_events()
    {
    	$center_cd = $this->input->post('center_cd') ;
    	$date = $this->input->post('date') ;
    	$eventsJson = $this->input->post('eventsJson') ;
    	    	
    	$dir_path = "./data/events/".$center_cd ;

        if(!is_dir($dir_path))
        {
            umask( 0 ) ;
            mkdir($dir_path, 0777) ;
        }

        $file_name = $dir_path."/".$date.".json" ;

    	$file = fopen($file_name, "w") ;

    	$result = false ;

    	if ( $file )
    	{
    		$result = fwrite($file, $eventsJson) ;
    		fclose($file) ;
    	}

    	echo $result ;
    }

    function file_exists()
    {
    	$url = $this->input->post('url') ;

    	echo file_exists('.'.$url) ;    	
    }

    function get_main_events($in_center_cd)
    {
    	$dir_path = "./data/events/".$in_center_cd ;

    	$main_events = array() ;

    	// 읽기
    	if ( is_dir($dir_path) && opendir($dir_path))
    	{
    		$file_name = $dir_path."/main.json" ;

    		if ( file_exists($file_name) )
    		{
	    		$file = fopen($file_name, "r") ;

	    		if ( $file )
	    		{
	    			$buffer = fread($file, filesize($file_name));
	    			fclose($file) ;	

	    			$main_events = unserialize($buffer) ;	    			
	    		}
	    	}
    	}

    	return $main_events ;
    }

    function save_main_events($in_center_cd, $in_main_events)
    {
		$str_main_events = serialize($in_main_events) ;        
	    	  
    	$dir_path = "./data/events/".$in_center_cd ;

        if(!is_dir($dir_path))
        {
            umask( 0 ) ;
            mkdir($dir_path, 0777) ;
        }

        $file_name = $dir_path."/main.json" ;

    	$file = fopen($file_name, "w") ;

    	$result = false ;

    	if ( $file )
    	{
    		$result = fwrite($file, $str_main_events) ;
    		fclose($file) ;
    	}     		 
    }

    function add()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소        
        $seg      =& $this->seg;        

        $center_cd  = $seg->get('cd');        
        $event_title  = urldecode($seg->get('event_title'));
        $event_class  = $seg->get('event_class');

        if ( $center_cd != '' && $event_title != '' && $event_class != '' )
        {
            $main_events = $this->get_main_events($center_cd) ;
            
            $event['title'] = $event_title ;
            $event['class'] = $event_class ;

            array_push($main_events, $event) ;
            
            $this->save_main_events($center_cd, $main_events) ;

            alert('추가되었습니다.', URL) ;
        }

        alert('잘못된 정보입니다.', URL) ;
    }

    function del()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소        
        $seg      =& $this->seg;        

        $center_cd  = $seg->get('cd');
        $event_id  = $seg->get('event_id');

        if ( $center_cd != '' && $event_id != '' )
        {
            $main_events = $this->get_main_events($center_cd) ;

            unset($main_events[$event_id]);            
            $main_events = array_values($main_events);

            $this->save_main_events($center_cd, $main_events) ;

            alert('삭제되었습니다.', URL) ;
        }

        alert('잘못된 정보입니다.', URL) ;
    }

    function _print()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;        

        $center_cd  = $seg->get('cd', '0000');
        $center_list = $this->S_Center_model->get_center_list() ;

        $head = array('title' => $center_list[$center_cd].' 교육일정');
        $data = array(    
            'main_events'   => $this->get_main_events($center_cd),      
            'center_list'   => $center_list, 
            'center_cd'     => $center_cd,
            'default_date'  => date("Y-m-d"),           
            'dir_path'      => "/data/events/".$center_cd."/",
            'sources_list'  => $this->get_event_sources($center_cd),
        ) ;
        
        widget::run('head', $head);
        $this->load->view('center/print_calendar', $data);        
        widget::run('tail');
    }

    function calendar2()
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $center_cd  = $seg->get('cd', '0000');
        $year   = $param->get('year', date('Y'));
        $month  = $param->get('month', date('m'));

        $center_list = $this->S_Center_model->get_center_list() ;

        $head = array('title' => $center_list[$center_cd].' 교육일정');
        $data = array(
            'center_list'  	=> $center_list,
            'center_cd'		=> $center_cd,
            'img_path'		=> "/data/class/".$center_cd."/".$year."_".$month.".png",
            'img_file_name'	=> $year."_".$month.".png",
            'year'          => $year,
            'month'         => $month,
        ) ;

        widget::run('head', $head);
        $this->load->view('center/calendar2', $data);
        widget::run('tail');
    }

    public function upload_image()
    {
        if ( count($_FILES) > 0 )
        {
            $this->load->library('upload') ;

            $center_cd = $this->input->post('CENTER_CD') ;

            $config['upload_path'] = "./data/class/".$center_cd ;
            $config['open_file_path'] = "/data/class/".$center_cd ;

            $config['allowed_types'] = 'gif|jpg|png|gif|bmp|pdf';
            $config['max_size'] = '1536' ;
            $config['max_width'] = '3200';
            $config['max_height'] = '3200';

            if(!is_dir($config['upload_path']))
            {
                umask( 0 );
                mkdir($config['upload_path'], 0777) ;
            }

            $file_path = $config['upload_path']."/".$this->input->post('IMG_FILE_NAME') ;

            if ( file_exists($file_path) )
            {
                unlink($file_path) ;
            }

            $file_form_name = 'file';

            if ($_FILES[$file_form_name]["size"] > 0 && $_FILES[$file_form_name]["name"] != 'none')
            {
                if ($_FILES[$file_form_name]["error"] <= 0)
                {
                    $config['file_name'] = $this->input->post('IMG_FILE_NAME') ;

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload($file_form_name)) {
                    } else {
                        alert($this->upload->display_errors());
                    }
                }
            }

            alert('일정이 업로드되었습니다.', URL);
        }
    }

    public function del_image()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $center_cd = $this->seg->get('center_cd') ;
        $year = $this->seg->get('year') ;
        $month = $this->seg->get('month') ;

        if ( $center_cd != '' && $year != '' && $month != '' ) {
            $img_path = "./data/class/".$center_cd."/".$year."_".$month.".png";

            if (file_exists($img_path))
            {
                if (unlink($img_path))
                    alert('일정이 삭제되었습니다.', URL);
            }
        }

        alert('일정이 삭제가 실패하였습니다.', URL);
    }

    function _print2()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $center_cd = $this->seg->get('center_cd') ;
        $year = $this->seg->get('year') ;
        $month = $this->seg->get('month') ;

        $img_path = "/data/class/".$center_cd."/".$year."_".$month.".png";

        $center_list = $this->S_Center_model->get_center_list() ;

        $head = array('title' => $center_list[$center_cd].' 교육일정');
        $data = array(
            'img_path'   => $img_path,
        ) ;

        widget::run('head', $head);
        $this->load->view('center/print_calendar2', $data);
        widget::run('tail');
    }
}