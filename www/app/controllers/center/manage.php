<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('S_Center_model', 'Member_info_model', 'HB_Use_ucube_center_model'));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'tab':
            case 'page':
            case 'index':
                $this->_list() ;
            break;       
            case 'add_help_center':
                $this->add_help_center() ;
            break ;
            case 'del_help_center':
                $this->del_help_center() ;
                break ;
            case 'use_ucube':
                $this->use_ucube() ;
                break ;
            default:
                show_404();
            break;
        }
    }

    function _list() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg    =& $this->seg;
        $param  =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();            

        $total_count = $this->HB_Use_ucube_center_model->list_count() ;

        $config['suffix']      = $qstr ;
        $config['base_url']    = RT_PATH.'/center/manage/page/' ;
        $config['per_page']    = 10 ;
        $config['total_rows']  = $total_count ;
        $config['uri_segment'] = $seg->pos('page') ;

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];     

        $result = $this->HB_Use_ucube_center_model->list_result($config['per_page'], $offset) ;

        $list = array();
        foreach ($result as $i => $row)
        {
            $list[$i] = $row;
            $list[$i]['NO'] = $total_count - $i ;

            $arr = explode(',', $row['HELP_CENTER_CDS'] ) ;

            $list[$i]['HELP_CENTER_CDS'] = '' ;
            foreach ($arr as $value)
            {
                if ( $value != "" )
                {
                    $center_name = $this->S_Center_model->get_center_name($value) ;
                    $list[$i]['HELP_CENTER_CDS'] .= ' <span class="label label-red rounded-2x btn DEL_CENTER" id="DEL_CENTER" name="' . $value . '" tag="' . $row['CENTER_CD'] . '"><i class="fa fa-minus"></i></span> ' . $center_name;
                }
            }
        }
            
        $data = array(     
            'total_count'       => $total_count,
            'list'              => $list,
            'qstr'              => $qstr,
            'center_list'       => $this->S_Center_model->get_center_list(false),
            'paging'            => $CI->pagination->create_links(),             
        );

        $head = array('title' => '지점 관리');
        widget::run('head', $head);
        $this->load->view('center/manage', $data);
        widget::run('tail');
    }

    function add_help_center()
    {
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $select_center_cd = $this->param->get('select_center_cd') ;
        $center_cd = $this->param->get('center_cd') ;

        $result = $this->HB_Use_ucube_center_model->add_help_center($select_center_cd, $center_cd) ;

        if ( $result == true )
        {
            alert('정상적으로 정보가 수정되었습니다.', URL) ;
        }
        else
        {
            alert('추가 할 수 없는 센터입니다.', URL) ;
        }
    }

    function del_help_center()
    {
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $select_center_cd = $this->param->get('select_center_cd') ;
        $center_cd = $this->param->get('center_cd') ;

        $result = $this->HB_Use_ucube_center_model->del_help_center($select_center_cd, $center_cd) ;

        if ( $result == true )
        {
            alert('정상적으로 삭제되었습니다.', URL) ;
        }
        else
        {
            alert('DB에 연결할 수 없습니다.', URL) ;
        }
    }

    function use_ucube()
    {
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $use_ucube = $this->param->get('use_ucube') ;
        $center_cd = $this->param->get('center_cd') ;

        $use_ucube = ($use_ucube == 'true') ? 'O' : 'X' ;

        $result = $this->HB_Use_ucube_center_model->use_ucube($center_cd, $use_ucube) ;

        if ( $result == true )
        {
            alert('정상적으로 수정되었습니다.', URL) ;
        }
        else
        {
            alert('DB에 연결할 수 없습니다.', URL) ;
        }
    }
}
?>