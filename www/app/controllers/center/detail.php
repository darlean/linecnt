<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Detail extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('S_Center_model'));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {            
            case 'cd':
                $this->_detail() ;
            break;
            case 'excel':
                $this->excel() ;
            break;
            default:
                show_404();
            break;
        }
    }

    function _detail()
    { 
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $ord_type  = $seg->get('ord_type', 0); // tab
        $center_cd  = $seg->get('cd'); // tab
        $page  = $seg->get('page', 1); // 페이지

        $year   = $param->get('year', date('Y'));   
        $month  = $param->get('month', date('m'));  
        $qstr  = $param->output();                
        
        $total_count = $this->S_Center_model->get_order_open_count_by_center($center_cd, $year, $month, $ord_type) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/center/detail/cd/'.$center_cd.'/page/';
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);        

        $limit = $config['per_page'] ;
        $offset = ($page - 1) * $config['per_page'];

        $result = $this->S_Center_model->get_order_open_list_by_center($center_cd, $limit, $offset, $year, $month, $ord_type) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i ;
            $list[$i]['href']               = '/order/view/ord_no/'.$row['ORDNO'];

            if ( $row['ORD_DATE'] > '20160411' && $row['TYPE_CD'] == '2504' )       // wired
            {
                $list[$i]['center_fee']         = 0 ;
                $list[$i]['total_center_fee']   = number_format($row['PV4']) ;
            }
            else
            {
                $list[$i]['center_fee']         = number_format($row['PV2'] * 0.03) ;   
                $list[$i]['total_center_fee']   = number_format(($row['PV2'] * 0.03) + $row['PV4']) ;
            }            
        }

        if ( $row['ORD_DATE'] > '20160411' )
        {
            $sub_total = array() ;
            $sub_total['CENTER_FEE'] = 0 ;
            $sub_total['PV'] = 0 ;
            $sub_total_result = $this->S_Center_model->get_cv_pv_by_center2($center_cd, $year, $month) ;

            foreach ($sub_total_result as $key => $value) 
            {
                $sub_total['PV'] += $value['PV'] ;

                if ( $value['TYPE_CD'] == '2504' )      // wired
                {
                    $sub_total['CENTER_FEE'] += $value['PV4'] ;
                }                
                else
                {
                    $sub_total['CENTER_FEE'] += ( $value['CV'] * 0.03 ) + $value['PV4'] ;
                }
            }            
        }
        else
        {
            $sub_total = $this->S_Center_model->get_cv_pv_by_center($center_cd, $year, $month) ;
            $sub_total['CENTER_FEE'] = ( $sub_total['CV'] * 0.03 ) + $sub_total['PV4'] ;
        }

        $head = array('title' => $this->S_Center_model->get_center_name($center_cd));
        $data = array(
            'sub_total'     => $sub_total,
            'total_count'   => $total_count,  
            'list'          => $list,
            'paging'        => $CI->pagination->create_links(),  
            'center_cd'     => $center_cd,
            'year'          => $year, 
            'month'         => $month,

            'ord_type_list' => array( 0 => '개통완료', 1 => '주문', 2 => '반품', 3 => '교환', 4 => '취소'),
            'ord_type'      => $ord_type,
            'qstr'          => $qstr,
        );

        widget::run('head', $head);
        $this->load->view('center/detail', $data);
        widget::run('tail');        
    }  

    function excel()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소   
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;
        
        $center_cd  = $seg->get('cd'); // tab        

        $year   = $param->get('year', date('Y'));   
        $month  = $param->get('month', date('m'));  

        $this->load->library('excel') ;

        // Create new PHPExcel object
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");

        // Set properties
        // Excel 문서 속성을 지정해주는 부분이다. 적당히 수정하면 된다.
        /*$objPHPExcel->getProperties()
                    ->setCreator("작성자")  //작성자
                    ->setLastModifiedBy("최종 수정자")  //최종수정자
                    ->setTitle("타이틀")  //타이틀
                    ->setSubject("주제")  //주제
                    ->setDescription("설명")  //설명
                    ->setKeywords("키워드")  //키워드
                    ->setCategory("라이센스");  //License*/

        $ord_type_list = array( 0 => '개통완료', 1 => '주문', 2 => '반품', 3 => '교환', 4 => '취소') ;                   
        
        foreach ($ord_type_list as $idx => $value) 
        {
            // Add new sheet
            if ( $idx > 0 )
            {
                $objWorkSheet = $objPHPExcel->createSheet();
            }

            $i = 3 ;
            $objPHPExcel->setActiveSheetIndex($idx)
                        ->setCellValue("A$i", "NO")
                        ->setCellValue("B$i", "회원정보")
                        ->setCellValue("C$i", "주문일자")
                        ->setCellValue("D$i", "구분")
                        ->setCellValue("E$i", "단가")
                        ->setCellValue("F$i", "부가세")
                        ->setCellValue("G$i", "결제금액")
                        ->setCellValue("H$i", "매출HB")
                        ->setCellValue("I$i", "기본 수수료")
                        ->setCellValue("J$i", "HB 3%")
                        ->setCellValue("K$i", "총 센터수수료") ;                        
        
            $i++ ;

            $total_count = $this->S_Center_model->get_order_open_count_by_center($center_cd, $year, $month, $idx) ;
            $list = $this->S_Center_model->get_order_open_list_by_center($center_cd, $total_count, 0, $year, $month, $idx) ;

            foreach ($list as $key => $row) 
            {                
                $list[$key]['num'] = $total_count - $key ;                
            }

            foreach ($list as $key => $row) 
            {
                if ( $row['ORD_DATE'] > '20160411' && $row['TYPE_CD'] == '2504' )       // wired
                {
                    $center_fee_3 = 0 ;
                    $center_fee   = number_format($row['PV4']) ;
                }
                else
                {
                    $center_fee_3 = ($row['PV1'] * 0.03) ;
                    $center_fee = $center_fee_3 + $row['PV4'] ;
                }

                $objPHPExcel->setActiveSheetIndex($idx)
                            ->setCellValue("A$i", "$row[num]")
                            ->setCellValue("B$i", "$row[USERTEXT]")                        
                            ->setCellValue("C$i", "$row[ORD_DATES]")                        
                            ->setCellValue("D$i", "$row[ORD_TYPES]")   
                            ->setCellValue("E$i", "$row[PRICE]")
                            ->setCellValue("F$i", "$row[VAT]")
                            ->setCellValue("G$i", "$row[AMT]")
                            ->setCellValue("H$i", "$row[PV2]")
                            ->setCellValue("I$i", "$row[PV4]")
                            ->setCellValue("J$i", "$center_fee_3")
                            ->setCellValue("K$i", "$center_fee") ;

                $i++ ;
            }

            $i-- ;

            $objPHPExcel->getActiveSheet()->getStyle('E4:K'.$i)->getNumberFormat()->setFormatCode('#,##0') ;        
            $objPHPExcel->SetHBDefaultStyle($value, $i, $idx) ;
        }

        $title = $this->S_Center_model->get_center_name($center_cd) ;
        $objPHPExcel->CreateExcelFile($title) ;            
    }
}
?>