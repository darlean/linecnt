<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Count extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('S_Center_model', 'Member_info_model'));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {            
            case 'index':
                $this->_count() ;
                break ;
            break;
            case 'growup':
                $this->_growup() ;
                break;
            default:
                show_404();
            break;
        }
    }

    function _count()
    { 
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $param    =& $this->param;

        $year   = $param->get('year', date('Y'));   
        $month  = $param->get('month', date('m'));

        $center_list = $this->S_Center_model->get_center_list() ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $tmp = $center_list[$this->session->userdata('CENTER_CD')] ;
            $center_list = array() ;
            $center_list[$this->session->userdata('CENTER_CD')] = $tmp ;
        }

        $mobile_vender_list = array('3203', '3201') ;
        $mobile_vender_name_list['3203'] = 'LG U+' ;
        $mobile_vender_name_list['3201'] = 'KT' ;

        $wired_vender_list = array('3203', '3201', '3202') ;
        $wired_vender_name_list['3203'] = 'LG U+' ;
        $wired_vender_name_list['3201'] = 'KT' ;
        $wired_vender_name_list['3202'] = 'SK' ;

        $reg_kind_list['3203'] = array('3607', '3608', '3609', '3616') ;
        $reg_kind_list['3201'] = array('3601', '3602', '3603') ;

        $reg_kind_name_list['3203'] = array('신규가입', '번호이동', '재가입', '기기변경') ;
        $reg_kind_name_list['3201'] = array('신규가입', '번호이동', '기기변경') ;

        $total_reg_kind_count = 0 ;
        foreach ($reg_kind_list as $key => $reg_kind)
        {
            $total_reg_kind_count += count($reg_kind_list[$key]) + 1 ;
        }
        
        $data = array
        (         
            'center_list'                  => $center_list,
            'mobile_vender_list'           => $mobile_vender_list,
            'mobile_vender_name_list'      => $mobile_vender_name_list,
            'wired_vender_list'            => $wired_vender_list,
            'wired_vender_name_list'       => $wired_vender_name_list,
            'reg_kind_list'                => $reg_kind_list,
            'reg_kind_name_list'           => $reg_kind_name_list,
            'total_reg_kind_count'         => $total_reg_kind_count,
            'mobile_list'                  => $this->S_Center_model->get_mobile_order_open_count($year, $month),
            'wired_list'                   => $this->S_Center_model->get_wired_order_open_count($year, $month),
            'year'                         => $year,
            'month'                        => $month,
        ) ;

        $data['total_count'] = $data['mobile_list']['TOTAL'] + $data['wired_list']['TOTAL'] ;

        $prev_year = $year ;
        $prev_month = ( $month == 1 ) ? 12 + (--$prev_year - $prev_year) : $month - 1 ;

        if ( $prev_month < 10 )
            $prev_month = "0".$prev_month ;

        $prev_month_mobile_list = $this->S_Center_model->get_mobile_order_open_count($prev_year, $prev_month) ;
        $prev_month_wired_list = $this->S_Center_model->get_wired_order_open_count($prev_year, $prev_month) ;

        foreach ($center_list as $key => $value)
        {
            $data['center_count'][$key] = isset($data['mobile_list'][$key]['TOTAL']) ? $data['mobile_list'][$key]['TOTAL'] : 0 ;
            $data['center_count'][$key] += isset($data['wired_list'][$key]['TOTAL']) ? $data['wired_list'][$key]['TOTAL'] : 0 ;

            $prev_total_count = isset($prev_month_mobile_list[$key]['TOTAL']) ? $prev_month_mobile_list[$key]['TOTAL'] : 0 ;
            $prev_total_count += isset($prev_month_wired_list[$key]['TOTAL']) ? $prev_month_wired_list[$key]['TOTAL'] : 0 ;

            $data['center_percent'][$key] = ($prev_total_count > 0) ? round( ( ( $data['center_count'][$key] - $prev_total_count ) / $prev_total_count ) * 100, 1) : 0 ;
        }

        $head = array('title' => '실적현황');
        widget::run('head', $head);        
        $this->load->view('center/count', $data);
        widget::run('tail');
    }

    function _growup()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $center_cd  = $seg->get('cd', '');
        $year   = $param->get('year', date('Y'));
        $center_list[''] = '전체' ;
        $center_list = array_merge($center_list, $this->S_Center_model->get_center_list()) ;

        if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $center_cd = $this->session->userdata('CENTER_CD') ;
            $tmp = $center_list[$center_cd] ;
            $center_list = array() ;
            $center_list[$this->session->userdata('CENTER_CD')] = $tmp ;
        }

        $mobile_list = array() ;
        $wired_list = array() ;
        $member_list = array() ;
        $mobile_total = 0 ;
        $wired_total = 0 ;
        $member_total = 0 ;

        for ( $i = 1 ; $i < 13 ; $i++ )
        {
            $mobile_list[$i] = $this->S_Center_model->get_mobile_order_open_count2($year, $i, $center_cd) ;
            $wired_list[$i] = $this->S_Center_model->get_wired_order_open_count2($year, $i, $center_cd) ;
            $member_list[$i] = $this->Member_info_model->get_member_count($year, $i, $center_cd) ;

            $mobile_total += $mobile_list[$i] ;
            $wired_total += $wired_list[$i] ;
            $member_total += $member_list[$i] ;
        }

        $max_value = max(max($mobile_list), max($wired_list), max($member_list)) ;

        $head = array('title' => $center_list[$center_cd].' 성장현황');
        $data = array(
            'center_list'  	=> $center_list,
            'center_cd'		=> $center_cd,
            'year'          => $year,

            'mobile_list'   => $mobile_list,
            'wired_list'    => $wired_list,
            'member_list'   => $member_list,
            'max_value'     => $max_value,

            'mobile_total'   => $mobile_total,
            'wired_total'    => $wired_total,
            'member_total'   => $member_total,
        ) ;

        widget::run('head', $head);
        $this->load->view('center/growup', $data);
        widget::run('tail');
    }
}
?>