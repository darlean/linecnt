<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Insert extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('Policy_model', 'Basic_model', 'Order_model', 'Order_Image_model', 'Order_state_model', 'S_Reason_model', 'S_Center_model', 'P_ptplink_model', 'Member_info_model', 'OrderMaster_model', 'HB_credit_model'));
        $this->load->config('cf_order') ;
        
        define('WIDGET_SKIN', 'main');
        define('CSS_SKIN', 'editor,shop.style,ace_file_input');
        $this->load->helper(array('socket_helper', 'mail')) ;        

        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {         
    	if (!IS_MEMBER && $index != 'pdt_cd')
        {
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login') ;
        }

        if (IS_AGENCY)
        {
            alert('일반 회원만 상품 주문이 가능합니다.', '/') ;
        }

        switch($index)
        {
            case 'pdt_cd':
                $this->_form();
            break;
            case 'update':
                $this->_update();
            break;
            case 'get_detail_info':
                $this->get_detail_info();
            break;  
            case 'get_stock_count':
                $this->get_stock_count();
            break;                
            case 'find_reception':
                $this->find_reception();
            break;   
            case 'find_hb_recipient':
                $this->find_hb_recipient();
            break;         
            case 'find_lgu_reg_no':
                $this->find_lgu_reg_no();
            break;          
            case 'find_reg_no':
                $this->find_reg_no();
            break;                      
            case 'from_hbplanner':
                $this->_update_for_hbplanner() ;
                break ;

            /*case 'test':
            {
                $postValues = "ORDERNO=20150715S00648" ;
                $result = $this->post_request("http://openpageutf.cafe24.com/guild/hbtest/mlmorder.php", $postValues) ;                        
                var_dump($result) ;                           
            }
            break ;*/
            default:
                show_404();
            break;
        }
    }

    function _form() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $pdt_cd  = $seg->get('pdt_cd', 0); 

        if ( $pdt_cd == 0 )
        {
            alert('잘못된 정보입니다.', './order/order');
            return false ;
        }        

        $token = get_token();

        // product_info
        {
            $product_info_list = $this->Policy_model->get_policy_info_list($pdt_cd) ;

            $product_info = array() ;

            foreach ($product_info_list as $key => $product) 
            {
                /*if ( !IS_MANAGER && $product['TEL_COMPANY'] == '3203' && $this->session->userdata('LGU_REG_NO') == '' )                    
                {
                    alert('사전등록 미등록자 구매 불가합니다.', './board/NOTICE/view/wr_id/783') ;
                    return false ;
                }*/

                if ( !isset($product_info['PDT_CD']) )
                {
                    $product_info['PDT_CD'] = $pdt_cd ;
                    $product_info['PDT_NAME'] = $product['PDT_NAME'] ;
                    $product_info['PDT_MODEL'] = $product['PDT_MODEL'] ;
                    $product_info['IMG_PATH'] = $product['IMG_PATH'] ;
                    $product_info['DETAIL_IMG_PATH'] = $product['DETAIL_IMG_PATH'] ;
                    $product_info['SELL_FORCED'] = $product['SELL_FORCED'] ;
                    $product_info['TEL_COMPANY'] = $product['TEL_COMPANY'] ;
                }

                if ( $product['PRICE_KIND'] )
                {
                    $product_info['calling_plan'][$product['PRICE_KIND']] = $product['PRICE_NAME'] ;                                                   
                }

                if ( $product['REG_KIND'] )
                {
                    $product_info['join_type'][$product['REG_KIND']] = $product['REG_NAME'] ;                               
                }
            }

            $product_info['pt_colors'] = $this->Policy_model->get_color_info($pdt_cd) ;

            $stock_price_infos = $this->Policy_model->get_stock_price($pdt_cd) ;                      
            $product_info['pc_price'] = isset($stock_price_infos->AMT) ? number_format($stock_price_infos->AMT).' 원' : '가격 오류' ;            

            $product_info['pt_thumbnail'] = explode (",", $product_info['IMG_PATH']);
            $product_info['pt_contents'] = '' ;                       
        }    

        foreach ($product_info['pt_colors'] as $key => $pt_color) 
        {
            $product_info['pt_colors'][$key]['stock_counts'] = $this->OrderMaster_model->get_stock_count($pdt_cd, $pt_color['PDTDETAIL_NO']) ;
        }

        // 별점
        $product_info['nStar'] = $this->Policy_model->get_pdt_star_count($pdt_cd) ;          

        if ( ( $product_info['SELL_FORCED'] == 'X' ) )
        {
            $stock_total_count = $this->OrderMaster_model->get_stock_total_count($product_info['PDT_CD']) ;
            $product_info['out_of_stock'] = ($stock_total_count == 0) ;            
            $product_info['close_out_of_stock'] = ($stock_total_count < 10) ;            
        }
        else
        {
            $product_info['out_of_stock'] = false ;
            $product_info['close_out_of_stock'] = false ;                                
        }         

        // procedure insert value!!!
        $vender_list = array
        (
            '0'     =>      '없음', 
            '3'     =>      'LGU+',  
            '2'     =>      'SK',  
            '1'     =>      'KT',  
            '4'     =>      'MVNO',  
        ) ;

        $head = array('title' => '상품보기');
        $data = array(            
            'product_info'          => $product_info,
            'receipt_kind'          => $this->Policy_model->get_receipt_kind(),
            'token'                 => $token,
            'vender_list'           => $vender_list,
            'center_list'           => $this->S_Center_model->get_center_list(),
        );

        widget::run('head', $head);

        if (!IS_MEMBER || IS_MANAGER || IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH)
        {
            $this->load->view('order/insert_manager', $data);
        }
        else
        {
            $fields = 'USERNAME,TEL,MOBILE,ZIPCODE,ADDR1,ADDR2,EMAIL,REG_DATE' ;
            $orderer_info = $this->Basic_model->get_member($this->session->userdata('ss_mb_id'), $fields) ;            

            $data['orderer_info'] = $orderer_info ;

            $this->load->view('order/insert', $data);
        }

        widget::run('tail');
    }

    function _update()
    {
        check_token('order/order') ;

        $this->load->helper('chkstr');  
        $config = array(
            array('field'=>'odv_type', 'label'=>'수령 방법', 'rules'=>'trim|required'),
            array('field'=>'odv_recipient', 'label'=>'받는 사람', 'rules'=>'trim|required'),
            array('field'=>'odv_recipient_tel', 'label'=>'연락처', 'rules'=>'trim|required|max_length[15]'),
            array('field'=>'odv_recipient_hp', 'label'=>'비상 연락처', 'rules'=>'trim|required|max_length[15]'),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) 
        {
            $this->_form();
        }
        else
        {
            $pdt_cd = $this->input->post('pdt_cd') ;
            $ptp_id = $this->input->post('ptp_id') ;

            $pdt_telprice_info = $this->Policy_model->get_telprice_info($ptp_id) ;
            $stockprice_info = $this->Policy_model->get_stock_price($pdt_cd) ;
            $pdt_master_info = $this->Policy_model->get_master_info($pdt_cd) ;
            $sum_price_info = $this->Policy_model->get_sum_point_info($ptp_id, $pdt_cd) ;

            if ( strpos($pdt_master_info->PDT_NAME, "선불") !== false )
            {
                $usim_info = $this->Policy_model->get_usim_info($pdt_master_info->TEL_COMPANY, true) ;
            }
            else
            {
                $usim_info = $this->Policy_model->get_usim_info($pdt_master_info->TEL_COMPANY);
            }

            $pdt_stockprice_info = $this->Policy_model->get_stock_price($pdt_cd) ;

            if ( $usim_info )
            {
                $stockprice_info->PRICE += $usim_info['PRICE'] ;
                $stockprice_info->VAT += $usim_info['VAT'] ;
                $stockprice_info->AMT += $usim_info['AMT'] ;
            }

            $order_info = $this->Order_model->create_order($pdt_master_info, $stockprice_info, $sum_price_info) ;      

            if ( $order_info->STATUS == '1' )
            {                      
                // 파일 업로드
                {
                    $rst_data = $this->upload_files($order_info->NEW_ORDER) ;

                    if ( $rst_data['result'] == FALSE )
                    {
                       alert($rst_data['error']) ;
                    }
                    else if ( $rst_data['IMG_PATH'] != '' )
                    {                        
                        $this->Order_Image_model->insert_order_image($order_info->NEW_ORDER, $rst_data['IMG_PATH'] ) ;
                    }
                }
              
                $order_detail = $this->Order_model->create_detail($order_info->NEW_ORDER, $pdt_master_info, $pdt_stockprice_info, $sum_price_info) ;

                if ( $order_detail->STATUS == '1' )
                {
                    $usim_detail = $this->Order_model->create_usim_detail($order_info->NEW_ORDER, $usim_info, $pdt_master_info) ;                    

                    if ( $usim_detail->STATUS == '1' )
                    {
                        $order_delivery = $this->Order_model->create_delivery($order_info->NEW_ORDER) ;    

                        if ( $order_delivery->STATUS == '1' )
                        {
                            $order_tel = $this->Order_model->create_tel($order_info->NEW_ORDER, $pdt_master_info, $pdt_telprice_info, $stockprice_info) ;

                            if ( $order_tel->STATUS == '1' )
                            {   
                                $link_path = $this->P_ptplink_model->get_link($pdt_cd, $this->input->post('select_calling_plan'), $this->input->post('select_join_type')) ;

                                $reg_no = ( $this->session->userdata('KAIT_NO') != '' ) ? $this->session->userdata('KAIT_NO') : $this->input->post('reg_no') ;
                                $seller_id = $this->input->post('seller_id') ;

                                $this->Order_state_model->insert($order_info->NEW_ORDER, 1, $reg_no, $seller_id) ;                                                            
                                
                                $postValues = "ORDERNO=".$order_info->NEW_ORDER ;

                                if ( $pdt_cd == "000000000007117" )
                                {
                                    mail_sender("gyrhkd620@hbn.co.kr,jyj@hbn.co.kr,shwncks@hbn.co.kr", "New 단말기 주문 : ".$order_info->NEW_ORDER, $order_info->NEW_ORDER) ;

                                    alert('중고폰 USIM 주문이 완료되었습니다.', 'order/lists') ;       
                                }
                                else if ( $stockprice_info->AMT == 0 )
                                {
                                    mail_sender("gyrhkd620@hbn.co.kr,jyj@hbn.co.kr,shwncks@hbn.co.kr", "New 단말기 주문 : ".$order_info->NEW_ORDER, $order_info->NEW_ORDER) ;
                                    alert('주문이 완료되었습니다.', 'order/lists') ;
                                }
                                else
                                {
                                    /*if ( IS_TEST_SERVER )
                                    {
                                        $result = $this->post_request("http://openpageutf.cafe24.com/guild/hbtest/mlmorder.php", $postValues) ;
                                    }
                                    else
                                    {
                                        $result = $this->post_request("http://vansales.cafe24.com/guild/hbnetworks/mlmorder.php", $postValues) ;
                                    }*/

                                    if ( $link_path != '' )
                                    {
                                        echo ("<script>window.open('".$link_path."');</script>") ;
                                    }

                                    //mail_sender("gyrhkd620@hbn.co.kr,jyj@hbn.co.kr,shwncks@hbn.co.kr", "New 단말기 주문 : ".$order_info->NEW_ORDER, $order_info->NEW_ORDER) ;

                                    //if ( $result['CODE'] == '0000' )
                                    {                                    
                                        alert('주문이 완료되었습니다.', 'order/lists_mobile') ;
                                    }
                                    /*else
                                    {
                                        alert('공제 번호 발행이 실패했습니다.('.$result['CODE'].')', 'order/lists') ;                                
                                    }*/
                                }
                            }
                            else
                            {
                                alert( 'CREATE_TEL : '.$order_tel->MESSAGE ) ;
                            }                        
                        }
                        else
                        {
                            alert( 'CREATE_DELIVERY : '.$order_delivery->MESSAGE ) ;
                        }
                    }                
                    else
                    {
                        alert( 'CREATE_USIM_DETAIL : '.$usim_detail->MESSAGE ) ;
                    } 
                }                
                else
                {
                    alert( 'CREATE_DETAIL : '.$order_detail->MESSAGE ) ;
                }                
            }
            else
            {
                alert( 'CREATE_ORDER : '.$order_info->MESSAGE ) ;
            }         
        }
    }

    function get_detail_info()
    {
        $pdt_cd = $this->input->post('pdt_cd') ;
        $price_kind = $this->input->post('price_kind') ;
        $reg_kind = $this->input->post('reg_kind') ;        

        if ( $pdt_cd != '' && $price_kind != '' && $reg_kind != '' )
        {
            $detail_info = $this->Policy_model->get_detail_info($pdt_cd, $price_kind, $reg_kind) ;
            $stock_price_infos = $this->Policy_model->get_stock_price($pdt_cd) ;                      

            if ( $detail_info )
            {
                $detail_info['INSTALMENT_AMOUNT'] = ($stock_price_infos->AMT - $detail_info['PRICE_DISCOUNT']) * 0.0027 ; 

                $detail_info['POINT1'] = number_format($detail_info['POINT1']) ;
                $detail_info['POINT2'] = number_format($detail_info['POINT2']) ;
                $detail_info['PRICE_DISCOUNT'] = number_format($detail_info['PRICE_DISCOUNT'])." 원" ; 
                $detail_info['INSTALMENT_AMOUNT'] = number_format($detail_info['INSTALMENT_AMOUNT'])." 원" ;      
                //$detail_info['left_days'] = (isset($detail_info['E_DATE'])) ? floor((strtotime($detail_info['E_DATE']) - time())/60/60/24) : 0 ;                        

                echo json_encode($detail_info) ;
                return TRUE ;            
            }
        }

        echo 'NULL' ;
        return FALSE ;
    }

    function get_stock_count()
    {
        $pdt_cd = $this->input->post('pdt_cd') ;
        $pdtdetail_no = $this->input->post('pdtdetail_no') ;

        if ( $pdt_cd != '' && $pdtdetail_no != '' )
        {
            echo $this->OrderMaster_model->get_stock_count($pdt_cd, $pdtdetail_no) ;
            return TRUE ;    
        }        
     
        echo 'NULL' ;
        return FALSE ;                
    }

    function find_reception()
    {
        $mb_id = $this->input->post('mb_id') ;
        $reg_name = $this->input->post('reg_name') ;

        if ( $mb_id != '' )
        {
            if ( $reg_name == '' )
            {
                echo 'NAME' ;
                return FALSE ;
            }
            else if ( $reg_name == $this->session->userdata('USERNAME') )
            {
                if ( $mb_id == $this->session->userdata('ss_mb_id') )
                {
                    echo 'FAILED' ;
                    return FALSE ;
                }
            }

            $member = $this->Member_info_model->get_member_info_by_id($mb_id) ;

            if ( $member/* && $member['IS_RECEPTION'] == 'O'*/ )
            {
                echo $member['USERNAME'] ;
                return TRUE ;
            }
        }

        echo 'NULL' ;
        return FALSE ;
    }

    function find_hb_recipient()
    {
        $mb_id = $this->input->post('mb_id') ;
        $ord_name = $this->input->post('ord_name') ;
        $pv_value = $this->input->post('pv_value') ;

        if ( $mb_id != '' )
        {
            $member = $this->Member_info_model->get_member_info_by_id($mb_id) ;

            if ( $member )
            {
                // 8월 2일 부터 본인 주문 건 무조건 가능
                /*if ( $member['REG_DATE'] < '20160705' && $ord_name == $member['USERNAME'] && $pv_value != "0" )
                {
                    echo 'CANT' ;
                    return FALSE ;
                }
                else*/
                {
                    echo $member['USERNAME'];
                    return TRUE;
                }
            }
        }

        echo 'NULL' ;
        return FALSE ;
    }

    function find_lgu_reg_no()
    {
        $mb_id = $this->input->post('mb_id') ;        

        if ( $mb_id != '' )
        {            
            $member = $this->Member_info_model->get_member_info_by_id($mb_id) ;

            if ( $member && $member['KAIT_NO'] != NULL )
            {
                echo $member['KAIT_NO'] ;
                return TRUE ;
            }
        }

        echo 'NULL' ;
        return FALSE ;
    }

    function find_reg_no()
    {
        $mb_id = $this->input->post('mb_id') ;        
        $mb_pw = $this->input->post('mb_pw') ;        

        if ( $mb_id != '' && $mb_id != '' )
        {            
            $member = $this->Member_info_model->get_reg_no_by_id_pw($mb_id, $mb_pw) ;

            if ( $member && ($member['PASSWD'] == $member['PASSWD2']) && $member['KAIT_NO'] != NULL )
            {
                echo $member['KAIT_NO'] ;
                return TRUE ;
            }
        }

        echo 'NULL' ;
        return FALSE ;
    }

    function post_request($url, $data) 
    {
        // parse the given URL
        $url = parse_url($url);

        if ($url['scheme'] != 'http') {
            return "Error:Only HTTP request are supported!";
        }

        // extract host and path:
        $host = $url['host'];
        $path = $url['path'];
        $res = fsk_open($url['host'], $url['path'], $data) ;        
    
        $content = explode("\r\n", $res, 4);     

        //$result['CODE'] = isset($content[1]) ? $content[1] : '';
        //$result['GT_CD'] = isset($content[2]) ? $content[2] : '';

        $result['CODE'] = isset($content[1]) ? substr($content[1], 0, 4) : '' ;
        $result['GT_CD'] = isset($content[1]) ? substr($content[1], 4) : '' ;
        
        return $result ;
    }

    public function upload_files($in_ODRNO)
    {
        $rst_data['result'] = TRUE ;
        $rst_data['IMG_PATH'] = '' ;

        if ( count($_FILES) > 0 )
        {              
            $this->load->library('upload') ;

            $config['upload_path'] = "./data/order_image/" ;
            $config['open_file_path'] = "/data/order_image/" ;

            $config['allowed_types'] = 'gif|jpg|png|gif|bmp|pdf';
            $config['max_size'] = '1536' ;
            $config['max_width'] = '3200';
            $config['max_height'] = '3200';
            
            if(!is_dir($config['upload_path']))
            {
                umask( 0 );
                mkdir($config['upload_path'], 0777) ;
            }

            for ( $i = 1 ; $i <= count($_FILES) ; $i++ ) 
            {
                $file_form_name = 'file'.$i ;

                if ( $_FILES[$file_form_name]["size"] > 0 && $_FILES[$file_form_name]["name"] != 'none' )
                {
                    if ($_FILES[$file_form_name]["error"] <= 0)
                    {
                        $ext = array_pop(explode(".", $_FILES[$file_form_name]['name'])) ; // 확장자
                        $config['file_name'] = "order_img_".time()."_".$in_ODRNO.".".$ext ;  

                        $this->upload->initialize($config) ;

                        if($this->upload->do_upload($file_form_name))
                        {
                            $rst_data['IMG_PATH'] .= $config['open_file_path'].$config['file_name'] ;               
                        }
                        else
                        {
                            $rst_data['result'] = FALSE ;
                            $rst_data['error'] = $this->upload->display_errors() ;

                            return $rst_data ;
                        }
                    }
                }
                else
                {
                    $idx = $i-1 ;

                    if ( isset($IMG_PATH) && isset($IMG_PATH[$idx]) && $IMG_PATH[$idx] != '' && $IMG_PATH[$idx] != ',' )
                    {
                        $rst_data['IMG_PATH'] .= $thumbnails[$idx].',' ;
                    }
                }
            }
        }

        return $rst_data ;
    }

    function _update_for_hbplanner()
    {
        $result->STATUS = 0 ;
        $result->NEW_ORDERID = "" ;
        $result->MESSAGE = $_POST ;

        $pdt_cd = $this->input->post('pdt_cd') ;
        $ptp_id = $this->input->post('ptp_id') ;

        $pdt_telprice_info = $this->Policy_model->get_telprice_info($ptp_id) ;
        $stockprice_info = $this->Policy_model->get_stock_price($pdt_cd) ;
        $pdt_master_info = $this->Policy_model->get_master_info($pdt_cd) ;
        $sum_price_info = $this->Policy_model->get_sum_point_info($ptp_id, $pdt_cd) ;

        if ( strpos($pdt_master_info->PDT_NAME, "선불") !== false )
        {
            $usim_info = $this->Policy_model->get_usim_info($pdt_master_info->TEL_COMPANY, true) ;
        }
        else
        {
            $usim_info = $this->Policy_model->get_usim_info($pdt_master_info->TEL_COMPANY);
        }

        $pdt_stockprice_info = $this->Policy_model->get_stock_price($pdt_cd) ;

        if ( $usim_info )
        {
            $stockprice_info->PRICE += $usim_info['PRICE'] ;
            $stockprice_info->VAT += $usim_info['VAT'] ;
            $stockprice_info->AMT += $usim_info['AMT'] ;
        }

        $order_info = $this->Order_model->create_order($pdt_master_info, $stockprice_info, $sum_price_info) ;      

        if ( $order_info->STATUS == '1' )
        {                                
            $order_detail = $this->Order_model->create_detail($order_info->NEW_ORDER, $pdt_master_info, $pdt_stockprice_info, $sum_price_info) ;

            if ( $order_detail->STATUS == '1' )
            {
                $usim_detail = $this->Order_model->create_usim_detail($order_info->NEW_ORDER, $usim_info, $pdt_master_info) ;                    

                if ( $usim_detail->STATUS == '1' )
                {
                    $order_delivery = $this->Order_model->create_delivery($order_info->NEW_ORDER) ;    

                    if ( $order_delivery->STATUS == '1' )
                    {
                        $order_tel = $this->Order_model->create_tel($order_info->NEW_ORDER, $pdt_master_info, $pdt_telprice_info, $stockprice_info) ;

                        if ( $order_tel->STATUS == '1' )
                        {   
                            $link_path = $this->P_ptplink_model->get_link($pdt_cd, $this->input->post('select_calling_plan'), $this->input->post('select_join_type')) ;

                            $reg_no = ( $this->session->userdata('KAIT_NO') != '' ) ? $this->session->userdata('KAIT_NO') : $this->input->post('reg_no') ;
                            $seller_id = $this->input->post('seller_id') ;

                            //$benefit_service = 'X' ;
                            //$add_service = 'X' ;

                            $benefit_service = ( $this->input->post('benefit_service') == '미사용' || $this->input->post('benefit_service') == '' ) ? 'X' : 'O' ;
                            $add_service = ( $this->input->post('add_service') == '미사용' || $this->input->post('add_service') == '' ) ? 'X' : 'O' ;

                            $this->Order_state_model->insert($order_info->NEW_ORDER, 1, $reg_no, $seller_id, $benefit_service, $add_service) ;
                            
                            $postValues = "ORDERNO=".$order_info->NEW_ORDER ;

                            if ( $pdt_cd == "000000000007117" )
                            {
                                mail_sender("gyrhkd620@hbn.co.kr,jyj@hbn.co.kr,shwncks@hbn.co.kr", "HBPlanner New 단말기 주문 : ".$order_info->NEW_ORDER, $order_info->NEW_ORDER) ;

                                $result->MESSAGE = "중고폰 USIM 주문이 완료되었습니다." ;     
                            }
                            else if ( $stockprice_info->AMT == 0 )
                            {
                                mail_sender("gyrhkd620@hbn.co.kr,jyj@hbn.co.kr,shwncks@hbn.co.kr", "HBPlanner New 단말기 주문 : ".$order_info->NEW_ORDER, $order_info->NEW_ORDER) ;
                                $result->MESSAGE = "주문이 완료되었습니다." ;
                            }
                            else
                            {
                                // 신용조회 여부 확인 후 공제 번호 발행 (주문사업자, 가입자명, 주문날짜)
                                /*$completed_credit = $this->HB_credit_model->completed_credit_today($this->input->post('reception_id'), $this->input->post('reg_name'), date("Ymd") ) ;

                                if ( $completed_credit > 0 )
                                {
                                    if ( IS_TEST_SERVER )
                                    {                                    
                                        $post_result = $this->post_request("http://openpageutf.cafe24.com/guild/hbtest/mlmorder.php", $postValues) ;
                                    }
                                    else
                                    {
                                        $post_result = $this->post_request("http://vansales.cafe24.com/guild/hbnetworks/mlmorder.php", $postValues) ;  
                                    }
                                }
                                    
                                mail_sender("gyrhkd620@hbn.co.kr,jyj@hbn.co.kr,shwncks@hbn.co.kr", "HBPlanner New 단말기 주문 : ".$order_info->NEW_ORDER, $order_info->NEW_ORDER) ;*/

                                $result->MESSAGE = "주문서가 접수되었습니다. 다음 단계도 계속 진행 해 주시기 바랍니다." ;
                            }

                            $result->STATUS = 1 ;
                            $result->NEW_ORDERID = $order_info->NEW_ORDER ;
                        }
                        else
                        {
                            $result->MESSAGE = $order_tel->MESSAGE."(order_tel)" ;
                        }                        
                    }
                    else
                    {
                        $result->MESSAGE = $order_delivery->MESSAGE."(order_delivery)" ;
                    }
                }                
                else
                {
                    $result->MESSAGE = $usim_detail->MESSAGE."(usim_detail)" ;
                } 
            }                
            else
            {
                $result->MESSAGE = $order_detail->MESSAGE."(order_detail)" ;
            }                
        }
        else
        {
            $result->MESSAGE = $order_info->MESSAGE."(order_info)" ;
        }         

        echo json_encode($result) ;
    }
}
?>