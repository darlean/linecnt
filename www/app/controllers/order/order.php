<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

		$this->load->library('form_validation');
		$this->load->model(array('Policy_model', 'S_Reason_model', 'HB_card2_model', 'OrderMaster_model', 'Starion_model'));
        $this->load->helper(array( 'url' ));
        
        define('WIDGET_SKIN', 'main');
        define('CSS_SKIN', 'shop.style');

        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {         
    	//if (!IS_MEMBER)
		//	alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'mobile':
            case 'wired':        
            case 'card':
            case 'used_mobile':
                $this->_list($index);
            break;

            case 'starion':
                $this->_starion();
                break ;
            default:
                show_404();
            break;
        }
    }

    private function _list($in_type)
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $vender  = $seg->get('vender'); // 통신사
        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sfl   = $param->get('sfl');   // 검색필드
        $stx   = $param->get('stx');   // 검색어
        $sca   = $param->get('sca');   // 분류
        $per_page = $param->get('per_page', 'All');

        $vender_list = $this->S_Reason_model->get_vender_list() ;

        if ( $vender == '' )
        {
            $vender = key($vender_list) ;
        }
        
        $total_count = $this->Policy_model->list_count($sfl, $stx, $vender, $in_type) ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/order/order/'.$in_type.'/vender/'.$vender.'/page/';
        $config['per_page']    = ( $per_page == 'All' )? $total_count : $per_page ;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->Policy_model->list_result($sst, $sod, $sfl, $stx, $config['per_page'], $offset, $vender, $in_type) ;

        $pdt_star_count_list = $this->Policy_model->get_pdt_star_count_list() ;

        foreach ($result as $key => $product) 
        {
            $result[$key]['href_msg'] = '주문하기' ;

            if ( $in_type == 'mobile')
            {
                $result[$key]['href'] = '/order/insert/pdt_cd/'.$product['PDT_CD'] ;                
            }
            else if ( $in_type == 'card')
            {
                $card_info = $this->HB_card2_model->get_card_info($product['PDT_CD']) ;   
        
                if ( $card_info )
                {
                    $product['IMG_PATH'] = $card_info['IMG_PATH'] ;
                }                

                $result[$key]['href'] = '/order/insert_card/pdt_cd/'.$product['PDT_CD'] ;                            
            }
            else if ( $in_type == 'wired')
            {
                $result[$key]['href_msg'] = '상품보기' ;
                $result[$key]['href'] = '/order/insert_wired/pdt_cd/'.$product['PDT_CD'] ;                                    
            }
            else if ( $in_type == 'used_mobile')
            {
                $result[$key]['href_msg'] = '판매하기' ;
                $result[$key]['href'] = '/order/insert_used_mobile/pdt_cd/'.$product['PDT_CD'] ;
            }

            if ( ( $product['SELL_FORCED'] == 'X' ) && ( $in_type == 'mobile' ) )
            {
                $stock_total_count = $this->OrderMaster_model->get_stock_total_count($product['PDT_CD']) ;
                $result[$key]['out_of_stock'] = ($stock_total_count == 0) ;            
                $result[$key]['close_out_of_stock'] = ($stock_total_count < 10) ;            
            }
            else
            {
                $result[$key]['out_of_stock'] = false ;
                $result[$key]['close_out_of_stock'] = false ;                                
            }            

            if ( $result[$key]['out_of_stock'] )
            {
                if ( !IS_MANAGER )
                {
                    $result[$key]['href'] = '#' ;
                }
                $result[$key]['href_msg'] = '매진' ;
            }
            
            $result[$key]['pt_thumbnail'] = '' ;

            if ( $product['IMG_PATH'] != '' )
            {
                $IMG_PATH = explode (",", $product['IMG_PATH']);

                $result[$key]['pt_thumbnail'] = $IMG_PATH[0] ;
            }

            // 별점
            $result[$key]['nStar'] = isset($pdt_star_count_list[$product['PDT_CD']]) ? $pdt_star_count_list[$product['PDT_CD']] : 0 ;
            
            //$started_days = (isset($product['S_DATE'])) ? floor((time() - strtotime($product['S_DATE']))/60/60/24) : 0 ;
            $result[$key]['new_item'] = false ;//($started_days < 3) ? true : false ;

            /*if (!IS_MANAGER && $in_type == 'mobile')
            {
                if ( $vender != 3207 && $vender != 3208 && $this->session->userdata('KAIT_NO') == "" )
                {
                    $result[$key]['href_msg'] = 'KAIT 승낙번호를 <BR> 미발급시 구매 불가' ;
                    $result[$key]['href'] = '/../board/NOTICE/view/wr_id/1563' ;
                }
                else if ( $vender == 3203 && $this->session->userdata('LGU_REG_CHECK') == 'X' )
                {
                    $result[$key]['href_msg'] = 'LGU+ KAIT 승낙번호 <BR> 미승인시 구매 불가' ;
                    $result[$key]['href'] = '/../board/NOTICE/view/wr_id/1563' ;
                }
                else if ( $vender == 3202 && $this->session->userdata('SKT_REG_CHECK') == 'X' )
                {
                    $result[$key]['href_msg'] = 'SKT KAIT 승낙번호 <BR> 미승인시 구매 불가' ;
                    $result[$key]['href'] = '/../board/NOTICE/view/wr_id/1563' ;
                }
                else if ( $vender == 3201 && $this->session->userdata('KT_REG_CHECK') == 'X' )
                {
                    $result[$key]['href_msg'] = 'KT KAIT 승낙번호 <BR> 미승인시 구매 불가' ;
                    $result[$key]['href'] = '/../board/NOTICE/view/wr_id/1563' ;
                }                
            }*/
        }

        $data = array(
            'total_count' => $total_count,  
            'qstr' => $qstr,
            'sst' => $sst,
            'sod' => $sod,          
            'sca' => $sca,
            'sfl' => $sfl,
            'stx' => $stx,
            'per_page' => $per_page, 
            'list' => $result,
            'paging' => $CI->pagination->create_links(), 
            'type'  => $in_type, 
            'vender' => $vender,
            'vender_list' => $vender_list,            

            'sort_pc_id' => $param->sort('hb_product_for_policy.pc_id', 'desc'), 
            'sort_pc_start_date' => $param->sort('pc_start_date', 'desc'), 
            'sort_pc_end_date' => $param->sort('pc_end_date', 'desc'), 
        );

        if ( $in_type == 'mobile' )
        {
            $head = array('title' => '무선상품주문');
        }
        else if ( $in_type == 'wired' )
        {
            $head = array('title' => '유선상품주문');
        }
        else if ( $in_type == 'card' )
        {
            $head = array('title' => '제휴카드주문');
        }
        else if ( $in_type == 'used_mobile' )
        {
            $head = array('title' => '중고폰판매');
        }
        
        widget::run('head', $head);
        $this->load->view('order/order_grid', $data);
        widget::run('tail');        
    }

    private function _starion()
    {
       $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();
        $per_page = $param->get('per_page', 'All');

        $total_count = $this->Starion_model->list_count() ;

        $config['suffix']       = $qstr;
        $config['base_url']    = RT_PATH.'/order/order/starion'.'/page/';
        $config['per_page']    = ( $per_page == 'All' )? $total_count : $per_page ;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->Starion_model->list_result($config['per_page'], $offset) ;

        $pdt_star_count_list = $this->Policy_model->get_pdt_star_count_list() ;

        foreach ($result as $key => $product)
        {
            $result[$key]['href_msg'] = '상품보기' ;
            $result[$key]['href'] = '/order/insert_starion/pdt_cd/'.$product['PDT_CD'] ;

            if ( ( $product['SELL_FORCED'] == 'X' ) )
            {
                $stock_total_count = $this->OrderMaster_model->get_stock_total_count($product['PDT_CD']) ;
                $result[$key]['out_of_stock'] = ($stock_total_count == 0) ;
                $result[$key]['close_out_of_stock'] = ($stock_total_count < 10) ;
            }
            else
            {
                $result[$key]['out_of_stock'] = false ;
                $result[$key]['close_out_of_stock'] = false ;
            }

            if ( $result[$key]['out_of_stock'] )
            {
                if ( !IS_MANAGER )
                {
                    $result[$key]['href'] = '#' ;
                }
                $result[$key]['href_msg'] = '매진' ;
            }

            $result[$key]['pt_thumbnail'] = '' ;

            if ( $product['IMG_PATH'] != '' )
            {
                $IMG_PATH = explode (",", $product['IMG_PATH']);

                $result[$key]['pt_thumbnail'] = $IMG_PATH[0] ;
            }

            // 별점
            $result[$key]['nStar'] = isset($pdt_star_count_list[$product['PDT_CD']]) ? $pdt_star_count_list[$product['PDT_CD']] : 0 ;

            //$started_days = (isset($product['S_DATE'])) ? floor((time() - strtotime($product['S_DATE']))/60/60/24) : 0 ;
            $result[$key]['new_item'] = false ;//($started_days < 3) ? true : false ;
        }

        $data = array(
            'total_count' => $total_count,
            'qstr' => $qstr,
            'per_page' => $per_page,
            'list' => $result,
            'paging' => $CI->pagination->create_links(),

            'sort_pc_id' => $param->sort('hb_product_for_policy.pc_id', 'desc'),
            'sort_pc_start_date' => $param->sort('pc_start_date', 'desc'),
            'sort_pc_end_date' => $param->sort('pc_end_date', 'desc'),
        );

        $head = array('title' => '일반 상품 주문');

        widget::run('head', $head);
        $this->load->view('order/order_starion_grid', $data);
        widget::run('tail');
    }
}
?>