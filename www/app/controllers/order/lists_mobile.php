<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lists_mobile extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('Order_model', 'S_Order_state_model', 'S_Reason_model', 'Policy_model', 'P_ptplink_model', 'HB_credit_model', 'S_Rank_model', 'Order_state_model', 'Bk_o_ordermaster_model'));
        $this->load->helper(array( 'url', 'utill' ));
        $this->load->config('cf_order') ;
        $this->load->config('cf_position') ;        

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'tab':
            case 'page':
            case 'index':
                $this->_list($index);
            break;            
            case 'excel' :
                $this->excel() ;
            break;       
            case 'del_last_day':
                $this->del_last_day() ;
            break ;
            case 'request_open' :
                $this->request_open() ;
            break ;
            default:
                show_404();
            break;
        }
    }

    function excel()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소   
        $seg      =& $this->seg;    
        $tab  = $seg->get('tab', 0); 

        $this->load->library('excel') ;

        // Create new PHPExcel object
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");

        $cell_list = range('A', 'Z') ;

        $str_list = array( 'NO', '주문번호', '공제번호', '사업자명', '사전승낙번호', '상품명', '명의자', '연락처', '비상연락처', '색상', '주문상태', '메모', '주문일자',
                        '개통일자', '수령방법', '배송주소', '주문센터', '접수처(회원ID)', '수령 센터' ) ;
        $val_list = array( 'num', 'ORDNO', 'GT_CDS', 'ORD_NAME', 'UPPER_REG_NO', 'PDT_NAME', 'REG_NAME', 'MOBILE', 'RECV_MOBILE', 'OPT_VALUE', 'STATE_CD', 'ORD_MSG', 'ORD_DATE',
                        'REAL_OPEN_DT4', 'RECEIPT_KIND', 'RECV_ZIPCODE', 'ORDER_CENTER', 'RECEPTION_NAME', 'CENTER_NAME' ) ;      
 
        $i = 3 ;

        foreach ($str_list as $key => $value) 
        {            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$key]."$i", $str_list[$key]) ;
        }                   

        $data = $this->_list('excel') ;     
        $i++ ;
        foreach ($data['list'] as $key => $row) 
        {
            $order_state = isset($data['o_state_list'][$row['STATE_CD']])?$data['o_state_list'][$row['STATE_CD']] : $data['o_state_list'][1] ;

            foreach ($val_list as $idx => $value) 
            {
                if ( $value == 'GT_CDS' /*|| $value == 'OLD_TEL' || $value == 'RECV_MOBILE'*/ )
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($cell_list[$idx]."$i", $row[$value], PHPExcel_Cell_DataType::TYPE_STRING) ;
                }
                else if ( $value == 'STATE_CD' )
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$idx]."$i", "$order_state") ;
                }
                else if ( $value == 'RECV_ZIPCODE' )
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$idx]."$i", "[$row[RECV_ZIPCODE]] $row[RECV_ADDR1] $row[RECV_ADDR2]") ;
                }
                else if ( $value == 'RECEPTION_NAME' )
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$idx]."$i", "$row[RECEPTION_NAME] ($row[RECEPTION_ID])") ;
                }
                else
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$idx]."$i", $row[$value]) ;
                }
            }

            $i++ ;
        }

        $title = '무선주문내역조회';

        if ( IS_MANAGER )
        {
            $tab_list = array('전체', '개통요청', '센터개통', '공제번호 미발급', '반려', '삭제');
        }
        else if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $tab_list = array('전체', '개통요청', '반려', '삭제');
        }
        else
        {
            $tab_list = array('본인주문', '접수주문');
        }

        $title .= '_'.$tab_list[$tab] ;

        $i-- ;
        $objPHPExcel->CreateExcelFileWithHBDefaultStyle($title, $i) ;
    }    

    function _list($in_excel = '') 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소   
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $tab  = $seg->get('tab', 0); 
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sfl   = $param->get('sfl');   // 검색필드
        $stx   = $param->get('stx');   // 검색어
        $sca   = $param->get('sca');   // 분류

        $sdt   = $param->get('sdt');        // 날짜검색타입
        $sdt_s   = $param->get('sdt_s');    // 날짜검색타입명
        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜

        $vender = $param->get('vender') ;
        $state = $param->get('state') ;
        $rkind = $param->get('rkind') ;

        // 정렬
        if (!$sst) 
        {
            $sst = 'ORDNO';
            $sod = 'desc';            
        }
        else
        {
            $sst = preg_match("/^(ORDNO|RECEIPT_KIND|ORD_DATE|REAL_OPEN_DT4)$/i", $sst) ? $sst : FALSE;
        }   

        $type = "" ;
        if ($tab == 1)
        {
            $type = "reception" ;
        }

        if ( IS_MANAGER )
        {
            $type_cds = array('2502', 'opened', 'center', 'none', 'reject', 'deleted') ;
        }
        else if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $type_cds = array('2502', 'center', 'reject', 'deleted');
        }
        else
        {
            $type_cds = array('2502', '2502') ;
        }

        $total_count = 0 ;

        if ( $type_cds[$tab] == "deleted" )
        {
            $total_count = $this->Bk_o_ordermaster_model->list_count($sfl, $stx, $vender) ;
        }
        else
        {
            $total_count = $this->Order_model->list_count($sfl, $stx, $vender, $state, $rkind, $type, $type_cds[$tab]) ;
        }

        $config['suffix']       = $qstr;        
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');
        $config['base_url']    = RT_PATH.'/order/lists_mobile/tab/'.$tab.'/page/';

        //  페이지 선택 후,  검색시 오류 방지
        $page = ( $total_count < ($page - 1) * $config['per_page'] ) ? 1 : $page ;

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        if ( $in_excel == 'excel' )
        {
            $limit = $total_count ;
            $offset = 0 ;
        }
        else
        {   
            $limit = $config['per_page'] ;
            $offset = ($page - 1) * $config['per_page'];
        }

        if ( $type_cds[$tab] == "deleted" )
        {
            $result = $this->Bk_o_ordermaster_model->list_result($sst, $sod, $sfl, $stx, $limit, $offset, $vender);
        }
        else
        {
            $result = $this->Order_model->list_result($sst, $sod, $sfl, $stx, $limit, $offset, $vender, $state, $rkind, $type, $type_cds[$tab]);
        }

        $opened_total_count = 0 ;
        $center_total_count = 0 ;
        $none_total_count = 0 ;
        $reject_total_count = 0 ;
        $deleted_total_count = 0 ;

        if ( IS_MANAGER )
        {
            $tab_list = array('전체', '개통요청', '센터개통', '공제번호 미발급', '반려', '삭제');

            $opened_total_count = $this->Order_model->list_count($sfl, $stx, $vender, $state, $rkind, $type, $type_cds[1]) ;
            $center_total_count = $this->Order_model->list_count($sfl, $stx, $vender, $state, $rkind, $type, $type_cds[2]) ;
            $none_total_count = $this->Order_model->list_count($sfl, $stx, $vender, $state, $rkind, $type, $type_cds[3]) ;
            $reject_total_count = $this->Order_model->list_count($sfl, $stx, $vender, $state, $rkind, $type, $type_cds[4]) ;
            $deleted_total_count = $this->Bk_o_ordermaster_model->list_count($sfl, $stx, $vender) ;
        }
        else if ( IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
        {
            $tab_list = array('전체', '개통요청', '반려', '삭제');

            $center_total_count = $this->Order_model->list_count($sfl, $stx, $vender, $state, $rkind, $type, $type_cds[1]) ;
            $reject_total_count = $this->Order_model->list_count($sfl, $stx, $vender, $state, $rkind, $type, $type_cds[2]) ;
            $deleted_total_count = $this->Bk_o_ordermaster_model->list_count($sfl, $stx, $vender) ;
        }
        else
        {
            $tab_list = array('본인주문', '접수주문');
        }
    
        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
            $list[$i]['href']               = '/order/view/mobile/ord_no/'.$row['ORDNO'] ;

            if ( $type_cds[$tab] != "deleted" )
            {
                $list[$i]['OLD_TEL'] = typedTel($row['OLD_TEL']);
                $link_path = $this->P_ptplink_model->get_link($row['PDT_CD'], $row['PRICE_KIND'], $row['REGKIND']) ;
                $list[$i]['LINK_PATH'] = $link_path ;
            }

            if ( IS_MANAGER || IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
            {
                $list[$i]['completed_credit'] = $this->HB_credit_model->completed_credit_today($row['RECEPTION_ID'], $row['REG_NAME'], $row['ORD_DATE'] ) ;
            }

            if ( isset($row['RECV_MOBILE']) )                     
            {
                $list[$i]['RECV_MOBILE'] = typedTel($row['RECV_MOBILE']) ;                        
            }

            if ( isset($row['TEL_COMPANY']) )
            {
                $type = "new" ;

                if ( $row['REG_KIND'] == "재가입" || $row['REG_KIND'] == "기기변경" )
                {
                    $type = "re" ;
                }

                $filepath = "/data/hbplanner/order/".$row['ORDNO'].".png" ;
                $list[$i]['order_file_path'] = file_exists(".".$filepath) ? "/hbplanner/order/view/".$type."/vender/".$row['TEL_COMPANY']."/ord_no/".$row['ORDNO'] : "" ;

                $list[$i]['view_all_file_path'] = file_exists(".".$filepath) ? "/hbplanner/order/view_all/".$type."/vender/".$row['TEL_COMPANY']."/ord_no/".$row['ORDNO'] : "" ;

                $filepath = "/data/hbplanner/agree/".$row['ORDNO'].".png" ;
                $list[$i]['agree_file_path'] = file_exists(".".$filepath) ? $filepath : "" ;

                $filepath = "/data/hbplanner/private/".$row['ORDNO'].".png" ;
                $list[$i]['private_file_path'] = file_exists(".".$filepath) ? $filepath : "" ;

                $filepath = "/data/hbplanner/select_agree/".$row['ORDNO'].".png" ;
                $list[$i]['select_agree_file_path'] = file_exists(".".$filepath) ? $filepath : "" ;

                $filepath = "/data/hbplanner/guide/".$row['ORDNO'].".png" ;
                $list[$i]['guide_file_path'] = file_exists(".".$filepath) ? $filepath : "" ;

                $list[$i]['kt_sponsor1_file_path'] = "" ;
                $list[$i]['kt_sponsor2_file_path'] = "" ;

                if ( $row['TEL_COMPANY'] == '3201' )
                {
                    $filepath = "/data/hbplanner/kt_sponsor1/".$row['ORDNO'].".png" ;
                    $list[$i]['kt_sponsor1_file_path'] = file_exists(".".$filepath) ? $filepath : "" ;                    

                    $filepath = "/data/hbplanner/kt_sponsor2/".$row['ORDNO'].".png" ;
                    $list[$i]['kt_sponsor2_file_path'] = file_exists(".".$filepath) ? $filepath : "" ;                    
                }
            }
        }

        $open_agency_list = $this->S_Reason_model->get_reason_list(42, true) ;
                    
        $data = array(
            'total_count' => $total_count,
            'opened_total_count' => $opened_total_count,
            'center_total_count' => $center_total_count,
            'none_total_count' => $none_total_count,
            'reject_total_count' => $reject_total_count,
            'deleted_total_count' => $deleted_total_count,
            'sst' => $sst,
            'sod' => $sod,          
            'sca' => $sca,
            'sfl' => $sfl,
            'stx' => $stx,
            'list' => $list,
            'paging' => $CI->pagination->create_links(), 
            'o_state_list' => $this->S_Order_state_model->get_order_state_list(), 
            'vender_list' => $this->S_Reason_model->get_vender_list(true),
            'receipt_kind' => $this->Policy_model->get_receipt_kind(),
            'mb_position'   => $this->S_Rank_model->get_rank_list(),
            'mb_position_color' => $this->config->item('mb_position_color'),
            'open_agency_list'   => $open_agency_list,

            'vender' => $vender,
            'state' => $state,
            'rkind' => $rkind,
            'type' => $type,
            'qstr' => $qstr,

            'sdt'           => $sdt,        // 날짜검색타입
            'sdt_s'         => $sdt_s,      // 날짜검색타입명
            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜

            'sort_no' => $param->sort('ORDNO', 'desc'), 
            'sort_or_date' => $param->sort('ORD_DATE', 'desc'), 
            'sort_open_date' => $param->sort('REAL_OPEN_DT4', 'desc'), 
            'sort_odv_type' => $param->sort('RECEIPT_KIND', 'desc'), 

            'tab_list'  => $tab_list,
            'type_cds'  => $type_cds,
            'tab'       => $tab,
        );

        if ( $in_excel == 'excel' )
        {
            return $data ;
        }
        else
        {
            $head = array('title' => '무선주문내역조회');

            widget::run('head', $head);

            if ( IS_MANAGER || IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
            {
                if ( $type_cds[$tab] == "deleted" )
                {
                    $this->load->view('order/lists_mobile_deleted', $data);
                }
                else
                {
                    $this->load->view('order/lists_mobile_manager', $data);
                }
            }
            else
            {
                $this->load->view('order/lists_mobile', $data);
            }

            widget::run('tail');
        }
    }

    function del_last_day() 
    {        
        $dirs = array('kt_sponsor1', 'kt_sponsor2', 'agree', 'order', 'private', 'select_agree', 'id_card', 'with_id_card') ;
        $msg = "" ;

        foreach ($dirs as $key => $value) 
        {
            $dir = $_SERVER['DOCUMENT_ROOT'].RT_PATH."/data/hbplanner/".$value ;
            $cnt = 0 ;

            $dir_handle = opendir($dir) ;
            
            while( ( $fname = readdir($dir_handle) ) !== false ) 
            {
                if($fname == "." || $fname == "..") 
                {
                    continue;
                }

                $last_day = floor((time() - strtotime(substr($fname, 0, 8)))/60/60/24) ;  

                if (  $last_day > 14 )
                {
                    $cnt++ ;

                    unlink($dir.'/'.$fname) ;   
                }
            }

            $msg .= $value.'에서 '.$cnt.'건이 삭제 되었습니다.\n' ;

            closedir($dir_handle) ;
        }

        alert($msg, 'order/lists') ;
    }

    function request_open()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $or_id = $this->seg->get('or_id') ;
        $order_info = $this->Order_model->get_order_info($or_id) ;

        if( $order_info && $this->Order_state_model->request_open($or_id) )
        {
            mail_sender("hbncs@naver.com", "명의자 ".$order_info['REG_NAME']."(".$order_info['ORDNO'].") 개통 요청", "") ;

            alert('개통요청이 완료되었습니다.', 'order/lists_mobile') ;
        }
        else
        {
            alert('개통요청이 실패하였습니다.', 'order/lists_mobile') ;
        }
    }
}
?>