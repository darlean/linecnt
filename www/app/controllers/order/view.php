<?php
class View extends CI_Controller {
	function __construct() {
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model(array('Order_model', 'S_Center_model', 'O_Order_Acard_model', 'O_Order_Ctel_model', 'member_info_model', 'HB_sell_used_mobile_model', 'HB_temp_order_starion', 'HB_product_gift_model'));
        $this->load->config('cf_order') ;
        $this->load->helper(array('utill')) ;

        define('WIDGET_SKIN', 'main');
		define('CSS_SKIN', 'editor,shop.style');
	}

    function _remap($index)
    {         
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'mobile' :
                $this->view_mobile() ;
                break;
            case 'wired' :
                $this->view_wired() ;
                break;
            case 'card' :
                $this->view_card() ;
                break;
            case 'used_mobile' :
                $this->view_used_mobile() ;
                break ;
            case 'starion' :
                $this->view_starion() ;
                break ;
            default:
                show_404();
            break;
        }
    }

    function view_mobile() 
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $ord_no  = $seg->get('ord_no', 0); 

        if ( $ord_no == 0 )
        {
            alert('잘못된 정보입니다.', '/order/lists');
            return false ;
        }

        $token = get_token();

        $order_info = $this->Order_model->get_order_info($ord_no) ;

        if ( isset($order_info['IMG_PATH']) )
        {
            $order_info['pt_thumbnail'] = explode (",", $order_info['IMG_PATH']);
        }

        $vender_list = array('없음', 'KT', 'SKT', 'LGU+', 'MVNO') ;
        $order_info['OLD_TEL_COMP'] = $vender_list[$order_info['OLD_TEL_COMP']] ;

        $vender_list = array('', '모바일M청구서', '스마트청구서', '이메일') ;
        $order_info['BILL_KIND'] = $vender_list[$order_info['BILL_KIND']] ;

        $center_list = $this->S_Center_model->get_center_list(true) ;

        $order_info['CENTER_NAME'] = $center_list[$order_info['CENTER_CD']] ;
        $order_info['START_CENTER'] = $center_list[$order_info['START_CENTER']] ;

        if ( $order_info['RECEPTION_ID'] == 'D0000000' )
        {
            $order_info['RECEPTION'] = '' ;    
        }
        else
        {
            $order_info['RECEPTION'] = $order_info['RECEPTION_NAME']." (".$order_info['RECEPTION_ID'].")" ;        
        }
        
        if ( IS_MANAGER )
        {
            $seller_info = $this->member_info_model->get_member_info_by_id($order_info['SELLER_ID']) ;

            if ( isset($seller_info['MOBILE']) && $seller_info['MOBILE'] != '' )
                    $order_info['SELLER_ID'] .= ' ['.$seller_info['MOBILE'].']' ;
        }  

        $order_info['OLD_TEL'] = typedTel($order_info['OLD_TEL']) ;                
        $order_info['RECV_TEL'] = typedTel($order_info['RECV_TEL']) ;                
        $order_info['RECV_MOBILE'] = typedTel($order_info['RECV_MOBILE']) ;                

        $head = array('title' => '주문상세정보');
        $data = array(
            'order_info'        => $order_info,
            'token'             => $token,
        );

        widget::run('head', $head);
        $this->load->view('order/view', $data);
        widget::run('tail');
    }

    function view_card() 
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $ord_no  = $seg->get('ord_no', 0); 

        if ( $ord_no == 0 )
        {
            alert('잘못된 정보입니다.', '/order/lists/tab/2');
            return false ;
        }

        $token = get_token();

        $order_info = $this->O_Order_Acard_model->get_order_info($ord_no) ;

        if ( $order_info['RECEPTION_ID'] == 'D0000000' )
        {
            $order_info['RECEPTION'] = '' ;    
        }
        else
        {
            $order_info['RECEPTION'] = $order_info['RECEPTION_NAME']." (".$order_info['RECEPTION_ID'].")" ;      
        }

        $head = array('title' => '주문상세정보');
        $data = array(
            'order_info'        => $order_info,
            'token'             => $token,
        );

        widget::run('head', $head);
        $this->load->view('order/view_card', $data);
        widget::run('tail');
    }

    function view_wired() 
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $ord_no  = $seg->get('ord_no', 0); 

        if ( $ord_no == 0 )
        {
            alert('잘못된 정보입니다.', '/order/lists/tab/1');
            return false ;
        }

        $token = get_token();

        $order_info = $this->O_Order_Ctel_model->get_order_info($ord_no) ;
        
        if ( $order_info['RECEPTION_ID'] == 'D0000000' )
        {
            $order_info['RECEPTION'] = '' ;    
        }
        else
        {
            $order_info['RECEPTION'] = $order_info['RECEPTION_NAME']." (".$order_info['RECEPTION_ID'].")" ;        
        }       

        if ( isset($order_info['IMG_PATH']) )
        {
            $order_info['IMG_PATH'] = str_replace(',', '', $order_info['IMG_PATH']) ; 
        } 

        if ( IS_MANAGER )
        {
            $seller_info = $this->member_info_model->get_member_info_by_id($order_info['SELLER_ID']) ;
            //$order_info['SELLER_ID'] .= ' ['.$seller_info['MOBILE'].']' ;
        }        

        $head = array('title' => '주문상세정보');
        $data = array(
            'order_info'        => $order_info,            
            'token'             => $token,
        );

        widget::run('head', $head);
        $this->load->view('order/view_wired', $data);
        widget::run('tail');
    }

    function view_used_mobile()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $ord_no  = $seg->get('ord_no', 0);

        if ( $ord_no == 0 )
        {
            alert('잘못된 정보입니다.', '/order/lists_used_mobile');
            return false ;
        }

        $token = get_token();

        $order_info = $this->HB_sell_used_mobile_model->get_order_info($ord_no) ;

        if ( isset($order_info['IMG_PATH']) )
        {
            $order_info['IMG_PATH'] = str_replace(',', '', $order_info['IMG_PATH']) ;
        }

        if ( isset($order_info['ORD_IMG']) )
        {
            $order_info['ORD_IMG'] = explode(',', $order_info['ORD_IMG']) ;
        }

        if ( $order_info['SEND_TYPE'] == 0 )
            $order_info['SEND_TYPE'] = '센터방문' ;
        else
            $order_info['SEND_TYPE'] = '택배전달' ;

        if ( !IS_MANAGER )
        {
            if ($order_info['CONFIRM_AMT'] == '')
                $order_info['CONFIRM_AMT'] = '미정';
            else
                $order_info['CONFIRM_AMT'] = number_format($order_info['CONFIRM_AMT']);
        }

        $head = array('title' => '판매상세정보');
        $data = array(
            'order_info'        => $order_info,
            'token'             => $token,
        );

        widget::run('head', $head);
        $this->load->view('order/view_used_mobile', $data);
        widget::run('tail');
    }

    function view_starion()
    {
        //$this->output->enable_profiler(TRUE);
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $ord_no  = $seg->get('ord_no', 0);

        if ( $ord_no == 0 )
        {
            alert('잘못된 정보입니다.', '/order/lists_starion/tab/1');
            return false ;
        }

        $token = get_token();

        $order_info = $this->HB_temp_order_starion->get_real_order_info($ord_no) ;

        $tmp_ord_info = array() ;
        if ( $order_info['ORD_MSG'] != '' ) // TMP_ORDNO 를 담음
        {
            $tmp_ord_info = $this->HB_temp_order_starion->get_order_info($order_info['ORD_MSG']) ;
        }

        $order_info['INS_DATE'] = isset($tmp_ord_info['INS_DATE']) ? $tmp_ord_info['INS_DATE'] : '' ;
        $order_info['GIFT_TYPE'] = isset($tmp_ord_info['GIFT_TYPE']) ? $tmp_ord_info['GIFT_TYPE'] : '' ;
        $order_info['PAY_TYPE'] = isset($tmp_ord_info['PAY_TYPE']) ? $tmp_ord_info['PAY_TYPE'] : '' ;
        $order_info['DEPOSITOR'] = isset($tmp_ord_info['DEPOSITOR']) ? $tmp_ord_info['DEPOSITOR'] : '' ;
        $order_info['ORD_MSG'] = isset($tmp_ord_info['ORD_MSG']) ? $tmp_ord_info['ORD_MSG'] : '' ;
        $order_info['DEP_BIRTHDAY'] = '' ;

        if ( $order_info['RECEPTION_ID'] == 'D0000000' )
        {
            $order_info['RECEPTION'] = '' ;
        }
        else
        {
            $order_info['RECEPTION'] = $order_info['RECEPTION_NAME']." (".$order_info['RECEPTION_ID'].")" ;
        }

        if ( isset($order_info['IMG_PATH']) )
        {
            $order_info['IMG_PATH'] = str_replace(',', '', $order_info['IMG_PATH']) ;
        }

        $head = array('title' => '주문상세정보');
        $data = array(
            'order_info'        => $order_info,
            'gift_type'         => $this->HB_product_gift_model->get_gift_type($order_info['PDT_CD']),
            'token'             => $token,
        );

        widget::run('head', $head);
        $this->load->view('order/view_starion', $data);
        widget::run('tail');
    }
}
?>