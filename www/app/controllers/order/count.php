<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Count extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('Order_model', 'S_Reason_model', 'S_Order_state_model', 'P_Calling_Plan_model'));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function day() 
    { 
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');   

        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소   
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $vender  = $seg->get('vender', 0);

        $year   = $param->get('year', date('Y'));   
        $month  = $param->get('month', date('m'));  
        $qstr   = $param->output();        

        $vender_list = $this->S_Reason_model->get_vender_list(false) ;
        $tab_list = array('일일 개통 카운팅') ;
        $tab_list = array_merge($tab_list, $vender_list) ;

        $list = $this->Order_model->get_order_daily_count_list($year, $month) ;

        $head = array('title' => '일일 개통 현황');
        $data = array
        (
            'vender_list'       => $vender_list,                        
            'year'              => $year,
            'month'             => $month,              
            'qstr'              => $qstr,
            'list'              => $list,
            'today'             => date("Ymd"),

            'tab_list'          => $tab_list,
            'vender'            => $vender,
        ) ;        

        widget::run('head', $head);
        $this->load->view('order/day_count', $data);
        widget::run('tail');
    }   

    function d_recommend() 
    { 
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');   

        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소   
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;
 
        $page  = $seg->get('page', 1); // 페이지
        $excel  = $seg->get('excel', 0); // 페이지
        $user  = $seg->get('user', '-'); // 페이지
        $qstr   = $param->output();     
        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜     

        $total_count = $this->Order_model->get_order_direct_recomend_count_total() ;

        if ( $excel )
        {
            $limit = $total_count ;
            $offset = 0 ;
        }
        else
        {
            $config['suffix']       = $qstr;        
            $config['per_page']    = 10;
            $config['total_rows']  = $total_count;
            $config['uri_segment'] = $seg->pos('page');
            $config['base_url']    = RT_PATH.'/order/count/d_recommend/user/'.$user.'/page/';      

            $CI =& get_instance();
            $CI->load->library('pagination', $config);          

            $limit = $config['per_page'] ;
            $offset = ($page - 1) * $config['per_page'];    
        }

        $result = $this->Order_model->get_order_direct_recomend_count_list($limit, $offset) ;        

        $user_name = "" ;
        $user_order_count = 0 ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - $offset - $i ;            
            $list[$i]['href']               = "/order/count/d_recommend/user/".$row['USERID']."/page/".$page.$qstr ;  

            if ( $user == $row['USERID'] )          
            {
                $user_name = $row['USERNAME'] ;
                $user_order_count = $row['CNT'] ;
            }
        }

        if ( $user_name == "" )
        {
            $user = "-" ;
        }

        $user_order_list = array() ;

        if ( $user != "-" )
        {            
            $result = $this->Order_model->get_user_order_direct_recomend_list($user) ;                

            foreach ($result as $i => $row) 
            {
                $user_order_list[$i]                       = $row ;
                $user_order_list[$i]['num']                = $user_order_count - $i ;            
                $user_order_list[$i]['href']               = "/order/view/mobile/ord_no/".$row['ORDNO'] ;            
            }
        }

        $head = array('title' => '단말기 직추천 개통 현황');
        $data = array
        (                                 
            'qstr'              => $qstr,
            'list'              => $list,            
            'total_count'       => $total_count,

            'start_date'        => $start_date,  // 시작날짜
            'finish_date'       => $finish_date, // 종료날짜     
            'user'              => $user,   
            'user_name'         => $user_name,  
            'user_order_list'   => $user_order_list,  
            'o_state_list'      => $this->S_Order_state_model->get_order_state_list(),             
        ) ;        

        if ( $excel )
        {
            $this->d_recommend_excel($data) ;
        }
        else
        {
            $data['paging'] = $CI->pagination->create_links() ;

            widget::run('head', $head);
            $this->load->view('order/d_recommend_count', $data);
            widget::run('tail');
        }
    }  

    function vender() 
    {     
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');   
        
        $data = $this->_vender() ;
        $head = array('title' => $data['vender_list'][$data['vender']].' 기기별 개통현황') ;              

        widget::run('head', $head) ;
        $this->load->view('order/vender_count2', $data) ;
        widget::run('tail') ;
    }  

    function _vender($type = '')
    {
        if ( $type == 'excel' )
        {
            $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소         
        }
        else
        {
            $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소     
        }
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $vender  = $seg->get('vender', 0);

        $year   = $param->get('year', date('Y'));   
        $month  = $param->get('month', date('m'));  
        $qstr   = $param->output();        

        $vender_list = $this->S_Reason_model->get_vender_list(false) ;
        $tab_list = array('일일 개통 카운팅') ;
        $tab_list = array_merge($tab_list, $vender_list) ;

        $model_list = $this->Order_model->get_model_list_by_vender($vender, $year, $month) ;        
        //$list = $this->Order_model->get_order_daily_count_list_by_vender($vender, $year, $month) ;

        //$calling_plan_list = $this->P_Calling_Plan_model->get_calling_plan_list($vender) ;
        $calling_plan_list = $this->Order_model->get_calling_plan_list_by_vender($vender, $year, $month) ;
        $list = $this->Order_model->get_order_daily_count_list_by_vender2($vender, $year, $month) ;        
        
        $data = array
        (
            'vender_list'       => $vender_list,                        
            'year'              => $year,
            'month'             => $month,              
            'qstr'              => $qstr,
            'brand_list'        => $model_list['BRAND_LIST'],
            'model_list'        => $model_list['MODEL_LIST'],
            'calling_plan_list' => $calling_plan_list,
            'list'              => $list,
            'today'             => date("Ymd"),

            'tab_list'          => $tab_list,
            'vender'            => $vender,
        ) ;  

        return $data ;
    }

    function excel()
    {
        $this->load->library('excel') ;

        // Create new PHPExcel object
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");     
 
        $data = $this->_vender('excel') ;      
        $cell_list = range('A', 'Z') ;

        $i = 1 ;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i", "날짜") ;
        $objPHPExcel->getActiveSheet()->mergeCells('A1:A3') ;

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B$i", "요금제") ;
        $objPHPExcel->getActiveSheet()->mergeCells('B1:B3') ;

        $k = 2 ;
        foreach ($data['brand_list'] as $key => $value)
        {
            $j = $k + $value - 1 ;

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k]."$i", $key) ;            
            $objPHPExcel->getActiveSheet()->mergeCells($cell_list[$k]."$i:".$cell_list[$j]."$i") ;
            
            $k = $j + 1 ;            
        }

        $i++ ;        
        foreach ($data['model_list'] as $key => $value)
        {            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$key + 2].$i, substr($value['NAME'], 5)) ;            
        }      

        $i++ ;        
        foreach ($data['model_list'] as $key => $value)
        {            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$key + 2].$i, $value['MODEL']) ;            
        }              

        $style = array
        (
            'font' => array('bold'      => true),            
            'fill' => array('type'       => PHPExcel_Style_Fill::FILL_SOLID,    
                            'color' => array('argb'=>'EEEEEE'))                              
        );

        $endCol = $objPHPExcel->getActiveSheet()->getHighestDataColumn() ;        
        $objPHPExcel->getActiveSheet()->getStyle("A1:$endCol$i")->applyFromArray($style) ;
                                    
        $i++ ;
        foreach ($data['list'] as $key => $value) 
        {
            if ( $key != 'TOTAL' && $key != 'MODEL_TOTAL' )
            {              
                $rowspan = $i + count($data['calling_plan_list']) ;

                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i", $key) ;
                $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':A'.$rowspan) ;

                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B$i", "합계") ;

                $k = 2 ;
                foreach ($data['model_list'] as $key2 => $value2)
                {
                    $nCount = isset($value[$value2['MODEL']]['TOTAL'])?$value[$value2['MODEL']]['TOTAL'] : 0 ;
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k]."$i", $nCount) ;                                            
                    $k++ ;
                }

                $objPHPExcel->getActiveSheet()->getStyle("A$i:$endCol$i")->applyFromArray($style) ;
                            
                $i++ ;                
                foreach ($data['calling_plan_list'] as $cp_cd => $cp_name)
                {
                    $k = 1 ;
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k]."$i", $cp_name) ; 
                           
                    $k++ ;                               
                    foreach ($data['model_list'] as $key2 => $value2)
                    {
                        $nCount = isset($value[$value2['MODEL']][$cp_cd])? $value[$value2['MODEL']][$cp_cd] : 0 ;

                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k]."$i", $nCount) ;                                            
                        $k++ ;
                    }                                                               

                    $i++ ;
                }                                                                          
            }            
        }

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i", "합계") ;

        $k = 2 ;
        foreach ($data['model_list'] as $key => $value)
        {                            
            $nCount = isset($data['list']['MODEL_TOTAL'][$value['MODEL']])? $data['list']['MODEL_TOTAL'][$value['MODEL']] : 0 ;                

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k]."$i", $nCount) ;                        
                        
            $k++ ;            
        }

        $objPHPExcel->getActiveSheet()->getStyle("A$i:$endCol$i")->applyFromArray($style) ;
                
        $title = $data['vender_list'][$data['vender']].' 기기별 개통현황' ;                      
        $objPHPExcel->SetStyleNoTileBar($title, $i) ;
        $objPHPExcel->CreateExcelFile($title) ;
    }  

    function d_recommend_excel($data)
    {
        $this->load->library('excel') ;

        // Create new PHPExcel object
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");     
         
        $cell_list = range('A', 'Z') ;
        $str_list = array('NO', '회원명', '회원ID', '직추천개통건수', '연락처', '비상연락처', '직급', '가입일자', '센터') ;
        $val_list = array('num', 'USERNAME', 'USERID', 'CNT', 'TEL', 'MOBILE', 'RANK_NAME', 'REG_DATE', 'CENTER_NAME') ;

        $i = 3 ;        
        foreach ($str_list as $key => $value) 
        {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$key].$i, $value) ;              
        }        
        
        $i++ ;
        foreach ($data['list'] as $row)
        {            
            foreach ($val_list as $key => $value) 
            {
                if ( $value == 'USERID' )
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($cell_list[$key]."$i", $row[$value], PHPExcel_Cell_DataType::TYPE_STRING) ;
                }
                else
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$key]."$i", $row[$value]) ;                        
                }
            }

            $i++ ;
        }                

        $title = '단말기 직추천 개통현황' ;                              

        if ( $data['start_date'] && $data['finish_date'] )
        {
            $title .= "(".$data['start_date']." ~ ".$data['finish_date'].")" ;
        }

        $i-- ;
        $objPHPExcel->CreateExcelFileWithHBDefaultStyle($title, $i) ;
    } 

    function period() 
    {     
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');   
        
        $data = $this->_period() ;
        $head = array('title' => $data['tab_list'][$data['vender']].' 기간별 개통현황') ;              

        widget::run('head', $head) ;
        $this->load->view('order/period_count', $data) ;
        widget::run('tail') ;
    }  

    function _period($type = '')
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소     
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $vender  = $seg->get('vender', 3203);

        $start_date = $param->get('start', date('Y').date('m').'01') ;
        $finish_date = $param->get('finish', date('Y').date('m').date("t")) ;
        
        $qstr   = $param->output();                

        $tab_list = $this->S_Reason_model->get_vender_list(false) ;                    

        $model_list = $this->Order_model->_get_model_list_by_vender($vender, $start_date, $finish_date) ;   
        $calling_plan_list = $this->Order_model->_get_calling_plan_list_by_vender($vender, $start_date, $finish_date) ;
        $reg_kind_list = $this->Order_model->get_reg_kind_list_by_vender($vender, $start_date, $finish_date) ;


        $list = $this->Order_model->get_order_period_count_list_by_vender($vender, $start_date, $finish_date) ;        

        $data = array
        (            
            'start_date'        => $start_date,
            'finish_date'       => $finish_date,              
            'qstr'              => $qstr,
            'brand_list'        => $model_list['BRAND_LIST'],
            'model_list'        => $model_list['MODEL_LIST'],
            'calling_plan_list' => $calling_plan_list,
            'reg_kind_list'     => $reg_kind_list,
            'list'              => $list,
            'today'             => date("Ymd"),

            'tab_list'          => $tab_list,
            'vender'            => $vender,
        ) ;  

        return $data ;
    }

    function excel_period()
    {
        $this->load->library('excel') ;

        // Create new PHPExcel object
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");     
 
        $data = $this->_period('excel') ;      

        $cell_list = array('A') ;
        $current = 'A' ;
        while ($current != 'ZZZ') 
        {
            $cell_list[] = ++$current ;
        }

        $i = 1 ;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i", "요금제") ;
        $objPHPExcel->getActiveSheet()->mergeCells('A1:A4') ;   

        $reg_kind_count = count($data['reg_kind_list']) ;     

        $k = 1 ;
        foreach ($data['brand_list'] as $key => $value)
        {
            $j = $k + ($value * $reg_kind_count) - 1 ;            

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k]."$i", $key) ;            
            $objPHPExcel->getActiveSheet()->mergeCells($cell_list[$k]."$i:".$cell_list[$j]."$i") ;            
            
            $k = $j + 1 ;            
        }

        $i++ ;      
        $k = 1 ;  
        foreach ($data['model_list'] as $key => $value)
        {         
            $j = $k + $reg_kind_count - 1 ;               

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k].$i, substr($value['NAME'], 5)) ;            
            $objPHPExcel->getActiveSheet()->mergeCells($cell_list[$k]."$i:".$cell_list[$j]."$i") ;            

            $k = $j + 1 ;    
        }      

        $i++ ;    
        $k = 1 ;      
        foreach ($data['model_list'] as $key => $value)
        {            
            $j = $k + $reg_kind_count - 1 ;               

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k].$i, $value['MODEL']) ;            
            $objPHPExcel->getActiveSheet()->mergeCells($cell_list[$k]."$i:".$cell_list[$j]."$i") ;            

            $k = $j + 1 ;    
        }     

        $i++ ;
        $k = 1 ;
        foreach ($data['model_list'] as $key => $value)
        {
            foreach ($data['reg_kind_list'] as $key2 => $value2)
            {          
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k].$i, $value2) ;                                          
                $k++ ;
            }            
        }

        $style = array
        (
            'font' => array('bold'      => true),            
            'fill' => array('type'       => PHPExcel_Style_Fill::FILL_SOLID,    
                            'color' => array('argb'=>'EEEEEE'))                              
        );

        $endCol = $objPHPExcel->getActiveSheet()->getHighestDataColumn() ;        
        $objPHPExcel->getActiveSheet()->getStyle("A1:$endCol$i")->applyFromArray($style) ;
                                                                
        $i++ ;                
        foreach ($data['calling_plan_list'] as $cp_cd => $cp_name)
        {
            $k = 0 ;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k]."$i", $cp_name) ; 
                   
            $k++ ;                               
            foreach ($data['model_list'] as $key => $value)
            {
                foreach ($data['reg_kind_list'] as $key2 => $value2)
                {          
                    $nCount = isset($data['list'][$value['MODEL']][$cp_cd][$key2]) ? $data['list'][$value['MODEL']][$cp_cd][$key2] : 0 ;

                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k]."$i", $nCount) ;                                            
                    $k++ ;
                }
            }                                                               

            $i++ ;
        }                                                                          

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i", "가입 유형별 합계") ;

        $k = 1 ;
        foreach ($data['model_list'] as $key => $value)
        {
            foreach ($data['reg_kind_list'] as $key2 => $value2)
            {          
                $nCount = isset($data['list']['MODEL_TOTAL'][$value['MODEL']][$key2]) ? $data['list']['MODEL_TOTAL'][$value['MODEL']][$key2] : 0 ;

                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k]."$i", $nCount) ;                                            
                $k++ ;
            }
        }  

        $objPHPExcel->getActiveSheet()->getStyle("A$i:$endCol$i")->applyFromArray($style) ;

        $i++ ;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i", "합계") ;

        $k = 1 ;
        foreach ($data['model_list'] as $key => $value)
        {
            $j = $k + $reg_kind_count - 1 ;                            
            
            $nCount = isset($data['list'][$value['MODEL']]['TOTAL']) ? $data['list'][$value['MODEL']]['TOTAL'] : 0 ;

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$k]."$i", $nCount) ;         
            $objPHPExcel->getActiveSheet()->mergeCells($cell_list[$k]."$i:".$cell_list[$j]."$i") ;            

            $k = $j + 1 ;                                                 
        }

        $objPHPExcel->getActiveSheet()->getStyle("A$i:$endCol$i")->applyFromArray($style) ;
                
        $title = $data['tab_list'][$data['vender']].' 기간별 개통현황('.$data['start_date'].'~'.$data['finish_date'].')' ;                      
        $objPHPExcel->SetStyleNoTileBar($title, $i) ;
        $objPHPExcel->CreateExcelFile($title) ;
    }

    function mobile()
    {
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        $data = $this->_mobile() ;
        $head = array('title' => '무선실적현황') ;

        widget::run('head', $head) ;
        $this->load->view('order/mobile_count', $data) ;
        widget::run('tail') ;
    }

    function _mobile()
    {
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $param    =& $this->param;

        $start_date = $param->get('start', date('Y').date('m').'01') ;
        $finish_date = $param->get('finish', date('Y').date('m').date("d")) ;

        $qstr   = $param->output();

        $accumulate_list = $this->Order_model->get_order_mobile_accumulate_count_list($start_date, $finish_date) ;
        $today_open_list = $this->Order_model->get_order_mobile_accumulate_count_list($finish_date, $finish_date) ;
        $order_list = $this->Order_model->get_order_mobile_count_list($finish_date, $finish_date) ;
        $not_open_list = $this->Order_model->get_order_mobile_count_list2($start_date, $finish_date, 'not_opened') ;
        $canceled_list = $this->Order_model->get_order_mobile_count_list2($start_date, $finish_date, 'canceled') ;

        $list_by_model = $this->Order_model->get_order_mobile_model_count_list($start_date, $finish_date) ;

        $vender_list = array('3203', '3201', '3207', '3208') ;
        $vender_name_list = array() ;
        $vender_name_list['3203'] = 'LG U+' ;
        $vender_name_list['3201'] = 'KT' ;
        $vender_name_list['3207'] = 'UMOBI' ;
        $vender_name_list['3208'] = 'MMOBILE' ;

        $reg_kind_list = array() ;
        $reg_kind_list['3203'] = array('3607', '3608', '3609', '3616') ;
        $reg_kind_list['3201'] = array('3601', '3602', '3603') ;
        $reg_kind_list['3207'] = array('3620', '3621', '3622') ;
        $reg_kind_list['3208'] = array('3623', '3624', '3625') ;

        $reg_kind_name_list['3203'] = array('신규가입', '번호이동', '재가입', '기기변경') ;
        $reg_kind_name_list['3201'] = array('신규가입', '번호이동', '기기변경') ;
        $reg_kind_name_list['3207'] = array('신규가입', '번호이동', '기기변경') ;
        $reg_kind_name_list['3208'] = array('신규가입', '번호이동', '기기변경') ;

        $monthly_count_list = array() ;
        $monthly_count_list['accumulate_total'] = 0 ;
        $monthly_count_list['today_open_total'] = 0 ;
        $monthly_count_list['order_total'] = 0 ;
        $monthly_count_list['not_open_total'] = 0 ;
        $monthly_count_list['canceled_total'] = 0 ;
        $card_order_list['TOTAL'] = 0 ;
        foreach ( $vender_list as $vender )
        {
            $list_by_model[$vender]['TOTAL'] = isset($list_by_model[$vender]['TOTAL']) ? $list_by_model[$vender]['TOTAL'] : 0 ;

            $monthly_count_list[$vender]['accumulate_total'] = isset($accumulate_list[$vender]) ? $accumulate_list[$vender]['TOTAL'] : 0 ;
            $monthly_count_list[$vender]['today_open_total'] = isset($today_open_list[$vender]) ? $today_open_list[$vender]['TOTAL'] : 0 ;
            $monthly_count_list[$vender]['order_total'] = isset($order_list[$vender]) ? $order_list[$vender]['TOTAL'] : 0 ;
            $monthly_count_list[$vender]['not_open_total'] = isset($not_open_list[$vender]) ? $not_open_list[$vender]['TOTAL'] : 0 ;
            $monthly_count_list[$vender]['canceled_total'] = isset($canceled_list[$vender]) ? $canceled_list[$vender]['TOTAL'] : 0 ;

            $monthly_count_list['accumulate_total'] += $monthly_count_list[$vender]['accumulate_total'] ;
            $monthly_count_list['today_open_total'] += $monthly_count_list[$vender]['today_open_total'] ;
            $monthly_count_list['order_total'] += $monthly_count_list[$vender]['order_total'] ;
            $monthly_count_list['not_open_total'] += $monthly_count_list[$vender]['not_open_total'] ;
            $monthly_count_list['canceled_total'] += $monthly_count_list[$vender]['canceled_total'] ;

            foreach ( $reg_kind_list[$vender] as $reg_kind )
            {
                $accumulate = ( isset($accumulate_list[$vender][$reg_kind]) ) ? $accumulate_list[$vender][$reg_kind] : 0 ;
                $accumulate_percent = (isset($accumulate_list[$vender]) && $accumulate_list[$vender]['TOTAL'] > 0) ? round($accumulate / $accumulate_list[$vender]['TOTAL'] * 100, 1) : 0 ;
                $monthly_count_list[$vender][$reg_kind]['accumulate'] = $accumulate . " 건 (" .$accumulate_percent. " %)";

                $today_open = ( isset($today_open_list[$vender][$reg_kind]) ) ? $today_open_list[$vender][$reg_kind] : 0 ;
                $today_open_percent = (isset($today_open_list[$vender]) && $today_open_list[$vender]['TOTAL'] > 0) ? round($today_open / $today_open_list[$vender]['TOTAL'] * 100, 1) : 0 ;
                $monthly_count_list[$vender][$reg_kind]['today_open'] = $today_open . " 건 (" .$today_open_percent. " %)";

                $order = ( isset($order_list[$vender][$reg_kind]) ) ? $order_list[$vender][$reg_kind] : 0 ;
                $order_percent = (isset($order_list[$vender]) && $order_list[$vender]['TOTAL'] > 0) ? round($order / $order_list[$vender]['TOTAL'] * 100, 1) : 0 ;
                $monthly_count_list[$vender][$reg_kind]['order'] = $order . " 건 (" .$order_percent. " %)";

                $not_open = ( isset($not_open_list[$vender][$reg_kind]) ) ? $not_open_list[$vender][$reg_kind] : 0 ;
                $not_open_percent = (isset($not_open_list[$vender]) && $not_open_list[$vender]['TOTAL'] > 0) ? round($not_open / $not_open_list[$vender]['TOTAL'] * 100, 1) : 0 ;
                $monthly_count_list[$vender][$reg_kind]['not_open'] = $not_open . " 건 (" .$not_open_percent. " %)";

                $canceled = ( isset($canceled_list[$vender][$reg_kind])) ? $canceled_list[$vender][$reg_kind] : 0 ;
                $canceled_percent = (isset($canceled_list[$vender]) && $canceled_list[$vender]['TOTAL'] > 0) ? round($canceled / $canceled_list[$vender]['TOTAL'] * 100, 1) : 0 ;
                $monthly_count_list[$vender][$reg_kind]['canceled'] = $canceled . " 건 (" .$canceled_percent. " %)";
            }

            $card_order_list[$vender] = $this->Order_model->list_count('', '', $vender, '', '', '', "2506", $start_date, $finish_date) ;
            $card_order_list['TOTAL'] += $card_order_list[$vender] ;
        }

        $data = array
        (
            'start_date'            => $start_date,
            'finish_date'           => $finish_date,
            'qstr'                  => $qstr,
            'vender_list'           => $vender_list,
            'vender_name_list'      => $vender_name_list,
            'reg_kind_list'         => $reg_kind_list,
            'reg_kind_name_list'    => $reg_kind_name_list,
            'card_order_list'       => $card_order_list,
            'monthly_count_list'    => $monthly_count_list,
            'list_by_model'         => $list_by_model,
        ) ;

        return $data ;
    }
}
?>
