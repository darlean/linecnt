<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('Order_model'));

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function count() 
    { 
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소   
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $year   = $param->get('year', date('Y'));   
        $month  = $param->get('month', date('m'));  
        $qstr   = $param->output();

        $tab_idx = $seg->get('tab', 0);   // 게시물아이디

        $group_manager_id = array('00000004', '00000005', '00000900', '00000091') ;

        //$list = $this->_list($tab_idx, $group_manager_id[$tab_idx], $year, $month) ;

        $head = array('title' => '그룹별 개통 카운팅(작업중)');
        $data = array
        (
            'group_name'        => array('가온', '위너스', '에이스', '베스트'),
            'group_manager'     => array('방문숙', '김승호', '안충훈', '김형미'),
            'group_manager_id'  => $group_manager_id, 
            'group_color'       => array('color-blue', 'color-orange', 'color-red', 'color-green'), 
            'total_count'       => 0,
            'year'              => $year,
            'month'             => $month, 
            'tab'               => $tab_idx,
            'qstr'              => $qstr,
        //    'list'              => $list['list'],
        //    'paging'            => $list['paging'],
        ) ;

        foreach ($group_manager_id as $key => $value) 
        {
            $data['group_count'][$key]['mobile'] = $this->Order_model->get_order_open_count($value, $year, $month, '2502') ;
            $data['group_count'][$key]['wired'] = $this->Order_model->get_order_open_count($value, $year, $month, '2504') ;
            $data['group_count'][$key]['card'] = $this->Order_model->get_order_open_count($value, $year, $month, '2506') ;
            $data['group_count'][$key]['total'] = $data['group_count'][$key]['mobile'] + $data['group_count'][$key]['wired'] + $data['group_count'][$key]['card'] ;
            $data['total_count'] += $data['group_count'][$key]['total'] ;
        }

        widget::run('head', $head);
        $this->load->view('order/group_count', $data);
        widget::run('tail');
    }

    private function _list($tab_idx, $in_manager_id, $in_year, $in_month) 
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소   
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $qstr  = $param->output();
        
        $total_count = $this->Order_model->get_order_open_count($in_manager_id, $in_year, $in_month) ;

        $config['suffix']       = $qstr;        
        $config['base_url']    = RT_PATH.'/order/group/count/tab/'.$tab_idx.'/page/';       
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page'); 

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        $offset = ($page - 1) * $config['per_page'];

        $result = $this->Order_model->get_order_open_list($in_manager_id, $in_year, $in_month, $config['per_page'], $offset) ;

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row) 
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
            $list[$i]['href']               = '/order/view/mobile/ord_no/'.$row['ORDNO'];
        }
            
        $data = array(
            'list' => $list,
            'paging' => $CI->pagination->create_links(),
        );

        return $data ;
    }
}
?>