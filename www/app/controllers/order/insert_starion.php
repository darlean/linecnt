<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Insert_starion extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('Basic_model', 'Policy_model', 'S_Card_model', 'S_Center_model', 'Starion_model', 'HB_temp_order_starion', 'HB_product_gift_model'));

        define('WIDGET_SKIN', 'main');
        define('CSS_SKIN', 'editor,shop.style,ace_file_input');
        
        $this->load->helper('mail') ;

        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {         
    	if (!IS_MEMBER && $index != 'pdt_cd')
        {
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login') ;
        }

        if (IS_AGENCY)
        {
            alert('일반 회원만 상품 주문이 가능합니다.', '/') ;
        }

        switch($index)
        {
            case 'pdt_cd':            
                $this->_form();
            break;
            case 'update':
                $this->_update();
            break;  
            default:
                show_404();
            break;
        }
    }

    function _form() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $pdt_cd  = $seg->get('pdt_cd', 0); 

        if ( $pdt_cd == 0 )
        {
            alert('잘못된 정보입니다.', '/order/order/starion');
            return false ;
        }        

        $token = get_token();

        $pdt_info = $this->Starion_model->get_policy_info($pdt_cd) ;

        if ( isset($pdt_info['IMG_PATH']) )
        {
            $pdt_info['pt_thumbnail'] = explode (",", $pdt_info['IMG_PATH']);
        }

        $pdt_info['PAY_TYPE'] = ($pdt_info['ISSELL_CASH'] == 'O') ? 1 : 2 ;

        // 별점
        $pdt_info['nStar'] = $this->Policy_model->get_pdt_star_count($pdt_cd) ;                       

        $head = array('title' => '상품보기');
        $data = array(            
            'pdt_info'           => $pdt_info,
            'token'              => $token,
            'card_list'          => $this->S_Card_model->get_card_list(),    
            'center_list'        => $this->S_Center_model->get_center_list(),
        );

        $data['gift_type'] = $this->HB_product_gift_model->get_gift_type($pdt_cd) ;

        widget::run('head', $head);

        if (!IS_MEMBER || IS_MANAGER || IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH)
        {
            $this->load->view('order/insert_starion_manager', $data);
        }
        else
        {
            $fields = 'USERID,USERNAME,MOBILE,ZIPCODE,ADDR1,ADDR2,JUMIN_NO' ;
            $orderer_info = $this->Basic_model->get_member($this->session->userdata('ss_mb_id'), $fields) ;
            $orderer_info['JUMIN_NO'] = substr($orderer_info['JUMIN_NO'], 0, 6) ;

            $data['orderer_info'] = $orderer_info ;

            $this->load->view('order/insert_starion2', $data);
        }

        widget::run('tail');
    }

    function _update()
    {
        check_token('order/order/starion') ;

        $this->load->helper('chkstr');  
        $config = array(
            array('field'=>'reg_name', 'label'=>'고객명', 'rules'=>'trim|required'),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) 
        {
            $this->_form();
        }
        else
        {
            $stockprice_info = $this->Policy_model->get_stock_price($_POST["PDT_CD"]) ;
            $point_info = $this->Policy_model->get_point_info($_POST["PDT_CD"]) ;

            $result = $this->HB_temp_order_starion->insert($stockprice_info, $point_info) ;

            if ( $result->STATUS == 1 )
            {
                alert($result->MESSAGE, 'order/lists_starion') ;
            }
            else
            {
                alert($result->MESSAGE, 'order/order/starion');
            }
        }
    }
}
?>