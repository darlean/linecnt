<?php
class view_starion extends CI_Controller {
	function __construct() {
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model(array('HB_temp_order_starion', 'Policy_model', 'HB_product_gift_model'));
        $this->load->config('cf_order') ;
        $this->load->helper(array('utill')) ;

        define('WIDGET_SKIN', 'main');
		define('CSS_SKIN', 'editor,shop.style');

        //$this->output->enable_profiler(TRUE);
	}

    function ord_no()
    {
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $ord_no  = $seg->get('ord_no', 0); 

        if ( $ord_no == 0 )
        {
            alert('잘못된 정보입니다.', './order/lists_starion');
            return false ;
        }

        $token = get_token();

        $order_info = $this->HB_temp_order_starion->get_order_info($ord_no) ;
        $order_info['ORDNO'] = $ord_no ;

        if ( isset($order_info['IMG_PATH']) )
        {
            $order_info['IMG_PATH'] = str_replace(',', '', $order_info['IMG_PATH']) ; 
        }

        $head = array('title' => '주문상세정보');
        $data = array(
            'order_info'        => $order_info,
            'gift_type'         => $this->HB_product_gift_model->get_gift_type($order_info['PDT_CD']),
            'token'             => $token,
        );

        widget::run('head', $head);
        $this->load->view('order/view_starion', $data);
        widget::run('tail');
    }    
}
?>