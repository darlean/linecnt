<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Insert_wired extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('Basic_model', 'Policy_model', 'S_Bank_model', 'S_Card_model', 'P_pdtmaster_model', 'S_Center_model', 'Order_model', 'Order_state_model'));
        
        define('WIDGET_SKIN', 'main');
        define('CSS_SKIN', 'editor,shop.style,ace_file_input');
        
        $this->load->helper('mail') ;        

        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {         
    	if (!IS_MEMBER && $index != 'pdt_cd')
        {
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login') ;
        }

        if (IS_AGENCY)
        {
            alert('일반 회원만 상품 주문이 가능합니다.', '/') ;
        }

        switch($index)
        {
            case 'pdt_cd':            
                $this->_form();
            break;
            case 'get_detail_info':
                $this->get_detail_info();
            break;              
            case 'update':
                $this->_update();
            break;  
            default:
                show_404();
            break;
        }
    }

    function _form() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $pdt_cd  = $seg->get('pdt_cd', 0); 

        if ( $pdt_cd == 0 )
        {
            alert('잘못된 정보입니다.', '/order/order/wired');
            return false ;
        }        

        $token = get_token();

        // pdt_info
        {
            $product_info_list = $this->Policy_model->get_policy_info_list($pdt_cd) ;                     

            $pdt_info = array() ;

            foreach ($product_info_list as $key => $product) 
            {
                if ( !isset($pdt_info['PDT_CD']) )
                {
                    $pdt_info['PDT_CD'] = $pdt_cd ;
                    $pdt_info['PDT_NAME'] = $product['PDT_NAME'] ;
                    $pdt_info['PDT_MODEL'] = $product['PDT_MODEL'] ;
                    $pdt_info['IMG_PATH'] = $product['IMG_PATH'] ;                    
                    $pdt_info['DETAIL_IMG_PATH'] = $product['DETAIL_IMG_PATH'] ;
                }

                if ( $product['PRICE_KIND'] )
                {
                    $pdt_info['calling_plan'][$product['PRICE_KIND']] = $product['PRICE_NAME'] ;
                }
            }                

            if ( isset($pdt_info['IMG_PATH']) )
            {
                $pdt_info['pt_thumbnail'] = explode (",", $pdt_info['IMG_PATH']);                                        
            }
        }        
        
        // 별점
        $pdt_info['nStar'] = $this->Policy_model->get_pdt_star_count($pdt_cd) ;                       

        $head = array('title' => '상품보기');
        $data = array(            
            'pdt_info'           => $pdt_info,            
            'receipt_kind'       => $this->Policy_model->get_receipt_kind(),
            'token'              => $token,            
            'bank_list'          => $this->S_Bank_model->get_bank_list(),          
            'card_list'          => $this->S_Card_model->get_card_list(),    
            'center_list'        => $this->S_Center_model->get_center_list(),           
        );

        widget::run('head', $head);

        if (!IS_MEMBER || IS_MANAGER || IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH)
        {            
            $this->load->view('order/insert_wired_manager', $data);
        }
        else
        {
            //$this->load->view('order/insert_wired_manager', $data);

            $fields = 'USERNAME,TEL,MOBILE,ZIPCODE,ADDR1,ADDR2,EMAIL,JUMIN_NO' ;
            $orderer_info = $this->Basic_model->get_member($this->session->userdata('ss_mb_id'), $fields) ;    
            $orderer_info['JUMIN_NO'] = substr($orderer_info['JUMIN_NO'], 0, 6) ;        

            $data['orderer_info'] = $orderer_info ;

            $this->load->view('order/insert_wired2', $data);
        }

        widget::run('tail');
    }

    function get_detail_info()
    {
        $pdt_cd = $this->input->post('pdt_cd') ;
        $price_kind = $this->input->post('price_kind') ;        

        if ( $pdt_cd != '' && $price_kind != '' )
        {
            $detail_info = $this->Policy_model->get_detail_info($pdt_cd, $price_kind) ;            

            if ( $detail_info )
            {                
                $detail_info['POINT1'] = number_format($detail_info['POINT1']) ;
                $detail_info['POINT2'] = number_format($detail_info['POINT2']) ;                
                $detail_info['POINT3'] = number_format($detail_info['POINT3']) ;                
                $detail_info['POINT4'] = number_format($detail_info['POINT4']) ;                
                $detail_info['left_days'] = (isset($detail_info['E_DATE'])) ? floor((strtotime($detail_info['E_DATE']) - time())/60/60/24) : 0 ;                        

                echo json_encode($detail_info) ;
                return TRUE ;            
            }
        }

        echo 'NULL' ;
        return FALSE ;
    }

    function _update()
    {
        check_token('order/order/wired') ;

        $this->load->helper('chkstr');  
        $config = array(
            array('field'=>'reg_name', 'label'=>'가입자명', 'rules'=>'trim|required'),            
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) 
        {
            $this->_form();
        }
        else
        {
            $pdt_cd = $this->input->post('pdt_cd') ;   
            $ptp_id = $this->input->post('ptp_id') ;         
            
            $stockprice_info = $this->Policy_model->get_stock_price($pdt_cd) ;
            $pdt_master_info = $this->Policy_model->get_master_info($pdt_cd) ;  
            $pdt_telprice_info = $this->Policy_model->get_telprice_info($ptp_id) ;            

            $sum_price_info = $this->Policy_model->get_sum_point_info($ptp_id, $pdt_cd) ;         

            $order_info = $this->Order_model->create_order($pdt_master_info, $stockprice_info, $sum_price_info, 'wired') ;      

            if ( $order_info->STATUS == '1' )
            {                      
                $order_detail = $this->Order_model->create_detail($order_info->NEW_ORDER, $pdt_master_info, $stockprice_info, $sum_price_info) ;

                if ( $order_detail->STATUS == '1' )
                {                    
                    $order_delivery = $this->Order_model->create_delivery($order_info->NEW_ORDER, 'wired') ;

                    if ( $order_delivery->STATUS == '1' )
                    {
                        $save_ctel = $this->Order_model->save_ctel($order_info->NEW_ORDER, $pdt_master_info, $pdt_telprice_info) ;

                        if ( $save_ctel->STATUS == '1' )
                        {                               
                            $seller_id = $this->input->post('seller_id') ;
                            $this->Order_state_model->insert($order_info->NEW_ORDER, 1, "", $seller_id) ;                                                                                                     
                             
                            //mail_sender("gyrhkd620@hbn.co.kr,jyj@hbn.co.kr,shwncks@hbn.co.kr", "New 유선상품 주문 : ".$order_info->NEW_ORDER, $order_info->NEW_ORDER) ;
                             
                            alert('주문이 완료되었습니다.', 'order/lists/tab/1') ;                          
                        }
                        else
                        {
                            alert( 'SAVE_CTEL : '.$save_ctel->MESSAGE ) ;
                        }    
                    }
                    else
                    {
                        alert( 'CREATE_DELIVERY : '.$order_delivery->MESSAGE ) ;
                    }                                        
                }                
                else
                {
                    alert( 'CREATE_DETAIL : '.$order_detail->MESSAGE ) ;
                }                
            }
            else
            {
                alert( 'CREATE_ORDER : '.$order_info->MESSAGE ) ;
            }         
        }
    }
}
?>