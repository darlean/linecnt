<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Insert_card extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('HB_card2_model', 'S_Reason_model', 'Basic_model', 'Policy_model', 'S_Bank_model', 'Order_model', 'S_Center_model', 'Order_state_model'));
        
        define('WIDGET_SKIN', 'main');
        define('CSS_SKIN', 'editor,shop.style,ace_file_input');

        $this->load->helper('mail') ;        

        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {         
    	if (!IS_MEMBER)
        {
			alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login') ;
        }

        if (IS_AGENCY)
        {
            alert('일반 회원만 상품 주문이 가능합니다.', '/') ;
        }

        switch($index)
        {
            case 'pdt_cd':            
                $this->_form();
            break;
            case 'update':
                $this->_update();
            break;
            default:
                show_404();
            break;
        }
    }

    function _form() 
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $pdt_cd  = $seg->get('pdt_cd', 0); 

        if ( $pdt_cd == 0 )
        {
            alert('잘못된 정보입니다.', '/order/order/card');
            return false ;
        }        

        $token = get_token();
        
        $card_info = $this->HB_card2_model->get_card_info($pdt_cd) ;       

        // 별점
        $card_info['nStar'] = $this->Policy_model->get_pdt_star_count($pdt_cd) ;     

        $detail_info = $this->Policy_model->get_point_info($pdt_cd) ;

        if ( $detail_info )
        {
            $card_info['POINT1'] = number_format($detail_info->POINT1) ;
            $card_info['POINT2'] = number_format($detail_info->POINT2) ;            
            $card_info['POINT3'] = number_format($detail_info->POINT3) ;
            $card_info['POINT4'] = number_format($detail_info->POINT4) ;
        }             

        $vender_list = $this->S_Reason_model->get_vender_list(true) ;

        $head = array('title' => '상품보기');
        $data = array(            
            'card_info'          => $card_info,            
            'receipt_kind'       => $this->Policy_model->get_receipt_kind(),
            'token'              => $token,
            'vender_list'        => $vender_list,  
            'bank_list'          => $this->S_Bank_model->get_bank_list(),     
            'center_list'        => $this->S_Center_model->get_center_list(),     
        );        

        widget::run('head', $head);

        if (IS_MANAGER || IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH)
        {
            $this->load->view('order/insert_card_manager', $data);
        }
        else
        {
            $fields = 'USERNAME,TEL,MOBILE,ZIPCODE,ADDR1,ADDR2,EMAIL' ;
            $orderer_info = $this->Basic_model->get_member($this->session->userdata('ss_mb_id'), $fields) ;            
            $data['orderer_info'] = $orderer_info ;
        
            $this->load->view('order/insert_card2', $data);
        }

        widget::run('tail');
    }

    function _update()
    {
        check_token('order/order/card') ;

        $this->load->helper('chkstr');  
        $config = array(
            array('field'=>'reg_name', 'label'=>'가입자명', 'rules'=>'trim|required'),            
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) 
        {
            $this->_form();
        }
        else
        {
            $pdt_cd = $this->input->post('pdt_cd') ;            
            
            $stockprice_info = $this->Policy_model->get_stock_price($pdt_cd) ;
            $pdt_master_info = $this->Policy_model->get_master_info($pdt_cd) ;  
            $stockpoint_info = $this->Policy_model->get_point_info($pdt_cd) ;              

            $order_info = $this->Order_model->create_order($pdt_master_info, $stockprice_info, $stockpoint_info) ;      

            if ( $order_info->STATUS == '1' )
            {                      
                $order_detail = $this->Order_model->create_detail($order_info->NEW_ORDER, $pdt_master_info, $stockprice_info, $stockpoint_info) ;

                if ( $order_detail->STATUS == '1' )
                {                    
                    $order_delivery = $this->Order_model->create_delivery($order_info->NEW_ORDER, 'card') ;

                    if ( $order_delivery->STATUS == '1' )
                    {
                        $save_acard = $this->Order_model->save_acard($order_info->NEW_ORDER) ;

                        if ( $save_acard->STATUS == '1' )
                        {   
                            $this->Order_state_model->insert($order_info->NEW_ORDER, 1) ;                                                                                                                
                            
                            mail_sender("gyrhkd620@hbn.co.kr,jyj@hbn.co.kr,shwncks@hbn.co.kr", "New 제휴 카드 주문 : ".$order_info->NEW_ORDER, $order_info->NEW_ORDER) ;

                            alert('주문이 완료되었습니다.', 'order/lists/tab/2') ;                          
                        }
                        else
                        {
                            alert( 'SAVE_ACARD : '.$save_acard->MESSAGE ) ;
                        }             
                    }
                    else
                    {
                        alert( 'CREATE_DELIVERY : '.$order_delivery->MESSAGE ) ;
                    }
                }                
                else
                {
                    alert( 'CREATE_DETAIL : '.$order_detail->MESSAGE ) ;
                }                
            }
            else
            {
                alert( 'CREATE_ORDER : '.$order_info->MESSAGE ) ;
            }         
        }
    }
}
?>