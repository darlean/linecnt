<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lists_used_mobile extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('HB_sell_used_mobile_model', 'S_Rank_model', 'HB_Used_mobile_order_state_model', 'S_Center_model', 'S_Reason_model'));
        $this->load->helper(array( 'url', 'utill' ));
        $this->load->config('cf_order') ;
        $this->load->config('cf_position') ;        

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'center':
            case 'page':
            case 'index':
            case 'tab':
                $this->list_used_mobile() ;
            break;            
            case 'excel' :
                $this->excel() ;
            break;
            case 'del' :
                $this->_del() ;
            break ;
            default:
                show_404();
            break;
        }
    }

    function excel()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;
        $tab  = $seg->get('tab', 0);

        $this->load->library('excel') ;

        // Create new PHPExcel object
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");

        $cell_list = range('A', 'Z') ;

        $str_list = array( 'NO', '주문번호', '상품명', '사업자명', '연락처', '매입상태', '판매자메모', '접수일자', '판매센터', '센터전달방법', '확정매입가', '매입일자' ) ;
        $val_list = array( 'num', 'ORDNO', 'PDT_NAME', 'USERNAME', 'ORD_MOBILE', 'ORD_STATE', 'ORD_MSG', 'ORD_DATE', 'ORD_CENTER', 'SEND_TYPE', 'CONFIRM_AMT', 'CONFIRM_DATE' ) ;
 
        $i = 3 ;

        foreach ($str_list as $key => $value) 
        {            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$key]."$i", $str_list[$key]) ;
        }                   

        $data = $this->list_used_mobile('excel') ;
        $i++ ;
        foreach ($data['list'] as $key => $row) 
        {
            $order_state = isset($data['o_state_list'][$row['ORD_STATE']])?$data['o_state_list'][$row['ORD_STATE']] : $data['o_state_list'][1] ;

            foreach ($val_list as $idx => $value) 
            {
                if ( $value == 'ORD_MOBILE' )
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($cell_list[$idx]."$i", $row[$value], PHPExcel_Cell_DataType::TYPE_STRING) ;
                }
                else if ( $value == 'ORD_STATE' )
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$idx]."$i", "$order_state") ;
                }
                else
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$idx]."$i", $row[$value]) ;
                }
            }

            $i++ ;
        }

        $title = '중고폰판매내역';

        $title .= '_'.$data['o_state_list'][$tab] ;

        $i-- ;
        $objPHPExcel->CreateExcelFileWithHBDefaultStyle($title, $i) ;
    }
    
    function list_used_mobile($in_excel = '')
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $tab  = $seg->get('tab', 0);
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sfl   = $param->get('sfl');   // 검색필드
        $stx   = $param->get('stx');   // 검색어
        $sca   = $param->get('sca');   // 분류

        $sdt   = $param->get('sdt');        // 날짜검색타입
        $sdt_s   = $param->get('sdt_s');    // 날짜검색타입명
        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜

        $vender = $param->get('vender') ;

        $center  = $seg->get('center'); // 페이지

        // 정렬
        if (!$sst)
        {
            $sst = 'ORDNO';
            $sod = 'desc';
        }

        $state = $tab - 1 ;

        $total_count = $this->HB_sell_used_mobile_model->list_count($sfl, $stx, $vender, $state, $center);

        $config['suffix']       = $qstr;
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        //  페이지 선택 후,  검색시 오류 방지
        $page = ( $total_count < ($page - 1) * $config['per_page'] ) ? 1 : $page ;

        $config['base_url']    = RT_PATH.'/order/lists_used_mobile/tab/'.$tab.'/page/';

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        if ( $in_excel == 'excel' )
        {
            $limit = $total_count ;
            $offset = 0 ;
        }
        else
        {
            $limit = $config['per_page'] ;
            $offset = ($page - 1) * $config['per_page'];
        }

        $result = $this->HB_sell_used_mobile_model->list_result($sst, $sod, $sfl, $stx, $limit, $offset, $vender, $state, $center);

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row)
        {
            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;
            $list[$i]['href']               = '/order/view/used_mobile/ord_no/' . $row['ORDNO'];

            if ( $list[$i]['SEND_TYPE'] == 0 )
                $list[$i]['SEND_TYPE'] = '센터방문' ;
            else
                $list[$i]['SEND_TYPE'] = '택배전달' ;

            if ( $list[$i]['CONFIRM_AMT'] == '' )
            {
                $list[$i]['CONFIRM_AMT'] = '미정';
                $list[$i]['CONFIRM_DATE'] = '' ;
            }
            else
                $list[$i]['CONFIRM_AMT'] = number_format($list[$i]['CONFIRM_AMT']) ;
        }

        $o_state_list = $this->HB_Used_mobile_order_state_model->get_used_mobile_order_state_list() ;

        $tab_list = array ( '전체' ) ;
        $tab_list = array_merge($tab_list, $o_state_list) ;

        $data = array(
            'total_count' => $total_count,
            'sst' => $sst,
            'sod' => $sod,
            'sca' => $sca,
            'sfl' => $sfl,
            'stx' => $stx,
            'list' => $list,
            'paging' => $CI->pagination->create_links(),
            'o_state_list' => $o_state_list,
            'vender_list' => $this->S_Reason_model->get_vender_list(),
            'mb_position'   => $this->S_Rank_model->get_rank_list(),
            'mb_position_color' => $this->config->item('mb_position_color'),

            'tab_list'  => $tab_list,
            'tab'       => $tab,

            'vender' => $vender,
            'qstr' => $qstr,

            'sdt'           => $sdt,        // 날짜검색타입
            'sdt_s'         => $sdt_s,      // 날짜검색타입명
            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜

            'sort_no' => $param->sort('ORDNO', 'desc'),
        );

        if ( $in_excel == 'excel' )
        {
            return $data ;
        }
        else
        {
            $head = array('title' => '중고폰판매내역');

            if ( $center != '' )
            {
                $center_name = $this->S_Center_model->get_center_name($center) ;
                $head['title'] = $center_name." ". $head['title'];
            }

            widget::run('head', $head);
            $this->load->view('order/lists_used_mobile', $data);
            widget::run('tail');
        }
    }

    function _del()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소

        $or_id = $this->seg->get('or_id') ;
        $result = $this->HB_sell_used_mobile_model->delete($or_id);

        if ( $result )
        {
            alert('중고폰 매입 내역이 삭제되었습니다.', 'order/lists_used_mobile') ;
        }
    }
}
?>