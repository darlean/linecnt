<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Insert_used_mobile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('Basic_model', 'Policy_model', 'HB_sell_used_mobile_model', 'S_Center_model'));

        define('WIDGET_SKIN', 'main');
        define('CSS_SKIN', 'editor,shop.style,ace_file_input');

        $this->load->helper('mail') ;

        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {
        if (!IS_MEMBER)
        {
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login') ;
        }

        if (IS_AGENCY)
        {
            alert('일반 회원만 상품 주문이 가능합니다.', '/') ;
        }

        switch($index)
        {
            case 'pdt_cd':
                $this->_form();
                break;
            case 'update':
                $this->_update();
                break;
            case 'modify':
                $this->_modify();
                break;
            case 'center_modify':
                $this->_center_modify() ;
                break ;
            default:
                show_404();
                break;
        }
    }

    function _form()
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $pdt_cd  = $seg->get('pdt_cd', 0);

        if ( $pdt_cd == 0 )
        {
            $pdt_cd = $this->input->post('PDT_CD') ;
        }

        if ( $pdt_cd == 0 )
        {
            alert('잘못된 정보입니다.', '/order/order/used_mobile');
            return false ;
        }

        $token = get_token();

        $pdt_info = $this->Policy_model->get_used_mobile_policy_info($pdt_cd) ;

        if ( isset($pdt_info['IMG_PATH']) )
        {
            $pdt_info['pt_thumbnail'] = explode (",", $pdt_info['IMG_PATH']);
        }

        $fields = 'USERNAME,MOBILE' ;
        $orderer_info = $this->Basic_model->get_member($this->session->userdata('ss_mb_id'), $fields) ;

        $head = array('title' => '상품보기');
        $data = array(
            'pdt_info'           => $pdt_info,
            'orderer_info'       => $orderer_info,
            'center_list'        => $this->S_Center_model->get_center_list(),
            'token'              => $token,
        );

        widget::run('head', $head);
        $this->load->view('order/insert_used_mobile', $data);
        widget::run('tail');
    }

    function _update()
    {
        check_token('order/order/used_mobile') ;

        $this->load->helper('chkstr');
        $config = array(
            array('field'=>'ORD_NAME', 'label'=>'판매자명', 'rules'=>'trim|required'),
            array('field'=>'ORD_MOBILE', 'label'=>'휴대폰 번호', 'rules'=>'trim|required'),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE)
        {
            $this->_form();
        }
        else
        {
            $result = $this->HB_sell_used_mobile_model->insert() ;

            if ( $result != 0 )
            {
                $result = $this->upload_image($this->input->post('PDT_CD'), $result) ;

                if ( $result )
                {
                    alert('매입이 완료되었습니다.', 'order/lists_used_mobile');
                }
            }
            else
            {
                alert('DB 연결이 실패하였습니다.', 'order/lists_used_mobile') ;
            }
        }
    }

    public function upload_image($in_pdt_cd, $in_ord_no)
    {
        $IMAGE_PATH = '' ;

        if ( count($_FILES) > 0 )
        {
            $this->load->library('upload') ;

            $config['upload_path'] = "./data/used_mobile_image/" ;
            $config['open_file_path'] = "/data/used_mobile_image/" ;

            $config['allowed_types'] = 'gif|jpg|png|gif|bmp|pdf';
            $config['max_size'] = '1536' ;
            $config['max_width'] = '3200';
            $config['max_height'] = '3200';

            if(!is_dir($config['upload_path']))
            {
                umask( 0 );
                mkdir($config['upload_path'], 0777) ;
            }

            for ( $i = 1 ; $i < 3 ; $i++ )
            {
                $file_form_name = 'file'.$i;

                if ($_FILES[$file_form_name]["size"] > 0 && $_FILES[$file_form_name]["name"] != 'none') {
                    if ($_FILES[$file_form_name]["error"] <= 0) {
                        $ext = array_pop(explode(".", $_FILES[$file_form_name]['name'])); // 확장자
                        $config['file_name'] = $in_ord_no . "_" . time() . "_".$i."." . $ext;

                        $this->upload->initialize($config);

                        if ($this->upload->do_upload($file_form_name)) {
                            if ( $IMAGE_PATH != '' )
                                $IMAGE_PATH .= "," ;

                            $IMAGE_PATH .= $config['open_file_path'] . $config['file_name'];
                        } else {
                            alert($this->upload->display_errors());
                        }
                    }
                }
            }
        }

        return $this->HB_sell_used_mobile_model->insert_mobile_image( $in_ord_no, $IMAGE_PATH ) ;
    }

    function _modify()
    {
        $or_id = $this->input->post('ORDNO') ;

        $result = $this->HB_sell_used_mobile_model->complete($or_id) ;

        if ( $result )
        {
            alert('수정되었습니다.', 'order/lists_used_mobile');
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }

    function _center_modify()
    {
        $or_id = $this->input->post('ORDNO') ;

        $data = array(
            'CENTER_MSG'       => $this->input->post('CENTER_MSG'),
        );

        $result = $this->HB_sell_used_mobile_model->update($or_id, $data) ;

        if ( $result )
        {
            alert('수정되었습니다', URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }
}
?>