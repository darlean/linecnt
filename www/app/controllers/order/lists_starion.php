<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lists_starion extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('Order_model', 'S_Order_state_model', 'S_Reason_model', 'Policy_model', 'P_ptplink_model', 'S_Rank_model', 'HB_temp_order_starion', 'HB_product_gift_model'));
        $this->load->helper(array( 'url', 'utill' ));
        $this->load->config('cf_order') ;
        $this->load->config('cf_position') ;

        define('WIDGET_SKIN', 'main');
        
        //$this->output->enable_profiler(TRUE);
    }

    function _remap($index)
    {        
        if (!IS_MEMBER)
            alert('로그인 한 회원만 접근하실 수 있습니다.', 'member/login');

        switch($index)
        {
            case 'page':
            case 'tab' ;
            case 'index':
                $this->list_starion_before_open() ;
            break;            
            case 'excel' :
                $this->excel() ;
            break;       
            case 'del_last_day':
                $this->del_last_day() ;
            break ;
            case 'del' :
                $this->_del() ;
            break ;
            default:
                show_404();
            break;
        }
    }

    function excel()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;
        $tab  = $seg->get('tab', 0);

        $this->load->library('excel') ;

        // Create new PHPExcel object
        $objPHPExcel = $this->excel ;
        
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Encoding: utf-8");

        $cell_list = range('A', 'Z') ;

        $str_list = array( 'NO', '주문번호', '사업자명', '상품명', '명의자', '연락처', '사업자연락처', '주문상태', '메모', '주문센터', '주문일자', '결제방법' ) ;
        $val_list = array( 'num', 'TMP_ORDNO', 'USERNAME', 'PDT_NAME', 'ORD_NAME', 'ORD_MOBILE', 'MOBILE', 'STATE_CD', 'ORD_MSG', 'CENTER_NAME', 'ORD_DATE', 'PAY_TYPE' ) ;
 
        $i = 3 ;

        foreach ($str_list as $key => $value) 
        {            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$key]."$i", $str_list[$key]) ;
        }                   

        $data = $this->list_starion_before_open('excel') ;
        $i++ ;
        foreach ($data['list'] as $key => $row) 
        {
            $order_state = isset($data['o_state_list'][$row['STATE_CD']])?$data['o_state_list'][$row['STATE_CD']] : $data['o_state_list'][1] ;

            foreach ($val_list as $idx => $value) 
            {
                if ( $value == 'ORD_TEL' || $value == 'OLD_TEL' )
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($cell_list[$idx]."$i", $row[$value], PHPExcel_Cell_DataType::TYPE_STRING) ;
                }
                else if ( $value == 'STATE_CD' )
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$idx]."$i", "$order_state") ;
                }
                else
                {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_list[$idx]."$i", $row[$value]) ;
                }
            }

            $i++ ;
        }

        if( IS_MANAGER || IS_AGENCY || IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH || IS_CARD_AGENCY )
        {
            $tab_list = array('설치전', '설치완료') ;
        }
        else
        {
            $tab_list = array('본인주문_설치전', '본인주문_설치완료', '접수주문_설치전', '접수주문_설치완료') ;
        }

        $title = '일반상품 주문내역조회';

        $title .= '_'.$tab_list[$tab] ;

        $i-- ;
        $objPHPExcel->CreateExcelFileWithHBDefaultStyle($title, $i) ;
    }
    
    function list_starion_before_open($in_excel = '')
    {
        $this->load->library('segment', array('offset'=>3), 'seg'); // 세그먼트 주소
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $seg      =& $this->seg;
        $param    =& $this->param;

        $page  = $seg->get('page', 1); // 페이지
        $tab  = $seg->get('tab', 0);
        $qstr  = $param->output();
        $sst   = $param->get('sst');   // 정렬필드
        $sod   = $param->get('sod');   // 정렬순서
        $sfl   = $param->get('sfl');   // 검색필드
        $stx   = $param->get('stx');   // 검색어
        $sca   = $param->get('sca');   // 분류

        $sdt   = $param->get('sdt');        // 날짜검색타입
        $sdt_s   = $param->get('sdt_s');    // 날짜검색타입명
        $start_date = $param->get('start');   // 시작날짜
        $finish_date = $param->get('finish'); // 종료날짜

        $state = $param->get('state') ;

        // 정렬
        if (!$sst)
        {
            $sst = 'TMP_ORDNO';
            $sod = 'desc';
        }

        $total_count = 0 ;

        if ( $tab == 0 ) 
        {
            $total_count = $this->HB_temp_order_starion->list_count($sfl, $stx, $state);
        }
        else if ( $tab == 1 )
        {
            if ( $sfl == "A.TMP_ORDNO" ) {
                $sfl = "A.ORDNO";
            }

            $total_count = $this->Order_model->list_count($sfl, $stx, "", $state, "", "", "2501") ;
        }
        else if ( $tab == 2 )
        {
            $total_count = $this->HB_temp_order_starion->list_count($sfl, $stx, $state, "reception");
        }
        else if ( $tab == 3 )
        {
            $total_count = $this->Order_model->list_count($sfl, $stx, "", $state, "", "reception", "2501") ;
        }

        $config['suffix']       = $qstr;
        $config['per_page']    = 10;
        $config['total_rows']  = $total_count;
        $config['uri_segment'] = $seg->pos('page');

        //  페이지 선택 후,  검색시 오류 방지
        $page = ( $total_count < ($page - 1) * $config['per_page'] ) ? 1 : $page ;

        $config['base_url']    = RT_PATH.'/order/lists_starion/tab/'.$tab.'/page/';

        $CI =& get_instance();
        $CI->load->library('pagination', $config);

        if ( $in_excel == 'excel' )
        {
            $limit = $total_count ;
            $offset = 0 ;
        }
        else
        {
            $limit = $config['per_page'] ;
            $offset = ($page - 1) * $config['per_page'];
        }

        $result = array() ;

        if ( $tab == 0 ) 
        {
            $result = $this->HB_temp_order_starion->list_result($sst, $sod, $sfl, $stx, $limit, $offset, $state);
        }
        else if ( $tab == 1 ) 
        {
            $sst = 'ORDNO';
            $result = $this->Order_model->list_result($sst, $sod, $sfl, $stx, $limit, $offset, "", $state, "", "", "2501");
        }
        else if ( $tab == 2 ) 
        {
            $result = $this->HB_temp_order_starion->list_result($sst, $sod, $sfl, $stx, $limit, $offset, $state, "reception");
        }
        else if ( $tab == 3 ) 
        {
            $sst = 'ORDNO';
            $result = $this->Order_model->list_result($sst, $sod, $sfl, $stx, $limit, $offset, "", $state, "", "reception", "2501");
        }

        // 일반 리스트
        $list = array();
        foreach ($result as $i => $row)
        {
            if ( $tab == 1 || $tab == 3 ) 
            {
                $tmp_ord_info = array() ;

                if ( $row['ORD_MSG'] != '' ) // TMP_ORDNO 를 담음
                {
                    $tmp_ord_info = $this->HB_temp_order_starion->get_order_info($row['ORD_MSG']) ;
                    $row['ORD_MSG'] = $tmp_ord_info['ORD_MSG'] ;
                }

                $row['TMP_ORDNO'] = $row['ORDNO'];
                $row['ORD_DATE2'] = $row['ORD_DATE'] ;
                $row['USERNAME'] = $row['ORD_NAME'] ;
                $row['CENTER_NAME'] = $row['ORDER_CENTER'] ;

                $row['ORD_MOBILE'] = isset($tmp_ord_info['REG_MOBILE']) ? $tmp_ord_info['REG_MOBILE'] : "" ;
                $row['PAY_TYPE'] = isset($tmp_ord_info['PAY_TYPE']) ? $tmp_ord_info['PAY_TYPE'] : "" ;
                $row['GIFT_TYPE'] = isset($tmp_ord_info['GIFT_TYPE']) ? $tmp_ord_info['GIFT_TYPE'] : "" ;
            }

            $list[$i]                       = $row ;
            $list[$i]['num']                = $total_count - ($page - 1) * $config['per_page'] - $i;

            $gift_type = $this->HB_product_gift_model->get_gift_type($row['PDT_CD']) ;

            if ( $row['GIFT_TYPE'] != "" )
            {
                $list[$i]['GIFT_TYPE2'] = isset($gift_type[$row['GIFT_TYPE']]) ? $gift_type[$row['GIFT_TYPE']] : "" ;
            }

            if ( $tab == 0 || $tab == 2 )
            {
                $list[$i]['href'] = '/order/view_starion/ord_no/' . $row['TMP_ORDNO'];
            }
            else
            {
                $list[$i]['href'] = '/order/view/starion/ord_no/' . $row['TMP_ORDNO'];
            }

            $filepath = "/data/hbplanner/wired_order/".$row['TMP_ORDNO'].".png" ;
            $list[$i]['order_file_path'] = file_exists(".".$filepath) ? $filepath : "" ;

            $filepath = "/data/hbplanner/wired_order2/".$row['TMP_ORDNO'].".png" ;
            $list[$i]['order_file_path2'] = file_exists(".".$filepath) ? $filepath : "" ;

            $filepath = "/data/hbplanner/wired_order3/".$row['TMP_ORDNO'].".png" ;
            $list[$i]['order_file_path3'] = file_exists(".".$filepath) ? $filepath : "" ;
        }

        if( IS_MANAGER || IS_AGENCY || IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH || IS_CARD_AGENCY )
        {
            $tab_list = array('설치전', '설치완료') ;   //var_dump($list) ;
        }
        else
        {
            $tab_list = array('본인주문_설치전', '본인주문_설치완료', '접수주문_설치전', '접수주문_설치완료') ;
        }

        $data = array(
            'total_count' => $total_count,
            'sst' => $sst,
            'sod' => $sod,
            'sca' => $sca,
            'sfl' => $sfl,
            'stx' => $stx,
            'list' => $list,
            'paging' => $CI->pagination->create_links(),
            'o_state_list' => $this->S_Order_state_model->get_starion_order_state_list(false),
            'mb_position'   => $this->S_Rank_model->get_rank_list(),
            'mb_position_color' => $this->config->item('mb_position_color'),
            'tab_list' => $tab_list,
            'tab'       => $tab,

            'state' => $state,
            'qstr' => $qstr,

            'sdt'           => $sdt,        // 날짜검색타입
            'sdt_s'         => $sdt_s,      // 날짜검색타입명
            'start_date'    => $start_date,  // 시작날짜
            'finish_date'   => $finish_date, // 종료날짜

            'sort_no' => $param->sort('TMP_ORDNO', 'desc'),
        );

        if ( $in_excel == 'excel' )
        {
            return $data ;
        }
        else
        {
            $head = array('title' => '일반상품 주문내역조회');

            widget::run('head', $head);

            if( IS_MANAGER || IS_CENTER || IS_BRANCH || IS_DIRECT_BRANCH )
            {
                $this->load->view('order/lists_starion_manager', $data);
            }
            else
            {
                $this->load->view('order/lists_starion_manager', $data);
            }
            widget::run('tail');
        }
    }

    function del_last_day() 
    {
    }

    function _del()
    {
        $this->load->library('segment', array('offset'=>4), 'seg'); // 세그먼트 주소
        $seg      =& $this->seg;

        $or_id = $this->seg->get('or_id') ;
        $result = $this->HB_temp_order_starion->delete($or_id);

        if ( $result )
        {
            alert('해당 주문건이 삭제되었습니다.', 'order/lists_starion') ;
        }
    }
}
?>