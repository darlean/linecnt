<?php
class modify extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();

        $this->load->model(array('Order_model', 'Order_state_model', 'O_Ordertel_model', 'S_Center_model', 'HB_temp_order_wired', 'Policy_model', 'HB_sell_used_mobile_model', 'HB_temp_order_starion'));
        $this->load->helper(array( 'url', 'mail' ));
        $this->load->library('querystring', NULL, 'param'); // 쿼리스트링 주소

        $this->load->helper(array('socket_helper', 'mail')) ;

        //$this->output->enable_profiler(TRUE);
    }

	function index()
	{

	} 

	function modify_msg()
	{
		$or_id = $this->param->get('or_id') ;
	
		$modify_or_data['ORD_MSG'] = urldecode($this->param->get('or_simple_msg')) ;

        $result = $this->Order_model->update_msg($or_id, $modify_or_data) ;

        if ( $result )
        {
            redirect(URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
	}

    function modify_wired_msg()
    {
        $or_id = $this->param->get('or_id') ;

        $modify_or_data['ORD_MSG'] = urldecode($this->param->get('or_simple_msg')) ;

        $result = $this->HB_temp_order_wired->update($or_id, $modify_or_data) ;

        if ( $result )
        {
            redirect(URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }

    function modify_starion_msg()
    {
        $or_id = $this->param->get('or_id') ;

        $modify_or_data['ORD_MSG'] = urldecode($this->param->get('or_simple_msg')) ;

        $result = $this->HB_temp_order_starion->update($or_id, $modify_or_data) ;

        if ( $result )
        {
            redirect(URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }

    function modify_manager_msg()
    {
        $or_id = $this->param->get('or_id') ;
    
        $modify_or_data['ORD_MANAGER_MSG'] = urldecode($this->param->get('or_simple_msg')) ;

        $result = $this->Order_state_model->update_msg($or_id, $modify_or_data) ;

        if ( $result )
        {
            redirect(URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }

    function select_open_agency()
    {
        $open_agency = $this->param->get('open_agency') ;
        $arr_ordno = $this->param->get('arr_ordno') ;
        $arr_ordno = explode(".", $arr_ordno) ;

        foreach ($arr_ordno as $key => $value)
        {
            if ( $value != '' )
            {
                $result = $this->O_Ordertel_model->update_open_agency($value, $open_agency) ;
            }
        }

        if ( $result == true )
        {
            alert('정상적으로 정보가 수정되었습니다.', URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }

    function select_order_state()
    {        
        $ord_state = $this->param->get('ord_state') ;        
        $arr_ordno = $this->param->get('arr_ordno') ;           
        $arr_ordno = explode(".", $arr_ordno) ;

        foreach ($arr_ordno as $key => $value) 
        {
            if ( $value != '' )
            {
                $result = $this->Order_state_model->update($value, $ord_state) ;

                if ( $ord_state == '6' ) // 개통완료 시 개통완료 일자 insert
                {
                    $result = $this->O_Ordertel_model->update($value) ;
                    $this->mail_to_center($value) ;
                }
                else if ( $ord_state != '7' ) // 개통취소가 아닐 때만 개통완료 일자 초기화
                {
                    $result = $this->O_Ordertel_model->back_update($value) ;
                }
            }
        }

        if ( $result == true )
        {
            alert('정상적으로 정보가 수정되었습니다.', URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }

    function select_wired_order_state()
    {
        $ord_state = $this->param->get('ord_state') ;
        $or_id = $this->param->get('or_id') ;

        $modify_or_data['STATE_CD'] = $ord_state ;

        $result = true ;

        if ( $ord_state == '6' ) // 개통완료 시 원래 DB에 추가
        {
            $result = $this->move_wired_order_to_real_db($or_id) ;
        }

        if ( $result )
            $result = $this->HB_temp_order_wired->update($or_id, $modify_or_data) ;

        if ( $result )
        {
            alert('정상적으로 정보가 수정되었습니다.', 'order/lists_wired/tab/1') ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }

    function select_starion_order_state()
    {
        $ord_state = $this->param->get('ord_state') ;
        $or_id = $this->param->get('or_id') ;

        $modify_or_data['STATE_CD'] = $ord_state ;

        $result = true ;

        if ( $ord_state == '4' ) // 개통완료 시 원래 DB에 추가
        {
            $result = $this->move_starion_order_to_real_db($or_id) ;
        }

        if ( $result )
            $result = $this->HB_temp_order_starion->update($or_id, $modify_or_data) ;

        if ( $result )
        {
            alert('정상적으로 정보가 수정되었습니다.', 'order/lists_starion/tab/1') ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }

    function select_used_mobile_order_state()
    {
        $ord_state = $this->param->get('ord_state') ;
        $or_id = $this->param->get('or_id') ;

        $modify_or_data['ORD_STATE'] = $ord_state ;

        $result = $this->HB_sell_used_mobile_model->update($or_id, $modify_or_data) ;

        if ( $result )
        {
            alert('정상적으로 정보가 수정되었습니다.', URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }

    function modify_used_mobile_msg()
    {
        $or_id = $this->param->get('or_id') ;

        $modify_or_data['ORD_MSG'] = urldecode($this->param->get('or_simple_msg')) ;

        $result = $this->HB_sell_used_mobile_model->update($or_id, $modify_or_data) ;

        if ( $result )
        {
            redirect(URL) ;
        }
        else
        {
            alert('DB에 연결 할 수 없습니다.', URL) ;
        }
    }

    function mail_to_center($in_ordno)
    {
        $email = "" ;

        if ( $in_ordno == "" )        
            return ;        

        $email = $this->S_Center_model->get_order_center_email($in_ordno) ;

        if ( $email != "" )
        {
            $order_info = $this->Order_model->get_order_info($in_ordno) ;

            if ( $order_info )
            {                
                mail_sender($email, "명의자 ".$order_info['REG_NAME']."(".$order_info['ORDNO'].")가 개통 완료 되었습니다.", "") ;            
                //mail_sender("sadlyfox@gmail.com", "명의자 ".$order_info['REG_NAME']."(".$order_info['ORDNO'].")가 개통 완료 되었습니다.", "") ;            
            }
        }
    }

    // 서류 확인으로 상태 변경시 공제 번호 신고 
    function publish_mlm_order($in_ordno)
    {
        /*if ( $in_ordno == "" )
            return ;

        $order_info = $this->Order_model->get_order_info($in_ordno) ;

        if ( $order_info && $order_info['GT_CD'] == ""  && $order_info['AMT'] > 0 )
        {            
            $postValues = "ORDERNO=".$in_ordno ;

            if ( IS_TEST_SERVER )
            {
                $post_result = $this->post_request("http://openpageutf.cafe24.com/guild/hbtest/mlmorder.php", $postValues) ;
            }
            else
            {
                $post_result = $this->post_request("http://vansales.cafe24.com/guild/hbnetworks/mlmorder.php", $postValues) ;  
            }
        }*/
    }

    function move_wired_order_to_real_db($in_ord_no)
    {
        $order_info = $this->HB_temp_order_wired->get_temp_order_info($in_ord_no) ;

        $pdt_master_info = $this->Policy_model->get_master_info($order_info['PDT_CD']) ;
        $pdt_telprice_info = $this->Policy_model->get_telprice_info($order_info['PTP_ID']) ;

        $create_order_info = $this->HB_temp_order_wired->create_order($order_info, $pdt_master_info) ;

        if ( $create_order_info->STATUS == '1' )
        {
            $order_detail = $this->HB_temp_order_wired->create_detail($create_order_info->NEW_ORDER, $order_info, $pdt_master_info) ;

            if ( $order_detail->STATUS == '1' )
            {
                $order_delivery = $this->HB_temp_order_wired->create_delivery($create_order_info->NEW_ORDER, $order_info) ;

                if ( $order_delivery->STATUS == '1' )
                {
                    $save_ctel = $this->HB_temp_order_wired->save_ctel($create_order_info->NEW_ORDER, $order_info, $pdt_master_info, $pdt_telprice_info) ;

                    if ( $save_ctel->STATUS == '1' )
                    {
                        $this->Order_state_model->insert($create_order_info->NEW_ORDER, 6, "", $order_info['RECEPTION_ID']) ;

                        return true ;
                    }
                    else
                    {
                        alert( 'SAVE_CTEL : '.$save_ctel->MESSAGE, URL ) ;
                    }
                }
                else
                {
                    alert( 'CREATE_DELIVERY : '.$order_delivery->MESSAGE, URL ) ;
                }
            }
            else
            {
                alert( 'CREATE_DETAIL : '.$order_detail->MESSAGE, URL ) ;
            }
        }
        else
        {
            alert( 'CREATE_ORDER : '.$create_order_info->MESSAGE, URL ) ;
        }

        return false ;
    }

    function move_starion_order_to_real_db($in_ord_no)
    {
        $order_info = $this->HB_temp_order_starion->get_temp_order_info($in_ord_no) ;
        $pdt_master_info = $this->Policy_model->get_master_info($order_info['PDT_CD']) ;

        $create_order_info = $this->HB_temp_order_starion->create_order_normal($order_info, $pdt_master_info) ;

        if ( $create_order_info->STATUS == '1' )
        {
            $order_detail = $this->HB_temp_order_starion->create_detail($create_order_info->NEW_ORDER, $order_info, $pdt_master_info) ;

            if ( $order_detail->STATUS == '1' )
            {
                $order_delivery = $this->HB_temp_order_starion->create_delivery($create_order_info->NEW_ORDER, $order_info) ;

                if ( $order_delivery->STATUS == '1' )
                {
                    $order_money = $this->HB_temp_order_starion->add_order_money($create_order_info->NEW_ORDER, $order_info) ;

                    if ( $order_money->STATUS == '1' )
                    {
                        $this->Order_state_model->insert($create_order_info->NEW_ORDER, 4, "", $order_info['RECEPTION_ID']) ;

                        $postValues = "ORDERNO=".$create_order_info->NEW_ORDER ;

                        /*if ( IS_TEST_SERVER )
                        {
                            $result = $this->post_request("http://openpageutf.cafe24.com/guild/hbtest/mlmorder.php", $postValues) ;
                        }
                        else
                        {
                            $result = $this->post_request("http://vansales.cafe24.com/guild/hbnetworks/mlmorder.php", $postValues) ;
                        }*/

                        return true ;
                    }
                    else
                    {
                        alert( 'ADD_ORDER_MONEY : '.$order_money->MESSAGE, URL ) ;
                    }
                }
                else
                {
                    alert( 'CREATE_DELIVERY : '.$order_delivery->MESSAGE, URL ) ;
                }
            }
            else
            {
                alert( 'CREATE_DETAIL : '.$order_detail->MESSAGE, URL ) ;
            }
        }
        else
        {
            alert( 'CREATE_ORDER_NORMAL : '.$create_order_info->MESSAGE, URL ) ;
        }

        return false ;
    }

    function post_request($url, $data)
    {
        // parse the given URL
        $url = parse_url($url);

        if ($url['scheme'] != 'http') {
            return "Error:Only HTTP request are supported!";
        }

        // extract host and path:
        $res = fsk_open($url['host'], $url['path'], $data) ;

        $content = explode("\r\n", $res, 4);

        $result['CODE'] = isset($content[1]) ? substr($content[1], 0, 4) : '' ;
        $result['GT_CD'] = isset($content[1]) ? substr($content[1], 4) : '' ;

        return $result ;
    }
}
?>