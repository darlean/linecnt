<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['mb_position'] = array(
	'9'		 => '비기너',
	'11'	 => '브론즈',
	'21'     => '실버',
	'31'	 => '골드',
	'41'	 => '루비',
	'51'	 => '사파이어',
	'61'	 => '다이아몬드',
	'71'	 => '크라운 로얄',
);

$config['mb_position_color'] = array(
	'9'		 => 'color-dark-blue',
	'11'     => 'color-brown',
	'21'	 => 'color-grey',
	'31'	 => 'color-yellow',
	'41'	 => 'color-red',
	'51'	 => 'color-light-green',
	'61'	 => 'color-blue',	
	'71'	 => 'color-purple',
);
?>