<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 define('G_CRYPTO', 'visionplus20140328!@#$%^') ; // 암호화키


/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* 사용자 정의 */
define('ADMIN', 'admin'); // 최고관리자
define('ADM_F', 'adm'); // 관리자폴더
define('MANAGER_LV', 10 );

define('RT_PATH', ''); // ex) /test
define('SKIN_PATH', $_SERVER['DOCUMENT_ROOT'].RT_PATH.'/skin/');

define('IMG_DIR', RT_PATH.'/assets/img');
define('JS_DIR',  RT_PATH.'/assets/js');
define('CSS_DIR', RT_PATH.'/assets/css');
define('DATA_DIR', RT_PATH.'/data');
define('PLUGIN_DIR', RT_PATH.'/assets/plugins');
define('DATA_PATH', $_SERVER['DOCUMENT_ROOT'].DATA_DIR);
define('EDT_DIR', RT_PATH.'/editor');

define('TIME_YMD2', date('Y/m/d', time()));
define('TIME_YMD', date('Y-m-d', time()));
define('TIME_HIS', date('H:i:s', time()));
define('TIME_YMDHIS', date('Y-m-d H:i:s', time()));

// 회원 등급
define('BEGINNER', 0 );
define('BRONZE', 1 );
define('SILVER', 2 );
define('GOLD', 3 );
define('PLATINUM', 4 );
define('DIAMOND', 5 );
define('CROWN', 6 );
define('CROWN_ROYAL', 7 );

// 수당 타입
define('SUPPORT_DIRECT', 0 );
define('SUPPORT_SALES_FOR_BRONZE', 1 );
define('SUPPORT_SALES_FOR_SILVER', 2 );
define('SUPPORT_GOLD_UPPER_BONUS', 3 );
define('SUPPORT_GOLD_BONUS', 4 );
define('SUPPORT_PLATINUM_BONUS', 5 );
define('SUPPORT_DIAMOND_UPPER_BONUS', 6 );

// 지급 결과
define('PAYMENT_SUCCESS', 0 );
define('PAYMENT_FAIL', 1 );
define('PAYMENT_RECOMMAND_DATE_EXPIRED', 2 );



define('DIRECT_PAYMENT_RATIO', 0.4);					// 정착지원 40%
define('BRONZE_FOR_SALES_PAYMENT_RATIO', 0.07);			// 브론즈 영업지원 7%
define('SILVER_FOR_SALES_PAYMENT_RATIO', 0.03);			// 실버 영업지원 3%
define('GOLD_UPPER_BONUS_RATIO', 0.5);					// 골드 이상 공유보너스 50%

define('GOLD_UPPER_BONUS_RATIO_ALL', 0.6);				// 골드 이상 공유보너스 전체 직급자 60%
define('GOLD_UPPER_BONUS_RATIO_ONLY_GOLD', 0.25);		// 골드 이상 공유 보너스 골드만 지급 25%
define('GOLD_UPPER_BONUS_RATIO_ONLY_PLATINUM', 0.15);	// 골드 이상 공유 보너스 플래티넘만 지급 15%


define('PAYMENT_TERMS_COUNT', 15);
define('RECOMMAND_DAY_LIMIT', 60);


/*
 *	Android Session Constants
 */ 
defined('SESSION_OK') 			or define('SESSION_OK', 0);
defined('SESSION_EXPIRED') 		or define('SESSION_EXPIRED', 1);
defined('SESSION_REMAKED') 		or define('SESSION_REMAKED', 2);
defined('SESSION_EMPTY') 		or define('SESSION_EMPTY', 3);

defined('CURR_HBPLANNER_VER') 		or define('CURR_HBPLANNER_VER', "1.0.79");

/* End of file constants.php */
/* Location: ./application/config/constants.php */