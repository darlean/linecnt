<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['weekly_acount'] = array(
	'0'		 => '브론즈 이상 정착지원',
	'1'		 => '브론즈 영업지원',
	'2'      => '실버 영업지원',
	'3'		 => '골드 이상 공유보너스',
	'4'		 => '골드 등급 공유보너스',
	'5'		 => '플래티넘 등급 공유보너스',
	'6'		 => '다이아몬드 이상 요율 보너스',
);

$config['payment_state'] = array(
	'0'		=> '정상지급',
	'1'		=> '미지급',
	'2'		=> '60일 이내 1명 미추천'
	);