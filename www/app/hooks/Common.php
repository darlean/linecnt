<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class _Common {
	function index() 
	{
		$CI =& get_instance();

		/*header("Content-Type: text/html; charset=".$CI->config->item('charset'));
		header("Expires: 0"); // rfc2616 - Section 14.21
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
		header("Cache-Control: pre-check=0, post-check=0, max-age=0"); // HTTP/1.1
		header("Pragma: no-cache"); // HTTP/1.0*/
		
		$is_member = $is_manager = $is_super = $is_hbn_cast = $is_recycle = FALSE;
		$login_id = $CI->session->userdata('ss_mb_id');
		$RANK_NAME = $CI->session->userdata('RANK_NAME');

		//exit( make_session_result( array('dd' => 'dd' ) ) );

		//return;

		$is_manager = $CI->session->userdata('is_manager');		
		$is_agency = $CI->session->userdata('is_agency');		
		$is_card_agency = $CI->session->userdata('is_card_agency');		
		$is_center = $CI->session->userdata('is_center');				
		$is_branch = $CI->session->userdata('is_branch');				
		$is_direct_branch = $CI->session->userdata('is_direct_branch');				
		$is_reception = false ;//$CI->session->userdata('is_reception');
        $only_view_notice = $CI->session->userdata('only_view_notice');
        $is_hbn_cast = $CI->session->userdata('is_hbn_cast');
        $is_recycle = $CI->session->userdata('is_recycle') ;

        if ($login_id && ( $is_manager || $is_agency || $is_card_agency || $is_center || $is_branch || $is_direct_branch || $only_view_notice || $is_hbn_cast || $is_recycle ))
		{		
			$CI->load->model(ADM_F.'/Member_model');
			$member = $CI->Member_model->get_member($login_id);
			if ($member['USERID']) 
			{
				$is_member = TRUE;
			}
		}
		else if($login_id)
		{
			$member = $CI->Basic_model->get_member($login_id);
			if (isset($member['USERID']) && $member['USERID']) 
			{
				$is_member = TRUE;
			}
		}
		else
			$member['mb_level'] = 1;
		
		$php_self     = $CI->input->server('PHP_SELF');
		$http_referer = $CI->input->server('HTTP_REFERER');

        $is_test_server = FALSE ;
		if ( $_SERVER['SERVER_NAME'] == "hbnetwork2.cafe24.com" )        	
		{
			$is_test_server = TRUE ;
		}

		// SSL 관련 설정
		/*if ( !$is_test_server && !$CI->agent->is_mobile() )
		{
			$CI->load->helper('url') ;
			$repself = str_replace('/index.php/www', '', $php_self) ;

			if ( isset($_SERVER['HTTPS']) )
			{
				if ( !$is_member && $repself != "/member/find/" && $repself != "/member/find/result" && $repself != "/member/login/" && $repself != "/member/login/qry/1" && $repself != "/member/login/in" )
				{

					$url = "http://" . $_SERVER['SERVER_NAME'] . $repself ;
					redirect($url) ;
				}
			}
			else
			{
				if ( $is_member || $repself == "/member/find/" || $repself == "/member/find/result" || $repself == "/member/login/" || $repself == "/member/login/qry/1" || $repself == "/member/login/in")
				{

					//exit( make_session_result( $result ) );
					//exit( make_session_result($result) );



					//$result["is_mobile"] = $CI->agent->is_mobile();
					//$result["session_result"] = 0;
					//echo json_encode($result);
					//return;

					$url = "https://" . $_SERVER['SERVER_NAME'] . $repself ;
					redirect($url) ;
				}
			}
		}*/

		// visit
		/*if ($CI->session->userdata('ck_visit_ip') != $CI->input->server('REMOTE_ADDR')) {
			$CI->session->set_userdata('ck_visit_ip', $CI->input->server('REMOTE_ADDR'));
			
			$visit_referer = ($http_referer) ? $http_referer : $php_self;
			$CI->db->simple_query(" insert into hb_visit ( vi_ip, vi_date, vi_time, vi_referer, vi_agent ) values ( '".$CI->input->server('REMOTE_ADDR')."', '".TIME_YMD."', '".TIME_HIS."', '".$visit_referer."', '".$CI->input->server('HTTP_USER_AGENT')."' ) ");
		}*/

		// 관리자 페이지
		if ($is_manager) 
		{
			
        }
        elseif ($CI->uri->segment(1) == ADM_F)
        {
        	if( $CI->uri->segment(2) != 'login' )
        		show_404();
        }

		$referer = parse_url($http_referer);
		$repself = str_replace('/index.php', '', $php_self);
		if (!empty($referer['path']) && $referer['path'] != $repself)
			$url = $http_referer;
		else
			$url = '/';		
	
		define('URL', $url);
		define('IS_MEMBER', $is_member);
		define('SU_ADMIN', $is_super);
		define('IS_MANAGER', $is_manager);
		define('IS_AGENCY', $is_agency);
		define('IS_CARD_AGENCY', $is_card_agency);
		define('IS_CENTER', $is_center);
		define('IS_BRANCH', $is_branch);
		define('IS_DIRECT_BRANCH', $is_direct_branch);
		define('IS_RECEPTION', $is_reception);
		define('MEMBER', serialize($member));
        define('ONLY_VIEW_NOTICE', $only_view_notice) ;
        define('IS_HBN_CAST', $is_hbn_cast);
        define('IS_RECYCLE', $is_recycle) ;
		
		define('IS_TEST_SERVER', $is_test_server);
	}
}
?>