var MemberForm = function () {

    return {
        
        //Masking
        init: function (form) {
	        // Validation for login form
	        $(form).validate({
	        	onkeyup: false,
	            rules: 
	            {
		            mb_name: { required:true,  minlength:2 },
		            mb_jumin_no: { required:true,  minlength:13, maxlength:13 },
					mb_password: { required:true, minlength:3 },
					mb_password_re: { required:true, equalTo:'#mb_password'},

		            /*mb_tel_1: { required:true, number:true, minlength:3, maxlength:3 },
		            mb_tel_2: { required:true, number:true, minlength:3, maxlength:4 },
		            mb_tel_3: { required:true, number:true, minlength:4, maxlength:4 },*/

		            mb_hp_1: { required:true, number:true, minlength:3, maxlength:3 },
		            mb_hp_2: { required:true, number:true, minlength:3, maxlength:4 },
		            mb_hp_3: { required:true, number:true, minlength:4, maxlength:4 },
		            
		            //mb_email: { required:true },
		            mb_addr1: { required:true },
		            mb_addr2: { required:true },

		            mb_bank: { required:true },
		            mb_bank_owner: { required:true },
		            mb_bank_serial: { required:true, number:true },	            

		            mb_center: { required:true },
		            re_confirm: { required:true },

		            username: { required:true },
		            userid: { required:true },
		            jumin_no: { required:true, number:true },

		            login_id: { required:true },
		            e_mail: { required:true },
		        },
		        messages: {
		            mb_id: '아이디를 확인 해 주세요',
		            mb_name: { required:'이름을 입력 해 주세요', minlength:'최소 2자 이상 입력하세요' },
		            mb_jumin_no: '주민등록번호를 입력 해 주세요',
		            
		            mb_password: { required:'비밀번호를 입력하세요.', minlength:'최소 3자 이상 입력하세요.' },
					mb_password_re: { required:'비밀번호 확인을 입력하세요.', equalTo:'비밀번호가 일치하지 않습니다.' },
		            
		            mb_tel_1: { required:'전화 번호를 입력하세요', number:'숫자만 입력 가능합니다', minlength:'숫자 3자로 입력하세요' },
		            mb_tel_2: { required:'전화 번호를 입력하세요', number:'숫자만 입력 가능합니다', minlength:'최소 3자 이상 입력하세요', maxlength:'최대 4자로 입력하세요' },
		            mb_tel_3: { required:'전화 번호를 입력하세요', number:'숫자만 입력 가능합니다', minlength:'숫자 4자로 입력하세요' },

		            mb_hp_1: { required:'휴대폰 번호를 입력하세요', number:'숫자만 입력 가능합니다', minlength:'숫자 3자로 입력하세요' },
		            mb_hp_2: { required:'휴대폰 번호를 입력하세요', number:'숫자만 입력 가능합니다', minlength:'최소 3자 이상 입력하세요', maxlength:'최대 4자로 입력하세요' },
		            mb_hp_3: { required:'휴대폰 번호를 입력하세요', number:'숫자만 입력 가능합니다', minlength:'숫자 4자로 입력하세요' },

		            mb_email: '이메일 주소를 입력하세요',
		            mb_addr1: '주소를 입력 해 주세요',
		            mb_addr2: '주소를 입력 해 주세요',

		            mb_bank: '은행을 선택 해 주세요',
		            mb_bank_owner: '예금주를 입력 해 주세요',
		           	mb_bank_serial: { required:'계좌번호를 입력하세요', number:'숫자만 입력 가능합니다' },

		           	mb_supporter_id: '후원인 정보를 입력 해 주세요',
		           	mb_recommander_id: '추천인 정보를 입력 해 주세요',

		           	mb_center: '가입 센터를 선택해 주세요',
		           	re_confirm: '체크 해 주세요',

		           	username: '회원명을 입력해주세요',
		           	userid: '회원ID를 입력해주세요',
		           	jumin_no: { required:'주민번호를 입력해주세요', number:'숫자만 입력 가능합니다' },

		           	login_id: '아이디를 입력해주세요',
		           	e_mail: '이메일을 입력해주세요',
		        },                  
	                                        
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };

}();