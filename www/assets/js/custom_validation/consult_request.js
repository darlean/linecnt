var ConsultRequestForm = function () {

    return {
        
        //Masking
        init: function (form) {
	        // Validation for login form
	        $(form).validate({
	        	onkeyup: false,
	            rules: 
	            {
	            	cs_name: { required:true },
		            cs_region: { required:true },
		            cs_mobile_no_1: { required:true, number:true, minlength:3, maxlength:3 },
		            cs_mobile_no_2: { required:true, number:true, minlength:3, maxlength:4 },
		            cs_mobile_no_3: { required:true, number:true, minlength:4, maxlength:4 },
		        },
		        messages: {
		            cs_name: '이름을 입력해주세요',
		            cs_region: '지역을 입력해주세요',
		            mb_name: { required:'이름을 입력하세요', minlength:'최소 2자 이상 입력하세요' },
		            cs_mobile_no_1: { required:'휴대폰 번호를 입력하세요', number:'숫자만 입력 가능합니다', minlength:'숫자 3자로 입력하세요' },
		            cs_mobile_no_2: { required:'휴대폰 번호를 입력하세요', number:'숫자만 입력 가능합니다', minlength:'최소 3자 이상 입력하세요', maxlength:'최대 4자로 입력하세요' },
		            cs_mobile_no_3: { required:'휴대폰 번호를 입력하세요', number:'숫자만 입력 가능합니다', minlength:'숫자 4자로 입력하세요' },
		        },                  
	                                        
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };

}();