var RegionDiv = function() 
{
    function append(obj)
    {
        var htmlText = '<div class="row">\
                            <div id="add_region_section">\
                                <div id="div_region_1">\
                                    <section class="col col-5" style="margin-bottom:0px;">\
                                        <label class="select">\
                                            <select name="mb_sido_1" id="mb_sido">\
                                                <option value="">시, 도</option>\
                                            </select>\
                                            <i></i>\
                                        </label>\
                                    </section>\
                                    <section class="col col-5" style="margin-bottom:0px;">\
                                        <label class="select">\
                                            <select name="mb_gugun_1" id="mb_gugun_1">\
                                                <option value="">구, 군</option>\
                                            </select>\
                                            <i></i>\
                                        </label>\
                                    </section>\
                                </div>\
                            </div>\
                            <section class="col col-2" style="margin-bottom:0px;">\
                                <a href="#" id="add_mb_region" class="btn-u btn-u-default">지역추가</a>\
                            </section>\
                        </div>' ;

        $(obj).append(htmlText) ;

        $.ajax({
            type : "POST",
            url : "/member/region/main_region",
            success : function (data) 
            { 
                $('#mb_sido').append(data) ;
            }
        });
    }

    function add_region_div()
    {
        $('#add_mb_region').click(function()
        {
            var div_num = $("div[id^='div_region']").length + 1 ;
            var new_div = $('#div_region_1').clone(true) ;  
            new_div.attr('id', "div_region_"+div_num) ;
            new_div.find("select[name^='mb_sido']").attr('name', "mb_sido_"+div_num) ;
            new_div.find("select[name^='mb_gugun']").attr('name', "mb_gugun_"+div_num).attr('id', "mb_gugun_"+div_num) ;

            $('#add_region_section').append(new_div) ;
        }) ;
    }

    function add_sub_region()
    {
        $('#mb_sido').change(function()
        {
            var obj_sido_name = ($(this).attr('name')) ;
            var obj_gugun_id = obj_sido_name.replace("mb_sido_", "mb_gugun_") ;
            var sido = $(this).val() ;

            $.ajax({
                type : "POST",
                url : "/member/region/sub_region",
                data : 
                {
                    "obj_gugun_id" : obj_gugun_id, "sido" : sido
                },
                success : function (data) 
                { 
                    if (obj_gugun_id && sido) 
                    {   
                        $('#'+obj_gugun_id).empty() ;
                        $('#'+obj_gugun_id).append(data) ;
                    } 
                }
            });

        }) ;
    }

    function add_region()
    {
        $.ajax({
            type : "POST",
            url : "<?=RT_PATH?>/member/join/bbb",
            success : function (data) 
            { 
                $('#mb_sido').append(data) ;
            }
        });
    }

    return {
        init: function (obj) {
            append(obj);
            add_region_div();
            add_sub_region();
        },
    }

}() ;                