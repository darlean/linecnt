document.write("<link rel='stylesheet' type='text/css' href='/assets/plugins/d3/css/digraph.css'>"); 
document.write("<script src='/assets/plugins/d3/js/d3.v3.min.js'></script>"); 
document.write("<script src='/assets/plugins/d3/js/dagre-d3.js'></script>"); 
document.write("<script src='/assets/plugins/d3/js/spin.js'></script>"); 

var opts = {
  lines: 13 // The number of lines to draw
, length: 28 // The length of each line
, width: 14 // The line thickness
, radius: 42 // The radius of the inner circle
, scale: 1 // Scales overall size of the spinner
, corners: 1 // Corner roundness (0..1)
, color: '#000' // #rgb or #rrggbb or array of colors
, opacity: 0.25 // Opacity of the lines
, rotate: 0 // The rotation offset
, direction: 1 // 1: clockwise, -1: counterclockwise
, speed: 1 // Rounds per second
, trail: 60 // Afterglow percentage
, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
, zIndex: 2e9 // The z-index (defaults to 2000000000)
, className: 'spinner' // The CSS class to assign to the spinner
, top: '50%' // Top position relative to parent
, left: '50%' // Left position relative to parent
, shadow: false // Whether to render a shadow
, hwaccel: false // Whether to use hardware acceleration
, position: 'absolute' // Element positioning
} ;

var target ;
var spinner ;

var nodes = {};
var edges = [];
var hides = [];
var node_pos = {};
var svgGroup ;
var treeData ;
var nLimitDepth = -1 ;
var bSimpleTree = false ;

var zoomListener ;

function zoom() 
{
    svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
}

function make_D3_Json_data(data, user_kind, bPrint)
{    
    var dataMap = data.reduce(function(map, node) 
    {        
        node.name = node.USERNAME ;
        map[node.USERID] = node ;
        
        return map ;
    }, {});
     
    var depth = 0 ;
    var treeData = [];
    data.forEach(function(node) 
    {
        var newNode = dataMap[node.USERID] ;   
        var parent ;

        if ( user_kind == "P" )     
        {
            parent = dataMap[node.P_ID] ; 
        }
        else
        {
            parent = dataMap[node.R_ID] ; 
        }                

        depth = ( parent && parent.children ) ? parent.children[0].depth : depth + 1 ;     
        newNode.depth = depth ;

        if (parent) 
        {               
            (parent.children || (parent.children = [])).push(newNode) ;
        } 
        else 
        {            
            treeData = newNode ;
        }
    });       

    if ( bPrint )
    {
        alert(print_arr(treeData));
    }

    return treeData ;
}

function createTree(i_treedata, i_mb_id, i_bSimpleTree, i_nLimitDepth)
{
    bSimpleTree = i_bSimpleTree ;

    target = document.getElementById('spinner');
    spinner = new Spinner(opts) ;    

    zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom) ;

    treeData = i_treedata ;

    if ( i_nLimitDepth )
    {
        nLimitDepth = i_nLimitDepth ;
    }

    renderTree(true) ;
    transformNode(i_mb_id, 0, true) ;
}

function renderTree(bfirst)
{
    spinner.spin(target) ;

    $("#svg-canvas").empty() ;     
    
    nodes = {};
    edges = [];         
        
    populate(treeData, nodes, edges, bfirst) ;    

    var g = new dagreD3.graphlib.Graph()
        .setGraph({})
        .setDefaultEdgeLabel(function () {
          return {};
        });

    for (var key in nodes) {
      var node = nodes[key];
      g.setNode(node.id, {            
        shape: "rect", 
        label: node.label,
        class: node.nodeclass,
        rx: 5, 
        ry: 5,         
        paddingLeft: 0, 
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0, 
        labelType:(bSimpleTree) ? "text" : "html"      
      });
    }

    edges.forEach(function (e) {
      g.setEdge(e.source, e.target, {
        lineTension: .8,        
        arrowhead: "undirected",            
        lineInterpolate: "basis"        
      });
    });

    var render = new dagreD3.render();

    var svg = d3.select("#svg-canvas") ;
    svgGroup = svg.append("g") ;

    render(d3.select("#svg-canvas g"), g);

    svg.call(zoomListener) ;    

    drawCollapseImg() ;

    d3.select('g').transition().each("end", function () {
        spinner.stop() ;
    });      

    svg.on("dblclick.zoom", null);

    svgGroup.selectAll("g.node").on("dblclick", dblclick) ;    
}

function populate(data, nodes, edges, bfirst) 
{    
    var newNode = {
        label:(bSimpleTree) ? data.textNode : data.drawNode,
        id: data.USERID,
    };
    
    newNode.children = ( data.children && data.children.length ) ? true : false ;    

    if ( data.ISCLASS == 'X' && data.USERID != 'D0000000' )
    {
        newNode.nodeclass = "x-node" ;
    }
    else 
    {
        newNode.nodeclass = "" ;
    }

    nodes[data.USERID] = newNode ;

    if ( bfirst && nLimitDepth > 0 && data.depth >= nLimitDepth )
    {
        hides[data.USERID] = true ;
    }

    if ( !hides[data.USERID] )
    {
        for (var key in data.children) 
        {
            var child = data.children[key] ;
            var newChild = populate(child, nodes, edges, bfirst);

            if ( newChild )
            {
                edges.push({
                    source: newNode.id,
                    target: newChild.id,
                    id: newNode.id + "-" + newChild.id
                });
            }
        }        
    }

    return newNode;
}

function drawCollapseImg()
{
    d3.selectAll("g.node").selectAll("g.label").each(function() 
    {      
        var id = d3.select(this).data() ;          

        if (hides[id]) 
        {
            d3.select(this).append('image')
                .attr("transform", function(d) {
                var y = getRectHeight() / 2 ;
                return "translate(-7," + y + ")";
            })
            .attr("width", 14)
            .attr("height", 16)
            .attr("xlink:href", "/data/img/plus_circle.png");
        }
    }) ;
}

function saveNodePos()
{
    d3.selectAll("g.node").each(function() 
    {      
        var id = d3.select(this).data() ;          
        var t = d3.transform(d3.select(this).attr("transform")) ;

        var new_pos = {
          x0: -t.translate[0],
          y0: t.translate[1]
        };

        node_pos[id] = new_pos ;    
    }) ;
}

function toggleChildren(d) 
{  
    if (hides[d]) 
    {
        hides[d] = false ;        
    } 
    else
    {
        hides[d] = true ;
    }    
}

function getRectHeight() 
{
    return d3.select("rect").attr('height') ;
}

function transformNode(d, i, bfirst) 
{
    var x = 0, y = 0 ;

    d3.selectAll("g.node").each(function() 
    {      
        if ( d3.select(this).data() == d )
        {
            var t = d3.transform(d3.select(this).attr("transform")) ;

            x = -t.translate[0] ;
            y = t.translate[1] ;        

            return ;
        }
    }) ;

    var scale = zoomListener.scale() ;       

    if ( bfirst )
    {
        x = x * scale + $(document).width() / 2 ;
        y = y * scale - getRectHeight() / 2 + 10 ;
    }       
    else
    {                
        var pos = zoomListener.translate() ;    

        x = ( x - node_pos[d].x0 ) * scale + pos[0] ;
        y = ( y - node_pos[d].y0 ) * scale + pos[1] ;
    }        

    d3.select('g').attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");

    zoomListener.scale(scale);
    zoomListener.translate([x, y]);

    saveNodePos() ;
}

function centerNode(d) 
{
    var x = 0, y = 0 ;

    d3.selectAll("g.node").each(function() 
    {      
        if ( d3.select(this).data() == d )
        {
            var t = d3.transform(d3.select(this).attr("transform")) ;

            x = -t.translate[0] ;
            y = -t.translate[1] ;        

            return ;
        }
    }) ;

    var scale = zoomListener.scale() ;       
    
    x = x * scale + $(document).width() / 2 ;
    y = y * scale + $(document).height() / 2 ;

    d3.select('g').attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");

    zoomListener.scale(scale);
    zoomListener.translate([x, y]);

    saveNodePos() ;
}

// Toggle children on click.
function default_click(d, i) 
{
    if ( nodes[d].children )
    {    
        if (d3.event.defaultPrevented) 
            return ;

        toggleChildren(d) ;
        renderTree() ;
        transformNode(d, i) ;    
    }
}

function dblclick()
{   
}

function click(d, i)
{
    default_click(d, i) ;
}
